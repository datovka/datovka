/*-----QWidget-----*/
QWidget
{
	background-color: @soft_black;
	color: @smoke_white;
	border-color: @darker_gray;
	selection-background-color: @selection_bg_color_dark_theme;
	selection-color: @smoke_white;
	font-size: @font_size_pt;
}


/*-----QLabel-----*/
QLabel
{
	color: @smoke_white;
	border: none;
}

QLabel::disabled
{
	color: @darker_gray;
	border: none
}


/*-----QTextEdit-----*/
QTextEdit
{
	color: @smoke_white;
	border: 1px solid @darker_gray;
}

QTextEdit::disabled
{
	color: @darker_gray;
	border: 1px solid @darker_gray;
}


/*-----QPushButton-----*/
QPushButton
{
	background-color: @selection_bg_color_dark_theme;
	color: @smoke_white;
	font-weight: bold;
	border-radius: 8px;
	padding: @0.50frtopx @0.80frtopx;
	border: none;
}

QPushButton::disabled
{
	background-color: @dark_gray;
	color: @darker_gray;
	border: 2px solid @darker_gray;
}

QPushButton:hover
{
	background-color: @orange_hover;
}

QPushButton:pressed
{
	background-color: @orange_pressed;
}


/*-----QLineEdit-----*/
QLineEdit
{
	background-color: transparent;
	color: @smoke_white;
	border-bottom: 1px solid @smoke_white;
	border-top: none;
	border-left: none;
	border-right: none;
}

QLineEdit::disabled
{
	color: @darker_gray;
	border-bottom: 1px solid @darker_gray;
}


/*-----QCalendarWidget-----*/
QCalendarWidget QWidget#qt_calendar_navigationbar
{
	background-color: @selection_bg_color_dark_theme;
}

QCalendarWidget QToolButton
{
	background-color: transparent;
	color: @smoke_white;
}

QCalendarWidget:disabled
{
	color: @darker_gray;
}


/*-----QToolButton-----*/
QToolButton
{
	color: @smoke_white;
	padding: @0.20frtopx @0.40frtopx;
}

QToolButton::disabled
{
	color: @darker_gray;
}

QToolButton:hover:!pressed
{
	border: 1px solid @smoke_white;
	border-radius: 8px;
}

QToolButton::pressed
{
	border: 2px solid @smoke_white;
	border-radius: 8px;
}


/*-----QToolBar-----*/
QToolBar::separator
{
	width: 1px;
	background-color: @darker_gray;
}


/*-----QTableView-----*/
QTableView
{
	border: 1px solid @darker_gray;
	color: @smoke_white;
	gridline-color: @soft_black;
	outline : 0;
}

QTableView::disabled
{
	border: none;
	color: @darker_gray;
	gridline-color: @soft_black;
	outline : 0;
}

QTableView::item:selected
{
	background-color: @selection_bg_color_dark_theme;
	color: @smoke_white;
}

QTableView::item:selected:disabled
{
	background-color: @light_gray;
	border: none;
	color: @extra_light_gray;
}


/*-----QTableCornerButton-----*/
QTableCornerButton::section
{
	background-color: @ultra_light_gray;
	border: none;
	color: @smoke_white;
}


/*-----QHeaderView-----*/
QHeaderView::section
{
	color: @smoke_white;
	border: none;
	background-color: @gray;
	padding: @0.20frtopx 0px @0.20frtopx @0.20frtopx;
}

QHeaderView::section:checked
{
	color: @soft_black;
	background-color: @blue_gray;
}


/*-----QScrollBar-----*/
QScrollBar:horizontal
{
	background-color: transparent;
	height: 8px;
	margin: 0px;
	padding: 0px;
}

QScrollBar::handle:horizontal
{
	border: none;
	min-width: 100px;
	background-color: @light_gray;
}

QScrollBar::add-line:horizontal,
QScrollBar::sub-line:horizontal,
QScrollBar::add-page:horizontal,
QScrollBar::sub-page:horizontal
{
	width: 0px;
	background-color: @ultra_light_gray;
}

QScrollBar:vertical
{
	background-color: transparent;
	width: 8px;
	margin: 0;
}

QScrollBar::handle:vertical
{
	border: none;
	min-height: 100px;
	background-color: @light_gray;
}

QScrollBar::add-line:vertical,
QScrollBar::sub-line:vertical,
QScrollBar::add-page:vertical,
QScrollBar::sub-page:vertical
{
	height: 0px;
	background-color: @ultra_light_gray;
}


/*-----QMenu-----*/
QMenu::separator
{
	height: 1px;
	background-color: @darker_gray;
}

QMenu::item:selected
{
	background-color: @selection_bg_color_dark_theme;
}

QMenu::item:pressed
{
	background-color: @selection_bg_color_dark_theme;
}

QMenu::item:disabled
{
	color: @darker_gray;
}


/*-----QMenuBar-----*/
QMenuBar::item:selected 
{
	background-color: @selection_bg_color_dark_theme;
	color: @smoke_white;
}

QMenuBar::item:disabled
{
	color: @darker_gray;
}


/*-----LoweredTreeView-----*/
LoweredTreeView
{
	color: @smoke_white;
	border: 1px solid @darker_gray;
}


/*-----QPlainTextEdit-----*/
QPlainTextEdit
{
	color: @smoke_white;
	border: 1px solid @smoke_white;
}

QPlainTextEdit::disabled
{
	color: @darker_gray;
	border: 1px solid @darker_gray;
}


/*-----QTabBar-----*/
QTabBar::tab
{
	background-color: @gray;
	color: @smoke_white;
	border: none;
	padding: 5px 10px;
}

QTabBar::tab:selected
{
	background-color: transparent;
}


/*-----QTabWidget-----*/
QTabWidget::pane
{
	border: none;
}


/*-----QCheckBox-----*/
QCheckBox
{
	color: @smoke_white;
}

QCheckBox::indicator
{
	width: 15px;
	height: 15px;
	border: 1px solid @smoke_white;
	background-color: transparent;
}

QCheckBox::indicator:checked
{
	background-color: transparent;
	border: 1px solid @smoke_white;
	image: url('://stylesheet/icons/16x16/app_ok_white.png');
}

QCheckBox::indicator:unchecked
{
	image: url(none);
}

QCheckBox:disabled
{
	color: @darker_gray;
}

QCheckBox::indicator:disabled
{
	border: 2px solid @darker_gray;
	background-color: transparent;
	image: url(none);
}

QCheckBox::indicator:checked:disabled
{
	border: 2px solid @darker_gray;
	background-color: transparent;
	image: url('://stylesheet/icons/16x16/app_ok_disabled.png');
}


/*-----LoweredTableView-----*/
LoweredTableView
{
	background-color: transparent;
}


/*-----QRadioButton-----*/
QRadioButton
{
	color: @smoke_white;
}

QRadioButton::indicator
{
	width: 16px;
	height: 16px;
	background-image: url('://stylesheet/icons/16x16/radio_btn_unchecked.png');
	background-repeat: no-repeat;
	background-position: center;
	border: none;
}

QRadioButton::indicator:checked
{
	background-image: url('://stylesheet/icons/16x16/radio_btn_checked.png');
	border: none;
}

QRadioButton:disabled
{
	color: @darker_gray;
}

QRadioButton::indicator:disabled
{
	background-image: url('://stylesheet/icons/16x16/app_ok_disabled.png');
	border: none;
}


/*-----QComboBox-----*/
QComboBox
{
	border: 1px solid @smoke_white;
	padding: 5px;
	color: @smoke_white;
	background-color: @almost_dark_gray;
}

QComboBox QAbstractItemView
{
	border: 1px solid @smoke_white;
	background-color: @almost_dark_gray;
	color: @smoke_white;
}

QComboBox:disabled
{
	border: 1px solid @darker_gray;
	color: @darker_gray;
}

/*-----QGroupBox-----*/
QGroupBox
{
	border-radius: 8px;
	border: 1px solid @light_gray;
	margin-top: 10px;
	padding: 5px;
}

QGroupBox::title
{
	subcontrol-origin: margin;
	subcontrol-position: top left;
	padding: 0 5px;
}


/*-----QListView-----*/
QListView
{
	border-radius: 5px;
	border: 1px solid @darker_gray;
	padding: 5px;
}
