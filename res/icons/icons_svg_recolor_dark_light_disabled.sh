#!/usr/bin/env sh

# Light Theme
LIGHT_MAIN_BORDER="#818181"
LIGHT_ACTION_BACKGROUND="#0086FB"
LIGHT_ACTION_BORDER="#F5F5F5"

# Dark Theme
DARK_MAIN_BORDER="#B0B0B0"
DARK_ACTION_BACKGROUND="#F57C00"
DARK_ACTION_BORDER="#303030"

# Disabled icon
DISABLED_MAIN_BORDER="#616161"
DISABLED_ACTION_BACKGROUND="#616161"
DISABLED_ACTION_BORDER="#303030"
DISABLED_ACTION_BACKGROUND2="#616161"

#----------------------------------

# Source svg color
SRC_MAIN_BORDER="#B0B0B1"
SRC_ACTION_BACKGROUND="#F57C01"
SRC_ACTION_BORDER="#303031"

SRC_DIR="svg"

TRG_DIR_LIGHT="svg_light"
TGR_DIR_DARK="svg_dark"
TRG_DIR_DISABLED="svg_disabled"

if [ ! -d ${SRC_DIR} ]; then
	echo "Directory ${SRC_DIR} doesn't exist or isn't a directory." >2
	exit 1
fi

rm -rf ${TRG_DIR_LIGHT}
rm -rf ${TGR_DIR_DARK}
rm -rf ${TRG_DIR_DISABLED}

mkdir -p ${TRG_DIR_LIGHT}
mkdir -p ${TGR_DIR_DARK}
mkdir -p ${TRG_DIR_DISABLED}

# LIGHT
for SVG_PATH in ${SRC_DIR}/*; do
	SVG_FILE=$(echo ${SVG_PATH} | sed -e 's/^.*\///g') # Strip leading path up to last '/'.
    sed -e "s/${SRC_MAIN_BORDER}/${LIGHT_MAIN_BORDER}/g" -e "s/${SRC_ACTION_BACKGROUND}/${LIGHT_ACTION_BACKGROUND}/g" -e "s/${SRC_ACTION_BORDER}/${LIGHT_ACTION_BORDER}/g" ${SVG_PATH} > ${TRG_DIR_LIGHT}/${SVG_FILE}
done

# DARK
for SVG_PATH in ${SRC_DIR}/*; do
 	SVG_FILE=$(echo ${SVG_PATH} | sed -e 's/^.*\///g') # Strip leading path up to last '/'.
    sed -e "s/${SRC_MAIN_BORDER}/${DARK_MAIN_BORDER}/g" -e "s/${SRC_ACTION_BACKGROUND}/${DARK_ACTION_BACKGROUND}/g" -e "s/${SRC_ACTION_BORDER}/${DARK_ACTION_BORDER}/g" ${SVG_PATH} > ${TGR_DIR_DARK}/${SVG_FILE}
done

# DISABLED
for SVG_PATH in ${SRC_DIR}/*; do
 	SVG_FILE=$(echo ${SVG_PATH} | sed -e 's/^.*\///g') # Strip leading path up to last '/'.
      sed -e "s/${SRC_MAIN_BORDER}/${DISABLED_MAIN_BORDER}/g" -e "s/${SRC_ACTION_BACKGROUND}/${DISABLED_ACTION_BACKGROUND}/g" -e "s/${SRC_ACTION_BORDER}/${DISABLED_ACTION_BORDER}/g" -e "s/${SRC_ACTION_BACKGROUND2}/${DISABLED_ACTION_BACKGROUND2}/g" ${SVG_PATH} > ${TRG_DIR_DISABLED}/${SVG_FILE}
done
