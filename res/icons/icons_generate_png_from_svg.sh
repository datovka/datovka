#!/usr/bin/env sh

#$(export DIM=64; export FILE=filter; mkdir "${DIM}x${DIM}"; inkscape -w ${DIM} -h ${DIM} svg/${FILE}.svg -o "${DIM}x${DIM}/${FILE}.png")

DIMENSIONS=""
DIMENSIONS="${DIMENSIONS} 16"
DIMENSIONS="${DIMENSIONS} 24"
DIMENSIONS="${DIMENSIONS} 32"
DIMENSIONS="${DIMENSIONS} 48"
DIMENSIONS="${DIMENSIONS} 64"
#DIMENSIONS="${DIMENSIONS} 128"
#DIMENSIONS="${DIMENSIONS} 256"

# Dark PNGs
SRC_DIR="svg_dark"
THEME_DARK="dark"
if [ ! -d ${SRC_DIR} ]; then
	echo "Directory ${SRC_DIR} doesn't exist or isn't a directory." >2
	exit 1
fi

for SVG_PATH in ${SRC_DIR}/*; do

	SVG_FILE=$(echo ${SVG_PATH} | sed -e 's/^.*\///g') # Strip leading path up to last '/'.
	PNG_FILE=$(echo ${SVG_FILE} | sed -e 's/\..*$/.png/g') # Replace complete suffix with ".png".

	for DIM in ${DIMENSIONS}; do
		TGT_DIR="${THEME_DARK}/${DIM}x${DIM}"
		PNG_PATH="${TGT_DIR}/${PNG_FILE}"
		# Create missing directory.
		if [ ! -e ${TGT_DIR} ]; then
			mkdir -p ${TGT_DIR}
		fi
		# Test directory existence.
		if [ ! -d ${TGT_DIR} ]; then
			echo "Directory ${TGT_DIR} doesn't exist or isn't a directory." >2
			exit 1
		fi
		# Convert images.
		if inkscape -w ${DIM} -h ${DIM} ${SVG_PATH} -o "${PNG_PATH}"; then
			echo "Succeeded: ${SVG_PATH} -> ${PNG_PATH}"
		else 
			echo "Failed: ${SVG_PATH} -> ${PNG_PATH}" >2
		fi
	done
done


# Light PNGs
SRC_DIR="svg_light"
THEME_LIGHT="light"

if [ ! -d ${SRC_DIR} ]; then
	echo "Directory ${SRC_DIR} doesn't exist or isn't a directory." >2
	exit 1
fi

for SVG_PATH in ${SRC_DIR}/*; do

	SVG_FILE=$(echo ${SVG_PATH} | sed -e 's/^.*\///g') # Strip leading path up to last '/'.
	PNG_FILE=$(echo ${SVG_FILE} | sed -e 's/\..*$/.png/g') # Replace complete suffix with ".png".

	for DIM in ${DIMENSIONS}; do
		TGT_DIR="${THEME_LIGHT}/${DIM}x${DIM}"
		PNG_PATH="${TGT_DIR}/${PNG_FILE}"
		# Create missing directory.
		if [ ! -e ${TGT_DIR} ]; then
			mkdir -p ${TGT_DIR}
		fi
		# Test directory existence.
		if [ ! -d ${TGT_DIR} ]; then
			echo "Directory ${TGT_DIR} doesn't exist or isn't a directory." >2
			exit 1
		fi
		# Convert images.
		if inkscape -w ${DIM} -h ${DIM} ${SVG_PATH} -o "${PNG_PATH}"; then
			echo "Succeeded: ${SVG_PATH} -> ${PNG_PATH}"
		else 
			echo "Failed: ${SVG_PATH} -> ${PNG_PATH}" >2
		fi
	done
done


# Disabled PNGs
SRC_DIR="svg_disabled"
THEME_DISABLED="disabled"

if [ ! -d ${SRC_DIR} ]; then
	echo "Directory ${SRC_DIR} doesn't exist or isn't a directory." >2
	exit 1
fi

for SVG_PATH in ${SRC_DIR}/*; do

	SVG_FILE=$(echo ${SVG_PATH} | sed -e 's/^.*\///g') # Strip leading path up to last '/'.
	PNG_FILE=$(echo ${SVG_FILE} | sed -e 's/\..*$/.png/g') # Replace complete suffix with ".png".

	for DIM in ${DIMENSIONS}; do
		TGT_DIR="${THEME_DISABLED}/${DIM}x${DIM}"
		PNG_PATH="${TGT_DIR}/${PNG_FILE}"
		# Create missing directory.
		if [ ! -e ${TGT_DIR} ]; then
			mkdir -p ${TGT_DIR}
		fi
		# Test directory existence.
		if [ ! -d ${TGT_DIR} ]; then
			echo "Directory ${TGT_DIR} doesn't exist or isn't a directory." >2
			exit 1
		fi
		# Convert images.
		if inkscape -w ${DIM} -h ${DIM} ${SVG_PATH} -o "${PNG_PATH}"; then
			echo "Succeeded: ${SVG_PATH} -> ${PNG_PATH}"
		else 
			echo "Failed: ${SVG_PATH} -> ${PNG_PATH}" >2
		fi
	done
done


# Permanent PNGs
SRC_DIR="svg_permanent"

if [ ! -d ${SRC_DIR} ]; then
	echo "Directory ${SRC_DIR} doesn't exist or isn't a directory." >2
	exit 1
fi

for SVG_PATH in ${SRC_DIR}/*; do

	SVG_FILE=$(echo ${SVG_PATH} | sed -e 's/^.*\///g') # Strip leading path up to last '/'.
	PNG_FILE=$(echo ${SVG_FILE} | sed -e 's/\..*$/.png/g') # Replace complete suffix with ".png".

	for DIM in ${DIMENSIONS}; do
		TGT_DIR="${THEME_LIGHT}/${DIM}x${DIM}"
		PNG_PATH="${TGT_DIR}/${PNG_FILE}"
		# Create missing directory.
		if [ ! -e ${TGT_DIR} ]; then
			mkdir -p ${TGT_DIR}
		fi
		# Test directory existence.
		if [ ! -d ${TGT_DIR} ]; then
			echo "Directory ${TGT_DIR} doesn't exist or isn't a directory." >2
			exit 1
		fi
		# Convert images.
		if inkscape -w ${DIM} -h ${DIM} ${SVG_PATH} -o "${PNG_PATH}"; then
			echo "Succeeded: ${SVG_PATH} -> ${PNG_PATH}"
		else 
			echo "Failed: ${SVG_PATH} -> ${PNG_PATH}" >2
		fi

		TGT_DIR="${THEME_DARK}/${DIM}x${DIM}"
		PNG_PATH="${TGT_DIR}/${PNG_FILE}"
		# Create missing directory.
		if [ ! -e ${TGT_DIR} ]; then
			mkdir -p ${TGT_DIR}
		fi
		# Test directory existence.
		if [ ! -d ${TGT_DIR} ]; then
			echo "Directory ${TGT_DIR} doesn't exist or isn't a directory." >2
			exit 1
		fi
		# Convert images.
		if inkscape -w ${DIM} -h ${DIM} ${SVG_PATH} -o "${PNG_PATH}"; then
			echo "Succeeded: ${SVG_PATH} -> ${PNG_PATH}"
		else 
			echo "Failed: ${SVG_PATH} -> ${PNG_PATH}" >2
		fi
	done
done

