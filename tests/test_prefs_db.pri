
QT += sql

DEFINES += \
	TEST_PREFS_DB=1

SOURCES += \
	$${top_srcdir}src/datovka_shared/graphics/colour.cpp \
	$${top_srcdir}src/datovka_shared/io/prefs_db.cpp \
	$${top_srcdir}src/datovka_shared/io/prefs_db_tables.cpp \
	$${top_srcdir}src/datovka_shared/io/sqlite/db.cpp \
	$${top_srcdir}src/datovka_shared/io/sqlite/db_single.cpp \
	$${top_srcdir}src/datovka_shared/io/sqlite/table.cpp \
	$${top_srcdir}src/datovka_shared/settings/prefs.cpp \
	$${top_srcdir}src/global.cpp \
	$${top_srcdir}tests/test_prefs_db.cpp

HEADERS += \
	$${top_srcdir}src/datovka_shared/graphics/colour.h \
	$${top_srcdir}src/datovka_shared/io/prefs_db.h \
	$${top_srcdir}src/datovka_shared/io/prefs_db_tables.h \
	$${top_srcdir}src/datovka_shared/io/db_tables.h \
	$${top_srcdir}src/datovka_shared/io/sqlite/db.h \
	$${top_srcdir}src/datovka_shared/io/sqlite/db_single.h \
	$${top_srcdir}src/datovka_shared/io/sqlite/table.h \
	$${top_srcdir}src/datovka_shared/settings/prefs.h \
	$${top_srcdir}src/global.h \
	$${top_srcdir}tests/test_prefs_db.h
