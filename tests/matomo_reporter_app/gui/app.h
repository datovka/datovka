/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMainWindow>
#include <QTimer>

#include "ui_app.h"

class MatomoReporter;
class QUrl;

class MainWindow : public QMainWindow, public Ui::MainWindow {
	Q_OBJECT
public:
	MainWindow(void);

private Q_SLOTS:
	void runTracker(void);

	void timerStart(void);

	void timerStop(void);

	void setTimerInterval(int ms);

	void appendUrl(const QUrl &url);

private:
	/*!
	 * @brief Connect top menu entries to appropriate actions.
	 */
	void connectTopMenuBarActions(void);

	QTimer m_timer;
	MatomoReporter *m_reporter;
};
