/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <cstdlib>
#include <QApplication>

#include "src/datovka_shared/log/log.h"
#include "tests/matomo_reporter_app/gui/app.h"

static
int allocGlobLog(void)
{
	GlobInstcs::logPtr = new (::std::nothrow) LogDevice;
	if (Q_NULLPTR == GlobInstcs::logPtr) {
		return -1;
	}

	return 0;
}

static
void deallocGlobLog(void)
{
	if (Q_NULLPTR != GlobInstcs::logPtr) {
		delete GlobInstcs::logPtr;
		GlobInstcs::logPtr = Q_NULLPTR;
	}
}

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	if (0 != allocGlobLog()) {
		/* Cannot continue without logging facility. */
		return EXIT_FAILURE;
	}

	{
		/* Use default Qt handler. */
		QtMessageHandler oldHandler = qInstallMessageHandler(globalLogOutput);
		GlobInstcs::logPtr->installMessageHandler(oldHandler);

		GlobInstcs::logPtr->setLogVerbosity(3);
		GlobInstcs::logPtr->setDebugVerbosity(3);
		GlobInstcs::logPtr->setLogLevelBits(LogDevice::LF_STDERR, LOGSRC_ANY,
	            LOG_UPTO(LOG_WARNING));
	}

	QCoreApplication::setApplicationName(APP_NAME);
	QCoreApplication::setApplicationVersion(VERSION);

	MainWindow mainWin;
	mainWin.show();

	int ret = app.exec();

	deallocGlobLog();

	return ret;
}
