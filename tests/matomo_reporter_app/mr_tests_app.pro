
QT += core network
QT += gui widgets

top_srcdir = ../..

TEMPLATE = app
APP_NAME = mr_test_app

include($${top_srcdir}/pri/version.pri)
include($${top_srcdir}/pri/check_qt_version.pri)

sufficientQtVersion(5, 2, 3, 2)

isEmpty(MOC_DIR) {
	MOC_DIR = gen_moc
}
isEmpty(OBJECTS_DIR) {
	OBJECTS_DIR = gen_objects
}
isEmpty(UI_DIR) {
	UI_DIR = gen_ui
}
CONFIG += object_parallel_to_source

DEFINES += \
	MATOMO_REPORTER_DEBUG=0 \
	DEBUG=1 \
	VERSION=\\\"$${VERSION}\\\" \
	APP_NAME=\\\"$${APP_NAME}\\\"

QMAKE_CXXFLAGS = -g -O0
isEqual(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++11
}
greaterThan(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++17
}
QMAKE_CXXFLAGS += \
	-Wall -Wextra -pedantic \
	-Wdate-time -Wformat -Werror=format-security

INCLUDEPATH += \
	$${top_srcdir}

SOURCES += \
	$${top_srcdir}/src/datovka_shared/compat_qt/random.cpp \
	$${top_srcdir}/src/datovka_shared/io/matomo_reporter.cpp \
	$${top_srcdir}/src/datovka_shared/log/global.cpp \
	$${top_srcdir}/src/datovka_shared/json/helper.cpp \
	$${top_srcdir}/src/datovka_shared/json/object.cpp \
	$${top_srcdir}/src/datovka_shared/json/report.cpp \
	$${top_srcdir}/src/datovka_shared/log/log.cpp \
	$${top_srcdir}/src/datovka_shared/log/log_c.cpp \
	$${top_srcdir}/src/datovka_shared/log/log_device.cpp \
	$${top_srcdir}/src/datovka_shared/log/memory_log.cpp \
	$${top_srcdir}/src/io/stats_reporter.cpp \
	$${top_srcdir}/tests/matomo_reporter_app/gui/app.cpp \
	main.cpp

HEADERS += \
	$${top_srcdir}/src/datovka_shared/compat/compiler.h \
	$${top_srcdir}/src/datovka_shared/compat_qt/random.h \
	$${top_srcdir}/src/datovka_shared/io/matomo_reporter.h \
	$${top_srcdir}/src/datovka_shared/log/global.h \
	$${top_srcdir}/src/datovka_shared/json/helper.h \
	$${top_srcdir}/src/datovka_shared/json/object.h \
	$${top_srcdir}/src/datovka_shared/json/report.h \
	$${top_srcdir}/src/datovka_shared/log/log_c.h \
	$${top_srcdir}/src/datovka_shared/log/log_common.h \
	$${top_srcdir}/src/datovka_shared/log/log_device.h \
	$${top_srcdir}/src/datovka_shared/log/log.h \
	$${top_srcdir}/src/datovka_shared/log/memory_log.h \
	$${top_srcdir}/src/io/stats_reporter.h \
	$${top_srcdir}/tests/matomo_reporter_app/gui/app.h \

FORMS += \
	$${top_srcdir}/tests/matomo_reporter_app/ui/app.ui

RESOURCES +=
TRANSLATIONS +=
OTHER_FILES +=
