/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <libdatovka/isds.h>
#include <QString>
#include <QtTest/QtTest>

#include "src/datovka_shared/isds/generic_interface.h"
#include "src/datovka_shared/log/log.h"
#include "src/isds/generic_conversion.h"
#include "tests/test_isds_generic_interface.h"

class TestIsdsGenericInterface : public QObject {
	Q_OBJECT

public:
	TestIsdsGenericInterface(void);

private slots:
	void initTestCase(void);

	void cleanupTestCase(void);

	void status(void);

	void statusCompare(void);

	void statusCostructor(void);

	void statusCopy(void);

	void statusSetValues(void);

	void statusConversion(void);
};

TestIsdsGenericInterface::TestIsdsGenericInterface(void)
{
}

void TestIsdsGenericInterface::initTestCase(void)
{
	QVERIFY(GlobInstcs::logPtr == Q_NULLPTR);
}

void TestIsdsGenericInterface::cleanupTestCase(void)
{
}

void TestIsdsGenericInterface::status(void)
{
	Isds::Status st1, st2;
	enum Isds::Type::ResponseStatusType type = Isds::Type::RST_DB;
	const QString code("code");
	const QString message("message");
	const QString refNum("refNum");

	/* Must be null. */
	QVERIFY(st1.type() == Isds::Type::RST_UNKNOWN);
	QVERIFY(st1.code().isNull());
	QVERIFY(st1.message().isNull());
	QVERIFY(st1.refNum().isNull());
	QVERIFY(st2.type() == Isds::Type::RST_UNKNOWN);
	QVERIFY(st2.code().isNull());
	QVERIFY(st2.message().isNull());
	QVERIFY(st2.refNum().isNull());

	QVERIFY(st1.isNull());
	QVERIFY(st2.isNull());
	QVERIFY(st1 == st2);

	/* Check whether values are not cross-linked. */
	st1.setType(type);
	st1.setCode(code);
	st1.setMessage(message);
	st1.setRefNum(refNum);

	QVERIFY(st1.type() == type);
	QVERIFY(st1.code() == code);
	QVERIFY(st1.message() == message);
	QVERIFY(st1.refNum() == refNum);

	QVERIFY(!st1.isNull());
	QVERIFY(st2.isNull());
	QVERIFY(st1 != st2);

	/* Check value copying. */
	st2 = st1;

	QVERIFY(st2.type() == type);
	QVERIFY(st2.code() == code);
	QVERIFY(st2.message() == message);
	QVERIFY(st2.refNum() == refNum);

	QVERIFY(!st1.isNull());
	QVERIFY(!st2.isNull());
	QVERIFY(st1 == st2);

	/* Clear value. */
	st1 = Isds::Status();

	QVERIFY(st1.type() == Isds::Type::RST_UNKNOWN);
	QVERIFY(st1.code().isNull());
	QVERIFY(st1.message().isNull());
	QVERIFY(st1.refNum().isNull());
	QVERIFY(st2.type() == type);
	QVERIFY(st2.code() == code);
	QVERIFY(st2.message() == message);
	QVERIFY(st2.refNum() == refNum);

	QVERIFY(st1.isNull());
	QVERIFY(!st2.isNull());
	QVERIFY(st1 != st2);
}

void TestIsdsGenericInterface::statusCompare(void)
{
	Isds::Status st1, st2;
	enum Isds::Type::ResponseStatusType type = Isds::Type::RST_DB;
	const QString code("code");
	const QString message("message");
	const QString refNum("refNum");

	st1.setType(type);
	st1.setCode(code);
	st1.setMessage(message);
	st1.setRefNum(refNum);
	st2.setType(type);
	st2.setCode(code);
	st2.setMessage(message);
	st2.setRefNum(refNum);

	QVERIFY(st1 == st2);

	st2.setType(Isds::Type::RST_DM);
	QVERIFY(st1 != st2);
	st2.setType(type);
	QVERIFY(st1 == st2);

	st2.setCode("");
	QVERIFY(st1 != st2);
	st2.setCode(code);;
	QVERIFY(st1 == st2);

	st2.setMessage("");
	QVERIFY(st1 != st2);
	st2.setMessage(message);;
	QVERIFY(st1 == st2);

	st2.setRefNum("");
	QVERIFY(st1 != st2);
	st2.setRefNum(refNum);;
	QVERIFY(st1 == st2);
}

void TestIsdsGenericInterface::statusCostructor(void)
{
	{
		Isds::Status st;

		QVERIFY(st.isNull());

		QVERIFY(st.type() == Isds::Type::RST_UNKNOWN);
		QVERIFY(st.code().isNull());
		QVERIFY(st.message().isNull());
		QVERIFY(st.refNum().isNull());
	}
}

void TestIsdsGenericInterface::statusCopy(void)
{
	Isds::Status st1;
	enum Isds::Type::ResponseStatusType type = Isds::Type::RST_DB;
	const QString code("code");
	const QString message("message");
	const QString refNum("refNum");

	st1.setType(type);
	st1.setCode(code);
	st1.setMessage(message);
	st1.setRefNum(refNum);

	/* Constructor. */
	{
		Isds::Status st2(st1);

		QVERIFY(!st1.isNull());
		QVERIFY(!st2.isNull());
		QVERIFY(st1 == st2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::Status st3(::std::move(st2));

		QVERIFY(st2.isNull());
		QVERIFY(st1 != st2);

		QVERIFY(!st1.isNull());
		QVERIFY(!st3.isNull());
		QVERIFY(st1 == st3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::Status st2;

		QVERIFY(st2.isNull());

		st2 = st1;

		QVERIFY(!st1.isNull());
		QVERIFY(!st2.isNull());
		QVERIFY(st1 == st2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::Status st3;

		QVERIFY(st3.isNull());

		st3 = ::std::move(st2);

		QVERIFY(st2.isNull());
		QVERIFY(st1 != st2);

		QVERIFY(!st1.isNull());
		QVERIFY(!st3.isNull());
		QVERIFY(st1 == st3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsGenericInterface::statusSetValues(void)
{
	const QString cCode("code");
	const QString cMessage("message");
	const QString cRefNum("refNum");

	Isds::Status st;
	//enum Isds::Type::ResponseStatusType type = Isds::Type::RST_DB;
	QString code(cCode);
	QString message(cMessage);
	QString refNum(cRefNum);

	QVERIFY(st.code().isNull());
	st.setCode(code);
	QVERIFY(st.code() == cCode);
	QVERIFY(code == cCode);
#ifdef Q_COMPILER_RVALUE_REFS
	st.setCode("other_code");
	QVERIFY(st.code() != cCode);
	st.setCode(::std::move(code));
	QVERIFY(st.code() == cCode);
	QVERIFY(code != cCode);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(st.message().isNull());
	st.setMessage(message);
	QVERIFY(st.message() == cMessage);
	QVERIFY(message == cMessage);
#ifdef Q_COMPILER_RVALUE_REFS
	st.setMessage("other_message");
	QVERIFY(st.message() != cMessage);
	st.setMessage(::std::move(message));
	QVERIFY(st.message() == cMessage);
	QVERIFY(message != cMessage);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(st.refNum().isNull());
	st.setRefNum(refNum);
	QVERIFY(st.refNum() == cRefNum);
	QVERIFY(refNum == cRefNum);
#ifdef Q_COMPILER_RVALUE_REFS
	st.setRefNum("other_refNum");
	QVERIFY(st.refNum() != cRefNum);
	st.setRefNum(::std::move(refNum));
	QVERIFY(st.refNum() == cRefNum);
	QVERIFY(refNum != cRefNum);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsGenericInterface::statusConversion(void)
{
	Isds::Status st1, st2;
	enum Isds::Type::ResponseStatusType type = Isds::Type::RST_DB;
	const QString code("code");
	const QString message("message");
	const QString refNum("refNum");

	bool iOk = false;
	struct isds_status *ist = NULL;

	QVERIFY(st1.isNull());
	QVERIFY(st2.isNull());
	QVERIFY(st1 == st2);

	st1.setType(type);
	st1.setCode(code);
	st1.setMessage(message);
	st1.setRefNum(refNum);

	QVERIFY(!st1.isNull());
	QVERIFY(st2.isNull());
	QVERIFY(st1 != st2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	st2 = Isds::libisds2status(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(st2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	ist = Isds::status2libisds(st2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(ist == NULL);

	iOk = false;
	ist = Isds::status2libisds(st1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(ist != NULL);

	iOk = false;
	st2 = Isds::libisds2status(ist, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!st2.isNull());

	QVERIFY(st2.type() == type);
	QVERIFY(st2.code() == code);
	QVERIFY(st2.message() == message);
	QVERIFY(st2.refNum() == refNum);

	QVERIFY(!st1.isNull());
	QVERIFY(!st2.isNull());
	QVERIFY(st1 == st2);

	isds_status_free(&ist);
	QVERIFY(ist == NULL);
}

QObject *newTestIsdsGenericInterface(void)
{
	return new (::std::nothrow) TestIsdsGenericInterface();
}

//QTEST_MAIN(TestIsdsGenericInterface)
#include "test_isds_generic_interface.moc"
