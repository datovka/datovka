
DEFINES += \
	TEST_VERSION=1

INCLUDEPATH += \
	/usr/include/libxml2

LIBS += \
	-ldatovka \
	-lcrypto

SOURCES += \
	$${top_srcdir}src/datovka_shared/app_version_info.cpp \
	$${top_srcdir}tests/test_version.cpp

HEADERS += \
	$${top_srcdir}src/datovka_shared/app_version_info.h \
	$${top_srcdir}tests/test_version.h
