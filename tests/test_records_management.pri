
DEFINES += \
	TEST_RECORDS_MANAGEMENT=1

SOURCES += \
	$${top_srcdir}src/datovka_shared/json/basic.cpp \
	$${top_srcdir}src/datovka_shared/json/helper.cpp \
	$${top_srcdir}src/datovka_shared/json/object.cpp \
	$${top_srcdir}src/datovka_shared/records_management/json/entry_error.cpp \
	$${top_srcdir}src/datovka_shared/records_management/json/service_info.cpp \
	$${top_srcdir}src/datovka_shared/records_management/json/stored_files.cpp \
	$${top_srcdir}src/datovka_shared/records_management/json/upload_account_status.cpp \
	$${top_srcdir}src/datovka_shared/records_management/json/upload_file.cpp \
	$${top_srcdir}src/datovka_shared/records_management/json/upload_hierarchy.cpp \
	$${top_srcdir}src/records_management/content/automatic_upload_target.cpp \
	$${top_srcdir}tests/test_records_management.cpp

HEADERS += \
	$${top_srcdir}src/datovka_shared/json/basic.h \
	$${top_srcdir}src/datovka_shared/json/helper.h \
	$${top_srcdir}src/datovka_shared/json/object.h \
	$${top_srcdir}src/datovka_shared/records_management/json/entry_error.h \
	$${top_srcdir}src/datovka_shared/records_management/json/service_info.h \
	$${top_srcdir}src/datovka_shared/records_management/json/stored_files.h \
	$${top_srcdir}src/datovka_shared/records_management/json/upload_account_status.h \
	$${top_srcdir}src/datovka_shared/records_management/json/upload_file.h \
	$${top_srcdir}src/datovka_shared/records_management/json/upload_hierarchy.h \
	$${top_srcdir}src/records_management/content/automatic_upload_target.h \
	$${top_srcdir}tests/test_records_management.h
