
DEFINES += \
	TEST_CRYPTO_PIN_PWD=1

LIBS += \
	-lcrypto

SOURCES += \
	$${top_srcdir}src/common.cpp \
	$${top_srcdir}src/datovka_shared/compat_qt/random.cpp \
	$${top_srcdir}src/datovka_shared/crypto/crypto_pin.c \
	$${top_srcdir}src/datovka_shared/crypto/crypto_pwd.c \
	$${top_srcdir}src/datovka_shared/crypto/crypto_wrapped.cpp \
	$${top_srcdir}src/datovka_shared/identifiers/account_id.cpp \
	$${top_srcdir}src/datovka_shared/settings/account.cpp \
	$${top_srcdir}src/datovka_shared/settings/account_container.cpp \
	$${top_srcdir}src/datovka_shared/settings/pin.cpp \
	$${top_srcdir}src/datovka_shared/settings/prefs_helper.cpp \
	$${top_srcdir}src/settings/account.cpp \
	$${top_srcdir}src/settings/accounts.cpp \
	$${top_srcdir}tests/test_crypto_pin_pwd.cpp

HEADERS += \
	$${top_srcdir}src/common.h \
	$${top_srcdir}src/datovka_shared/compat_qt/random.h \
	$${top_srcdir}src/datovka_shared/crypto/crypto_pin.h \
	$${top_srcdir}src/datovka_shared/crypto/crypto_pwd.h \
	$${top_srcdir}src/datovka_shared/crypto/crypto_wrapped.h \
	$${top_srcdir}src/datovka_shared/identifiers/account_id.h \
	$${top_srcdir}src/datovka_shared/identifiers/account_id_p.h \
	$${top_srcdir}src/datovka_shared/settings/account.h \
	$${top_srcdir}src/datovka_shared/settings/account_container.h \
	$${top_srcdir}src/datovka_shared/settings/account_p.h \
	$${top_srcdir}src/datovka_shared/settings/pin.h \
	$${top_srcdir}src/datovka_shared/settings/prefs_helper.h \
	$${top_srcdir}src/settings/account.h \
	$${top_srcdir}src/settings/accounts.h \
	$${top_srcdir}tests/test_crypto_pin_pwd.h
