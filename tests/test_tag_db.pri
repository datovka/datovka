
QT += sql

DEFINES += \
	TEST_TAG_DB=1

SOURCES += \
	$${top_srcdir}src/datovka_shared/graphics/colour.cpp \
	$${top_srcdir}src/datovka_shared/identifiers/account_id.cpp \
	$${top_srcdir}src/datovka_shared/io/sqlite/db.cpp \
	$${top_srcdir}src/datovka_shared/io/sqlite/db_single.cpp \
	$${top_srcdir}src/datovka_shared/io/sqlite/table.cpp \
	$${top_srcdir}src/datovka_shared/json/basic.cpp \
	$${top_srcdir}src/datovka_shared/json/helper.cpp \
	$${top_srcdir}src/datovka_shared/json/object.cpp \
	$${top_srcdir}src/datovka_shared/settings/prefs_helper.cpp \
	$${top_srcdir}src/io/tag_db.cpp \
	$${top_srcdir}src/io/tag_db_tables.cpp \
	$${top_srcdir}src/json/tag_assignment.cpp \
	$${top_srcdir}src/json/tag_assignment_hash.cpp \
	$${top_srcdir}src/json/tag_entry.cpp \
	$${top_srcdir}src/json/tag_message_id.cpp \
	$${top_srcdir}src/json/tag_text_search_request.cpp \
	$${top_srcdir}src/json/tag_text_search_request_hash.cpp \
	$${top_srcdir}tests/test_tag_db.cpp

HEADERS += \
	$${top_srcdir}src/datovka_shared/graphics/colour.h \
	$${top_srcdir}src/datovka_shared/identifiers/account_id.h \
	$${top_srcdir}src/datovka_shared/identifiers/account_id_p.h \
	$${top_srcdir}src/datovka_shared/io/sqlite/db.h \
	$${top_srcdir}src/datovka_shared/io/sqlite/db_single.h \
	$${top_srcdir}src/datovka_shared/io/sqlite/table.h \
	$${top_srcdir}src/datovka_shared/isds/types.h \
	$${top_srcdir}src/datovka_shared/json/basic.h \
	$${top_srcdir}src/datovka_shared/json/helper.h \
	$${top_srcdir}src/datovka_shared/json/object.h \
	$${top_srcdir}src/datovka_shared/settings/prefs_helper.h \
	$${top_srcdir}src/io/tag_db.h \
	$${top_srcdir}src/io/tag_db_tables.h \
	$${top_srcdir}src/json/tag_assignment.h \
	$${top_srcdir}src/json/tag_assignment_hash.h \
	$${top_srcdir}src/json/tag_entry.h \
	$${top_srcdir}src/json/tag_message_id.h \
	$${top_srcdir}src/json/tag_text_search_request.h \
	$${top_srcdir}src/json/tag_text_search_request_hash.h \
	$${top_srcdir}tests/test_tag_db.h
