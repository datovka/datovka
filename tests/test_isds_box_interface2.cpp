/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <libdatovka/isds.h>
#include <QDate>
#include <QString>
#include <QtTest/QtTest>

#include "src/datovka_shared/isds/box_interface.h" /* Isds::BirthInfo */
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/log/log.h"
#include "src/isds/box_conversion2.h"
#include "src/isds/initialisation.h"
#include "tests/test_isds_box_interface.h"

class TestIsdsBoxInterface2 : public QObject {
	Q_OBJECT

public:
	TestIsdsBoxInterface2(void);

private slots:
	void initTestCase(void);

	void cleanupTestCase(void);

	void addressExt2(void);

	void addressExt2Compare(void);

	void addressExt2Constructor(void);

	void addressExt2Copy(void);

	void addressExt2SetValues(void);

	void addressExt2Conversion(void);

	void personName2(void);

	void personName2Compare(void);

	void personName2Constructor(void);

	void personName2Copy(void);

	void personName2SetValues(void);

	void personName2Conversion(void);

	void dbOwnerInfoExt2(void);

	void dbOwnerInfoExt2Compare(void);

	void dbOwnerInfoExt2Constructor(void);

	void dbOwnerInfoExt2Copy(void);

	void dbOwnerInfoExt2SetValues(void);

	void dbOwnerInfoExt2Conversion(void);

	void dbUserInfoExt2(void);

	void dbUserInfoExt2Compare(void);

	void dbUserInfoExt2Constructor(void);

	void dbUserInfoExt2Copy(void);

	void dbUserInfoExt2SetValues(void);

	void dbUserInfoExt2Conversion(void);
};

TestIsdsBoxInterface2::TestIsdsBoxInterface2(void)
{
}

void TestIsdsBoxInterface2::initTestCase(void)
{
	QVERIFY(GlobInstcs::logPtr == Q_NULLPTR);
}

void TestIsdsBoxInterface2::cleanupTestCase(void)
{
}

void TestIsdsBoxInterface2::addressExt2(void)
{
	Isds::AddressExt2 addr1, addr2;
	const QString code("adCode");
	const QString city("adCity");
	const QString district("adDistrict");
	const QString street("adStreet");
	const QString numberInStreet("adNumberInStreet");
	const QString numberInMunicipality("adNumberInMunicipality");
	const QString zipCode("adZipCode");
	const QString state("adState");

	/* Must be null. */
	QVERIFY(addr1.code().isNull());
	QVERIFY(addr1.city().isNull());
	QVERIFY(addr1.district().isNull());
	QVERIFY(addr1.street().isNull());
	QVERIFY(addr1.numberInStreet().isNull());
	QVERIFY(addr1.numberInMunicipality().isNull());
	QVERIFY(addr1.zipCode().isNull());
	QVERIFY(addr1.state().isNull());
	QVERIFY(addr2.code().isNull());
	QVERIFY(addr2.city().isNull());
	QVERIFY(addr2.district().isNull());
	QVERIFY(addr2.street().isNull());
	QVERIFY(addr2.numberInStreet().isNull());
	QVERIFY(addr2.numberInMunicipality().isNull());
	QVERIFY(addr2.zipCode().isNull());
	QVERIFY(addr2.state().isNull());

	QVERIFY(addr1.isNull());
	QVERIFY(addr2.isNull());
	QVERIFY(addr1 == addr2);

	/* Check whether values are not cross-linked. */
	addr1.setCode(code);
	addr1.setCity(city);
	addr1.setDistrict(district);
	addr1.setStreet(street);
	addr1.setNumberInStreet(numberInStreet);
	addr1.setNumberInMunicipality(numberInMunicipality);
	addr1.setZipCode(zipCode);
	addr1.setState(state);

	QVERIFY(addr1.code() == code);
	QVERIFY(addr1.city() == city);
	QVERIFY(addr1.district() == district);
	QVERIFY(addr1.street() == street);
	QVERIFY(addr1.numberInStreet() == numberInStreet);
	QVERIFY(addr1.numberInMunicipality() == numberInMunicipality);
	QVERIFY(addr1.zipCode() == zipCode);
	QVERIFY(addr1.state() == state);

	QVERIFY(!addr1.isNull());
	QVERIFY(addr2.isNull());
	QVERIFY(addr1 != addr2);

	/* Check value copying. */
	addr2 = addr1;

	QVERIFY(addr2.code() == code);
	QVERIFY(addr2.city() == city);
	QVERIFY(addr2.district() == district);
	QVERIFY(addr2.street() == street);
	QVERIFY(addr2.numberInStreet() == numberInStreet);
	QVERIFY(addr2.numberInMunicipality() == numberInMunicipality);
	QVERIFY(addr2.zipCode() == zipCode);
	QVERIFY(addr2.state() == state);

	QVERIFY(!addr1.isNull());
	QVERIFY(!addr2.isNull());
	QVERIFY(addr1 == addr2);

	/* Clear value. */
	addr1 = Isds::AddressExt2();

	QVERIFY(addr1.code().isNull());
	QVERIFY(addr1.city().isNull());
	QVERIFY(addr1.district().isNull());
	QVERIFY(addr1.street().isNull());
	QVERIFY(addr1.numberInStreet().isNull());
	QVERIFY(addr1.numberInMunicipality().isNull());
	QVERIFY(addr1.zipCode().isNull());
	QVERIFY(addr1.state().isNull());
	QVERIFY(addr2.code() == code);
	QVERIFY(addr2.city() == city);
	QVERIFY(addr2.district() == district);
	QVERIFY(addr2.street() == street);
	QVERIFY(addr2.numberInStreet() == numberInStreet);
	QVERIFY(addr2.numberInMunicipality() == numberInMunicipality);
	QVERIFY(addr2.zipCode() == zipCode);
	QVERIFY(addr2.state() == state);

	QVERIFY(addr1.isNull());
	QVERIFY(!addr2.isNull());
	QVERIFY(addr1 != addr2);
}

void TestIsdsBoxInterface2::addressExt2Compare(void)
{
	Isds::AddressExt2 addr1, addr2;
	const QString code("adCode");
	const QString city("adCity");
	const QString district("adDistrict");
	const QString street("adStreet");
	const QString numberInStreet("adNumberInStreet");
	const QString numberInMunicipality("adNumberInMunicipality");
	const QString zipCode("adZipCode");
	const QString state("adState");

	addr1.setCode(code);
	addr1.setCity(city);
	addr1.setDistrict(district);
	addr1.setStreet(street);
	addr1.setNumberInStreet(numberInStreet);
	addr1.setNumberInMunicipality(numberInMunicipality);
	addr1.setZipCode(zipCode);
	addr1.setState(state);
	addr2.setCode(code);
	addr2.setCity(city);
	addr2.setDistrict(district);
	addr2.setStreet(street);
	addr2.setNumberInStreet(numberInStreet);
	addr2.setNumberInMunicipality(numberInMunicipality);
	addr2.setZipCode(zipCode);
	addr2.setState(state);

	QVERIFY(addr1 == addr2);

	addr2.setCode("");
	QVERIFY(addr1 != addr2);
	addr2.setCode(code);
	QVERIFY(addr1 == addr2);

	addr2.setCity("");
	QVERIFY(addr1 != addr2);
	addr2.setCity(city);
	QVERIFY(addr1 == addr2);

	addr2.setDistrict("");
	QVERIFY(addr1 != addr2);
	addr2.setDistrict(district);
	QVERIFY(addr1 == addr2);

	addr2.setStreet("");
	QVERIFY(addr1 != addr2);
	addr2.setStreet(street);
	QVERIFY(addr1 == addr2);

	addr2.setNumberInStreet("");
	QVERIFY(addr1 != addr2);
	addr2.setNumberInStreet(numberInStreet);
	QVERIFY(addr1 == addr2);

	addr2.setNumberInMunicipality("");
	QVERIFY(addr1 != addr2);
	addr2.setNumberInMunicipality(numberInMunicipality);
	QVERIFY(addr1 == addr2);

	addr2.setZipCode("");
	QVERIFY(addr1 != addr2);
	addr2.setZipCode(zipCode);
	QVERIFY(addr1 == addr2);

	addr2.setState("");
	QVERIFY(addr1 != addr2);
	addr2.setState(state);
	QVERIFY(addr1 == addr2);
}

void TestIsdsBoxInterface2::addressExt2Constructor(void)
{
	{
		Isds::AddressExt2 addr;

		QVERIFY(addr.isNull());

		QVERIFY(addr.code().isNull());
		QVERIFY(addr.city().isNull());
		QVERIFY(addr.district().isNull());
		QVERIFY(addr.street().isNull());
		QVERIFY(addr.numberInStreet().isNull());
		QVERIFY(addr.numberInMunicipality().isNull());
		QVERIFY(addr.zipCode().isNull());
		QVERIFY(addr.state().isNull());
	}
}

void TestIsdsBoxInterface2::addressExt2Copy(void)
{
	Isds::AddressExt2 addr1;
	const QString code("adCode");
	const QString city("adCity");
	const QString district("adDistrict");
	const QString street("adStreet");
	const QString numberInStreet("adNumberInStreet");
	const QString numberInMunicipality("adNumberInMunicipality");
	const QString zipCode("adZipCode");
	const QString state("adState");

	addr1.setCode(code);
	addr1.setCity(city);
	addr1.setDistrict(district);
	addr1.setStreet(street);
	addr1.setNumberInStreet(numberInStreet);
	addr1.setNumberInMunicipality(numberInMunicipality);
	addr1.setZipCode(zipCode);
	addr1.setState(state);

	/* Constructor. */
	{
		Isds::AddressExt2 addr2(addr1);

		QVERIFY(!addr1.isNull());
		QVERIFY(!addr2.isNull());
		QVERIFY(addr1 == addr2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::AddressExt2 addr3(::std::move(addr2));

		QVERIFY(addr2.isNull());
		QVERIFY(addr1 != addr2);

		QVERIFY(!addr1.isNull());
		QVERIFY(!addr3.isNull());
		QVERIFY(addr1 == addr3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::AddressExt2 addr2;

		QVERIFY(addr2.isNull());

		addr2 = addr1;

		QVERIFY(!addr1.isNull());
		QVERIFY(!addr2.isNull());
		QVERIFY(addr1 == addr2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::AddressExt2 addr3;

		QVERIFY(addr3.isNull());

		addr3 = ::std::move(addr2);

		QVERIFY(addr2.isNull());
		QVERIFY(addr1 != addr2);

		QVERIFY(!addr1.isNull());
		QVERIFY(!addr3.isNull());
		QVERIFY(addr1 == addr3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface2::addressExt2SetValues(void)
{
	const QString cCode("adCode");
	const QString cCity("adCity");
	const QString cDistrict("adDistrict");
	const QString cStreet("adStreet");
	const QString cNumberInStreet("adNumberInStreet");
	const QString cNumberInMunicipality("adNumberInMunicipality");
	const QString cZipCode("adZipCode");
	const QString cState("adState");

	Isds::AddressExt2 addr;
	QString code(cCode);
	QString city(cCity);
	QString district(cDistrict);
	QString street(cStreet);
	QString numberInStreet(cNumberInStreet);
	QString numberInMunicipality(cNumberInMunicipality);
	QString zipCode(cZipCode);
	QString state(cState);

	QVERIFY(addr.code().isNull());
	addr.setCode(code);
	QVERIFY(addr.code() == cCode);
	QVERIFY(code == cCode);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setCode("other_code");
	QVERIFY(addr.code() != cCode);
	addr.setCode(::std::move(code));
	QVERIFY(addr.code() == cCode);
	QVERIFY(code != cCode);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.city().isNull());
	addr.setCity(city);
	QVERIFY(addr.city() == cCity);
	QVERIFY(city == cCity);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setCity("other_city");
	QVERIFY(addr.city() != cCity);
	addr.setCity(::std::move(city));
	QVERIFY(addr.city() == cCity);
	QVERIFY(city != cCity);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.district().isNull());
	addr.setDistrict(district);
	QVERIFY(addr.district() == cDistrict);
	QVERIFY(district == cDistrict);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setDistrict("other_district");
	QVERIFY(addr.district() != cDistrict);
	addr.setDistrict(::std::move(district));
	QVERIFY(addr.district() == cDistrict);
	QVERIFY(district != cDistrict);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.street().isNull());
	addr.setStreet(street);
	QVERIFY(addr.street() == cStreet);
	QVERIFY(street == cStreet);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setStreet("other_street");
	QVERIFY(addr.street() != cStreet);
	addr.setStreet(::std::move(street));
	QVERIFY(addr.street() == cStreet);
	QVERIFY(street != cStreet);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.numberInStreet().isNull());
	addr.setNumberInStreet(numberInStreet);
	QVERIFY(addr.numberInStreet() == cNumberInStreet);
	QVERIFY(numberInStreet == cNumberInStreet);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setNumberInStreet("other_numberInStreet");
	QVERIFY(addr.numberInStreet() != cNumberInStreet);
	addr.setNumberInStreet(::std::move(numberInStreet));
	QVERIFY(addr.numberInStreet() == cNumberInStreet);
	QVERIFY(numberInStreet != cNumberInStreet);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.numberInMunicipality().isNull());
	addr.setNumberInMunicipality(numberInMunicipality);
	QVERIFY(addr.numberInMunicipality() == cNumberInMunicipality);
	QVERIFY(numberInMunicipality == cNumberInMunicipality);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setNumberInMunicipality("other_numberInMunicipality");
	QVERIFY(addr.numberInMunicipality() != cNumberInMunicipality);
	addr.setNumberInMunicipality(::std::move(numberInMunicipality));
	QVERIFY(addr.numberInMunicipality() == cNumberInMunicipality);
	QVERIFY(numberInMunicipality != cNumberInMunicipality);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.zipCode().isNull());
	addr.setZipCode(zipCode);
	QVERIFY(addr.zipCode() == cZipCode);
	QVERIFY(zipCode == cZipCode);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setZipCode("other_zipCode");
	QVERIFY(addr.zipCode() != cZipCode);
	addr.setZipCode(::std::move(zipCode));
	QVERIFY(addr.zipCode() == cZipCode);
	QVERIFY(zipCode != cZipCode);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.state().isNull());
	addr.setState(state);
	QVERIFY(addr.state() == cState);
	QVERIFY(state == cState);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setState("other_state");
	QVERIFY(addr.state() != cState);
	addr.setState(::std::move(state));
	QVERIFY(addr.state() == cState);
	QVERIFY(state != cState);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface2::addressExt2Conversion(void)
{
	Isds::AddressExt2 addr1, addr2;
	const QString code("adCode");
	const QString city("adCity");
	const QString district("adDistrict");
	const QString street("adStreet");
	const QString numberInStreet("adNumberInStreet");
	const QString numberInMunicipality("adNumberInMunicipality");
	const QString zipCode("adZipCode");
	const QString state("adState");

	bool iOk = false;
	struct isds_AddressExt2 *iaddr = NULL;

	QVERIFY(addr1.isNull());
	QVERIFY(addr2.isNull());
	QVERIFY(addr1 == addr2);

	addr1.setCode(code);
	addr1.setCity(city);
	addr1.setDistrict(district);
	addr1.setStreet(street);
	addr1.setNumberInStreet(numberInStreet);
	addr1.setNumberInMunicipality(numberInMunicipality);
	addr1.setZipCode(zipCode);
	addr1.setState(state);

	QVERIFY(!addr1.isNull());
	QVERIFY(addr2.isNull());
	QVERIFY(addr1 != addr2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	addr2 = Isds::libisds2addressExt2(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(addr2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	iaddr = Isds::addressExt22libisds(addr2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(iaddr == NULL);

	iOk = false;
	iaddr = Isds::addressExt22libisds(addr1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(iaddr != NULL);

	iOk = false;
	addr2 = Isds::libisds2addressExt2(iaddr, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!addr2.isNull());

	QVERIFY(addr2.city() == city);
	QVERIFY(addr2.street() == street);
	QVERIFY(addr2.numberInStreet() == numberInStreet);
	QVERIFY(addr2.numberInMunicipality() == numberInMunicipality);
	QVERIFY(addr2.zipCode() == zipCode);
	QVERIFY(addr2.state() == state);

	QVERIFY(!addr1.isNull());
	QVERIFY(!addr2.isNull());
	QVERIFY(addr1 == addr2);

	isds_AddressExt2_free(&iaddr);
	QVERIFY(iaddr == NULL);
}

void TestIsdsBoxInterface2::personName2(void)
{
	Isds::PersonName2 pn1, pn2;
	const QString givenNames("pnGivenNames");
	const QString lastName("pnLastName");

	/* Must be null. */
	QVERIFY(pn1.givenNames().isNull());
	QVERIFY(pn1.lastName().isNull());
	QVERIFY(pn2.givenNames().isNull());
	QVERIFY(pn2.lastName().isNull());

	QVERIFY(pn1.isNull());
	QVERIFY(pn2.isNull());
	QVERIFY(pn1 == pn2);

	/* Check whether values are not cross-linked. */
	pn1.setGivenNames(givenNames);
	pn1.setLastName(lastName);

	QVERIFY(pn1.givenNames() == givenNames);
	QVERIFY(pn1.lastName() == lastName);

	QVERIFY(!pn1.isNull());
	QVERIFY(pn2.isNull());
	QVERIFY(pn1 != pn2);

	/* Check value copying. */
	pn2 = pn1;

	QVERIFY(pn2.givenNames() == givenNames);
	QVERIFY(pn2.lastName() == lastName);

	QVERIFY(!pn1.isNull());
	QVERIFY(!pn2.isNull());
	QVERIFY(pn1 == pn2);

	/* Clear value. */
	pn1 = Isds::PersonName2();

	QVERIFY(pn1.givenNames().isNull());
	QVERIFY(pn1.lastName().isNull());
	QVERIFY(pn2.givenNames() == givenNames);
	QVERIFY(pn2.lastName() == lastName);

	QVERIFY(pn1.isNull());
	QVERIFY(!pn2.isNull());
	QVERIFY(pn1 != pn2);
}

void TestIsdsBoxInterface2::personName2Compare(void)
{
	Isds::PersonName2 pn1, pn2;
	const QString givenNames("pnGivenNames");
	const QString lastName("pnLastName");

	pn1.setGivenNames(givenNames);
	pn1.setLastName(lastName);
	pn2.setGivenNames(givenNames);
	pn2.setLastName(lastName);

	QVERIFY(pn1 == pn2);

	pn2.setGivenNames("");
	QVERIFY(pn1 != pn2);
	pn2.setGivenNames(givenNames);
	QVERIFY(pn1 == pn2);

	pn2.setLastName("");
	QVERIFY(pn1 != pn2);
	pn2.setLastName(lastName);
	QVERIFY(pn1 == pn2);
}

void TestIsdsBoxInterface2::personName2Constructor(void)
{
	{
		Isds::PersonName2 pn;

		QVERIFY(pn.isNull());

		QVERIFY(pn.givenNames().isNull());
		QVERIFY(pn.lastName().isNull());
	}
}

void TestIsdsBoxInterface2::personName2Copy(void)
{
	Isds::PersonName2 pn1, pn2;
	const QString givenNames("pnGivenNames");
	const QString lastName("pnLastName");

	pn1.setGivenNames(givenNames);
	pn1.setLastName(lastName);

	/* Constructor. */
	{
		Isds::PersonName2 pn2(pn1);

		QVERIFY(!pn1.isNull());
		QVERIFY(!pn2.isNull());
		QVERIFY(pn1 == pn2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::PersonName2 pn3(::std::move(pn2));

		QVERIFY(pn2.isNull());
		QVERIFY(pn1 != pn2);

		QVERIFY(!pn1.isNull());
		QVERIFY(!pn3.isNull());
		QVERIFY(pn1 == pn3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::PersonName2 pn2;

		QVERIFY(pn2.isNull());

		pn2 = pn1;

		QVERIFY(!pn1.isNull());
		QVERIFY(!pn2.isNull());
		QVERIFY(pn1 == pn2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::PersonName2 pn3;

		QVERIFY(pn3.isNull());

		pn3 = ::std::move(pn2);

		QVERIFY(pn2.isNull());
		QVERIFY(pn1 != pn2);

		QVERIFY(!pn1.isNull());
		QVERIFY(!pn3.isNull());
		QVERIFY(pn1 == pn3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface2::personName2SetValues(void)
{
	const QString cGivenNames("pnGivenNames");
	const QString cLastName("pnLastName");

	Isds::PersonName2 pn;
	QString givenNames(cGivenNames);
	QString lastName(cLastName);

	QVERIFY(pn.givenNames().isNull());
	pn.setGivenNames(givenNames);
	QVERIFY(pn.givenNames() == cGivenNames);
	QVERIFY(givenNames == cGivenNames);
#ifdef Q_COMPILER_RVALUE_REFS
	pn.setGivenNames("other_givenNames");
	QVERIFY(pn.givenNames() != cGivenNames);
	pn.setGivenNames(::std::move(givenNames));
	QVERIFY(pn.givenNames() == cGivenNames);
	QVERIFY(givenNames != cGivenNames);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(pn.lastName().isNull());
	pn.setLastName(lastName);
	QVERIFY(pn.lastName() == cLastName);
	QVERIFY(lastName == cLastName);
#ifdef Q_COMPILER_RVALUE_REFS
	pn.setLastName("other_lastName");
	QVERIFY(pn.lastName() != cLastName);
	pn.setLastName(::std::move(lastName));
	QVERIFY(pn.lastName() == cLastName);
	QVERIFY(lastName != cLastName);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface2::personName2Conversion(void)
{
	Isds::PersonName2 pn1, pn2;
	const QString givenNames("pnGivenNames");
	const QString lastName("pnLastName");

	bool iOk = false;
	struct isds_PersonName2 *ipn = NULL;

	QVERIFY(pn1.isNull());
	QVERIFY(pn2.isNull());
	QVERIFY(pn1 == pn2);

	pn1.setGivenNames(givenNames);
	pn1.setLastName(lastName);

	QVERIFY(!pn1.isNull());
	QVERIFY(pn2.isNull());
	QVERIFY(pn1 != pn2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	pn2 = Isds::libisds2personName2(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(pn2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	ipn = Isds::personName22libisds(pn2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(ipn == NULL);

	iOk = false;
	ipn = Isds::personName22libisds(pn1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(ipn != NULL);

	iOk = false;
	pn2 = Isds::libisds2personName2(ipn, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!pn2.isNull());

	QVERIFY(pn2.givenNames() == givenNames);
	QVERIFY(pn2.lastName() == lastName);

	QVERIFY(!pn1.isNull());
	QVERIFY(!pn2.isNull());
	QVERIFY(pn1 == pn2);

	isds_PersonName2_free(&ipn);
	QVERIFY(ipn == NULL);
}

/*!
 * @brief Builds Isds::PersonName instance.
 */
static
Isds::PersonName2 buildPersonName2(void)
{
	Isds::PersonName2 pn;

	pn.setGivenNames("pnGivenNames");
	pn.setLastName("pnLastName");

	return pn;
}

/*!
 * @brief Build Isds::BirthInfo instance;
 */
static
Isds::BirthInfo buildBirthInfo(void)
{
	Isds::BirthInfo bi;

	bi.setDate(QDate(2001, 2, 3));
	bi.setCity("biCity");
	bi.setCounty("biCounty");
	bi.setState("biState");

	return bi;
}

/*!
 * @brief Build Isds::Address instance.
 */
static
Isds::AddressExt2 buildAddressExt2(void)
{
	Isds::AddressExt2 addr;

	addr.setCode("adCode");
	addr.setCity("adCity");
	addr.setDistrict("adDistrict");
	addr.setStreet("adStreet");
	addr.setNumberInStreet("adNumberInStreet");
	addr.setNumberInMunicipality("adNumberInMunicipality");
	addr.setZipCode("adZipCode");
	addr.setState("adState");

	return addr;
}

void TestIsdsBoxInterface2::dbOwnerInfoExt2(void)
{
	Isds::DbOwnerInfoExt2 doi1, doi2;
	const QString dbID("dbID");
	const enum Isds::Type::NilBool aifoIsds = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::DbType dbType = Isds::Type::BT_OVM;
	const QString ic("ic");
	const Isds::PersonName2 personName(buildPersonName2());
	const QString firmName("firmName");
	const Isds::BirthInfo birthInfo(buildBirthInfo());
	const Isds::AddressExt2 address(buildAddressExt2());
	const QString nationality("nationality");
	const QString dbIdOVM("dbIdOVM");
	const enum Isds::Type::DbState dbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool dbOpenAddressing = Isds::Type::BOOL_TRUE;
	const QString dbUpperID("dbUpperID");

	/* Must be null. */
	QVERIFY(doi1.dbID().isNull());
	QVERIFY(doi1.aifoIsds() == Isds::Type::BOOL_NULL);
	QVERIFY(doi1.dbType() == Isds::Type::BT_NULL);
	QVERIFY(doi1.ic().isNull());
	QVERIFY(doi1.personName().isNull());
	QVERIFY(doi1.firmName().isNull());
	QVERIFY(doi1.birthInfo().isNull());
	QVERIFY(doi1.address().isNull());
	QVERIFY(doi1.nationality().isNull());
	QVERIFY(doi1.dbIdOVM().isNull());
	QVERIFY(doi1.dbState() == Isds::Type::BS_ERROR);
	QVERIFY(doi1.dbOpenAddressing() == Isds::Type::BOOL_NULL);
	QVERIFY(doi1.dbUpperID().isNull());
	QVERIFY(doi2.dbID().isNull());
	QVERIFY(doi2.aifoIsds() == Isds::Type::BOOL_NULL);
	QVERIFY(doi2.dbType() == Isds::Type::BT_NULL);
	QVERIFY(doi2.ic().isNull());
	QVERIFY(doi2.personName().isNull());
	QVERIFY(doi2.firmName().isNull());
	QVERIFY(doi2.birthInfo().isNull());
	QVERIFY(doi2.address().isNull());
	QVERIFY(doi2.nationality().isNull());
	QVERIFY(doi2.dbIdOVM().isNull());
	QVERIFY(doi2.dbState() == Isds::Type::BS_ERROR);
	QVERIFY(doi2.dbOpenAddressing() == Isds::Type::BOOL_NULL);
	QVERIFY(doi2.dbUpperID().isNull());

	QVERIFY(doi1.isNull());
	QVERIFY(doi2.isNull());
	QVERIFY(doi1 == doi2);

	/* Check whether values are not cross-linked. */
	doi1.setDbID(dbID);
	doi1.setAifoIsds(aifoIsds);
	doi1.setDbType(dbType);
	doi1.setIc(ic);
	doi1.setPersonName(personName);
	doi1.setFirmName(firmName);
	doi1.setBirthInfo(birthInfo);
	doi1.setAddress(address);
	doi1.setNationality(nationality);
	doi1.setDbIdOVM(dbIdOVM);
	doi1.setDbState(dbState);
	doi1.setDbOpenAddressing(dbOpenAddressing);
	doi1.setDbUpperID(dbUpperID);

	QVERIFY(doi1.dbID() == dbID);
	QVERIFY(doi1.aifoIsds() == aifoIsds);
	QVERIFY(doi1.dbType() == dbType);
	QVERIFY(doi1.ic() == ic);
	QVERIFY(doi1.personName() == personName);
	QVERIFY(doi1.firmName() == firmName);
	QVERIFY(doi1.birthInfo() == birthInfo);
	QVERIFY(doi1.address() == address);
	QVERIFY(doi1.nationality() == nationality);
	QVERIFY(doi1.dbIdOVM() == dbIdOVM);
	QVERIFY(doi1.dbState() == dbState);
	QVERIFY(doi1.dbOpenAddressing() == dbOpenAddressing);
	QVERIFY(doi1.dbUpperID() == dbUpperID);

	QVERIFY(!doi1.isNull());
	QVERIFY(doi2.isNull());
	QVERIFY(doi1 != doi2);

	/* Check value copying. */
	doi2 = doi1;

	QVERIFY(doi2.dbID() == dbID);
	QVERIFY(doi2.aifoIsds() == aifoIsds);
	QVERIFY(doi2.dbType() == dbType);
	QVERIFY(doi2.ic() == ic);
	QVERIFY(doi2.personName() == personName);
	QVERIFY(doi2.firmName() == firmName);
	QVERIFY(doi2.birthInfo() == birthInfo);
	QVERIFY(doi2.address() == address);
	QVERIFY(doi2.nationality() == nationality);
	QVERIFY(doi2.dbIdOVM() == dbIdOVM);
	QVERIFY(doi2.dbState() == dbState);
	QVERIFY(doi2.dbOpenAddressing() == dbOpenAddressing);
	QVERIFY(doi2.dbUpperID() == dbUpperID);

	QVERIFY(!doi1.isNull());
	QVERIFY(!doi2.isNull());
	QVERIFY(doi1 == doi2);

	/* Clear value. */
	doi1 = Isds::DbOwnerInfoExt2();

	QVERIFY(doi1.dbID().isNull());
	QVERIFY(doi1.aifoIsds() == Isds::Type::BOOL_NULL);
	QVERIFY(doi1.dbType() == Isds::Type::BT_NULL);
	QVERIFY(doi1.ic().isNull());
	QVERIFY(doi1.personName().isNull());
	QVERIFY(doi1.firmName().isNull());
	QVERIFY(doi1.birthInfo().isNull());
	QVERIFY(doi1.address().isNull());
	QVERIFY(doi1.nationality().isNull());
	QVERIFY(doi1.dbIdOVM().isNull());
	QVERIFY(doi1.dbState() == Isds::Type::BS_ERROR);
	QVERIFY(doi1.dbOpenAddressing() == Isds::Type::BOOL_NULL);
	QVERIFY(doi1.dbUpperID().isNull());
	QVERIFY(doi2.dbID() == dbID);
	QVERIFY(doi2.aifoIsds() == aifoIsds);
	QVERIFY(doi2.dbType() == dbType);
	QVERIFY(doi2.ic() == ic);
	QVERIFY(doi2.personName() == personName);
	QVERIFY(doi2.firmName() == firmName);
	QVERIFY(doi2.birthInfo() == birthInfo);
	QVERIFY(doi2.address() == address);
	QVERIFY(doi2.nationality() == nationality);
	QVERIFY(doi2.dbIdOVM() == dbIdOVM);
	QVERIFY(doi2.dbState() == dbState);
	QVERIFY(doi2.dbOpenAddressing() == dbOpenAddressing);
	QVERIFY(doi2.dbUpperID() == dbUpperID);

	QVERIFY(doi1.isNull());
	QVERIFY(!doi2.isNull());
	QVERIFY(doi1 != doi2);
}

void TestIsdsBoxInterface2::dbOwnerInfoExt2Compare(void)
{
	Isds::DbOwnerInfoExt2 doi1, doi2;
	const QString dbID("dbID");
	const enum Isds::Type::NilBool aifoIsds = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::DbType dbType = Isds::Type::BT_OVM;
	const QString ic("ic");
	const Isds::PersonName2 personName(buildPersonName2());
	const QString firmName("firmName");
	const Isds::BirthInfo birthInfo(buildBirthInfo());
	const Isds::AddressExt2 address(buildAddressExt2());
	const QString nationality("nationality");
	const QString dbIdOVM("dbIdOVM");
	const enum Isds::Type::DbState dbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool dbOpenAddressing = Isds::Type::BOOL_TRUE;
	const QString dbUpperID("dbUpperID");

	doi1.setDbID(dbID);
	doi1.setAifoIsds(aifoIsds);
	doi1.setDbType(dbType);
	doi1.setIc(ic);
	doi1.setPersonName(personName);
	doi1.setFirmName(firmName);
	doi1.setBirthInfo(birthInfo);
	doi1.setAddress(address);
	doi1.setNationality(nationality);
	doi1.setDbIdOVM(dbIdOVM);
	doi1.setDbState(dbState);
	doi1.setDbOpenAddressing(dbOpenAddressing);
	doi1.setDbUpperID(dbUpperID);
	doi2.setDbID(dbID);
	doi2.setAifoIsds(aifoIsds);
	doi2.setDbType(dbType);
	doi2.setIc(ic);
	doi2.setPersonName(personName);
	doi2.setFirmName(firmName);
	doi2.setBirthInfo(birthInfo);
	doi2.setAddress(address);
	doi2.setNationality(nationality);
	doi2.setDbIdOVM(dbIdOVM);
	doi2.setDbState(dbState);
	doi2.setDbOpenAddressing(dbOpenAddressing);
	doi2.setDbUpperID(dbUpperID);

	QVERIFY(doi1 == doi2);

	doi2.setDbID("");
	QVERIFY(doi1 != doi2);
	doi2.setDbID(dbID);
	QVERIFY(doi1 == doi2);

	doi1.setAifoIsds(Isds::Type::BOOL_TRUE);
	QVERIFY(doi1 != doi2);
	doi1.setAifoIsds(aifoIsds);
	QVERIFY(doi1 == doi2);

	doi2.setDbType(Isds::Type::BT_PO);
	QVERIFY(doi1 != doi2);
	doi2.setDbType(dbType);
	QVERIFY(doi1 == doi2);

	doi2.setIc("");
	QVERIFY(doi1 != doi2);
	doi2.setIc(ic);
	QVERIFY(doi1 == doi2);

	doi2.setPersonName(Isds::PersonName2());
	QVERIFY(doi1 != doi2);
	doi2.setPersonName(personName);
	QVERIFY(doi1 == doi2);

	doi2.setFirmName("");
	QVERIFY(doi1 != doi2);
	doi2.setFirmName(firmName);
	QVERIFY(doi1 == doi2);

	doi2.setBirthInfo(Isds::BirthInfo());
	QVERIFY(doi1 != doi2);
	doi2.setBirthInfo(birthInfo);
	QVERIFY(doi1 == doi2);

	doi2.setAddress(Isds::AddressExt2());
	QVERIFY(doi1 != doi2);
	doi2.setAddress(address);
	QVERIFY(doi1 == doi2);

	doi2.setNationality("");
	QVERIFY(doi1 != doi2);
	doi2.setNationality(nationality);
	QVERIFY(doi1 == doi2);

	doi2.setDbIdOVM("");
	QVERIFY(doi1 != doi2);
	doi2.setDbIdOVM(dbIdOVM);
	QVERIFY(doi1 == doi2);

	doi2.setDbState(Isds::Type::BS_TEMP_INACCESSIBLE);
	QVERIFY(doi1 != doi2);
	doi2.setDbState(dbState);
	QVERIFY(doi1 == doi2);

	doi2.setDbOpenAddressing(Isds::Type::BOOL_FALSE);
	QVERIFY(doi1 != doi2);
	doi2.setDbOpenAddressing(dbOpenAddressing);
	QVERIFY(doi1 == doi2);

	doi2.setDbUpperID("");
	QVERIFY(doi1 != doi2);
	doi2.setDbUpperID(dbUpperID);
	QVERIFY(doi1 == doi2);
}

void TestIsdsBoxInterface2::dbOwnerInfoExt2Constructor(void)
{
	{
		Isds::DbOwnerInfoExt2 doi;

		QVERIFY(doi.isNull());

		QVERIFY(doi.dbID().isNull());
		QVERIFY(doi.aifoIsds() == Isds::Type::BOOL_NULL);
		QVERIFY(doi.dbType() == Isds::Type::BT_NULL);
		QVERIFY(doi.ic().isNull());
		QVERIFY(doi.personName().isNull());
		QVERIFY(doi.firmName().isNull());
		QVERIFY(doi.birthInfo().isNull());
		QVERIFY(doi.address().isNull());
		QVERIFY(doi.nationality().isNull());
		QVERIFY(doi.dbIdOVM().isNull());
		QVERIFY(doi.dbState() == Isds::Type::BS_ERROR);
		QVERIFY(doi.dbOpenAddressing() == Isds::Type::BOOL_NULL);
		QVERIFY(doi.dbUpperID().isNull());
	}
}

void TestIsdsBoxInterface2::dbOwnerInfoExt2Copy(void)
{
	Isds::DbOwnerInfoExt2 doi1;
	const QString dbID("dbID");
	const enum Isds::Type::NilBool aifoIsds = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::DbType dbType = Isds::Type::BT_OVM;
	const QString ic("ic");
	const Isds::PersonName2 personName(buildPersonName2());
	const QString firmName("firmName");
	const Isds::BirthInfo birthInfo(buildBirthInfo());
	const Isds::AddressExt2 address(buildAddressExt2());
	const QString nationality("nationality");
	const QString dbIdOVM("dbIdOVM");
	const enum Isds::Type::DbState dbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool dbOpenAddressing = Isds::Type::BOOL_TRUE;
	const QString dbUpperID("dbUpperID");

	doi1.setDbID(dbID);
	doi1.setAifoIsds(aifoIsds);
	doi1.setDbType(dbType);
	doi1.setIc(ic);
	doi1.setPersonName(personName);
	doi1.setFirmName(firmName);
	doi1.setBirthInfo(birthInfo);
	doi1.setAddress(address);
	doi1.setNationality(nationality);
	doi1.setDbIdOVM(dbIdOVM);
	doi1.setDbState(dbState);
	doi1.setDbOpenAddressing(dbOpenAddressing);
	doi1.setDbUpperID(dbUpperID);

	/* Constructor. */
	{
		Isds::DbOwnerInfoExt2 doi2(doi1);

		QVERIFY(!doi1.isNull());
		QVERIFY(!doi2.isNull());
		QVERIFY(doi1 == doi2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DbOwnerInfoExt2 doi3(::std::move(doi2));

		QVERIFY(doi2.isNull());
		QVERIFY(doi1 != doi2);

		QVERIFY(!doi1.isNull());
		QVERIFY(!doi3.isNull());
		QVERIFY(doi1 == doi3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::DbOwnerInfoExt2 doi2;

		QVERIFY(doi2.isNull());

		doi2 = doi1;

		QVERIFY(!doi1.isNull());
		QVERIFY(!doi2.isNull());
		QVERIFY(doi1 == doi2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DbOwnerInfoExt2 doi3;

		QVERIFY(doi3.isNull());

		doi3 = ::std::move(doi2);

		QVERIFY(doi2.isNull());
		QVERIFY(doi1 != doi2);

		QVERIFY(!doi1.isNull());
		QVERIFY(!doi3.isNull());
		QVERIFY(doi1 == doi3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface2::dbOwnerInfoExt2SetValues(void)
{
	const QString cDbID("dbID");
	const enum Isds::Type::NilBool cAifoIsds = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::DbType cDbType = Isds::Type::BT_OVM;
	const QString cIc("ic");
	const Isds::PersonName2 cPersonName(buildPersonName2());
	const QString cFirmName("firmName");
	const Isds::BirthInfo cBirthInfo(buildBirthInfo());
	const Isds::AddressExt2 cAddress(buildAddressExt2());
	const QString cNationality("nationality");
	const QString cDbIdOVM("dbIdOVM");
	const enum Isds::Type::DbState cDbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool cDbOpenAddressing = Isds::Type::BOOL_TRUE;
	const QString cDbUpperID("dbUpperID");

	Isds::DbOwnerInfoExt2 doi;
	QString dbID(cDbID);
	enum Isds::Type::NilBool aifoIsds = cAifoIsds;
	enum Isds::Type::DbType dbType = cDbType;
	QString ic(cIc);
	Isds::PersonName2 personName(cPersonName);
	QString firmName(cFirmName);
	Isds::BirthInfo birthInfo(cBirthInfo);
	Isds::AddressExt2 address(cAddress);
	QString nationality(cNationality);
	QString dbIdOVM(cDbIdOVM);
	enum Isds::Type::DbState dbState = cDbState;
	enum Isds::Type::NilBool dbOpenAddressing = cDbOpenAddressing;
	QString dbUpperID(cDbUpperID);

	QVERIFY(doi.dbID().isNull());
	doi.setDbID(dbID);
	QVERIFY(doi.dbID() == cDbID);
	QVERIFY(dbID == cDbID);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setDbID("other_dbID");
	QVERIFY(doi.dbID() != cDbID);
	doi.setDbID(::std::move(dbID));
	QVERIFY(doi.dbID() == cDbID);
	QVERIFY(dbID != cDbID);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.aifoIsds() == Isds::Type::BOOL_NULL);
	doi.setAifoIsds(aifoIsds);
	QVERIFY(doi.aifoIsds() == cAifoIsds);
	QVERIFY(aifoIsds == cAifoIsds);

	QVERIFY(doi.dbType() == Isds::Type::BT_NULL);
	doi.setDbType(dbType);
	QVERIFY(doi.dbType() == cDbType);
	QVERIFY(dbType == cDbType);

	QVERIFY(doi.ic().isNull());
	doi.setIc(ic);
	QVERIFY(doi.ic() == cIc);
	QVERIFY(ic == cIc);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setIc("other_ic");
	QVERIFY(doi.ic() != cIc);
	doi.setIc(::std::move(ic));
	QVERIFY(doi.ic() == cIc);
	QVERIFY(ic != cIc);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.personName().isNull());
	doi.setPersonName(personName);
	QVERIFY(doi.personName() == cPersonName);
	QVERIFY(personName == cPersonName);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setPersonName(Isds::PersonName2());
	QVERIFY(doi.personName() != cPersonName);
	doi.setPersonName(::std::move(personName));
	QVERIFY(doi.personName() == cPersonName);
	QVERIFY(personName != cPersonName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.firmName().isNull());
	doi.setFirmName(firmName);
	QVERIFY(doi.firmName() == cFirmName);
	QVERIFY(firmName == cFirmName);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setFirmName("other_firmName");
	QVERIFY(doi.firmName() != cFirmName);
	doi.setFirmName(::std::move(firmName));
	QVERIFY(doi.firmName() == cFirmName);
	QVERIFY(firmName != cFirmName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.birthInfo().isNull());
	doi.setBirthInfo(birthInfo);
	QVERIFY(doi.birthInfo() == cBirthInfo);
	QVERIFY(birthInfo == cBirthInfo);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setBirthInfo(Isds::BirthInfo());
	QVERIFY(doi.birthInfo() != cBirthInfo);
	doi.setBirthInfo(::std::move(birthInfo));
	QVERIFY(doi.birthInfo() == cBirthInfo);
	QVERIFY(birthInfo != cBirthInfo);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.address().isNull());
	doi.setAddress(address);
	QVERIFY(doi.address() == cAddress);
	QVERIFY(address == cAddress);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setAddress(Isds::AddressExt2());
	QVERIFY(doi.address() != cAddress);
	doi.setAddress(::std::move(address));
	QVERIFY(doi.address() == cAddress);
	QVERIFY(address != cAddress);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.nationality().isNull());
	doi.setNationality(nationality);
	QVERIFY(doi.nationality() == cNationality);
	QVERIFY(nationality == cNationality);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setNationality("other_nationality");
	QVERIFY(doi.nationality() != cNationality);
	doi.setNationality(::std::move(nationality));
	QVERIFY(doi.nationality() == cNationality);
	QVERIFY(nationality != cNationality);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.dbIdOVM().isNull());
	doi.setDbIdOVM(dbIdOVM);
	QVERIFY(doi.dbIdOVM() == cDbIdOVM);
	QVERIFY(dbIdOVM == cDbIdOVM);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setDbIdOVM("other_dbIdOVM");
	QVERIFY(doi.dbIdOVM() != cDbIdOVM);
	doi.setDbIdOVM(::std::move(dbIdOVM));
	QVERIFY(doi.dbIdOVM() == cDbIdOVM);
	QVERIFY(dbIdOVM != cDbIdOVM);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.dbState() == Isds::Type::BS_ERROR);
	doi.setDbState(dbState);
	QVERIFY(doi.dbState() == cDbState);
	QVERIFY(dbState == cDbState);

	QVERIFY(doi.dbOpenAddressing() == Isds::Type::BOOL_NULL);
	doi.setDbOpenAddressing(dbOpenAddressing);
	QVERIFY(doi.dbOpenAddressing() == cDbOpenAddressing);
	QVERIFY(dbOpenAddressing == cDbOpenAddressing);

	QVERIFY(doi.dbUpperID().isNull());
	doi.setDbUpperID(dbUpperID);
	QVERIFY(doi.dbUpperID() == cDbUpperID);
	QVERIFY(dbUpperID == cDbUpperID);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setDbUpperID("other_dbUpperID");
	QVERIFY(doi.dbUpperID() != cDbUpperID);
	doi.setDbUpperID(::std::move(dbUpperID));
	QVERIFY(doi.dbUpperID() == cDbUpperID);
	QVERIFY(dbUpperID != cDbUpperID);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface2::dbOwnerInfoExt2Conversion(void)
{
	Isds::DbOwnerInfoExt2 doi1, doi2;
	const QString dbID("dbID");
	const enum Isds::Type::NilBool aifoIsds = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::DbType dbType = Isds::Type::BT_OVM;
	const QString ic("ic");
	const Isds::PersonName2 personName(buildPersonName2());
	const QString firmName("firmName");
	const Isds::BirthInfo birthInfo(buildBirthInfo());
	const Isds::AddressExt2 address(buildAddressExt2());
	const QString nationality("nationality");
	const QString dbIdOVM("dbIdOVM");
	const enum Isds::Type::DbState dbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool dbOpenAddressing = Isds::Type::BOOL_TRUE;
	const QString dbUpperID("dbUpperID");

	bool iOk = false;
	struct isds_DbOwnerInfoExt2 *idoi = NULL;

	QVERIFY(doi1.isNull());
	QVERIFY(doi2.isNull());
	QVERIFY(doi1 == doi2);

	doi1.setDbID(dbID);
	doi1.setAifoIsds(aifoIsds);
	doi1.setDbType(dbType);
	doi1.setIc(ic);
	doi1.setPersonName(personName);
	doi1.setFirmName(firmName);
	doi1.setBirthInfo(birthInfo);
	doi1.setAddress(address);
	doi1.setNationality(nationality);
	doi1.setDbIdOVM(dbIdOVM);
	doi1.setDbState(dbState);
	doi1.setDbOpenAddressing(dbOpenAddressing);
	doi1.setDbUpperID(dbUpperID);

	QVERIFY(!doi1.isNull());
	QVERIFY(doi2.isNull());
	QVERIFY(doi1 != doi2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	doi2 = Isds::libisds2dbOwnerInfoExt2(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(doi2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	idoi = Isds::dbOwnerInfoExt22libisds(doi2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(idoi == NULL);

	iOk = false;
	idoi = Isds::dbOwnerInfoExt22libisds(doi1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(idoi != NULL);

	iOk = false;
	doi2 = Isds::libisds2dbOwnerInfoExt2(idoi, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!doi2.isNull());

	QVERIFY(doi2.dbID() == dbID);
	QVERIFY(doi2.aifoIsds() == aifoIsds);
	QVERIFY(doi2.dbType() == dbType);
	QVERIFY(doi2.ic() == ic);
	QVERIFY(doi2.personName() == personName);
	QVERIFY(doi2.firmName() == firmName);
	QVERIFY(doi2.birthInfo() == birthInfo);
	QVERIFY(doi2.address() == address);
	QVERIFY(doi2.nationality() == nationality);
	QVERIFY(doi2.dbIdOVM() == dbIdOVM);
	QVERIFY(doi2.dbState() == dbState);
	QVERIFY(doi2.dbOpenAddressing() == dbOpenAddressing);
	QVERIFY(doi2.dbUpperID() == dbUpperID);

	QVERIFY(!doi1.isNull());
	QVERIFY(!doi2.isNull());
	QVERIFY(doi1 == doi2);

	isds_DbOwnerInfoExt2_free(&idoi);
	QVERIFY(idoi == NULL);
}

void TestIsdsBoxInterface2::dbUserInfoExt2(void)
{
	Isds::DbUserInfoExt2 dui1, dui2;
	const enum Isds::Type::NilBool aifoIsds = Isds::Type::BOOL_TRUE;
	const Isds::PersonName2 personName(buildPersonName2());
	const Isds::AddressExt2 address(buildAddressExt2());
	const QDate biDate(2001, 2, 3);
	const QString isdsID("isdsID");
	const enum Isds::Type::UserType userType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges userPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const QString ic("ic");
	const QString firmName("firmName");
	const QString caStreet("caStreet");
	const QString caCity("caCity");
	const QString caZipCode("caZipCode");
	const QString caState("caState");

	/* Must be null. */
	QVERIFY(dui1.aifoIsds() == Isds::Type::BOOL_NULL);
	QVERIFY(dui1.personName().isNull());
	QVERIFY(dui1.address().isNull());
	QVERIFY(dui1.biDate().isNull());
	QVERIFY(dui1.isdsID().isNull());
	QVERIFY(dui1.userType() == Isds::Type::UT_NULL);
	QVERIFY(dui1.userPrivils() == Isds::Type::PRIVIL_NONE);
	QVERIFY(dui1.ic().isNull());
	QVERIFY(dui1.firmName().isNull());
	QVERIFY(dui1.caStreet().isNull());
	QVERIFY(dui1.caCity().isNull());
	QVERIFY(dui1.caZipCode().isNull());
	QVERIFY(dui1.caState().isNull());
	QVERIFY(dui2.aifoIsds() == Isds::Type::BOOL_NULL);
	QVERIFY(dui2.personName().isNull());
	QVERIFY(dui2.address().isNull());
	QVERIFY(dui2.biDate().isNull());
	QVERIFY(dui2.isdsID().isNull());
	QVERIFY(dui2.userType() == Isds::Type::UT_NULL);
	QVERIFY(dui2.userPrivils() == Isds::Type::PRIVIL_NONE);
	QVERIFY(dui2.ic().isNull());
	QVERIFY(dui2.firmName().isNull());
	QVERIFY(dui2.caStreet().isNull());
	QVERIFY(dui2.caCity().isNull());
	QVERIFY(dui2.caZipCode().isNull());
	QVERIFY(dui2.caState().isNull());

	QVERIFY(dui1.isNull());
	QVERIFY(dui2.isNull());
	QVERIFY(dui1 == dui2);

	dui1.setAifoIsds(aifoIsds);
	dui1.setPersonName(personName);
	dui1.setAddress(address);
	dui1.setBiDate(biDate);
	dui1.setIsdsID(isdsID);
	dui1.setUserType(userType);
	dui1.setUserPrivils(userPrivils);
	dui1.setIc(ic);
	dui1.setFirmName(firmName);
	dui1.setCaStreet(caStreet);
	dui1.setCaCity(caCity);
	dui1.setCaZipCode(caZipCode);
	dui1.setCaState(caState);

	QVERIFY(dui1.aifoIsds() == aifoIsds);
	QVERIFY(dui1.personName() == personName);
	QVERIFY(dui1.address() == address);
	QVERIFY(dui1.biDate() == biDate);
	QVERIFY(dui1.isdsID() == isdsID);
	QVERIFY(dui1.userType() == userType);
	QVERIFY(dui1.userPrivils() == userPrivils);
	QVERIFY(dui1.ic() == ic);
	QVERIFY(dui1.firmName() == firmName);
	QVERIFY(dui1.caStreet() == caStreet);
	QVERIFY(dui1.caCity() == caCity);
	QVERIFY(dui1.caZipCode() == caZipCode);
	QVERIFY(dui1.caState() == caState);

	QVERIFY(!dui1.isNull());
	QVERIFY(dui2.isNull());
	QVERIFY(dui1 != dui2);

	/* Check value copying. */
	dui2 = dui1;

	QVERIFY(dui2.aifoIsds() == aifoIsds);
	QVERIFY(dui2.personName() == personName);
	QVERIFY(dui2.address() == address);
	QVERIFY(dui2.biDate() == biDate);
	QVERIFY(dui2.isdsID() == isdsID);
	QVERIFY(dui2.userType() == userType);
	QVERIFY(dui2.userPrivils() == userPrivils);
	QVERIFY(dui2.ic() == ic);
	QVERIFY(dui2.firmName() == firmName);
	QVERIFY(dui2.caStreet() == caStreet);
	QVERIFY(dui2.caCity() == caCity);
	QVERIFY(dui2.caZipCode() == caZipCode);
	QVERIFY(dui2.caState() == caState);

	QVERIFY(!dui1.isNull());
	QVERIFY(!dui2.isNull());
	QVERIFY(dui1 == dui2);

	/* Clear value. */
	dui1 = Isds::DbUserInfoExt2();

	QVERIFY(dui1.aifoIsds() == Isds::Type::BOOL_NULL);
	QVERIFY(dui1.personName().isNull());
	QVERIFY(dui1.address().isNull());
	QVERIFY(dui1.biDate().isNull());
	QVERIFY(dui1.isdsID().isNull());
	QVERIFY(dui1.userType() == Isds::Type::UT_NULL);
	QVERIFY(dui1.userPrivils() == Isds::Type::PRIVIL_NONE);
	QVERIFY(dui1.ic().isNull());
	QVERIFY(dui1.firmName().isNull());
	QVERIFY(dui1.caStreet().isNull());
	QVERIFY(dui1.caCity().isNull());
	QVERIFY(dui1.caZipCode().isNull());
	QVERIFY(dui1.caState().isNull());
	QVERIFY(dui2.aifoIsds() == aifoIsds);
	QVERIFY(dui2.personName() == personName);
	QVERIFY(dui2.address() == address);
	QVERIFY(dui2.biDate() == biDate);
	QVERIFY(dui2.isdsID() == isdsID);
	QVERIFY(dui2.userType() == userType);
	QVERIFY(dui2.userPrivils() == userPrivils);
	QVERIFY(dui2.ic() == ic);
	QVERIFY(dui2.firmName() == firmName);
	QVERIFY(dui2.caStreet() == caStreet);
	QVERIFY(dui2.caCity() == caCity);
	QVERIFY(dui2.caZipCode() == caZipCode);
	QVERIFY(dui2.caState() == caState);

	QVERIFY(dui1.isNull());
	QVERIFY(!dui2.isNull());
	QVERIFY(dui1 != dui2);
}

void TestIsdsBoxInterface2::dbUserInfoExt2Compare(void)
{
	Isds::DbUserInfoExt2 dui1, dui2;
	const enum Isds::Type::NilBool aifoIsds = Isds::Type::BOOL_TRUE;
	const Isds::PersonName2 personName(buildPersonName2());
	const Isds::AddressExt2 address(buildAddressExt2());
	const QDate biDate(2001, 2, 3);
	const QString isdsID("isdsID");
	const enum Isds::Type::UserType userType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges userPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const QString ic("ic");
	const QString firmName("firmName");
	const QString caStreet("caStreet");
	const QString caCity("caCity");
	const QString caZipCode("caZipCode");
	const QString caState("caState");

	dui1.setAifoIsds(aifoIsds);
	dui1.setPersonName(personName);
	dui1.setAddress(address);
	dui1.setBiDate(biDate);
	dui1.setIsdsID(isdsID);
	dui1.setUserType(userType);
	dui1.setUserPrivils(userPrivils);
	dui1.setIc(ic);
	dui1.setFirmName(firmName);
	dui1.setCaStreet(caStreet);
	dui1.setCaCity(caCity);
	dui1.setCaZipCode(caZipCode);
	dui1.setCaState(caState);
	dui2.setAifoIsds(aifoIsds);
	dui2.setPersonName(personName);
	dui2.setAddress(address);
	dui2.setBiDate(biDate);
	dui2.setIsdsID(isdsID);
	dui2.setUserType(userType);
	dui2.setUserPrivils(userPrivils);
	dui2.setIc(ic);
	dui2.setFirmName(firmName);
	dui2.setCaStreet(caStreet);
	dui2.setCaCity(caCity);
	dui2.setCaZipCode(caZipCode);
	dui2.setCaState(caState);

	QVERIFY(dui1 == dui2);

	dui2.setAifoIsds(Isds::Type::BOOL_FALSE);
	QVERIFY(dui1 != dui2);
	dui2.setAifoIsds(aifoIsds);
	QVERIFY(dui1 == dui2);

	dui2.setPersonName(Isds::PersonName2());
	QVERIFY(dui1 != dui2);
	dui2.setPersonName(personName);
	QVERIFY(dui1 == dui2);

	dui2.setAddress(Isds::AddressExt2());
	QVERIFY(dui1 != dui2);
	dui2.setAddress(address);
	QVERIFY(dui1 == dui2);

	dui2.setBiDate(QDate(2001, 2, 4));
	QVERIFY(dui1 != dui2);
	dui2.setBiDate(biDate);
	QVERIFY(dui1 == dui2);

	dui2.setIsdsID("");
	QVERIFY(dui1 != dui2);
	dui2.setIsdsID(isdsID);
	QVERIFY(dui1 == dui2);

	dui2.setUserType(Isds::Type::UT_ENTRUSTED);
	QVERIFY(dui1 != dui2);
	dui2.setUserType(userType);
	QVERIFY(dui1 == dui2);

	dui2.setUserPrivils(Isds::Type::PRIVIL_READ_ALL);
	QVERIFY(dui1 != dui2);
	dui2.setUserPrivils(userPrivils);
	QVERIFY(dui1 == dui2);

	dui2.setIc("");
	QVERIFY(dui1 != dui2);
	dui2.setIc(ic);
	QVERIFY(dui1 == dui2);

	dui2.setFirmName("");
	QVERIFY(dui1 != dui2);
	dui2.setFirmName(firmName);
	QVERIFY(dui1 == dui2);

	dui2.setCaStreet("");
	QVERIFY(dui1 != dui2);
	dui2.setCaStreet(caStreet);
	QVERIFY(dui1 == dui2);

	dui2.setCaCity("");
	QVERIFY(dui1 != dui2);
	dui2.setCaCity(caCity);
	QVERIFY(dui1 == dui2);

	dui2.setCaZipCode("");
	QVERIFY(dui1 != dui2);
	dui2.setCaZipCode(caZipCode);
	QVERIFY(dui1 == dui2);

	dui2.setCaState("");
	QVERIFY(dui1 != dui2);
	dui2.setCaState(caState);
	QVERIFY(dui1 == dui2);
}

void TestIsdsBoxInterface2::dbUserInfoExt2Constructor(void)
{
	{
		Isds::DbUserInfoExt2 dui;

		QVERIFY(dui.isNull());

		QVERIFY(dui.aifoIsds() == Isds::Type::BOOL_NULL);
		QVERIFY(dui.personName().isNull());
		QVERIFY(dui.address().isNull());
		QVERIFY(dui.biDate().isNull());
		QVERIFY(dui.isdsID().isNull());
		QVERIFY(dui.userType() == Isds::Type::UT_NULL);
		QVERIFY(dui.userPrivils() == Isds::Type::PRIVIL_NONE);
		QVERIFY(dui.ic().isNull());
		QVERIFY(dui.firmName().isNull());
		QVERIFY(dui.caStreet().isNull());
		QVERIFY(dui.caCity().isNull());
		QVERIFY(dui.caZipCode().isNull());
		QVERIFY(dui.caState().isNull());
	}
}

void TestIsdsBoxInterface2::dbUserInfoExt2Copy(void)
{
	Isds::DbUserInfoExt2 dui1;
	const enum Isds::Type::NilBool aifoIsds = Isds::Type::BOOL_TRUE;
	const Isds::PersonName2 personName(buildPersonName2());
	const Isds::AddressExt2 address(buildAddressExt2());
	const QDate biDate(2001, 2, 3);
	const QString isdsID("isdsID");
	const enum Isds::Type::UserType userType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges userPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const QString ic("ic");
	const QString firmName("firmName");
	const QString caStreet("caStreet");
	const QString caCity("caCity");
	const QString caZipCode("caZipCode");
	const QString caState("caState");

	dui1.setAifoIsds(aifoIsds);
	dui1.setPersonName(personName);
	dui1.setAddress(address);
	dui1.setBiDate(biDate);
	dui1.setIsdsID(isdsID);
	dui1.setUserType(userType);
	dui1.setUserPrivils(userPrivils);
	dui1.setIc(ic);
	dui1.setFirmName(firmName);
	dui1.setCaStreet(caStreet);
	dui1.setCaCity(caCity);
	dui1.setCaZipCode(caZipCode);
	dui1.setCaState(caState);

	/* Constructor. */
	{
		Isds::DbUserInfoExt2 dui2(dui1);

		QVERIFY(!dui1.isNull());
		QVERIFY(!dui2.isNull());
		QVERIFY(dui1 == dui2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DbUserInfoExt2 dui3(::std::move(dui2));

		QVERIFY(dui2.isNull());
		QVERIFY(dui1 != dui2);

		QVERIFY(!dui1.isNull());
		QVERIFY(!dui3.isNull());
		QVERIFY(dui1 == dui3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::DbUserInfoExt2 dui2;

		QVERIFY(dui2.isNull());

		dui2 = dui1;

		QVERIFY(!dui1.isNull());
		QVERIFY(!dui2.isNull());
		QVERIFY(dui1 == dui2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DbUserInfoExt2 dui3;

		QVERIFY(dui3.isNull());

		dui3 = ::std::move(dui2);

		QVERIFY(dui2.isNull());
		QVERIFY(dui1 != dui2);

		QVERIFY(!dui1.isNull());
		QVERIFY(!dui3.isNull());
		QVERIFY(dui1 == dui3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface2::dbUserInfoExt2SetValues(void)
{
	const enum Isds::Type::NilBool cAifoIsds = Isds::Type::BOOL_TRUE;
	const Isds::PersonName2 cPersonName(buildPersonName2());
	const Isds::AddressExt2 cAddress(buildAddressExt2());
	const QDate cBiDate(2001, 2, 3);
	const QString cIsdsID("isdsID");
	const enum Isds::Type::UserType cUserType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges cUserPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const QString cIc("ic");
	const QString cFirmName("firmName");
	const QString cCaStreet("caStreet");
	const QString cCaCity("caCity");
	const QString cCaZipCode("caZipCode");
	const QString cCaState("caState");

	Isds::DbUserInfoExt2 dui;
	enum Isds::Type::NilBool aifoIsds = cAifoIsds;
	Isds::PersonName2 personName(cPersonName);
	Isds::AddressExt2 address(cAddress);
	QDate biDate(cBiDate);
	QString isdsID(cIsdsID);
	enum Isds::Type::UserType userType = cUserType;
	Isds::Type::Privileges userPrivils = cUserPrivils;
	QString ic(cIc);
	QString firmName(cFirmName);
	QString caStreet(cCaStreet);
	QString caCity(cCaCity);
	QString caZipCode(cCaZipCode);
	QString caState(cCaState);

	QVERIFY(dui.aifoIsds() == Isds::Type::BOOL_NULL);
	dui.setAifoIsds(aifoIsds);
	QVERIFY(dui.aifoIsds() == cAifoIsds);
	QVERIFY(aifoIsds == cAifoIsds);

	QVERIFY(dui.personName().isNull());
	dui.setPersonName(personName);
	QVERIFY(dui.personName() == cPersonName);
	QVERIFY(personName == cPersonName);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setPersonName(Isds::PersonName2());
	QVERIFY(dui.personName() != cPersonName);
	dui.setPersonName(::std::move(personName));
	QVERIFY(dui.personName() == cPersonName);
	QVERIFY(personName != cPersonName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.address().isNull());
	dui.setAddress(address);
	QVERIFY(dui.address() == cAddress);
	QVERIFY(address == cAddress);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setAddress(Isds::AddressExt2());
	QVERIFY(dui.address() != cAddress);
	dui.setAddress(::std::move(address));
	QVERIFY(dui.address() == cAddress);
	QVERIFY(address != cAddress);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.biDate().isNull());
	dui.setBiDate(biDate);
	QVERIFY(dui.biDate() == cBiDate);
	QVERIFY(biDate == cBiDate);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setBiDate(QDate(2001, 2, 4));
	QVERIFY(dui.biDate() != cBiDate);
	dui.setBiDate(::std::move(biDate));
	QVERIFY(dui.biDate() == cBiDate);
	//QVERIFY(biDate != cBiDate); /* QDate doesn't provide move assignment. */
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.isdsID().isNull());
	dui.setIsdsID(isdsID);
	QVERIFY(dui.isdsID() == cIsdsID);
	QVERIFY(isdsID == cIsdsID);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setIsdsID("other_isdsID");
	QVERIFY(dui.isdsID() != cIsdsID);
	dui.setIsdsID(::std::move(isdsID));
	QVERIFY(dui.isdsID() == cIsdsID);
	QVERIFY(isdsID != cIsdsID);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.userType() == Isds::Type::UT_NULL);
	dui.setUserType(userType);
	QVERIFY(dui.userType() == cUserType);
	QVERIFY(userType == cUserType);

	QVERIFY(dui.userPrivils() == Isds::Type::PRIVIL_NONE);
	dui.setUserPrivils(userPrivils);
	QVERIFY(dui.userPrivils() == cUserPrivils);
	QVERIFY(userPrivils == cUserPrivils);

	QVERIFY(dui.ic().isNull());
	dui.setIc(ic);
	QVERIFY(dui.ic() == cIc);
	QVERIFY(ic == cIc);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setIc("other_ic");
	QVERIFY(dui.ic() != cIc);
	dui.setIc(::std::move(ic));
	QVERIFY(dui.ic() == cIc);
	QVERIFY(ic != cIc);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.firmName().isNull());
	dui.setFirmName(firmName);
	QVERIFY(dui.firmName() == cFirmName);
	QVERIFY(firmName == cFirmName);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setFirmName("other_firmName");
	QVERIFY(dui.firmName() != cFirmName);
	dui.setFirmName(::std::move(firmName));
	QVERIFY(dui.firmName() == cFirmName);
	QVERIFY(firmName != cFirmName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.caStreet().isNull());
	dui.setCaStreet(caStreet);
	QVERIFY(dui.caStreet() == cCaStreet);
	QVERIFY(caStreet == cCaStreet);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setCaStreet("other_caStreet");
	QVERIFY(dui.caStreet() != cCaStreet);
	dui.setCaStreet(::std::move(caStreet));
	QVERIFY(dui.caStreet() == cCaStreet);
	QVERIFY(caStreet != cCaStreet);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.caCity().isNull());
	dui.setCaCity(caCity);
	QVERIFY(dui.caCity() == cCaCity);
	QVERIFY(caCity == cCaCity);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setCaCity("other_caCity");
	QVERIFY(dui.caCity() != cCaCity);
	dui.setCaCity(::std::move(caCity));
	QVERIFY(dui.caCity() == cCaCity);
	QVERIFY(caCity != cCaCity);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.caZipCode().isNull());
	dui.setCaZipCode(caZipCode);
	QVERIFY(dui.caZipCode() == cCaZipCode);
	QVERIFY(caZipCode == cCaZipCode);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setCaZipCode("other_caZipCode");
	QVERIFY(dui.caZipCode() != cCaZipCode);
	dui.setCaZipCode(::std::move(caZipCode));
	QVERIFY(dui.caZipCode() == cCaZipCode);
	QVERIFY(caZipCode != cCaZipCode);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.caState().isNull());
	dui.setCaState(caState);
	QVERIFY(dui.caState() == cCaState);
	QVERIFY(caState == cCaState);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setCaState("other_caState");
	QVERIFY(dui.caState() != cCaState);
	dui.setCaState(::std::move(caState));
	QVERIFY(dui.caState() == cCaState);
	QVERIFY(caState != cCaState);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface2::dbUserInfoExt2Conversion(void)
{
	Isds::DbUserInfoExt2 dui1, dui2;
	const enum Isds::Type::NilBool aifoIsds = Isds::Type::BOOL_TRUE;
	const Isds::PersonName2 personName(buildPersonName2());
	const Isds::AddressExt2 address(buildAddressExt2());
	const QDate biDate(2001, 2, 3);
	const QString isdsID("isdsID");
	const enum Isds::Type::UserType userType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges userPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const QString ic("ic");
	const QString firmName("firmName");
	const QString caStreet("caStreet");
	const QString caCity("caCity");
	const QString caZipCode("caZipCode");
	const QString caState("caState");

	bool iOk = false;
	struct isds_DbUserInfoExt2 *idui = NULL;

	QVERIFY(dui1.isNull());
	QVERIFY(dui2.isNull());
	QVERIFY(dui1 == dui2);

	dui1.setAifoIsds(aifoIsds);
	dui1.setPersonName(personName);
	dui1.setAddress(address);
	dui1.setBiDate(biDate);
	dui1.setIsdsID(isdsID);
	dui1.setUserType(userType);
	dui1.setUserPrivils(userPrivils);
	dui1.setIc(ic);
	dui1.setFirmName(firmName);
	dui1.setCaStreet(caStreet);
	dui1.setCaCity(caCity);
	dui1.setCaZipCode(caZipCode);
	dui1.setCaState(caState);

	QVERIFY(!dui1.isNull());
	QVERIFY(dui2.isNull());
	QVERIFY(dui1 != dui2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	dui2 = Isds::libisds2dbUserInfoExt2(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(dui2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	idui = Isds::dbUserInfoExt22libisds(dui2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(idui == NULL);

	iOk = false;
	idui = Isds::dbUserInfoExt22libisds(dui1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(idui != NULL);

	iOk = false;
	dui2 = Isds::libisds2dbUserInfoExt2(idui, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!dui2.isNull());

	QVERIFY(dui2.aifoIsds() == aifoIsds);
	QVERIFY(dui2.personName() == personName);
	QVERIFY(dui2.address() == address);
	QVERIFY(dui2.biDate() == biDate);
	QVERIFY(dui2.isdsID() == isdsID);
	QVERIFY(dui2.userType() == userType);
	QVERIFY(dui2.userPrivils() == userPrivils);
	QVERIFY(dui2.ic() == ic);
	QVERIFY(dui2.firmName() == firmName);
	QVERIFY(dui2.caStreet() == caStreet);
	QVERIFY(dui2.caCity() == caCity);
	QVERIFY(dui2.caZipCode() == caZipCode);
	QVERIFY(dui2.caState() == caState);

	QVERIFY(!dui1.isNull());
	QVERIFY(!dui2.isNull());
	QVERIFY(dui1 == dui2);

	isds_DbUserInfoExt2_free(&idui);
	QVERIFY(idui == NULL);
}

QObject *newTestIsdsBoxInterface2(void)
{
	return new (::std::nothrow) TestIsdsBoxInterface2();
}

//QTEST_MAIN(TestIsdsBoxInterface2)
#include "test_isds_box_interface2.moc"
