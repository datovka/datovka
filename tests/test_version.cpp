/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QString>
#include <QtTest/QtTest>

#include "src/datovka_shared/app_version_info.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"

class TestVersion : public QObject {
	Q_OBJECT

public:
	TestVersion(void);

private slots:
	void initTestCase(void);

	void cleanupTestCase(void);

	void checkReleaseVersionDetection(void);

	void checkGitArchiveDetection(void);

	void compareCleanLegalVersionStrings(void);

	void compareDirtyLegalVersionStrings(void);

	void compareIllegalVersionStrings(void);
};

TestVersion::TestVersion(void)
    : QObject(Q_NULLPTR)
{
}

QObject *newTestVersion(void)
{
	return new (::std::nothrow) TestVersion();
}

void TestVersion::initTestCase(void)
{
	QVERIFY(GlobInstcs::logPtr == Q_NULLPTR);
	GlobInstcs::logPtr = new (::std::nothrow) LogDevice;
	QVERIFY(GlobInstcs::logPtr != Q_NULLPTR);
}

void TestVersion::cleanupTestCase(void)
{
	delete GlobInstcs::logPtr; GlobInstcs::logPtr = Q_NULLPTR;
}

#define RELEASE_VER_STR_0 "0.0.0"
#define RELEASE_VER_STR_1 "4.24.1"

#define GIT_ARCHIVE_VER_STR_0 "0.0.0.9999.00000000.000000.0000000000000000"
#define GIT_ARCHIVE_VER_STR_1 "4.25.0.9999.20250106.151005.ee2327675e2f1a9a"

void TestVersion::checkReleaseVersionDetection(void)
{
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(""));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(" "));

	QVERIFY(true == AppVersionInfo::isReleaseVersionString(RELEASE_VER_STR_0));
	QVERIFY(true == AppVersionInfo::isReleaseVersionString(" " RELEASE_VER_STR_0));
	QVERIFY(true == AppVersionInfo::isReleaseVersionString(RELEASE_VER_STR_0 " "));
	QVERIFY(true == AppVersionInfo::isReleaseVersionString(" " RELEASE_VER_STR_0 " "));

	QVERIFY(false == AppVersionInfo::isReleaseVersionString("x" RELEASE_VER_STR_0));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(RELEASE_VER_STR_0 "x"));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString("x" RELEASE_VER_STR_0 "x"));

	QVERIFY(true == AppVersionInfo::isReleaseVersionString(RELEASE_VER_STR_1));
	QVERIFY(true == AppVersionInfo::isReleaseVersionString(" " RELEASE_VER_STR_1));
	QVERIFY(true == AppVersionInfo::isReleaseVersionString(RELEASE_VER_STR_1 " "));
	QVERIFY(true == AppVersionInfo::isReleaseVersionString(" " RELEASE_VER_STR_1 " "));

	QVERIFY(false == AppVersionInfo::isReleaseVersionString("x" RELEASE_VER_STR_1));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(RELEASE_VER_STR_1 "x"));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString("x" RELEASE_VER_STR_1 "x"));

	QVERIFY(false == AppVersionInfo::isReleaseVersionString(GIT_ARCHIVE_VER_STR_0));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(" " GIT_ARCHIVE_VER_STR_0));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(GIT_ARCHIVE_VER_STR_0 " "));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(" " GIT_ARCHIVE_VER_STR_0 " "));

	QVERIFY(false == AppVersionInfo::isReleaseVersionString(GIT_ARCHIVE_VER_STR_1));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(" " GIT_ARCHIVE_VER_STR_1));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(GIT_ARCHIVE_VER_STR_1 " "));
	QVERIFY(false == AppVersionInfo::isReleaseVersionString(" " GIT_ARCHIVE_VER_STR_1 " "));
}

void TestVersion::checkGitArchiveDetection(void)
{
	QVERIFY(false == AppVersionInfo::isGitArchiveString(""));
	QVERIFY(false == AppVersionInfo::isGitArchiveString(" "));

	QVERIFY(false == AppVersionInfo::isGitArchiveString(RELEASE_VER_STR_0));
	QVERIFY(false == AppVersionInfo::isGitArchiveString(" " RELEASE_VER_STR_0));
	QVERIFY(false == AppVersionInfo::isGitArchiveString(RELEASE_VER_STR_0 " "));
	QVERIFY(false == AppVersionInfo::isGitArchiveString(" " RELEASE_VER_STR_0 " "));

	QVERIFY(false == AppVersionInfo::isGitArchiveString(RELEASE_VER_STR_1));
	QVERIFY(false == AppVersionInfo::isGitArchiveString(" " RELEASE_VER_STR_1));
	QVERIFY(false == AppVersionInfo::isGitArchiveString(RELEASE_VER_STR_1 " "));
	QVERIFY(false == AppVersionInfo::isGitArchiveString(" " RELEASE_VER_STR_1 " "));

	QVERIFY(true == AppVersionInfo::isGitArchiveString(GIT_ARCHIVE_VER_STR_0));
	QVERIFY(true == AppVersionInfo::isGitArchiveString(" " GIT_ARCHIVE_VER_STR_0));
	QVERIFY(true == AppVersionInfo::isGitArchiveString(GIT_ARCHIVE_VER_STR_0 " "));
	QVERIFY(true == AppVersionInfo::isGitArchiveString(" " GIT_ARCHIVE_VER_STR_0 " "));

	QVERIFY(false == AppVersionInfo::isGitArchiveString("x" GIT_ARCHIVE_VER_STR_0));
	QVERIFY(false == AppVersionInfo::isGitArchiveString(GIT_ARCHIVE_VER_STR_0 "x"));
	QVERIFY(false == AppVersionInfo::isGitArchiveString("x" GIT_ARCHIVE_VER_STR_0 "x"));

	QVERIFY(true == AppVersionInfo::isGitArchiveString(GIT_ARCHIVE_VER_STR_1));
	QVERIFY(true == AppVersionInfo::isGitArchiveString(" " GIT_ARCHIVE_VER_STR_1));
	QVERIFY(true == AppVersionInfo::isGitArchiveString(GIT_ARCHIVE_VER_STR_1 " "));
	QVERIFY(true == AppVersionInfo::isGitArchiveString(" " GIT_ARCHIVE_VER_STR_1 " "));

	QVERIFY(false == AppVersionInfo::isGitArchiveString("x" GIT_ARCHIVE_VER_STR_1));
	QVERIFY(false == AppVersionInfo::isGitArchiveString(GIT_ARCHIVE_VER_STR_1 "x"));
	QVERIFY(false == AppVersionInfo::isGitArchiveString("x" GIT_ARCHIVE_VER_STR_1 "x"));
}

void TestVersion::compareCleanLegalVersionStrings(void)
{
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("0.0.0", "0.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.0", "0.0.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("0.0.1", "0.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.0", "0.1.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("0.1.0", "0.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.0", "0.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("0.1.1", "0.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.0", "1.0.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.0.0", "0.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.0", "1.0.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.0.1", "0.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.0", "1.1.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.0", "0.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.0", "1.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.1", "0.0.0"));
//
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("0.0.1", "0.0.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.1", "0.1.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("0.1.0", "0.0.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.1", "0.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("0.1.1", "0.0.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.1", "1.0.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.0.0", "0.0.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.1", "1.0.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.0.1", "0.0.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.1", "1.1.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.0", "0.0.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.0.1", "1.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.1", "0.0.1"));
//
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("0.1.0", "0.1.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.1.0", "0.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("0.1.1", "0.1.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.1.0", "1.0.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.0.0", "0.1.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.1.0", "1.0.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.0.1", "0.1.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.1.0", "1.1.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.0", "0.1.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.1.0", "1.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.1", "0.1.0"));
//
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("0.1.1", "0.1.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.1.1", "1.0.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.0.0", "0.1.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.1.1", "1.0.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.0.1", "0.1.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.1.1", "1.1.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.0", "0.1.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("0.1.1", "1.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.1", "0.1.1"));
//
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("1.0.0", "1.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("1.0.0", "1.0.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.0.1", "1.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("1.0.0", "1.1.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.0", "1.0.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("1.0.0", "1.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.1", "1.0.0"));
//
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("1.0.1", "1.0.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("1.0.1", "1.1.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.0", "1.0.1"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("1.0.1", "1.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.1", "1.0.1"));
//
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("1.1.0", "1.1.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("1.1.0", "1.1.1"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("1.1.1", "1.1.0"));
//
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("4.9.3", "4.9.3"));
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("4.10.0", "4.10.0"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("4.9.3", "4.10.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("4.10.0", "4.9.3"));
}

void TestVersion::compareDirtyLegalVersionStrings(void)
{
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("_0.0.0", "0.0.0+"));
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings(" 0.0.0", "0.0.0 "));
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings(" 0.0.0 ", " 0.0.0 "));
//
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("ballast_+" "4.9.3" " garbage", "added " "4.9.3" "-string"));
	QVERIFY( 0 == AppVersionInfo::compareVersionStrings("foo" "4.10.0" "bar", "goo " "4.10.0" "boo"));

	QVERIFY(-1 == AppVersionInfo::compareVersionStrings("_" "4.9.3", "+" "4.10.0"));
	QVERIFY( 1 == AppVersionInfo::compareVersionStrings("+" "4.10.0", "4.9.3" "_"));
}

void TestVersion::compareIllegalVersionStrings(void)
{
	QVERIFY(-2 == AppVersionInfo::compareVersionStrings("4.-9.3", "4.10.0"));
	QVERIFY(-2 == AppVersionInfo::compareVersionStrings("4.10.-0", "4.9.3"));

	QVERIFY( 2 == AppVersionInfo::compareVersionStrings("4.9.3", "4.+10.0"));
	QVERIFY( 2 == AppVersionInfo::compareVersionStrings("4.10.0", "4.9.+3"));

	QVERIFY(-2 == AppVersionInfo::compareVersionStrings("4.9", "4.10.0"));
	QVERIFY( 2 == AppVersionInfo::compareVersionStrings("4.10.0", "4.9"));

	QVERIFY( 2 == AppVersionInfo::compareVersionStrings("4.9.3", "4.10"));
	QVERIFY(-2 == AppVersionInfo::compareVersionStrings("4.10", "4.9.3"));
}

//QTEST_MAIN(TestVersion)
#include "test_version.moc"
