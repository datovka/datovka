
DEFINES += \
	TEST_ISDS_GENERIC_INTERFACE=1

INCLUDEPATH += \
	/usr/include/libxml2

LIBS += \
	-ldatovka \
	-lcrypto

SOURCES += \
	$${top_srcdir}src/datovka_shared/isds/generic_interface.cpp \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.cpp \
	$${top_srcdir}src/isds/generic_conversion.cpp \
	$${top_srcdir}tests/test_isds_generic_interface.cpp

HEADERS += \
	$${top_srcdir}src/datovka_shared/isds/generic_interface.h \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.h \
	$${top_srcdir}src/isds/generic_conversion.h \
	$${top_srcdir}tests/test_isds_generic_interface.h
