/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QCoreApplication>
#include <QDate>
#include <QDateTime>
#include <QDir>
#include <QFileInfo>
#include <QSet>
#include <QString>
#include <QStringList>
#include <QtTest/QtTest>
#include <QVariant>

#include "src/datovka_shared/io/prefs_db.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/prefs.h"
#include "src/global.h"
#include "tests/test_prefs_db.h"

class TestPrefsDb : public QObject {
	Q_OBJECT

public:
	TestPrefsDb(void);

private slots:
	void initTestCase(void);

	void cleanupTestCase(void);

	void init(void);

	void cleanup(void);

	void accessDbData(void);

	void noDefaultDataNoDb(void);

	void noDefaultDataEmptyDb(void);

	void noDefaultDataNonEmptyDb(void);

	void defaultDataNoDb(void);

	void defaultDataEmptyDbDbFirst(void);

	void defaultDataEmptyDbDefaultFirts(void);

	void defaultDataNonEmptyDbDbFirst(void);

	void defaultDataNonEmptyDbDefaultFirst(void);

private:
	/*
	 * An application object must exist before any QObject.
	 * See https://doc.qt.io/qt-6/threads-qobject.html ,
	 * also see QTEST_GUILESS_MAIN().
	 */
	int m_argc;
	char **m_argv;
	QCoreApplication m_app;

	const QString m_connectionPrefix; /*!< SQL connection prefix. */
	const QString m_dbLocationPath; /*!< Directory path. */
	QDir m_dbDir;  /*!< Directory containing testing databases. */
	const QString m_dbName; /*!< Database file name. */
	const QString m_dbPath;

	const QDateTime m_todayDt, m_tomorrowDt;
	const QDate m_todayD, m_tomorrowD;
};

TestPrefsDb::TestPrefsDb(void)
    : m_argc(0), m_argv(NULL), m_app(m_argc, m_argv),
    m_connectionPrefix(QLatin1String("PREFSDB")),
    m_dbLocationPath(QDir::currentPath() + QDir::separator() + QLatin1String("_db")),
    m_dbDir(m_dbLocationPath),
    m_dbName(QLatin1String("prefs.db")),
    m_dbPath(m_dbLocationPath + QDir::separator() + m_dbName),
    m_todayDt(QDateTime::currentDateTime()),
    m_tomorrowDt(QDateTime::currentDateTime().addDays(1)),
    m_todayD(QDate::currentDate()),
    m_tomorrowD(QDate::currentDate().addDays(1))
{
}

void TestPrefsDb::initTestCase(void)
{
	QVERIFY(GlobInstcs::logPtr == Q_NULLPTR);
	GlobInstcs::logPtr = new (::std::nothrow) LogDevice;
	QVERIFY(GlobInstcs::logPtr != Q_NULLPTR);

	/* Pointer must be null before initialisation. */
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);

	QVERIFY(m_dbDir.removeRecursively());
	QVERIFY(!m_dbDir.exists());

	QVERIFY(m_dbDir.mkpath("."));
}

void TestPrefsDb::cleanupTestCase(void)
{
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);

	delete GlobInstcs::logPtr; GlobInstcs::logPtr = Q_NULLPTR;

	QVERIFY(m_dbDir.removeRecursively());
	QVERIFY(!m_dbDir.exists());
}

void TestPrefsDb::init(void)
{
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);

	QFile(m_dbPath).remove();

	QVERIFY(!QFileInfo::exists(m_dbPath));

	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
}

void TestPrefsDb::cleanup(void)
{
	delete GlobInstcs::prefsDbPtr; GlobInstcs::prefsDbPtr = Q_NULLPTR;

	delete GlobInstcs::prefsPtr; GlobInstcs::prefsPtr = Q_NULLPTR;
}

void TestPrefsDb::accessDbData(void)
{
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);
	GlobInstcs::prefsDbPtr = new (::std::nothrow) PrefsDb(m_connectionPrefix, false);
	QVERIFY(GlobInstcs::prefsDbPtr != Q_NULLPTR);
	QVERIFY(GlobInstcs::prefsDbPtr->openDb(m_dbPath, SQLiteDb::CREATE_MISSING));
	QVERIFY(QFileInfo::exists(m_dbPath));

	QString name;

	QVERIFY(GlobInstcs::prefsDbPtr->names().isEmpty());

	{
		/* Null name. */
		name.clear();
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setBoolVal(name, true));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setIntVal(name, 1));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setFloatVal(name, 1.0));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setStrVal(name, "str1"));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setColourVal(name, "000001"));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setDateTimeVal(name, m_todayDt));
		{
			QDateTime val = m_tomorrowDt;
			QDateTime val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setDateVal(name, m_todayD));
		{
			QDate val = m_tomorrowD;
			QDate val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == val2);
		}
	}

	QVERIFY(GlobInstcs::prefsDbPtr->names().isEmpty());

	{
		/* Empty name. */
		name = "";
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setBoolVal(name, true));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setIntVal(name, 1));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setFloatVal(name, 1.0));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setStrVal(name, "str1"));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setColourVal(name, "000001"));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setDateTimeVal(name, m_todayDt));
		{
			QDateTime val = m_tomorrowDt;
			QDateTime val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->setDateVal(name, m_todayD));
		{
			QDate val = m_tomorrowD;
			QDate val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == val2);
		}
	}

	QVERIFY(GlobInstcs::prefsDbPtr->names().isEmpty());

	{
		name = "value.bool";
		/* Non-existent. */
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		/* Set and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setBoolVal(name, false));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = true;
			QVERIFY(GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = true;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == true);
		}
		/* Set, set again and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setBoolVal(name, false));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = true;
			QVERIFY(GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->setBoolVal(name, true));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == true);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		/* Set, no other type must exist under same name, original value must remain. */
		QVERIFY(GlobInstcs::prefsDbPtr->setBoolVal(name, true));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = m_tomorrowDt;
			QDateTime val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = m_tomorrowD;
			QDate val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == true);
		}
	}

	{
		QStringList nameList(GlobInstcs::prefsDbPtr->names());
		QVERIFY(nameList.size() == 1);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
		QVERIFY(QSet<QString>(nameList.begin(), nameList.end()) ==
		    (QSet<QString>() << "value.bool"));
#else /* < Qt-5.14.0 */
		QVERIFY(nameList.toSet() ==
		    (QSet<QString>() << "value.bool"));
#endif /* >= Qt-5.14.0 */
	}

	{
		name = "value.int";
		/* Non-existent. */
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		/* Set and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setIntVal(name, 0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 1;
			QVERIFY(GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 1;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 1);
		}
		/* Set, set again and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setIntVal(name, 0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 1;
			QVERIFY(GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->setIntVal(name, 1));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 1);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		/* Set, no other type must exist under same name, original value must remain. */
		QVERIFY(GlobInstcs::prefsDbPtr->setIntVal(name, 1));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = m_tomorrowDt;
			QDateTime val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = m_tomorrowD;
			QDate val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 1);
		}
	}

	{
		QStringList nameList(GlobInstcs::prefsDbPtr->names());
		QVERIFY(nameList.size() == 2);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
		QVERIFY(QSet<QString>(nameList.begin(), nameList.end()) ==
		    (QSet<QString>() << "value.bool" << "value.int"));
#else /* < Qt-5.14.0 */
		QVERIFY(nameList.toSet() ==
		    (QSet<QString>() << "value.bool" << "value.int"));
#endif /* >= Qt-5.14.0 */
	}

	{
		name = "value.float";
		/* Non-existent. */
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		/* Set and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setFloatVal(name, 0.0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 1.0;
			QVERIFY(GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 1.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 1.0);
		}
		/* Set, set again and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setFloatVal(name, 0.0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 1.0;
			QVERIFY(GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->setFloatVal(name, 1.0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 1.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		/* Set, no other type must exist under same name, original value must remain. */
		QVERIFY(GlobInstcs::prefsDbPtr->setFloatVal(name, 1.0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = m_tomorrowDt;
			QDateTime val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = m_tomorrowD;
			QDate val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 1.0);
		}
	}

	{
		QStringList nameList(GlobInstcs::prefsDbPtr->names());
		QVERIFY(nameList.size() == 3);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
		QVERIFY(QSet<QString>(nameList.begin(), nameList.end()) ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float"));
#else /* < Qt-5.14.0 */
		QVERIFY(nameList.toSet() ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float"));
#endif /* >= Qt-5.14.0 */
	}

	{
		name = "value.str";
		/* Non-existent. */
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		/* Set null string and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setStrVal(name, QString()));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val.isNull());
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		/* Set empty string and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setStrVal(name, ""));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(!val.isNull());
			QVERIFY(val.isEmpty());
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		/* Set and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setStrVal(name, "str0"));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str1";
			QVERIFY(GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str1";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str1");
		}
		/* Set, set again and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setStrVal(name, "str0"));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str1";
			QVERIFY(GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->setStrVal(name, "str1"));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str1");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		/* Set, no other type must exist under same name, original value must remain. */
		QVERIFY(GlobInstcs::prefsDbPtr->setStrVal(name, "str1"));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = m_tomorrowDt;
			QDateTime val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = m_tomorrowD;
			QDate val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str1");
		}
	}

	{
		QStringList nameList(GlobInstcs::prefsDbPtr->names());
		QVERIFY(nameList.size() == 4);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
		QVERIFY(QSet<QString>(nameList.begin(), nameList.end()) ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float" << "value.str"));
#else /* < Qt-5.14.0 */
		QVERIFY(nameList.toSet() ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float" << "value.str"));
#endif /* >= Qt-5.14.0 */
	}

	{
		name = "value.colour";
		/* Non-existent. */
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		/* Set null string and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setColourVal(name, QString()));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val.isNull());
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		/* Set empty string. */
		QVERIFY(!GlobInstcs::prefsDbPtr->setColourVal(name, ""));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		/* Set and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setColourVal(name, "000000"));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000001";
			QVERIFY(GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000001";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000001");
		}
		/* Set, set again and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setColourVal(name, "000000"));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000001";
			QVERIFY(GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->setColourVal(name, "000001"));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000001");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		/* Set, no other type must exist under same name, original value must remain. */
		QVERIFY(GlobInstcs::prefsDbPtr->setColourVal(name, "000001"));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = m_tomorrowDt;
			QDateTime val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = m_tomorrowD;
			QDate val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000001");
		}
	}

	{
		QStringList nameList(GlobInstcs::prefsDbPtr->names());
		QVERIFY(nameList.size() == 5);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
		QVERIFY(QSet<QString>(nameList.begin(), nameList.end()) ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float" << "value.str" << "value.colour"));
#else /* < Qt-5.14.0 */
		QVERIFY(nameList.toSet() ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float" << "value.str" << "value.colour"));
#endif /* >= Qt-5.14.0 */
	}

	{
		name = "value.datetime";

		const QDateTime dt0 = m_todayDt;
		const QDateTime dt1 = m_tomorrowDt;
		/* Non-existent. */
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = dt0;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == dt0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		/* Set invalid and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setDateTimeVal(name, QDateTime()));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = dt0;
			QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val.isNull());
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = dt0;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == dt0);
		}
		/* Set and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setDateTimeVal(name, dt0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = dt1;
			QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == dt0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = dt1;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == dt1);
		}
		/* Set, set again and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setDateTimeVal(name, dt0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = dt1;
			QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == dt0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->setDateTimeVal(name, dt1));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = dt0;
			QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == dt1);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = dt0;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == dt0);
		}
		/* Set, no other type must exist under same name, original value must remain. */
		QVERIFY(GlobInstcs::prefsDbPtr->setDateTimeVal(name, dt1));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = m_tomorrowD;
			QDate val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = dt0;
			QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == dt1);
		}
	}

	{
		QStringList nameList(GlobInstcs::prefsDbPtr->names());
		QVERIFY(nameList.size() == 6);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
		QVERIFY(QSet<QString>(nameList.begin(), nameList.end()) ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float" << "value.str" << "value.colour"
		        << "value.datetime"));
#else /* < Qt-5.14.0 */
		QVERIFY(nameList.toSet() ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float" << "value.str" << "value.colour"
		        << "value.datetime"));
#endif /* >= Qt-5.14.0 */
	}

	{
		name = "value.date";

		const QDate d0 = m_todayD;
		const QDate d1 = m_tomorrowD;
		/* Non-existent. */
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = d0;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == d0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		/* Set invalid  and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setDateVal(name, QDate()));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = d0;
			QVERIFY(GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val.isNull());
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = d0;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == d0);
		}
		/* Set and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setDateVal(name, d0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = d1;
			QVERIFY(GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == d0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = d1;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == d1);
		}
		/* Set, set again and erase. */
		QVERIFY(GlobInstcs::prefsDbPtr->setDateVal(name, d0));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = d1;
			QVERIFY(GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == d0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->setDateVal(name, d1));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = d0;
			QVERIFY(GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == d1);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->erase(name));
		QVERIFY(!GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = d0;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == d0);
		}
		/* Set, no other type must exist under same name, original value must remain. */
		QVERIFY(GlobInstcs::prefsDbPtr->setDateVal(name, d1));
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			bool val = false;
			QVERIFY(!GlobInstcs::prefsDbPtr->boolVal(name, val));
			QVERIFY(val == false);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			qint64 val = 0;
			QVERIFY(!GlobInstcs::prefsDbPtr->intVal(name, val));
			QVERIFY(val == 0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			double val = 0.0;
			QVERIFY(!GlobInstcs::prefsDbPtr->floatVal(name, val));
			QVERIFY(val == 0.0);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "str0";
			QVERIFY(!GlobInstcs::prefsDbPtr->strVal(name, val));
			QVERIFY(val == "str0");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QString val = "000000";
			QVERIFY(!GlobInstcs::prefsDbPtr->colourVal(name, val));
			QVERIFY(val == "000000");
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDateTime val = m_tomorrowDt;
			QDateTime val2 = val;
			QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal(name, val));
			QVERIFY(val == val2);
		}
		QVERIFY(GlobInstcs::prefsDbPtr->contains(name));
		{
			QDate val = d0;
			QVERIFY(GlobInstcs::prefsDbPtr->dateVal(name, val));
			QVERIFY(val == d1);
		}
	}

	{
		QStringList nameList(GlobInstcs::prefsDbPtr->names());
		QVERIFY(nameList.size() == 7);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
		QVERIFY(QSet<QString>(nameList.begin(), nameList.end()) ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float" << "value.str" << "value.colour"
		        << "value.datetime" << "value.date"));
#else /* < Qt-5.14.0 */
		QVERIFY(nameList.toSet() ==
		    (QSet<QString>() << "value.bool" << "value.int"
		        << "value.float" << "value.str" << "value.colour"
		        << "value.datetime" << "value.date"));
#endif /* >= Qt-5.14.0 */
	}

	{
		enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
		QVariant val;

		type = PrefsDb::VAL_INTEGER;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.bool", type));
		QVERIFY(type == PrefsDb::VAL_BOOLEAN);
		type = PrefsDb::VAL_INTEGER;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.bool", type, &val));
		QVERIFY(type == PrefsDb::VAL_BOOLEAN);
		QVERIFY(!val.isNull());
		QVERIFY(val.canConvert<bool>());
		QVERIFY(val == QVariant(true));

		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.int", type));
		QVERIFY(type == PrefsDb::VAL_INTEGER);
		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.int", type, &val));
		QVERIFY(type == PrefsDb::VAL_INTEGER);
		QVERIFY(!val.isNull());
		QVERIFY(val.canConvert<qint64>());
		QVERIFY(val == QVariant(1));

		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.float", type));
		QVERIFY(type == PrefsDb::VAL_FLOAT);
		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.float", type, &val));
		QVERIFY(type == PrefsDb::VAL_FLOAT);
		QVERIFY(!val.isNull());
		QVERIFY(val.canConvert<double>());
		QVERIFY(val == QVariant(1.0));

		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.str", type));
		QVERIFY(type == PrefsDb::VAL_STRING);
		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.str", type, &val));
		QVERIFY(type == PrefsDb::VAL_STRING);
		QVERIFY(!val.isNull());
		QVERIFY(val.canConvert<QString>());
		QVERIFY(val == QVariant("str1"));

		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.colour", type));
		QVERIFY(type == PrefsDb::VAL_COLOUR);
		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.colour", type, &val));
		QVERIFY(type == PrefsDb::VAL_COLOUR);
		QVERIFY(!val.isNull());
		QVERIFY(val.canConvert<QString>());
		QVERIFY(val == QVariant("000001"));

		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.datetime", type));
		QVERIFY(type == PrefsDb::VAL_DATETIME);
		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.datetime", type, &val));
		QVERIFY(type == PrefsDb::VAL_DATETIME);
		QVERIFY(!val.isNull());
		QVERIFY(val.canConvert<QDateTime>());
		QVERIFY(val == QVariant(m_tomorrowDt));

		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.date", type));
		QVERIFY(type == PrefsDb::VAL_DATE);
		type = PrefsDb::VAL_BOOLEAN;
		QVERIFY(GlobInstcs::prefsDbPtr->type("value.date", type, &val));
		QVERIFY(type == PrefsDb::VAL_DATE);
		QVERIFY(!val.isNull());
		QVERIFY(val.canConvert<QDate>());
		QVERIFY(val == QVariant(m_tomorrowD));
	}
}

void TestPrefsDb::noDefaultDataNoDb(void)
{
	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
	GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
	QVERIFY(GlobInstcs::prefsPtr != Q_NULLPTR);

	/* No preference data present. */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	{
		QVERIFY(!GlobInstcs::prefsPtr->setBoolVal("value.bool", true));
		QVERIFY(!GlobInstcs::prefsPtr->setIntVal("value.int", 1));
		QVERIFY(!GlobInstcs::prefsPtr->setFloatVal("value.float", 1.0));
		QVERIFY(!GlobInstcs::prefsPtr->setStrVal("value.str", "str1"));
		QVERIFY(!GlobInstcs::prefsPtr->setColourVal("value.colour", "000001"));
		QVERIFY(!GlobInstcs::prefsPtr->setDateTimeVal("value.datetime", m_tomorrowDt));
		QVERIFY(!GlobInstcs::prefsPtr->setDateVal("value.date", m_tomorrowD));
	}

	/* No preference data present. */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}
}

void TestPrefsDb::noDefaultDataEmptyDb(void)
{
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);
	GlobInstcs::prefsDbPtr = new (::std::nothrow) PrefsDb(m_connectionPrefix, false);
	QVERIFY(GlobInstcs::prefsDbPtr != Q_NULLPTR);
	QVERIFY(GlobInstcs::prefsDbPtr->openDb(m_dbPath, SQLiteDb::CREATE_MISSING));
	QVERIFY(QFileInfo::exists(m_dbPath));

	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
	GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
	QVERIFY(GlobInstcs::prefsPtr != Q_NULLPTR);

	/* No preference data present. */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* No default set, empty database loaded. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(GlobInstcs::prefsDbPtr));
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Set data. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setBoolVal("value.bool", true));
		QVERIFY(GlobInstcs::prefsPtr->setIntVal("value.int", 1));
		QVERIFY(GlobInstcs::prefsPtr->setFloatVal("value.float", 1.0));
		QVERIFY(GlobInstcs::prefsPtr->setStrVal("value.str", "str1"));
		QVERIFY(GlobInstcs::prefsPtr->setColourVal("value.colour", "000001"));
		QVERIFY(GlobInstcs::prefsPtr->setDateTimeVal("value.datetime", m_tomorrowDt));
		QVERIFY(GlobInstcs::prefsPtr->setDateVal("value.date", m_tomorrowD));
	}

	/* Data must be accessible. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Detach database. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(Q_NULLPTR));
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Data in database must remain intact. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}
}

void TestPrefsDb::noDefaultDataNonEmptyDb(void)
{
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);
	GlobInstcs::prefsDbPtr = new (::std::nothrow) PrefsDb(m_connectionPrefix, false);
	QVERIFY(GlobInstcs::prefsDbPtr != Q_NULLPTR);
	QVERIFY(GlobInstcs::prefsDbPtr->openDb(m_dbPath, SQLiteDb::CREATE_MISSING));
	QVERIFY(QFileInfo::exists(m_dbPath));

	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
	GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
	QVERIFY(GlobInstcs::prefsPtr != Q_NULLPTR);

	/* Set database data. */
	QVERIFY(GlobInstcs::prefsDbPtr->setBoolVal("value.bool", true));
	QVERIFY(GlobInstcs::prefsDbPtr->setIntVal("value.int", 1));
	QVERIFY(GlobInstcs::prefsDbPtr->setFloatVal("value.float", 1.0));
	QVERIFY(GlobInstcs::prefsDbPtr->setStrVal("value.str", "str1"));
	QVERIFY(GlobInstcs::prefsDbPtr->setColourVal("value.colour", "000001"));
	QVERIFY(GlobInstcs::prefsDbPtr->setDateTimeVal("value.datetime", m_tomorrowDt));
	QVERIFY(GlobInstcs::prefsDbPtr->setDateVal("value.date", m_tomorrowD));

	/* No preference data present. */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* No default set but database loaded. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(GlobInstcs::prefsDbPtr));
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Detach database. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(Q_NULLPTR));
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Data in database must remain intact. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}
}

void TestPrefsDb::defaultDataNoDb(void)
{
	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
	GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
	QVERIFY(GlobInstcs::prefsPtr != Q_NULLPTR);

	/* No preference data present. */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Set defaults. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.bool", PrefsDb::VAL_BOOLEAN, false)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.int", PrefsDb::VAL_INTEGER, 0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.float", PrefsDb::VAL_FLOAT, 0.0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.str", PrefsDb::VAL_STRING, "str0")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.colour", PrefsDb::VAL_COLOUR, "000000")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.datetime", PrefsDb::VAL_DATETIME, m_todayDt)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.date", PrefsDb::VAL_DATE, m_todayD)));
	}

	/* Defaults set. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Changing values must fail because of missing database. */
	{
		QVERIFY(!GlobInstcs::prefsPtr->setBoolVal("value.bool", true));
		QVERIFY(!GlobInstcs::prefsPtr->setIntVal("value.int", 1));
		QVERIFY(!GlobInstcs::prefsPtr->setFloatVal("value.float", 1.0));
		QVERIFY(!GlobInstcs::prefsPtr->setStrVal("value.str", "str1"));
		QVERIFY(!GlobInstcs::prefsPtr->setColourVal("value.colour", "000001"));
		QVERIFY(!GlobInstcs::prefsPtr->setDateTimeVal("value.datetime", m_tomorrowDt));
		QVERIFY(!GlobInstcs::prefsPtr->setDateVal("value.date", m_tomorrowD));
	}

	/* Default values must be unchanged. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Change defaults. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.bool", PrefsDb::VAL_BOOLEAN, true)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.int", PrefsDb::VAL_INTEGER, 1)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.float", PrefsDb::VAL_FLOAT, 1.0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.str", PrefsDb::VAL_STRING, "str1")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.colour", PrefsDb::VAL_COLOUR, "000001")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.datetime", PrefsDb::VAL_DATETIME, m_tomorrowDt)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.date", PrefsDb::VAL_DATE, m_tomorrowD)));
	}

	/* Default values must be changed. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}
}

void TestPrefsDb::defaultDataEmptyDbDbFirst(void)
{
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);
	GlobInstcs::prefsDbPtr = new (::std::nothrow) PrefsDb(m_connectionPrefix, false);
	QVERIFY(GlobInstcs::prefsDbPtr != Q_NULLPTR);
	QVERIFY(GlobInstcs::prefsDbPtr->openDb(m_dbPath, SQLiteDb::CREATE_MISSING));
	QVERIFY(QFileInfo::exists(m_dbPath));

	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
	GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
	QVERIFY(GlobInstcs::prefsPtr != Q_NULLPTR);

	/* No preference data present. */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* No default set, empty database loaded. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(GlobInstcs::prefsDbPtr));
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Set defaults. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.bool", PrefsDb::VAL_BOOLEAN, false)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.int", PrefsDb::VAL_INTEGER, 0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.float", PrefsDb::VAL_FLOAT, 0.0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.str", PrefsDb::VAL_STRING, "str0")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.colour", PrefsDb::VAL_COLOUR, "000000")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.datetime", PrefsDb::VAL_DATETIME, m_todayDt)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.date", PrefsDb::VAL_DATE, m_todayD)));
	}

	/* Defaults set. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Changing values. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setBoolVal("value.bool", true));
		QVERIFY(GlobInstcs::prefsPtr->setIntVal("value.int", 1));
		QVERIFY(GlobInstcs::prefsPtr->setFloatVal("value.float", 1.0));
		QVERIFY(GlobInstcs::prefsPtr->setStrVal("value.str", "str1"));
		QVERIFY(GlobInstcs::prefsPtr->setColourVal("value.colour", "000001"));
		QVERIFY(GlobInstcs::prefsPtr->setDateTimeVal("value.datetime", m_tomorrowDt));
		QVERIFY(GlobInstcs::prefsPtr->setDateVal("value.date", m_tomorrowD));
	}

	/* Check changed values via container. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Check database content. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Change defaults to database content. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.bool", PrefsDb::VAL_BOOLEAN, true)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.int", PrefsDb::VAL_INTEGER, 1)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.float", PrefsDb::VAL_FLOAT, 1.0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.str", PrefsDb::VAL_STRING, "str1")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.colour", PrefsDb::VAL_COLOUR, "000001")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.datetime", PrefsDb::VAL_DATETIME, m_tomorrowDt)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.date", PrefsDb::VAL_DATE, m_tomorrowD)));
	}

	/* Default values must be changed. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/*
	 * Database must be empty because defaults were changed to match
	 * database content.
	 */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Changing values. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setBoolVal("value.bool", false));
		QVERIFY(GlobInstcs::prefsPtr->setIntVal("value.int", 0));
		QVERIFY(GlobInstcs::prefsPtr->setFloatVal("value.float", 0.0));
		QVERIFY(GlobInstcs::prefsPtr->setStrVal("value.str", "str0"));
		QVERIFY(GlobInstcs::prefsPtr->setColourVal("value.colour", "000000"));
		QVERIFY(GlobInstcs::prefsPtr->setDateTimeVal("value.datetime", m_todayDt));
		QVERIFY(GlobInstcs::prefsPtr->setDateVal("value.date", m_todayD));
	}

	/* Values changed. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Database must contain values. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Detach database. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(Q_NULLPTR));
	/* Must read default data. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Data in database must remain intact. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}
}

void TestPrefsDb::defaultDataEmptyDbDefaultFirts(void)
{
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);
	GlobInstcs::prefsDbPtr = new (::std::nothrow) PrefsDb(m_connectionPrefix, false);
	QVERIFY(GlobInstcs::prefsDbPtr != Q_NULLPTR);
	QVERIFY(GlobInstcs::prefsDbPtr->openDb(m_dbPath, SQLiteDb::CREATE_MISSING));
	QVERIFY(QFileInfo::exists(m_dbPath));

	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
	GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
	QVERIFY(GlobInstcs::prefsPtr != Q_NULLPTR);

	/* No preference data present. */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Set defaults. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.bool", PrefsDb::VAL_BOOLEAN, false)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.int", PrefsDb::VAL_INTEGER, 0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.float", PrefsDb::VAL_FLOAT, 0.0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.str", PrefsDb::VAL_STRING, "str0")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.colour", PrefsDb::VAL_COLOUR, "000000")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.datetime", PrefsDb::VAL_DATETIME, m_todayDt)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.date", PrefsDb::VAL_DATE, m_todayD)));
	}

	/* Defaults set. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Default set, empty database loaded. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(GlobInstcs::prefsDbPtr));
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Changing values. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setBoolVal("value.bool", true));
		QVERIFY(GlobInstcs::prefsPtr->setIntVal("value.int", 1));
		QVERIFY(GlobInstcs::prefsPtr->setFloatVal("value.float", 1.0));
		QVERIFY(GlobInstcs::prefsPtr->setStrVal("value.str", "str1"));
		QVERIFY(GlobInstcs::prefsPtr->setColourVal("value.colour", "000001"));
		QVERIFY(GlobInstcs::prefsPtr->setDateTimeVal("value.datetime", m_tomorrowDt));
		QVERIFY(GlobInstcs::prefsPtr->setDateVal("value.date", m_tomorrowD));
	}

	/* Check changed values via container. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Check database content. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Change defaults to database content. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.bool", PrefsDb::VAL_BOOLEAN, true)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.int", PrefsDb::VAL_INTEGER, 1)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.float", PrefsDb::VAL_FLOAT, 1.0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.str", PrefsDb::VAL_STRING, "str1")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.colour", PrefsDb::VAL_COLOUR, "000001")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.datetime", PrefsDb::VAL_DATETIME, m_tomorrowDt)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.date", PrefsDb::VAL_DATE, m_tomorrowD)));
	}

	/* Default values must be changed. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/*
	 * Database must be empty because defaults were changed to match
	 * database content.
	 */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Changing values. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setBoolVal("value.bool", false));
		QVERIFY(GlobInstcs::prefsPtr->setIntVal("value.int", 0));
		QVERIFY(GlobInstcs::prefsPtr->setFloatVal("value.float", 0.0));
		QVERIFY(GlobInstcs::prefsPtr->setStrVal("value.str", "str0"));
		QVERIFY(GlobInstcs::prefsPtr->setColourVal("value.colour", "000000"));
		QVERIFY(GlobInstcs::prefsPtr->setDateTimeVal("value.datetime", m_todayDt));
		QVERIFY(GlobInstcs::prefsPtr->setDateVal("value.date", m_todayD));
	}

	/* Values changed. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Database must contain values. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}

	/* Detach database. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(Q_NULLPTR));
	/* Must read default data. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Data in database must remain intact. */
	{
		bool valBool = true;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 1;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 1.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str1";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000001";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_tomorrowDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_tomorrowD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}
}

void TestPrefsDb::defaultDataNonEmptyDbDbFirst(void)
{
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);
	GlobInstcs::prefsDbPtr = new (::std::nothrow) PrefsDb(m_connectionPrefix, false);
	QVERIFY(GlobInstcs::prefsDbPtr != Q_NULLPTR);
	QVERIFY(GlobInstcs::prefsDbPtr->openDb(m_dbPath, SQLiteDb::CREATE_MISSING));
	QVERIFY(QFileInfo::exists(m_dbPath));

	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
	GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
	QVERIFY(GlobInstcs::prefsPtr != Q_NULLPTR);

	/* Populate database. */
	{
		QVERIFY(GlobInstcs::prefsDbPtr->setBoolVal("value.bool", true));
		QVERIFY(GlobInstcs::prefsDbPtr->setIntVal("value.int", 1));
		QVERIFY(GlobInstcs::prefsDbPtr->setFloatVal("value.float", 1.0));
		QVERIFY(GlobInstcs::prefsDbPtr->setStrVal("value.str", "str1"));
		QVERIFY(GlobInstcs::prefsDbPtr->setColourVal("value.colour", "000001"));
		QVERIFY(GlobInstcs::prefsDbPtr->setDateTimeVal("value.datetime", m_tomorrowDt));
		QVERIFY(GlobInstcs::prefsDbPtr->setDateVal("value.date", m_tomorrowD));
	}

	/* Defaults not set, database loaded. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(GlobInstcs::prefsDbPtr));
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Check database. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Set defaults. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.bool", PrefsDb::VAL_BOOLEAN, false)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.int", PrefsDb::VAL_INTEGER, 0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.float", PrefsDb::VAL_FLOAT, 0.0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.str", PrefsDb::VAL_STRING, "str0")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.colour", PrefsDb::VAL_COLOUR, "000000")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.datetime", PrefsDb::VAL_DATETIME, m_todayDt)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.date", PrefsDb::VAL_DATE, m_todayD)));
	}

	/* Check values. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Check database. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Set defaults to contain database content. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.bool", PrefsDb::VAL_BOOLEAN, true)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.int", PrefsDb::VAL_INTEGER, 1)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.float", PrefsDb::VAL_FLOAT, 1.0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.str", PrefsDb::VAL_STRING, "str1")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.colour", PrefsDb::VAL_COLOUR, "000001")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.datetime", PrefsDb::VAL_DATETIME, m_tomorrowDt)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.date", PrefsDb::VAL_DATE, m_tomorrowD)));
	}

	/* Check values. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Check database - must be empty. */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}
}

void TestPrefsDb::defaultDataNonEmptyDbDefaultFirst(void)
{
	QVERIFY(GlobInstcs::prefsDbPtr == Q_NULLPTR);
	GlobInstcs::prefsDbPtr = new (::std::nothrow) PrefsDb(m_connectionPrefix, false);
	QVERIFY(GlobInstcs::prefsDbPtr != Q_NULLPTR);
	QVERIFY(GlobInstcs::prefsDbPtr->openDb(m_dbPath, SQLiteDb::CREATE_MISSING));
	QVERIFY(QFileInfo::exists(m_dbPath));

	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
	GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
	QVERIFY(GlobInstcs::prefsPtr != Q_NULLPTR);

	/* Populate database. */
	{
		QVERIFY(GlobInstcs::prefsDbPtr->setBoolVal("value.bool", true));
		QVERIFY(GlobInstcs::prefsDbPtr->setIntVal("value.int", 1));
		QVERIFY(GlobInstcs::prefsDbPtr->setFloatVal("value.float", 1.0));
		QVERIFY(GlobInstcs::prefsDbPtr->setStrVal("value.str", "str1"));
		QVERIFY(GlobInstcs::prefsDbPtr->setColourVal("value.colour", "000001"));
		QVERIFY(GlobInstcs::prefsDbPtr->setDateTimeVal("value.datetime", m_tomorrowDt));
		QVERIFY(GlobInstcs::prefsDbPtr->setDateVal("value.date", m_tomorrowD));
	}

	/* Set defaults to contain database content. */
	{
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.bool", PrefsDb::VAL_BOOLEAN, true)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.int", PrefsDb::VAL_INTEGER, 1)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.float", PrefsDb::VAL_FLOAT, 1.0)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.str", PrefsDb::VAL_STRING, "str1")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.colour", PrefsDb::VAL_COLOUR, "000001")));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.datetime", PrefsDb::VAL_DATETIME, m_tomorrowDt)));
		QVERIFY(GlobInstcs::prefsPtr->setDefault(
		    Prefs::Entry("value.date", PrefsDb::VAL_DATE, m_tomorrowD)));
	}

	/* Check values. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Check database. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Set database. */
	QVERIFY(GlobInstcs::prefsPtr->setDatabase(GlobInstcs::prefsDbPtr));

	/* Check values. */
	{
		bool valBool = false;
		QVERIFY(GlobInstcs::prefsPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == true);
		qint64 valInt = 0;
		QVERIFY(GlobInstcs::prefsPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 1);
		double valFloat = 0.0;
		QVERIFY(GlobInstcs::prefsPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 1.0);
		QString valStr = "str0";
		QVERIFY(GlobInstcs::prefsPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str1");
		QString valColour = "000000";
		QVERIFY(GlobInstcs::prefsPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000001");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(GlobInstcs::prefsPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_tomorrowDt);
		QDate valDate = m_todayD;
		QVERIFY(GlobInstcs::prefsPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_tomorrowD);
	}

	/* Check database - must be empty. */
	{
		bool valBool = false;
		QVERIFY(!GlobInstcs::prefsDbPtr->boolVal("value.bool", valBool));
		QVERIFY(valBool == false);
		qint64 valInt = 0;
		QVERIFY(!GlobInstcs::prefsDbPtr->intVal("value.int", valInt));
		QVERIFY(valInt == 0);
		double valFloat = 0.0;
		QVERIFY(!GlobInstcs::prefsDbPtr->floatVal("value.float", valFloat));
		QVERIFY(valFloat == 0.0);
		QString valStr = "str0";
		QVERIFY(!GlobInstcs::prefsDbPtr->strVal("value.str", valStr));
		QVERIFY(valStr == "str0");
		QString valColour = "000000";
		QVERIFY(!GlobInstcs::prefsDbPtr->colourVal("value.colour", valColour));
		QVERIFY(valColour == "000000");
		QDateTime valDateTime = m_todayDt;
		QVERIFY(!GlobInstcs::prefsDbPtr->dateTimeVal("value.datetime", valDateTime));
		QVERIFY(valDateTime == m_todayDt);
		QDate valDate = m_todayD;
		QVERIFY(!GlobInstcs::prefsDbPtr->dateVal("value.date", valDate));
		QVERIFY(valDate == m_todayD);
	}
}

QObject *newTestPrefsDb(void)
{
	return new (::std::nothrow) TestPrefsDb();
}

//QTEST_MAIN(TestPrefsDb)
#include "test_prefs_db.moc"
