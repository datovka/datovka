/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QCoreApplication>
#include <QDir>
#include <QtTest/QtTest>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/prefs.h"
#include "src/global.h"
#include "src/io/message_db_set_container.h"
#include "tests/test_db_container.h"

class TestDbContainer : public QObject {
	Q_OBJECT

public:
	TestDbContainer(void);

private slots:
	void initTestCase(void);

	void cleanupTestCase(void);

	void deleteAndRecreate00(void);

	void accessDbSet00(void);

	void deleteAndRecreate01(void);

	void accessDbSet01(void);

	void accessDbSet02(void);

	void accessDbSet03(void);

	void deleteAndRecreate02(void);

	void accessDbSet04(void);

	void accessDbSet05(void);

	void accessDbSet06(void);

	void deleteAndRecreate03(void);

	void accessDbSet07(void);

	void deleteAndRecreate04(void);

	void accessDbSet08(void);

	void deleteAndRecreate05(void);

	void deleteDbSet01(void);

	void deleteAndRecreate06(void);

	void deleteDbSet02(void);

private:
	inline
	QStringList dbFileNameList(const QDir &dbDir) const
	{
		return dbDir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries);
	}

	void deleteAndRecreate(bool onDisk);

	/*
	 * An application object must exist before any QObject.
	 * See https://doc.qt.io/qt-6/threads-qobject.html ,
	 * also see QTEST_GUILESS_MAIN().
	 */
	int m_argc;
	char **m_argv;
	QCoreApplication m_app;

	const QString m_connectionPrefix; /*!< SQL connection prefix. */
	const QString m_dbLocationPathExisting; /*!< Existing directory path. */
	QDir m_dbDirExisting;  /*!< Existing directory containing testing databases. */
	const QString m_dbLocationPathMissing; /*!< Missing directory path. */
	QDir m_dbDirMissing; /*!< Missing directory. */
};

#define printDirContent() \
	fprintf(stderr, "Content: %s\n", dbFileNameList(m_dbDirExisting).join(" ").toUtf8().constData())

TestDbContainer::TestDbContainer(void)
    : m_argc(0), m_argv(NULL), m_app(m_argc, m_argv),
    m_connectionPrefix(QLatin1String("GLOBALDBS")),
    m_dbLocationPathExisting(QDir::currentPath() + QDir::separator() + QLatin1String("_db_sets")),
    m_dbDirExisting(m_dbLocationPathExisting),
    m_dbLocationPathMissing(QDir::currentPath() + QDir::separator() + QLatin1String("_db_sets_missing")),
    m_dbDirMissing(m_dbLocationPathMissing)
{
}

void TestDbContainer::initTestCase(void)
{
	bool onDisk = true;

	QVERIFY(GlobInstcs::logPtr == Q_NULLPTR);
	GlobInstcs::logPtr = new (::std::nothrow) LogDevice;
	QVERIFY(GlobInstcs::logPtr != Q_NULLPTR);

	QVERIFY(GlobInstcs::prefsPtr == Q_NULLPTR);
	GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
	QVERIFY(GlobInstcs::prefsPtr != Q_NULLPTR);

	/* Pointer must be null before initialisation. */
	QVERIFY(GlobInstcs::msgDbsPtr == Q_NULLPTR);
	GlobInstcs::msgDbsPtr = new (::std::nothrow) DbContainer(onDisk, m_connectionPrefix);
	QVERIFY(GlobInstcs::msgDbsPtr != Q_NULLPTR);

	/* Remove and check that is not present. */
	QVERIFY(m_dbDirExisting.removeRecursively());
	QVERIFY(!m_dbDirExisting.exists());
	QVERIFY(m_dbDirMissing.removeRecursively());
	QVERIFY(!m_dbDirMissing.exists());
}

void TestDbContainer::cleanupTestCase(void)
{
	delete GlobInstcs::msgDbsPtr; GlobInstcs::msgDbsPtr = Q_NULLPTR;

	delete GlobInstcs::prefsPtr; GlobInstcs::prefsPtr = Q_NULLPTR;

	delete GlobInstcs::logPtr; GlobInstcs::logPtr = Q_NULLPTR;

	QVERIFY(m_dbDirExisting.removeRecursively());
	QVERIFY(!m_dbDirExisting.exists());
	QVERIFY(m_dbDirMissing.removeRecursively());
	QVERIFY(!m_dbDirMissing.exists());
}

void TestDbContainer::deleteAndRecreate00(void)
{
	deleteAndRecreate(false);
}

void TestDbContainer::accessDbSet00(void)
{
	bool testing = false;
	const AcntId acntId0("user000", testing);
	const AcntId acntId1("user001", testing);
	const AcntId acntId2("user002", testing);

	MessageDbSet *dbSet;

	/* Nothing does exist, there is nothing to open. Opening in memory succeeds. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId0, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 0);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId0, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(!m_dbDirMissing.exists());

	/* CM_CREATE_EMPTY_CURRENT cannot be used with DO_UNKNOWN. Opening in memory succeeds. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_CREATE_ON_DEMAND);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 0);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId1, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(!m_dbDirMissing.exists());

	/* CM_CREATE_ON_DEMAND cannot be used with DO_UNKNOWN. Opening in memory succeeds. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_CREATE_ON_DEMAND);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 0);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId2, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(!m_dbDirMissing.exists());
}

void TestDbContainer::deleteAndRecreate01(void)
{
	deleteAndRecreate(true);
}

void TestDbContainer::accessDbSet01(void)
{
	bool testing = false;
	const AcntId acntId0("user000", testing);

	MessageDbSet *dbSet;

	/* Nothing does exist, there is nothing to open. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId0, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 0);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId0, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(!m_dbDirMissing.exists());

	/* CM_CREATE_EMPTY_CURRENT cannot be used with DO_UNKNOWN. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	     acntId0, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_CREATE_ON_DEMAND);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 0);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId0, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(!m_dbDirMissing.exists());

	/* CM_CREATE_ON_DEMAND cannot be used with DO_UNKNOWN. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId0, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_CREATE_ON_DEMAND);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 0);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId0, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(!m_dbDirMissing.exists());
}

void TestDbContainer::accessDbSet02(void)
{
	bool testing = false;
	const AcntId acntId1("user001", testing);
	const AcntId acntId2("user002", testing);
	const AcntId acntId3("user003", testing);

	MessageDbSet *dbSet, *dbSet2;

	/* Nothing does exist, there is nothing to open. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 0);

	/* Open database, but actually don't create a file. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_CREATE_ON_DEMAND);
	QVERIFY(dbSet != Q_NULLPTR);

	/* There must be no files in the directory. */
	QVERIFY(dbFileNameList(m_dbDirExisting).isEmpty());

	/* Open database, create a file. There must be one file. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_CREATE_EMPTY_CURRENT);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 1);

	/* Open database again. There must be one file. */
	dbSet2 = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_CREATE_EMPTY_CURRENT);
	QVERIFY(dbSet2 == dbSet);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 1);

	/* Open database, create a file. There must be two files. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_YEARLY, MessageDbSet::CM_CREATE_EMPTY_CURRENT);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	/* Open database again. There must be one file.  */
	dbSet2 = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_YEARLY, MessageDbSet::CM_CREATE_EMPTY_CURRENT);
	QVERIFY(dbSet2 == dbSet);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
}

void TestDbContainer::accessDbSet03(void)
{
	bool testing = false;
	const AcntId acntId4("user004", testing);
	const AcntId acntId5("user005", testing);
	const AcntId acntId6("user006", testing);

	MessageDbSet *dbSet, *dbSet2;

	QVERIFY(!m_dbDirMissing.exists());

	/* Nothing does exist, there is nothing to open. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId4, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);

	/* Directory mjst not be created. */
	QVERIFY(!m_dbDirMissing.exists());

	/* Open database, but actually don't create a file. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId4, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_CREATE_ON_DEMAND);
	QVERIFY(dbSet != Q_NULLPTR);

	/* There must be no files in the created directory. */
	QVERIFY(m_dbDirMissing.exists());
	QVERIFY(dbFileNameList(m_dbDirMissing).isEmpty());
	QVERIFY(m_dbDirMissing.removeRecursively());
	QVERIFY(!m_dbDirMissing.exists());

	/* Open database, create a file. There must be one file. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId5, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_CREATE_EMPTY_CURRENT);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(m_dbDirMissing.exists());
	QVERIFY(dbFileNameList(m_dbDirMissing).size() == 1);

	/* Open database again. There must be one file. */
	dbSet2 = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId5, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_CREATE_EMPTY_CURRENT);
	QVERIFY(dbSet2 == dbSet);
	QVERIFY(m_dbDirMissing.exists());
	QVERIFY(dbFileNameList(m_dbDirMissing).size() == 1);

	/* Delete directory. */
	QVERIFY(m_dbDirMissing.removeRecursively());
	QVERIFY(!m_dbDirMissing.exists());

	/* Open database, create a file. There must be two files. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId6, MessageDbSet::DO_YEARLY, MessageDbSet::CM_CREATE_EMPTY_CURRENT);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(m_dbDirMissing.exists());
	QVERIFY(dbFileNameList(m_dbDirMissing).size() == 1);

	/* Open database again. There must be one file.  */
	dbSet2 = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathMissing,
	    acntId6, MessageDbSet::DO_YEARLY, MessageDbSet::CM_CREATE_EMPTY_CURRENT);
	QVERIFY(dbSet2 == dbSet);
	QVERIFY(m_dbDirMissing.exists());
	QVERIFY(dbFileNameList(m_dbDirMissing).size() == 1);

	/* Delete directory. */
	QVERIFY(m_dbDirMissing.removeRecursively());
	QVERIFY(!m_dbDirMissing.exists());
}

void TestDbContainer::deleteAndRecreate02(void)
{
	deleteAndRecreate(true);
}

void TestDbContainer::accessDbSet04(void)
{
	bool testing = false;
	const AcntId acntId1("user001", testing);
	const AcntId acntId2("user002", testing);
	const AcntId acntId3("user003", testing);

	MessageDbSet *dbSet;

	/* Nothing does exist, there is nothing to open. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_YEARLY, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	/* Database exists, but is a single file. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_YEARLY, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	/* Database exists, but is yearly organised. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
}

void TestDbContainer::accessDbSet05(void)
{
	bool testing = true;
	const AcntId acntId1("user001", testing);
	const AcntId acntId2("user002", testing);
	const AcntId acntId3("user003", testing);

	MessageDbSet *dbSet;

	/* Nothing does exist, there is nothing to open. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_YEARLY, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	/* Database exists, is a single file, but is not testing. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_YEARLY, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	/* Database exists, is yearly organised, but is not testing. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_YEARLY, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
}

void TestDbContainer::accessDbSet06(void)
{
	bool testing = false;
	const AcntId acntId2("user002", testing);
	const AcntId acntId3("user003", testing);

	MessageDbSet *dbSet2, *dbSet3;

	/* Databases exist. */
	dbSet2 = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet2 != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	dbSet3 = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_YEARLY, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet3 != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	QVERIFY(dbSet2 != dbSet3);
}

void TestDbContainer::deleteAndRecreate03(void)
{
	deleteAndRecreate(false);
}

void TestDbContainer::accessDbSet07(void)
{
	bool testing = false;
	const AcntId acntId1("user001", testing);
	const AcntId acntId2("user002", testing);
	const AcntId acntId3("user003", testing);

	MessageDbSet *dbSet1, *dbSet2, *dbSet3, *dbSet;

	/* Nothing does exist, there is nothing to open, database in memory. */
	dbSet1 = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet1 != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_YEARLY, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	QVERIFY(dbSet1 == dbSet);

	/* Database exists as single file, but must be opened in memory. */
	dbSet2 = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_YEARLY, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet2 != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	QVERIFY(dbSet2 == dbSet);
	QVERIFY(dbSet1 != dbSet2);

	/* Database exists is yearly organised, but must be opened in memory. */
	dbSet3 = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_SINGLE_FILE, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet3 != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_YEARLY, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	QVERIFY(dbSet3 == dbSet);
	QVERIFY(dbSet1 != dbSet3);
	QVERIFY(dbSet2 != dbSet3);
}

void TestDbContainer::deleteAndRecreate04(void)
{
	deleteAndRecreate(true);
}

void TestDbContainer::accessDbSet08(void)
{
	bool testing = false;
	const AcntId acntId2("user002", testing);
	const AcntId acntId3("user003", testing);

	MessageDbSet *dbSet;

	/* Databases exist. */
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
}

void TestDbContainer::deleteAndRecreate05(void)
{
	deleteAndRecreate(false);
}

void TestDbContainer::deleteDbSet01(void)
{
	bool testing = false;
	const AcntId acntId1("user001", testing);
	const AcntId acntId2("user002", testing);
	const AcntId acntId3("user003", testing);

	MessageDbSet *dbSet;

	/*
	 * Deleting database sets residing in memory. No disk-based content
	 * must be removed.
	 */

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
	QVERIFY(GlobInstcs::msgDbsPtr->deleteDbSet(dbSet));
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId1, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
	QVERIFY(GlobInstcs::msgDbsPtr->deleteDbSet(dbSet));
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
	QVERIFY(GlobInstcs::msgDbsPtr->deleteDbSet(dbSet));
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
}

void TestDbContainer::deleteAndRecreate06(void)
{
	deleteAndRecreate(true);
}

void TestDbContainer::deleteDbSet02(void)
{
	bool testing = false;
	const AcntId acntId1("user001", testing);
	const AcntId acntId2("user002", testing);
	const AcntId acntId3("user003", testing);

	MessageDbSet *dbSet;

	/*
	 * The method will fail on NULL pointer or on values that aren't
	 * handled by the container.
	 */
	//QVERIFY(!GlobInstcs::msgDbsPtr->deleteDbSet(Q_NULLPTR));

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 2);
	QVERIFY(GlobInstcs::msgDbsPtr->deleteDbSet(dbSet));
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 1);
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId2, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);

	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet != Q_NULLPTR);
	QVERIFY(dbFileNameList(m_dbDirExisting).size() == 1);
	QVERIFY(GlobInstcs::msgDbsPtr->deleteDbSet(dbSet));
	QVERIFY(dbFileNameList(m_dbDirExisting).isEmpty());
	dbSet = GlobInstcs::msgDbsPtr->accessDbSet(m_dbLocationPathExisting,
	    acntId3, MessageDbSet::DO_UNKNOWN, MessageDbSet::CM_MUST_EXIST);
	QVERIFY(dbSet == Q_NULLPTR);
}

void TestDbContainer::deleteAndRecreate(bool onDisk)
{
	delete GlobInstcs::msgDbsPtr; GlobInstcs::msgDbsPtr = Q_NULLPTR;

	GlobInstcs::msgDbsPtr = new (::std::nothrow) DbContainer(onDisk, m_connectionPrefix);
	QVERIFY(GlobInstcs::msgDbsPtr != Q_NULLPTR);
}

QObject *newTestDbContainer(void)
{
	return new (::std::nothrow) TestDbContainer();
}

//QTEST_MAIN(TestDbContainer)
#include "test_db_container.moc"
