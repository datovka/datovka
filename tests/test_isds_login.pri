
# TODO -- Modify the co de so that widgets are not heeded.
QT += sql
QT += widgets

DEFINES += \
	TEST_ISDS_LOGIN=1

INCLUDEPATH += \
	/usr/include/libxml2

LIBS += \
	-ldatovka \
	-lcrypto

SOURCES += \
	$${top_srcdir}src/common.cpp \
	$${top_srcdir}src/compat/compat_time.cpp \
	$${top_srcdir}src/crypto/crypto.c \
	$${top_srcdir}src/datovka_shared/compat_qt/random.cpp \
	$${top_srcdir}src/datovka_shared/crypto/crypto_pin.c \
	$${top_srcdir}src/datovka_shared/crypto/crypto_pwd.c \
	$${top_srcdir}src/datovka_shared/crypto/crypto_trusted_certs.c \
	$${top_srcdir}src/datovka_shared/crypto/crypto_wrapped.cpp \
	$${top_srcdir}src/datovka_shared/graphics/colour.cpp \
	$${top_srcdir}src/datovka_shared/identifiers/account_id.cpp \
	$${top_srcdir}src/datovka_shared/io/prefs_db.cpp \
	$${top_srcdir}src/datovka_shared/io/prefs_db_tables.cpp \
	$${top_srcdir}src/datovka_shared/io/sqlite/db.cpp \
	$${top_srcdir}src/datovka_shared/io/sqlite/table.cpp \
	$${top_srcdir}src/datovka_shared/isds/account_interface.cpp \
	$${top_srcdir}src/datovka_shared/isds/box_interface.cpp \
	$${top_srcdir}src/datovka_shared/isds/error.cpp \
	$${top_srcdir}src/datovka_shared/isds/generic_interface.cpp \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.cpp \
	$${top_srcdir}src/datovka_shared/isds/type_conversion.cpp \
	$${top_srcdir}src/datovka_shared/json/basic.cpp \
	$${top_srcdir}src/datovka_shared/json/helper.cpp \
	$${top_srcdir}src/datovka_shared/json/object.cpp \
	$${top_srcdir}src/datovka_shared/localisation/localisation.cpp \
	$${top_srcdir}src/datovka_shared/settings/account.cpp \
	$${top_srcdir}src/datovka_shared/settings/account_container.cpp \
	$${top_srcdir}src/datovka_shared/settings/prefs.cpp \
	$${top_srcdir}src/datovka_shared/settings/prefs_helper.cpp \
	$${top_srcdir}src/datovka_shared/utility/date_time.cpp \
	$${top_srcdir}src/global.cpp \
	$${top_srcdir}src/io/filesystem.cpp \
	$${top_srcdir}src/io/isds_login.cpp \
	$${top_srcdir}src/io/isds_sessions.cpp \
	$${top_srcdir}src/isds/account_conversion.cpp \
	$${top_srcdir}src/isds/box_conversion.cpp \
	$${top_srcdir}src/isds/error_conversion.cpp \
	$${top_srcdir}src/isds/generic_conversion.cpp \
	$${top_srcdir}src/isds/initialisation.cpp \
	$${top_srcdir}src/isds/internal_conversion.cpp \
	$${top_srcdir}src/isds/internal_type_conversion.cpp \
	$${top_srcdir}src/isds/services_box.cpp \
	$${top_srcdir}src/isds/services_login.cpp \
	$${top_srcdir}src/isds/session.cpp \
	$${top_srcdir}src/settings/account.cpp \
	$${top_srcdir}src/settings/accounts.cpp \
	$${top_srcdir}src/settings/preferences.cpp \
	$${top_srcdir}src/settings/prefs_defaults.cpp \
	$${top_srcdir}src/settings/prefs_specific.cpp \
	$${top_srcdir}tests/test_isds_login.cpp

HEADERS += \
	$${top_srcdir}src/common.h \
	$${top_srcdir}src/compat/compat_time.h \
	$${top_srcdir}src/crypto/crypto.h \
	$${top_srcdir}src/crypto/crypto_funcs.h \
	$${top_srcdir}src/datovka_shared/compat_qt/random.h \
	$${top_srcdir}src/datovka_shared/crypto/crypto_pin.h \
	$${top_srcdir}src/datovka_shared/crypto/crypto_pwd.h \
	$${top_srcdir}src/datovka_shared/crypto/crypto_trusted_certs.h \
	$${top_srcdir}src/datovka_shared/crypto/crypto_wrapped.h \
	$${top_srcdir}src/datovka_shared/graphics/colour.h \
	$${top_srcdir}src/datovka_shared/identifiers/account_id.h \
	$${top_srcdir}src/datovka_shared/identifiers/account_id_p.h \
	$${top_srcdir}src/datovka_shared/io/prefs_db.h \
	$${top_srcdir}src/datovka_shared/io/prefs_db_tables.h \
	$${top_srcdir}src/datovka_shared/io/sqlite/db.h \
	$${top_srcdir}src/datovka_shared/io/sqlite/table.h \
	$${top_srcdir}src/datovka_shared/isds/account_interface.h \
	$${top_srcdir}src/datovka_shared/isds/box_interface.h \
	$${top_srcdir}src/datovka_shared/isds/error.h \
	$${top_srcdir}src/datovka_shared/isds/generic_interface.h \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.h \
	$${top_srcdir}src/datovka_shared/isds/type_conversion.h \
	$${top_srcdir}src/datovka_shared/isds/types.h \
	$${top_srcdir}src/datovka_shared/json/basic.h \
	$${top_srcdir}src/datovka_shared/json/helper.h \
	$${top_srcdir}src/datovka_shared/json/object.h \
	$${top_srcdir}src/datovka_shared/localisation/localisation.h \
	$${top_srcdir}src/datovka_shared/settings/account.h \
	$${top_srcdir}src/datovka_shared/settings/account_container.h \
	$${top_srcdir}src/datovka_shared/settings/account_p.h \
	$${top_srcdir}src/datovka_shared/settings/prefs.h \
	$${top_srcdir}src/datovka_shared/settings/prefs_helper.h \
	$${top_srcdir}src/datovka_shared/utility/date_time.h \
	$${top_srcdir}src/global.h \
	$${top_srcdir}src/io/filesystem.h \
	$${top_srcdir}src/io/isds_login.h \
	$${top_srcdir}src/io/isds_sessions.h \
	$${top_srcdir}src/isds/account_conversion.h \
	$${top_srcdir}src/isds/box_conversion.h \
	$${top_srcdir}src/isds/conversion_internal.h \
	$${top_srcdir}src/isds/error_conversion.h \
	$${top_srcdir}src/isds/generic_conversion.h \
	$${top_srcdir}src/isds/initialisation.h \
	$${top_srcdir}src/isds/internal_conversion.h \
	$${top_srcdir}src/isds/internal_type_conversion.h \
	$${top_srcdir}src/isds/services.h \
	$${top_srcdir}src/isds/services_login.h \
	$${top_srcdir}src/isds/session.h \
	$${top_srcdir}src/settings/account.h \
	$${top_srcdir}src/settings/accounts.h \
	$${top_srcdir}src/settings/preferences.h \
	$${top_srcdir}src/settings/prefs_defaults.h \
	$${top_srcdir}src/settings/prefs_specific.h \
	$${top_srcdir}tests/test_isds_login.h
