
DEFINES += \
	TEST_ISDS_MESSAGE=1

INCLUDEPATH += \
	/usr/include/libxml2

LIBS += \
	-ldatovka \
	-lcrypto

SOURCES += \
	$${top_srcdir}src/compat/compat_time.cpp \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.cpp \
	$${top_srcdir}src/datovka_shared/isds/message_interface.cpp \
	$${top_srcdir}src/datovka_shared/isds/message_interface_vodz.cpp \
	$${top_srcdir}src/datovka_shared/isds/type_conversion.cpp \
	$${top_srcdir}src/isds/error_conversion.cpp \
	$${top_srcdir}src/isds/initialisation.cpp \
	$${top_srcdir}src/isds/internal_conversion.cpp \
	$${top_srcdir}src/isds/message_conversion.cpp \
	$${top_srcdir}src/isds/message_functions.cpp \
	$${top_srcdir}tests/test_isds_message.cpp

HEADERS += \
	$${top_srcdir}src/compat/compat_time.h \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.h \
	$${top_srcdir}src/datovka_shared/isds/message_interface.h \
	$${top_srcdir}src/datovka_shared/isds/message_interface_vodz.h \
	$${top_srcdir}src/datovka_shared/isds/type_conversion.h \
	$${top_srcdir}src/datovka_shared/isds/types.h \
	$${top_srcdir}src/isds/conversion_internal.h \
	$${top_srcdir}src/isds/error_conversion.h \
	$${top_srcdir}src/isds/initialisation.h \
	$${top_srcdir}src/isds/internal_conversion.h \
	$${top_srcdir}src/isds/message_conversion.h \
	$${top_srcdir}src/isds/message_functions.h \
	$${top_srcdir}tests/test_isds_message.h
