/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <libdatovka/isds.h>
#include <QDate>
#include <QString>
#include <QtTest/QtTest>

#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/log/log.h"
#include "src/isds/box_conversion.h"
#include "src/isds/initialisation.h"
#include "tests/test_isds_box_interface.h"

class TestIsdsBoxInterface : public QObject {
	Q_OBJECT

public:
	TestIsdsBoxInterface(void);

private slots:
	void initTestCase(void);

	void cleanupTestCase(void);

	void address(void);

	void addressCompare(void);

	void addressConstructor(void);

	void addressCopy(void);

	void addressSetValues(void);

	void addressConversion(void);

	void birthInfo(void);

	void birthInfoCompare(void);

	void birthInfoConstructor(void);

	void birthInfoCopy(void);

	void birthInfoSetValues(void);

	void birthInfoConversion(void);

	void personName(void);

	void personNameCompare(void);

	void personNameConstructor(void);

	void personNameCopy(void);

	void personNameSetValues(void);

	void personNameConversion(void);

	void dbOwnerInfo(void);

	void dbOwnerInfoCompare(void);

	void dbOwnerInfoConstructor(void);

	void dbOwnerInfoCopy(void);

	void dbOwnerInfoSetValues(void);

	void dbOwnerInfoConversion(void);

	void dbUserInfo(void);

	void dbUserInfoCompare(void);

	void dbUserInfoConstructor(void);

	void dbUserInfoCopy(void);

	void dbUserInfoSetValues(void);

	void dbUserInfoConversion(void);

	void credentialsDelivery(void);

	void credentialsDeliveryCompare(void);

	void credentialsDeliveryConstructor(void);

	void credentialsDeliveryCopy(void);

	void credentialsDeliverySetValues(void);

	void credentialsDeliveryConversion(void);

	void pdzInfoRec(void);

	void pdzInfoRecCompare(void);

	void pdzInfoRecConstructor(void);

	void pdzInfoRecCopy(void);

	void pdzInfoRecSetValues(void);

	void dtInfoOutput(void);

	void dtInfoOutputCompare(void);

	void dtInfoOutputConstructor(void);

	void dtInfoOutputCopy(void);

	void dtInfoOutputSetValues(void);
};

TestIsdsBoxInterface::TestIsdsBoxInterface(void)
{
}

void TestIsdsBoxInterface::initTestCase(void)
{
	QVERIFY(GlobInstcs::logPtr == Q_NULLPTR);
}

void TestIsdsBoxInterface::cleanupTestCase(void)
{
}

void TestIsdsBoxInterface::address(void)
{
	Isds::Address addr1, addr2;
	const QString city("adCity");
	const QString street("adStreet");
	const QString numberInStreet("adNumberInStreet");
	const QString numberInMunicipality("adNumberInMunicipality");
	const QString zipCode("adZipCode");
	const QString state("adState");

	/* Must be null. */
	QVERIFY(addr1.city().isNull());
	QVERIFY(addr1.street().isNull());
	QVERIFY(addr1.numberInStreet().isNull());
	QVERIFY(addr1.numberInMunicipality().isNull());
	QVERIFY(addr1.zipCode().isNull());
	QVERIFY(addr1.state().isNull());
	QVERIFY(addr2.city().isNull());
	QVERIFY(addr2.street().isNull());
	QVERIFY(addr2.numberInStreet().isNull());
	QVERIFY(addr2.numberInMunicipality().isNull());
	QVERIFY(addr2.zipCode().isNull());
	QVERIFY(addr2.state().isNull());

	QVERIFY(addr1.isNull());
	QVERIFY(addr2.isNull());
	QVERIFY(addr1 == addr2);

	/* Check whether values are not cross-linked. */
	addr1.setCity(city);
	addr1.setStreet(street);
	addr1.setNumberInStreet(numberInStreet);
	addr1.setNumberInMunicipality(numberInMunicipality);
	addr1.setZipCode(zipCode);
	addr1.setState(state);

	QVERIFY(addr1.city() == city);
	QVERIFY(addr1.street() == street);
	QVERIFY(addr1.numberInStreet() == numberInStreet);
	QVERIFY(addr1.numberInMunicipality() == numberInMunicipality);
	QVERIFY(addr1.zipCode() == zipCode);
	QVERIFY(addr1.state() == state);

	QVERIFY(!addr1.isNull());
	QVERIFY(addr2.isNull());
	QVERIFY(addr1 != addr2);

	/* Check value copying. */
	addr2 = addr1;

	QVERIFY(addr2.city() == city);
	QVERIFY(addr2.street() == street);
	QVERIFY(addr2.numberInStreet() == numberInStreet);
	QVERIFY(addr2.numberInMunicipality() == numberInMunicipality);
	QVERIFY(addr2.zipCode() == zipCode);
	QVERIFY(addr2.state() == state);

	QVERIFY(!addr1.isNull());
	QVERIFY(!addr2.isNull());
	QVERIFY(addr1 == addr2);

	/* Clear value. */
	addr1 = Isds::Address();

	QVERIFY(addr1.city().isNull());
	QVERIFY(addr1.street().isNull());
	QVERIFY(addr1.numberInStreet().isNull());
	QVERIFY(addr1.numberInMunicipality().isNull());
	QVERIFY(addr1.zipCode().isNull());
	QVERIFY(addr1.state().isNull());
	QVERIFY(addr2.city() == city);
	QVERIFY(addr2.street() == street);
	QVERIFY(addr2.numberInStreet() == numberInStreet);
	QVERIFY(addr2.numberInMunicipality() == numberInMunicipality);
	QVERIFY(addr2.zipCode() == zipCode);
	QVERIFY(addr2.state() == state);

	QVERIFY(addr1.isNull());
	QVERIFY(!addr2.isNull());
	QVERIFY(addr1 != addr2);
}

void TestIsdsBoxInterface::addressCompare(void)
{
	Isds::Address addr1, addr2;
	const QString city("adCity");
	const QString street("adStreet");
	const QString numberInStreet("adNumberInStreet");
	const QString numberInMunicipality("adNumberInMunicipality");
	const QString zipCode("adZipCode");
	const QString state("adState");

	addr1.setCity(city);
	addr1.setStreet(street);
	addr1.setNumberInStreet(numberInStreet);
	addr1.setNumberInMunicipality(numberInMunicipality);
	addr1.setZipCode(zipCode);
	addr1.setState(state);
	addr2.setCity(city);
	addr2.setStreet(street);
	addr2.setNumberInStreet(numberInStreet);
	addr2.setNumberInMunicipality(numberInMunicipality);
	addr2.setZipCode(zipCode);
	addr2.setState(state);

	QVERIFY(addr1 == addr2);

	addr2.setCity("");
	QVERIFY(addr1 != addr2);
	addr2.setCity(city);
	QVERIFY(addr1 == addr2);

	addr2.setStreet("");
	QVERIFY(addr1 != addr2);
	addr2.setStreet(street);
	QVERIFY(addr1 == addr2);

	addr2.setNumberInStreet("");
	QVERIFY(addr1 != addr2);
	addr2.setNumberInStreet(numberInStreet);
	QVERIFY(addr1 == addr2);

	addr2.setNumberInMunicipality("");
	QVERIFY(addr1 != addr2);
	addr2.setNumberInMunicipality(numberInMunicipality);
	QVERIFY(addr1 == addr2);

	addr2.setZipCode("");
	QVERIFY(addr1 != addr2);
	addr2.setZipCode(zipCode);
	QVERIFY(addr1 == addr2);

	addr2.setState("");
	QVERIFY(addr1 != addr2);
	addr2.setState(state);
	QVERIFY(addr1 == addr2);
}

void TestIsdsBoxInterface::addressConstructor(void)
{
	{
		Isds::Address addr;

		QVERIFY(addr.isNull());

		QVERIFY(addr.city().isNull());
		QVERIFY(addr.street().isNull());
		QVERIFY(addr.numberInStreet().isNull());
		QVERIFY(addr.numberInMunicipality().isNull());
		QVERIFY(addr.zipCode().isNull());
		QVERIFY(addr.state().isNull());
	}
}

void TestIsdsBoxInterface::addressCopy(void)
{
	Isds::Address addr1;
	const QString city("adCity");
	const QString street("adStreet");
	const QString numberInStreet("adNumberInStreet");
	const QString numberInMunicipality("adNumberInMunicipality");
	const QString zipCode("adZipCode");
	const QString state("adState");

	addr1.setCity(city);
	addr1.setStreet(street);
	addr1.setNumberInStreet(numberInStreet);
	addr1.setNumberInMunicipality(numberInMunicipality);
	addr1.setZipCode(zipCode);
	addr1.setState(state);

	/* Constructor. */
	{
		Isds::Address addr2(addr1);

		QVERIFY(!addr1.isNull());
		QVERIFY(!addr2.isNull());
		QVERIFY(addr1 == addr2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::Address addr3(::std::move(addr2));

		QVERIFY(addr2.isNull());
		QVERIFY(addr1 != addr2);

		QVERIFY(!addr1.isNull());
		QVERIFY(!addr3.isNull());
		QVERIFY(addr1 == addr3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::Address addr2;

		QVERIFY(addr2.isNull());

		addr2 = addr1;

		QVERIFY(!addr1.isNull());
		QVERIFY(!addr2.isNull());
		QVERIFY(addr1 == addr2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::Address addr3;

		QVERIFY(addr3.isNull());

		addr3 = ::std::move(addr2);

		QVERIFY(addr2.isNull());
		QVERIFY(addr1 != addr2);

		QVERIFY(!addr1.isNull());
		QVERIFY(!addr3.isNull());
		QVERIFY(addr1 == addr3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface::addressSetValues(void)
{
	const QString cCity("adCity");
	const QString cStreet("adStreet");
	const QString cNumberInStreet("adNumberInStreet");
	const QString cNumberInMunicipality("adNumberInMunicipality");
	const QString cZipCode("adZipCode");
	const QString cState("adState");

	Isds::Address addr;
	QString city(cCity);
	QString street(cStreet);
	QString numberInStreet(cNumberInStreet);
	QString numberInMunicipality(cNumberInMunicipality);
	QString zipCode(cZipCode);
	QString state(cState);

	QVERIFY(addr.city().isNull());
	addr.setCity(city);
	QVERIFY(addr.city() == cCity);
	QVERIFY(city == cCity);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setCity("other_city");
	QVERIFY(addr.city() != cCity);
	addr.setCity(::std::move(city));
	QVERIFY(addr.city() == cCity);
	QVERIFY(city != cCity);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.street().isNull());
	addr.setStreet(street);
	QVERIFY(addr.street() == cStreet);
	QVERIFY(street == cStreet);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setStreet("other_street");
	QVERIFY(addr.street() != cStreet);
	addr.setStreet(::std::move(street));
	QVERIFY(addr.street() == cStreet);
	QVERIFY(street != cStreet);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.numberInStreet().isNull());
	addr.setNumberInStreet(numberInStreet);
	QVERIFY(addr.numberInStreet() == cNumberInStreet);
	QVERIFY(numberInStreet == cNumberInStreet);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setNumberInStreet("other_numberInStreet");
	QVERIFY(addr.numberInStreet() != cNumberInStreet);
	addr.setNumberInStreet(::std::move(numberInStreet));
	QVERIFY(addr.numberInStreet() == cNumberInStreet);
	QVERIFY(numberInStreet != cNumberInStreet);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.numberInMunicipality().isNull());
	addr.setNumberInMunicipality(numberInMunicipality);
	QVERIFY(addr.numberInMunicipality() == cNumberInMunicipality);
	QVERIFY(numberInMunicipality == cNumberInMunicipality);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setNumberInMunicipality("other_numberInMunicipality");
	QVERIFY(addr.numberInMunicipality() != cNumberInMunicipality);
	addr.setNumberInMunicipality(::std::move(numberInMunicipality));
	QVERIFY(addr.numberInMunicipality() == cNumberInMunicipality);
	QVERIFY(numberInMunicipality != cNumberInMunicipality);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.zipCode().isNull());
	addr.setZipCode(zipCode);
	QVERIFY(addr.zipCode() == cZipCode);
	QVERIFY(zipCode == cZipCode);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setZipCode("other_zipCode");
	QVERIFY(addr.zipCode() != cZipCode);
	addr.setZipCode(::std::move(zipCode));
	QVERIFY(addr.zipCode() == cZipCode);
	QVERIFY(zipCode != cZipCode);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(addr.state().isNull());
	addr.setState(state);
	QVERIFY(addr.state() == cState);
	QVERIFY(state == cState);
#ifdef Q_COMPILER_RVALUE_REFS
	addr.setState("other_state");
	QVERIFY(addr.state() != cState);
	addr.setState(::std::move(state));
	QVERIFY(addr.state() == cState);
	QVERIFY(state != cState);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface::addressConversion(void)
{
	Isds::Address addr1, addr2;
	const QString city("adCity");
	const QString street("adStreet");
	const QString numberInStreet("adNumberInStreet");
	const QString numberInMunicipality("adNumberInMunicipality");
	const QString zipCode("adZipCode");
	const QString state("adState");

	bool iOk = false;
	struct isds_Address *iaddr = NULL;

	QVERIFY(addr1.isNull());
	QVERIFY(addr2.isNull());
	QVERIFY(addr1 == addr2);

	addr1.setCity(city);
	addr1.setStreet(street);
	addr1.setNumberInStreet(numberInStreet);
	addr1.setNumberInMunicipality(numberInMunicipality);
	addr1.setZipCode(zipCode);
	addr1.setState(state);

	QVERIFY(!addr1.isNull());
	QVERIFY(addr2.isNull());
	QVERIFY(addr1 != addr2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	addr2 = Isds::libisds2address(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(addr2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	iaddr = Isds::address2libisds(addr2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(iaddr == NULL);

	iOk = false;
	iaddr = Isds::address2libisds(addr1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(iaddr != NULL);

	iOk = false;
	addr2 = Isds::libisds2address(iaddr, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!addr2.isNull());

	QVERIFY(addr2.city() == city);
	QVERIFY(addr2.street() == street);
	QVERIFY(addr2.numberInStreet() == numberInStreet);
	QVERIFY(addr2.numberInMunicipality() == numberInMunicipality);
	QVERIFY(addr2.zipCode() == zipCode);
	QVERIFY(addr2.state() == state);

	QVERIFY(!addr1.isNull());
	QVERIFY(!addr2.isNull());
	QVERIFY(addr1 == addr2);

	isds_Address_free(&iaddr);
	QVERIFY(iaddr == NULL);
}

void TestIsdsBoxInterface::birthInfo(void)
{
	Isds::BirthInfo bi1, bi2;
	const QDate date(2001, 2, 3);
	const QString city("biCity");
	const QString county("biCounty");
	const QString state("biState");

	/* Must be null. */
	QVERIFY(bi1.date().isNull());
	QVERIFY(bi1.city().isNull());
	QVERIFY(bi1.county().isNull());
	QVERIFY(bi1.state().isNull());
	QVERIFY(bi2.date().isNull());
	QVERIFY(bi2.city().isNull());
	QVERIFY(bi2.county().isNull());
	QVERIFY(bi2.state().isNull());

	QVERIFY(bi1.isNull());
	QVERIFY(bi2.isNull());
	QVERIFY(bi1 == bi2);

	/* Check whether values are not cross-linked. */
	bi1.setDate(date);
	bi1.setCity(city);
	bi1.setCounty(county);
	bi1.setState(state);

	QVERIFY(bi1.date() == date);
	QVERIFY(bi1.city() == city);
	QVERIFY(bi1.county() == county);
	QVERIFY(bi1.state() == state);

	QVERIFY(!bi1.isNull());
	QVERIFY(bi2.isNull());
	QVERIFY(bi1 != bi2);

	/* Check value copying. */
	bi2 = bi1;

	QVERIFY(bi2.date() == date);
	QVERIFY(bi2.city() == city);
	QVERIFY(bi2.county() == county);
	QVERIFY(bi2.state() == state);

	QVERIFY(!bi1.isNull());
	QVERIFY(!bi2.isNull());
	QVERIFY(bi1 == bi2);

	/* Clear value. */
	bi1 = Isds::BirthInfo();

	QVERIFY(bi1.date().isNull());
	QVERIFY(bi1.city().isNull());
	QVERIFY(bi1.county().isNull());
	QVERIFY(bi1.state().isNull());
	QVERIFY(bi2.date() == date);
	QVERIFY(bi2.city() == city);
	QVERIFY(bi2.county() == county);
	QVERIFY(bi2.state() == state);

	QVERIFY(bi1.isNull());
	QVERIFY(!bi2.isNull());
	QVERIFY(bi1 != bi2);
}

void TestIsdsBoxInterface::birthInfoCompare(void)
{
	Isds::BirthInfo bi1, bi2;
	const QDate date(2001, 2, 3);
	const QString city("biCity");
	const QString county("biCounty");
	const QString state("biState");

	bi1.setDate(date);
	bi1.setCity(city);
	bi1.setCounty(county);
	bi1.setState(state);
	bi2.setDate(date);
	bi2.setCity(city);
	bi2.setCounty(county);
	bi2.setState(state);

	QVERIFY(bi1 == bi2);

	bi2.setDate(QDate(2001, 2, 4));
	QVERIFY(bi1 != bi2);
	bi2.setDate(date);
	QVERIFY(bi1 == bi2);

	bi2.setCity("");
	QVERIFY(bi1 != bi2);
	bi2.setCity(city);
	QVERIFY(bi1 == bi2);

	bi2.setCounty("");
	QVERIFY(bi1 != bi2);
	bi2.setCounty(county);
	QVERIFY(bi1 == bi2);

	bi2.setState("");
	QVERIFY(bi1 != bi2);
	bi2.setState(state);
	QVERIFY(bi1 == bi2);
}

void TestIsdsBoxInterface::birthInfoConstructor(void)
{
	{
		Isds::BirthInfo bi;

		QVERIFY(bi.isNull());

		QVERIFY(bi.date().isNull());
		QVERIFY(bi.city().isNull());
		QVERIFY(bi.county().isNull());
		QVERIFY(bi.state().isNull());
	}
}

void TestIsdsBoxInterface::birthInfoCopy(void)
{
	Isds::BirthInfo bi1;
	const QDate date(2001, 2, 3);
	const QString city("biCity");
	const QString county("biCounty");
	const QString state("biState");

	bi1.setDate(date);
	bi1.setCity(city);
	bi1.setCounty(county);
	bi1.setState(state);

	/* Constructor. */
	{
		Isds::BirthInfo bi2(bi1);

		QVERIFY(!bi1.isNull());
		QVERIFY(!bi2.isNull());
		QVERIFY(bi1 == bi2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::BirthInfo bi3(::std::move(bi2));

		QVERIFY(bi2.isNull());
		QVERIFY(bi1 != bi2);

		QVERIFY(!bi1.isNull());
		QVERIFY(!bi3.isNull());
		QVERIFY(bi1 == bi3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::BirthInfo bi2;

		QVERIFY(bi2.isNull());

		bi2 = bi1;

		QVERIFY(!bi1.isNull());
		QVERIFY(!bi2.isNull());
		QVERIFY(bi1 == bi2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::BirthInfo bi3;

		QVERIFY(bi3.isNull());

		bi3 = ::std::move(bi2);

		QVERIFY(bi2.isNull());
		QVERIFY(bi1 != bi2);

		QVERIFY(!bi1.isNull());
		QVERIFY(!bi3.isNull());
		QVERIFY(bi1 == bi3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface::birthInfoSetValues(void)
{
	const QDate cDate(2001, 2, 3);
	const QString cCity("biCity");
	const QString cCounty("biCounty");
	const QString cState("biState");

	Isds::BirthInfo bi;
	QDate date(cDate);
	QString city(cCity);
	QString county(cCounty);
	QString state(cState);

	QVERIFY(bi.date().isNull());
	bi.setDate(date);
	QVERIFY(bi.date() == cDate);
	QVERIFY(date == cDate);
#ifdef Q_COMPILER_RVALUE_REFS
	bi.setDate(QDate(2001, 2, 4));
	QVERIFY(bi.date() != cDate);
	bi.setDate(::std::move(date));
	QVERIFY(bi.date() == cDate);
	//QVERIFY(date != cDate); /* QDate doesn't provide move assignment. */
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(bi.city().isNull());
	bi.setCity(city);
	QVERIFY(bi.city() == cCity);
	QVERIFY(city == cCity);
#ifdef Q_COMPILER_RVALUE_REFS
	bi.setCity("other_city");
	QVERIFY(bi.city() != cCity);
	bi.setCity(::std::move(city));
	QVERIFY(bi.city() == cCity);
	QVERIFY(city != cCity);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(bi.county().isNull());
	bi.setCounty(county);
	QVERIFY(bi.county() == cCounty);
	QVERIFY(county == cCounty);
#ifdef Q_COMPILER_RVALUE_REFS
	bi.setCounty("other_county");
	QVERIFY(bi.county() != cCounty);
	bi.setCounty(::std::move(county));
	QVERIFY(bi.county() == cCounty);
	QVERIFY(county != cCounty);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(bi.state().isNull());
	bi.setState(state);
	QVERIFY(bi.state() == cState);
	QVERIFY(state == cState);
#ifdef Q_COMPILER_RVALUE_REFS
	bi.setState("other_state");
	QVERIFY(bi.state() != cState);
	bi.setState(::std::move(state));
	QVERIFY(bi.state() == cState);
	QVERIFY(state != cState);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface::birthInfoConversion(void)
{
	Isds::BirthInfo bi1, bi2;
	const QDate date(2001, 2, 3);
	const QString city("biCity");
	const QString county("biCounty");
	const QString state("biState");

	bool iOk = false;
	struct isds_BirthInfo *ibi = NULL;

	QVERIFY(bi1.isNull());
	QVERIFY(bi2.isNull());
	QVERIFY(bi1 == bi2);

	bi1.setDate(date);
	bi1.setCity(city);
	bi1.setCounty(county);
	bi1.setState(state);

	/* NULL pointer converts onto null object. */
	iOk = false;
	bi2 = Isds::libisds2birthInfo(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(bi2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	ibi = Isds::birthInfo2libisds(bi2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(ibi == NULL);

	iOk = false;
	ibi = Isds::birthInfo2libisds(bi1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(ibi != NULL);

	iOk = false;
	bi2 = Isds::libisds2birthInfo(ibi, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!bi2.isNull());

	QVERIFY(bi2.date() == date);
	QVERIFY(bi2.city() == city);
	QVERIFY(bi2.county() == county);
	QVERIFY(bi2.state() == state);

	QVERIFY(!bi1.isNull());
	QVERIFY(!bi2.isNull());
	QVERIFY(bi1 == bi2);

	isds_BirthInfo_free(&ibi);
	QVERIFY(ibi == NULL);
}

void TestIsdsBoxInterface::personName(void)
{
	Isds::PersonName pn1, pn2;
	const QString firstName("pnFirstName");
	const QString middleName("pnMiddleName");
	const QString lastName("pnLastName");
	const QString lastNameAtBirth("pnLastNameAtBirth");

	/* Must be null. */
	QVERIFY(pn1.firstName().isNull());
	QVERIFY(pn1.middleName().isNull());
	QVERIFY(pn1.lastName().isNull());
	QVERIFY(pn1.lastNameAtBirth().isNull());
	QVERIFY(pn2.firstName().isNull());
	QVERIFY(pn2.middleName().isNull());
	QVERIFY(pn2.lastName().isNull());
	QVERIFY(pn2.lastNameAtBirth().isNull());

	QVERIFY(pn1.isNull());
	QVERIFY(pn2.isNull());
	QVERIFY(pn1 == pn2);

	/* Check whether values are not cross-linked. */
	pn1.setFirstName(firstName);
	pn1.setMiddleName(middleName);
	pn1.setLastName(lastName);
	pn1.setLastNameAtBirth(lastNameAtBirth);

	QVERIFY(pn1.firstName() == firstName);
	QVERIFY(pn1.middleName() == middleName);
	QVERIFY(pn1.lastName() == lastName);
	QVERIFY(pn1.lastNameAtBirth() == lastNameAtBirth);

	QVERIFY(!pn1.isNull());
	QVERIFY(pn2.isNull());
	QVERIFY(pn1 != pn2);

	/* Check value copying. */
	pn2 = pn1;

	QVERIFY(pn2.firstName() == firstName);
	QVERIFY(pn2.middleName() == middleName);
	QVERIFY(pn2.lastName() == lastName);
	QVERIFY(pn2.lastNameAtBirth() == lastNameAtBirth);

	QVERIFY(!pn1.isNull());
	QVERIFY(!pn2.isNull());
	QVERIFY(pn1 == pn2);

	/* Clear value. */
	pn1 = Isds::PersonName();

	QVERIFY(pn1.firstName().isNull());
	QVERIFY(pn1.middleName().isNull());
	QVERIFY(pn1.lastName().isNull());
	QVERIFY(pn1.lastNameAtBirth().isNull());
	QVERIFY(pn2.firstName() == firstName);
	QVERIFY(pn2.middleName() == middleName);
	QVERIFY(pn2.lastName() == lastName);
	QVERIFY(pn2.lastNameAtBirth() == lastNameAtBirth);

	QVERIFY(pn1.isNull());
	QVERIFY(!pn2.isNull());
	QVERIFY(pn1 != pn2);
}

void TestIsdsBoxInterface::personNameCompare(void)
{
	Isds::PersonName pn1, pn2;
	const QString firstName("pnFirstName");
	const QString middleName("pnMiddleName");
	const QString lastName("pnLastName");
	const QString lastNameAtBirth("pnLastNameAtBirth");

	pn1.setFirstName(firstName);
	pn1.setMiddleName(middleName);
	pn1.setLastName(lastName);
	pn1.setLastNameAtBirth(lastNameAtBirth);
	pn2.setFirstName(firstName);
	pn2.setMiddleName(middleName);
	pn2.setLastName(lastName);
	pn2.setLastNameAtBirth(lastNameAtBirth);

	QVERIFY(pn1 == pn2);

	pn2.setFirstName("");
	QVERIFY(pn1 != pn2);
	pn2.setFirstName(firstName);
	QVERIFY(pn1 == pn2);

	pn2.setMiddleName("");
	QVERIFY(pn1 != pn2);
	pn2.setMiddleName(middleName);
	QVERIFY(pn1 == pn2);

	pn2.setLastName("");
	QVERIFY(pn1 != pn2);
	pn2.setLastName(lastName);
	QVERIFY(pn1 == pn2);

	pn2.setLastNameAtBirth("");
	QVERIFY(pn1 != pn2);
	pn2.setLastNameAtBirth(lastNameAtBirth);
	QVERIFY(pn1 == pn2);
}

void TestIsdsBoxInterface::personNameConstructor(void)
{
	{
		Isds::PersonName pn;

		QVERIFY(pn.isNull());

		QVERIFY(pn.firstName().isNull());
		QVERIFY(pn.middleName().isNull());
		QVERIFY(pn.lastName().isNull());
		QVERIFY(pn.lastNameAtBirth().isNull());
	}
}

void TestIsdsBoxInterface::personNameCopy(void)
{
	Isds::PersonName pn1;
	const QString firstName("pnFirstName");
	const QString middleName("pnMiddleName");
	const QString lastName("pnLastName");
	const QString lastNameAtBirth("pnLastNameAtBirth");

	pn1.setFirstName(firstName);
	pn1.setMiddleName(middleName);
	pn1.setLastName(lastName);
	pn1.setLastNameAtBirth(lastNameAtBirth);

	/* Constructor. */
	{
		Isds::PersonName pn2(pn1);

		QVERIFY(!pn1.isNull());
		QVERIFY(!pn2.isNull());
		QVERIFY(pn1 == pn2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::PersonName pn3(::std::move(pn2));

		QVERIFY(pn2.isNull());
		QVERIFY(pn1 != pn2);

		QVERIFY(!pn1.isNull());
		QVERIFY(!pn3.isNull());
		QVERIFY(pn1 == pn3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::PersonName pn2;

		QVERIFY(pn2.isNull());

		pn2 = pn1;

		QVERIFY(!pn1.isNull());
		QVERIFY(!pn2.isNull());
		QVERIFY(pn1 == pn2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::PersonName pn3;

		QVERIFY(pn3.isNull());

		pn3 = ::std::move(pn2);

		QVERIFY(pn2.isNull());
		QVERIFY(pn1 != pn2);

		QVERIFY(!pn1.isNull());
		QVERIFY(!pn3.isNull());
		QVERIFY(pn1 == pn3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface::personNameSetValues(void)
{
	const QString cFirstName("pnFirstName");
	const QString cMiddleName("pnMiddleName");
	const QString cLastName("pnLastName");
	const QString cLastNameAtBirth("pnLastNameAtBirth");

	Isds::PersonName pn;
	QString firstName(cFirstName);
	QString middleName(cMiddleName);
	QString lastName(cLastName);
	QString lastNameAtBirth(cLastNameAtBirth);

	QVERIFY(pn.firstName().isNull());
	pn.setFirstName(firstName);
	QVERIFY(pn.firstName() == cFirstName);
	QVERIFY(firstName == cFirstName);
#ifdef Q_COMPILER_RVALUE_REFS
	pn.setFirstName("other_firstName");
	QVERIFY(pn.firstName() != cFirstName);
	pn.setFirstName(::std::move(firstName));
	QVERIFY(pn.firstName() == cFirstName);
	QVERIFY(firstName != cFirstName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(pn.middleName().isNull());
	pn.setMiddleName(middleName);
	QVERIFY(pn.middleName() == cMiddleName);
	QVERIFY(middleName == cMiddleName);
#ifdef Q_COMPILER_RVALUE_REFS
	pn.setMiddleName("other_middleName");
	QVERIFY(pn.middleName() != cMiddleName);
	pn.setMiddleName(::std::move(middleName));
	QVERIFY(pn.middleName() == cMiddleName);
	QVERIFY(middleName != cMiddleName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(pn.lastName().isNull());
	pn.setLastName(lastName);
	QVERIFY(pn.lastName() == cLastName);
	QVERIFY(lastName == cLastName);
#ifdef Q_COMPILER_RVALUE_REFS
	pn.setLastName("other_lastName");
	QVERIFY(pn.lastName() != cLastName);
	pn.setLastName(::std::move(lastName));
	QVERIFY(pn.lastName() == cLastName);
	QVERIFY(lastName != cLastName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(pn.lastNameAtBirth().isNull());
	pn.setLastNameAtBirth(lastNameAtBirth);
	QVERIFY(pn.lastNameAtBirth() == cLastNameAtBirth);
	QVERIFY(lastNameAtBirth == cLastNameAtBirth);
#ifdef Q_COMPILER_RVALUE_REFS
	pn.setLastNameAtBirth("other_lastNameAtBirth");
	QVERIFY(pn.lastNameAtBirth() != cLastNameAtBirth);
	pn.setLastNameAtBirth(::std::move(lastNameAtBirth));
	QVERIFY(pn.lastNameAtBirth() == cLastNameAtBirth);
	QVERIFY(lastNameAtBirth != cLastNameAtBirth);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface::personNameConversion(void)
{
	Isds::PersonName pn1, pn2;
	const QString firstName("pnFirstName");
	const QString middleName("pnMiddleName");
	const QString lastName("pnLastName");
	const QString lastNameAtBirth("pnLastNameAtBirth");

	bool iOk = false;
	struct isds_PersonName *ipn = NULL;

	QVERIFY(pn1.isNull());
	QVERIFY(pn2.isNull());
	QVERIFY(pn1 == pn2);

	pn1.setFirstName(firstName);
	pn1.setMiddleName(middleName);
	pn1.setLastName(lastName);
	pn1.setLastNameAtBirth(lastNameAtBirth);

	QVERIFY(!pn1.isNull());
	QVERIFY(pn2.isNull());
	QVERIFY(pn1 != pn2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	pn2 = Isds::libisds2personName(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(pn2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	ipn = Isds::personName2libisds(pn2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(ipn == NULL);

	iOk = false;
	ipn = Isds::personName2libisds(pn1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(ipn != NULL);

	iOk = false;
	pn2 = Isds::libisds2personName(ipn, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!pn2.isNull());

	QVERIFY(pn2.firstName() == firstName);
	QVERIFY(pn2.middleName() == middleName);
	QVERIFY(pn2.lastName() == lastName);
	QVERIFY(pn2.lastNameAtBirth() == lastNameAtBirth);

	QVERIFY(!pn1.isNull());
	QVERIFY(!pn2.isNull());
	QVERIFY(pn1 == pn2);

	isds_PersonName_free(&ipn);
	QVERIFY(ipn == NULL);
}

/*!
 * @brief Builds Isds::PersonName instance.
 */
static
Isds::PersonName buildPersonName(void)
{
	Isds::PersonName pn;

	pn.setFirstName("pnFirstName");
	pn.setMiddleName("pnMiddleName");
	pn.setLastName("pnLastName");
	pn.setLastNameAtBirth("pnLastNameAtBirth");

	return pn;
}

/*!
 * @brief Build Isds::BirthInfo instance;
 */
static
Isds::BirthInfo buildBirthInfo(void)
{
	Isds::BirthInfo bi;

	bi.setDate(QDate(2001, 2, 3));
	bi.setCity("biCity");
	bi.setCounty("biCounty");
	bi.setState("biState");

	return bi;
}

/*!
 * @brief Build Isds::Address instance.
 */
static
Isds::Address buildAddress(void)
{
	Isds::Address addr;

	addr.setCity("adCity");
	addr.setStreet("adStreet");
	addr.setNumberInStreet("adNumberInStreet");
	addr.setNumberInMunicipality("adNumberInMunicipality");
	addr.setZipCode("adZipCode");
	addr.setState("adState");

	return addr;
}

void TestIsdsBoxInterface::dbOwnerInfo(void)
{
	Isds::DbOwnerInfo doi1, doi2;
	const QString dbID("dbID");
	const enum Isds::Type::DbType dbType = Isds::Type::BT_OVM;
	const QString ic("ic");
	const Isds::PersonName personName(buildPersonName());
	const QString firmName("firmName");
	const Isds::BirthInfo birthInfo(buildBirthInfo());
	const Isds::Address address(buildAddress());
	const QString nationality("nationality");
	const QString email("email");
	const QString telNumber("telNumber");
	const QString identifier("identifier");
	const QString registryCode("registryCode");
	const enum Isds::Type::DbState dbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool dbEffectiveOVM = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::NilBool dbOpenAddressing = Isds::Type::BOOL_TRUE;

	/* Must be null. */
	QVERIFY(doi1.dbID().isNull());
	QVERIFY(doi1.dbType() == Isds::Type::BT_NULL);
	QVERIFY(doi1.ic().isNull());
	QVERIFY(doi1.personName().isNull());
	QVERIFY(doi1.firmName().isNull());
	QVERIFY(doi1.birthInfo().isNull());
	QVERIFY(doi1.address().isNull());
	QVERIFY(doi1.nationality().isNull());
	QVERIFY(doi1.email().isNull());
	QVERIFY(doi1.telNumber().isNull());
	QVERIFY(doi1.identifier().isNull());
	QVERIFY(doi1.registryCode().isNull());
	QVERIFY(doi1.dbState() == Isds::Type::BS_ERROR);
	QVERIFY(doi1.dbEffectiveOVM() == Isds::Type::BOOL_NULL);
	QVERIFY(doi1.dbOpenAddressing() == Isds::Type::BOOL_NULL);
	QVERIFY(doi2.dbID().isNull());
	QVERIFY(doi2.dbType() == Isds::Type::BT_NULL);
	QVERIFY(doi2.ic().isNull());
	QVERIFY(doi2.personName().isNull());
	QVERIFY(doi2.firmName().isNull());
	QVERIFY(doi2.birthInfo().isNull());
	QVERIFY(doi2.address().isNull());
	QVERIFY(doi2.nationality().isNull());
	QVERIFY(doi2.email().isNull());
	QVERIFY(doi2.telNumber().isNull());
	QVERIFY(doi2.identifier().isNull());
	QVERIFY(doi2.registryCode().isNull());
	QVERIFY(doi2.dbState() == Isds::Type::BS_ERROR);
	QVERIFY(doi2.dbEffectiveOVM() == Isds::Type::BOOL_NULL);
	QVERIFY(doi2.dbOpenAddressing() == Isds::Type::BOOL_NULL);

	QVERIFY(doi1.isNull());
	QVERIFY(doi2.isNull());
	QVERIFY(doi1 == doi2);

	/* Check whether values are not cross-linked. */
	doi1.setDbID(dbID);
	doi1.setDbType(dbType);
	doi1.setIc(ic);
	doi1.setPersonName(personName);
	doi1.setFirmName(firmName);
	doi1.setBirthInfo(birthInfo);
	doi1.setAddress(address);
	doi1.setNationality(nationality);
	doi1.setEmail(email);
	doi1.setTelNumber(telNumber);
	doi1.setIdentifier(identifier);
	doi1.setRegistryCode(registryCode);
	doi1.setDbState(dbState);
	doi1.setDbEffectiveOVM(dbEffectiveOVM);
	doi1.setDbOpenAddressing(dbOpenAddressing);

	QVERIFY(doi1.dbID() == dbID);
	QVERIFY(doi1.dbType() == dbType);
	QVERIFY(doi1.ic() == ic);
	QVERIFY(doi1.personName() == personName);
	QVERIFY(doi1.firmName() == firmName);
	QVERIFY(doi1.birthInfo() == birthInfo);
	QVERIFY(doi1.address() == address);
	QVERIFY(doi1.nationality() == nationality);
	QVERIFY(doi1.email() == email);
	QVERIFY(doi1.telNumber() == telNumber);
	QVERIFY(doi1.identifier() == identifier);
	QVERIFY(doi1.registryCode() == registryCode);
	QVERIFY(doi1.dbState() == dbState);
	QVERIFY(doi1.dbEffectiveOVM() == dbEffectiveOVM);
	QVERIFY(doi1.dbOpenAddressing() == dbOpenAddressing);

	QVERIFY(!doi1.isNull());
	QVERIFY(doi2.isNull());
	QVERIFY(doi1 != doi2);

	/* Check value copying. */
	doi2 = doi1;

	QVERIFY(doi2.dbID() == dbID);
	QVERIFY(doi2.dbType() == dbType);
	QVERIFY(doi2.ic() == ic);
	QVERIFY(doi2.personName() == personName);
	QVERIFY(doi2.firmName() == firmName);
	QVERIFY(doi2.birthInfo() == birthInfo);
	QVERIFY(doi2.address() == address);
	QVERIFY(doi2.nationality() == nationality);
	QVERIFY(doi2.email() == email);
	QVERIFY(doi2.telNumber() == telNumber);
	QVERIFY(doi2.identifier() == identifier);
	QVERIFY(doi2.registryCode() == registryCode);
	QVERIFY(doi2.dbState() == dbState);
	QVERIFY(doi2.dbEffectiveOVM() == dbEffectiveOVM);
	QVERIFY(doi2.dbOpenAddressing() == dbOpenAddressing);

	QVERIFY(!doi1.isNull());
	QVERIFY(!doi2.isNull());
	QVERIFY(doi1 == doi2);

	/* Clear value. */
	doi1 = Isds::DbOwnerInfo();

	QVERIFY(doi1.dbID().isNull());
	QVERIFY(doi1.dbType() == Isds::Type::BT_NULL);
	QVERIFY(doi1.ic().isNull());
	QVERIFY(doi1.personName().isNull());
	QVERIFY(doi1.firmName().isNull());
	QVERIFY(doi1.birthInfo().isNull());
	QVERIFY(doi1.address().isNull());
	QVERIFY(doi1.nationality().isNull());
	QVERIFY(doi1.email().isNull());
	QVERIFY(doi1.telNumber().isNull());
	QVERIFY(doi1.identifier().isNull());
	QVERIFY(doi1.registryCode().isNull());
	QVERIFY(doi1.dbState() == Isds::Type::BS_ERROR);
	QVERIFY(doi1.dbEffectiveOVM() == Isds::Type::BOOL_NULL);
	QVERIFY(doi1.dbOpenAddressing() == Isds::Type::BOOL_NULL);
	QVERIFY(doi2.dbID() == dbID);
	QVERIFY(doi2.dbType() == dbType);
	QVERIFY(doi2.ic() == ic);
	QVERIFY(doi2.personName() == personName);
	QVERIFY(doi2.firmName() == firmName);
	QVERIFY(doi2.birthInfo() == birthInfo);
	QVERIFY(doi2.address() == address);
	QVERIFY(doi2.nationality() == nationality);
	QVERIFY(doi2.email() == email);
	QVERIFY(doi2.telNumber() == telNumber);
	QVERIFY(doi2.identifier() == identifier);
	QVERIFY(doi2.registryCode() == registryCode);
	QVERIFY(doi2.dbState() == dbState);
	QVERIFY(doi2.dbEffectiveOVM() == dbEffectiveOVM);
	QVERIFY(doi2.dbOpenAddressing() == dbOpenAddressing);

	QVERIFY(doi1.isNull());
	QVERIFY(!doi2.isNull());
	QVERIFY(doi1 != doi2);
}

void TestIsdsBoxInterface::dbOwnerInfoCompare(void)
{
	Isds::DbOwnerInfo doi1, doi2;
	const QString dbID("dbID");
	const enum Isds::Type::DbType dbType = Isds::Type::BT_OVM;
	const QString ic("ic");
	const Isds::PersonName personName(buildPersonName());
	const QString firmName("firmName");
	const Isds::BirthInfo birthInfo(buildBirthInfo());
	const Isds::Address address(buildAddress());
	const QString nationality("nationality");
	const QString email("email");
	const QString telNumber("telNumber");
	const QString identifier("identifier");
	const QString registryCode("registryCode");
	const enum Isds::Type::DbState dbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool dbEffectiveOVM = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::NilBool dbOpenAddressing = Isds::Type::BOOL_TRUE;

	doi1.setDbID(dbID);
	doi1.setDbType(dbType);
	doi1.setIc(ic);
	doi1.setPersonName(personName);
	doi1.setFirmName(firmName);
	doi1.setBirthInfo(birthInfo);
	doi1.setAddress(address);
	doi1.setNationality(nationality);
	doi1.setEmail(email);
	doi1.setTelNumber(telNumber);
	doi1.setIdentifier(identifier);
	doi1.setRegistryCode(registryCode);
	doi1.setDbState(dbState);
	doi1.setDbEffectiveOVM(dbEffectiveOVM);
	doi1.setDbOpenAddressing(dbOpenAddressing);
	doi2.setDbID(dbID);
	doi2.setDbType(dbType);
	doi2.setIc(ic);
	doi2.setPersonName(personName);
	doi2.setFirmName(firmName);
	doi2.setBirthInfo(birthInfo);
	doi2.setAddress(address);
	doi2.setNationality(nationality);
	doi2.setEmail(email);
	doi2.setTelNumber(telNumber);
	doi2.setIdentifier(identifier);
	doi2.setRegistryCode(registryCode);
	doi2.setDbState(dbState);
	doi2.setDbEffectiveOVM(dbEffectiveOVM);
	doi2.setDbOpenAddressing(dbOpenAddressing);

	QVERIFY(doi1 == doi2);

	doi2.setDbID("");
	QVERIFY(doi1 != doi2);
	doi2.setDbID(dbID);
	QVERIFY(doi1 == doi2);

	doi2.setDbType(Isds::Type::BT_PO);
	QVERIFY(doi1 != doi2);
	doi2.setDbType(dbType);
	QVERIFY(doi1 == doi2);

	doi2.setIc("");
	QVERIFY(doi1 != doi2);
	doi2.setIc(ic);
	QVERIFY(doi1 == doi2);

	doi2.setPersonName(Isds::PersonName());
	QVERIFY(doi1 != doi2);
	doi2.setPersonName(personName);
	QVERIFY(doi1 == doi2);

	doi2.setFirmName("");
	QVERIFY(doi1 != doi2);
	doi2.setFirmName(firmName);
	QVERIFY(doi1 == doi2);

	doi2.setBirthInfo(Isds::BirthInfo());
	QVERIFY(doi1 != doi2);
	doi2.setBirthInfo(birthInfo);
	QVERIFY(doi1 == doi2);

	doi2.setAddress(Isds::Address());
	QVERIFY(doi1 != doi2);
	doi2.setAddress(address);
	QVERIFY(doi1 == doi2);

	doi2.setNationality("");
	QVERIFY(doi1 != doi2);
	doi2.setNationality(nationality);
	QVERIFY(doi1 == doi2);

	doi2.setEmail("");
	QVERIFY(doi1 != doi2);
	doi2.setEmail(email);
	QVERIFY(doi1 == doi2);

	doi2.setTelNumber("");
	QVERIFY(doi1 != doi2);
	doi2.setTelNumber(telNumber);
	QVERIFY(doi1 == doi2);

	doi2.setIdentifier("");
	QVERIFY(doi1 != doi2);
	doi2.setIdentifier(identifier);
	QVERIFY(doi1 == doi2);

	doi2.setRegistryCode("");
	QVERIFY(doi1 != doi2);
	doi2.setRegistryCode(registryCode);
	QVERIFY(doi1 == doi2);

	doi2.setDbState(Isds::Type::BS_TEMP_INACCESSIBLE);
	QVERIFY(doi1 != doi2);
	doi2.setDbState(dbState);
	QVERIFY(doi1 == doi2);

	doi2.setDbEffectiveOVM(Isds::Type::BOOL_TRUE);
	QVERIFY(doi1 != doi2);
	doi2.setDbEffectiveOVM(dbEffectiveOVM);
	QVERIFY(doi1 == doi2);

	doi2.setDbOpenAddressing(Isds::Type::BOOL_FALSE);
	QVERIFY(doi1 != doi2);
	doi2.setDbOpenAddressing(dbOpenAddressing);
	QVERIFY(doi1 == doi2);
}

void TestIsdsBoxInterface::dbOwnerInfoConstructor(void)
{
	{
		Isds::DbOwnerInfo doi;

		QVERIFY(doi.isNull());

		QVERIFY(doi.dbID().isNull());
		QVERIFY(doi.dbType() == Isds::Type::BT_NULL);
		QVERIFY(doi.ic().isNull());
		QVERIFY(doi.personName().isNull());
		QVERIFY(doi.firmName().isNull());
		QVERIFY(doi.birthInfo().isNull());
		QVERIFY(doi.address().isNull());
		QVERIFY(doi.nationality().isNull());
		QVERIFY(doi.email().isNull());
		QVERIFY(doi.telNumber().isNull());
		QVERIFY(doi.identifier().isNull());
		QVERIFY(doi.registryCode().isNull());
		QVERIFY(doi.dbState() == Isds::Type::BS_ERROR);
		QVERIFY(doi.dbEffectiveOVM() == Isds::Type::BOOL_NULL);
		QVERIFY(doi.dbOpenAddressing() == Isds::Type::BOOL_NULL);
	}
}

void TestIsdsBoxInterface::dbOwnerInfoCopy(void)
{
	Isds::DbOwnerInfo doi1;
	const QString dbID("dbID");
	const enum Isds::Type::DbType dbType = Isds::Type::BT_OVM;
	const QString ic("ic");
	const Isds::PersonName personName(buildPersonName());
	const QString firmName("firmName");
	const Isds::BirthInfo birthInfo(buildBirthInfo());
	const Isds::Address address(buildAddress());
	const QString nationality("nationality");
	const QString email("email");
	const QString telNumber("telNumber");
	const QString identifier("identifier");
	const QString registryCode("registryCode");
	const enum Isds::Type::DbState dbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool dbEffectiveOVM = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::NilBool dbOpenAddressing = Isds::Type::BOOL_TRUE;

	doi1.setDbID(dbID);
	doi1.setDbType(dbType);
	doi1.setIc(ic);
	doi1.setPersonName(personName);
	doi1.setFirmName(firmName);
	doi1.setBirthInfo(birthInfo);
	doi1.setAddress(address);
	doi1.setNationality(nationality);
	doi1.setEmail(email);
	doi1.setTelNumber(telNumber);
	doi1.setIdentifier(identifier);
	doi1.setRegistryCode(registryCode);
	doi1.setDbState(dbState);
	doi1.setDbEffectiveOVM(dbEffectiveOVM);
	doi1.setDbOpenAddressing(dbOpenAddressing);

	/* Constructor. */
	{
		Isds::DbOwnerInfo doi2(doi1);

		QVERIFY(!doi1.isNull());
		QVERIFY(!doi2.isNull());
		QVERIFY(doi1 == doi2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DbOwnerInfo doi3(::std::move(doi2));

		QVERIFY(doi2.isNull());
		QVERIFY(doi1 != doi2);

		QVERIFY(!doi1.isNull());
		QVERIFY(!doi3.isNull());
		QVERIFY(doi1 == doi3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::DbOwnerInfo doi2;

		QVERIFY(doi2.isNull());

		doi2 = doi1;

		QVERIFY(!doi1.isNull());
		QVERIFY(!doi2.isNull());
		QVERIFY(doi1 == doi2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DbOwnerInfo doi3;

		QVERIFY(doi3.isNull());

		doi3 = ::std::move(doi2);

		QVERIFY(doi2.isNull());
		QVERIFY(doi1 != doi2);

		QVERIFY(!doi1.isNull());
		QVERIFY(!doi3.isNull());
		QVERIFY(doi1 == doi3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface::dbOwnerInfoSetValues(void)
{
	const QString cDbID("dbID");
	const enum Isds::Type::DbType cDbType = Isds::Type::BT_OVM;
	const QString cIc("ic");
	const Isds::PersonName cPersonName(buildPersonName());
	const QString cFirmName("firmName");
	const Isds::BirthInfo cBirthInfo(buildBirthInfo());
	const Isds::Address cAddress(buildAddress());
	const QString cNationality("nationality");
	const QString cEmail("email");
	const QString cTelNumber("telNumber");
	const QString cIdentifier("identifier");
	const QString cRegistryCode("registryCode");
	const enum Isds::Type::DbState cDbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool cDbEffectiveOVM = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::NilBool cDbOpenAddressing = Isds::Type::BOOL_TRUE;

	Isds::DbOwnerInfo doi;
	QString dbID(cDbID);
	enum Isds::Type::DbType dbType = cDbType;
	QString ic(cIc);
	Isds::PersonName personName(cPersonName);
	QString firmName(cFirmName);
	Isds::BirthInfo birthInfo(cBirthInfo);
	Isds::Address address(cAddress);
	QString nationality(cNationality);
	QString email(cEmail);
	QString telNumber(cTelNumber);
	QString identifier(cIdentifier);
	QString registryCode(cRegistryCode);
	enum Isds::Type::DbState dbState = cDbState;
	enum Isds::Type::NilBool dbEffectiveOVM = cDbEffectiveOVM;
	enum Isds::Type::NilBool dbOpenAddressing = cDbOpenAddressing;

	QVERIFY(doi.dbID().isNull());
	doi.setDbID(dbID);
	QVERIFY(doi.dbID() == cDbID);
	QVERIFY(dbID == cDbID);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setDbID("other_dbID");
	QVERIFY(doi.dbID() != cDbID);
	doi.setDbID(::std::move(dbID));
	QVERIFY(doi.dbID() == cDbID);
	QVERIFY(dbID != cDbID);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.dbType() == Isds::Type::BT_NULL);
	doi.setDbType(dbType);
	QVERIFY(doi.dbType() == cDbType);
	QVERIFY(dbType == cDbType);

	QVERIFY(doi.ic().isNull());
	doi.setIc(ic);
	QVERIFY(doi.ic() == cIc);
	QVERIFY(ic == cIc);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setIc("other_ic");
	QVERIFY(doi.ic() != cIc);
	doi.setIc(::std::move(ic));
	QVERIFY(doi.ic() == cIc);
	QVERIFY(ic != cIc);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.personName().isNull());
	doi.setPersonName(personName);
	QVERIFY(doi.personName() == cPersonName);
	QVERIFY(personName == cPersonName);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setPersonName(Isds::PersonName());
	QVERIFY(doi.personName() != cPersonName);
	doi.setPersonName(::std::move(personName));
	QVERIFY(doi.personName() == cPersonName);
	QVERIFY(personName != cPersonName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.firmName().isNull());
	doi.setFirmName(firmName);
	QVERIFY(doi.firmName() == cFirmName);
	QVERIFY(firmName == cFirmName);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setFirmName("other_firmName");
	QVERIFY(doi.firmName() != cFirmName);
	doi.setFirmName(::std::move(firmName));
	QVERIFY(doi.firmName() == cFirmName);
	QVERIFY(firmName != cFirmName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.birthInfo().isNull());
	doi.setBirthInfo(birthInfo);
	QVERIFY(doi.birthInfo() == cBirthInfo);
	QVERIFY(birthInfo == cBirthInfo);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setBirthInfo(Isds::BirthInfo());
	QVERIFY(doi.birthInfo() != cBirthInfo);
	doi.setBirthInfo(::std::move(birthInfo));
	QVERIFY(doi.birthInfo() == cBirthInfo);
	QVERIFY(birthInfo != cBirthInfo);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.address().isNull());
	doi.setAddress(address);
	QVERIFY(doi.address() == cAddress);
	QVERIFY(address == cAddress);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setAddress(Isds::Address());
	QVERIFY(doi.address() != cAddress);
	doi.setAddress(::std::move(address));
	QVERIFY(doi.address() == cAddress);
	QVERIFY(address != cAddress);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.nationality().isNull());
	doi.setNationality(nationality);
	QVERIFY(doi.nationality() == cNationality);
	QVERIFY(nationality == cNationality);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setNationality("other_nationality");
	QVERIFY(doi.nationality() != cNationality);
	doi.setNationality(::std::move(nationality));
	QVERIFY(doi.nationality() == cNationality);
	QVERIFY(nationality != cNationality);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.email().isNull());
	doi.setEmail(email);
	QVERIFY(doi.email() == cEmail);
	QVERIFY(email == cEmail);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setEmail("other_email");
	QVERIFY(doi.email() != cEmail);
	doi.setEmail(::std::move(email));
	QVERIFY(doi.email() == cEmail);
	QVERIFY(email != cEmail);
#endif /* Q_COMPILER_RVALUE_REFS */


	QVERIFY(doi.telNumber().isNull());
	doi.setTelNumber(telNumber);
	QVERIFY(doi.telNumber() == cTelNumber);
	QVERIFY(telNumber == cTelNumber);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setTelNumber("other_telNumber");
	QVERIFY(doi.telNumber() != cTelNumber);
	doi.setTelNumber(::std::move(telNumber));
	QVERIFY(doi.telNumber() == cTelNumber);
	QVERIFY(telNumber != cTelNumber);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.identifier().isNull());
	doi.setIdentifier(identifier);
	QVERIFY(doi.identifier() == cIdentifier);
	QVERIFY(identifier == cIdentifier);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setIdentifier("other_identifier");
	QVERIFY(doi.identifier() != cIdentifier);
	doi.setIdentifier(::std::move(identifier));
	QVERIFY(doi.identifier() == cIdentifier);
	QVERIFY(identifier != cIdentifier);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.registryCode().isNull());
	doi.setRegistryCode(registryCode);
	QVERIFY(doi.registryCode() == cRegistryCode);
	QVERIFY(registryCode == cRegistryCode);
#ifdef Q_COMPILER_RVALUE_REFS
	doi.setRegistryCode("other_registryCode");
	QVERIFY(doi.registryCode() != cRegistryCode);
	doi.setRegistryCode(::std::move(registryCode));
	QVERIFY(doi.registryCode() == cRegistryCode);
	QVERIFY(registryCode != cRegistryCode);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(doi.dbState() == Isds::Type::BS_ERROR);
	doi.setDbState(dbState);
	QVERIFY(doi.dbState() == cDbState);
	QVERIFY(dbState == cDbState);

	QVERIFY(doi.dbEffectiveOVM() == Isds::Type::BOOL_NULL);
	doi.setDbEffectiveOVM(dbEffectiveOVM);
	QVERIFY(doi.dbEffectiveOVM() == cDbEffectiveOVM);
	QVERIFY(dbEffectiveOVM == cDbEffectiveOVM);

	QVERIFY(doi.dbOpenAddressing() == Isds::Type::BOOL_NULL);
	doi.setDbOpenAddressing(dbOpenAddressing);
	QVERIFY(doi.dbOpenAddressing() == cDbOpenAddressing);
	QVERIFY(dbOpenAddressing == cDbOpenAddressing);
}

void TestIsdsBoxInterface::dbOwnerInfoConversion(void)
{
	Isds::DbOwnerInfo doi1, doi2;
	const QString dbID("dbID");
	const enum Isds::Type::DbType dbType = Isds::Type::BT_OVM;
	const QString ic("ic");
	const Isds::PersonName personName(buildPersonName());
	const QString firmName("firmName");
	const Isds::BirthInfo birthInfo(buildBirthInfo());
	const Isds::Address address(buildAddress());
	const QString nationality("nationality");
	const QString email("email");
	const QString telNumber("telNumber");
	const QString identifier("identifier");
	const QString registryCode("registryCode");
	const enum Isds::Type::DbState dbState = Isds::Type::BS_ACCESSIBLE;
	const enum Isds::Type::NilBool dbEffectiveOVM = Isds::Type::BOOL_FALSE;
	const enum Isds::Type::NilBool dbOpenAddressing = Isds::Type::BOOL_TRUE;

	bool iOk = false;
	struct isds_DbOwnerInfo *idoi = NULL;

	QVERIFY(doi1.isNull());
	QVERIFY(doi2.isNull());
	QVERIFY(doi1 == doi2);

	doi1.setDbID(dbID);
	doi1.setDbType(dbType);
	doi1.setIc(ic);
	doi1.setPersonName(personName);
	doi1.setFirmName(firmName);
	doi1.setBirthInfo(birthInfo);
	doi1.setAddress(address);
	doi1.setNationality(nationality);
	doi1.setEmail(email);
	doi1.setTelNumber(telNumber);
	doi1.setIdentifier(identifier);
	doi1.setRegistryCode(registryCode);
	doi1.setDbState(dbState);
	doi1.setDbEffectiveOVM(dbEffectiveOVM);
	doi1.setDbOpenAddressing(dbOpenAddressing);

	QVERIFY(!doi1.isNull());
	QVERIFY(doi2.isNull());
	QVERIFY(doi1 != doi2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	doi2 = Isds::libisds2dbOwnerInfo(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(doi2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	idoi = Isds::dbOwnerInfo2libisds(doi2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(idoi == NULL);

	iOk = false;
	idoi = Isds::dbOwnerInfo2libisds(doi1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(idoi != NULL);

	iOk = false;
	doi2 = Isds::libisds2dbOwnerInfo(idoi, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!doi2.isNull());

	QVERIFY(doi2.dbID() == dbID);
	QVERIFY(doi2.dbType() == dbType);
	QVERIFY(doi2.ic() == ic);
	QVERIFY(doi2.personName() == personName);
	QVERIFY(doi2.firmName() == firmName);
	QVERIFY(doi2.birthInfo() == birthInfo);
	QVERIFY(doi2.address() == address);
	QVERIFY(doi2.nationality() == nationality);
	QVERIFY(doi2.email() == email);
	QVERIFY(doi2.telNumber() == telNumber);
	QVERIFY(doi2.identifier() == identifier);
	QVERIFY(doi2.registryCode() == registryCode);
	QVERIFY(doi2.dbState() == dbState);
	QVERIFY(doi2.dbEffectiveOVM() == dbEffectiveOVM);
	QVERIFY(doi2.dbOpenAddressing() == dbOpenAddressing);

	QVERIFY(!doi1.isNull());
	QVERIFY(!doi2.isNull());
	QVERIFY(doi1 == doi2);

	isds_DbOwnerInfo_free(&idoi);
	QVERIFY(idoi == NULL);
}

void TestIsdsBoxInterface::dbUserInfo(void)
{
	Isds::DbUserInfo dui1, dui2;
	const QString userID("userID");
	const enum Isds::Type::UserType userType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges userPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const Isds::PersonName personName(buildPersonName());
	const Isds::Address address(buildAddress());
	const QDate biDate(2001, 2, 3);
	const QString ic("ic");
	const QString firmName("firmName");
	const QString caStreet("caStreet");
	const QString caCity("caCity");
	const QString caZipCode("caZipCode");
	const QString caState("caState");

	/* Must be null. */
	QVERIFY(dui1.userID().isNull());
	QVERIFY(dui1.userType() == Isds::Type::UT_NULL);
	QVERIFY(dui1.userPrivils() == Isds::Type::PRIVIL_NONE);
	QVERIFY(dui1.personName().isNull());
	QVERIFY(dui1.address().isNull());
	QVERIFY(dui1.biDate().isNull());
	QVERIFY(dui1.ic().isNull());
	QVERIFY(dui1.firmName().isNull());
	QVERIFY(dui1.caStreet().isNull());
	QVERIFY(dui1.caCity().isNull());
	QVERIFY(dui1.caZipCode().isNull());
	QVERIFY(dui1.caState().isNull());
	QVERIFY(dui2.userID().isNull());
	QVERIFY(dui2.userType() == Isds::Type::UT_NULL);
	QVERIFY(dui2.userPrivils() == Isds::Type::PRIVIL_NONE);
	QVERIFY(dui2.personName().isNull());
	QVERIFY(dui2.address().isNull());
	QVERIFY(dui2.biDate().isNull());
	QVERIFY(dui2.ic().isNull());
	QVERIFY(dui2.firmName().isNull());
	QVERIFY(dui2.caStreet().isNull());
	QVERIFY(dui2.caCity().isNull());
	QVERIFY(dui2.caZipCode().isNull());
	QVERIFY(dui2.caState().isNull());

	QVERIFY(dui1.isNull());
	QVERIFY(dui2.isNull());
	QVERIFY(dui1 == dui2);

	/* Check whether values are not cross-linked. */
	dui1.setUserID(userID);
	dui1.setUserType(userType);
	dui1.setUserPrivils(userPrivils);
	dui1.setPersonName(personName);
	dui1.setAddress(address);
	dui1.setBiDate(biDate);
	dui1.setIc(ic);
	dui1.setFirmName(firmName);
	dui1.setCaStreet(caStreet);
	dui1.setCaCity(caCity);
	dui1.setCaZipCode(caZipCode);
	dui1.setCaState(caState);

	QVERIFY(dui1.userID() == userID);
	QVERIFY(dui1.userType() == userType);
	QVERIFY(dui1.userPrivils() == userPrivils);
	QVERIFY(dui1.personName() == personName);
	QVERIFY(dui1.address() == address);
	QVERIFY(dui1.biDate() == biDate);
	QVERIFY(dui1.ic() == ic);
	QVERIFY(dui1.firmName() == firmName);
	QVERIFY(dui1.caStreet() == caStreet);
	QVERIFY(dui1.caCity() == caCity);
	QVERIFY(dui1.caZipCode() == caZipCode);
	QVERIFY(dui1.caState() == caState);

	QVERIFY(!dui1.isNull());
	QVERIFY(dui2.isNull());
	QVERIFY(dui1 != dui2);

	/* Check value copying. */
	dui2 = dui1;

	QVERIFY(dui2.userID() == userID);
	QVERIFY(dui2.userType() == userType);
	QVERIFY(dui2.userPrivils() == userPrivils);
	QVERIFY(dui2.personName() == personName);
	QVERIFY(dui2.address() == address);
	QVERIFY(dui2.biDate() == biDate);
	QVERIFY(dui2.ic() == ic);
	QVERIFY(dui2.firmName() == firmName);
	QVERIFY(dui2.caStreet() == caStreet);
	QVERIFY(dui2.caCity() == caCity);
	QVERIFY(dui2.caZipCode() == caZipCode);
	QVERIFY(dui2.caState() == caState);

	QVERIFY(!dui1.isNull());
	QVERIFY(!dui2.isNull());
	QVERIFY(dui1 == dui2);

	/* Clear value. */
	dui1 = Isds::DbUserInfo();

	QVERIFY(dui1.userID().isNull());
	QVERIFY(dui1.userType() == Isds::Type::UT_NULL);
	QVERIFY(dui1.userPrivils() == Isds::Type::PRIVIL_NONE);
	QVERIFY(dui1.personName().isNull());
	QVERIFY(dui1.address().isNull());
	QVERIFY(dui1.biDate().isNull());
	QVERIFY(dui1.ic().isNull());
	QVERIFY(dui1.firmName().isNull());
	QVERIFY(dui1.caStreet().isNull());
	QVERIFY(dui1.caCity().isNull());
	QVERIFY(dui1.caZipCode().isNull());
	QVERIFY(dui1.caState().isNull());
	QVERIFY(dui2.userID() == userID);
	QVERIFY(dui2.userType() == userType);
	QVERIFY(dui2.userPrivils() == userPrivils);
	QVERIFY(dui2.personName() == personName);
	QVERIFY(dui2.address() == address);
	QVERIFY(dui2.biDate() == biDate);
	QVERIFY(dui2.ic() == ic);
	QVERIFY(dui2.firmName() == firmName);
	QVERIFY(dui2.caStreet() == caStreet);
	QVERIFY(dui2.caCity() == caCity);
	QVERIFY(dui2.caZipCode() == caZipCode);
	QVERIFY(dui2.caState() == caState);

	QVERIFY(dui1.isNull());
	QVERIFY(!dui2.isNull());
	QVERIFY(dui1 != dui2);
}

void TestIsdsBoxInterface::dbUserInfoCompare(void)
{
	Isds::DbUserInfo dui1, dui2;
	const QString userID("userID");
	const enum Isds::Type::UserType userType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges userPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const Isds::PersonName personName(buildPersonName());
	const Isds::Address address(buildAddress());
	const QDate biDate(2001, 2, 3);
	const QString ic("ic");
	const QString firmName("firmName");
	const QString caStreet("caStreet");
	const QString caCity("caCity");
	const QString caZipCode("caZipCode");
	const QString caState("caState");

	dui1.setUserID(userID);
	dui1.setUserType(userType);
	dui1.setUserPrivils(userPrivils);
	dui1.setPersonName(personName);
	dui1.setAddress(address);
	dui1.setBiDate(biDate);
	dui1.setIc(ic);
	dui1.setFirmName(firmName);
	dui1.setCaStreet(caStreet);
	dui1.setCaCity(caCity);
	dui1.setCaZipCode(caZipCode);
	dui1.setCaState(caState);
	dui2.setUserID(userID);
	dui2.setUserType(userType);
	dui2.setUserPrivils(userPrivils);
	dui2.setPersonName(personName);
	dui2.setAddress(address);
	dui2.setBiDate(biDate);
	dui2.setIc(ic);
	dui2.setFirmName(firmName);
	dui2.setCaStreet(caStreet);
	dui2.setCaCity(caCity);
	dui2.setCaZipCode(caZipCode);
	dui2.setCaState(caState);

	QVERIFY(dui1 == dui2);

	dui2.setUserID("");
	QVERIFY(dui1 != dui2);
	dui2.setUserID(userID);
	QVERIFY(dui1 == dui2);

	dui2.setUserType(Isds::Type::UT_ENTRUSTED);
	QVERIFY(dui1 != dui2);
	dui2.setUserType(userType);
	QVERIFY(dui1 == dui2);

	dui2.setUserPrivils(Isds::Type::PRIVIL_READ_ALL);
	QVERIFY(dui1 != dui2);
	dui2.setUserPrivils(userPrivils);
	QVERIFY(dui1 == dui2);

	dui2.setPersonName(Isds::PersonName());
	QVERIFY(dui1 != dui2);
	dui2.setPersonName(personName);
	QVERIFY(dui1 == dui2);

	dui2.setAddress(Isds::Address());
	QVERIFY(dui1 != dui2);
	dui2.setAddress(address);
	QVERIFY(dui1 == dui2);

	dui2.setBiDate(QDate(2001, 2, 4));
	QVERIFY(dui1 != dui2);
	dui2.setBiDate(biDate);
	QVERIFY(dui1 == dui2);

	dui2.setIc("");
	QVERIFY(dui1 != dui2);
	dui2.setIc(ic);
	QVERIFY(dui1 == dui2);

	dui2.setFirmName("");
	QVERIFY(dui1 != dui2);
	dui2.setFirmName(firmName);
	QVERIFY(dui1 == dui2);

	dui2.setCaStreet("");
	QVERIFY(dui1 != dui2);
	dui2.setCaStreet(caStreet);
	QVERIFY(dui1 == dui2);

	dui2.setCaCity("");
	QVERIFY(dui1 != dui2);
	dui2.setCaCity(caCity);
	QVERIFY(dui1 == dui2);

	dui2.setCaZipCode("");
	QVERIFY(dui1 != dui2);
	dui2.setCaZipCode(caZipCode);
	QVERIFY(dui1 == dui2);

	dui2.setCaState("");
	QVERIFY(dui1 != dui2);
	dui2.setCaState(caState);
	QVERIFY(dui1 == dui2);
}

void TestIsdsBoxInterface::dbUserInfoConstructor(void)
{
	{
		Isds::DbUserInfo dui;

		QVERIFY(dui.isNull());

		QVERIFY(dui.userID().isNull());
		QVERIFY(dui.userType() == Isds::Type::UT_NULL);
		QVERIFY(dui.userPrivils() == Isds::Type::PRIVIL_NONE);
		QVERIFY(dui.personName().isNull());
		QVERIFY(dui.address().isNull());
		QVERIFY(dui.biDate().isNull());
		QVERIFY(dui.ic().isNull());
		QVERIFY(dui.firmName().isNull());
		QVERIFY(dui.caStreet().isNull());
		QVERIFY(dui.caCity().isNull());
		QVERIFY(dui.caZipCode().isNull());
		QVERIFY(dui.caState().isNull());
	}
}

void TestIsdsBoxInterface::dbUserInfoCopy(void)
{
	Isds::DbUserInfo dui1;
	const QString userID("userID");
	const enum Isds::Type::UserType userType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges userPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const Isds::PersonName personName(buildPersonName());
	const Isds::Address address(buildAddress());
	const QDate biDate(2001, 2, 3);
	const QString ic("ic");
	const QString firmName("firmName");
	const QString caStreet("caStreet");
	const QString caCity("caCity");
	const QString caZipCode("caZipCode");
	const QString caState("caState");

	dui1.setUserID(userID);
	dui1.setUserType(userType);
	dui1.setUserPrivils(userPrivils);
	dui1.setPersonName(personName);
	dui1.setAddress(address);
	dui1.setBiDate(biDate);
	dui1.setIc(ic);
	dui1.setFirmName(firmName);
	dui1.setCaStreet(caStreet);
	dui1.setCaCity(caCity);
	dui1.setCaZipCode(caZipCode);
	dui1.setCaState(caState);

	/* Constructor. */
	{
		Isds::DbUserInfo dui2(dui1);

		QVERIFY(!dui1.isNull());
		QVERIFY(!dui2.isNull());
		QVERIFY(dui1 == dui2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DbUserInfo dui3(::std::move(dui2));

		QVERIFY(dui2.isNull());
		QVERIFY(dui1 != dui2);

		QVERIFY(!dui1.isNull());
		QVERIFY(!dui3.isNull());
		QVERIFY(dui1 == dui3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::DbUserInfo dui2;

		QVERIFY(dui2.isNull());

		dui2 = dui1;

		QVERIFY(!dui1.isNull());
		QVERIFY(!dui2.isNull());
		QVERIFY(dui1 == dui2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DbUserInfo dui3;

		QVERIFY(dui3.isNull());

		dui3 = ::std::move(dui2);

		QVERIFY(dui2.isNull());
		QVERIFY(dui1 != dui2);

		QVERIFY(!dui1.isNull());
		QVERIFY(!dui3.isNull());
		QVERIFY(dui1 == dui3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface::dbUserInfoSetValues(void)
{
	const QString cUserID("userID");
	const enum Isds::Type::UserType cUserType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges cUserPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const Isds::PersonName cPersonName(buildPersonName());
	const Isds::Address cAddress(buildAddress());
	const QDate cBiDate(2001, 2, 3);
	const QString cIc("ic");
	const QString cFirmName("firmName");
	const QString cCaStreet("caStreet");
	const QString cCaCity("caCity");
	const QString cCaZipCode("caZipCode");
	const QString cCaState("caState");

	Isds::DbUserInfo dui;
	QString userID(cUserID);
	enum Isds::Type::UserType userType = cUserType;
	Isds::Type::Privileges userPrivils = cUserPrivils;
	Isds::PersonName personName(cPersonName);
	Isds::Address address(cAddress);
	QDate biDate(cBiDate);
	QString ic(cIc);
	QString firmName(cFirmName);
	QString caStreet(cCaStreet);
	QString caCity(cCaCity);
	QString caZipCode(cCaZipCode);
	QString caState(cCaState);

	QVERIFY(dui.userID().isNull());
	dui.setUserID(userID);
	QVERIFY(dui.userID() == cUserID);
	QVERIFY(userID == cUserID);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setUserID("other_userID");
	QVERIFY(dui.userID() != cUserID);
	dui.setUserID(::std::move(userID));
	QVERIFY(dui.userID() == cUserID);
	QVERIFY(userID != cUserID);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.userType() == Isds::Type::UT_NULL);
	dui.setUserType(userType);
	QVERIFY(dui.userType() == cUserType);
	QVERIFY(userType == cUserType);

	QVERIFY(dui.userPrivils() == Isds::Type::PRIVIL_NONE);
	dui.setUserPrivils(userPrivils);
	QVERIFY(dui.userPrivils() == cUserPrivils);
	QVERIFY(userPrivils == cUserPrivils);

	QVERIFY(dui.personName().isNull());
	dui.setPersonName(personName);
	QVERIFY(dui.personName() == cPersonName);
	QVERIFY(personName == cPersonName);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setPersonName(Isds::PersonName());
	QVERIFY(dui.personName() != cPersonName);
	dui.setPersonName(::std::move(personName));
	QVERIFY(dui.personName() == cPersonName);
	QVERIFY(personName != cPersonName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.address().isNull());
	dui.setAddress(address);
	QVERIFY(dui.address() == cAddress);
	QVERIFY(address == cAddress);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setAddress(Isds::Address());
	QVERIFY(dui.address() != cAddress);
	dui.setAddress(::std::move(address));
	QVERIFY(dui.address() == cAddress);
	QVERIFY(address != cAddress);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.biDate().isNull());
	dui.setBiDate(biDate);
	QVERIFY(dui.biDate() == cBiDate);
	QVERIFY(biDate == cBiDate);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setBiDate(QDate(2001, 2, 4));
	QVERIFY(dui.biDate() != cBiDate);
	dui.setBiDate(::std::move(biDate));
	QVERIFY(dui.biDate() == cBiDate);
	//QVERIFY(biDate != cBiDate); /* QDate doesn't provide move assignment. */
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.ic().isNull());
	dui.setIc(ic);
	QVERIFY(dui.ic() == cIc);
	QVERIFY(ic == cIc);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setIc("other_ic");
	QVERIFY(dui.ic() != cIc);
	dui.setIc(::std::move(ic));
	QVERIFY(dui.ic() == cIc);
	QVERIFY(ic != cIc);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.firmName().isNull());
	dui.setFirmName(firmName);
	QVERIFY(dui.firmName() == cFirmName);
	QVERIFY(firmName == cFirmName);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setFirmName("other_firmName");
	QVERIFY(dui.firmName() != cFirmName);
	dui.setFirmName(::std::move(firmName));
	QVERIFY(dui.firmName() == cFirmName);
	QVERIFY(firmName != cFirmName);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.caStreet().isNull());
	dui.setCaStreet(caStreet);
	QVERIFY(dui.caStreet() == cCaStreet);
	QVERIFY(caStreet == cCaStreet);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setCaStreet("other_caStreet");
	QVERIFY(dui.caStreet() != cCaStreet);
	dui.setCaStreet(::std::move(caStreet));
	QVERIFY(dui.caStreet() == cCaStreet);
	QVERIFY(caStreet != cCaStreet);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.caCity().isNull());
	dui.setCaCity(caCity);
	QVERIFY(dui.caCity() == cCaCity);
	QVERIFY(caCity == cCaCity);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setCaCity("other_caCity");
	QVERIFY(dui.caCity() != cCaCity);
	dui.setCaCity(::std::move(caCity));
	QVERIFY(dui.caCity() == cCaCity);
	QVERIFY(caCity != cCaCity);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.caZipCode().isNull());
	dui.setCaZipCode(caZipCode);
	QVERIFY(dui.caZipCode() == cCaZipCode);
	QVERIFY(caZipCode == cCaZipCode);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setCaZipCode("other_caZipCode");
	QVERIFY(dui.caZipCode() != cCaZipCode);
	dui.setCaZipCode(::std::move(caZipCode));
	QVERIFY(dui.caZipCode() == cCaZipCode);
	QVERIFY(caZipCode != cCaZipCode);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(dui.caState().isNull());
	dui.setCaState(caState);
	QVERIFY(dui.caState() == cCaState);
	QVERIFY(caState == cCaState);
#ifdef Q_COMPILER_RVALUE_REFS
	dui.setCaState("other_caState");
	QVERIFY(dui.caState() != cCaState);
	dui.setCaState(::std::move(caState));
	QVERIFY(dui.caState() == cCaState);
	QVERIFY(caState != cCaState);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface::dbUserInfoConversion(void)
{
	Isds::DbUserInfo dui1, dui2;
	const QString userID("userID");
	const enum Isds::Type::UserType userType = Isds::Type::UT_PRIMARY;
	const Isds::Type::Privileges userPrivils = Isds::Type::PRIVIL_READ_NON_PERSONAL;
	const Isds::PersonName personName(buildPersonName());
	const Isds::Address address(buildAddress());
	const QDate biDate(2001, 2, 3);
	const QString ic("ic");
	const QString firmName("firmName");
	const QString caStreet("caStreet");
	const QString caCity("caCity");
	const QString caZipCode("caZipCode");
	const QString caState("caState");

	bool iOk = false;
	struct isds_DbUserInfo *idui = NULL;

	QVERIFY(dui1.isNull());
	QVERIFY(dui2.isNull());
	QVERIFY(dui1 == dui2);

	dui1.setUserID(userID);
	dui1.setUserType(userType);
	dui1.setUserPrivils(userPrivils);
	dui1.setPersonName(personName);
	dui1.setAddress(address);
	dui1.setBiDate(biDate);
	dui1.setIc(ic);
	dui1.setFirmName(firmName);
	dui1.setCaStreet(caStreet);
	dui1.setCaCity(caCity);
	dui1.setCaZipCode(caZipCode);
	dui1.setCaState(caState);

	QVERIFY(!dui1.isNull());
	QVERIFY(dui2.isNull());
	QVERIFY(dui1 != dui2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	dui2 = Isds::libisds2dbUserInfo(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(dui2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	idui = Isds::dbUserInfo2libisds(dui2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(idui == NULL);

	iOk = false;
	idui = Isds::dbUserInfo2libisds(dui1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(idui != NULL);

	iOk = false;
	dui2 = Isds::libisds2dbUserInfo(idui, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!dui2.isNull());

	QVERIFY(dui2.userID() == userID);
	QVERIFY(dui2.userType() == userType);
	QVERIFY(dui2.userPrivils() == userPrivils);
	QVERIFY(dui2.personName() == personName);
	QVERIFY(dui2.address() == address);
	QVERIFY(dui2.biDate() == biDate);
	QVERIFY(dui2.ic() == ic);
	QVERIFY(dui2.firmName() == firmName);
	QVERIFY(dui2.caStreet() == caStreet);
	QVERIFY(dui2.caCity() == caCity);
	QVERIFY(dui2.caZipCode() == caZipCode);
	QVERIFY(dui2.caState() == caState);

	QVERIFY(!dui1.isNull());
	QVERIFY(!dui2.isNull());
	QVERIFY(dui1 == dui2);

	isds_DbUserInfo_free(&idui);
	QVERIFY(idui == NULL);
}

void TestIsdsBoxInterface::credentialsDelivery(void)
{
	Isds::CredentialsDelivery cd1, cd2;
	const QString email("email");
	const QString token("token");
	const QString username("username");

	/* Must be null. */
	QVERIFY(cd1.email().isNull());
	QVERIFY(cd1.token().isNull());
	QVERIFY(cd1.username().isNull());
	QVERIFY(cd2.email().isNull());
	QVERIFY(cd2.token().isNull());
	QVERIFY(cd2.username().isNull());

	QVERIFY(cd1.isNull());
	QVERIFY(cd2.isNull());
	QVERIFY(cd1 == cd2);

	/* Check whether values are not cross-linked. */
	cd1.setEmail(email);
	cd1.setToken(token);
	cd1.setUsername(username);

	QVERIFY(cd1.email() == email);
	QVERIFY(cd1.token() == token);
	QVERIFY(cd1.username() == username);

	QVERIFY(!cd1.isNull());
	QVERIFY(cd2.isNull());
	QVERIFY(cd1 != cd2);

	/* Check value copying. */
	cd2 = cd1;

	QVERIFY(cd2.email() == email);
	QVERIFY(cd2.token() == token);
	QVERIFY(cd2.username() == username);

	QVERIFY(!cd1.isNull());
	QVERIFY(!cd2.isNull());
	QVERIFY(cd1 == cd2);

	/* Clear value. */
	cd1 = Isds::CredentialsDelivery();

	QVERIFY(cd1.email().isNull());
	QVERIFY(cd1.token().isNull());
	QVERIFY(cd1.username().isNull());
	QVERIFY(cd2.email() == email);
	QVERIFY(cd2.token() == token);
	QVERIFY(cd2.username() == username);

	QVERIFY(cd1.isNull());
	QVERIFY(!cd2.isNull());
	QVERIFY(cd1 != cd2);
}

void TestIsdsBoxInterface::credentialsDeliveryCompare(void)
{
	Isds::CredentialsDelivery cd1, cd2;
	const QString email("email");
	const QString token("token");
	const QString username("username");

	cd1.setEmail(email);
	cd1.setToken(token);
	cd1.setUsername(username);
	cd2.setEmail(email);
	cd2.setToken(token);
	cd2.setUsername(username);

	QVERIFY(cd1 == cd2);

	cd2.setEmail("");
	QVERIFY(cd1 != cd2);
	cd2.setEmail(email);
	QVERIFY(cd1 == cd2);

	cd2.setToken("");
	QVERIFY(cd1 != cd2);
	cd2.setToken(token);
	QVERIFY(cd1 == cd2);

	cd2.setUsername("");
	QVERIFY(cd1 != cd2);
	cd2.setUsername(username);
	QVERIFY(cd1 == cd2);
}

void TestIsdsBoxInterface::credentialsDeliveryConstructor(void)
{
	{
		Isds::CredentialsDelivery cd;

		QVERIFY(cd.isNull());

		QVERIFY(cd.email().isNull());
		QVERIFY(cd.token().isNull());
		QVERIFY(cd.username().isNull());
	}
}

void TestIsdsBoxInterface::credentialsDeliveryCopy(void)
{
	Isds::CredentialsDelivery cd1;
	const QString email("email");
	const QString token("token");
	const QString username("username");

	cd1.setEmail(email);
	cd1.setToken(token);
	cd1.setUsername(username);

	/* Constructor. */
	{
		Isds::CredentialsDelivery cd2(cd1);

		QVERIFY(!cd1.isNull());
		QVERIFY(!cd2.isNull());
		QVERIFY(cd1 == cd2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::CredentialsDelivery cd3(::std::move(cd2));

		QVERIFY(cd2.isNull());
		QVERIFY(cd1 != cd2);

		QVERIFY(!cd1.isNull());
		QVERIFY(!cd3.isNull());
		QVERIFY(cd1 == cd3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}


	/* Assignment operator. */
	{
		Isds::CredentialsDelivery cd2;

		QVERIFY(cd2.isNull());

		cd2 = cd1;

		QVERIFY(!cd1.isNull());
		QVERIFY(!cd2.isNull());
		QVERIFY(cd1 == cd2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::CredentialsDelivery cd3;

		QVERIFY(cd3.isNull());

		cd3 = ::std::move(cd2);

		QVERIFY(cd2.isNull());
		QVERIFY(cd1 != cd2);

		QVERIFY(!cd1.isNull());
		QVERIFY(!cd3.isNull());
		QVERIFY(cd1 == cd3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface::credentialsDeliverySetValues(void)
{
	const QString cEmail("email");
	const QString cToken("token");
	const QString cUsername("username");

	Isds::CredentialsDelivery cd;
	QString email(cEmail);
	QString token(cToken);
	QString username(cUsername);

	QVERIFY(cd.email().isNull());
	cd.setEmail(email);
	QVERIFY(cd.email() == cEmail);
	QVERIFY(email == cEmail);
#ifdef Q_COMPILER_RVALUE_REFS
	cd.setEmail("other_email");
	QVERIFY(cd.email() != cEmail);
	cd.setEmail(::std::move(email));
	QVERIFY(cd.email() == cEmail);
	QVERIFY(email != cEmail);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(cd.token().isNull());
	cd.setToken(token);
	QVERIFY(cd.token() == cToken);
	QVERIFY(token == cToken);
#ifdef Q_COMPILER_RVALUE_REFS
	cd.setToken("other_token");
	QVERIFY(cd.token() != cToken);
	cd.setToken(::std::move(token));
	QVERIFY(cd.token() == cToken);
	QVERIFY(token != cToken);
#endif /* Q_COMPILER_RVALUE_REFS */


	QVERIFY(cd.username().isNull());
	cd.setUsername(username);
	QVERIFY(cd.username() == cUsername);
	QVERIFY(username == cUsername);
#ifdef Q_COMPILER_RVALUE_REFS
	cd.setUsername("other_username");
	QVERIFY(cd.username() != cUsername);
	cd.setUsername(::std::move(username));
	QVERIFY(cd.username() == cUsername);
	QVERIFY(username != cUsername);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface::credentialsDeliveryConversion(void)
{
	Isds::CredentialsDelivery cd1, cd2;
	const QString email("email");
	const QString token("token");
	const QString username("username");

	bool iOk = false;
	struct isds_credentials_delivery *icd = NULL;

	QVERIFY(cd1.isNull());
	QVERIFY(cd2.isNull());
	QVERIFY(cd1 == cd2);

	cd1.setEmail(email);
	cd1.setToken(token);
	cd1.setUsername(username);

	QVERIFY(!cd1.isNull());
	QVERIFY(cd2.isNull());
	QVERIFY(cd1 != cd2);

	/* NULL pointer converts onto null object. */
	iOk = false;
	cd2 = Isds::libisds2credentialsDelivery(NULL, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(cd2.isNull());

	/* Null object converts onto NULL pointer. */
	iOk = false;
	icd = Isds::credentialsDelivery2libisds(cd2, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(icd == NULL);

	iOk = false;
	icd = Isds::credentialsDelivery2libisds(cd1, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(icd != NULL);

	iOk = false;
	cd2 = Isds::libisds2credentialsDelivery(icd, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!cd2.isNull());

	QVERIFY(cd2.email() == email);
	QVERIFY(cd2.token() == token);
	QVERIFY(cd2.username() == username);

	QVERIFY(!cd1.isNull());
	QVERIFY(!cd2.isNull());
	QVERIFY(cd1 == cd2);

	isds_credentials_delivery_free(&icd);
	QVERIFY(icd == NULL);
}

void TestIsdsBoxInterface::pdzInfoRec(void)
{
	Isds::PDZInfoRec pi1, pi2;
	const enum Isds::Type::PdzPaymentType type = Isds::Type::PPT_E;
	const QString recip("recip");
	const QString payer("payer");
	const QDateTime expire(QDate(2021, 5, 6), QTime(8, 30, 0), Qt::UTC);
	const qint64 cnt = 1;
	const QString ident("ident");

	/* Must be null. */
	QVERIFY(pi1.pdzType() == Isds::Type::PPT_UNKNOWN);
	QVERIFY(pi1.pdzRecip().isNull());
	QVERIFY(pi1.pdzPayer().isNull());
	QVERIFY(pi1.pdzExpire().isNull());
	QVERIFY(pi1.pdzCnt() == -1);
	QVERIFY(pi1.odzIdent().isNull());
	QVERIFY(pi2.pdzType() == Isds::Type::PPT_UNKNOWN);
	QVERIFY(pi2.pdzRecip().isNull());
	QVERIFY(pi2.pdzPayer().isNull());
	QVERIFY(pi2.pdzExpire().isNull());
	QVERIFY(pi2.pdzCnt() == -1);
	QVERIFY(pi2.odzIdent().isNull());

	QVERIFY(pi1.isNull());
	QVERIFY(pi2.isNull());
	QVERIFY(pi1 == pi2);

	/* Check whether values are not cross-linked. */
	pi1.setPdzType(type);
	pi1.setPdzRecip(recip);
	pi1.setPdzPayer(payer);
	pi1.setPdzExpire(expire);
	pi1.setPdzCnt(cnt);
	pi1.setOdzIdent(ident);

	QVERIFY(pi1.pdzType() == type);
	QVERIFY(pi1.pdzRecip() == recip);
	QVERIFY(pi1.pdzPayer() == payer);
	QVERIFY(pi1.pdzExpire() == expire);
	QVERIFY(pi1.pdzCnt() == cnt);
	QVERIFY(pi1.odzIdent() == ident);

	QVERIFY(!pi1.isNull());
	QVERIFY(pi2.isNull());
	QVERIFY(pi1 != pi2);

	/* Check value copying. */
	pi2 = pi1;

	QVERIFY(pi2.pdzType() == type);
	QVERIFY(pi2.pdzRecip() == recip);
	QVERIFY(pi2.pdzPayer() == payer);
	QVERIFY(pi2.pdzExpire() == expire);
	QVERIFY(pi2.pdzCnt() == cnt);
	QVERIFY(pi2.odzIdent() == ident);

	QVERIFY(!pi1.isNull());
	QVERIFY(!pi1.isNull());
	QVERIFY(pi1 == pi2);

	/* Clear value. */
	pi1 = Isds::PDZInfoRec();

	QVERIFY(pi1.pdzType() == Isds::Type::PPT_UNKNOWN);
	QVERIFY(pi1.pdzRecip().isNull());
	QVERIFY(pi1.pdzPayer().isNull());
	QVERIFY(pi1.pdzExpire().isNull());
	QVERIFY(pi1.pdzCnt() == -1);
	QVERIFY(pi1.odzIdent().isNull());

	QVERIFY(pi2.pdzType() == type);
	QVERIFY(pi2.pdzRecip() == recip);
	QVERIFY(pi2.pdzPayer() == payer);
	QVERIFY(pi2.pdzExpire() == expire);
	QVERIFY(pi2.pdzCnt() == cnt);
	QVERIFY(pi2.odzIdent() == ident);

	QVERIFY(pi1.isNull());
	QVERIFY(!pi2.isNull());
	QVERIFY(pi1 != pi2);
}

void TestIsdsBoxInterface::pdzInfoRecCompare(void)
{
	Isds::PDZInfoRec pi1, pi2;
	const enum Isds::Type::PdzPaymentType type = Isds::Type::PPT_E;
	const QString recip("recip");
	const QString payer("payer");
	const QDateTime expire(QDate(2021, 5, 6), QTime(8, 30, 0), Qt::UTC);
	const qint64 cnt = 1;
	const QString ident("ident");

	pi1.setPdzType(type);
	pi1.setPdzRecip(recip);
	pi1.setPdzPayer(payer);
	pi1.setPdzExpire(expire);
	pi1.setPdzCnt(cnt);
	pi1.setOdzIdent(ident);
	pi2.setPdzType(type);
	pi2.setPdzRecip(recip);
	pi2.setPdzPayer(payer);
	pi2.setPdzExpire(expire);
	pi2.setPdzCnt(cnt);
	pi2.setOdzIdent(ident);

	QVERIFY(pi1 == pi2);

	pi2.setPdzType(Isds::Type::PPT_K);
	QVERIFY(pi1 != pi2);
	pi2.setPdzType(type);
	QVERIFY(pi1 == pi2);

	pi2.setPdzRecip("");
	QVERIFY(pi1 != pi2);
	pi2.setPdzRecip(recip);
	QVERIFY(pi1 == pi2);

	pi2.setPdzPayer("");
	QVERIFY(pi1 != pi2);
	pi2.setPdzPayer(payer);
	QVERIFY(pi1 == pi2);

	pi2.setPdzExpire(QDateTime::currentDateTime());
	QVERIFY(pi1 != pi2);
	pi2.setPdzExpire(expire);
	QVERIFY(pi1 == pi2);

	pi2.setPdzCnt(0);
	QVERIFY(pi1 != pi2);
	pi2.setPdzCnt(cnt);
	QVERIFY(pi1 == pi2);

	pi2.setOdzIdent("");
	QVERIFY(pi1 != pi2);
	pi2.setOdzIdent(ident);
	QVERIFY(pi1 == pi2);
}

void TestIsdsBoxInterface::pdzInfoRecConstructor(void)
{
	{
		Isds::PDZInfoRec pi;

		QVERIFY(pi.isNull());

		QVERIFY(pi.pdzType() == Isds::Type::PPT_UNKNOWN);
		QVERIFY(pi.pdzRecip().isNull());
		QVERIFY(pi.pdzPayer().isNull());
		QVERIFY(pi.pdzExpire().isNull());
		QVERIFY(pi.pdzCnt() == -1);
		QVERIFY(pi.odzIdent().isNull());
	}
}

void TestIsdsBoxInterface::pdzInfoRecCopy(void)
{
	Isds::PDZInfoRec pi1;
	const enum Isds::Type::PdzPaymentType type = Isds::Type::PPT_E;
	const QString recip("recip");
	const QString payer("payer");
	const QDateTime expire(QDate(2021, 5, 6), QTime(8, 30, 0), Qt::UTC);
	const qint64 cnt = 1;
	const QString ident("ident");

	pi1.setPdzType(type);
	pi1.setPdzRecip(recip);
	pi1.setPdzPayer(payer);
	pi1.setPdzExpire(expire);
	pi1.setPdzCnt(cnt);
	pi1.setOdzIdent(ident);

	/* Constructor. */
	{
		Isds::PDZInfoRec pi2(pi1);

		QVERIFY(!pi1.isNull());
		QVERIFY(!pi2.isNull());
		QVERIFY(pi1 == pi2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::PDZInfoRec pi3(::std::move(pi2));

		QVERIFY(pi2.isNull());
		QVERIFY(pi1 != pi2);

		QVERIFY(!pi1.isNull());
		QVERIFY(!pi3.isNull());
		QVERIFY(pi1 == pi3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}

	/* Assignment operator. */
	{
		Isds::PDZInfoRec pi2;

		QVERIFY(pi2.isNull());

		pi2 = pi1;

		QVERIFY(!pi1.isNull());
		QVERIFY(!pi2.isNull());
		QVERIFY(pi1 == pi2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::PDZInfoRec pi3;

		QVERIFY(pi3.isNull());

		pi3 = ::std::move(pi2);

		QVERIFY(pi2.isNull());
		QVERIFY(pi1 != pi2);

		QVERIFY(!pi1.isNull());
		QVERIFY(!pi3.isNull());
		QVERIFY(pi1 == pi3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface::pdzInfoRecSetValues(void)
{
//	const enum Isds::Type::PdzPaymentType cTtype = Isds::Type::PPT_E;
	const QString cRecip("recip");
	const QString cPayer("payer");
	const QDateTime cExpire(QDate(2021, 5, 6), QTime(8, 30, 0), Qt::UTC);
//	const qint64 cCnt = 1;
	const QString cIdent("ident");

	Isds::PDZInfoRec pi;
	QString recip(cRecip);
	QString payer(cPayer);
	QString ident(cIdent);
	QDateTime expire(cExpire);

	QVERIFY(pi.pdzRecip().isNull());
	pi.setPdzRecip(recip);
	QVERIFY(pi.pdzRecip() == cRecip);
	QVERIFY(recip == cRecip);
#ifdef Q_COMPILER_RVALUE_REFS
	pi.setPdzRecip("other_recip");
	QVERIFY(pi.pdzRecip() != cRecip);
	pi.setPdzRecip(::std::move(recip));
	QVERIFY(pi.pdzRecip() == cRecip);
	QVERIFY(recip != cRecip);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(pi.pdzPayer().isNull());
	pi.setPdzPayer(payer);
	QVERIFY(pi.pdzPayer() == cPayer);
	QVERIFY(payer == cPayer);
#ifdef Q_COMPILER_RVALUE_REFS
	pi.setPdzPayer("other_payer");
	QVERIFY(pi.pdzPayer() != cPayer);
	pi.setPdzPayer(::std::move(payer));
	QVERIFY(pi.pdzPayer() == cPayer);
	QVERIFY(payer != cPayer);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(pi.pdzExpire().isNull());
	pi.setPdzExpire(expire);
	QVERIFY(pi.pdzExpire() == cExpire);
	QVERIFY(expire == cExpire);
#ifdef Q_COMPILER_RVALUE_REFS
	pi.setPdzExpire(QDateTime::currentDateTime());
	QVERIFY(pi.pdzExpire() != cExpire);
	pi.setPdzExpire(::std::move(expire));
	QVERIFY(pi.pdzExpire() == cExpire);
	QVERIFY(expire != cExpire);
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(pi.odzIdent().isNull());
	pi.setOdzIdent(ident);
	QVERIFY(pi.odzIdent() == cIdent);
	QVERIFY(ident == cIdent);
#ifdef Q_COMPILER_RVALUE_REFS
	pi.setOdzIdent("other_ident");
	QVERIFY(pi.odzIdent() != cIdent);
	pi.setOdzIdent(::std::move(ident));
	QVERIFY(pi.odzIdent() == cIdent);
	QVERIFY(ident != cIdent);
#endif /* Q_COMPILER_RVALUE_REFS */
}

void TestIsdsBoxInterface::dtInfoOutput(void)
{
	Isds::DTInfoOutput di1, di2;
	const enum Isds::Type::DTType actType = Isds::Type::DTT_PREPAID;
	const qint64 actCap = 64;
	const QDate actFrom(2021, 1, 1);
	const QDate actTo(2021, 1, 31);
	const qint64 actCapUsed = 8;
	const enum Isds::Type::DTType futType = Isds::Type::DTT_SPECIAL_OFFER;
	const qint64 futCap = 128;
	const QDate futFrom(2021, 2, 1);
	const QDate futTo(2021, 2, 24);
	const enum Isds::Type::FutDTPaidState futState = Isds::Type::FDTPS_NOT_PAID_YET;

	/* Must be null. */
	QVERIFY(di1.actDTType() == Isds::Type::DTT_UNKNOWN);
	QVERIFY(di1.actDTCapacity() == -1);
	QVERIFY(di1.actDTFrom().isNull());
	QVERIFY(di1.actDTTo().isNull());
	QVERIFY(di1.actDTCapUsed() == -1);
	QVERIFY(di1.futDTType() == Isds::Type::DTT_UNKNOWN);
	QVERIFY(di1.futDTCapacity() == -1);
	QVERIFY(di1.futDTFrom().isNull());
	QVERIFY(di1.futDTTo().isNull());
	QVERIFY(di1.futDTPaid() == Isds::Type::FDTPS_UNKNOWN);
	QVERIFY(di2.actDTType() == Isds::Type::DTT_UNKNOWN);
	QVERIFY(di2.actDTCapacity() == -1);
	QVERIFY(di2.actDTFrom().isNull());
	QVERIFY(di2.actDTTo().isNull());
	QVERIFY(di2.actDTCapUsed() == -1);
	QVERIFY(di2.futDTType() == Isds::Type::DTT_UNKNOWN);
	QVERIFY(di2.futDTCapacity() == -1);
	QVERIFY(di2.futDTFrom().isNull());
	QVERIFY(di2.futDTTo().isNull());
	QVERIFY(di2.futDTPaid() == Isds::Type::FDTPS_UNKNOWN);

	QVERIFY(di1.isNull());
	QVERIFY(di2.isNull());
	QVERIFY(di1 == di2);

	/* Check whether values are not cross-linked. */
	di1.setActDTType(actType);
	di1.setActDTCapacity(actCap);
	di1.setActDTFrom(actFrom);
	di1.setActDTTo(actTo);
	di1.setActDTCapUsed(actCapUsed);
	di1.setFutDTType(futType);
	di1.setFutDTCapacity(futCap);
	di1.setFutDTFrom(futFrom);
	di1.setFutDTTo(futTo);
	di1.setFutDTPaid(futState);

	QVERIFY(di1.actDTType() == actType);
	QVERIFY(di1.actDTCapacity() == actCap);
	QVERIFY(di1.actDTFrom() == actFrom);
	QVERIFY(di1.actDTTo() == actTo);
	QVERIFY(di1.actDTCapUsed() == actCapUsed);
	QVERIFY(di1.futDTType() == futType);
	QVERIFY(di1.futDTCapacity() == futCap);
	QVERIFY(di1.futDTFrom() == futFrom);
	QVERIFY(di1.futDTTo() == futTo);
	QVERIFY(di1.futDTPaid() == futState);

	QVERIFY(!di1.isNull());
	QVERIFY(di2.isNull());
	QVERIFY(di1 != di2);

	/* Check value copying. */
	di2 = di1;

	QVERIFY(di2.actDTType() == actType);
	QVERIFY(di2.actDTCapacity() == actCap);
	QVERIFY(di2.actDTFrom() == actFrom);
	QVERIFY(di2.actDTTo() == actTo);
	QVERIFY(di2.actDTCapUsed() == actCapUsed);
	QVERIFY(di2.futDTType() == futType);
	QVERIFY(di2.futDTCapacity() == futCap);
	QVERIFY(di2.futDTFrom() == futFrom);
	QVERIFY(di2.futDTTo() == futTo);
	QVERIFY(di2.futDTPaid() == futState);

	QVERIFY(!di1.isNull());
	QVERIFY(!di2.isNull());
	QVERIFY(di1 == di2);

	/* Clear value. */
	di1 = Isds::DTInfoOutput();

	QVERIFY(di1.actDTType() == Isds::Type::DTT_UNKNOWN);
	QVERIFY(di1.actDTCapacity() == -1);
	QVERIFY(di1.actDTFrom().isNull());
	QVERIFY(di1.actDTTo().isNull());
	QVERIFY(di1.actDTCapUsed() == -1);
	QVERIFY(di1.futDTType() == Isds::Type::DTT_UNKNOWN);
	QVERIFY(di1.futDTCapacity() == -1);
	QVERIFY(di1.futDTFrom().isNull());
	QVERIFY(di1.futDTTo().isNull());
	QVERIFY(di1.futDTPaid() == Isds::Type::FDTPS_UNKNOWN);

	QVERIFY(di2.actDTType() == actType);
	QVERIFY(di2.actDTCapacity() == actCap);
	QVERIFY(di2.actDTFrom() == actFrom);
	QVERIFY(di2.actDTTo() == actTo);
	QVERIFY(di2.actDTCapUsed() == actCapUsed);
	QVERIFY(di2.futDTType() == futType);
	QVERIFY(di2.futDTCapacity() == futCap);
	QVERIFY(di2.futDTFrom() == futFrom);
	QVERIFY(di2.futDTTo() == futTo);
	QVERIFY(di2.futDTPaid() == futState);

	QVERIFY(di1.isNull());
	QVERIFY(!di2.isNull());
	QVERIFY(di1 != di2);
}

void TestIsdsBoxInterface::dtInfoOutputCompare(void)
{
	Isds::DTInfoOutput di1, di2;
	const enum Isds::Type::DTType actType = Isds::Type::DTT_PREPAID;
	const qint64 actCap = 64;
	const QDate actFrom(2021, 1, 1);
	const QDate actTo(2021, 1, 31);
	const qint64 actCapUsed = 8;
	const enum Isds::Type::DTType futType = Isds::Type::DTT_SPECIAL_OFFER;
	const qint64 futCap = 128;
	const QDate futFrom(2021, 2, 1);
	const QDate futTo(2021, 2, 24);
	const enum Isds::Type::FutDTPaidState futState = Isds::Type::FDTPS_NOT_PAID_YET;

	di1.setActDTType(actType);
	di1.setActDTCapacity(actCap);
	di1.setActDTFrom(actFrom);
	di1.setActDTTo(actTo);
	di1.setActDTCapUsed(actCapUsed);
	di1.setFutDTType(futType);
	di1.setFutDTCapacity(futCap);
	di1.setFutDTFrom(futFrom);
	di1.setFutDTTo(futTo);
	di1.setFutDTPaid(futState);
	di2.setActDTType(actType);
	di2.setActDTCapacity(actCap);
	di2.setActDTFrom(actFrom);
	di2.setActDTTo(actTo);
	di2.setActDTCapUsed(actCapUsed);
	di2.setFutDTType(futType);
	di2.setFutDTCapacity(futCap);
	di2.setFutDTFrom(futFrom);
	di2.setFutDTTo(futTo);
	di2.setFutDTPaid(futState);

	QVERIFY(di1 == di2);

	di2.setActDTType(Isds::Type::DTT_CONTRACTUAL);
	QVERIFY(di1 != di2);
	di2.setActDTType(actType);
	QVERIFY(di1 == di2);

	di2.setActDTCapacity(0);
	QVERIFY(di1 != di2);
	di2.setActDTCapacity(actCap);
	QVERIFY(di1 == di2);

	di2.setActDTFrom(QDate::currentDate());
	QVERIFY(di1 != di2);
	di2.setActDTFrom(actFrom);
	QVERIFY(di1 == di2);

	di2.setActDTTo(QDate::currentDate());
	QVERIFY(di1 != di2);
	di2.setActDTTo(actTo);
	QVERIFY(di1 == di2);

	di2.setActDTCapUsed(0);
	QVERIFY(di1 != di2);
	di2.setActDTCapUsed(actCapUsed);
	QVERIFY(di1 == di2);

	di2.setFutDTType(Isds::Type::DTT_TRIAL);
	QVERIFY(di1 != di2);
	di2.setFutDTType(futType);
	QVERIFY(di1 == di2);

	di2.setFutDTCapacity(0);
	QVERIFY(di1 != di2);
	di2.setFutDTCapacity(futCap);
	QVERIFY(di1 == di2);

	di2.setFutDTFrom(QDate::currentDate());
	QVERIFY(di1 != di2);
	di2.setFutDTFrom(futFrom);
	QVERIFY(di1 == di2);

	di2.setFutDTTo(QDate::currentDate());
	QVERIFY(di1 != di2);
	di2.setFutDTTo(futTo);
	QVERIFY(di1 == di2);

	di2.setFutDTPaid(Isds::Type::FDTPS_PAID_ALREADY);
	QVERIFY(di1 != di2);
	di2.setFutDTPaid(futState);
	QVERIFY(di1 == di2);
}

void TestIsdsBoxInterface::dtInfoOutputConstructor(void)
{
	{
		Isds::DTInfoOutput di;

		QVERIFY(di.isNull());

		QVERIFY(di.actDTType() == Isds::Type::DTT_UNKNOWN);
		QVERIFY(di.actDTCapacity() == -1);
		QVERIFY(di.actDTFrom().isNull());
		QVERIFY(di.actDTTo().isNull());
		QVERIFY(di.actDTCapUsed() == -1);
		QVERIFY(di.futDTType() == Isds::Type::DTT_UNKNOWN);
		QVERIFY(di.futDTCapacity() == -1);
		QVERIFY(di.futDTFrom().isNull());
		QVERIFY(di.futDTTo().isNull());
		QVERIFY(di.futDTPaid() == Isds::Type::FDTPS_UNKNOWN);
	}
}

void TestIsdsBoxInterface::dtInfoOutputCopy(void)
{
	Isds::DTInfoOutput di1;
	const enum Isds::Type::DTType actType = Isds::Type::DTT_PREPAID;
	const qint64 actCap = 64;
	const QDate actFrom(2021, 1, 1);
	const QDate actTo(2021, 1, 31);
	const qint64 actCapUsed = 8;
	const enum Isds::Type::DTType futType = Isds::Type::DTT_SPECIAL_OFFER;
	const qint64 futCap = 128;
	const QDate futFrom(2021, 2, 1);
	const QDate futTo(2021, 2, 24);
	const enum Isds::Type::FutDTPaidState futState = Isds::Type::FDTPS_NOT_PAID_YET;

	di1.setActDTType(actType);
	di1.setActDTCapacity(actCap);
	di1.setActDTFrom(actFrom);
	di1.setActDTTo(actTo);
	di1.setActDTCapUsed(actCapUsed);
	di1.setFutDTType(futType);
	di1.setFutDTCapacity(futCap);
	di1.setFutDTFrom(futFrom);
	di1.setFutDTTo(futTo);
	di1.setFutDTPaid(futState);

	/* Constructor. */
	{
		Isds::DTInfoOutput di2(di1);

		QVERIFY(!di1.isNull());
		QVERIFY(!di2.isNull());
		QVERIFY(di1 == di2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DTInfoOutput di3(::std::move(di2));

		QVERIFY(di2.isNull());
		QVERIFY(di1 != di2);

		QVERIFY(!di1.isNull());
		QVERIFY(!di3.isNull());
		QVERIFY(di1 == di3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}

	/* Assignment operator. */
	{
		Isds::DTInfoOutput di2;

		QVERIFY(di2.isNull());

		di2 = di1;

		QVERIFY(!di1.isNull());
		QVERIFY(!di2.isNull());
		QVERIFY(di1 == di2);

#ifdef Q_COMPILER_RVALUE_REFS
		/* Move. */
		Isds::DTInfoOutput di3;

		QVERIFY(di3.isNull());

		di3 = ::std::move(di2);

		QVERIFY(di2.isNull());
		QVERIFY(di1 != di2);

		QVERIFY(!di1.isNull());
		QVERIFY(!di3.isNull());
		QVERIFY(di1 == di3);
#endif /* Q_COMPILER_RVALUE_REFS */
	}
}

void TestIsdsBoxInterface::dtInfoOutputSetValues(void)
{
//	const enum Isds::Type::DTType cActType = Isds::Type::DTT_PREPAID;
//	const qint64 cActCap = 64;
	const QDate cActFrom(2021, 1, 1);
	const QDate cActTo(2021, 1, 31);
//	const qint64 cActCapUsed = 8;
//	const enum Isds::Type::DTType cFutType = Isds::Type::DTT_SPECIAL_OFFER;
//	const qint64 cFutCap = 128;
	const QDate cFutFrom(2021, 2, 1);
	const QDate cFutTo(2021, 2, 24);
//	const enum Isds::Type::FutDTPaidState cFutState = Isds::Type::FDTPS_NOT_PAID_YET;

	Isds::DTInfoOutput di;
	QDate actFrom(cActFrom);
	QDate actTo(cActTo);
	QDate futFrom(cFutFrom);
	QDate futTo(cFutTo);

	QVERIFY(di.actDTFrom().isNull());
	di.setActDTFrom(actFrom);
	QVERIFY(di.actDTFrom() == cActFrom);
	QVERIFY(actFrom == cActFrom);
#ifdef Q_COMPILER_RVALUE_REFS
	di.setActDTFrom(QDate::currentDate());
	QVERIFY(di.actDTFrom() != cActFrom);
	di.setActDTFrom(::std::move(actFrom));
	QVERIFY(di.actDTFrom() == cActFrom);
	//QVERIFY(actFrom != cActFrom); /* QDate doesn't provide move assignment. */
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(di.actDTTo().isNull());
	di.setActDTTo(actTo);
	QVERIFY(di.actDTTo() == cActTo);
	QVERIFY(actTo == cActTo);
#ifdef Q_COMPILER_RVALUE_REFS
	di.setActDTTo(QDate::currentDate());
	QVERIFY(di.actDTTo() != cActTo);
	di.setActDTTo(::std::move(actTo));
	QVERIFY(di.actDTTo() == cActTo);
	//QVERIFY(actTo != cActTo); /* QDate doesn't provide move assignment. */
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(di.futDTFrom().isNull());
	di.setFutDTFrom(futFrom);
	QVERIFY(di.futDTFrom() == cFutFrom);
	QVERIFY(futFrom == cFutFrom);
#ifdef Q_COMPILER_RVALUE_REFS
	di.setFutDTFrom(QDate::currentDate());
	QVERIFY(di.futDTFrom() != cFutFrom);
	di.setFutDTFrom(::std::move(futFrom));
	QVERIFY(di.futDTFrom() == cFutFrom);
	//QVERIFY(futFrom != cFutFrom); /* QDate doesn't provide move assignment. */
#endif /* Q_COMPILER_RVALUE_REFS */

	QVERIFY(di.futDTTo().isNull());
	di.setFutDTTo(futTo);
	QVERIFY(di.futDTTo() == cFutTo);
	QVERIFY(futTo == cFutTo);
#ifdef Q_COMPILER_RVALUE_REFS
	di.setFutDTTo(QDate::currentDate());
	QVERIFY(di.futDTTo() != cFutTo);
	di.setFutDTTo(::std::move(futTo));
	QVERIFY(di.futDTTo() == cFutTo);
	//QVERIFY(futTo != cFutTo); /* QDate doesn't provide move assignment. */
#endif /* Q_COMPILER_RVALUE_REFS */
}

QObject *newTestIsdsBoxInterface(void)
{
	return new (::std::nothrow) TestIsdsBoxInterface();
}

//QTEST_MAIN(TestIsdsBoxInterface)
#include "test_isds_box_interface.moc"
