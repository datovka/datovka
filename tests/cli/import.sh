#!/usr/bin/env bash

# Obtain location of source root.
src_root () {
	local SCRIPT_LOCATION=""
	local SYSTEM=$(uname -s)
	if [ ! "x${SYSTEM}" = "xDarwin" ]; then
		local SCRIPT=$(readlink -f "$0")
		SCRIPT_LOCATION=$(dirname $(readlink -f "$0"))
	else
		SCRIPT_LOCATION=$(cd "$(dirname "$0")"; pwd)
	fi

	echo $(cd "$(dirname "${SCRIPT_LOCATION}")"; cd ..; pwd)
}

SRC_ROOT=$(src_root)
cd "${SRC_ROOT}"

SCRIPT_PATH="${SRC_ROOT}/tests/cli"

. "${SCRIPT_PATH}/helper.sh" # Contains HAVE_ERROR variable.


. "${SRC_ROOT}/untracked/logins.sh"

APP_BINARY_PATH="$1"
if [ ! -e "${APP_BINARY_PATH}" ]; then
	echo_error "ERROR: Cannot locate tested binary."
	exit 1
fi
ATTACH_LOAD_PATH="$2"
if [ "x${ATTACH_LOAD_PATH}" = "x" -o ! -d "${ATTACH_LOAD_PATH}" ]; then
	echo_error "ERROR: Cannot locate directory where to load zfo from."
	exit 1
fi

CMDARGS="${CMDARGS} -D"
CMDARGS="${CMDARGS} --conf-subdir .dsgui"
CMDARGS="${CMDARGS} --debug-verbosity 2"
CMDARGS="${CMDARGS} --log-verbosity 2"

echo ""
echo "***********************************************************************"
echo "* IMPORT MSG: only message zfo"
echo "***********************************************************************"
for login in $USERNAME_SEND; do
	"${APP_BINARY_PATH}" ${CMDARGS} \
		--login "username='$login'" \
		--import-msg "importFile='${ATTACH_LOAD_PATH}/DDZ-11156640.zfo',verifyIsds='yes'" \
		2>/dev/null
	if [ 0 != $? ]; then
		echo_error "ExportMsgs: $login - ERROR"
		echo ""
	else
		echo_success "ExportMsgs: $login - OK"
		echo ""
	fi
done

echo ""
echo "***********************************************************************"
echo "* IMPORT MSG: only delivery info"
echo "***********************************************************************"
for login in $USERNAME_SEND; do
	"${APP_BINARY_PATH}" ${CMDARGS} \
		--login "username='$login'" \
		--import-msg "importFile='${ATTACH_LOAD_PATH}/DD-11156640.zfo',verifyIsds='no'" \
		2>/dev/null
	if [ 0 != $? ]; then
		echo_error "ExportMsgs: $login - ERROR"
		echo ""
	else
		echo_success "ExportMsgs: $login - OK"
		echo ""
	fi
done

echo ""
echo "***********************************************************************"
echo "* IMPORT MSGS: from dir"
echo "***********************************************************************"
for login in $USERNAME_SEND; do
	"${APP_BINARY_PATH}" ${CMDARGS} \
		--login "username='$login'" \
		--import-msgs "dmType='sent',importDir='${ATTACH_LOAD_PATH}',includeSubDirs='yes'" \
		2>/dev/null
	if [ 0 != $? ]; then
		echo_error "ExportMsgs: $login - ERROR"
		echo ""
	else
		echo_success "ExportMsgs: $login - OK"
		echo ""
	fi
done

if [ "x${HAVE_ERROR}" = "xfalse" ]; then
	echo ""
	echo_success "-----------------------------------------------------------------------"
	echo_success "SUCCESS: All import tests finished as expected."
	echo_success "-----------------------------------------------------------------------"
	echo ""
	exit 0
else
	echo ""
	echo_error "-----------------------------------------------------------------------"
	echo_error "FAILURE: Some import tests have failed!"
	echo_error "-----------------------------------------------------------------------"
	echo ""
	exit 1
fi
