#!/usr/bin/env bash

# Obtain location of source root.
src_root () {
	local SCRIPT_LOCATION=""
	local SYSTEM=$(uname -s)
	if [ ! "x${SYSTEM}" = "xDarwin" ]; then
		local SCRIPT=$(readlink -f "$0")
		SCRIPT_LOCATION=$(dirname $(readlink -f "$0"))
	else
		SCRIPT_LOCATION=$(cd "$(dirname "$0")"; pwd)
	fi

	echo $(cd "$(dirname "${SCRIPT_LOCATION}")"; cd ..; pwd)
}

SRC_ROOT=$(src_root)
cd "${SRC_ROOT}"

SCRIPT_PATH="${SRC_ROOT}/tests/cli"

. "${SCRIPT_PATH}/helper.sh" # Contains HAVE_ERROR variable.


. "${SRC_ROOT}/untracked/logins.sh"

APP_BINARY_PATH="$1"
if [ ! -e "${APP_BINARY_PATH}" ]; then
	echo_error "ERROR: Cannot locate tested binary."
	exit 1
fi
EXPORT_TMP_DIR="$2"
if [ "x${EXPORT_TMP_DIR}" = "x" -o ! -d "${EXPORT_TMP_DIR}" ]; then
	echo_error "ERROR: Cannot locate directory for export."
	exit 1
fi

CMDARGS="${CMDARGS} -D"
CMDARGS="${CMDARGS} --conf-subdir .dsgui"
CMDARGS="${CMDARGS} --debug-verbosity 2"
CMDARGS="${CMDARGS} --log-verbosity 2"

echo ""
echo "***********************************************************************"
echo "* EXPORT MSGS: only received without any paramters."
echo "***********************************************************************"
for login in $USERNAME_SEND; do
	"${APP_BINARY_PATH}" ${CMDARGS} \
		--login "username='$login'" \
		--export-msgs "dmType='received',exportPath='${EXPORT_TMP_DIR}'" \
		2>/dev/null
	if [ 0 != $? ]; then
		echo_error "ExportMsgs: $login - ERROR"
		echo ""
	else
		echo_success "ExportMsgs: $login - OK"
		echo ""
	fi
done

echo ""
echo "***********************************************************************"
echo "* EXPORT MSGS: only sent, export all files."
echo "***********************************************************************"
for login in $USERNAME_SEND; do
	"${APP_BINARY_PATH}" ${CMDARGS} \
		--login "username='$login'" \
		--export-msgs "dmType='sent',exportPath='${EXPORT_TMP_DIR}',exportMsgType='all',exportDelInfo='all'" \
		2>/dev/null
	if [ 0 != $? ]; then
		echo_error "ExportMsgs: $login - ERROR"
		echo ""
	else
		echo_success "ExportMsgs: $login - OK"
		echo ""
	fi
done

echo ""
echo "***********************************************************************"
echo "* EXPORT MSGS: only received, only msg zfo, no delivery info, from date."
echo "***********************************************************************"
for login in $USERNAME_SEND; do
	"${APP_BINARY_PATH}" ${CMDARGS} \
		--login "username='$login'" \
		--export-msgs "dmType='received',exportPath='${EXPORT_TMP_DIR}',exportMsgType='zfo',exportDelInfo='no',fromDate='2024-01-01'" \
		2>/dev/null
	if [ 0 != $? ]; then
		echo_error "ExportMsgs: $login - ERROR"
		echo ""
	else
		echo_success "ExportMsgs: $login - OK"
		echo ""
	fi
done

echo ""
echo "***********************************************************************"
echo "* EXPORT MSGS: only received, no msg, delivery info, from date, to date."
echo "***********************************************************************"
for login in $USERNAME_SEND; do
	"${APP_BINARY_PATH}" ${CMDARGS} \
		--login "username='$login'" \
		--export-msgs "dmType='received',exportPath='${EXPORT_TMP_DIR}',exportMsgType='no',exportDelInfo='all',fromDate='2024-01-01',fromDate='2024-03-01'" \
		2>/dev/null
	if [ 0 != $? ]; then
		echo_error "ExportMsgs: $login - ERROR"
		echo ""
	else
		echo_success "ExportMsgs: $login - OK"
		echo ""
	fi
done


if [ "x${HAVE_ERROR}" = "xfalse" ]; then
	echo ""
	echo_success "-----------------------------------------------------------------------"
	echo_success "SUCCESS: All export tests finished as expected."
	echo_success "-----------------------------------------------------------------------"
	echo ""
	exit 0
else
	echo ""
	echo_error "-----------------------------------------------------------------------"
	echo_error "FAILURE: Some export tests have failed!"
	echo_error "-----------------------------------------------------------------------"
	echo ""
	exit 1
fi
