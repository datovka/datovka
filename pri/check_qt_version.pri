
defineTest(sufficientQtVersion) {
	# Required Qt versions
	required_major = $$1
	required_minor = $$2
	advised_minor = $$3
	advised_patch = $$4

	lessThan(QT_MAJOR_VERSION, $${required_major}) {
		error(Qt version $${required_major}.$${required_minor} is required.)
		return(false)
	}

	isEqual(QT_MAJOR_VERSION, $${required_major}) {
		lessThan(QT_MINOR_VERSION, $${required_minor}) {
			error(Qt version $${required_major}.$${required_minor} is required.)
			return(false)
		}

		lessThan(QT_MINOR_VERSION, $${advised_minor}) {
			warning(Qt version at least $${required_major}.$${advised_minor}.$${advised_patch} is suggested.)
		} else {
			isEqual(QT_MINOR_VERSION, $${advised_minor}) {
				lessThan(QT_PATCH_VERSION, $${advised_patch}) {
					warning(Qt version at least $${required_major}.$${advised_minor}.$${advised_patch} is suggested.)
				}
			}
		}
	} else {
		greaterThan(QT_MAJOR_VERSION, $${required_major}) {
			isEqual(QT_MAJOR_VERSION, 6) {
				isEqual(QT_MINOR_VERSION, 0) {
					# Qt-6.0 complains about unknown std::numeric_limits whenever QString and others are included on some compilers.
					# It's Qt that should be be fixed here - not this application.
					warning(The current Qt version $${QT_MAJOR_VERSION}.$${QT_MINOR_VERSION} is expected to cause compilation errors.)
				} else {
				}
			} else {
				warning(The current Qt version $${QT_MAJOR_VERSION}.$${QT_MINOR_VERSION} may not work.)
			}
		}
	}
	return(true)
}

# Behaves similarly to versionAtLeast().
defineTest(atLeastQtVersion) {
	required_major = $$1
	required_minor = $$2
	required_patch = $$3

	lessThan(QT_MAJOR_VERSION, $${required_major}) {
		return(false)
	}

	isEqual(QT_MAJOR_VERSION, $${required_major}) {
		lessThan(QT_MINOR_VERSION, $${required_minor}) {
			return(false)
		}

		isEqual(QT_MINOR_VERSION, $${required_minor}) {
			lessThan(QT_PATCH_VERSION, $${required_patch}) {
				return(false)
			}
		}
	}

	return(true)
}
