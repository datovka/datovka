
include(check_qt_version.pri)

# OpenSSL-1.1.1 required since Qt-5.12.4.
# https://www.qt.io/blog/2019/06/17/qt-5-12-4-released-support-openssl-1-1-1
# https://mta.openssl.org/pipermail/openssl-dev/2016-August/008351.html

atLeastQtVersion(5, 12, 4) {
	message(libcrypto)
} else {
	message(libeay32)
}
