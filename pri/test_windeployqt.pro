
include(check_qt_version.pri)

# Qt-5.14.0 introduced changes which affect the behaviour of windeployqt.exe.
# https://forum.qt.io/topic/109779/windeployqt-exe-comes-with-qt-5-14-not-copy-the-dlls-to-the-app-directory
# https://bugreports.qt.io/browse/QTBUG-80763
# In Qt-5.14.0 windeployqt.exe seems to be unusable.
# In Qt-5.14.1 it works but it is better not to provide the debug/release parameter.

atLeastQtVersion(5, 14, 0) {
	message(omit_debug_or_release)
} else {
	message(provide_debug_or_release)
}
