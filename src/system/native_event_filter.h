/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractNativeEventFilter>

/*!
 * @brief Listens to native events.
 */
class NativeEventFilter : public QAbstractNativeEventFilter {
public:
	/*!
	 * @brief Called for every native event.
	 *
	 * @param[in] eventType Native event type. Depends on the chosen platform plug-in.
	 * @param[in,out] message Platform dependent event description.
	 * @param[in,out] result Only used on Windows.
	 * @retturn True to filter the event i.e. to stop further processing.
	 */
	virtual
	bool nativeEventFilter(const QByteArray &eventType, void *message,
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
	    qintptr
#else /* < Qt-6.0 */
	    long
#endif /* >= Qt-6.0 */
	    *result) Q_DECL_OVERRIDE;
};
