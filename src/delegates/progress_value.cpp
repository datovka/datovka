/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QApplication>
#include <QStyleOptionViewItem>
#include <QPainter>

#include "src/delegates/progress_value.h"

ProgressValue::ProgressValue(void)
    : minimum(0),
    maximum(0),
    value(0),
    text()
{
}

ProgressValue::ProgressValue(const ProgressValue &other)
    : minimum(other.minimum),
    maximum(other.maximum),
    value(other.value),
    text(other.text)
{
}

#ifdef Q_COMPILER_RVALUE_REFS
ProgressValue::ProgressValue(ProgressValue &&other) Q_DECL_NOEXCEPT
    : minimum(other.minimum),
    maximum(other.maximum),
    value(other.value),
    text(::std::move(other.text))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

ProgressValue::ProgressValue(int min, int max, int val, const QString &txt)
    : minimum(min),
    maximum(max),
    value(val),
    text(txt)
{
}

#ifdef Q_COMPILER_RVALUE_REFS
ProgressValue::ProgressValue(int min, int max, int val, QString &&txt)
    : minimum(min),
    maximum(max),
    value(val),
    text(::std::move(txt))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

ProgressValue &ProgressValue::operator=(const ProgressValue &other) Q_DECL_NOTHROW
{
	minimum = other.minimum;
	maximum = other.maximum;
	value = other.value;
	text = other.text;
	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
ProgressValue &ProgressValue::operator=(ProgressValue &&other) Q_DECL_NOTHROW
{
	minimum = other.minimum;
	maximum = other.maximum;
	value = other.value;
	text = ::std::move(other.text);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool ProgressValue::operator==(const ProgressValue &other) const
{
	return (minimum == other.minimum) &&
	    (maximum == other.maximum) &&
	    (value == other.value) &&
	    (text == other.text);
}

bool ProgressValue::operator!=(const ProgressValue &other) const
{
	return !operator==(other);
}

/*
 * See https://forum.qt.io/topic/136097/qprogressbar-inside-qtableview-column-size-problem .
 */

int ProgressValue::paint(QPainter *painter, const QStyleOptionViewItem &option) const
{
	if (Q_UNLIKELY(Q_NULLPTR == painter)) {
		Q_ASSERT(0);
		return 0;
	}

	painter->save();

	const QSize hint = sizeHint(option);

	int width = hint.width();

#if defined(Q_OS_MACOS)
#  warning "Progress bars in table cells seem to be broken on macOS."
	{
		const int progress = value;
		QStyleOptionViewItem opt = option;
		if (maximum > 0) {
			opt.text = QString("(%1 %) %2")
			    .arg((((qint64)100) * ((qint64)progress)) / ((qint64)maximum))
			    .arg(text);
		}
		QStyle *style = (Q_NULLPTR != opt.widget) ? opt.widget->style() : QApplication::style();
		style->drawControl(QStyle::CE_ItemViewItem, &opt, painter, opt.widget);
	}
#else /* !defined(Q_OS_MACOS) */
	{
		const int progress = value;
		QStyleOptionProgressBar progressBarOption;
		progressBarOption.rect = option.rect;
		progressBarOption.direction = option.direction;
		progressBarOption.fontMetrics = option.fontMetrics;
		progressBarOption.palette = option.palette;
		progressBarOption.state = option.state | QStyle::State_Horizontal;
		progressBarOption.styleObject = option.styleObject;
		progressBarOption.minimum = minimum;
		progressBarOption.maximum = maximum;
		progressBarOption.progress = progress;
		progressBarOption.text = text;
		progressBarOption.textVisible = true;
		progressBarOption.textAlignment = option.displayAlignment;
		((Q_NULLPTR != option.widget) ? option.widget->style() : QApplication::style())
		    ->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter);
	}
#endif /* !defined(Q_OS_MACOS) */

	painter->restore();

	return width;
}

QSize ProgressValue::sizeHint(const QStyleOptionViewItem &option) const
{
	const QFont &font(option.font);

	int height = QFontMetrics(font).height();

	/*
	 * Stopped using deprecated QFontMetrics::width().
	 * https://kdepepo.wordpress.com/2019/08/05/about-deprecation-of-qfontmetricswidth/
	 * https://doc.qt.io/qt-5/qfontmetrics-obsolete.html#width
	 */
	int width = QFontMetrics(font).boundingRect(text).width();

	return QSize(width, height);
}
