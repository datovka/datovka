/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QMetaType>
#include <QSize>
#include <QString>
#include <QStyleOptionViewItem>

#include "src/json/tag_entry.h"

namespace Json {
	class Int64StringList; /* Forward declaration. */
}
class QPainter; /* Forward declaration. */

/*!
 * @brief Describes tag information.
 */
class TagItem : public Json::TagEntry {

public:
	/*!
	 * @brief Constructor of invalid tag item.
	 */
	TagItem(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] entry Tag entry to construct item from.
	 */
	explicit TagItem(const Json::TagEntry &entry);

	/*!
	 * @brief Paint tag rectangle.
	 *
	 * @param[in,out] painter Painter.
	 * @param[in]     option  Drawing options.
	 * @return Width of the drawn rectangle (including margin);
	 */
	int paint(QPainter *painter, const QStyleOptionViewItem &option) const;

	/*!
	 * @brief Gives size hint for the tag rectangle.
	 *
	 * @param[in] option Drawing options.
	 * @return Size of the element.
	 */
	QSize sizeHint(const QStyleOptionViewItem &option) const;

	/*!
	 * @brief Adjust foreground colour according to the supplied label
	 *     colour.
	 *
	 * @param[in] fgColour  Foreground colour.
	 * @param[in] tagColour Tag rectangle colour.
	 * @return Colour adjusted to the background colour.
	 */
	static
	QColor adjustForegroundColour(const QColor &fgColour,
	    const QColor &tagColour);
};

class TagItemList : public QList<TagItem> {

public:
	/*!
	 * @brief Constructor.
	 */
	TagItemList(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] tagList List of tag entries.
	 */
	TagItemList(const Json::TagEntryList &tagList);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] tagList List of tag entries.
	 */
	TagItemList(const QList<TagItem> &tagList);

	/*!
	 * @brief Paint all list elements.
	 *
	 * @param[in,out] painter Painter.
	 * @param[in]     option  Drawing options.
	 */
	void paint(QPainter *painter, const QStyleOptionViewItem &option) const;

	/*!
	 * @brief Gives size hint for the tag rectangles.
	 *
	 * @param[in] option Drawing options.
	 * @return Size of the element.
	 */
	QSize sizeHint(const QStyleOptionViewItem &option) const;

	/*!
	 * @brief Update tag with matching identifier to new content.
	 *
	 * @param[in] entry New tag content.
	 * @return True if tag with matching identifier found and updated.
	 */
	bool updateTag(const Json::TagEntry &entry);

	/*!
	 * @brief Delete tag entry if an entry with the supplied identifier
	 *     is held.
	 *
	 * @param[in] ids Identifier of the tags to be deleted.
	 * @return True if some tags with matching identifiers found and deleted.
	 */
	bool deleteTags(const Json::Int64StringList &ids);

	/*!
	 * @brief Performs a locale-aware sorting of the tag list according to
	 *     tag names.
	 */
	void sortNames(void);

	/*!
	 * @brief Behaves similarly to ::std::upper_bound according to entry name.
	 *
	 * @param[in] item Tag item to serve for value comparison.
	 * @return Iterator pointing to the first element that is greater than
	 *     supplied value, or last if no such element is found.
	 */
	iterator upperBoundName(const TagItem &item);

	/*!
	 * @brief Returns the index of fires occurrence of a tag with supplied
	 *     identifier.
	 *
	 * @param[in] tagId Sought tag identifier.
	 * @return Non-negative position if found.
	 */
	int indexOfId(qint64 tagId) const;

	/*!
	 * @brief Check whether list contains a tag with supplied identifier.
	 *
	 * @param[in] tagId Sought tag identifier.
	 * @return True if list contains entry with same identifier.
	 */
	bool containsId(qint64 tagId) const;
};

Q_DECLARE_METATYPE(TagItem)
Q_DECLARE_METATYPE(TagItemList)
