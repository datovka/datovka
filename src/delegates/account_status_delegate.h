/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QStyledItemDelegate>

/*!
 * @brief Draws account status entries.
 */
class AccountStatusDelegate : public QStyledItemDelegate {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @brief[in] parent Parent widget.
	 */
	explicit AccountStatusDelegate(QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Paint the item.
	 *
	 * @param[in,out] painter Painter to be used.
	 * @param[in]     option  Drawing parameters.
	 * @param[in]     index   Specifies the item to be drawn.
	 */
	virtual
	void paint(QPainter *painter, const QStyleOptionViewItem &option,
	    const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the size needed to display the data.
	 *
	 * @param[in] option Drawing parameters.
	 * @param[in] index  Specifies the item to be drawn.
	 * @return Size needed to display the data.
	 */
	virtual
	QSize sizeHint(const QStyleOptionViewItem &option,
	    const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Create the editor item.
	 *
	 * @param[in] parent Parent widget.
	 * @param[in] option Editor parameters.
	 * @param[in] index Index of the cell where the editor is being displayed.
	 * @return Pointer to editor widget.
	 */
	virtual
	QWidget *createEditor(QWidget *parent,
	    const QStyleOptionViewItem &option,
	    const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Set editor data.
	 *
	 * @param[in] editor Editor widget.
	 * @param[in] index Index of the cell where the editor is being displayed.
	 */
	virtual
	void setEditorData(QWidget *editor,
	    const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Set editor geometry relative to view item.
	 *
	 * @param[in] editor Editor widget.
	 * @param[in] option Editor parameters.
	 * @param[in] index Index of the cell where the editor is being displayed.
	 */
	virtual
	void updateEditorGeometry(QWidget *editor,
	    const QStyleOptionViewItem &option,
	    const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Set selected data from editor in the model.
	 *
	 * @param[in] editor Editor widget.
	 * @param[in] model Model to be modified.
	 * @param[in] index Index of the cell where the editor is being
	 *                  displayed and whose content is to be set.
	 */
	virtual
	void setModelData(QWidget *editor, QAbstractItemModel *model,
	    const QModelIndex &index) const Q_DECL_OVERRIDE;

private Q_SLOTS:
	/*!
	 * @brief Emits commitData() when button pressed.
	 */
	void onButtonClicked(void);
};
