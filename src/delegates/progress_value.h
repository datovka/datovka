/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMetaType>
#include <QString>

class QPainter; /* Forward declaration. */
class QStyleOptionViewItem; /* Forward declaration. */

/*!
 * @brief Describes progress information.
 */
class ProgressValue {
public:
	/*!
	 * @brief Constructor.
	 */
	ProgressValue(void);

	ProgressValue(const ProgressValue &other);
#ifdef Q_COMPILER_RVALUE_REFS
	ProgressValue(ProgressValue &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

	ProgressValue(int min, int max, int val, const QString &txt = QString());
#ifdef Q_COMPILER_RVALUE_REFS
	ProgressValue(int min, int max, int val, QString &&txt);
#endif /* Q_COMPILER_RVALUE_REFS */

	ProgressValue &operator=(const ProgressValue &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	ProgressValue &operator=(ProgressValue &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const ProgressValue &other) const;
	bool operator!=(const ProgressValue &other) const;

	/*!
	 * @brief Paint progress rectangle.
	 *
	 * @param[in,out] painter Painter.
	 * @param[in]     option  Drawing options.
	 * @return Width of the drawn rectangle (including margin);
	 */
	int paint(QPainter *painter, const QStyleOptionViewItem &option) const;

	/*!
	 * @brief Gives size hint for the progress rectangle.
	 *
	 * @param[in] option Drawing options.
	 * @return Size of the element.
	 */
	QSize sizeHint(const QStyleOptionViewItem &option) const;

	int minimum; /*!< Minimum progress value. */
	int maximum; /*!< Maximum progress value. */
	int value; /*!< Current progress value. */
	QString text; /*!< Progress-bar text. */
};

Q_DECLARE_METATYPE(ProgressValue)
