/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */
#include <QMetaType>
#include <QString>

class QPainter; /* Forward declaration. */
class QStyleOptionViewItem; /* Forward declaration. */

#define ACCOUNT_STATUS_MARGIN 5 /* Content margin. */

/*!
 * @brief Describes account status information.
 */
class AccountStatusValue {
	Q_DECLARE_TR_FUNCTIONS(AccountStatusValue)

public:
	/*!
	 * @brief Constructor.
	 */
	AccountStatusValue(void);

	AccountStatusValue(const AccountStatusValue &other);
#ifdef Q_COMPILER_RVALUE_REFS
	AccountStatusValue(AccountStatusValue &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

	AccountStatusValue(const QString &aName, const QString &uName,
	    int lMethod, bool sActive);
#ifdef Q_COMPILER_RVALUE_REFS
	AccountStatusValue(QString &&aName, QString &&uName, int lMethod,
	    bool sActive);
#endif /* Q_COMPILER_RVALUE_REFS */

	AccountStatusValue &operator=(const AccountStatusValue &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	AccountStatusValue &operator=(AccountStatusValue &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const AccountStatusValue &other) const;
	bool operator!=(const AccountStatusValue &other) const;

	/*!
	 * @brief Paint progress rectangle.
	 *
	 * @param[in,out] painter Painter.
	 * @param[in]     option  Drawing options.
	 * @return Width of the drawn rectangle (including margin);
	 */
	int paint(QPainter *painter, const QStyleOptionViewItem &option) const;

	/*!
	 * @brief Gives size hint for the progress rectangle.
	 *
	 * @param[in] option Drawing options.
	 * @return Size of the element.
	 */
	QSize sizeHint(const QStyleOptionViewItem &option) const;

	QString acntName;
	QString username;
	int loginMethod; /* enum AcntSettings::LoginMethod */
	bool sessionActive;
};

Q_DECLARE_METATYPE(AccountStatusValue)
