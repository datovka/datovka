/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QStyleOptionViewItem>
#include <QVector>

class QTableView; /* Forward declaration. */
class QSplitter; /* Forward declaration. */

/*!
 * @brief Defines some basic dimensions.
 */
class Dimensions {

public:
	/*!
	 * @brief Returns margin as function of font dimensions.
	 *
	 * @param[in] option Style options.
	 * @return Margin width in pixels.
	 */
	static
	int margin(const QStyleOptionViewItem &option);

	/*!
	 * @brief Returns padding as function of supplied dimensions.
	 *
	 * @param[in] height Font height.
	 * @return Padding width in pixels.
	 */
	static
	int padding(int height);

	/*!
	 * @brief Return default line height to table views and widgets.
	 *
	 * @param[in] option Style options.
	 * @return Line height pixels.
	 */
	static
	int tableLineHeight(const QStyleOptionViewItem &option);

	/*!
	 * @brief Return size of available space on desktop.
	 *
	 * @param[in] widget Widget used to determine the screen.
	 * @note This excludes some control elements (e.g. dock, menu bar,
	 *     task bar). This may not work when using X11.
	 */
	static
	QRect availableScreenSize(const QWidget *widget);

	/*!
	 * @brief Return screen size.
	 *
	 * @param[in] widget Widget used to determine the screen.
	 * @return Screen size.
	 */
	static
	QRect screenSize(const QWidget *widget);

	/*!
	 * @brief Returns size of dialogue specified as the ratio of the
	 *     detected font height.
	 *
	 * @param[in] widget Widget to obtain font metrics from.
	 * @param[in] wr Width ratio relative to font height.
	 * @param[in] hr Height ratio relative to font height.
	 * @return Window size.
	 */
	static
	QSize windowSize(const QWidget *widget, qreal wr, qreal hr);

	/*!
	 * @brief Returns dimensions of window specified as the ratio of the
	 *     detected font height.
	 *
	 * @note If \a wr of \a hr is negative, the window size will be
	 *     computed to fill available space.
	 *
	 * @param[in] widget Widget to obtain font metrics from.
	 * @param[in] wr Width ratio relative to font height.
	 * @param[in] hr Height ratio relative to font height.
	 * @return Window dimensions.
	 */
	static
	QRect windowDimensions(const QWidget *widget, qreal wr, qreal hr);

	/*!
	 * @brief Returns window geometry to fit the screen..
	 *
	 * @param[in] widget Window widget.
	 * @return Widget geometry to fit into the screen.
	 */
	static
	QRect windowOnScreenDimensions(const QWidget *widget);

	/*!
	 * @brief Returns preferred size of dialogue.
	 *     If the screen is too small or the screen cannot be determined
	 *     or \a newSize is invalid then \a dfltSize is returned.
	 *
	 * @param[in] widget Window widget.
	 * @param[in] newSize New size.
	 * @param[in] dfltSize Default/current size.
	 * @return Dialogue window size.
	 */
	static
	QSize dialogueSize(const QWidget *widget, const QSize &newSize,
	    const QSize &dfltSize);

	/*!
	 * @brief Returns table column widths as ration to the table width.
	 *
	 * @param[in] table Table view.
	 * @return Respective column to table width ratios.
	 */
	static
	QVector<double> relativeTableColumnWidths(const QTableView *table);

	/*!
	 * @brief Set table columns width.
	 *
	 * @param[in,out] table Table view.
	 * @param[in] relativeWidths Relative column widths.
	 */
	static
	void setRelativeTableColumnWidths(QTableView *table,
	    const QVector<double> &relativeWidths);

	/*!
	 * @brief Returns table column widths.
	 *
	 * @param[in] table Table view.
	 * @return Table column widths.
	 */
	static
	QVector<qint64> tableColumnWidths(const QTableView *table);

	/*!
	 * @brief Set table columns width.
	 *
	 * @param[in,out] table Table view.
	 * @param[in] widths Column widths.
	 */
	static
	void setTableColumnWidths(QTableView *table, const QVector<qint64> &widths);

	/*!
	 * @brief Get splitter size.
	 *
	 * @param[in,out] splitter Splitter.
	 */
	static
	int splitterSize(const QSplitter *splitter);

	/*!
	 * @brief Set splitter size.
	 *
	 * @param[in,out] splitter Splitter.
	 * @param[in] paneSize First pane size.
	 */
	static
	void setSplitterSize(QSplitter *splitter, int paneSize);

	/*!
	 * @brief Returns table colum sort order.
	 *
	 * @param[in] table Table view.
	 * @return Column index and sort order value.
	 */
	static
	QVector<int> tableColumnSortOrder(const QTableView *table);

	/*!
	 * @brief Set table column sort order.
	 *
	 * @param[in,out] table Table view.
	 * @param[in] columnAndOrder Index and sort order value.
	 */
	static
	void setTableColumnSortOrder(QTableView *table,
	    const QVector<int> &columnAndOrder);

private:
	/*!
	 * @brief Constructor.
	 *
	 * @note Prohibit any class instance.
	 */
	Dimensions(void);

	static
	const qreal m_margin; /*!< Text margin as ratio of text height. */

	static
	const qreal m_padding; /*!< Text padding as ratio of text height. */

	static
	const qreal m_lineHeight; /*!< Height of line as ration of text height. */

	static
	const qreal m_screenRatio; /*!< Ratio of screen dimensions to use. */

	static
	const QRect m_dfltWinRect; /*!< Default window rectangle. */
};
