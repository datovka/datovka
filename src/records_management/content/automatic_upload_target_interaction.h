/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

namespace Isds {
	class Envelope; /* Forward declaration. */
}

namespace RecMgmt {
	class AutomaticUploadTarget; /* Forward declaration. */
}

namespace RecMgmt {

	/*!
	 * @brief Check for matching upload hierarchy location for a received message.
	 *
	 * @note Reference numbers and identifiers (i.e. file marks) are treated to be
	 *     of priority (i.e. both are treated as case identifiers).
	 *
	 * @param[in]  env Message envelope.
	 * @param[in]  rmTgts Record management targets.
	 * @param[out] ambiguous Set to true, if ambiguous upload hierarchy target detected.
	 * @return Non-empty string with hierarchy location identifier if unique match found.
	 */
	QString uniqueUploadReceived(const Isds::Envelope &env,
	    const RecMgmt::AutomaticUploadTarget &rmTgts,
	    bool *ambiguous = Q_NULLPTR);

}
