/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QtGlobal> /* QT_VERSION_CHECK */

#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */
#include <QString>
#include <QStringList>

namespace RecMgmt {

	class UploadHierarchyResp; /* Forward declaration. */

	class AutomaticUploadTargetPrivate;
	/*!
	 * @class Encapsulates automatic upload target descriptions.
	 */
	class AutomaticUploadTarget {
		Q_DECLARE_PRIVATE(AutomaticUploadTarget)

	public:
		AutomaticUploadTarget(void);
		AutomaticUploadTarget(const AutomaticUploadTarget &other);
#ifdef Q_COMPILER_RVALUE_REFS
		AutomaticUploadTarget(AutomaticUploadTarget &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~AutomaticUploadTarget(void);

		AutomaticUploadTarget &operator=(const AutomaticUploadTarget &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		AutomaticUploadTarget &operator=(AutomaticUploadTarget &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		friend void swap(AutomaticUploadTarget &first, AutomaticUploadTarget &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/* case identifier to upload hierarchy identifier */
		bool haveCaseId(const QString &cId) const;
		QString hierarchyForCaseId(const QString &cId) const;
		void setHierarchyForCaseId(const QString &cId, const QString &hId);
		/* court case identifier to upload hierarchy identifier */
		bool haveCourtCaseId(const QString &ccId) const;
		QString hierarchyForCourtCaseId(const QString &ccId) const;
		void setHierarchyForCourtCaseId(const QString &ccId, const QString &hId);
		/* ambiguous case identifiers */
		bool haveAmbiguousCaseId(const QString &cId) const;
		void insertAmbiguousCaseId(const QString &cId);
		/* ambiguous court case identifiers. */
		bool haveAmbiguousCourtCaseId(const QString &ccId) const;
		void insertAmbiguousCourtCaseId(const QString &ccId);

		/* Constructs printable mapping overview. */
		QStringList dumpCaseIdMapping(void) const;
		QStringList dumpCourtCaseIdMapping(void) const;

		/*!
		 * @brief Collects targets from upload hierarchy.
		 *
		 * @param[in] resp Upload hierarchy response.
		 * @return Unambiguous targets.
		 */
		static
		AutomaticUploadTarget getUnambiguousTargets(
		    const UploadHierarchyResp &resp);

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<AutomaticUploadTargetPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<AutomaticUploadTargetPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(AutomaticUploadTarget &first, AutomaticUploadTarget &second) Q_DECL_NOTHROW;

}
