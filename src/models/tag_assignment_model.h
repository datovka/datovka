/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QHash>
#include <QSet>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/json/basic.h" /* Json::Int64StringList is used in slots. Q_MOC_INCLUDE */
#include "src/delegates/tag_item.h"
#include "src/json/tag_assignment.h" /* Json::TagAssignmentList is used in slots. Q_MOC_INCLUDE */
#include "src/json/tag_entry.h" /* Json::TagEntryList is used in slots. Q_MOC_INCLUDE */
#include "src/json/tag_message_id.h"

class TagContainer; /* Forward declaration. */

/*!
 * @brief Tag assignment model class.
 */
class TagAssignmentModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Additional roles.
	 */
	enum UserRoles {
		ROLE_PROXYSORT = (Qt::UserRole + 1) /*!< Allows sorting according to names. */
	};
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_ENUM(UserRoles)
#else /* < Qt-5.5 */
	Q_ENUMS(UserRoles)
#endif /* >= Qt-5.5 */

	/*!
	 * @brief Column which this model holds.
	 */
	enum Columns {
		COL_TAG_NAME = 0, /*!< Tag name. */
		MAX_COLNUM /* Maximal number of columns (convenience value). */
	};
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_ENUM(Columns)
#else /* < Qt-5.5 */
	Q_ENUMS(Columns)
#endif /* >= Qt-5.5 */

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] tagCont Pointer to tag container.
	 * @param[in] parent Parent object.
	 */
	explicit TagAssignmentModel(TagContainer *tagCont,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Returns number of rows under given parent.
	 *
	 * @param[in] parent Parent index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the data stored under the given role.
	 *
	 * @param[in] index Position.
	 * @param[in] role Role if the position.
	 * @return Data or invalid QVariant if no matching data found.
	 */
	virtual
	QVariant data(const QModelIndex &index,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for checkable elements.
	 *
	 * @param[in] index Index which to obtain flags for.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for changing the check state.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] index Index specifying the element.
	 * @param[in] value Value to be set.
	 * @param[in] role Specifies role of the modified data.
	 * @return True if check state was changed.
	 */
	virtual
	bool setData(const QModelIndex &index, const QVariant &value,
	    int role = Qt::EditRole) Q_DECL_OVERRIDE;

	/*!
	 * @brief Obtains header data.
	 *
	 * @param[in] section Position.
	 * @param[in] orientation Orientation of the header.
	 * @param[in] role Role of the data.
	 * @return Data or invalid QVariant in no matching data found.
	 */
	virtual
	QVariant headerData(int section, Qt::Orientation orientation,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Set watched message identifiers.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgIds Set of message IDs, empty set if no assignment
	 *                   should be edited.
	 */
	void setMsgIds(const AcntId &acntId, const QSet<qint64> &msgIds);

private:
	/*!
	 * @brief Set model data.
	 *
	 * @param[in] tagList Tag list.
	 */
	void setTagList(const TagItemList &tagList);

	/*!
	 * @brief Insert tag entry.
	 *
	 * @param[in] entry Tag to be added.
	 */
	void insertTag(const Json::TagEntry &entry);

	/*!
	 * @brief Update tag entry if entry with same identifier is held within
	 *     the model.
	 *
	 * @param[in] entry Updated tag entry.
	 */
	void updateTag(const Json::TagEntry &entry);

	/*!
	 * @brief Delete tag entry if an entry with the supplied identifier
	 *     is held within the model.
	 *
	 * @param[in] id Identifier of the tag to be deleted.
	 */
	void deleteTag(qint64 id);

private Q_SLOTS:
/* Handle database signals. */
	/*!
	 * @brief Update model data when new tags are created.
	 *
	 * @param[in] entries Newly added tag entries.
	 */
	void watchTagsInserted(const Json::TagEntryList &entries);

	/*!
	 * @brief Update model data when tags updated.
	 *
	 * @param[in] entries Updated tag entries.
	 */
	void watchTagsUpdated(const Json::TagEntryList &entries);

	/*!
	 * @brief Update model data when tags deleted.
	 *
	 * @param[in] ids Deleted tag identifiers.
	 */
	void watchTagsDeleted(const Json::Int64StringList &ids);

	/*!
	 * @brief Update model data when tag assignment changes.
	 *
	 * @param[in] assignments Updated tag assignments.
	 */
	void watchTagAssignmentChanged(const Json::TagAssignmentList &assignments);

	/*!
	 * @brief Action when tag client is connected.
	 */
	void watchTagContConnected(void);

	/*!
	 * @brief Action when tag client is disconnected.
	 */
	void watchTagContDisconnected(void);

	/*!
	 * @brief Action when tag container is reset.
	 */
	void watchTagContReset(void);

private:
	/*!
	 * @brief Fill all assignment-related data from tag container.
	 */
	void loadAssignments(void);

	/*!
	 * @brief Load content.
	 */
	void loadContent(void);

	TagContainer *m_tagContPtr; /*!< Tag database/client pointer. */

	AcntId m_acntId; /*!< Account identifier. */
	QSet<qint64> m_msgIds; /*!< Set of watched message ids. */

	/* Values below are kept locally to avoid unnecessary queries. */
	TagItemList m_tagList; /*!< Tags ordered according to tag name. */
	QSet<TagItem> m_assignedTags; /*!< Tags assigned to any watched message id. */
	QHash<Json::TagMsgId, TagItemList> m_msgTagItemsMap; /*!< Maps message identifiers to tag entry lists. */
};
