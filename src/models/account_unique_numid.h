/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QMap>

class AcntId; /* Forward declaration. */

/*!
 * @brief This class provides a unique (within a single instance of the class)
 *    integer to AcntId mapping.
 *
 * The unique integer to AcntId is required for tree models where child nodes
 * need to refer a specific account using a QModelIndex. The model indexes can
 * hold numeric values. Therefore a unique AccountId to number mapping within
 * a single model instance is required.
 */
class AcntNumId {
public:
	/*!
	 * @brief Constructor.
	 */
	AcntNumId(void);

	/*!
	 * @brief Clear the content.
	 */
	void clear(void);

	/*!
	 * @brief Add a account identifier.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Non-negative unique number related to the account when added,
	 *     -1 else.
	 */
	int addAcntId(const AcntId &acntId);

	/*!
	 * @brief Remove account identifier.
	 *
	 * @note Related numeric identifier won't be re-used.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void removeAcntId(const AcntId &acntId);

	/*!
	 * @brief Replace account identifier.
	 *
	 * @param[in] oldId Account identifier to be replaced.
	 * @param[in] newId Replacement account identifier.
	 * @return Numerif account identifier which is re-used for the new account identifier,
	 *     -1 on any error.
	 */
	int replaceAcntId(const AcntId &oldId, const AcntId &newId);

	/*!
	 * @brief Get account identifier for the numeric value.
	 *
	 * @param[in] numId Numeric identifier.
	 * @return Valid account identifier when found,
	 *     invalid account identifier when nothing found.
	 */
	const AcntId &acntId(int numId) const;

	/*!
	 * @brief Get numeric identifier for account.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Non-negative number when found,
	 *     -1 when nothing found.
	 */
	int numId(const AcntId &acntId) const;

private:
	QList<AcntId> m_num2acntId; /*!< Held account identifiers. */
	QMap<AcntId, int> m_acntId2num; /*!< Mapping from account identifier to number. */
};
