/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/types.h"
#include "src/io/message_db.h"
#include "src/models/table_model.h"

/*!
 * @brief List of data boxes and additional information.
 */
class BoxContactsModel : public TblModel {
	Q_OBJECT
public:
	/*!
	 * @brief Additional roles.
	 */
	enum UserRoles {
		ROLE_PROXYSORT = (Qt::UserRole + 1) /*!< Allows fine-grained sorting of boolean values which may be displayed as icons or check boxes. */
	};
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_ENUM(UserRoles)
#else /* < Qt-5.5 */
	Q_ENUMS(UserRoles)
#endif /* >= Qt-5.5 */

	/*!
	 * @brief Identifies the column index.
	 */
	enum ColumnNumbers {
		CHECKBOX_COL = 0, /* First column holds checkboxes. */
		BOX_ID_COL = 1, /* Data box identifier identifier. */
		BOX_TYPE_COL = 2, /* Data box type. */
		BOX_NAME_COL = 3, /* Data box name. */
		ADDRESS_COL = 4, /* Subject address. */
		POST_CODE_COL = 5, /* Postal code. */
		PUBLIC_COL = 6, /* Public message. */
		PDZ_COL = 7, /* Commercial message. */
		PAYMENT_COL = 8, /* Current payment method. */
		PAYMENTS_COL = 9, /* Available payment method list. */
		MAX_COL = 10 /* Number of columns. */
	};

	/*!
	 * @brief Used for identifying check state for entries.
	 */
	enum EntryState {
		UNCHECKED, /* Unchecked entries. */
		CHECKED, /* Checked entries. */
		ANY /* Unchecked and checked entries. */
	};

	/*!
	 * @brief Used for returning model entries.
	 */
	class PartialEntry {
	public:
		/*!
		 * @brief Constructor.
		 */
		PartialEntry(void)
		    : id(), name(), address(), publ(Isds::Type::BOOL_NULL),
		    pdz(Isds::Type::BOOL_NULL), dmType(Isds::Type::MT_UNKNOWN)
		{}

		QString id; /*!< Data box identifier. */
		QString name; /*!< Data box name. */
		QString address; /*!< Data box subject address. */
		enum Isds::Type::NilBool publ; /*!< True only if true held in the model. */
		enum Isds::Type::NilBool pdz; /*!< True only if true held in the model. */
		enum Isds::Type::DmType dmType; /*!< Commercial message payment method. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit BoxContactsModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Returns the data stored under the given role.
	 *
	 * @param[in] index Position.
	 * @param[in] role  Role if the position.
	 * @return Data or invalid QVariant if no matching data found.
	 */
	virtual
	QVariant data(const QModelIndex &index,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for checkable elements.
	 *
	 * @param[in] index Index which to obtain flags for.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for changing the check state.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] index Index specifying the element.
	 * @param[in] value Value to be set.
	 * @param[in] role Specifies role of the modified data.
	 * @return True if check state was changed.
	 */
	virtual
	bool setData(const QModelIndex &index, const QVariant &value,
	    int role = Qt::EditRole) Q_DECL_OVERRIDE;

	/*!
	 * @brief Sets default header.
	 */
	void setHeader(void);

	/*!
	 * @brief Appends data into the model.
	 *
	 * @param[in] entryList List of entries to append into the model.
	 */
	void appendData(const QList<Isds::DbOwnerInfo> &entryList);

	/*!
	 * @brief Appends data into the model.
	 *
	 * @param[in] entryList List of entries to append into the model.
	 */
	void appendData(const QList<Isds::FulltextResult> &entryList);

	/*!
	 * @brief Appends data into the model.
	 *
	 * @param[in] entryList List of entries to append into the model.
	 */
	void appendData(const QList<MessageDb::ContactEntry> &entryList);

	/*!
	 * @brief Appends single entry to the model.
	 *
	 * @param[in] id Box identifier.
	 * @param[in] type Data box type.
	 * @param[in] name Box name.
	 * @param[in] addr Data box subject address.
	 * @param[in] postCode Postal code.
	 * @param[in] publ True if bublic message can be sent.
	 * @param[in] pdz True if sending of PDZ is necessary.
	 * @param[in] dmTypes PDZ payment method list.
	 */
	void appendData(const QString &id, enum Isds::Type::DbType type,
	    const QString &name, const QString &addr, const QString &postCode,
	    enum Isds::Type::NilBool publ, enum Isds::Type::NilBool pdz,
	    const QList<enum Isds::Type::DmType> &dmTypes);

	/*!
	 * @brief Returns true if there are some items checked.
	 *
	 * @return True if something checked.
	 */
	bool somethingChecked(void) const;

	/*!
	 * @brief Returns true if model contains supplied data box identifier.
	 *
	 * @oaram[in] boxId Data box identifier.
	 * @return True if model contains provided identifier.
	 */
	bool containsBoxId(const QString &boxId) const;

	/*!
	 * @brief Returns list of box identifiers according to required state.
	 *
	 * @param[in] entryState Specifies the checked state of required data.
	 * @return List of data box identifiers.
	 */
	QStringList boxIdentifiers(enum EntryState entryState) const;

	/*!
	 * @brief Returns list of entries according to required state.
	 *
	 * @Param[in] entryState Specifies the checked state of required data.
	 * @return List of partial entries.
	 */
	QList<PartialEntry> partialBoxEntries(enum EntryState entryState) const;

	/*!
	 * @brief Enable or disable colours.
	 *
	 * @param[in] colouring True to enable colours.
	 */
	void setColouring(bool colouring);

	/*!
	 * @brief Let the model know whether the sender has is an effective OVM.
	 *     This information is needed in order to choose the colour.
	 *
	 * @param[in] senderEffectiveOVM True if sender is an effective OVM.
	 */
	void setSenderEffectiveOVM(bool senderEffectiveOVM);

	/*!
	 * @brief Returns localised message type description text.
	 *
	 * @param[in] type Message type.
	 * @return Localised message type description.
	 */
	static
	QString descrDmType(int type);

	/*!
	 * @brief Returns localised message type tooltip description text.
	 *
	 * @param[in] type Message type.
	 * @return Localised message type tooltip description.
	 */
	static
	QString dmTypeToolTipDescr(int type);

private:
	/*!
	 * @brief Get box ID related to supplied index.
	 *
	 * @param[in] index Index which specifies the row used to take box
	 *                  identifier from.
	 * @return Box ID.
	 */
	QString getBoxId(const QModelIndex &index) const;

	bool m_colouring; /*!< True when colours should be enabled. */
	bool m_senderEffectiveOVM; /*!< True when sender is an effective OVM. */
};
