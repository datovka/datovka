/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractTableModel>
#include <QList>

#include "src/datovka_shared/identifiers/account_id.h"

/*!
 * @brief Model used for account selection for back-up.
 */
class BackupSelectionModel : public QAbstractTableModel {
	Q_OBJECT

public:
	/*!
	 * @brief Additional roles.
	 */
	enum UserRoles {
		ROLE_PROXYSORT = (Qt::UserRole + 1) /*!< Allows fine-grained sorting of boolean values which may be displayed as icons or check boxes. */
	};
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_ENUM(UserRoles)
#else /* < Qt-5.5 */
	Q_ENUMS(UserRoles)
#endif /* >= Qt-5.5 */

	/*!
	 * @brief Column which this model holds.
	 */
	enum Columns {
		COL_MSGS_CHECKBOX = 0, /*!< Messages related to this account are selected. */
		COL_ACCOUNT_NAME, /*!< Account name. */
		COL_USERNAME, /*!< Primary login to access this account by this application. */
		COL_TESTING, /*!< Whether this is a testing account. */
		COL_BOX_ID, /*!< Data box identifier. */

		MAX_COLNUM /* Maximal number of columns (convenience value). */
	};
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_ENUM(Columns)
#else /* < Qt-5.5 */
	Q_ENUMS(Columns)
#endif /* >= Qt-5.5 */

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit BackupSelectionModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Returns number of rows under given parent.
	 *
	 * @param[in] parent Parent index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns number of columns (for the children of given parent).
	 *
	 * @param[in] parent Parent index.
	 * @return Number of columns.
	 */
	virtual
	int columnCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the data stored under the given role.
	 *
	 * @param[in] index Position.
	 * @param[in] role  Role if the position.
	 * @return Data or invalid QVariant if no matching data found.
	 */
	virtual
	QVariant data(const QModelIndex &index,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for checkable elements.
	 *
	 * @param[in] index Index which to obtain flags for.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for changing the check state.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] index Index specifying the element.
	 * @param[in] value Value to be set.
	 * @param[in] role Specifies role of the modified data.
	 * @return True if check state was changed.
	 */
	virtual
	bool setData(const QModelIndex &index, const QVariant &value,
	    int role = Qt::EditRole) Q_DECL_OVERRIDE;

	/*!
	 * @brief Obtains header data.
	 *
	 * @param[in] section     Position.
	 * @param[in] orientation Orientation of the header.
	 * @param[in] role        Role of the data.
	 * @return Data or invalid QVariant in no matching data found.
	 */
	virtual
	QVariant headerData(int section, Qt::Orientation orientation,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Appends data into the model.
	 *
	 * @param[in] mSelected Selection state.
	 * @param[in] aName Account name.
	 * @param[in] acntId Account identifier.
	 * @param[in] bId Data box identifier.
	 */
	void appendData(bool mSelected, const QString &aName,
	    const AcntId &acntId, const QString &bId);

	/*!
	 * @brief Returns true if there are some items checked.
	 *
	 * @return Number of checked rows.
	 */
	int numChecked(void) const;

	/*!
	 * @brief Set all selection state.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] select Use true to select, false to unselect.
	 */
	void setAllCheck(bool select);

	/*!
	 * @brief Returns list of checked account identifiers.
	 *
	 * @return Account identifier list.
	 */
	QList<AcntId> checkedAcntIds(void) const;

	/*!
	 * @brief Clears the model content.
	 */
	void removeAllRows(void);

private:
	/*!
	 * @brief Describes model entries.
	 */
	class Entry {
	public:
		/*!
		 * @brief Constructor.
		 */
		Entry(bool m, const QString &a, const AcntId &aId,
		    const QString &b)
		    : msgsSelected(m), accountName(a), acntId(aId), boxId(b)
		{}

		bool msgsSelected;
		QString accountName;
		AcntId acntId;
		QString boxId;
	};

	QList<Entry> m_entries; /*!< List of model entries. */
	int m_numMsgsSelected; /*!< Total number of entries with selected message data. */
};
