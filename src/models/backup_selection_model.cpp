/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/gui/icon_container.h"
#include "src/models/backup_selection_model.h"
#include "src/models/helper.h"

static IconContainer inconContainer; /* Local icon container. */

BackupSelectionModel::BackupSelectionModel(QObject *parent)
    : QAbstractTableModel(parent),
    m_entries(),
    m_numMsgsSelected(0)
{
}

int BackupSelectionModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	} else {
		return m_entries.size();
	}
}

int BackupSelectionModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	} else {
		return MAX_COLNUM;
	}
}

QVariant BackupSelectionModel::data(const QModelIndex &index, int role) const
{
	const int row = index.row();
	const int col = index.column();
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return QVariant();
	}
	if (Q_UNLIKELY((col < 0) || (col >= columnCount()))) {
		Q_ASSERT(0);
		return QVariant();
	}

	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case COL_MSGS_CHECKBOX:
			return QVariant();
			break;
		case COL_ACCOUNT_NAME:
			return m_entries[row].accountName;
			break;
		case COL_USERNAME:
			return m_entries[row].acntId.username();
			break;
		case COL_TESTING:
			return m_entries[row].acntId.testing();
			break;
		case COL_BOX_ID:
			return m_entries[row].boxId;
			break;
		default:
			Q_ASSERT(0);
			return QVariant();
			break;
		}
		break;
	case Qt::CheckStateRole:
		if (col == COL_MSGS_CHECKBOX) {
			return m_entries[row].msgsSelected ? Qt::Checked : Qt::Unchecked;
		} else {
			return QVariant();
		}
		break;
	case ROLE_PROXYSORT:
		switch (col) {
		case COL_MSGS_CHECKBOX:
			return ModelHelper::sortRank(
			    m_entries.at(row).msgsSelected ? QStringLiteral("1 ") : QStringLiteral("0 "),
			    m_entries.at(row).acntId.username());
			break;
		case COL_ACCOUNT_NAME:
			return m_entries[row].accountName;
			break;
		case COL_USERNAME:
			return m_entries[row].acntId.username();
			break;
		case COL_TESTING:
			return ModelHelper::sortRank(
			    m_entries.at(row).acntId.testing() ? QStringLiteral("1 ") : QStringLiteral("0 "),
			    m_entries.at(row).acntId.username());
			break;
		case COL_BOX_ID:
			return m_entries[row].boxId;
			break;
		default:
			Q_ASSERT(0);
			return QVariant();
			break;
		}
		break;
	default:
		return QVariant();
		break;
	}
}

Qt::ItemFlags BackupSelectionModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags defaultFlags = QAbstractTableModel::flags(index);

	if (index.column() == COL_MSGS_CHECKBOX) {
		defaultFlags |= Qt::ItemIsUserCheckable;
	}

	return defaultFlags;
}

bool BackupSelectionModel::setData(const QModelIndex &index,
    const QVariant &value, int role)
{
	if (Q_UNLIKELY(!index.isValid())) {
		return false;
	}

	const int row = index.row();
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return false;
	}

	if ((index.column() != COL_MSGS_CHECKBOX) || (role != Qt::CheckStateRole)) {
		return QAbstractTableModel::setData(index, value, role);
	}

	bool newVal = (value == Qt::Checked);
	if (newVal != m_entries[row].msgsSelected) {
		m_entries[row].msgsSelected = newVal;
		if (newVal) {
			++m_numMsgsSelected;
		} else {
			--m_numMsgsSelected;
		}
		emit dataChanged(index, index);
		return true;
	}

	return false;
}

QVariant BackupSelectionModel::headerData(int section,
    Qt::Orientation orientation, int role) const
{
	Q_UNUSED(orientation);

	switch (role) {
	case Qt::DisplayRole:
		switch (section) {
		case COL_MSGS_CHECKBOX:
			return QVariant(); /* Hide text. */
			break;
		case COL_ACCOUNT_NAME:
			return tr("Data box");
			break;
		case COL_USERNAME:
			return tr("Username");
			break;
		case COL_TESTING:
			return tr("Testing");
			break;
		case COL_BOX_ID:
			return tr("Box identifier");
			break;
		default:
			Q_ASSERT(0);
			return QVariant();
			break;
		}
		break;

	case Qt::DecorationRole:
		switch (section) {
		case COL_MSGS_CHECKBOX:
			return inconContainer.icon(IconContainer::ICON_MESSAGE);
			break;
		default:
			return QVariant();
			break;
		}
		break;

	case Qt::ToolTipRole:
		switch (section) {
		case COL_MSGS_CHECKBOX:
			return tr("Messages");
			break;
		default:
			return QVariant();
			break;
		}
		break;

	default:
		return QVariant();
		break;
	}
}

void BackupSelectionModel::appendData(bool mSelected, const QString &aName,
    const AcntId &acntId, const QString &bId)
{
	Entry entry(mSelected, aName, acntId, bId);

	beginInsertRows(QModelIndex(), rowCount(), rowCount());

	m_entries.append(entry);
	if (mSelected) {
		++m_numMsgsSelected;
	}

	endInsertRows();
}

int BackupSelectionModel::numChecked(void) const
{
	return m_numMsgsSelected;
}

void BackupSelectionModel::setAllCheck(bool select)
{
	int top = -1;
	int bottom = -1;

	for (int row = 0; row < m_entries.size(); ++row) {
		Entry &e(m_entries[row]);
		if (e.msgsSelected != select) {
			if (top < 0) {
				top = row;
			}
			bottom = row;
			e.msgsSelected = select;
		}
	}
	m_numMsgsSelected = select ? rowCount() : 0;

	if (top >= 0) {
		if (Q_UNLIKELY((bottom < 0) || bottom >= m_entries.size())) {
			Q_ASSERT(0);
		}

		emit dataChanged(index(top, COL_MSGS_CHECKBOX),
		    index(bottom, COL_MSGS_CHECKBOX));
	}
}

QList<AcntId> BackupSelectionModel::checkedAcntIds(void) const
{
	QList<AcntId> checkedIds;

	for (const Entry &e : m_entries) {
		if (e.msgsSelected) {
			checkedIds.append(e.acntId);
		}
	}

	return checkedIds;
}

void BackupSelectionModel::removeAllRows(void)
{
	beginResetModel();
	m_entries.clear();
	endResetModel();
}
