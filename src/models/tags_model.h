/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QVariant>

#include "src/delegates/tag_item.h"

namespace Json {
	class TagEntry; /* Forward declaration. */
}

/*!
 * @brief Custom tags model class.
 */
class TagsModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit TagsModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Returns number of rows under given parent.
	 *
	 * @param[in] parent Parent index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the data stored under the given role.
	 *
	 * @param[in] index Position.
	 * @param[in] role  Role if the position.
	 * @return Data or invalid QVariant if no matching data found.
	 */
	virtual
	QVariant data(const QModelIndex &index,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Obtains header data.
	 *
	 * @param[in] section     Position.
	 * @param[in] orientation Orientation of the header.
	 * @param[in] role        Role of the data.
	 * @return Data or invalid QVariant in no matching data found.
	 */
	virtual
	QVariant headerData(int section, Qt::Orientation orientation,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Set model data.
	 *
	 * @param[in] tagList Tag list.
	 */
	void setTagList(const TagItemList &tagList);

	/*!
	 * @brief Insert tag entry.
	 *
	 * @param[in] entry Tag to be added.
	 */
	void insertTag(const Json::TagEntry &entry);

	/*!
	 * @brief Update tag entry if entry with same identifier is held within
	 *     the model.
	 *
	 * @param[in] entry Updated tag entry.
	 */
	void updateTag(const Json::TagEntry &entry);

	/*!
	 * @brief Delete tag entry if an entry with the supplied identifier
	 *     is held within the model.
	 *
	 * @param[in] id Identifier of the tag to be deleted.
	 */
	void deleteTag(qint64 id);

private:
	TagItemList m_tagList; /*!< Tags ordered according to tag name. */
};
