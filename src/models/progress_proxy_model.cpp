/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/compat/compiler.h" /* macroStdMove */
#include "src/models/progress_proxy_model.h"

ProgressBarProxyModel::ProgressBarProxyModel(QObject *parent)
    : QIdentityProxyModel(parent),
    m_progresses()
{
}

QVariant ProgressBarProxyModel::data(const QModelIndex &index, int role) const
{
	QAbstractItemModel *model = sourceModel();
	if (Q_UNLIKELY(Q_NULLPTR == model)) {
		/* Use defaults if no source model specified. */
		return QIdentityProxyModel::data(index, role);
	}

	const QPair<int, int> coordinates = {index.row(), index.column()};
	if (m_progresses.contains(coordinates)) {
		const ProgressValue progress = m_progresses[coordinates];
		return QVariant::fromValue(ProgressValue(progress.minimum,
		    progress.maximum, progress.value,
		    macroStdMove(QIdentityProxyModel::data(index, Qt::DisplayRole).toString())));
	}

	/* Use default implementation. */
	return QIdentityProxyModel::data(index, role);
}

void ProgressBarProxyModel::clearAllProgress(void)
{
	QMap< QPair<int, int>, ProgressValue >::iterator it = m_progresses.begin();
	while (it != m_progresses.end()) {
		/* Make coordinates copy before removing. */
		int row;
		int col;
		{
			const QPair<int, int> &coordinates = it.key();
			row = coordinates.first;
			col = coordinates.second;
		}
		it = m_progresses.erase(it);

		if ((row >= 0) && (row < rowCount()) &&
		    (col >= 0) && (col < columnCount())) {
			const QModelIndex changedIdx = index(row, col);
			emit dataChanged(changedIdx, changedIdx);
		}
	}
}

void ProgressBarProxyModel::clearProgress(int row, int col)
{
	const QPair<int, int> coordinates = {row, col};
	if (m_progresses.contains(coordinates)) {
		m_progresses.remove(coordinates);
		if ((row >= 0) && (row < rowCount()) &&
		    (col >= 0) && (col < columnCount())) {
			const QModelIndex changedIdx = index(row, col);
			emit dataChanged(changedIdx, changedIdx);
		}
	}
}

void ProgressBarProxyModel::setProgress(int row, int col, int min, int max, int progress)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()) ||
	        (col < 0) || (col >= columnCount()))) {
		return;
	}

	const QPair<int, int> coordinates = {row, col};

	if (m_progresses.contains(coordinates)) {
		const ProgressValue storedProgress = m_progresses[coordinates];
		ProgressValue newProgress(min, max, progress);
		if (storedProgress != newProgress) {
			m_progresses[coordinates] = macroStdMove(newProgress);
			const QModelIndex changedIdx = index(row, col);
			emit dataChanged(changedIdx, changedIdx);
		}
	} else {
		m_progresses[coordinates] = ProgressValue(min, max, progress);
		const QModelIndex changedIdx = index(row, col);
		emit dataChanged(changedIdx, changedIdx);
	}
}
