/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

class QDateTime; /* Forward declaration. */

/*!
 * @brief Model helper functions.
 */
namespace ModelHelper {

	/*!
	 * @brief Construct a numeric sorting identifier.
	 *
	 * @param[in] num Small number with higher significance.
	 * @param[in] dmId Message ID. The message ID has a low significance.
	 * @return A number that can be used for sorting purposes.
	 */
	qint64 sortRank(qint16 num, qint64 dmId);

	/*!
	 * @brief Construct a string-based sorting identifier.
	 *
	 * @param[in] hiStr Short string with higher significance.
	 * @param[in] dmId Message ID. The message ID has a low significance.
	 * @return A string that can be used for sorting purposes.
	 */
	QString sortRank(const QString &hiStr, qint64 dmId);

	/*!
	 * @brief Construct a string-based sorting identifier.
	 *
	 * @param[in] hiStr Short string with higher significance.
	 * @param[in] loStr String with lower significance.
	 * @return A string that can be used for sorting purposes.
	 */
	QString sortRank(const QString &hiStr, const QString &loStr);

	/*!
	 * @brief Construct a string-based sorting identifier.
	 *
	 * @param[in] hiDateTime Date time value with higher significance.
	 * @param[in] loDmId Message ID. The message ID has a low significance.
	 * @return A string that can be used for sorting purposes.
	 */
	QString sortRank(const QDateTime &hiDateTime, qint64 loDmId);

}
