/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QIdentityProxyModel>

class QTreeView; /* Forward declaration. */

/*!
 * @brief Account proxy model. Passes information whether a node is collapsed
 *     from the view into the underlying model.
 */
class CollapsibleAccountProxyModel : public QIdentityProxyModel {
	Q_OBJECT
public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit CollapsibleAccountProxyModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Return data stored in given location under given role.
	 *
	 * @param[in] index Index specifying the item.
	 * @param[in] role  Data role.
	 * @return Data from model.
	 */
	virtual
	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

	/*
	 * Tree view from which the information about collapsed nodes is acquired.
	 */
	QTreeView *watchedTreeView(void) const;
	void setWatchedTreeView(QTreeView *treeView);

	/*
	 * Bit which indicates to the source model that a node is collapsed.
	 */
	int collapsedRoleBit(void) const;
	void setCollapsedRoleBit(int roleBit);

private slots:
	/*!
	 * @brief Watch expanded/collapsed nodes from the tree view.
	 *
	 * @note Emits dataChanged signal.
	 */
	void nodeCollapsedOrExpanded(const QModelIndex &index);

private:
	QTreeView *m_watchedTreeView; /*!< Watched tree view. */
	int m_collapsedRoleBit; /*!< Set this bit in role to indicate a collapsed node. */
};
