/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QIdentityProxyModel>
#include <QMap>
#include <QPair>

#include "src/delegates/progress_value.h"

/*!
 * @brief Progress-bar proxy model. Shows progress bars in specified locations.
 */
class ProgressBarProxyModel : public QIdentityProxyModel {
	Q_OBJECT
public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit ProgressBarProxyModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Return data stored in given location under given role.
	 *
	 * @param[in] index Index specifying the item.
	 * @param[in] role  Data role.
	 * @return Data from model.
	 */
	virtual
	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Clear all progress bars at any position.
	 *
	 * @note Emits dataChanged signal.
	 */
	void clearAllProgress(void);

	/*!
	 * @brief Clear progress bar at position.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] row Row.
	 * @param[in] col Column.
	 */
	void clearProgress(int row, int col);

	/*!
	 * @brief Set progress bar at position.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] row Row.
	 * @param[in] col Column.
	 * @param[in] min Progress minimum value.
	 * @param[in] max Progress maximum value.
	 * @param[in] progress Progress current value.
	 */
	void setProgress(int row, int col, int min, int max, int progress);

private:
	QMap< QPair<int, int>, ProgressValue > m_progresses;
};
