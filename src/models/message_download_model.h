/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractTableModel>
#include <QList>
#include <QPair>
#include <QString>

#include "src/identifiers/account_id_db.h"
#include "src/identifiers/message_id.h"

/*!
 * @brief Message download state model.
 */
class MsgDownloadModel : public QAbstractTableModel {
	Q_OBJECT

public:
	/*!
	 * @brief Additional roles.
	 */
	enum UserRoles {
		ROLE_PROXYSORT = (Qt::UserRole + 1) /*!< Allows fine-grained sorting of boolean values which may be displayed as icons or check boxes. */
	};
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_ENUM(UserRoles)
#else /* < Qt-5.5 */
	Q_ENUMS(UserRoles)
#endif /* >= Qt-5.5 */

	/*!
	 * @brief Column which this model holds.
	 */
	enum Columns {
		COL_MSG_ID = 0, /*!< Messages related to this account are selected. */
		COL_STATE, /*!< Indicates download state. */
		COL_ERROR, /*!< Contains error description. */

		MAX_COLNUM /* Maximal number of columns (convenience value). */
	};
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_ENUM(Columns)
#else /* < Qt-5.5 */
	Q_ENUMS(Columns)
#endif /* >= Qt-5.5 */

	enum DownloadState {
		STAT_NOT_ATTEMPTED = 0, /*!< Download not attempted yet. */
		STAT_FAILED, /*!< Download failed. */
		STAT_SUCCEEDED /* !< Download succeeded. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit MsgDownloadModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Returns number of rows under given parent.
	 *
	 * @param[in] parent Parent index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns number of columns (for the children of given parent).
	 *
	 * @param[in] parent Parent index.
	 * @return Number of columns.
	 */
	virtual
	int columnCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the data stored under the given role.
	 *
	 * @param[in] index Position.
	 * @param[in] role  Role if the position.
	 * @return Data or invalid QVariant if no matching data found.
	 */
	virtual
	QVariant data(const QModelIndex &index,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Obtains header data.
	 *
	 * @param[in] section     Position.
	 * @param[in] orientation Orientation of the header.
	 * @param[in] role        Role of the data.
	 * @return Data or invalid QVariant in no matching data found.
	 */
	virtual
	QVariant headerData(int section, Qt::Orientation orientation,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Appends data into the model.
	 *
	 * @param[in] anctIdDb Account identifier.
	 * @param[in] msgId Message identifier.
	 * @param[in] state Download state.
	 * @param[in] error Error description.
	 */
	void appendData(const AcntIdDb &anctIdDb, const MsgId &msgId,
	    enum DownloadState state, const QString &error);

	/*!
	 * @brief Update download state and error description.
	 *
	 * @param[in] anctIdDb Account identifier.
	 * @param[in] msgId Message identifier.
	 * @param[in] state Download state.
	 * @param[in] error Error description.
	 */
	void updateDownloadState(const AcntIdDb &anctIdDb, const MsgId &msgId,
	    enum DownloadState state, const QString &error);

	/*!
	 * @brief Check whether all messages have been downloaded successfully.
	 *
	 * @note Returns true on empty model.
	 *
	 * @return True if all messages have been downloaded.
	 */
	bool allDownloaded(void) const;

	/*!
	 * @brief Check whether some messages have been downloaded successfully.
	 *
	 * @return True if some messages have been downloaded.
	 */
	bool someDownloaded(void) const;

	/*!
	 * @brief Return list of all downloaded messages.
	 *
	 * @return List of account and message identifier pairs of downloaded
	 *     messages.
	 */
	QList< QPair<AcntIdDb, MsgId> > downloaded(void) const;

private:
	/*!
	 * @brief Describes model entries.
	 */
	class Entry {
	public:
		Entry(const AcntIdDb &a, const MsgId &m, enum DownloadState s,
		    const QString &e)
		    : acntIdDb(a), msgId(m), state(s), error(e)
		{}

		AcntIdDb acntIdDb; /*!< Account and database set. */
		MsgId msgId; /*!< Message identifier. */
		enum DownloadState state; /*< Download state. */
		QString error; /*!< Download error description. */
	};

	QList<Entry> m_entries; /*!< List of model entries. */
};
