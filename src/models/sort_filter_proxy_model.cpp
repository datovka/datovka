/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QString>

#include "src/datovka_shared/localisation/localisation.h"
#include "src/delegates/tag_item.h"
#include "src/models/sort_filter_proxy_model.h"

const QString SortFilterProxyModel::invalidFilterEditStyle("-1");
const QString SortFilterProxyModel::blankFilterEditStyle(
    "QLineEdit{background: white;}");
const QString SortFilterProxyModel::foundFilterEditStyle(
    "QLineEdit{background: #1a11a011;}");
const QString SortFilterProxyModel::notFoundFilterEditStyle(
    "QLineEdit{background: #1ace1616;}");

SortFilterProxyModel::SortFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent),
    m_filterColumns()
{
}

void SortFilterProxyModel::setFilterKeyColumn(int column)
{
	m_filterColumns.clear();
	m_filterColumns.append(column);
}

void SortFilterProxyModel::setFilterKeyColumns(const QList<int> &columns)
{
	m_filterColumns = columns;
}

const QList<int> &SortFilterProxyModel::filterKeyColumns(void) const
{
	return m_filterColumns;
}

/*
 * There's a problem in Qt-5 because QSortFilterProxyModel::setFilterFixedString()
 * sets the QRegExp but not the QRegularExpression.
 * QRegularExpression::escape() is only available since Qt-5.15 and
 * QSortFilterProxyModel::setFilterRegularExpression() is available since Qt-5.12.
 */
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
#  define _filterRegularExpressionEmpty() filterRegularExpression().pattern().isEmpty()
#  define _filterRegularExpression() filterRegularExpression()
#else /* < Qt-5.15 */
#  define _filterRegularExpressionEmpty() filterRegExp().isEmpty()
#  define _filterRegularExpression() filterRegExp()
#endif /* >= Qt-5.15 */

bool SortFilterProxyModel::filterAcceptsRow(int sourceRow,
    const QModelIndex &sourceParent) const
{
	/*
	 * Adapted from
	 * qtbase/src/corelib/itemmodels/qsortfilterproxymodel.cpp .
	 */

	if (_filterRegularExpressionEmpty()) {
		return true;
	}

	if (m_filterColumns.isEmpty() || m_filterColumns.contains(-1)) {
		int columnCnt = columnCount();
		for (int column = 0; column < columnCnt; ++column) {
			QModelIndex sourceIndex(sourceModel()->index(sourceRow,
			    column, sourceParent));
			if (filterAcceptsItem(sourceIndex)) {
				return true;
			}
		}
		return false;
	}

	for (int column : m_filterColumns) {
		QModelIndex sourceIndex(
		    sourceModel()->index(sourceRow, column, sourceParent));
		/* The column may not exist. */
		if (filterAcceptsItem(sourceIndex)) {
			return true;
		}
	}
	return false;
}

bool SortFilterProxyModel::lessThan(const QModelIndex &sourceLeft,
    const QModelIndex &sourceRight) const
{
	const QVariant leftData(sourceModel()->data(sourceLeft, sortRole()));
	const QVariant rightData(sourceModel()->data(sourceRight, sortRole()));

	if (leftData.userType() == QMetaType::QString) {
		Q_ASSERT(rightData.userType() == QMetaType::QString);
		return Localisation::stringCollator.compare(leftData.toString(),
		    rightData.toString()) < 0;
	} else if (leftData.canConvert<TagItem>()) {
		Q_ASSERT(rightData.canConvert<TagItem>());
		TagItem leftTagItem(qvariant_cast<TagItem>(leftData));
		TagItem rightTagItem(qvariant_cast<TagItem>(rightData));
		return Localisation::stringCollator.compare(leftTagItem.name(),
		    rightTagItem.name()) < 0;
	} else if (leftData.canConvert<TagItemList>()) {
		Q_ASSERT(rightData.canConvert<TagItemList>());
		TagItemList leftTagList(qvariant_cast<TagItemList>(leftData));
		TagItemList rightTagList(qvariant_cast<TagItemList>(rightData));

		forever {
			bool leftEmpty = leftTagList.isEmpty();
			bool rightEmpty = rightTagList.isEmpty();

			if (leftEmpty && rightEmpty) {
				return false;
			} else if (leftEmpty && !rightEmpty) {
				return true;
			} else if (!leftEmpty && rightEmpty) {
				return false;
			}

			/* None of the lists are empty. */
			int ret = Localisation::stringCollator.compare(
			    leftTagList.first().name(),
			    rightTagList.first().name());
			if (ret < 0) {
				return true;
			} else if (ret > 0) {
				return false;
			}

			/* Both tags have equal names. */
			leftTagList.removeFirst();
			rightTagList.removeFirst();
		}
	} else {
		return QSortFilterProxyModel::lessThan(sourceLeft, sourceRight);
	}
}

bool SortFilterProxyModel::filterAcceptsItem(const QModelIndex &sourceIdx) const
{
	if (!sourceIdx.isValid()) {
		return false;
	}

	const QVariant data(sourceModel()->data(sourceIdx, filterRole()));

	if (data.canConvert<TagItem>()) {
		TagItem tagItem = qvariant_cast<TagItem>(data);
		return tagItem.name().contains(_filterRegularExpression());
	} else if (data.canConvert<TagItemList>()) {
		TagItemList tagList = qvariant_cast<TagItemList>(data);
		for (const TagItem &tagItem : tagList) {
			if (tagItem.name().contains(_filterRegularExpression())) {
				return true;
			}
		}
		return false;
	} else {
		QString key(data.toString());
		return key.contains(_filterRegularExpression());
	}
}
