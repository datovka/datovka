/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractTableModel>
#include <QStringList>
#include <QVariant>

class Prefs; /* Forward declaration. */

/*!
 * @brief Custom prefs model class.
 */
class PrefsModel : public QAbstractTableModel {
	Q_OBJECT

public:
	/*!
	 * @brief Column which this model holds.
	 */
	enum Columns {
		COL_NAME = 0, /*!< Preference name. */
		COL_STATUS, /*!< Modification status. */
		COL_TYPE, /*!< Value type. */
		COL_VALUE, /*!< Preference value. */

		MAX_COLNUM /* Maximal number of columns (convenience value). */
	};
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_ENUM(Columns)
#else /* < Qt-5.5 */
	Q_ENUMS(Columns)
#endif /* >= Qt-5.5 */

	/*!
	 * @brief Constructor.
	 *
	 * @param[in,out] prefs Preferences to be worked with.
	 * @param[in] parent Parent object.
	 */
	explicit PrefsModel(Prefs &prefs, QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Returns number of rows under given parent.
	 *
	 * @param[in] parent Parent index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns number of columns (for the children of given parent).
	 *
	 * @param[in] parent Parent index.
	 * @return Number of columns.
	 */
	virtual
	int columnCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the data stored under the given role.
	 *
	 * @param[in] index Position.
	 * @param[in] role  Role if the position.
	 * @return Data or invalid QVariant if no matching data found.
	 */
	virtual
	QVariant data(const QModelIndex &index,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for checkable elements.
	 *
	 * @param[in] index Index which to obtain flags for.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Obtains header data.
	 *
	 * @param[in] section     Position.
	 * @param[in] orientation Orientation of the header.
	 * @param[in] role        Role of the data.
	 * @return Data or invalid QVariant in no matching data found.
	 */
	virtual
	QVariant headerData(int section, Qt::Orientation orientation,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Find first row with given \a name.
	 *
	 * @param[in] name Name to search for.
	 * @return Number of given row or -1 if not found or on error.
	 */
	int findRow(const QString &name) const;

	/*!
	 * @brief Get type of entry on row.
	 *
	 * @param[in] row Row number.
	 * @return enum PrefsDb::ValueType or -1 on any error.
	 */
	int type(int row) const;

	/*
	 * Toggles or modifies values value on row. Must be used with correct type.
	 */
	void alterBool(int row);
	void modifyInt(int row, qint64 val);
	void modifyFloat(int row, double val);
	void modifyString(int row, const QString &val);
	void modifyColour(int row, const QString &val);
	void modifyDateTime(int row, const QDateTime &val);
	void modifyDate(int row, const QDate &val);

	/*
	 * @brief Reset selected entry on given \a row.
	 *
	 * @param[in] row Row number.
	 */
	void resetEntry(int row);

private slots:
	/*!
	 * @brief Watch when a value is added to preferences.
	 *     Handle configuration signals.
	 *
	 * @param[in] valueType Utilises enum PrefsDb::ValueType values.
	 * @param[in] name Entry name.
	 * @param[in] value Entry value holding the respective type.
	 */
	void watchPrefsCreation(int valueType, const QString &name,
	    const QVariant &value);

	/*!
	 * @brief Watch changes in preferences. Handle configuration signals.
	 *
	 * @param[in] valueType Utilises enum PrefsDb::ValueType values.
	 * @param[in] name Entry name.
	 * @param[in] value Entry value holding the respective type.
	 */
	void watchPrefsModification(int valueType, const QString &name,
	    const QVariant &value);

	/*!
	 * @brief Watch when a value is removed from preferences.
	 *     Handle configuration signals.
	 *
	 * @param[in] valueType Utilises enum PrefsDb::ValueType values.
	 * @param[in] name Entry name.
	 */
	void watchPrefsRemoval(int valueType, const QString &name);

private:
	Prefs &m_prefs; /*!< Model source. */
	QStringList m_prefKeys; /*!< Preferences names. */
};
