/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/models/tags_model.h"

TagsModel::TagsModel(QObject *parent)
    : QAbstractListModel(parent),
    m_tagList()
{
}

int TagsModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	} else {
		return m_tagList.size();
	}
}

QVariant TagsModel::data(const QModelIndex &index, int role) const
{
	if (role != Qt::DisplayRole) {
		return QVariant();
	}

	if ((index.row() < m_tagList.size()) && (index.column() == 0)) {
		return QVariant::fromValue(m_tagList.at(index.row()));
	} else {
		return QVariant();
	}
}

QVariant TagsModel::headerData(int section, Qt::Orientation orientation,
    int role) const
{
	Q_UNUSED(orientation);

	if (role != Qt::DisplayRole) {
		return QVariant();
	}

	if (0 == section) {
		return tr("Tags");
	} else {
		return QVariant();
	}
}

void TagsModel::setTagList(const TagItemList &tagList)
{
	beginResetModel();

	m_tagList = tagList;
	/* Keep tags ordered. */
	m_tagList.sortNames();

	endResetModel();
}

void TagsModel::insertTag(const Json::TagEntry &entry)
{
	if (Q_UNLIKELY(!entry.isValid())) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(m_tagList.containsId(entry.id()))) {
		return;
	}

	/* Find proper place in ordered list. */
	TagItem item(entry);
	TagItemList::iterator it = m_tagList.upperBoundName(item);
	int pos = it - m_tagList.begin(); /* Get position to be inserted. */

	beginInsertRows(QModelIndex(), pos, pos);

	m_tagList.insert(it, item);

	endInsertRows();
}

void TagsModel::updateTag(const Json::TagEntry &entry)
{
	if (Q_UNLIKELY(!entry.isValid())) {
		Q_ASSERT(0);
		return;
	}

	int oldPos = m_tagList.indexOfId(entry.id());
	if (Q_UNLIKELY(oldPos < 0)) {
		return;
	}

	int newPos = 0;
	{
		TagItem item(entry);
		TagItemList::iterator it = m_tagList.upperBoundName(item);
		newPos = it - m_tagList.begin(); /* Get position to be inserted. */
	}

	if ((oldPos + 1) == newPos) {
		/*
		 * New position is just past the old position.
		 * Technically the position does not change in this case.
		 */
		newPos = oldPos;
	} else if (oldPos != newPos) {
		/* Moved to different position. */

		/*
		 * QAbstractItemModel::beginMoveRows() behaves slightly
		 * differently to QList::move().
		 */
		beginMoveRows(QModelIndex(), oldPos, oldPos, QModelIndex(), newPos);

		if (oldPos < newPos) {
			/*
			 * New position was computed behind the element which
			 * is moved and therefore technically not being
			 * present in it's position.
			 */
			--newPos;
		}

		m_tagList.move(oldPos, newPos);

		endMoveRows();
	}

	m_tagList[newPos] = TagItem(entry);

	emit dataChanged(index(newPos, 0), index(newPos, 0));
}

void TagsModel::deleteTag(qint64 id)
{
	if (Q_UNLIKELY(id < 0)) {
		Q_ASSERT(0);
		return;
	}

	int pos = m_tagList.indexOfId(id);
	if (Q_UNLIKELY(pos < 0)) {
		return;
	}

	beginRemoveRows(QModelIndex(), pos, pos);

	m_tagList.removeAt(pos);

	endRemoveRows();
}
