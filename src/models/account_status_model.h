/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>

#include "src/datovka_shared/identifiers/account_id.h"

class AccountsMap; /* Forward declaration. */
class IsdsSessions; /* Forward declaration. */

/*!
 * @brief Account status list model.
 */
class AccountStatusModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] accounts Account container.
	 * @param[in] sessions Session container.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit AccountStatusModel(AccountsMap *accounts,
	    IsdsSessions *sessions, QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Returns number of rows under given parent.
	 *
	 * @param[in] parent Parent index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the data stored under the given role.
	 *
	 * @param[in] index Position.
	 * @param[in] role  Role if the position.
	 * @return Data or invalid QVariant if no matching data found.
	 */
	virtual
	QVariant data(const QModelIndex &index,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for connected sessions.
	 *
	 * @param[in] index Index which to obtain flags for.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for disconnecting a session.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] index Index specifying the element.
	 * @param[in] value Value to be set.
	 * @param[in] role Specifies role of the modified data.
	 * @return True if data changed.
	 */
	virtual
	bool setData(const QModelIndex &index, const QVariant &value,
	    int role = Qt::EditRole) Q_DECL_OVERRIDE;

private Q_SLOTS:
	/*!
	 * @brief Notify before an account is going to be added.
	 */
	void watchAccountAboutToBeAdded(const AcntId &acntId, int row);

	/*!
	 * @brief Update the model after an account has been added.
	 */
	void watchAccountAdded(const AcntId &acntId, int row);

	/*!
	 * @brief Notify before an account is going to be removed.
	 */
	void watchAccountAboutToBeRemoved(const AcntId &acntId, int row);

	/*!
	 * @brief Update the model after an account has been removed.
	 */
	void watchAccountRemoved(const AcntId &acntId, int row);

	/*!
	 * @brief Notify before accounts are going to be moved.
	 */
	void watchAccountsAboutToBeMoved(int srcFrst, int srcLst, int dst);

	/*!
	 * @brief Update the model after accounts have been moved.
	 */
	void watchAccountsMoved(int srcFrst, int srcLst, int dst);

	/*!
	 * @brief This slot handles changes of username.
	 */
	void watchUsernameChanged(const AcntId &oldId, const AcntId &newId, int index);

	/*!
	 * @brief Update the model after account data has been modified.
	 */
	void watchAccountDataChanged(const AcntId &acntId);

	/*!
	 * @brief Notifies that the content is going to be changed.
	 */
	void watchContentAboutToBeReset(void);

	/*!
	 * @brief Notifies that the content has been changed.
	 */
	void watchContentReset(void);

	/*!
	 * @brief Updates model data.
	 */
	void watchSessionChanges(const QString &username, int nowActive);

private:
	AccountsMap *m_accounts; /*!< Pointer to account container (regular or shadow). */
	IsdsSessions *m_sessions; /*!< Session container. */
};
