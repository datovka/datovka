/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QNetworkProxy>
#include <QString>

#define PROXY_NO_PORT -1

class Prefs; /* Forward declaration. */

class ProxiesSettings {
public:
	/*!
	 * @brief Connection type.
	 */
	enum Type {
		HTTP,
		HTTPS
	};

	static
	const QByteArray httpProxyEnvVar; /*!< http_proxy environment variable at application start-up. */
	static
	const QByteArray httpsProxyEnvVar; /*!< https_proxy environment variable at application start-up. */

	class ProxySettings {
	public:
		/*!
		 * @brief Proxy usage.
		 */
		enum Usage {
			NO_PROXY, /*!< Disable proxy. */
			AUTO_PROXY, /*!< Automatic proxy detection. */
			DEFINED_PROXY /*!< Use defined values. */
		};

		/*!
		 * @brief Constructor.
		 */
		ProxySettings(void);

		enum Usage usage; /*!< How to interpret the proxy data.  */

		QString userName; /*!< Proxy username. */
		QString password; /*!< Proxy password. */

		QString hostName; /*!< Proxy host. */
		int port; /*!< Proxy port. */

		/*!
		 * @brief Converts value to value as would be stored in
		 *     environment variable value.
		 *
		 * @return Values as they would be stored in a http(s)_proxy
		 *     variable without protocol specification (e.g.
		 *     'b:bb@192.168.1.1:3128', '172.0.0.1:3128') if contains
		 *     DEFINED_PROXY data, no data else.
		 */
		QByteArray toEnvVal(void) const;
	};

	/*!
	 * @brief Constructor.
	 */
	ProxiesSettings(void);

	/*!
	 * @brief Load data from supplied preferences.
	 *
	 * @param[in] prefs Preferences.
	 */
	void loadFromPrefs(const Prefs &prefs);

	/*!
	 * @brief Store data to preferences.
	 *
	 * @param[out] prefs Preferences.
	 */
	void saveToPrefs(Prefs &prefs) const;

	/*
	 * Libcurl automatically uses the variables http_proxy and https_proxy
	 * if they are set. Therefore, if no proxy is forced from the
	 * configuration file, then these two environmental variables are going
	 * to be cleared inside this application.
	 */

	/*!
	 * @brief Detects proxy settings from environment.
	 *
	 * @note Takes values from stored environmental values or from system.
	 *
	 * @param[in] type Whether to obtain HTTP or HTTPS settings.
	 * @returns Values as they would be stored in a http(s)_proxy variable (e.g.
	 *     'http://a:aaa@127.0.0.1:3128', 'b:bb@192.168.1.1:3128',
	 *     '172.0.0.1:3128').
	 */
	static
	QByteArray detectEnvironment(enum Type type);

	/*!
	 * @brief Sets environmental variables according to proxy settings.
	 *
	 * @return False on any error.
	 */
	bool setProxyEnvVars(void) const;

	/*!
	 * @brief Returns proxy settings as they are configures.
	 *
	 * @param[in] type Whether to obtain HTTP or HTTPS settings.
	 * @return Proxy settings.
	 */
	ProxySettings proxySettings(enum Type type) const;

	/*!
	 * @brief Returns network proxy object according to stored settings.
	 *
	 * @param[in] type Whether to obtain HTTP or HTTPS settings.
	 * @return QNetworkProxy::HttpProxy proxy if proxy set,
	 *     QNetworkProxy::DefaultProxy otherwise.
	 */
	QNetworkProxy networkProxy(enum Type type) const;

	ProxySettings https; /*!< HTTPS proxy settings. */
	ProxySettings http; /*!< HTTP proxy settings. */
};
