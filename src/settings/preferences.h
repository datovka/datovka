/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

/*!
 * @brief Encapsulates INI-style preferences.
 */
class INIPreferences {
public:
	/*!
	 * @brief Constructor.
	 */
	INIPreferences(void);

	/* Set via command line. */
	QString confSubdir; /*!< Configuration directory. */
	QString loadFromConf; /*!< Configuration file to load from. */
	QString saveToConf; /*!< Configuration file to save to. */

	/*!
	 * @brief Create configuration file if not present.
	 *
	 * @note Location of the configuration file is taken from this
	 *     preferences structure.
	 */
	bool ensureConfPresence(void) const;

	/*!
	 * @brief Return path to configuration directory.
	 *
	 * @return Path to configuration directory.
	 */
	QString confDir(void) const;

	/*!
	 * @brief Returns whole configuration file path.
	 *
	 * @return Whole path to loading configuration file.
	 */
	QString loadConfPath(void) const;

	/*!
	 * @brief Returns whole configuration file path.
	 *
	 * @return Whole path to saving configuration file.
	 */
	QString saveConfPath(void) const;

	/*!
	 * @brief Returns whole account db path.
	 *
	 * @return Whole path to account database file.
	 */
	QString acntDbPath(void) const;

	/*!
	 * @brief Returns whole tag db path.
	 *
	 * @return Whole path to tag database file.
	 */
	QString tagDbPath(void) const;

	/*!
	 * @brief Returns whole records management db path.
	 *
	 * @return Whole path to records management database file.
	 */
	QString recMgmtDbPath(void) const;

	/*!
	 * @brief Return whole preferences db path.
	 *
	 * @return Whole path to preferences database file.
	 */
	QString prefsDbPath(void) const;
};
