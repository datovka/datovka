/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/settings/account.h"

class QSettings; /* Forward declaration. */

class AcntDataPrivate;
/*!
 * @brief Holds account settings and other data.
 */
class AcntData : public AcntSettings {
	Q_DECLARE_PRIVATE(AcntData)

public:
	/*!
	 * @brief Constructor.
	 */
	AcntData(void);
	AcntData(const AcntData &other);
#ifdef Q_COMPILER_RVALUE_REFS
	AcntData(AcntData &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	virtual /* To keep the compiler quiet, the destructor needs not to be virtual here. */
	~AcntData(void);

	AcntData &operator=(const AcntData &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	AcntData &operator=(AcntData &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const AcntData &other) const;
	bool operator!=(const AcntData &other) const;

	friend void swap(AcntData &first, AcntData &second) Q_DECL_NOTHROW;

	bool _createdFromScratch(void) const;
	void _setCreatedFromScratch(bool fromScratch);
	/* Certificate passphrase. Is not saved. */
	const QString &_passphrase(void) const;
	void _setPassphrase(const QString &pp);
#ifdef Q_COMPILER_RVALUE_REFS
	void _setPassphrase(QString &&pp);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QString &_otp(void) const;
	void _setOtp(const QString &otpCode);
#ifdef Q_COMPILER_RVALUE_REFS
	void _setOtp(QString &&otpCode);
#endif /* Q_COMPILER_RVALUE_REFS */
	bool _pwdExpirDlgShown(void) const;
	void _setPwdExpirDlgShown(bool pwdExpirDlgShown);

protected:
	/* This class uses the d_ptr from its parent class. */
	/* Allow subclasses to initialise with their own specific Private. */
	AcntData(AcntDataPrivate *d);

private:
	/* Allow parent code to instantiate *Private subclass. */
	virtual
	bool ensurePrivate(void) Q_DECL_OVERRIDE;
};

void swap(AcntData &first, AcntData &second) Q_DECL_NOTHROW;
