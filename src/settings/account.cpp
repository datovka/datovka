/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QSettings>

#include "src/datovka_shared/settings/account_p.h"
#include "src/settings/account.h"

/* Null objects - for convenience. */
static const QString nullString;

/*!
 * @brief PIMPL AcntData class.
 */
class AcntDataPrivate : public AcntSettingsPrivate {
	//Q_DISABLE_COPY(AcntDataPrivate)
public:
	AcntDataPrivate(void)
	    : AcntSettingsPrivate(), _m_createdFromScratch(false),
	    _m_passphrase(), _m_otp(), _m_pwdExpirDlgShown(false)
	{ }

	virtual
	~AcntDataPrivate(void)
	{ }

	AcntDataPrivate &operator=(const AcntDataPrivate &other) Q_DECL_NOTHROW
	{
		AcntSettingsPrivate::operator=(other);
		_m_createdFromScratch = other._m_createdFromScratch;
		_m_passphrase = other._m_passphrase;
		_m_otp = other._m_otp;
		_m_pwdExpirDlgShown = other._m_pwdExpirDlgShown;

		return *this;
	}

	bool operator==(const AcntDataPrivate &other) const
	{
		return AcntSettingsPrivate::operator==(other) &&
		    (_m_createdFromScratch == other._m_createdFromScratch) &&
		    (_m_passphrase == other._m_passphrase) &&
		    (_m_otp == other._m_otp) &&
		    (_m_pwdExpirDlgShown == other._m_pwdExpirDlgShown);
	}

	/* The following are not stored into the configuration file. */
	bool _m_createdFromScratch; /* Only set on new accounts. */
	QString _m_passphrase;
	QString _m_otp;
	bool _m_pwdExpirDlgShown;
};

AcntData::AcntData(void)
    : AcntSettings()
{
}

AcntData::AcntData(const AcntData &other)
    : AcntSettings((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) AcntDataPrivate) : Q_NULLPTR)
{
	Q_D(AcntData);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
AcntData::AcntData(AcntData &&other) Q_DECL_NOEXCEPT
    : AcntSettings(::std::move(other))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

AcntData::~AcntData(void)
{
}

/*!
 * @brief Ensures private account data presence.
 *
 * @note Returns if private account data could not be allocated.
 */
#define ensureAcntDataPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(!ensurePrivate())) { \
			return _x_; \
		} \
	} while (0)

AcntData &AcntData::operator=(const AcntData &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureAcntDataPrivate(*this);
	Q_D(AcntData);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
AcntData &AcntData::operator=(AcntData &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool AcntData::operator==(const AcntData &other) const
{
	Q_D(const AcntData);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool AcntData::operator!=(const AcntData &other) const
{
	return !operator==(other);
}

bool AcntData::_createdFromScratch(void) const
{
	Q_D(const AcntData);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return false;
	}

	return d->_m_createdFromScratch;
}

void AcntData::_setCreatedFromScratch(bool fromScratch)
{
	ensureAcntDataPrivate();
	Q_D(AcntData);
	d->_m_createdFromScratch = fromScratch;
}

const QString &AcntData::_passphrase(void) const
{
	Q_D(const AcntData);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->_m_passphrase;
}

void AcntData::_setPassphrase(const QString &pp)
{
	ensureAcntDataPrivate();
	Q_D(AcntData);
	d->_m_passphrase = pp;
}

#ifdef Q_COMPILER_RVALUE_REFS
void AcntData::_setPassphrase(QString &&pp)
{
	ensureAcntDataPrivate();
	Q_D(AcntData);
	d->_m_passphrase = ::std::move(pp);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &AcntData::_otp(void) const
{
	Q_D(const AcntData);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->_m_otp;
}

void AcntData::_setOtp(const QString &otpCode)
{
	ensureAcntDataPrivate();
	Q_D(AcntData);
	d->_m_otp = otpCode;
}

#ifdef Q_COMPILER_RVALUE_REFS
void AcntData::_setOtp(QString &&otpCode)
{
	ensureAcntDataPrivate();
	Q_D(AcntData);
	d->_m_otp = ::std::move(otpCode);
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool AcntData::_pwdExpirDlgShown(void) const
{
	Q_D(const AcntData);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return false;
	}

	return d->_m_pwdExpirDlgShown;
}

void AcntData::_setPwdExpirDlgShown(bool pwdExpirDlgShown)
{
	ensureAcntDataPrivate();
	Q_D(AcntData);
	d->_m_pwdExpirDlgShown = pwdExpirDlgShown;
}

AcntData::AcntData(AcntDataPrivate *d)
    : AcntSettings(d)
{
}

bool AcntData::ensurePrivate(void)
{
	if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) {
		AcntDataPrivate *p = new (::std::nothrow) AcntDataPrivate;
		if (Q_UNLIKELY(p == Q_NULLPTR)) {
			Q_ASSERT(0);
			return false;
		}
		d_ptr.reset(p);
	}
	return true;
}

void swap(AcntData &first, AcntData &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
