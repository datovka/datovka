/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>

#include "src/common.h" /* TIMESTAMP_EXPIR_BEFORE_DAYS, TIMER_MARK_MSG_READ_MS, ISDS_DOWNLOAD_TIMEOUT_MS, RECMGMT_COMMUNICATION_TIMEOUT_MS */
#include "src/datovka_shared/localisation/localisation.h"
#include "src/datovka_shared/settings/prefs.h"
#include "src/io/export_file_name.h"
#include "src/settings/prefs_defaults.h"
#include "src/settings/prefs_specific.h"

void PrefsDefaults::setDefaults(Prefs &prefs)
{
	prefs.setDefault(Prefs::Entry(
	    "window.main.position.maximised",
	    PrefsDb::VAL_BOOLEAN, false));

	prefs.setDefault(Prefs::Entry(
	    "network.proxy.http",
	    PrefsDb::VAL_STRING, QString()));
	prefs.setDefault(Prefs::Entry(
	    "network.proxy.http.password.base64",
	    PrefsDb::VAL_STRING, QString()));
	prefs.setDefault(Prefs::Entry(
	    "network.proxy.http.username",
	    PrefsDb::VAL_STRING, QString()));
	prefs.setDefault(Prefs::Entry(
	    "network.proxy.https",
	    PrefsDb::VAL_STRING, QString()));
	prefs.setDefault(Prefs::Entry(
	    "network.proxy.https.password.base64",
	    PrefsDb::VAL_STRING, QString()));
	prefs.setDefault(Prefs::Entry(
	    "network.proxy.https.username",
	    PrefsDb::VAL_STRING, QString()));

	prefs.setDefault(Prefs::Entry(
	    "global.path.import.database",
	    PrefsDb::VAL_STRING, QDir::homePath()));

	prefs.setDefault(Prefs::Entry(
	    "action.sync_account.tie.action.download_message",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "action.sync_account.tie.action.check_downloaded_message_list",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "storage.database.messages.on_disk.enabled",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "storage.database.accounts.on_disk.enabled",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "storage.client.tags.enabled",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "storage.client.tags.communication.timeout.seconds",
	    PrefsDb::VAL_INTEGER, 5)); /* 5 seconds */
	prefs.setDefault(Prefs::Entry(
	    "storage.client.tags.keep_alive.timer.period.seconds",
	    PrefsDb::VAL_INTEGER, 120)); /* 2 minutes */
	prefs.setDefault(Prefs::Entry(
	    "storage.client.tags.reconnect.timer.delay.seconds",
	    PrefsDb::VAL_INTEGER, 10)); /* 10 seconds */
	prefs.setDefault(Prefs::Entry(
	    "storage.client.tags.reconnect.timer.period.seconds",
	    PrefsDb::VAL_INTEGER, 60)); /* 1 minute */
	prefs.setDefault(Prefs::Entry(
	    "window.main.action.sync_all_accounts.background.enabled",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "window.main.action.sync_all_accounts.background.timer.period.minutes",
	    PrefsDb::VAL_INTEGER, TIMER_DEFAULT_TIMEOUT_MS / 60000));
	prefs.setDefault(Prefs::Entry(
	    "window.main.action.shadow_sync_all_accounts.background.timer.period.minutes",
	    PrefsDb::VAL_INTEGER, TIMER_DEFAULT_TIMEOUT_MS / 60000));
	prefs.setDefault(Prefs::Entry(
	    "window.main.on.start.tie.action.sync_all_accounts",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "application.version.check.enabled",
	    PrefsDb::VAL_BOOLEAN, PrefsSpecific::checkNewVersionsDefault()));
	prefs.setDefault(Prefs::Entry(
	    "application.version.check.timeout.seconds",
	    PrefsDb::VAL_INTEGER, 14400)); /* 4 hours cool down */
	prefs.setDefault(Prefs::Entry(
	    "application.update.automatic_download.enabled",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "application.update.automatic_download.attempt.max",
	    PrefsDb::VAL_INTEGER, 2));
	prefs.setDefault(Prefs::Entry(
	    "application.update.downloaded_package.version",
	    PrefsDb::VAL_STRING, QString()));
	prefs.setDefault(Prefs::Entry(
	    "application.update.downloaded_package.sha256sum",
	    PrefsDb::VAL_STRING, QString()));
	prefs.setDefault(Prefs::Entry(
	    "application.update.downloaded_package.path",
	    PrefsDb::VAL_STRING, QString()));
	prefs.setDefault(Prefs::Entry(
	    "network.isds.communication.timeout.ms",
	    PrefsDb::VAL_INTEGER, ISDS_DOWNLOAD_TIMEOUT_MS));
	prefs.setDefault(Prefs::Entry(
	    "network.isds.keep_alive.on.fail.quit_session",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "network.isds.keep_alive.timer.period.seconds",
	    PrefsDb::VAL_INTEGER, 1500)); /*
	                                   * 25 minutes;
	                                   * MEP and OTP cookies are valid for
	                                   * 30 minutes of inactivity according
	                                   * to the Operating Rules of ISDS.
	                                   */
	prefs.setDefault(Prefs::Entry(
	    "network.isds.keep_alive.accounts.uname_pwd.enabled",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "network.isds.keep_alive.accounts.uname_crt.enabled",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "network.isds.keep_alive.accounts.uname_pwd_crt.enabled",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "network.isds.keep_alive.accounts.uname_pwd_hotp.enabled",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "network.isds.keep_alive.accounts.uname_pwd_totp.enabled",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "network.isds.keep_alive.accounts.uname_mep.enabled",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "network.records_management.communication.timeout.ms",
	    PrefsDb::VAL_INTEGER, RECMGMT_COMMUNICATION_TIMEOUT_MS));
	prefs.setDefault(Prefs::Entry(
	    "window.main.message_list.read.mark_read.timeout.ms",
	    PrefsDb::VAL_INTEGER, TIMER_MARK_MSG_READ_MS));
	prefs.setDefault(Prefs::Entry(
	    "crypto.message.validation.date.type",
	    PrefsDb::VAL_INTEGER, PrefsSpecific::DOWNLOAD_DATE));
	prefs.setDefault(Prefs::Entry(
	    "crypto.crl.check.enabled",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "crypto.message.timestamp.expiration.notify_ahead.days",
	    PrefsDb::VAL_INTEGER, TIMESTAMP_EXPIR_BEFORE_DAYS));
	prefs.setDefault(Prefs::Entry(
	    "window.main.message_list.select.on.account_change",
	    PrefsDb::VAL_INTEGER, PrefsSpecific::SELECT_NOTHING));
	prefs.setDefault(Prefs::Entry(
	    "window.main.toolbar.button.style",
	    PrefsDb::VAL_INTEGER, PrefsSpecific::TEXT_UNDER_ICON));
	prefs.setDefault(Prefs::Entry(
	    "global.path.enabled",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "global.path.save.attachment",
	    PrefsDb::VAL_STRING, QDir::homePath()));
	prefs.setDefault(Prefs::Entry(
	    "global.path.load.attachment",
	    PrefsDb::VAL_STRING, QDir::homePath()));
	prefs.setDefault(Prefs::Entry(
	    "action.save_all_attachments.tie.action.export_message_zfo",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "action.save_all_attachments.tie.action.export_acceptance_info_zfo",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "action.save_all_attachments.tie.action.export_envelope_pdf",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "action.save_all_attachments.tie.action.export_acceptance_info_pdf",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "action.save_all_attachments.every_attachment.export_acceptance_info",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "file_system.format.filename.message",
	    PrefsDb::VAL_STRING, DEFAULT_MESSAGE_FILENAME_FORMAT));
	prefs.setDefault(Prefs::Entry(
	    "file_system.format.filename.acceptance_info",
	    PrefsDb::VAL_STRING, DEFAULT_ACCEPTANCE_INFO_FILENAME_FORMAT));
	prefs.setDefault(Prefs::Entry(
	    "file_system.format.filename.attachment",
	    PrefsDb::VAL_STRING, DEFAULT_ATTACHMENT_FILENAME_FORMAT));
	prefs.setDefault(Prefs::Entry(
	    "file_system.format.filename.every_attachment_acceptance_info",
	    PrefsDb::VAL_STRING, DEFAULT_ATTACHMENT_ACCEPTANCE_FORMAT));
	prefs.setDefault(Prefs::Entry(
	    "translation.language",
	    PrefsDb::VAL_STRING, Localisation::langSystem));

	prefs.setDefault(Prefs::Entry(
	    "records_management.action.automatic_upload.enabled",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "records_management.action.automatic_account_status_upload.enabled",
	    PrefsDb::VAL_BOOLEAN, true));

	prefs.setDefault(Prefs::Entry(
	    "file_system.temporary.files.store.temp_path.enabled",
	    PrefsDb::VAL_BOOLEAN, false));

	prefs.setDefault(Prefs::Entry(
	    "message.attachment.zfo.direct_view.enabled",
	    PrefsDb::VAL_BOOLEAN, true));

	prefs.setDefault(Prefs::Entry(
	    "window.create_message.notification.send_pdz.enabled",
	    PrefsDb::VAL_BOOLEAN, true));

	prefs.setDefault(Prefs::Entry(
	    "window.about.notification.update.quit_app.enabled",
	    PrefsDb::VAL_BOOLEAN, true));

	prefs.setDefault(Prefs::Entry(
	    "action.send_message.tie.action.download_message.timeout.ms",
	    PrefsDb::VAL_INTEGER, 5000));

	prefs.setDefault(Prefs::Entry(
	    "action.edit_tag_assignment.tie.simplified_tag_assignment.enabled",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "action.edit_tag_assignment.simplified_tag_assignment.window_decorations.enabled",
	    PrefsDb::VAL_BOOLEAN, true));

	prefs.setDefault(Prefs::Entry(
	    "message.limit.megabyte.bytes",
	    PrefsDb::VAL_INTEGER, 1000000));
	prefs.setDefault(Prefs::Entry(
	    "message.basic.limit.attachments.megabytes.max",
	    PrefsDb::VAL_INTEGER, 20));
	prefs.setDefault(Prefs::Entry(
	    "message.some_ovm.limit.attachments.megabytes.max",
	    PrefsDb::VAL_INTEGER, 50));
	prefs.setDefault(Prefs::Entry(
	    "message.vodz.limit.attachments.megabytes.max",
	    PrefsDb::VAL_INTEGER, 100));
	prefs.setDefault(Prefs::Entry(
	    "message.limit.attachment.count.max",
	    PrefsDb::VAL_INTEGER, 50));
	prefs.setDefault(Prefs::Entry(
	    "window.send_message.vodz.enable",
	    PrefsDb::VAL_BOOLEAN, true));

	prefs.setDefault(Prefs::Entry(
	    "window.main.message.isds_deletion.notify_ahead.days",
	    PrefsDb::VAL_INTEGER, 0)); /* Disabled by default. */
	prefs.setDefault(Prefs::Entry(
	    "window.main.message.isds_deletion.notify_ahead.colour",
	    PrefsDb::VAL_COLOUR, "800000"));

	prefs.setDefault(Prefs::Entry(
	    "file_system.format.filename.export.truncate.max_chars",
	    PrefsDb::VAL_INTEGER, 50));

	prefs.setDefault(Prefs::Entry(
	    "stats.app.report.usage.send.enabled",
	    PrefsDb::VAL_BOOLEAN, false));

	prefs.setDefault(Prefs::Entry(
	    "application.ui.theme",
	    PrefsDb::VAL_INTEGER, 0));
	prefs.setDefault(Prefs::Entry(
	    "application.ui.theme.custom.font_scale.percent",
	    PrefsDb::VAL_INTEGER, 100));
}
