/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QtGlobal> /* QT_VERSION_CHECK */

#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/io/tag_container.h"

class QByteArray; /* Forward declaration. */
class QSettings; /* Forward declaration. */
class QString; /* Forward declaration. */

class TagContainerSettingsPrivate;
/*!
 * @brief Holds tag container settings.
 */
class TagContainerSettings : public QObject {
	Q_OBJECT
	Q_DECLARE_PRIVATE(TagContainerSettings)

public:
	TagContainerSettings(void);
	TagContainerSettings(const TagContainerSettings &other);
#ifdef Q_COMPILER_RVALUE_REFS
	TagContainerSettings(TagContainerSettings &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	~TagContainerSettings(void);

	/*!
	 * @note Emits contentChanged().
	 */
	TagContainerSettings &operator=(const TagContainerSettings &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	TagContainerSettings &operator=(TagContainerSettings &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const TagContainerSettings &other) const;
	bool operator!=(const TagContainerSettings &other) const;

	friend void swap(TagContainerSettings &first, TagContainerSettings &second) Q_DECL_NOTHROW;

	bool isNull(void) const;

	/* Active backend. */
	enum TagContainer::Backend backend(void) const;
	void setBackend(enum TagContainer::Backend b);
	/* Client server hostname. */
	const QString &clientHostname(void) const;
	void setClientHostname(const QString &h);
#ifdef Q_COMPILER_RVALUE_REFS
	void setClientHostname(QString &&h);
#endif /* Q_COMPILER_RVALUE_REFS */
	/* Client server port number. */
	int clientPort(void) const;
	void setClientPort(int p);
	/* CA certificate. */
	const QString &certPath(void) const;
	void setCertPath(const QString &p);
#ifdef Q_COMPILER_RVALUE_REFS
	void setCertPath(QString &&p);
#endif /* Q_COMPILER_RVALUE_REFS */
	/* Username. */
	const QString &username(void) const;
	void setUsername(const QString &n);
#ifdef Q_COMPILER_RVALUE_REFS
	void setUsername(QString &&n);
#endif /* Q_COMPILER_RVALUE_REFS */

	const QString &password(void) const;
	void setPassword(const QString &pwd);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPassword(QString &&pwd);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QString &pwdAlg(void) const;
	void setPwdAlg(const QString &pwdAlg);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPwdAlg(QString &&pwdAlg);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QByteArray &pwdSalt(void) const;
	void setPwdSalt(const QByteArray &pwdSalt);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPwdSalt(QByteArray &&pwdSalt);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QByteArray &pwdIv(void) const;
	void setPwdIv(const QByteArray &pwdIv);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPwdIv(QByteArray &&pwdIv);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QByteArray &pwdCode(void) const;
	void setPwdCode(const QByteArray &pwdCode);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPwdCode(QByteArray &&pwdCode);
#endif /* Q_COMPILER_RVALUE_REFS */

	qint64 profileId(void) const;
	void setProfileId(qint64 i);
	const QString &profileName(void) const;
	void setProfileName(const QString &n);
#ifdef Q_COMPILER_RVALUE_REFS
	void setProfileName(QString &&n);
#endif /* Q_COMPILER_RVALUE_REFS */

	/*!
	 * @brief Used to decrypt the password.
	 *
	 * @param[in] oldPin PIN value used to decrypt old passwords.
	 */
	void decryptPassword(const QString &oldPin);

	void loadFromSettings(const QSettings &settings);
	void saveToSettings(const QString &pinVal, QSettings &settings) const;

Q_SIGNALS:
	/*!
	 * @brief Emitted when content changed.
	 */
	void contentChanged(void);

private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
	::std::unique_ptr<TagContainerSettingsPrivate> d_ptr;
#else /* < Qt-5.12 */
	QScopedPointer<TagContainerSettingsPrivate> d_ptr;
#endif /* >= Qt-5.12 */
};

void swap(TagContainerSettings &first, TagContainerSettings &second) Q_DECL_NOTHROW;
