/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QStringBuilder>
#include <QStringList>

#include "src/io/filesystem.h"
#include "src/settings/preferences.h"

/*! Default configuration folder location. */
#define DFLT_CONF_SUBDIR ".dsgui"
/*! Default configuration file name. */
#define DFLT_CONF_FILE "dsgui.conf"

/* Fixed settings. */
static const QString acntDbFile("messages.shelf.db"); /*!< Account db file. */
static const QString tagDbFile("tag.db"); /*!< Tag db file. */
static const QString recMgmtDbFile("records_management.db"); /*!< Records management db file. */
static const QString prefsDbFile("prefs.db"); /*!< Preferences db file. */

INIPreferences::INIPreferences(void)
    : confSubdir(DFLT_CONF_SUBDIR),
    loadFromConf(DFLT_CONF_FILE),
    saveToConf(DFLT_CONF_FILE)
{
}

/*!
 * @brief Search for auxiliary file in specified location.
 *
 * @note The file should be created using the auxConfFilePath() function.
 *
 * @param[in] confDir Directory to search in.
 * @param[in] confFileName Normal configuration file name (without the aux suffix).
 * @return Path to the latest found file.
 */
static
QString findAuxFilePath(const QString &confDir, const QString &confFileName)
{
	QStringList auxFiles;

	{
		QDirIterator dirIt(confDir, {confFileName + ".aux.*"},
		    QDir::Files | QDir::NoDotAndDotDot,
		    QDirIterator::NoIteratorFlags);
		while (dirIt.hasNext()) {
			dirIt.next();

			auxFiles.append(dirIt.fileName());
		}
	}

	if (auxFiles.isEmpty()) {
		return QString();
	}

	/* File name contains creation time. */
	auxFiles.sort();

	/* Use last matching found. */
	return confDir % QDir::separator() % auxFiles.last();
}

bool INIPreferences::ensureConfPresence(void) const
{
	{
		QDir dir(confDir());
		if (!dir.exists()) {
			if (!dir.mkpath(".")) {
				return false;
			}
		}
	}

	/* Try finding an auxiliary file and renaming it to the original. */
	if (!QFileInfo(loadConfPath()).exists()) {
		const QString auxFilePath = findAuxFilePath(confDir(), loadFromConf);

		if (!auxFilePath.isEmpty()) {
			/* Rename found file. */
			QFile::rename(auxFilePath, loadConfPath());
		}
	}

	{
		/* Create empty file if still missing. */
		QFile file(loadConfPath());
		if (!file.exists()) {
			if (!file.open(QIODevice::ReadWrite)) {
				return false;
			}
			file.close();
		}
	}
	return true;
}

QString INIPreferences::confDir(void) const
{
	return confDirPath(confSubdir);
}

QString INIPreferences::loadConfPath(void) const
{
	return confDir() % QDir::separator() % loadFromConf;
}

QString INIPreferences::saveConfPath(void) const
{
	return confDir() % QDir::separator() % saveToConf;
}

QString INIPreferences::acntDbPath(void) const
{
	return confDir() % QDir::separator() % acntDbFile;
}

QString INIPreferences::tagDbPath(void) const
{
	return confDir() % QDir::separator() % tagDbFile;
}

QString INIPreferences::recMgmtDbPath(void) const
{
	return confDir() % QDir::separator() % recMgmtDbFile;
}

QString INIPreferences::prefsDbPath(void) const
{
	return confDir() % QDir::separator() % prefsDbFile;
}
