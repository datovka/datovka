/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

namespace SysDetect {

	/*!
	 * @brief Application package type.
	 */
	enum PkgType {
		PKG_UNKNOWN = 0, /*!< Convenience value. */
		PKG_WIN32_PORTABLE,
		PKG_WIN32_INST_EXE,
		PKG_WIN32_INST_MSI,
		PKG_WIN64_PORTABLE,
		PKG_WIN64_INST_EXE,
		PKG_WIN64_INST_MSI,
		PKG_MACOS32_DMG, /*!< Mac OS X on i386; 32-bit */
		PKG_MACOS64_DMG, /*!< OS X on x86_64; 64-bit */
		PKG_MACOS_ARM64_DMG /*!< macOS on amd64; 64-bit. */
	};

	/*!
	 * @brief Returns package type as printable string.
	 *
	 * @param[in] pkgType Application package type.
	 * @return String containing package type name.
	 */
	const QString pkgTypeName(enum PkgType pkgType);

	/*!
	 * @brief Return the detected package installation type.
	 *
	 * @return Application package type.
	 */
	enum PkgType detectPackageType(void);

	/*!
	 * @brief Returns true on supported Windows versions.
	 *
	 * @return True when supported.
	 */
	bool canInstallPortableVersion(void);

}
