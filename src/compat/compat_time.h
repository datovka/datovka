/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#ifdef __cplusplus
#  include <cstdint> /* int64_t */
#else
#  include <stdint.h> /* int64_t */
#endif

struct tm; /* Forward declaration. */

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * @brief Qt-based implementation of timegm() which always uses 64-bit time value.
 *
 * @param[in] tm Broken time in UTC.
 * @return Seconds since epoch, -1 on error.
 */
int64_t qtBasedTimegm(struct tm *tm);

/*!
 * @brief Qt-based implementation of gmtime_r() which always uses 64-bit time value.
 *
 * @param[in]     timep Pointer to seconds since epoch counter.
 * @param[in,out] result Stucture where to store the data.
 * @result Pointer to broken time in UTC, NULL on error.
 */
struct tm *qtBasedGmtime_r(const int64_t *timep, struct tm *result);

#ifdef __cplusplus
} /* extern "C" */
#endif
