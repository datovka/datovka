/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#ifdef WIN32
#include "src/compat/compat_win64.h"

/*
 * https://docs.microsoft.com/en-us/windows/win32/api/wow64apiset/nf-wow64apiset-iswow64process
 * https://stackoverflow.com/questions/23696749/how-to-detect-on-c-is-windows-32-or-64-bit
 */

#include <tchar.h>
#include <windows.h>

typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);

LPFN_ISWOW64PROCESS fnIsWow64Process;

bool isWow64(void)
{
	BOOL bIsWow64 = FALSE;

	/*
	 * IsWow64Process is not available on all supported versions of Windows.
	 * Use GetModuleHandle to get a handle to the DLL that contains the function
	 * and GetProcAddress to get a pointer to the function if available.
	 */

	fnIsWow64Process = (LPFN_ISWOW64PROCESS) GetProcAddress(
	    GetModuleHandle(TEXT("kernel32")), "IsWow64Process");

	if (NULL != fnIsWow64Process) {
		if (!fnIsWow64Process(GetCurrentProcess(), &bIsWow64)) {
			/* Ignore error. */
		}
	}

	return bIsWow64;
}

#endif /* WIN32 */
