/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QLabel>

#include "src/delegates/account_status_delegate.h"
#include "src/models/account_status_model.h"

class AccountsMap; /* Forward declaration. */
class QListView; /* Forward declaration. */
class QPushButton; /* Forward declaration. */

/*!
 * @brief Account status control widget.
 */
class AccountStatusControl : public QLabel {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] regularAccounts Regular account container.
	 * @param[in] shadowAccounts Shadow account container.
	 * @param[in] sessions Session container.
	 * @param[in] parent Parent widget.
	 */
	explicit AccountStatusControl(AccountsMap *regularAccounts,
	    AccountsMap *shadowAccounts, IsdsSessions *sessions,
	    QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Destructor.
	 */
	~AccountStatusControl(void);

protected:
	/*!
	 * @brief Raises status list widget.
	 *
	 * @param[in] event Mouse press event.
	 */
	virtual
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;

private Q_SLOTS:
	/*!
	 * @brief Terminates all active sessions.
	 */
	void disconnectAllSessions(void);

	/*!
	 * @brief Update status description according to session changes.
	 *
	 * @param[in] username Username identifying added/removed the session.
	 * @param[in] nowActive Number of active sessions after the session has been added.
	 */
	void watchSessionChanges(const QString &username, int nowActive);

	/*!
	 * @brief Updates persistent editor settings based on account status model.
	 */
	void watchRegularModelReset(void);

	/*!
	 * @brief Updates persistent editor settings based on account status model.
	 */
	void watchRegularRowsInserted(const QModelIndex &parent, int first, int last);

	/*!
	 * @brief Updates persistent editor settings based on account status model.
	 */
	void watchShadowModelReset(void);

	/*!
	 * @brief Updates persistent editor settings based on account status model.
	 */
	void watchShadowRowsInserted(const QModelIndex &parent, int first, int last);

private:
	IsdsSessions *m_sessions; /*!< Session container. */

	QWidget *m_accountPopup; /*!< Account status pop-up. */

	AccountStatusDelegate m_regularAccountStatusDelegate; /*!< Responsible for painting. */
	AccountStatusModel m_regularAccountStatusModel; /*!< Regular account status model. */
	QListView *m_regularAccountStatusView; /*!< Regular account status list. */

	AccountStatusDelegate m_shadowAccountStatusDelegate; /*!< Responsible for painting. */
	AccountStatusModel m_shadowAccountStatusModel; /*!< Shadow account status model. */
	QListView *m_shadowAccountStatusView; /*!< Shadow account status list. */

	QPushButton *m_disconnectAllButton; /*!< Disconnect all sessions button. */
};
