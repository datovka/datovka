/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QLabel>

class TagContainer; /* Forward declaration. */

/*!
 * @brief Tags control label.
 */
class TagsLabelControlWidget : public QLabel {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] tagCont Tag container.
	 * @param[in] parent Parent widget.
	 */
	explicit TagsLabelControlWidget(TagContainer *tagCont,
	    QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Destructor.
	 */
	~TagsLabelControlWidget(void);

Q_SIGNALS:
	/*!
	 * @brief Emitted when clicked on label.
	 */
	void clicked(void);

protected:
	/*!
	 * @brief Emits clicked().
	 *
	 * @param[in] event Mouse press event.
	 */
	virtual
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;

private Q_SLOTS:
	/*!
	 * @brief Action when tag client is connected.
	 */
	void watchTagContConnected(void);

	/*!
	 * @brief Action when tag client is disconnected.
	 */
	void watchTagContDisconnected(void);

	/*!
	 * @brief Action when tag container is reset.
	 */
	void watchTagContReset(void);

private:
	TagContainer *m_tagContainer; /*!< Tag container. */
};
