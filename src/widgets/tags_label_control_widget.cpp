/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/io/tag_client.h"
#include "src/io/tag_container.h"
#include "src/widgets/tags_label_control_widget.h"

/*!
 * @brief Set status label with tag container status description.
 *
 * @param[out] label Label whose test has to be set.
 * @param[in] tagCont Tag container whose status is read.
 */
static
void setTagStorageLabel(QLabel &label, TagContainer &tagCont)
{
	QString labelText;

	switch (tagCont.backend()) {
	case TagContainer::BACK_DB:
		labelText = TagsLabelControlWidget::tr("Tags: %1")
		    .arg(TagsLabelControlWidget::tr("local storage"));
		break;
	case TagContainer::BACK_CLIENT:
		{
			TagClient *client = tagCont.backendClient();
			if (Q_UNLIKELY(client == Q_NULLPTR)) {
				Q_ASSERT(0);
				labelText = TagsLabelControlWidget::tr("Tags: %1")
				    .arg(TagsLabelControlWidget::tr("error"));
			}
			labelText = TagsLabelControlWidget::tr("Tags: %1")
			    .arg(client->isConnected() ?
			        TagsLabelControlWidget::tr("connected") :
			        TagsLabelControlWidget::tr("disconnected"));
		}
		break;
	default:
		Q_ASSERT(0);
		labelText = TagsLabelControlWidget::tr("Tags: %1")
		    .arg(TagsLabelControlWidget::tr("error"));
		break;
	}

	label.setText(labelText + "   ");
}

TagsLabelControlWidget::TagsLabelControlWidget(TagContainer *tagCont,
    QWidget *parent)
    : QLabel(parent),
    m_tagContainer(tagCont)
{
	if (Q_NULLPTR != m_tagContainer) {
		connect(m_tagContainer, SIGNAL(connected()),
		    this, SLOT(watchTagContConnected()));
		connect(m_tagContainer, SIGNAL(disconnected()),
		    this, SLOT(watchTagContDisconnected()));
		connect(m_tagContainer, SIGNAL(reset()),
		    this, SLOT(watchTagContReset()));

		setTagStorageLabel(*this, *m_tagContainer);
	}
}

TagsLabelControlWidget::~TagsLabelControlWidget(void)
{
}

void TagsLabelControlWidget::mousePressEvent(QMouseEvent *event)
{
	Q_UNUSED(event);

	Q_EMIT clicked();
}

void TagsLabelControlWidget::watchTagContConnected(void)
{
	setTagStorageLabel(*this, *m_tagContainer);
}

void TagsLabelControlWidget::watchTagContDisconnected(void)
{
	setTagStorageLabel(*this, *m_tagContainer);
}

void TagsLabelControlWidget::watchTagContReset(void)
{
	setTagStorageLabel(*this, *m_tagContainer);
}
