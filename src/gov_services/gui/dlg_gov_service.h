/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>
#include <QSet>
#include <QString>

#include "src/datovka_shared/isds/message_interface.h"
#include "src/identifiers/account_id_db.h"

namespace Gov {
	class Service; /* Forward declaration. */
}

namespace Ui {
	class DlgGovService;
}

/*!
 * @brief Encapsulated e-gov service dialogue.
 */
class DlgGovService : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @note The dialogue takes the ownership of the service `gs` and
	 *     deletes it when destroyed.
	 *
	 * @param[in] acntIdDb Account descriptor.
	 * @param[in] gs Pointer holding e-gov service.
	 * @param[in] parent Parent widget.
	 */
	explicit DlgGovService(const AcntIdDb &acntIdDb, Gov::Service *gs,
	    QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	virtual
	~DlgGovService(void);

	/*!
	 * @brief Open e-gov service form dialogue.
	 *
	 * @param[in] acntIdDb Account descriptor.
	 * @param[in] cgs Pointer holding e-gov service.
	 * @param[in] dbSet Pointer holding account db set.
	 * @param[in] parent Parent widget.
	 */
	static
	void openForm(const AcntIdDb &acntIdDb, const Gov::Service *cgs,
	    QWidget *parent = Q_NULLPTR);

private slots:
	/*!
	 * @brief Activated when line edit text has been changed.
	 *
	 * @param[in] text New text.
	 */
	void lineEditTextChanged(const QString &text);

	/*!
	 * @brief Activated when calendar date has been changed.
	 *
	 * @param[in] date New date.
	 */
	void calendarDateChanged(void);

	/*!
	 * @brief Create data message with e-gov request send to ISDS.
	 */
	void createAndSendMsg(void);

	/*!
	 * @brief Show status after sending e-gov message via ISDS interface.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] transactId Unique transaction identifier.
	 * @param[in] result Sending status.
	 * @param[in] resultDesc Result description string.
	 * @param[in] dbIDRecipient Recipient data box identifier.
	 * @param[in] recipientName Recipient data box name.
	 * @param[in] isPDZ True if message was attempted to send as commercial
	 *                  message.
	 * @param[in] isVodz True if message was a high-volume message.
	 * @param[in] dmId Sent message identifier.
	 * @param[in] processFlags Message processing flags.
	 * @param[in] recMgmtHierarchyId Predefined target record management hierarchy id.
	 */
	void collectSendMessageStatus(const AcntId &acntId,
	    const QString &transactId, int result, const QString &resultDesc,
	    const QString &dbIDRecipient, const QString &recipientName,
	    bool isPDZ, bool isVodz, qint64 dmId, int processFlags,
	    const QString &recMgmtHierarchyId);

private:
	/*!
	 * @brief Initialise e-gov service dialogue.
	 */
	void initDialogue(void);

	/*!
	 * @brief Check whether all mandatory fields are filled. Activate
	 *     send button.
	 */
	void updateSendReadiness(void);

	/*!
	 * @brief Generate form UI layout from service fields.
	 */
	void generateFormLayoutUi(void);

	/*!
	 * @brief Is active when some form field has invalid value.
	 *
	 * @param[in] errText Error text.
	 */
	void showValidityNotification(const QString &errText);

	/*!
	 * @brief Send e-gov message to ISDS.
	 *
	 * @param[in] msg Message structure.
	 */
	void sendGovMessage(const Isds::Message &msg);

	Ui::DlgGovService *m_ui; /*!< UI generated from UI file. */

	AcntIdDb m_acntIdDb; /*!< Account descriptor. */
	Gov::Service *m_gs; /*!< Pointer holding e-gov service. */
	QSet<QString> m_transactIds; /*!< Temporary transaction identifiers. */
};
