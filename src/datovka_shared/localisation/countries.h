/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QPair>
#include <QString>

/* this file contains UTF-8 encoded strings. */

namespace Localisation {

	/*!
	 * @brief Country entries as defined in
	 *     pril_2/WS_vyhledavani_datovych_schranek.pdf, Appendix A.
	 */
	struct countryEntry {
		const char *code; /*!< Two upper-case country code. */
		const char *name_cz; /*!< Czech country name. UTF-8 encoded. */
	};

	extern struct countryEntry countryList[]; /*!< NULL-terminated list of countries. */

	/*!
	 * @brief Get number of countries in the listing.
	 *
	 * @param[in] countryList Array of countries.
	 * @return Length of the array.
	 */
	int countryListSize(const struct countryEntry *countryList);

	/*!
	 * @brief Get position of entry with desired country code.
	 *
	 * @param[in] countryList Array of countries.
	 * @param[in] code Country code.
	 * @return Position of entry, -1 if nothing found.
	 */
	int posOfCountryCode(const struct countryEntry *countryList,
	    const QString &code);

	/*!
	 * @brief Get country code at given position.
	 *
	 * @param[in] countryList Array of countries.
	 * @param[in] pos Position.
	 * @return Country code, null string on error.
	 */
	QString countryCodeAt(const struct countryEntry *countryList, int pos);

	/*!
	 * @brief Get country name in Czech at given position.
	 *
	 * @param[in] countryList Array of countries.
	 * @param[in] pos Position.
	 * @return Country name in Czech, null string on error.
	 */
	QString countryNameCzAt(const struct countryEntry *countryList, int pos);

	/*!
	 * @brief Get list of country entries in Czech suitable to be used
	 *     in combo boxes.
	 *
	 * @param[in] countryList Array of countries.
	 * @return List of pairs containing country names and corresponding
	 *     position in \a countryList. Returns empty list on error.
	 */
	QList< QPair<QString, int> > countryNamesCz(
	    const struct countryEntry *countryList);

	/*!
	 * @brief Get list of country entries in English suitable to be used
	 *     in combo boxes.
	 *
	 * @note English country names are derived from available entries
	 *     in QLocale. If no corresponding country name can be found then
	 *     the resulting name contains only the country code.
	 *
	 * @param[in] countryList Array of countries.
	 * @return List of pairs containing country names and corresponding
	 *     position in \a countryList. Returns empty list on error.
	 */
	QList< QPair<QString, int> > countryNamesEn(
	    const struct countryEntry *countryList);

}
