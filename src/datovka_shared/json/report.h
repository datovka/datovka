/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

class QJsonValue; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Json {

	namespace Report {
		enum AppType {
			AT_UNKNOWN = 0,
			AT_TESTING = 1, /*!< Testing application. */
			AT_DESKTOP = 2, /*!< Desktop application. */
			AT_MOBILE = 3 /*!< Mobile application. */
		};
	}

	class BuildReportPrivate;
	/*!
	 * @brief Writes build environment information into JSON.
	 *
	 * "at" - application type, int, enum
	 * "bca" - build CPU architecture, string
	 * "mui" - unique ID, string
	 * "ppn" - pretty product name, string
	 * "qcv" - Qt compilation version, string
	 * "qrv" - Qt runtime version, string
	 * "rca" - runtime CPU architecture, string
	 * "v" - report version, string, int
	 *
	 * Generated JSON example:
	 * {
	 *   "at":1,
	 *   "bca":"x86_64",
	 *   "mui":"YjUzOWQ2NDYxMzFkNDA4N2E2OGM4OTJiMDAwMDAwMTM=",
	 *   "ppn":"Gentoo Linux",
	 *   "qcv":"5.15.2",
	 *   "qrv":"5.15.2",
	 *   "rca":"x86_64",
	 *   "v":"0"
	 * }
	 */
	class BuildReport : public Object {
		Q_DECLARE_PRIVATE(BuildReport)

	public:
		BuildReport(void);
		BuildReport(const BuildReport &other);
#ifdef Q_COMPILER_RVALUE_REFS
		BuildReport(BuildReport &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~BuildReport(void);

		BuildReport &operator=(const BuildReport &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		BuildReport &operator=(BuildReport &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const BuildReport &other) const;
		bool operator!=(const BuildReport &other) const;

		static
		void swap(BuildReport &first, BuildReport &second) Q_DECL_NOTHROW;

		bool isNull(void) const;
		bool isValid(void) const;

		enum Report::AppType appType(void) const;
		void setAppType(enum Report::AppType at);

		const QString &uniqueId(void) const;
		void setUniqueId(const QString &ui);
#ifdef Q_COMPILER_RVALUE_REFS
		void setUniqueId(QString &&ui);
#endif /* Q_COMPILER_RVALUE_REFS */

	private:
		/*!
		 * @brief Creates an instance from supplied JSON data.
		 *
		 * @param[in]  jsonVal JSON object value.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		BuildReport fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

	public:
		/*!
		 * @brief Writes content of JSON object.
		 *
		 * @note Unspecified values are omitted.
		 *
		 * @param[out] jsonValue JSON value to write to.
		 * @return True on success.
		 */
		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<BuildReportPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<BuildReportPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	class ConfReportPrivate;
	/*!
	 * @brief Writes app configuration information into JSON.
	 *
	 * "at" - application type, int, enum
	 * "av" - application version, string
	 * "bqz" - built with bundled QuaZIP, int, bool
	 * "mui" - unique ID, string
	 * "port" - built as portable application, int, bool
	 * "tags" - built with tag support, int, bool
	 * "v" - report version, string, int
	 *
	 * Generated JSON example:
	 * {
	 *   "at":1,
	 *   "av":"4.23.6",
	 *   "bqz":0,
	 *   "mui":"YjUzOWQ2NDYxMzFkNDA4N2E2OGM4OTJiMDAwMDAwMTM=",
	 *   "port":0,
	 *   "tags":0,
	 *   "v":"0"
	 * }
	 */
	class ConfReport : public Object {
		Q_DECLARE_PRIVATE(ConfReport)

	public:
		ConfReport(void);
		ConfReport(const ConfReport &other);
#ifdef Q_COMPILER_RVALUE_REFS
		ConfReport(ConfReport &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~ConfReport(void);

		ConfReport &operator=(const ConfReport &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		ConfReport &operator=(ConfReport &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const ConfReport &other) const;
		bool operator!=(const ConfReport &other) const;

		static
		void swap(ConfReport &first, ConfReport &second) Q_DECL_NOTHROW;

		bool isNull(void) const;
		bool isValid(void) const;

		enum Report::AppType appType(void) const;
		void setAppType(enum Report::AppType at);

		const QString &uniqueId(void) const;
		void setUniqueId(const QString &ui);
#ifdef Q_COMPILER_RVALUE_REFS
		void setUniqueId(QString &&ui);
#endif /* Q_COMPILER_RVALUE_REFS */

	private:
		/*!
		 * @brief Creates an instance from supplied JSON data.
		 *
		 * @param[in]  jsonVal JSON object value.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		ConfReport fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

	public:
		/*!
		 * @brief Writes content of JSON object.
		 *
		 * @note Unspecified values are omitted.
		 *
		 * @param[out] jsonValue JSON value to write to.
		 * @return True on success.
		 */
		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<ConfReportPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<ConfReportPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	class UsageReportPrivate;
	/*!
	 * @brief Writes build environment information into JSON.
	 *
	 * "at" - application type, int, enum
	 * "mdsp" - message database size production, string, int
	 * "mdst" - message database size testing, string, int
	 * "mui" - unique ID, string
	 * "nbp' - number of boxes production, int
	 * "nbt' - number of boxes production, testing
	 * "ndt" - number of defined tags, int
	 * "udim" - uses databases in memory, int, bool
	 * "up" - uses PIN, int, bool
	 * "urm" - uses records management, int, bool
	 * "v" - report version, string, int
	 *
	 * Generated JSON example:
	 * {
	 *   "at":1,
	 *   "mdsp":"1000000",
	 *   "mdst":"16000000",
	 *   "mui":"YjUzOWQ2NDYxMzFkNDA4N2E2OGM4OTJiMDAwMDAwMTM=",
	 *   "nbp":1,
	 *   "nbt":4,
	 *   "ndt":7,
	 *   "udim":0,
	 *   "up":1,
	 *   "urm":0,
	 *   "v":"0"
	 * }
	 */
	class UsageReport : public Object {
		Q_DECLARE_PRIVATE(UsageReport)

	public:
		UsageReport(void);
		UsageReport(const UsageReport &other);
#ifdef Q_COMPILER_RVALUE_REFS
		UsageReport(UsageReport &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~UsageReport(void);

		UsageReport &operator=(const UsageReport &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		UsageReport &operator=(UsageReport &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const UsageReport &other) const;
		bool operator!=(const UsageReport &other) const;

		static
		void swap(UsageReport &first, UsageReport &second) Q_DECL_NOTHROW;

		bool isNull(void) const;
		bool isValid(void) const;

		enum Report::AppType appType(void) const;
		void setAppType(enum Report::AppType at);

		const QString &uniqueId(void) const;
		void setUniqueId(const QString &ui);
#ifdef Q_COMPILER_RVALUE_REFS
		void setUniqueId(QString &&ui);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* Number of configured production data boxes. */
		int numBoxesProd(void) const;
		void setNumBoxesProd(int nbp);
		/* Number of configured testing data boxes. */
		int numBoxesTesting(void) const;
		void setNumBoxesTesting(int nbt);
		/* Size of production message-related data. */
		qint64 messageDataSizeProd(void) const;
		void setMessageDataSizeProd(qint64 mdsp);
		/* Size of testing message-related data. */
		qint64 messageDataSizeTesting(void) const;
		void setMessageDataSizeTesting(qint64 mdst);
		/* Number of defined tags (may be unused). */
		int numDefinedTags(void) const;
		void setNumDefinedTags(int ndt);
		/* Whether databases in memory are used. */
		bool usesDatabasesInMemory(void) const;
		void setUsesDatabasesInMemory(bool udim);
		/* Whether uses PIN. */
		bool usesPin(void) const;
		void setUsesPin(bool up);
		/* Whether has records management service set. */
		bool usesRecordsManagement(void) const;
		void setUsesRecordsManagement(bool urm);

	private:
		/*!
		 * @brief Creates an instance from supplied JSON data.
		 *
		 * @param[in]  jsonVal JSON object value.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		UsageReport fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

	public:
		/*!
		 * @brief Writes content of JSON object.
		 *
		 * @note Unspecified values are omitted.
		 *
		 * @param[out] jsonValue JSON value to write to.
		 * @return True on success.
		 */
		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<UsageReportPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<UsageReportPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

}
