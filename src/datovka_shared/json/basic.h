/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QMetaType>
#include <QVector>

#include "src/datovka_shared/json/object.h"

class QByteArray; /* Forward declaration. */
class QJsonValue; /* Forward declaration. */

namespace Json {

	/*!
	 * @brief List of 64-bit integers.
	 */
	class Int64StringList : public Object, public QList<qint64> {
	public:
		/* Expose list constructors. */
		using QList<qint64>::QList;

		/* Some older compilers complain about missing constructor. */
		Int64StringList(void);

		Int64StringList(const QList<qint64> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		Int64StringList(QList<qint64> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		Int64StringList fromJsonStr(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		Int64StringList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	class DoubleStringVector : public Object, public QVector<double> {
	public:
		/* Expose vector constructors. */
		using QVector<double>::QVector;

		/* Some older compilers complain about missing constructor. */
		DoubleStringVector(void);

		DoubleStringVector(const QVector<double> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		DoubleStringVector(QVector<double> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		DoubleStringVector fromJsonStr(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		DoubleStringVector fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	class Int64StringVector : public Object, public QVector<qint64> {
	public:
		/* Expose vector constructors. */
		using QVector<qint64>::QVector;

		/* Some older compilers complain about missing constructor. */
		Int64StringVector(void);

		Int64StringVector(const QVector<qint64> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		Int64StringVector(QVector<qint64> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		Int64StringVector fromJsonStr(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		Int64StringVector fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	class IntVector : public Object, public QVector<int> {
	public:
		/* Expose vector constructors. */
		using QVector<int>::QVector;

		/* Some older compilers complain about missing constructor. */
		IntVector(void);

		IntVector(const QVector<int> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		IntVector(QVector<int> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		IntVector fromJsonStr(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		IntVector fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

}

Q_DECLARE_METATYPE(Json::Int64StringList)
