/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

class QPrinter; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Utility {

	/*!
	 * @brief Format of the input text.
	 */
	enum TextFormat {
		PLAIN_TEXT = 0, /*!< Treat as plain text. */
		HTML_TEXT, /*!< Treat as HTML text. */
		MARKDOWN_TEXT /*!< Markdown is supported since Qt-5.14. */
	};

#if !defined(Q_OS_IOS)
	/*!
	 * @brief Print text content on printer.
	 *
	 * @param[in,out] printer Printer device.
	 * @param[in]     text Text content to print on device.
	 * @param[in]     format Input text format.
	 * @return True on success.
	 */
	bool printText(QPrinter *printer, const QString &text,
	    enum TextFormat format = PLAIN_TEXT);
#endif /* !Q_OS_IOS */

	/*!
	 * @brief Generate PDF file from text.
	 *
	 * @param[in] fileName File path with file name.
	 * @param[in] text Text string to be printed.
	 * @param[in] format Input text format.
	 * @return True if success.
	 */
	bool printPDF(const QString &fileName, const QString &text,
	    enum TextFormat format = PLAIN_TEXT);

	/*!
	 * @brief Generate PDF file from text with defined font.
	 *
	 * @param[in] fileName File path with file name.
	 * @param[in] text Text string to be printed.
	 * @param[in] format Input text format.
	 * @param[in] fontFileName Font file name.
	 * @return True if success.
	 */
	bool printPDFWithFont(const QString &fileName, const QString &text,
	    enum TextFormat format = MARKDOWN_TEXT,
	    const QString &fontFileName = QString());
}
