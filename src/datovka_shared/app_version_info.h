/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */
#include <QString>

class AppVersionInfo {
	Q_DECLARE_TR_FUNCTIONS(AppVersionInfo)

public:
	/*!
	 * @brief Check whether string contains only release version
	 *     (eg. 4.25.0).
	 *
	 * @note The string may contain some leading and trailing white-space characters.
	 *
	 * @param[in] vStr Version string.
	 * @return If \a vStr is a release version.
	 */
	static
	bool isReleaseVersionString(const QString &vStr);

	/*!
	 * @brief Check whether string contains git development build version
	 *     (eg. 4.25.0.9999.20250106.151005.ee2327675e2f1a9a).
	 *
	 * @note The string may contain some leading and trailing white-space characters.
	 *
	 * @param[in] vStr Version string.
	 * @return If \a vStr is a git development build version.
	 */
	static
	bool isGitArchiveString(const QString &vStr);

	/*!
	 * @brief Compare newest available version and application version.
	 *
	 * @param[in] vStr1 Version string.
	 * @param[in] vStr2 Version string.
	 * @retval -2 if vStr1 doesn't contain a suitable version string
	 * @retval -1 if vStr1 is less than vStr2
	 * @retval  0 if vStr1 is equal to vStr2
	 * @retval  1 if vStr1 is greater than vStr2
	 * @retval  2 if vStr2 doesn't contain a suitable version string
	 */
	static
	int compareVersionStrings(const QString &vStr1, const QString &vStr2);

	/*!
	 * @brief Release news.
	 *
	 * @return Release news text.
	 */
	static
	QString releaseNewsText(void);
};
