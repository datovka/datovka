/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QString>

#include "src/datovka_shared/isds/generic_interface.h"

/* Null objects - for convenience. */
static const QString nullString;

/*!
 * @brief PIMPL response status class.
 */
class Isds::StatusPrivate {
public:
	StatusPrivate(void)
	    : m_type(Type::RST_UNKNOWN), m_code(), m_message(), m_refNum()
	{ }

	StatusPrivate &operator=(const StatusPrivate &other) Q_DECL_NOTHROW
	{
		m_type = other.m_type;
		m_code = other.m_code;
		m_message = other.m_message;
		m_refNum = other.m_refNum;

		return *this;
	}

	bool operator==(const StatusPrivate &other) const
	{
		return (m_type == other.m_type) &&
		    (m_code == other.m_code) &&
		    (m_message == other.m_message) &&
		    (m_refNum == other.m_refNum);
	}

	enum Type::ResponseStatusType m_type;
	QString m_code;
	QString m_message;
	QString m_refNum;
};

Isds::Status::Status(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::Status::Status(const Status &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) StatusPrivate) : Q_NULLPTR)
{
	Q_D(Status);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::Status::Status(Status &&other) Q_DECL_NOEXCEPT
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    : d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    : d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */
Isds::Status::~Status(void)
{
}

/*!
 * @brief Ensures private status presence.
 *
 * @note Returns if private status could not be allocated.
 */
#define ensureStatusPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			StatusPrivate *p = new (::std::nothrow) StatusPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::Status &Isds::Status::operator=(const Status &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureStatusPrivate(*this);
	Q_D(Status);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::Status &Isds::Status::operator=(Status &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::Status::operator==(const Status &other) const
{
	Q_D(const Status);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::Status::operator!=(const Status &other) const
{
	return !operator==(other);
}

bool Isds::Status::isNull(void) const
{
	Q_D(const Status);
	return d == Q_NULLPTR;
}

enum Isds::Type::ResponseStatusType Isds::Status::type(void) const
{
	Q_D(const Status);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Type::RST_UNKNOWN;
	}

	return d->m_type;
}

void Isds::Status::setType(enum Type::ResponseStatusType t)
{
	ensureStatusPrivate();
	Q_D(Status);
	d->m_type = t;
}

const QString &Isds::Status::code(void) const
{
	Q_D(const Status);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_code;
}

void Isds::Status::setCode(const QString &c)
{
	ensureStatusPrivate();
	Q_D(Status);
	d->m_code = c;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Status::setCode(QString &&c)
{
	ensureStatusPrivate();
	Q_D(Status);
	d->m_code = ::std::move(c);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::Status::message(void) const
{
	Q_D(const Status);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_message;
}

void Isds::Status::setMessage(const QString &m)
{
	ensureStatusPrivate();
	Q_D(Status);
	d->m_message = m;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Status::setMessage(QString &&m)
{
	ensureStatusPrivate();
	Q_D(Status);
	d->m_message = ::std::move(m);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::Status::refNum(void) const
{
	Q_D(const Status);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_refNum;
}

void Isds::Status::setRefNum(const QString &r)
{
	ensureStatusPrivate();
	Q_D(Status);
	d->m_refNum = r;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Status::setRefNum(QString &&r)
{
	ensureStatusPrivate();
	Q_D(Status);
	d->m_refNum = ::std::move(r);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void Isds::swap(Status &first, Status &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
