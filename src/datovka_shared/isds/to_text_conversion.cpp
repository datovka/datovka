/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDate>

#include "src/datovka_shared/isds/box_interface2.h" /* PersonName2 */
#include "src/datovka_shared/isds/message_interface2.h" /* DmMessageAuthor */
#include "src/datovka_shared/isds/to_text_conversion.h"
#include "src/datovka_shared/isds/type_description.h"
#include "src/datovka_shared/utility/date_time.h"

QString Isds::textFullName(const PersonName2 &personName)
{
	if (Q_UNLIKELY(personName.isNull())) {
		return QString();
	}

	QString fullName;
	if (!personName.givenNames().isEmpty()) {
		fullName = personName.givenNames();
	}
	if (!personName.lastName().isEmpty()) {
		if (!fullName.isEmpty()) {
			fullName += QStringLiteral(" ") + personName.lastName();
		} else {
			fullName = personName.lastName();
		}
	}

	return fullName;
}

QList< QPair<QString, QString> > Isds::textListMessageAuthor(
    const DmMessageAuthor &messageAuthor)
{
	typedef QPair<QString, QString> EntryPair;

	QList< QPair<QString, QString> > pairList;
	QString str;

	{
		const enum Type::SenderType type = messageAuthor.userType();
		if (type != Type::ST_NULL) {
			pairList.append(EntryPair(tr("User type"),
			    Description::descrSenderType(type)));
		}
	}
	str = textFullName(messageAuthor.personName());
	if (!str.isEmpty()) {
		pairList.append(EntryPair(tr("Full name"), str));
	}
	{
		const QDate date = messageAuthor.biDate();
		if (date.isValid()) {
			str = date.toString(Utility::dateDisplayFormat);
			if (!str.isEmpty()) {
				pairList.append(EntryPair(tr("Date of birth"), str));
			}
		}
	}
	str = messageAuthor.biCity();
	if (!str.isEmpty()) {
		pairList.append(EntryPair(tr("City of birth"), str));
	}
	str = messageAuthor.biCounty();
	if (!str.isEmpty()) {
		pairList.append(EntryPair(tr("County of birth"), str));
	}
	str = messageAuthor.adCode();
	if (!str.isEmpty()) {
		pairList.append(EntryPair(tr("RUIAN address code"), str));
	}
	str = messageAuthor.fullAddress();
	if (!str.isEmpty()) {
		pairList.append(EntryPair(tr("Full address"), str));
	}
	{
		const enum Type::NilBool robIdent = messageAuthor.robIdent();
		if (robIdent != Type::BOOL_NULL) {
			pairList.append(EntryPair(tr("ROB identification"),
			    (robIdent == Type::BOOL_TRUE) ? tr("identified") : tr("not identified")));
		}
	}

	return pairList;
}
