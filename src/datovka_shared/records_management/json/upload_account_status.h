/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

class QDateTime; /* Forward declaration. */
class QJsonValue; /* Forward declaration. */
class QString; /* Forward declaration. */
namespace RecMgmt {
	class ErrorEntry; /* Forward declaration. */
}

namespace RecMgmt {

	class UploadAccountStatusReqPrivate;
	/*!
	 * @brief Encapsulates the upload_account_status request.
	 */
	class UploadAccountStatusReq : public Json::Object {
		Q_DECLARE_PRIVATE(UploadAccountStatusReq)
	public:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		UploadAccountStatusReq(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Upload account status request.
		 */
		UploadAccountStatusReq(const UploadAccountStatusReq &other);
#ifdef Q_COMPILER_RVALUE_REFS
		UploadAccountStatusReq(UploadAccountStatusReq &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~UploadAccountStatusReq(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] boxId Data box identifier.
		 * @param[in] userId Username used to access the data box.
		 * @param[in] time Time.
		 * @param[in] rcvdNotAccepted Number of received unaccepted messages in box.
		 * @param[in] sntNotAccepted Number of sent unaccepted messages in box.
		 * @param[in] rcvdNotInRecMgmt Number of received messages not sent into records management.
		 * @param[in] sntNotInRecMgmt Number of sent messages not sent into records management.
		 */
		UploadAccountStatusReq(const QString &boxId,
		    const QString &userId, const QDateTime &time,
		    int rcvdNotAccepted, int sntNotAccepted,
		    int rcvdNotInRecMgmt, int sntNotInRecMgmt,
		    const QString &userAccountName);
#ifdef Q_COMPILER_RVALUE_REFS
		UploadAccountStatusReq(QString &&boxId,
		    QString &&userId, QDateTime &&time,
		    int rcvdNotAccepted, int sntNotAccepted,
		    int rcvdNotInRecMgmt, int sntNotInRecMgmt,
		    QString &&userAccountName);
#endif /* Q_COMPILER_RVALUE_REFS */

		UploadAccountStatusReq &operator=(const UploadAccountStatusReq &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		UploadAccountStatusReq &operator=(UploadAccountStatusReq &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const UploadAccountStatusReq &other) const;
		bool operator!=(const UploadAccountStatusReq &other) const;

		static
		void swap(UploadAccountStatusReq &first, UploadAccountStatusReq &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Get box identifier.
		 *
		 * @return Data box identifier.
		 */
		const QString &boxId(void) const;
		void setBoxId(const QString &b);
#ifdef Q_COMPILER_RVALUE_REFS
		void setBoxId(QString &&b);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Get username.
		 *
		 * @return Username used to access the data box.
		 */
		const QString &userId(void) const;
		void setUserId(const QString &u);
#ifdef Q_COMPILER_RVALUE_REFS
		void setUserId(QString &&u);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Get time.
		 *
		 * @return Time.
		 */
		const QDateTime &time(void) const;
		void setTime(const QDateTime &t);
#ifdef Q_COMPILER_RVALUE_REFS
		void setTime(QDateTime &&t);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Get number of received unaccepted messages.
		 *
		 * @return Number of received unaccepted messages in box.
		 */
		int rcvdNotAccepted(void) const;
		void setRcvdNotAccepted(int rna);

		/*!
		 * @brief Get number of sent unaccepted messages.
		 *
		 * @return Number of sent unaccepted messages in box.
		 */
		int sntNotAccepted(void) const;
		void setSntNotAccepted(int sna);

		/*!
		 * @brief Get number of received messages not sent into records management.
		 *
		 * @return Number of received messages not sent into records management.
		 */
		int rcvdNotInRecMgmt(void) const;
		void setRcvdNotInRecMgmt(int rni);

		/*!
		 * @brief Get number of sent messages not sent into records management.
		 *
		 * @return Number of sent messages not sent into records management.
		 */
		int sntNotInRecMgmt(void) const;
		void setSntNotInRecMgmt(int sni);

		/*!
		 * @brief Get user-defined account name as used by the application.
		 */
		const QString &userAccountName(void) const;
		void setUserAccountName(const QString &ua);
#ifdef Q_COMPILER_RVALUE_REFS
		void setUserAccountName(QString &&ua);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates a service info structure from supplied JSON document.
		 *
		 * @param[in]  json JSON document.
		 * @param[out] ok Set to true on success.
		 * @return Invalid structure on error a valid structure else.
		 */
		static
		UploadAccountStatusReq fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		UploadAccountStatusReq fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<UploadAccountStatusReqPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<UploadAccountStatusReqPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	class UploadAccountStatusRespPrivate;
	/*!
	 * @brief Encapsulates the upload_account_status response.
	 */
	class UploadAccountStatusResp : public Json::Object {
		Q_DECLARE_PRIVATE(UploadAccountStatusResp)
	public:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		UploadAccountStatusResp(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Upload account status response.
		 */
		UploadAccountStatusResp(const UploadAccountStatusResp &other);
#ifdef Q_COMPILER_RVALUE_REFS
		UploadAccountStatusResp(UploadAccountStatusResp &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~UploadAccountStatusResp(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] error Error entry.
		 */
		explicit UploadAccountStatusResp(const ErrorEntry &error);
#ifdef Q_COMPILER_RVALUE_REFS
		explicit UploadAccountStatusResp(ErrorEntry &&error);
#endif /* Q_COMPILER_RVALUE_REFS */

		UploadAccountStatusResp &operator=(const UploadAccountStatusResp &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		UploadAccountStatusResp &operator=(UploadAccountStatusResp &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const UploadAccountStatusResp &other) const;
		bool operator!=(const UploadAccountStatusResp &other) const;

		static
		void swap(UploadAccountStatusResp &first, UploadAccountStatusResp &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Return error entry.
		 *
		 * @return Error entry.
		 */
		const ErrorEntry &error(void) const;
		void setError(const ErrorEntry &e);
#ifdef Q_COMPILER_RVALUE_REFS
		void setError(ErrorEntry &&e);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates a upload account status response structure
		 *     from supplied JSON document.
		 *
		 * @param[in]  json JSON document.
		 * @param[out] ok Set to true on success.
		 * @return Invalid structure on error a valid structure else.
		 */
		static
		UploadAccountStatusResp fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		UploadAccountStatusResp fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<UploadAccountStatusRespPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<UploadAccountStatusRespPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

}
