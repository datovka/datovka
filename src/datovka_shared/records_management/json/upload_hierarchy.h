/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QList>
#include <QSet>
#include <QString>

#include "src/datovka_shared/json/object.h"

class QJsonValue; /* Forward declaration. */

namespace RecMgmt {

	/*!
	 * @brief Encapsulates the upload_hierarchy response.
	 */
	class UploadHierarchyResp : public Json::Object {
	public:
		/*!
		 * @brief Node entry element.
		 */
		class NodeEntry {
		private:
			/*!
			 * @brief Constructor.
			 */
			NodeEntry(void);

			NodeEntry(const NodeEntry &other) = delete;
#ifdef Q_COMPILER_RVALUE_REFS
			NodeEntry(const NodeEntry &&other) Q_DECL_NOEXCEPT = delete;
#endif /* Q_COMPILER_RVALUE_REFS */

			NodeEntry &operator=(const NodeEntry &other) Q_DECL_NOTHROW = delete;
#ifdef Q_COMPILER_RVALUE_REFS
			NodeEntry &operator=(NodeEntry &&other) Q_DECL_NOTHROW = delete;
#endif /* Q_COMPILER_RVALUE_REFS */

		public:
			/*!
			 * @brief Destructor.
			 *
			 * @note Destroys recursively all sub-nodes.
			 */
			~NodeEntry(void);

			/*!
			 * @brief Return superordinate node,
			 *
			 * @return Superordinate node.
			 */
			const NodeEntry *super(void) const;

			/*!
			 * @brief Returns node name.
			 *
			 * @return Node name.
			 */
			const QString &name(void) const;
			void setName(const QString &n);
#ifdef Q_COMPILER_RVALUE_REFS
			void setName(QString &&n);
#endif /* Q_COMPILER_RVALUE_REFS */

			/*!
			 * @brief Returns node id.
			 *
			 * @return Node id.
			 */
			const QString &id(void) const;
			void setId(const QString &i);
#ifdef Q_COMPILER_RVALUE_REFS
			void setId(QString &&i);
#endif /* Q_COMPILER_RVALUE_REFS */

			/*!
			 * @brief Returns metadata.
			 *
			 * @return Stored metadata.
			 */
			const QSet<QString> &metadata(void) const;
			void setMetadata(const QSet<QString> &m);
#ifdef Q_COMPILER_RVALUE_REFS
			void setMetadata(QSet<QString> &&m);
#endif /* Q_COMPILER_RVALUE_REFS */

			/*!
			 * @brief Returns case identifiers.
			 *
			 * @return Stored case identifiers.
			 */
			const QSet<QString> &caseId(void) const;
			void setCaseId(const QSet<QString> &c);
#ifdef Q_COMPILER_RVALUE_REFS
			void setCaseId(QSet<QString> &&c);
#endif /* Q_COMPILER_RVALUE_REFS */

			/*!
			 * @brief Return court case identifiers.
			 *
			 * @return Stored court case identifiers.
			 */
			const QSet<QString> &courtCaseId(void) const;
			void setCourtCaseId(const QSet<QString> &cc);
#ifdef Q_COMPILER_RVALUE_REFS
			void setCourtCaseId(QSet<QString> &&cc);
#endif /* Q_COMPILER_RVALUE_REFS */

			/*!
			 * @brief Returns list of subordinated nodes.
			 *
			 * @return List of subordinated nodes.
			 */
			const QList<NodeEntry *> &sub(void) const;

			/*!
			 * @brief Add new subordinate node.
			 *
			 * @param[in] sub Non-null pointer to added subordinate node.
			 * @return True on success, false on any error.
			 */
			bool appendSub(NodeEntry *sub);

		private:
			/*!
			 * @brief Performs a recursive (deep) copy.
			 *
			 * @param[in] root Non-null tree root.
			 * @return Copied tree on success, null pointer on error.
			 */
			static
			NodeEntry *copyRecursive(const NodeEntry *root);

			/*!
			 * @brief Recursively constructs a hierarchy.
			 *
			 * @param[in]  jsonVal Object to be used as root of the hierarchy.
			 * @param[out] ok Set to true if no error encountered.
			 * @param[in]  acceptNullName True if null values should be accepted.
			 * @return Parsed hierarchy if \a ok set to true.
			 */
			static
			NodeEntry *fromJsonValRecursive(const QJsonValue &jsonVal,
			    bool &ok, bool acceptNullName);

			/*!
			 * @brief Recursively constructs a JSON object hierarchy.
			 *
			 * @param[in,out] jsonVal JSON object value to write the content to.
			 * @return True on success.
			 */
			bool toJsonValRecursive(QJsonValue &jsonVal) const;

			NodeEntry *m_super; /*!< Superordinate node. */
			QString m_name; /*!< Entry name. Root entry name may be null. */
			QString m_id; /*!< Entry identifier. May be null. */
			QSet<QString> m_metadata; /*!< Metadata. May be empty. */
			QSet<QString> m_caseId; /*!< Related case identifiers. May be empty. */
			QSet<QString> m_courtCaseId; /*!< Related opposite party case identifiers. May be empty. */
			QList<NodeEntry *> m_sub; /*!< Subordinated nodes. */

		friend class UploadHierarchyResp;
		};

	public:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		UploadHierarchyResp(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Upload hierarchy response.
		 */
		UploadHierarchyResp(const UploadHierarchyResp &other);

#ifdef Q_COMPILER_RVALUE_REFS
		/*!
		 * @brief Move constructor.
		 *
		 * @param[in] other Upload hierarchy response.
		 */
		UploadHierarchyResp(UploadHierarchyResp &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Destructor.
		 */
		~UploadHierarchyResp(void);

		/*!
		 * @brief Copy assignment.
		 *
		 * @param[in] other Source.
		 * @return Destination reference.
		 */
		UploadHierarchyResp &operator=(
		    const UploadHierarchyResp &other) Q_DECL_NOTHROW;

#ifdef Q_COMPILER_RVALUE_REFS
		/*!
		 * @brief Move assignment.
		 *
		 * @param[in] other Source.
		 * @return Destination reference.
		 */
		UploadHierarchyResp &operator=(
		    UploadHierarchyResp &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Returns root node.
		 *
		 * @return Root node.
		 */
		const NodeEntry *root(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Creates a upload hierarchy structure from supplied
		 *     JSON document.
		 *
		 * @param[in]  json JSON document.
		 * @param[out] ok Set to true on success.
		 * @return Invalid structure on error a valid structure else.
		 */
		static
		UploadHierarchyResp fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		UploadHierarchyResp fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
		NodeEntry *m_root; /*!< Tree root. */
	};

}
