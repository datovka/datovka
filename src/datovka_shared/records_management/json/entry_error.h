/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS() */
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */
#include <QString>

#include "src/datovka_shared/json/object.h"

class QJsonValue; /* Forward declaration. */

namespace RecMgmt {

	class ErrorEntryPrivate;
	/*!
	 * @brief Encapsulates any error entry.
	 */
	class ErrorEntry : public Json::Object {
		Q_DECLARE_PRIVATE(ErrorEntry)
		Q_DECLARE_TR_FUNCTIONS(ErrorEntry)

	public:
		/*!
		 * @brief Error codes.
		 */
		enum Code {
			ERR_NO_ERROR, /*!< Just for convenience. */
			ERR_MALFORMED_REQUEST, /*!< JSON request was corrupt. */
			ERR_MISSING_IDENTIFIER, /*!< JSON request provided no identifier. */
			ERR_WRONG_IDENTIFIER, /*!< Wrong identifier provided in JSON request. */
			ERR_UNSUPPORTED_FILE_FORMAT, /*!< Uploaded file format not supported. */
			ERR_ALREADY_PRESENT, /*!< File is already present. */
			ERR_LIMIT_EXCEEDED, /*!< Too many identifiers in a single request. */
			ERR_UNSPECIFIED /*!< Unspecified error. */
		};

		/*!
		 * @brief Constructor. Constructs a no-error entry.
		 */
		ErrorEntry(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Error entry.
		 */
		ErrorEntry(const ErrorEntry &other);
#ifdef Q_COMPILER_RVALUE_REFS
		ErrorEntry(ErrorEntry &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~ErrorEntry(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] code Error code.
		 * @param[in] description Additional error description.
		 */
		ErrorEntry(enum Code code, const QString &description);
#ifdef Q_COMPILER_RVALUE_REFS
		ErrorEntry(enum Code code, QString &&description);
#endif /* Q_COMPILER_RVALUE_REFS */

		ErrorEntry &operator=(const ErrorEntry &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		ErrorEntry &operator=(ErrorEntry &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const ErrorEntry &other) const;
		bool operator!=(const ErrorEntry &other) const;

		static
		void swap(ErrorEntry &first, ErrorEntry &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Returns error code.
		 *
		 * @return Error code.
		 */
		enum Code code(void) const;
		void setCode(enum Code c);

		/*!
		 * @brief Returns error description.
		 *
		 * @return Error description.
		 */
		const QString &description(void) const;
		void setDescription(const QString &de);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDescription(QString &&de);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates an instance from supplied JSON data.
		 *
		 * @param[in]  jsonVal JSON object value.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		ErrorEntry fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Writes content of JSON object.
		 *
		 * @note No-error entries are converted into JSON null values.
		 *
		 * @param[out] jsonValue JSON value to write to.
		 * @return True on success.
		 */
		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

		/*!
		 * @brief Returns translated error description.
		 *
		 * @return String containing localised description.
		 */
		QString trVerbose(void) const;

	private:
		/*!
		 * @brief Converts error code into string as used in JSON.
		 *
		 * @param[in] code Code value to be converted into a string.
		 * @return JSON string code representation.
		 */
		static
		const QString &codeToString(enum Code code);

		/*!
		 * @brief Converts error string as used in JSON into error code.
		 *
		 * @param[in]  str JSON string code representation.
		 * @param[out] ok Set to true if conversion was ok.
		 * @return Code value.
		 */
		static
		enum Code stringToCode(const QString &str, bool *ok = Q_NULLPTR);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<ErrorEntryPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<ErrorEntryPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

}
