/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

class QJsonValue; /* Forward declaration. */
class QString; /* Forward declaration. */
namespace RecMgmt {
	class ErrorEntry; /* Forward declaration. */
}

namespace RecMgmt {

	class UploadFileReqPrivate;
	/*!
	 * @brief Encapsulates the upload_file request.
	 */
	class UploadFileReq : public Json::Object {
		Q_DECLARE_PRIVATE(UploadFileReq)
	public:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		UploadFileReq(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Upload file request.
		 */
		UploadFileReq(const UploadFileReq &other);
#ifdef Q_COMPILER_RVALUE_REFS
		UploadFileReq(UploadFileReq &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~UploadFileReq(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] ids Location identifiers as obtained from upload_hierarchy.
		 * @param[in] fileName File name.
		 * @param[in] fileContent Raw file content.
		 */
		UploadFileReq(const QStringList &ids, const QString &fileName,
		    const QByteArray &fileContent);
#ifdef Q_COMPILER_RVALUE_REFS
		UploadFileReq(QStringList &&ids, QString &&fileName,
		    QByteArray &&fileContent);
#endif /* Q_COMPILER_RVALUE_REFS */

		UploadFileReq &operator=(const UploadFileReq &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		UploadFileReq &operator=(UploadFileReq &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const UploadFileReq &other) const;
		bool operator!=(const UploadFileReq &other) const;

		static
		void swap(UploadFileReq &first, UploadFileReq &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Return location identifier.
		 *
		 * @return Location identifier.
		 */
		const QStringList &ids(void) const;
		void setIds(const QStringList &i);
#ifdef Q_COMPILER_RVALUE_REFS
		void setIds(QStringList &&i);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Return file name.
		 *
		 * @return File name.
		 */
		const QString &fileName(void) const;
		void setFileName(const QString &f);
#ifdef Q_COMPILER_RVALUE_REFS
		void setFileName(QString &&f);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Return file content.
		 *
		 * @return Raw file content.
		 */
		const QByteArray &fileContent(void) const;
		void setFileContent(const QByteArray &c);
#ifdef Q_COMPILER_RVALUE_REFS
		void setFileContent(QByteArray &&c);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates a upload file request structure from supplied JSON
		 *     document.
		 *
		 * @param[in]  json JSON document.
		 * @param[out] ok Set to true on success.
		 * @return Invalid structure on error a valid structure else.
		 */
		static
		UploadFileReq fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		UploadFileReq fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<UploadFileReqPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<UploadFileReqPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	class UploadFileRespPrivate;
	/*!
	 * @brief Encapsulates the upload_file response.
	 */
	class UploadFileResp : public Json::Object {
		Q_DECLARE_PRIVATE(UploadFileResp)
	public:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		UploadFileResp(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Upload file response.
		 */
		UploadFileResp(const UploadFileResp &other);
#ifdef Q_COMPILER_RVALUE_REFS
		UploadFileResp(UploadFileResp &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~UploadFileResp(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] id File identifier.
		 * @param[in] error Error entry.
		 * @param[in] locations List of locations.
		 */
		UploadFileResp(const QString &id, const ErrorEntry &error,
		    const QStringList &locations);
#ifdef Q_COMPILER_RVALUE_REFS
		UploadFileResp(QString &&id, ErrorEntry &&error,
		    QStringList &&locations);
#endif /* Q_COMPILER_RVALUE_REFS */

		UploadFileResp &operator=(const UploadFileResp &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		UploadFileResp &operator=(UploadFileResp &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const UploadFileResp &other) const;
		bool operator!=(const UploadFileResp &other) const;

		static
		void swap(UploadFileResp &first, UploadFileResp &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Return file identifier.
		 *
		 * @return File identifier.
		 */
		const QString &id(void) const;
		void setId(const QString &i);
#ifdef Q_COMPILER_RVALUE_REFS
		void setId(QString &&i);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Return error entry.
		 *
		 * @return Error entry.
		 */
		const ErrorEntry &error(void) const;
		void setError(const ErrorEntry &e);
#ifdef Q_COMPILER_RVALUE_REFS
		void setError(ErrorEntry &&e);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Return location list.
		 *
		 * @return List of places where the file is stored in the service.
		 */
		const QStringList &locations(void) const;
		void setLocations(const QStringList &l);
#ifdef Q_COMPILER_RVALUE_REFS
		void setLocations(QStringList &&l);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates a upload file response structure from supplied JSON
		 *     document.
		 *
		 * @param[in]  json JSON document.
		 * @param[out] ok Set to true on success.
		 * @return Invalid structure on error a valid structure else.
		 */
		static
		UploadFileResp fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		UploadFileResp fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<UploadFileRespPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<UploadFileRespPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

}
