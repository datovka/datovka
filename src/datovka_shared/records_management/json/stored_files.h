/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QList>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

namespace Json {
	class Int64StringList; /* Forward declaration. */
}
class QJsonValue; /* Forward declaration. */
namespace RecMgmt {
	class ErrorEntry; /* Forward declaration. */
}

namespace RecMgmt {

	class StoredFilesReqPrivate;
	/*!
	 * @brief Encapsulates the stored_files request.
	 */
	class StoredFilesReq : public Json::Object {
		Q_DECLARE_PRIVATE(StoredFilesReq)
	public:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		StoredFilesReq(void);

		StoredFilesReq(const StoredFilesReq &other);
#ifdef Q_COMPILER_RVALUE_REFS
		StoredFilesReq(StoredFilesReq &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~StoredFilesReq(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] dmIds Data message identifiers.
		 * @param[in] diIds Delivery info identifiers.
		 */
		StoredFilesReq(const Json::Int64StringList &dmIds,
		    const Json::Int64StringList &diIds);
#ifdef Q_COMPILER_RVALUE_REFS
		StoredFilesReq(Json::Int64StringList &&dmIds,
		    Json::Int64StringList &&diIds);
#endif /* Q_COMPILER_RVALUE_REFS */

		StoredFilesReq &operator=(const StoredFilesReq &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		StoredFilesReq &operator=(StoredFilesReq &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const StoredFilesReq &other) const;
		bool operator!=(const StoredFilesReq &other) const;

		static
		void swap(StoredFilesReq &first, StoredFilesReq &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Return data message identifiers.
		 *
		 * @return Data message identifiers as used in ISDS.
		 */
		const Json::Int64StringList &dmIds(void) const;
		void setDmIds(const Json::Int64StringList &dms);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDmIds(Json::Int64StringList &&dms);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Return delivery info identifiers.
		 *
		 * @return Delivery info identifiers as used in ISDS.
		 */
		const Json::Int64StringList &diIds(void) const;
		void setDiIds(const Json::Int64StringList &dis);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDiIds(Json::Int64StringList &&dis);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates a stored files request structure from supplied JSON
		 *     document.
		 *
		 * @param[in]  json JSON document.
		 * @param[out] ok Set to true on success.
		 * @return Invalid structure on error a valid structure else.
		 */
		static
		StoredFilesReq fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		StoredFilesReq fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<StoredFilesReqPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<StoredFilesReqPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	class DmEntryPrivate;
	/*!
	 * @brief Encapsulates stored_files data message entry structure.
	 */
	class DmEntry : public Json::Object {
		Q_DECLARE_PRIVATE(DmEntry)
	public:
		/*!
		 * @brief Constructor. Constructs invalid entry.
		 */
		DmEntry(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Message entry.
		 */
		DmEntry(const DmEntry &other);
#ifdef Q_COMPILER_RVALUE_REFS
		DmEntry(DmEntry &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~DmEntry(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] dmId Message identifier.
		 * @param[in] locations List of locations.
		 */
		DmEntry(qint64 dmId, const QStringList &locations);
#ifdef Q_COMPILER_RVALUE_REFS
		DmEntry(qint64 dmId, QStringList &&locations);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Assignment operator.
		 *
		 * @param[in] other Message entry.
		 */
		DmEntry &operator=(const DmEntry &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		DmEntry &operator=(DmEntry &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const DmEntry &other) const;
		bool operator!=(const DmEntry &other) const;

		static
		void swap(DmEntry &first, DmEntry &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Return message identifier.
		 *
		 * @return Data message identifier.
		 */
		qint64 dmId(void) const;
		void setDmId(qint64 id);

		/*!
		 * @brief Return list of locations.
		 *
		 * @return Locations.
		 */
		const QStringList &locations(void) const;
		void setLocations(const QStringList &l);
#ifdef Q_COMPILER_RVALUE_REFS
		void setLocations(QStringList &&l);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		DmEntry fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		DmEntry fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<DmEntryPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<DmEntryPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	/*!
	 * @brief List of message identifiers.
	 */
	class DmEntryList : public Json::Object, public QList<DmEntry> {
	public:
		/* Expose list constructors. */
		using QList<DmEntry>::QList;

		/* Some older compilers complain about missing constructor. */
		DmEntryList(void);

		static
		DmEntryList fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		DmEntryList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	class DiEntryPrivate;
	/*!
	 * @brief Encapsulates stored_files delivery info entry structure.
	 */
	class DiEntry : public Json::Object {
		Q_DECLARE_PRIVATE(DiEntry)
	public:
		/*!
		 * @brief Constructor. Constructs invalid entry.
		 */
		DiEntry(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Info entry.
		 */
		DiEntry(const DiEntry &other);
#ifdef Q_COMPILER_RVALUE_REFS
		DiEntry(DiEntry &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~DiEntry(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] diId Info identifier.
		 * @param[in] locations List of locations.
		 */
		DiEntry(qint64 diId, const QStringList &locations);
#ifdef Q_COMPILER_RVALUE_REFS
		DiEntry(qint64 diId, QStringList &&locations);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Assignment operator.
		 *
		 * @param[in] other Info entry.
		 */
		DiEntry &operator=(const DiEntry &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		DiEntry &operator=(DiEntry &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const DiEntry &other) const;
		bool operator!=(const DiEntry &other) const;

		static
		void swap(DiEntry &first, DiEntry &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Return info identifier.
		 *
		 * @return Data info identifier.
		 */
		qint64 diId(void) const;
		void setDiId(qint64 id);

		/*!
		 * @brief Return list of locations.
		 *
		 * @return Locations.
		 */
		const QStringList &locations(void) const;
		void setLocations(const QStringList &l);
#ifdef Q_COMPILER_RVALUE_REFS
		void setLocations(QStringList &&l);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		DiEntry fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		DiEntry fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<DiEntryPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<DiEntryPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	/*!
	 * @brief List of delivery info identifiers.
	 */
	class DiEntryList : public Json::Object, public QList<DiEntry> {
	public:
		/* Expose list constructors. */
		using QList<DiEntry>::QList;

		/* Some older compilers complain about missing constructor. */
		DiEntryList(void);

		static
		DiEntryList fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		DiEntryList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	class StoredFilesRespPrivate;
	/*!
	 * @brief Encapsulates the stored_files response.
	 */
	class StoredFilesResp : public Json::Object {
		Q_DECLARE_PRIVATE(StoredFilesResp)
	public:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		StoredFilesResp(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Stored files response.
		 */
		StoredFilesResp(const StoredFilesResp &other);
#ifdef Q_COMPILER_RVALUE_REFS
		StoredFilesResp(StoredFilesResp &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~StoredFilesResp(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] dms List of data message entries.
		 * @param[in] dis List of delivery information entries.
		 * @param[in] limit Request limit.
		 * @param[in] error Error entry.
		 */
		StoredFilesResp(const DmEntryList &dms, const DiEntryList &dis,
		    int limit, const ErrorEntry &error);
#ifdef Q_COMPILER_RVALUE_REFS
		StoredFilesResp(DmEntryList &&dms, DiEntryList &&dis,
		    int limit, ErrorEntry &&error);
#endif /* Q_COMPILER_RVALUE_REFS */

		StoredFilesResp &operator=(const StoredFilesResp &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		StoredFilesResp &operator=(StoredFilesResp &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const StoredFilesResp &other) const;
		bool operator!=(const StoredFilesResp &other) const;

		static
		void swap(StoredFilesResp &first, StoredFilesResp &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Return list of data message entries.
		 *
		 * @return List of held data message entries.
		 */
		const DmEntryList &dms(void) const;
		void setDms(const DmEntryList &dms);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDms(DmEntryList &&dms);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Return list of delivery info entries.
		 *
		 * @return List of held delivery info entries.
		 */
		const DiEntryList &dis(void) const;
		void setDis(const DiEntryList &dis);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDis(DiEntryList &&dis);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Return request limit.
		 *
		 * @return Request limit as obtained from the service.
		 */
		int limit(void) const;
		void setLimit(int l);

		/*!
		 * @brief Return error entry.
		 *
		 * @return Error entry.
		 */
		const ErrorEntry &error(void) const;
		void setError(const ErrorEntry &e);
#ifdef Q_COMPILER_RVALUE_REFS
		void setError(ErrorEntry &&e);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		StoredFilesResp fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		StoredFilesResp fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<StoredFilesRespPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<StoredFilesRespPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

}
