/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMetaType>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */
#include <QString>

class AcntIdPrivate;
/*!
 * @brief Holds internal account identifier.
 */
class AcntId {
#ifdef QT_QML_LIB
	Q_GADGET
#endif /* QT_QML_LIB */
	Q_DECLARE_PRIVATE(AcntId)

public:
	/*
	 * Don't forget to declare types in order to make them work with
	 * the meta-object system.
	 */
	static
	void declareTypes(void);

#ifdef QT_QML_LIB
	Q_PROPERTY(bool null READ isNull)
	Q_PROPERTY(bool valid READ isValid)
	Q_PROPERTY(QString username READ username WRITE setUsername)
	Q_PROPERTY(bool testing READ testing WRITE setTesting)
	Q_PROPERTY(QString strId READ strId)
#endif /* QT_QML_LIB */

	AcntId(void);
	AcntId(const AcntId &other);
#ifdef Q_COMPILER_RVALUE_REFS
	AcntId(AcntId &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	virtual /* To keep the compiler quiet, the destructor needs not to be virtual here. */
	~AcntId(void);

	explicit AcntId(const QString &username, bool testing);
#ifdef Q_COMPILER_RVALUE_REFS
	explicit AcntId(QString &&username, bool testing);
#endif /* Q_COMPILER_RVALUE_REFS */

	AcntId &operator=(const AcntId &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	AcntId &operator=(AcntId &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const AcntId &other) const;
	bool operator!=(const AcntId &other) const;

	bool operator<(const AcntId &other) const;

	friend void swap(AcntId &first, AcntId &second) Q_DECL_NOTHROW;

	bool isNull(void) const;

	void clear(void);

	bool isValid(void) const;

	const QString &username(void) const;
	void setUsername(const QString &n);
#ifdef Q_COMPILER_RVALUE_REFS
	void setUsername(QString &&n);
#endif /* Q_COMPILER_RVALUE_REFS */
	bool testing(void) const;
	void setTesting(bool t);

	/* String identifier combining held information. */
	const QString &strId(void) const;

protected:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
	::std::unique_ptr<AcntIdPrivate> d_ptr;
#else /* < Qt-5.12 */
	QScopedPointer<AcntIdPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	/* Allow subclasses to initialise with their own specific Private. */
	AcntId(AcntIdPrivate *d);

private:
	/* Allow parent code to instantiate *Private subclass. */
	virtual
	bool ensurePrivate(void);
};

void swap(AcntId &first, AcntId &second) Q_DECL_NOTHROW;

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
size_t qHash(const AcntId &key, size_t seed = 0);
#else /* < Qt-6.0 */
uint qHash(const AcntId &key, uint seed = 0);
#endif /* >= Qt-6.0 */

Q_DECLARE_METATYPE(AcntId)
