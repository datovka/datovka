/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/settings/prefs_helper.h"

/*!
 * @brief PIMPL AcntId class.
 */
class AcntIdPrivate {
	//Q_DISABLE_COPY(AcntIdPrivate)
public:
	AcntIdPrivate(void)
	    : m_username(), m_testing(false), _m_strId()
	{
		updateStrId();
	}

	virtual
	~AcntIdPrivate(void)
	{ }

	AcntIdPrivate &operator=(const AcntIdPrivate &other) Q_DECL_NOTHROW
	{
		m_username = other.m_username;
		m_testing = other.m_testing;
		_m_strId = other._m_strId;

		return *this;
	}

	bool operator==(const AcntIdPrivate &other) const
	{
		return (m_username == other.m_username) &&
		    (m_testing == other.m_testing);
	}

	bool operator<(const AcntIdPrivate &other) const
	{
		return (m_username != other.m_username) ?
		    (m_username < other.m_username) :
		    (m_testing < other.m_testing);
	}

	void updateStrId(void)
	{
		_m_strId = PrefsHelper::accountIdentifier(m_username, m_testing);
	}

	QString m_username; /*!< User account login. */
	bool m_testing; /*!< Whether this is a testing environment account. */

	QString _m_strId; /*!< Identifier string encoding all other data. */
};
