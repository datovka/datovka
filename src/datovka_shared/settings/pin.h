/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QObject>
#include <QString>

class QSettings; /* Forward declaration. */

/*!
 * @brief Encapsulates code needed for storing PIN code.
 */
class PinSettings : public QObject {
	Q_OBJECT
public:
	/*!
	 * @brief Constructor.
	 */
	PinSettings(void);

	PinSettings(const PinSettings &other);
#ifdef Q_COMPILER_RVALUE_REFS
	PinSettings(PinSettings &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

	/*!
	 * @note Emits contentChanged().
	 */
	PinSettings &operator=(const PinSettings &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	PinSettings &operator=(PinSettings &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const PinSettings &other) const;
	bool operator!=(const PinSettings &other) const;

	/*!
	 * @brief Load data from supplied settings.
	 *
	 * @param[in] settings Settings structure.
	 */
	void loadFromSettings(const QSettings &settings);

	/*!
	 * @brief Store data to settings structure.
	 *
	 * @param[out] settings Settings structure.
	 */
	void saveToSettings(QSettings &settings) const;

	/*!
	 * @brief Check whether PIN value is configured.
	 *
	 * @return True if algorithm, salt and encoded PIN are set.
	 */
	bool pinConfigured(void) const;

	/*!
	 * @brief Return stored PIN value.
	 *
	 * @note Proper PIN value may be returned only after pin is loaded from
	 *    settings and verified.
	 * @return PIN value.
	 */
	const QString &pinValue(void) const;

	/*!
	 * @brief Update PIN value.
	 *
	 * @note Emits contentChanged().
	 *
	 * @param[in] newPinValue New PIN value.
	 */
	void updatePinValue(const QString &newPinValue);

	/*!
	 * @brief Verifies the PIN.
	 *
	 * @note PIN value is stored within setting structure if entered PIN is
	 *     valid.
	 *
	 * @param[in] pinValue PIN to be verified.
	 * @return True if PIN value was successfully verified.
	 */
	bool verifyPin(const QString &pinValue);

Q_SIGNALS:
	/*!
	 * @brief Emitted when content changed.
	 */
	void contentChanged(void);

private:
	QString _pinVal; /*!< PIN value is not read from the configuration file, nor it is stored to the configuration file. */
	QString pinAlg; /*!< PIN algorithm identifier. */
	QByteArray pinSalt; /*!< Salt value used to generate PIN hash. */
	QByteArray pinCode; /*!< Hashed PIN code. */
};
