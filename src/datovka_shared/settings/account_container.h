/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMap>
#include <QObject>
#include <QString>
#include <QStringList>

#include "src/datovka_shared/identifiers/account_id.h"

class QSettings; /* Forward declaration. */

/*!
 * It is not possible to mix templates wit Q_OBJECT,
 * see https://stackoverflow.com/a/19138963 .
 */
class AcntContainer : public QObject {
	Q_OBJECT
public:
	/*!
	 * @brief Determines whether the contained holds regular or shadow
	 *     accounts.
	 */
	enum CredentialType {
		CT_REGULAR = 0, /*!< Regular accounts. */
		CT_SHADOW /*!< Shadow accounts. */
	};

signals:
	/*!
	 * @brief Notifies that an account is going to be added.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] index Index of the account.
	 */
	void accountAboutToBeAdded(const AcntId &acntId, int index);

	/*!
	 * @brief Notifies that an account has been added.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] index Index of the account.
	 */
	void accountAdded(const AcntId &acntId, int index);

	/*!
	 * @brief Notifies that an account is going to be removed.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] index Index of the account.
	 */
	void accountAboutToBeRemoved(const AcntId &acntId, int index);

	/*!
	 * @brief Notifies that an account has been removed.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] index Index of the account.
	 */
	void accountRemoved(const AcntId &acntId, int index);

	/*!
	 * @brief Notifies that accounts are going to be moved.
	 *
	 * @param[in] srcFrst Index of the first account in the sequence.
	 * @param[in] srcLst Index of the last account in the sequence.
	 * @param[in] dst Destination index.
	 */
	void accountsAboutToBeMoved(int srcFrst, int srcLst, int dst);

	/*!
	 * @brief Notifies that accounts have been moved.
	 *
	 * @param[in] srcFrst Index of the first account in the sequence.
	 * @param[in] srcLst Index of the last account in the sequence.
	 * @param[in] dst Destination index.
	 */
	void accountsMoved(int srcFrst, int srcLst, int dst);

	/*!
	 * @brief Notifies that username and possibly other account data have chnaged.
	 *
	 * @param[in] oldId Old account identifier.
	 * @param[in] newId New account identifier.
	 * @param[in] index Index of the account.
	 */
	void accountUsernameChanged(const AcntId &oldId, const AcntId &newId, int index);

	/*!
	 * @brief Notifies that account data have changed. Username did no change.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void accountDataChanged(const AcntId &acntId);

	/*!
	 * @brief Notifies that the content is going to be changed.
	 */
	void contentAboutToBeReset(void);

	/*!
	 * @brief Notifies that the content has been changed.
	 *
	 * @note This signal should be emitted always when the content changes
	 *    (e.g. when loaded from settings or elsewhere).
	 */
	void contentReset(void);

	/*!
	 * @brief Notifies that the content has been changed from within
	 *     the application.
	 *
	 * @note This signal should be emitted when already loaded content
	 *     changes because it was modified internally.
	 */
	void contentChanged(void);
};

/*!
 * @brief Associative array mapping a username to some settings.
 */
template <class T>
class AcntSettingsMap : public AcntContainer, public QMap<AcntId, T> {
public:
	/*!
	 * @brief Constructor.
	 *
	 * @note It is not possible to change the type of the accounts after
	 *     the container has been created.
	 *
	 * @param regularAccounts True for regular accounts,
	 *                        false for shadow accounts.
	 */
	explicit AcntSettingsMap(enum AcntContainer::CredentialType credType);

	/*!
	 * @brief Load data from supplied settings.
	 *
	 * @param[in] confDir Configuration directory path.
	 * @param[in] settings Settings structure to load data from.
	 */
	void loadFromSettings(const QString &confDir,
	    const QSettings &settings);

	/*!
	 * @brief Decrypts all encrypted passwords.
	 *
	 * @param[in] pinVal Pin value.
	 */
	void decryptAllPwds(const QString &pinVal);

	/*!
	 * @brief Returns the first found account identifier that matches the
	 *     supplied username.
	 *
	 * @note The usage if this method should be avoided because it ignores
	 *     the testing value.
	 *
	 * @param[in] username Username to search for.
	 * @return Matching account identifier of invalid instance if not found.
	 */
	AcntId acntIdFromUsername(const QString &username) const;

	/*!
	 * @brief Return a (numerically) sorted list of regular credential groups.
	 *
	 * @param[in] settings Setting to read from.
	 * @return Numerically sorted regular credential groups.
	 */
	static
	QStringList sortedCredentialGroups(const QSettings &settings);

	/*!
	 * @brief Return a (numerically) sorted list of shadow credential groups.
	 *
	 * @param[in] settings Setting to read from.
	 * @return Numerically sorted shadow credential groups.
	 */
	static
	QStringList sortedShadowGroups(const QSettings &settings);

	/*!
	 * @brief Create regular credential group name.
	 *
	 * @param[in] num Group number (greater than 0).
	 * @return Regular credential group name.
	 */
	static
	QString credentialGroupName(int num);

	/*!
	 * @brief Create shadow credential group name.
	 *
	 * @param[in] num Group number (greater than 0).
	 * @return Shadow credential group name.
	 */
	static
	QString shadowGroupName(int num);

	/*!
	 * @brief Add an account containing only a description and a username.
	 *
	 * @param[in] setting Setting to be modified.
	 * @param[in] accountName Account name.
	 * @param[in] acntId Account identifier.
	 */
	static
	void addCredentialsTorso(QSettings &settings,
	    const QString &accountName, const AcntId &acntId);

protected:
	const enum AcntContainer::CredentialType m_credType; /*!< Type of held credentials. */
};
