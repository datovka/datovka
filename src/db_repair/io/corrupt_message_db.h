/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QSet>

#include "src/io/message_db.h"

class QString; /* Forward declaration. */

class CorruptMessageDb : public MessageDb {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] connectionName Connection name.
	 */
	explicit CorruptMessageDb(const QString &connectionName);

	/*!
	 * @brief Open database file.
	 *
	 * @note It opens the database file immediately, despite the fact that
	 *     the parent class uses delayed opening.
	 *
	 * @param[in] fileName File name.
	 * @param[in] flag Can be NO_OPTIONS or CREATE_MISSING.
	 * @return True on success, false on any error.
	 */
	bool openDb(const QString &fileName,
	    enum SQLiteDb::OpenFlag flag = SQLiteDb::NO_OPTIONS);

	/*!
	 * @brief Check whether there is any relevant content.
	 *
	 * @note It searches for tables containing the message list, raw
	 *     messages and raw delivery info data.
	 *
	 * @return True is any of the tables is found.
	 */
	bool containsKnownTables(void);

	/*!
	 * @brief Check whether database knows high-volume flags.
	 *
	 * @note Older database versions didn't know this type of data.
	 *
	 * @return True f database knows the data.
	 */
	bool knowsDmVodz(void);

	/*!
	 * @brief Get message envelope.
	 *
	 * note Older database versions didn't know dmVODZ.
	 *
	 * @param[in] dmId Message id.
	 * @return Message envelope structure, null structure on failure.
	 */
	Isds::Envelope getMessageEnvelopeNoDmVodz(qint64 dmId);

	/*!
	 * @brief Collect message identifiers from table 'messages'.
	 *
	 * @param[out] all All message identifiers.
	 */
	void collectDmIdsFromMsgTable(QSet<qint64> &all);

	/*!
	 * @brief Collect message identifiers from table 'raw_delivery_info_data'.
	 *
	 * @param[out] received Message identifiers of received messages.
	 * @param[out] sent Message identifiers of sent messages.
	 * @param[out] unknown Message identifiers of unknown direction.
	 */
	void collectDmIdsFromRawMsgTable(QSet<qint64> &received,
	    QSet<qint64> &sent, QSet<qint64> &unknown);

	/*!
	 * @brief Collect message identifiers from table 'raw_delivery_info_data'.
	 *
	 * @param[out] all All message identifiers.
	 */
	void collectDmIdsFromRawDelInfoTable(QSet<qint64> &all);

	/*!
	 * @brief Collect message identifiers from table 'supplementary_message_data'.
	 *
	 * @param[out] received Message identifiers of received messages.
	 * @param[out] sent Message identifiers of sent messages.
	 * @param[out] unknown Message identifiers of unknown direction.
	 */
	void collectDmIdsFromSupplementary(QSet<qint64> &received,
	    QSet<qint64> &sent, QSet<qint64> &unknown);

	/*!
	 * @brief Collect message identifiers from table 'supplementary_message_data'.
	 *
	 * @param[out] highVolume Message identifiers with high-volume flag set.
	 */
	void collectDmIdsFromSupplementaryVODZ(QSet<qint64> &highVolume);

	/*!
	 * @brief Get message data in raw format.
	 *
	 * @note Similar to MessageDb::getCompleteMessageRaw() but reads data
	 *     only from database not from an external file.
	 *
	 * @param[in] dmId Message identifier.
	 * @return Empty byte array on error.
	 */
	QByteArray getCompleteMessageRawDirect(qint64 dmId);

	/*!
	 * @brief Get delivery info data in raw format.
	 *
	 * @note Similar to MessageDb::getDeliveryInfoRaw() but reads data
	 *     only from database not from an external file.
	 *
	 * @param[in] dmId Message identifier.
	 * @return Empty byte array on error.
	 */
	QByteArray getDeliveryInfoRawDirect(qint64 dmId);
};
