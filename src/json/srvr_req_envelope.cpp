/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QJsonValue>
#include <QJsonObject>
#include <QString>
#include <utility> /* ::std::swap */

#include "src/datovka_shared/json/helper.h"
#include "src/json/srvr_req_envelope.h"

static const QString nullString;
static const QJsonValue nullJsonValue;

class Json::SrvrRequestEnvelopePrivate {
public:
	SrvrRequestEnvelopePrivate(void)
	    : m_jsonValueName(), m_jsonValue()
	{ }

	SrvrRequestEnvelopePrivate &operator=(const SrvrRequestEnvelopePrivate &other) Q_DECL_NOTHROW
	{
		m_jsonValueName = other.m_jsonValueName;
		m_jsonValue = other.m_jsonValue;

		return *this;
	}

	bool operator==(const SrvrRequestEnvelopePrivate &other) const
	{
		return (m_jsonValueName == other.m_jsonValueName) &&
		    (m_jsonValue == other.m_jsonValue);
	}

	QString m_jsonValueName;
	QJsonValue m_jsonValue;
};

Json::SrvrRequestEnvelope::SrvrRequestEnvelope(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::SrvrRequestEnvelope::SrvrRequestEnvelope(const SrvrRequestEnvelope &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) SrvrRequestEnvelopePrivate) : Q_NULLPTR)
{
	Q_D(SrvrRequestEnvelope);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::SrvrRequestEnvelope::SrvrRequestEnvelope(SrvrRequestEnvelope &&other) Q_DECL_NOEXCEPT
    : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::SrvrRequestEnvelope::~SrvrRequestEnvelope(void)
{
}

/*!
 * @brief Ensures private server response envelope presence.
 *
 * @note Returns if private server response envelope could not be allocated.
 */
#define ensureSrvrRequestEnvelopePrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			SrvrRequestEnvelopePrivate *p = new (::std::nothrow) SrvrRequestEnvelopePrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::SrvrRequestEnvelope &Json::SrvrRequestEnvelope::operator=(const SrvrRequestEnvelope &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureSrvrRequestEnvelopePrivate(*this);
	Q_D(SrvrRequestEnvelope);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::SrvrRequestEnvelope &Json::SrvrRequestEnvelope::operator=(SrvrRequestEnvelope &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::SrvrRequestEnvelope::operator==(const SrvrRequestEnvelope &other) const
{
	Q_D(const SrvrRequestEnvelope);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::SrvrRequestEnvelope::operator!=(const SrvrRequestEnvelope &other) const
{
	return !operator==(other);
}

bool Json::SrvrRequestEnvelope::isNull(void) const
{
	Q_D(const SrvrRequestEnvelope);
	return d == Q_NULLPTR;
}

const QString &Json::SrvrRequestEnvelope::jsonValueName(void) const
{
	Q_D(const SrvrRequestEnvelope);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_jsonValueName;
}

void Json::SrvrRequestEnvelope::setJsonValueName(const QString &n)
{
	ensureSrvrRequestEnvelopePrivate();
	Q_D(SrvrRequestEnvelope);
	d->m_jsonValueName = n;
}

#ifdef COMPILER_RVALUE_REFS
void Json::SrvrRequestEnvelope::setJsonValueName(QString &&n)
{
	ensureSrvrRequestEnvelopePrivate();
	Q_D(SrvrRequestEnvelope);
	d->m_jsonValueName = ::std::move(n);
}
#endif /* COMPILER_RVALUE_REFS */

const QJsonValue &Json::SrvrRequestEnvelope::jsonValue(void) const
{
	Q_D(const SrvrRequestEnvelope);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullJsonValue;
	}

	return d->m_jsonValue;
}

void Json::SrvrRequestEnvelope::setJsonValue(const QJsonValue &v)
{
	ensureSrvrRequestEnvelopePrivate();
	Q_D(SrvrRequestEnvelope);
	d->m_jsonValue = v;
}

#ifdef COMPILER_RVALUE_REFS
void Json::SrvrRequestEnvelope::setJsonValue(QJsonValue &&v)
{
	ensureSrvrRequestEnvelopePrivate();
	Q_D(SrvrRequestEnvelope);
	d->m_jsonValue = ::std::move(v);
}
#endif /* COMPILER_RVALUE_REFS */

Json::SrvrRequestEnvelope Json::SrvrRequestEnvelope::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return SrvrRequestEnvelope();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString keyContentType("contentType");
static const QString keyContentValue("contentValue");

Json::SrvrRequestEnvelope Json::SrvrRequestEnvelope::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	SrvrRequestEnvelope sre;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyContentType,
			        valStr, Helper::ACCEPT_NULL))) {
				goto fail;
			}
			sre.setJsonValueName(::std::move(valStr));
		}
		{
			QJsonValue jsonVal;
			if (!Helper::readValue(jsonObj, keyContentValue, jsonVal,
			        Helper::ACCEPT_NULL)) {
				goto fail;
			}
			sre.setJsonValue(::std::move(jsonVal));
		}

		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return sre;
	}

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return SrvrRequestEnvelope();
}

bool Json::SrvrRequestEnvelope::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyContentType, !jsonValueName().isNull() ? jsonValueName() : QJsonValue());
	jsonObj.insert(keyContentValue, jsonValue());

	jsonVal = jsonObj;
	return true;
}

void Json::swap(SrvrRequestEnvelope &first, SrvrRequestEnvelope &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
