/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QMetaType>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

namespace Json {
	class Int64StringList; /* Forward declaration. */
}
class QByteArray; /* Forward declaration. */
class QJsonValue; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Json {

	class TagTextSearchRequestPrivate;
	/*
	 * @brief Encapsulates text search request.
	 */
	class TagTextSearchRequest : public Object {
		Q_DECLARE_PRIVATE(TagTextSearchRequest)
	public:
		/*!
		 * @brief Search type.
		 */
		enum SearchType {
			ST_SUBSTRING = 0, /*!< Matches a substring. */
			ST_FULL, /*!< Matches a full string. */
			ST_FULL_CASE_INSENSITIVE /*!< Matches a full string, case insensitive. */
		};

		TagTextSearchRequest(void);
		TagTextSearchRequest(const TagTextSearchRequest &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagTextSearchRequest(TagTextSearchRequest &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~TagTextSearchRequest(void);

		explicit TagTextSearchRequest(const QString &text, enum SearchType type);
#ifdef Q_COMPILER_RVALUE_REFS
		explicit TagTextSearchRequest(QString &&text, enum SearchType type);
#endif /* Q_COMPILER_RVALUE_REFS */

		TagTextSearchRequest &operator=(const TagTextSearchRequest &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		TagTextSearchRequest &operator=(TagTextSearchRequest &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const TagTextSearchRequest &other) const;
		bool operator!=(const TagTextSearchRequest &other) const;

		friend void swap(TagTextSearchRequest &first, TagTextSearchRequest &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		bool isValid(void) const;

		/* sought text */
		const QString &text(void) const;
		void setText(const QString &t);
#ifdef Q_COMPILER_RVALUE_REFS
		void setText(QString &&t);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* search type */
		enum SearchType type(void) const;
		void setType(enum SearchType t);

		static
		TagTextSearchRequest fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagTextSearchRequest fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<TagTextSearchRequestPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<TagTextSearchRequestPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(TagTextSearchRequest &first, TagTextSearchRequest &second) Q_DECL_NOTHROW;

	/*!
	 * @brief List of search requests.
	 */
	class TagTextSearchRequestList : public Object, public QList<TagTextSearchRequest> {
	public:
		/* Expose list constructors. */
		using QList<TagTextSearchRequest>::QList;

		/* Some older compilers complain about missing constructor. */
		TagTextSearchRequestList(void);

		TagTextSearchRequestList(const QList<TagTextSearchRequest> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagTextSearchRequestList(QList<TagTextSearchRequest> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		TagTextSearchRequestList fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagTextSearchRequestList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	/*!
	 * @brief Return a hash value for the supplied search request.
	 *
	 * @param[in] key Search request entry to compute hash from.
	 * @param[in] seed Is used to initialise the hash if specified.
	 * @return Hashed search request.
	 */
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
	size_t qHash(const TagTextSearchRequest &key, size_t seed = 0);
#else /* < Qt-6.0 */
	uint qHash(const TagTextSearchRequest &key, uint seed = 0);
#endif /* >= Qt-6.0 */

	class TagTextSearchResponsePrivate;
	/*
	 * @brief Encapsulates text search request.
	 */
	class TagTextSearchResponse : public Object {
		Q_DECLARE_PRIVATE(TagTextSearchResponse)
	public:
		TagTextSearchResponse(void);
		TagTextSearchResponse(const TagTextSearchResponse &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagTextSearchResponse(TagTextSearchResponse &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~TagTextSearchResponse(void);

		explicit TagTextSearchResponse(const QString &text,
		    enum TagTextSearchRequest::SearchType type,
		    const Int64StringList &ids);
#ifdef Q_COMPILER_RVALUE_REFS
		explicit TagTextSearchResponse(QString &&text,
		    enum TagTextSearchRequest::SearchType type,
		    Int64StringList &&ids);
#endif /* Q_COMPILER_RVALUE_REFS */

		TagTextSearchResponse &operator=(const TagTextSearchResponse &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		TagTextSearchResponse &operator=(TagTextSearchResponse &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const TagTextSearchResponse &other) const;
		bool operator!=(const TagTextSearchResponse &other) const;

		friend void swap(TagTextSearchResponse &first, TagTextSearchResponse &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		bool isValid(void) const;

		/* sought text */
		const QString &text(void) const;
		void setText(const QString &t);
#ifdef Q_COMPILER_RVALUE_REFS
		void setText(QString &&t);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* search type */
		enum TagTextSearchRequest::SearchType type(void) const;
		void setType(enum TagTextSearchRequest::SearchType t);
		/* found indetifiers */
		const Int64StringList &ids(void) const;
		void setIds(const Int64StringList &ids);
#ifdef Q_COMPILER_RVALUE_REFS
		void setIds(Int64StringList &&ids);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		TagTextSearchResponse fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagTextSearchResponse fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<TagTextSearchResponsePrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<TagTextSearchResponsePrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(TagTextSearchResponse &first, TagTextSearchResponse &second) Q_DECL_NOTHROW;

}

Q_DECLARE_METATYPE(Json::TagTextSearchRequest)
Q_DECLARE_METATYPE(Json::TagTextSearchRequestList)
Q_DECLARE_METATYPE(Json::TagTextSearchResponse)
