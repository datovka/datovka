/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QList>
#include <QMetaType>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/isds/types.h"
#include "src/datovka_shared/json/object.h"

namespace Json {
	class TagEntryList; /* Forward declaration. */
	class TagMsgIdList; /* Forward declaration. */
}

class QJsonValue; /* Forward declaration. */

namespace Json {

	class TagAssignmentCountPrivate;
	/*
	 * @brief Encapsulates tag to message assignment count.
	 */
	class TagAssignmentCount : public Object {
		Q_DECLARE_PRIVATE(TagAssignmentCount)
	public:
		TagAssignmentCount(void);
		TagAssignmentCount(const TagAssignmentCount &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagAssignmentCount(TagAssignmentCount &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~TagAssignmentCount(void);

		explicit TagAssignmentCount(qint64 tagId, qint64 assignmentCount);

		TagAssignmentCount &operator=(const TagAssignmentCount &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		TagAssignmentCount &operator=(TagAssignmentCount &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const TagAssignmentCount &other) const;
		bool operator!=(const TagAssignmentCount &other) const;

		friend void swap(TagAssignmentCount &first, TagAssignmentCount &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		bool isValid(void) const;

		/* tag identifier */
		qint64 tagId(void) const;
		void setTagId(qint64 i);
		/* assignment count */
		qint64 assignmentCount(void) const;
		void setAssignmentCount(qint64 c);

		static
		TagAssignmentCount fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagAssignmentCount fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<TagAssignmentCountPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<TagAssignmentCountPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(TagAssignmentCount &first, TagAssignmentCount &second) Q_DECL_NOTHROW;

	class TagIdToMsgIdListPrivate;
	/*
	 * @brief Encapsulates tag to message identifier list.
	 */
	class TagIdToMsgIdList : public Object {
		Q_DECLARE_PRIVATE(TagIdToMsgIdList)
	public:
		TagIdToMsgIdList(void);
		TagIdToMsgIdList(const TagIdToMsgIdList &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagIdToMsgIdList(TagIdToMsgIdList &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~TagIdToMsgIdList(void);

		explicit TagIdToMsgIdList(qint64 tagId, const TagMsgIdList &tagMsgIds);
#ifdef Q_COMPILER_RVALUE_REFS
		explicit TagIdToMsgIdList(qint64 tagId, TagMsgIdList &&tagMsgIds);
#endif /* Q_COMPILER_RVALUE_REFS */

		TagIdToMsgIdList &operator=(const TagIdToMsgIdList &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		TagIdToMsgIdList &operator=(TagIdToMsgIdList &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const TagIdToMsgIdList &other) const;
		bool operator!=(const TagIdToMsgIdList &other) const;

		friend void swap(TagIdToMsgIdList &first, TagIdToMsgIdList &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		bool isValid(void) const;

		/* tag identifier */
		qint64 tagId(void) const;
		void setTagId(qint64 i);
		/* message identifier list */
		const TagMsgIdList &tagMsgIds(void) const;
		void setTagMsgIds(const TagMsgIdList &l);
#ifdef Q_COMPILER_RVALUE_REFS
		void setTagMsgIds(TagMsgIdList &&l);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		TagIdToMsgIdList fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagIdToMsgIdList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<TagIdToMsgIdListPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<TagIdToMsgIdListPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(TagIdToMsgIdList &first, TagIdToMsgIdList &second) Q_DECL_NOTHROW;

	class TagAssignmentPrivate;
	/*!
	 * @brief Holds tag assignment.
	 */
	class TagAssignment : public Object {
		Q_DECLARE_PRIVATE(TagAssignment)
	public:
		TagAssignment(void);
		TagAssignment(const TagAssignment &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagAssignment(TagAssignment &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~TagAssignment(void);

		explicit TagAssignment(enum Isds::Type::NilBool testEnv,
		    qint64 dmId, const TagEntryList &entries);
#ifdef Q_COMPILER_RVALUE_REFS
		explicit TagAssignment(enum Isds::Type::NilBool testEnv,
		    qint64 dmId, TagEntryList &&entries);
#endif /* Q_COMPILER_RVALUE_REFS */

		TagAssignment &operator=(const TagAssignment &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		TagAssignment &operator=(TagAssignment &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const TagAssignment &other) const;
		bool operator!=(const TagAssignment &other) const;

		friend void swap(TagAssignment &first, TagAssignment &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		bool isValid(void) const;

		/* test environment */
		enum Isds::Type::NilBool testEnv(void) const;
		void setTestEnv(enum Isds::Type::NilBool t);
		/* message identifier */
		qint64 dmId(void) const;
		void setDmId(qint64 di);
		/* entries */
		const TagEntryList &entries(void) const;
		void setEntries(const TagEntryList &l);
#ifdef Q_COMPILER_RVALUE_REFS
		void setEntries(TagEntryList &&l);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		TagAssignment fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagAssignment fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<TagAssignmentPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<TagAssignmentPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(TagAssignment &first, TagAssignment &second) Q_DECL_NOTHROW;

	/*!
	 * @brief List of tag assignments.
	 */
	class TagAssignmentList : public Object, public QList<TagAssignment> {
	public:
		/* Expose list constructors. */
		using QList<TagAssignment>::QList;

		/* Some older compilers complain about missing constructor. */
		TagAssignmentList(void);

		TagAssignmentList(const QList<TagAssignment> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagAssignmentList(QList<TagAssignment> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		TagAssignmentList fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagAssignmentList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

}

Q_DECLARE_METATYPE(Json::TagAssignmentCount)
Q_DECLARE_METATYPE(Json::TagIdToMsgIdList)
Q_DECLARE_METATYPE(Json::TagAssignment)
Q_DECLARE_METATYPE(Json::TagAssignmentList)
