/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QList>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

class QJsonValue; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Json {

	class SrvrProfilePrivate;
	/*!
	 * @brief Server response envelope.
	 */
	class SrvrProfile : public Object {
		Q_DECLARE_PRIVATE(SrvrProfile)

	public:
		SrvrProfile(void);
		SrvrProfile(const SrvrProfile &other);
#ifdef Q_COMPILER_RVALUE_REFS
		SrvrProfile(SrvrProfile &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~SrvrProfile(void);

		SrvrProfile &operator=(const SrvrProfile &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		SrvrProfile &operator=(SrvrProfile &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const SrvrProfile &other) const;
		bool operator!=(const SrvrProfile &other) const;

		friend void swap(SrvrProfile &first, SrvrProfile &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/* identifier */
		qint64 id(void) const;
		void setId(qint64 i);
		/* profile name */
		const QString &name(void) const;
		void setName(const QString &n);
#ifdef Q_COMPILER_RVALUE_REFS
		void setName(QString &&n);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* profile description */
		const QString &description(void) const;
		void setDescription(const QString &de);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDescription(QString &&de);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates an instance from supplied JSON document.
		 *
		 * @param[in]  json JSON data.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		SrvrProfile fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Creates an instance from supplied JSON data.
		 *
		 * @param[in]  jsonObj JSON data.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		SrvrProfile fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Writes content of JSON object.
		 *
		 * @param[out] jsonVal JSON value to write to.
		 * @return True on success.
		 */
		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<SrvrProfilePrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<SrvrProfilePrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(SrvrProfile &first, SrvrProfile &second) Q_DECL_NOTHROW;

	/*!
	 * @brief List of profiles.
	 */
	class SrvrProfileList : public Object, public QList<SrvrProfile> {
	public:
		/* Expose list constructors. */
		using QList<SrvrProfile>::QList;

		/* Some older compilers complain about missing constructor. */
		SrvrProfileList(void);

		SrvrProfileList(const QList<SrvrProfile> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		SrvrProfileList(QList<SrvrProfile> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		SrvrProfileList fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		SrvrProfileList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

}
