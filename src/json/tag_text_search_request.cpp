/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QHash> /* ::qHash */
#include <QJsonObject>
#include <QString>
#include <utility> /* ::std::swap */

#include "src/datovka_shared/json/basic.h"
#include "src/datovka_shared/json/helper.h"
#include "src/json/tag_text_search_request.h"

#define DFLT_TYPE TagTextSearchRequest::ST_SUBSTRING
static const QString nullString;
static const Json::Int64StringList nullInt64StringList;

/*!
 * @brief PIMPL TagTextSearchRequest class.
 */
class Json::TagTextSearchRequestPrivate {
	//Q_DISABLE_COPY(TagTextSearchRequestPrivate)
public:
	TagTextSearchRequestPrivate(void)
	    : m_text(), m_type(DFLT_TYPE)
	{ }

	TagTextSearchRequestPrivate &operator=(const TagTextSearchRequestPrivate &other) Q_DECL_NOTHROW
	{
		m_text = other.m_text;
		m_type = other.m_type;

		return *this;
	}

	bool operator==(const TagTextSearchRequestPrivate &other) const
	{
		return (m_text == other.m_text) &&
		    (m_type == other.m_type);
	}

	QString m_text;
	enum TagTextSearchRequest::SearchType m_type;
};

Json::TagTextSearchRequest::TagTextSearchRequest(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::TagTextSearchRequest::TagTextSearchRequest(const TagTextSearchRequest &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) TagTextSearchRequestPrivate) : Q_NULLPTR)
{
	Q_D(TagTextSearchRequest);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagTextSearchRequest::TagTextSearchRequest(TagTextSearchRequest &&other) Q_DECL_NOEXCEPT
    : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagTextSearchRequest::~TagTextSearchRequest(void)
{
}

Json::TagTextSearchRequest::TagTextSearchRequest(const QString &text, enum SearchType type)
    : Object(),
    d_ptr(new (::std::nothrow) TagTextSearchRequestPrivate)
{
	Q_D(TagTextSearchRequest);
	d->m_text = text;
	d->m_type = type;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagTextSearchRequest::TagTextSearchRequest(QString &&text, enum SearchType type)
    : Object(),
    d_ptr(new (::std::nothrow) TagTextSearchRequestPrivate)
{
	Q_D(TagTextSearchRequest);
	d->m_text = ::std::move(text);
	d->m_type = type;
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures TagTextSearchRequestPrivate presence.
 *
 * @note Returns if TagTextSearchRequestPrivate could not be allocated.
 */
#define ensureTagTextSearchRequestPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			TagTextSearchRequestPrivate *p = new (::std::nothrow) TagTextSearchRequestPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::TagTextSearchRequest &Json::TagTextSearchRequest::operator=(const TagTextSearchRequest &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureTagTextSearchRequestPrivate(*this);
	Q_D(TagTextSearchRequest);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagTextSearchRequest &Json::TagTextSearchRequest::operator=(TagTextSearchRequest &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::TagTextSearchRequest::operator==(const TagTextSearchRequest &other) const
{
	Q_D(const TagTextSearchRequest);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::TagTextSearchRequest::operator!=(const TagTextSearchRequest &other) const
{
	return !operator==(other);
}

bool Json::TagTextSearchRequest::isNull(void) const
{
	Q_D(const TagTextSearchRequest);
	return d == Q_NULLPTR;
}

bool Json::TagTextSearchRequest::isValid(void) const
{
	return (!isNull()) && (!text().isEmpty());
}

const QString &Json::TagTextSearchRequest::text(void) const
{
	Q_D(const TagTextSearchRequest);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_text;
}

void Json::TagTextSearchRequest::setText(const QString &t)
{
	ensureTagTextSearchRequestPrivate();
	Q_D(TagTextSearchRequest);
	d->m_text = t;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Json::TagTextSearchRequest::setText(QString &&t)
{
	ensureTagTextSearchRequestPrivate();
	Q_D(TagTextSearchRequest);
	d->m_text = ::std::move(t);
}
#endif /* Q_COMPILER_RVALUE_REFS */

enum Json::TagTextSearchRequest::SearchType Json::TagTextSearchRequest::type(void) const
{
	Q_D(const TagTextSearchRequest);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return DFLT_TYPE;
	}

	return d->m_type;
}

void Json::TagTextSearchRequest::setType(enum SearchType t)
{
	ensureTagTextSearchRequestPrivate();
	Q_D(TagTextSearchRequest);
	d->m_type = t;
}

Json::TagTextSearchRequest Json::TagTextSearchRequest::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagTextSearchRequest();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString valSubstring("match_substring");
static const QString valFull("match_full");
static const QString valFullCaseInsensitive("match_full_case_insensitive");

/*!
 * @brief Converts search type to string.
 *
 * @param[in]  type Search type enumeration value.
 * @param[out] ok Set to true on success, false on error.
 * @return String.
 */
static
const QString &searchTypeToString(
    enum Json::TagTextSearchRequest::SearchType type, bool *ok = Q_NULLPTR)
{
	switch (type) {
	case Json::TagTextSearchRequest::ST_SUBSTRING:
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return valSubstring;
		break;
	case Json::TagTextSearchRequest::ST_FULL:
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return valFull;
		break;
	case Json::TagTextSearchRequest::ST_FULL_CASE_INSENSITIVE:
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return valFullCaseInsensitive;
		break;
	default:
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return valSubstring;
		break;
	}
}

/*!
 * @brief Converts string to search type.
 *
 * @param[in]  str String.
 * @param[out] ok Set to true on success, false on error.
 * @return Search type.
 */
static
enum Json::TagTextSearchRequest::SearchType stringToSearchType(
    const QString &str, bool *ok = Q_NULLPTR)
{
	if (str == valSubstring) {
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return Json::TagTextSearchRequest::ST_SUBSTRING;
	} else if (str == valFull) {
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return Json::TagTextSearchRequest::ST_FULL;
	} else if (str == valFullCaseInsensitive) {
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return Json::TagTextSearchRequest::ST_FULL_CASE_INSENSITIVE;
	}

	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Json::TagTextSearchRequest::ST_SUBSTRING;
}

static const QString keyText("text");
static const QString keyType("type");

Json::TagTextSearchRequest Json::TagTextSearchRequest::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	TagTextSearchRequest stsr;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();

		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyText,
			        valStr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			stsr.setText(::std::move(valStr));
		}
		{
			bool iOk = false;
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyType,
			        valStr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			stsr.setType(stringToSearchType(valStr, &iOk));
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return stsr;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagTextSearchRequest();
}

bool Json::TagTextSearchRequest::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;
	bool iOk = false;

	jsonObj.insert(keyText, (!text().isEmpty()) ? text() : QJsonValue());
	jsonObj.insert(keyType, searchTypeToString(type(), &iOk));
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	jsonVal = jsonObj;
	return true;

fail:
	jsonVal = QJsonValue();
	return false;
}

void Json::swap(TagTextSearchRequest &first, TagTextSearchRequest &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

Json::TagTextSearchRequestList::TagTextSearchRequestList(void)
    : Object(),
    QList<TagTextSearchRequest>()
{
}

Json::TagTextSearchRequestList::TagTextSearchRequestList(
    const QList<TagTextSearchRequest> &other)
    : Object(),
    QList<TagTextSearchRequest>(other)
{
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagTextSearchRequestList::TagTextSearchRequestList(
    QList<TagTextSearchRequest> &&other)
    : Object(),
    QList<TagTextSearchRequest>(::std::move(other))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagTextSearchRequestList Json::TagTextSearchRequestList::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonArray jsonArr;
	if (!Helper::readRootArray(json, jsonArr)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagTextSearchRequestList();
	}

	return fromJsonVal(jsonArr, ok);
}

Json::TagTextSearchRequestList Json::TagTextSearchRequestList::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	TagTextSearchRequestList tsr;

	if (Q_UNLIKELY(!jsonVal.isArray())) {
		goto fail;
	}

	{
		const QJsonArray jsonArr = jsonVal.toArray();
		for (const QJsonValue &v : jsonArr) {
			bool iOk = false;
			tsr.append(TagTextSearchRequest::fromJsonVal(v, &iOk));
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return tsr;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagTextSearchRequestList();
}

bool Json::TagTextSearchRequestList::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonArray arr;

	for (const TagTextSearchRequest &tsr : *this) {
		QJsonValue v;
		if (Q_UNLIKELY(!tsr.toJsonVal(v))) {
			goto fail;
		}
		arr.append(v);
	}

	jsonVal = arr;
	return true;

fail:
	jsonVal = QJsonArray();
	return false;
}

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
size_t Json::qHash(const TagTextSearchRequest &key, size_t seed)
#else /* < Qt-6.0 */
uint Json::qHash(const TagTextSearchRequest &key, uint seed)
#endif /* >= Qt-6.0 */
{
	return ::qHash(key.text(), seed);
}

/*!
 * @brief PIMPL TagTextSearchResponsePrivate class.
 */
class Json::TagTextSearchResponsePrivate {
	//Q_DISABLE_COPY(TagTextSearchResponsePrivate)
public:
	TagTextSearchResponsePrivate(void)
	    : m_text(), m_type(DFLT_TYPE), m_ids()
	{ }

	TagTextSearchResponsePrivate &operator=(const TagTextSearchResponsePrivate &other) Q_DECL_NOTHROW
	{
		m_text = other.m_text;
		m_type = other.m_type;
		m_ids = other.m_ids;

		return *this;
	}

	bool operator==(const TagTextSearchResponsePrivate &other) const
	{
		return (m_text == other.m_text) &&
		    (m_type == other.m_type) &&
		    (m_ids == other.m_ids);
	}

	QString m_text;
	enum TagTextSearchRequest::SearchType m_type;
	Int64StringList m_ids;
};

Json::TagTextSearchResponse::TagTextSearchResponse(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::TagTextSearchResponse::TagTextSearchResponse(const TagTextSearchResponse &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) TagTextSearchResponsePrivate) : Q_NULLPTR)
{
	Q_D(TagTextSearchResponse);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagTextSearchResponse::TagTextSearchResponse(TagTextSearchResponse &&other) Q_DECL_NOEXCEPT
    : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagTextSearchResponse::~TagTextSearchResponse(void)
{
}

Json::TagTextSearchResponse::TagTextSearchResponse(const QString &text,
    enum TagTextSearchRequest::SearchType type, const Int64StringList &ids)
    : Object(),
    d_ptr(new (::std::nothrow) TagTextSearchResponsePrivate)
{
	Q_D(TagTextSearchResponse);
	d->m_text = text;
	d->m_type = type;
	d->m_ids = ids;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagTextSearchResponse::TagTextSearchResponse(QString &&text,
    enum TagTextSearchRequest::SearchType type, Int64StringList &&ids)
    : Object(),
    d_ptr(new (::std::nothrow) TagTextSearchResponsePrivate)
{
	Q_D(TagTextSearchResponse);
	d->m_text = ::std::move(text);
	d->m_type = type;
	d->m_ids = ::std::move(ids);
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures TagTextSearchResponsePrivate presence.
 *
 * @note Returns if TagTextSearchResponsePrivate could not be allocated.
 */
#define ensureTagTextSearchResponsePrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			TagTextSearchResponsePrivate *p = new (::std::nothrow) TagTextSearchResponsePrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::TagTextSearchResponse &Json::TagTextSearchResponse::operator=(const TagTextSearchResponse &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureTagTextSearchResponsePrivate(*this);
	Q_D(TagTextSearchResponse);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagTextSearchResponse &Json::TagTextSearchResponse::operator=(TagTextSearchResponse &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::TagTextSearchResponse::operator==(const TagTextSearchResponse &other) const
{
	Q_D(const TagTextSearchResponse);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::TagTextSearchResponse::operator!=(const TagTextSearchResponse &other) const
{
	return !operator==(other);
}

bool Json::TagTextSearchResponse::isNull(void) const
{
	Q_D(const TagTextSearchResponse);
	return d == Q_NULLPTR;
}

bool Json::TagTextSearchResponse::isValid(void) const
{
	return (!isNull()) && (!text().isEmpty());
}

const QString &Json::TagTextSearchResponse::text(void) const
{
	Q_D(const TagTextSearchResponse);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_text;
}

void Json::TagTextSearchResponse::setText(const QString &t)
{
	ensureTagTextSearchResponsePrivate();
	Q_D(TagTextSearchResponse);
	d->m_text = t;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Json::TagTextSearchResponse::setText(QString &&t)
{
	ensureTagTextSearchResponsePrivate();
	Q_D(TagTextSearchResponse);
	d->m_text = ::std::move(t);
}
#endif /* Q_COMPILER_RVALUE_REFS */

enum Json::TagTextSearchRequest::SearchType Json::TagTextSearchResponse::type(void) const
{
	Q_D(const TagTextSearchResponse);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return DFLT_TYPE;
	}

	return d->m_type;
}

void Json::TagTextSearchResponse::setType(enum TagTextSearchRequest::SearchType t)
{
	ensureTagTextSearchResponsePrivate();
	Q_D(TagTextSearchResponse);
	d->m_type = t;
}

const Json::Int64StringList &Json::TagTextSearchResponse::ids(void) const
{
	Q_D(const TagTextSearchResponse);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullInt64StringList;
	}

	return d->m_ids;
}

void Json::TagTextSearchResponse::setIds(const Int64StringList &ids)
{
	ensureTagTextSearchResponsePrivate();
	Q_D(TagTextSearchResponse);
	d->m_ids = ids;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Json::TagTextSearchResponse::setIds(Int64StringList &&ids)
{
	ensureTagTextSearchResponsePrivate();
	Q_D(TagTextSearchResponse);
	d->m_ids = ::std::move(ids);
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagTextSearchResponse Json::TagTextSearchResponse::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagTextSearchResponse();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString keyIds("ids");

Json::TagTextSearchResponse Json::TagTextSearchResponse::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	TagTextSearchResponse stsr;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();

		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyText,
			        valStr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			stsr.setText(::std::move(valStr));
		}
		{
			bool iOk = false;
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyType,
			        valStr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			stsr.setType(stringToSearchType(valStr, &iOk));
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
		}
		{
			QJsonArray jsonArr;
			if (Helper::readArray(jsonObj, keyIds, jsonArr,
			        Helper::ACCEPT_VALID)) {
				bool iOk = false;
				stsr.setIds(Int64StringList::fromJsonVal(jsonArr, &iOk));
				if (Q_UNLIKELY(!iOk)) {
					goto fail;
				}
			} else {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return stsr;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagTextSearchResponse();
}

bool Json::TagTextSearchResponse::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;
	bool iOk = false;

	jsonObj.insert(keyText, (!text().isEmpty()) ? text() : QJsonValue());
	jsonObj.insert(keyType, searchTypeToString(type(), &iOk));
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	{
		QJsonValue jsonVal;
		if (ids().toJsonVal(jsonVal)) {
			jsonObj.insert(keyIds, jsonVal);
		} else {
			goto fail;
		}
	}

	jsonVal = jsonObj;
	return true;

fail:
	jsonVal = QJsonValue();
	return false;
}

void Json::swap(TagTextSearchResponse &first, TagTextSearchResponse &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
