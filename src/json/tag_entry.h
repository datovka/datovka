/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QList>
#include <QMetaType>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

class QJsonValue; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Json {

	class TagEntryPrivate;
	/*!
	 * @brief Encapsulates database tag entry.
	 */
	class TagEntry : public Object {
		Q_DECLARE_PRIVATE(TagEntry)
	public:
		TagEntry(void);
		TagEntry(const TagEntry &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagEntry(TagEntry &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~TagEntry(void);

		TagEntry(qint64 id, const QString &name,
		    const QString &colour, const QString &description);
#ifdef Q_COMPILER_RVALUE_REFS
		TagEntry(qint64 id, QString &&name, QString &&colour,
		    QString &&description);
#endif /* Q_COMPILER_RVALUE_REFS */

		TagEntry &operator=(const TagEntry &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		TagEntry &operator=(TagEntry &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const TagEntry &other) const;
		bool operator!=(const TagEntry &other) const;

		friend void swap(TagEntry &first, TagEntry &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check for validity.
		 *
		 * @note Invalid tag has negative id, empty name or bogus colour.
		 *
		 * @return True if tag contains valid data.
		 */
		bool isValid(void) const;

		/* identifier */
		qint64 id(void) const;
		void setId(qint64 i);
		/* tag name */
		const QString &name(void) const;
		void setName(const QString &n);
#ifdef Q_COMPILER_RVALUE_REFS
		void setName(QString &&n);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* tag colour */
		const QString &colour(void) const;
		void setColour(const QString &c);
#ifdef Q_COMPILER_RVALUE_REFS
		void setColour(QString &&c);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* tag description */
		const QString &description(void) const;
		void setDescription(const QString &de);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDescription(QString &&de);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates an instance from supplied JSON document.
		 *
		 * @param[in]  json JSON data.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		TagEntry fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Creates an instance from supplied JSON data.
		 *
		 * @param[in]  jsonObj JSON data.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		TagEntry fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Writes content of JSON object.
		 *
		 * @param[out] jsonVal JSON value to write to.
		 * @return True on success.
		 */
		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<TagEntryPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<TagEntryPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(TagEntry &first, TagEntry &second) Q_DECL_NOTHROW;

	/*!
	 * @brief List of tag entries.
	 */
	class TagEntryList : public Object, public QList<TagEntry> {
	public:
		/* Expose list constructors. */
		using QList<TagEntry>::QList;

		/* Some older compilers complain about missing constructor. */
		TagEntryList(void);

		TagEntryList(const QList<TagEntry> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagEntryList(QList<TagEntry> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		TagEntryList fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagEntryList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	/*!
	 * @brief Return a hash value for the supplied tag entry.
	 *
	 * @param[in] key Tag entry to compute hash from.
	 * @param[in] seed Is used to initialise the hash if specified.
	 * @return Hashed tag entry.
	 */
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
	size_t qHash(const TagEntry &key, size_t seed = 0);
#else /* < Qt-6.0 */
	uint qHash(const TagEntry &key, uint seed = 0);
#endif /* >= Qt-6.0 */

}

Q_DECLARE_METATYPE(Json::TagEntry)
Q_DECLARE_METATYPE(Json::TagEntryList)
