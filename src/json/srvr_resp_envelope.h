/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

class QJsonValue; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Json {

	class SrvrResponseEnvelopePrivate;
	/*!
	 * @brief Server response envelope.
	 */
	class SrvrResponseEnvelope : public Object {
		Q_DECLARE_PRIVATE(SrvrResponseEnvelope)

	public:
		SrvrResponseEnvelope(void);
		SrvrResponseEnvelope(const SrvrResponseEnvelope &other);
#ifdef Q_COMPILER_RVALUE_REFS
		SrvrResponseEnvelope(SrvrResponseEnvelope &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~SrvrResponseEnvelope(void);

		SrvrResponseEnvelope &operator=(const SrvrResponseEnvelope &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		SrvrResponseEnvelope &operator=(SrvrResponseEnvelope &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const SrvrResponseEnvelope &other) const;
		bool operator!=(const SrvrResponseEnvelope &other) const;

		friend void swap(SrvrResponseEnvelope &first, SrvrResponseEnvelope &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/* value name */
		const QString &jsonValueName(void) const;
		void setJsonValueName(const QString &n);
#ifdef COMPILER_RVALUE_REFS
		void setJsonValueName(QString &&n);
#endif /* COMPILER_RVALUE_REFS */
		/* value */
		const QJsonValue &jsonValue(void) const;
		void setJsonValue(const QJsonValue &v);
#ifdef COMPILER_RVALUE_REFS
		void setJsonValue(QJsonValue &&v);
#endif /* COMPILER_RVALUE_REFS */
		/* status code */
		const QString &statusCode(void) const;
		void setStatusCode(const QString &c);
#ifdef COMPILER_RVALUE_REFS
		void setStatusCode(QString &&c);
#endif /* COMPILER_RVALUE_REFS */
		/* status message */
		const QString &statusMessage(void) const;
		void setStatusMessage(const QString &m);
#ifdef COMPILER_RVALUE_REFS
		void setStatusMessage(QString &&m);
#endif /* COMPILER_RVALUE_REFS */

		/* Convenience method. */
		bool statusOk(void) const;

		/*!
		 * @brief Creates an instance from supplied JSON document.
		 *
		 * @param[in]  json JSON data.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		SrvrResponseEnvelope fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Creates an instance from supplied JSON data.
		 *
		 * @param[in]  jsonObj JSON data.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		SrvrResponseEnvelope fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Writes content of JSON object.
		 *
		 * @param[out] jsonVal JSON value to write to.
		 * @return True on success.
		 */
		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<SrvrResponseEnvelopePrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<SrvrResponseEnvelopePrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(SrvrResponseEnvelope &first, SrvrResponseEnvelope &second) Q_DECL_NOTHROW;

}
