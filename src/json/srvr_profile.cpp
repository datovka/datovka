/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QJsonArray>
#include <QJsonObject>
#include <QString>
#include <utility> /* ::std::swap */

#include "src/datovka_shared/json/helper.h"
#include "src/json/srvr_profile.h"

static const int nullInt = -1;
static const QString nullString;

class Json::SrvrProfilePrivate {
public:
	SrvrProfilePrivate(void)
	    : m_id(nullInt), m_name(), m_description()
	{ }

	SrvrProfilePrivate &operator=(const SrvrProfilePrivate &other) Q_DECL_NOTHROW
	{
		m_id = other.m_id;
		m_name = other.m_name;
		m_description = other.m_description;

		return *this;
	}

	bool operator==(const SrvrProfilePrivate &other) const
	{
		return (m_id == other.m_id) &&
		    (m_name == other.m_name) &&
		    (m_description == other.m_description);
	}

	qint64 m_id;
	QString m_name;
	QString m_description;
};

Json::SrvrProfile::SrvrProfile(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::SrvrProfile::SrvrProfile(const SrvrProfile &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) SrvrProfilePrivate) : Q_NULLPTR)
{
	Q_D(SrvrProfile);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::SrvrProfile::SrvrProfile(SrvrProfile &&other) Q_DECL_NOEXCEPT
    : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::SrvrProfile::~SrvrProfile(void)
{
}

/*!
 * @brief Ensures private server response envelope presence.
 *
 * @note Returns if private server response envelope could not be allocated.
 */
#define ensureSrvrProfilePrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			SrvrProfilePrivate *p = new (::std::nothrow) SrvrProfilePrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::SrvrProfile &Json::SrvrProfile::operator=(const SrvrProfile &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureSrvrProfilePrivate(*this);
	Q_D(SrvrProfile);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::SrvrProfile &Json::SrvrProfile::operator=(SrvrProfile &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::SrvrProfile::operator==(const SrvrProfile &other) const
{
	Q_D(const SrvrProfile);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::SrvrProfile::operator!=(const SrvrProfile &other) const
{
	return !operator==(other);
}

bool Json::SrvrProfile::isNull(void) const
{
	Q_D(const SrvrProfile);
	return d == Q_NULLPTR;
}

qint64 Json::SrvrProfile::id(void) const
{
	Q_D(const SrvrProfile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullInt;
	}

	return d->m_id;
}

void Json::SrvrProfile::setId(qint64 i)
{
	ensureSrvrProfilePrivate();
	Q_D(SrvrProfile);
	d->m_id = i;
}

const QString &Json::SrvrProfile::name(void) const
{
	Q_D(const SrvrProfile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_name;
}

void Json::SrvrProfile::setName(const QString &n)
{
	ensureSrvrProfilePrivate();
	Q_D(SrvrProfile);
	d->m_name = n;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Json::SrvrProfile::setName(QString &&n)
{
	ensureSrvrProfilePrivate();
	Q_D(SrvrProfile);
	d->m_name = ::std::move(n);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Json::SrvrProfile::description(void) const
{
	Q_D(const SrvrProfile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_description;
}

void Json::SrvrProfile::setDescription(const QString &de)
{
	ensureSrvrProfilePrivate();
	Q_D(SrvrProfile);
	d->m_description = de;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Json::SrvrProfile::setDescription(QString &&de)
{
	ensureSrvrProfilePrivate();
	Q_D(SrvrProfile);
	d->m_description = ::std::move(de);
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::SrvrProfile Json::SrvrProfile::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return SrvrProfile();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString keyId("id");
static const QString keyName("name");
static const QString keyDescription("description");

Json::SrvrProfile Json::SrvrProfile::fromJsonVal(const QJsonValue &jsonVal,
    bool *ok)
{
	SrvrProfile sp;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			qint64 valInt = 0;
			if (Q_UNLIKELY(!Helper::readQint64String(jsonObj, keyId,
			        valInt, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			sp.setId(valInt);
		}
		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyName,
			        valStr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			sp.setName(::std::move(valStr));
		}
		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyDescription,
			        valStr, Helper::ACCEPT_NULL))) {
				goto fail;
			}
			sp.setDescription(::std::move(valStr));
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return sp;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return SrvrProfile();
}

bool Json::SrvrProfile::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyId, (id() >= 0) ? QString::number(id()) : QJsonValue());
	jsonObj.insert(keyName, (!name().isEmpty()) ? name() : QJsonValue());
	jsonObj.insert(keyDescription, (!description().isEmpty()) ? description() : QJsonValue());

	jsonVal = jsonObj;
	return true;
}

void Json::swap(SrvrProfile &first, SrvrProfile &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

Json::SrvrProfileList::SrvrProfileList(void)
    : Object(),
    QList<SrvrProfile>()
{
}

Json::SrvrProfileList::SrvrProfileList(const QList<SrvrProfile> &other)
    : Object(),
    QList<SrvrProfile>(other)
{
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::SrvrProfileList::SrvrProfileList(QList<SrvrProfile> &&other)
    : Object(),
    QList<SrvrProfile>(::std::move(other))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::SrvrProfileList Json::SrvrProfileList::fromJson(const QByteArray &json,
    bool *ok)
{
	QJsonArray jsonArr;
	if (!Helper::readRootArray(json, jsonArr)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return SrvrProfileList();
	}

	return fromJsonVal(jsonArr, ok);
}

Json::SrvrProfileList Json::SrvrProfileList::fromJsonVal(const QJsonValue &jsonVal,
    bool *ok)
{
	SrvrProfileList pl;

	if (Q_UNLIKELY(!jsonVal.isArray())) {
		goto fail;
	}

	{
		const QJsonArray jsonArr = jsonVal.toArray();
		for (const QJsonValue &v : jsonArr) {
			bool iOk = false;
			pl.append(SrvrProfile::fromJsonVal(v, &iOk));
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return pl;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return SrvrProfileList();
}

bool Json::SrvrProfileList::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonArray arr;

	for (const SrvrProfile &p : *this) {
		QJsonValue v;
		if (Q_UNLIKELY(!p.toJsonVal(v))) {
			goto fail;
		}
		arr.append(v);
	}

	jsonVal = arr;
	return true;

fail:
	jsonVal = QJsonArray();
	return false;
}
