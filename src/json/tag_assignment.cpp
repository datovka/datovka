/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QJsonObject>
#include <QString>
#include <utility> /* ::std::swap */

#include "src/datovka_shared/json/helper.h"
#include "src/json/tag_assignment.h"
#include "src/json/tag_entry.h"
#include "src/json/tag_message_id.h"

#define DLFT_TEST_ENV Isds::Type::BOOL_NULL
#define nullInt -1
static const Json::TagMsgIdList nullTagMsgIds;
static const Json::TagEntryList nullEntries;

/*!
 * @brief PIMPL Json::TagAssignmentCount class.
 */
class Json::TagAssignmentCountPrivate {
	//Q_DISABLE_COPY(TagAssignmentCountPrivate)
public:
	TagAssignmentCountPrivate(void)
	    : m_tagId(nullInt), m_assignmentCount(nullInt)
	{ }

	TagAssignmentCountPrivate &operator=(const TagAssignmentCountPrivate &other) Q_DECL_NOTHROW
	{
		m_tagId = other.m_tagId;
		m_assignmentCount = other.m_assignmentCount;

		return *this;
	}

	bool operator==(const TagAssignmentCountPrivate &other) const
	{
		return (m_tagId == other.m_tagId) &&
		    (m_assignmentCount == other.m_assignmentCount);
	}

	qint64 m_tagId; /*!< Tag identifier. */
	qint64 m_assignmentCount; /*!< Tag assignment count. */
};

Json::TagAssignmentCount::TagAssignmentCount(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::TagAssignmentCount::TagAssignmentCount(const TagAssignmentCount &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) TagAssignmentCountPrivate) : Q_NULLPTR)
{
	Q_D(TagAssignmentCount);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagAssignmentCount::TagAssignmentCount(TagAssignmentCount &&other) Q_DECL_NOEXCEPT
   : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagAssignmentCount::~TagAssignmentCount(void)
{
}

Json::TagAssignmentCount::TagAssignmentCount(qint64 tagId, qint64 assignmentCount)
    : Object(),
    d_ptr(new (::std::nothrow) TagAssignmentCountPrivate)
{
	Q_D(TagAssignmentCount);
	d->m_tagId = tagId;
	d->m_assignmentCount = assignmentCount;
}

/*!
 * @brief Ensures TagAssignmentCountPrivate presence.
 *
 * @note Returns if TagAssignmentCountPrivate could not be allocated.
 */
#define ensureTagAssignmentCountPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			TagAssignmentCountPrivate *p = new (::std::nothrow) TagAssignmentCountPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::TagAssignmentCount &Json::TagAssignmentCount::operator=(const TagAssignmentCount &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureTagAssignmentCountPrivate(*this);
	Q_D(TagAssignmentCount);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagAssignmentCount &Json::TagAssignmentCount::operator=(TagAssignmentCount &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::TagAssignmentCount::operator==(const TagAssignmentCount &other) const
{
	Q_D(const TagAssignmentCount);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::TagAssignmentCount::operator!=(const TagAssignmentCount &other) const
{
	return !operator==(other);
}

bool Json::TagAssignmentCount::isNull(void) const
{
	Q_D(const TagAssignmentCount);
	return d == Q_NULLPTR;
}

bool Json::TagAssignmentCount::isValid(void) const
{
	return !isNull() && (tagId() >= 0) && (assignmentCount() >= 0);
}

qint64 Json::TagAssignmentCount::tagId(void) const
{
	Q_D(const TagAssignmentCount);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullInt;
	}

	return d->m_tagId;
}

void Json::TagAssignmentCount::setTagId(qint64 i)
{
	ensureTagAssignmentCountPrivate();
	Q_D(TagAssignmentCount);
	d->m_tagId = i;
}

qint64 Json::TagAssignmentCount::assignmentCount(void) const
{
	Q_D(const TagAssignmentCount);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullInt;
	}

	return d->m_assignmentCount;
}

void Json::TagAssignmentCount::setAssignmentCount(qint64 c)
{
	ensureTagAssignmentCountPrivate();
	Q_D(TagAssignmentCount);
	d->m_assignmentCount = c;
}

Json::TagAssignmentCount Json::TagAssignmentCount::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagAssignmentCount();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString keyTagId("tagId");
static const QString keyAssignmentCount("assignmentCount");

Json::TagAssignmentCount Json::TagAssignmentCount::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	TagAssignmentCount tac;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			qint64 valInt = 0;
			if (Q_UNLIKELY(!Helper::readQint64String(jsonObj,
			        keyTagId, valInt, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			tac.setTagId(valInt);
		}
		{
			qint64 valInt = 0;
			if (Q_UNLIKELY(!Helper::readQint64String(jsonObj,
			        keyAssignmentCount, valInt, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			tac.setAssignmentCount(valInt);
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return tac;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagAssignmentCount();
}

bool Json::TagAssignmentCount::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyTagId, (tagId() >= 0) ? QString::number(tagId()) : QJsonValue());
	jsonObj.insert(keyAssignmentCount, (assignmentCount() >= 0) ? QString::number(assignmentCount()) : QJsonValue());

	jsonVal = jsonObj;
	return true;
}

void Json::swap(TagAssignmentCount &first, TagAssignmentCount &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

/*!
 * @brief PIMPL Json::TagIdToMsgIdList class.
 */
class Json::TagIdToMsgIdListPrivate {
	//Q_DISABLE_COPY(TagIdToMsgIdListPrivate)
public:
	TagIdToMsgIdListPrivate(void)
	    : m_tagId(nullInt), m_tagMsgIds()
	{ }

	TagIdToMsgIdListPrivate &operator=(const TagIdToMsgIdListPrivate &other) Q_DECL_NOTHROW
	{
		m_tagId = other.m_tagId;
		m_tagMsgIds = other.m_tagMsgIds;

		return *this;
	}

	bool operator==(const TagIdToMsgIdListPrivate &other) const
	{
		return (m_tagId == other.m_tagId) &&
		    (m_tagMsgIds == other.m_tagMsgIds);
	}

	qint64 m_tagId; /*!< Tag identifier. */
	TagMsgIdList m_tagMsgIds; /*!< List of message identifiers. */
};

Json::TagIdToMsgIdList::TagIdToMsgIdList(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::TagIdToMsgIdList::TagIdToMsgIdList(const TagIdToMsgIdList &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) TagIdToMsgIdListPrivate) : Q_NULLPTR)
{
	Q_D(TagIdToMsgIdList);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagIdToMsgIdList::TagIdToMsgIdList(TagIdToMsgIdList &&other) Q_DECL_NOEXCEPT
    : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagIdToMsgIdList::~TagIdToMsgIdList(void)
{
}

Json::TagIdToMsgIdList::TagIdToMsgIdList(qint64 tagId,
    const TagMsgIdList &tagMsgIds)
    : Object(),
    d_ptr(new (::std::nothrow) TagIdToMsgIdListPrivate)
{
	Q_D(TagIdToMsgIdList);
	d->m_tagId = tagId;
	d->m_tagMsgIds = tagMsgIds;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagIdToMsgIdList::TagIdToMsgIdList(qint64 tagId, TagMsgIdList &&tagMsgIds)
    : Object(),
    d_ptr(new (::std::nothrow) TagIdToMsgIdListPrivate)
{
	Q_D(TagIdToMsgIdList);
	d->m_tagId = tagId;
	d->m_tagMsgIds = ::std::move(tagMsgIds);
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures TagIdToMsgIdListPrivate presence.
 *
 * @note Returns if TagIdToMsgIdListPrivate could not be allocated.
 */
#define ensureTagIdToMsgIdListPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			TagIdToMsgIdListPrivate *p = new (::std::nothrow) TagIdToMsgIdListPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::TagIdToMsgIdList &Json::TagIdToMsgIdList::operator=(const TagIdToMsgIdList &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureTagIdToMsgIdListPrivate(*this);
	Q_D(TagIdToMsgIdList);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagIdToMsgIdList &Json::TagIdToMsgIdList::operator=(TagIdToMsgIdList &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::TagIdToMsgIdList::operator==(const TagIdToMsgIdList &other) const
{
	Q_D(const TagIdToMsgIdList);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::TagIdToMsgIdList::operator!=(const TagIdToMsgIdList &other) const
{
	return !operator==(other);
}

bool Json::TagIdToMsgIdList::isNull(void) const
{
	Q_D(const TagIdToMsgIdList);
	return d == Q_NULLPTR;
}

bool Json::TagIdToMsgIdList::isValid(void) const
{
	return !isNull() && (tagId() >= 0);
}

qint64 Json::TagIdToMsgIdList::tagId(void) const
{
	Q_D(const TagIdToMsgIdList);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullInt;
	}

	return d->m_tagId;
}

void Json::TagIdToMsgIdList::setTagId(qint64 i)
{
	ensureTagIdToMsgIdListPrivate();
	Q_D(TagIdToMsgIdList);
	d->m_tagId = i;
}

const Json::TagMsgIdList &Json::TagIdToMsgIdList::tagMsgIds(void) const
{
	Q_D(const TagIdToMsgIdList);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullTagMsgIds;
	}

	return d->m_tagMsgIds;
}

void Json::TagIdToMsgIdList::setTagMsgIds(const TagMsgIdList &l)
{
	ensureTagIdToMsgIdListPrivate();
	Q_D(TagIdToMsgIdList);
	d->m_tagMsgIds = l;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Json::TagIdToMsgIdList::setTagMsgIds(TagMsgIdList &&l)
{
	ensureTagIdToMsgIdListPrivate();
	Q_D(TagIdToMsgIdList);
	d->m_tagMsgIds = ::std::move(l);
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagIdToMsgIdList Json::TagIdToMsgIdList::fromJson(const QByteArray &json,
    bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagIdToMsgIdList();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString keyMsgIdList("messageIdentifiers");

Json::TagIdToMsgIdList Json::TagIdToMsgIdList::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	TagIdToMsgIdList tmil;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			qint64 valInt = 0;
			if (Q_UNLIKELY(!Helper::readQint64String(jsonObj,
			        keyTagId, valInt, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			tmil.setTagId(valInt);
		}
		{
			QJsonArray arr;
			bool iOk = false;
			if (Q_UNLIKELY(!Helper::readArray(jsonObj,
			        keyMsgIdList, arr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			iOk = false;
			tmil.setTagMsgIds(TagMsgIdList::fromJsonVal(arr, &iOk));
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return tmil;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagIdToMsgIdList();
}

bool Json::TagIdToMsgIdList::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyTagId, (tagId() >= 0) ? QString::number(tagId()) : QJsonValue());
	{
		QJsonValue jsonVal;
		if (Q_UNLIKELY(!tagMsgIds().toJsonVal(jsonVal))) {
			return false;
		}
		jsonObj.insert(keyMsgIdList, jsonVal);
	}

	jsonVal = jsonObj;
	return true;
}

void Json::swap(TagIdToMsgIdList &first, TagIdToMsgIdList &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

/*!
 * @brief PIMPL Json::TagAssignment class.
 */
class Json::TagAssignmentPrivate {
	//Q_DISABLE_COPY(TagAssignmentPrivate)
public:
	TagAssignmentPrivate(void)
	    : m_testEnv(DLFT_TEST_ENV), m_dmId(nullInt), m_entries()
	{ }

	TagAssignmentPrivate &operator=(const TagAssignmentPrivate &other) Q_DECL_NOTHROW
	{
		m_testEnv = other.m_testEnv;
		m_dmId = other.m_dmId;
		m_entries = other.m_entries;

		return *this;
	}

	bool operator==(const TagAssignmentPrivate &other) const
	{
		return (m_testEnv == other.m_testEnv) &&
		    (m_dmId == other.m_dmId) &&
		    (m_entries == other.m_entries);
	}

	enum Isds::Type::NilBool m_testEnv; /*!< Test environment. */
	qint64 m_dmId; /*!< Message identifier. */
	TagEntryList m_entries;
};

Json::TagAssignment::TagAssignment(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::TagAssignment::TagAssignment(const TagAssignment &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) TagAssignmentPrivate) : Q_NULLPTR)
{
	Q_D(TagAssignment);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagAssignment::TagAssignment(TagAssignment &&other) Q_DECL_NOEXCEPT
    : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagAssignment::~TagAssignment(void)
{
}

Json::TagAssignment::TagAssignment(enum Isds::Type::NilBool testEnv,
    qint64 dmId, const TagEntryList &entries)
    : Object(),
    d_ptr(new (::std::nothrow) TagAssignmentPrivate)
{
	Q_D(TagAssignment);
	d->m_testEnv = testEnv;
	d->m_dmId = dmId;
	d->m_entries = entries;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagAssignment::TagAssignment(enum Isds::Type::NilBool testEnv,
    qint64 dmId, TagEntryList &&entries)
    : Object(),
    d_ptr(new (::std::nothrow) TagAssignmentPrivate)
{
	Q_D(TagAssignment);
	d->m_testEnv = testEnv;
	d->m_dmId = dmId;
	d->m_entries = ::std::move(entries);
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures TagAssignmentPrivate presence.
 *
 * @note Returns if TagAssignmentPrivate could not be allocated.
 */
#define ensureTagAssignmentPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			TagAssignmentPrivate *p = new (::std::nothrow) TagAssignmentPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::TagAssignment &Json::TagAssignment::operator=(const TagAssignment &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureTagAssignmentPrivate(*this);
	Q_D(TagAssignment);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagAssignment &Json::TagAssignment::operator=(TagAssignment &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::TagAssignment::operator==(const TagAssignment &other) const
{
	Q_D(const TagAssignment);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::TagAssignment::operator!=(const TagAssignment &other) const
{
	return !operator==(other);
}

bool Json::TagAssignment::isNull(void) const
{
	Q_D(const TagAssignment);
	return d == Q_NULLPTR;
}

bool Json::TagAssignment::isValid(void) const
{
	return !isNull() && (testEnv() != Isds::Type::BOOL_NULL) && (dmId() >= 0);
}

enum Isds::Type::NilBool Json::TagAssignment::testEnv(void) const
{
	Q_D(const TagAssignment);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return DLFT_TEST_ENV;
	}

	return d->m_testEnv;
}

void Json::TagAssignment::setTestEnv(enum Isds::Type::NilBool t)
{
	ensureTagAssignmentPrivate();
	Q_D(TagAssignment);
	d->m_testEnv = t;
}

qint64 Json::TagAssignment::dmId(void) const
{
	Q_D(const TagAssignment);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullInt;
	}

	return d->m_dmId;
}

void Json::TagAssignment::setDmId(qint64 di)
{
	ensureTagAssignmentPrivate();
	Q_D(TagAssignment);
	d->m_dmId = di;
}

const Json::TagEntryList &Json::TagAssignment::entries(void) const
{
	Q_D(const TagAssignment);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullEntries;
	}

	return d->m_entries;
}

void Json::TagAssignment::setEntries(const TagEntryList &l)
{
	ensureTagAssignmentPrivate();
	Q_D(TagAssignment);
	d->m_entries = l;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Json::TagAssignment::setEntries(TagEntryList &&l)
{
	ensureTagAssignmentPrivate();
	Q_D(TagAssignment);
	d->m_entries = ::std::move(l);
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagAssignment Json::TagAssignment::fromJson(const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagAssignment();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString keyTestEnv("testEnv");
static const QString keyMessageId("messageId");
static const QString keyTagEntries("tagEntries");

Json::TagAssignment Json::TagAssignment::fromJsonVal(const QJsonValue &jsonVal, bool *ok)
{
	TagAssignment ta;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			enum Isds::Type::NilBool valNilBool = Isds::Type::BOOL_NULL;
			if (Q_UNLIKELY(!Helper::readNilBool(jsonObj,
			        keyTestEnv, valNilBool, Helper::ACCEPT_NULL))) {
				goto fail;
			}
			ta.setTestEnv(valNilBool);
		}
		{
			qint64 valInt = 0;
			if (Q_UNLIKELY(!Helper::readQint64String(jsonObj,
			        keyMessageId, valInt, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			ta.setDmId(valInt);
		}
		{
			QJsonArray arr;
			bool iOk = false;
			if (Q_UNLIKELY(!Helper::readArray(jsonObj,
			        keyTagEntries, arr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			iOk = false;
			ta.setEntries(TagEntryList::fromJsonVal(arr, &iOk));
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return ta;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagAssignment();
}

bool Json::TagAssignment::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyTestEnv, (testEnv() != Isds::Type::BOOL_NULL) ?
	    QJsonValue(testEnv() == Isds::Type::BOOL_TRUE) : QJsonValue());
	jsonObj.insert(keyMessageId, (dmId() >= 0) ? QString::number(dmId()) : QJsonValue());
	{
		QJsonValue jsonVal;
		if (Q_UNLIKELY(!entries().toJsonVal(jsonVal))) {
			return false;
		}
		jsonObj.insert(keyTagEntries, jsonVal);
	}

	jsonVal = jsonObj;
	return true;
}

void Json::swap(TagAssignment &first, TagAssignment &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

Json::TagAssignmentList::TagAssignmentList(void)
    : Object(),
    QList<TagAssignment>()
{
}

Json::TagAssignmentList::TagAssignmentList(const QList<TagAssignment> &other)
    : Object(),
    QList<TagAssignment>(other)
{
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagAssignmentList::TagAssignmentList(QList<TagAssignment> &&other)
    : Object(),
    QList<TagAssignment>(::std::move(other))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagAssignmentList Json::TagAssignmentList::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonArray jsonArr;
	if (!Helper::readRootArray(json, jsonArr)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagAssignmentList();
	}

	return fromJsonVal(jsonArr, ok);
}

Json::TagAssignmentList Json::TagAssignmentList::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	TagAssignmentList mil;

	if (Q_UNLIKELY(!jsonVal.isArray())) {
		goto fail;
	}

	{
		const QJsonArray jsonArr = jsonVal.toArray();
		for (const QJsonValue &v : jsonArr) {
			bool iOk = false;
			mil.append(TagAssignment::fromJsonVal(v, &iOk));
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return mil;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagAssignmentList();
}

bool Json::TagAssignmentList::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonArray arr;

	for (const TagAssignment &mi : *this) {
		QJsonValue v;
		if (Q_UNLIKELY(!mi.toJsonVal(v))) {
			goto fail;
		}
		arr.append(v);
	}

	jsonVal = arr;
	return true;

fail:
	jsonVal = QJsonArray();
	return false;
}
