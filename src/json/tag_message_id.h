/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QList>
#include <QMetaType>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/isds/types.h"
#include "src/datovka_shared/json/object.h"

class QJsonValue; /* Forward declaration. */

namespace Json {

	class TagMsgIdPrivate;
	/*!
	 * @brief Holds message identifier.
	 */
	class TagMsgId : public Object {
		Q_DECLARE_PRIVATE(TagMsgId)
	public:
		TagMsgId(void);
		TagMsgId(const TagMsgId &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagMsgId(TagMsgId &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~TagMsgId(void);

		explicit TagMsgId(enum Isds::Type::NilBool testEnv, qint64 dmId);

		TagMsgId &operator=(const TagMsgId &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		TagMsgId &operator=(TagMsgId &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const TagMsgId &other) const;
		bool operator!=(const TagMsgId &other) const;

		friend void swap(TagMsgId &first, TagMsgId &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		bool isValid(void) const;

		/* test environment */
		enum Isds::Type::NilBool testEnv(void) const;
		void setTestEnv(enum Isds::Type::NilBool t);
		/* message identifier */
		qint64 dmId(void) const;
		void setDmId(qint64 di);

		static
		TagMsgId fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagMsgId fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<TagMsgIdPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<TagMsgIdPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(TagMsgId &first, TagMsgId &second) Q_DECL_NOTHROW;

	/*!
	 * @brief List of message identifiers.
	 */
	class TagMsgIdList : public Object, public QList<TagMsgId> {
	public:
		/* Expose list constructors. */
		using QList<TagMsgId>::QList;

		/* Some older compilers complain about missing constructor. */
		TagMsgIdList(void);

		TagMsgIdList(const QList<TagMsgId> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		TagMsgIdList(QList<TagMsgId> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		TagMsgIdList fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagMsgIdList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
	size_t qHash(const TagMsgId &key, size_t seed = 0);
#else /* < Qt-6.0 */
	uint qHash(const TagMsgId &key, uint seed = 0);
#endif /* >= Qt-6.0 */

}

Q_DECLARE_METATYPE(Json::TagMsgId)
Q_DECLARE_METATYPE(Json::TagMsgIdList)
