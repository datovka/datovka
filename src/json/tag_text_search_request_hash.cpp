/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QJsonArray>
#include <QJsonObject>

#include "src/datovka_shared/json/helper.h"
#include "src/json/tag_text_search_request_hash.h"

Json::TagTextSearchRequestToIdenfifierHash Json::TagTextSearchRequestToIdenfifierHash::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonArray jsonArr;
	if (!Helper::readRootArray(json, jsonArr)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagTextSearchRequestToIdenfifierHash();
	}

	return fromJsonVal(jsonArr, ok);
}

Json::TagTextSearchRequestToIdenfifierHash Json::TagTextSearchRequestToIdenfifierHash::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	TagTextSearchRequestToIdenfifierHash h;

	if (Q_UNLIKELY(!jsonVal.isArray())) {
		goto fail;
	}

	{
		const QJsonArray jsonArr = jsonVal.toArray();
		for (const QJsonValue &v : jsonArr) {
			bool iOk = false;
			TagTextSearchResponse tsr = TagTextSearchResponse::fromJsonVal(v, &iOk);
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
			h.insert(TagTextSearchRequest(tsr.text(), tsr.type()), tsr.ids());
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return h;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagTextSearchRequestToIdenfifierHash();
}

bool Json::TagTextSearchRequestToIdenfifierHash::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonArray arr;

	for (const TagTextSearchRequest &key : keys()) {
		QJsonValue v;
		if (Q_UNLIKELY(!TagTextSearchResponse(key.text(), key.type(), operator[](key)).toJsonVal(v))) {
			goto fail;
		}
		arr.append(v);
	}

	jsonVal = arr;
	return true;

fail:
	jsonVal = QJsonArray();
	return false;
}
