/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QHash>
#include <QMetaType>

#include "src/datovka_shared/json/basic.h"
#include "src/datovka_shared/json/object.h"
#include "src/json/tag_entry.h"
#include "src/json/tag_message_id.h"

namespace Json {

	/*!
	 * @brief Hash of tag assignment counts. Maps [tagId] -> assignmentCount.
	 */
	class TagIdToAssignmentCountHash : public Object, public QHash<qint64, qint64> {
	public:
		static
		TagIdToAssignmentCountHash fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagIdToAssignmentCountHash fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	/*!
	 * @brief Hash of messages assigned to tags. Maps [tagId] -> TagMsgIdList.
	 */
	class TagIdToMsgIdListHash : public Object, public QHash<qint64, TagMsgIdList> {
	public:
		static
		TagIdToMsgIdListHash fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagIdToMsgIdListHash fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	/*!
	 * @brief Hash of tag assignments.
	 */
	class TagAssignmentHash : public Object, public QHash<TagMsgId, TagEntryList> {
	public:
		static
		TagAssignmentHash fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagAssignmentHash fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

	/*!
	 * @brief Hash of tag identifiers.
	 */
	class TagAssignmentCommand : public Object, public QHash<TagMsgId, Int64StringList> {
	public:
		static
		TagAssignmentCommand fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		TagAssignmentCommand fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

}

Q_DECLARE_METATYPE(Json::TagIdToAssignmentCountHash)
Q_DECLARE_METATYPE(Json::TagIdToMsgIdListHash)
Q_DECLARE_METATYPE(Json::TagAssignmentHash)
Q_DECLARE_METATYPE(Json::TagAssignmentCommand)
