/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QHash> /* ::qHash */
#include <QJsonObject>
#include <QString>
#include <utility> /* ::std::swap */

#include "src/datovka_shared/json/helper.h"
#include "src/json/tag_message_id.h"

#define DLFT_TEST_ENV Isds::Type::BOOL_NULL
#define nullInt -1

/*!
 * @brief PIMPL Json::TagMsgId class.
 */
class Json::TagMsgIdPrivate {
	//Q_DISABLE_COPY(MsgIdPrivate)
public:
	TagMsgIdPrivate(void)
	    : m_testEnv(DLFT_TEST_ENV), m_dmId(nullInt)
	{ }

	TagMsgIdPrivate &operator=(const TagMsgIdPrivate &other) Q_DECL_NOTHROW
	{
		m_testEnv = other.m_testEnv;
		m_dmId = other.m_dmId;

		return *this;
	}

	bool operator==(const TagMsgIdPrivate &other) const
	{
		return (m_testEnv == other.m_testEnv) &&
		    (m_dmId == other.m_dmId);
	}

	enum Isds::Type::NilBool m_testEnv; /*!< Test environment. */
	qint64 m_dmId; /*!< Message identifier. */
};

Json::TagMsgId::TagMsgId(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::TagMsgId::TagMsgId(const TagMsgId &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) TagMsgIdPrivate) : Q_NULLPTR)
{
	Q_D(TagMsgId);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagMsgId::TagMsgId(TagMsgId &&other) Q_DECL_NOEXCEPT
    : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagMsgId::~TagMsgId(void)
{
}

Json::TagMsgId::TagMsgId(enum Isds::Type::NilBool testEnv, qint64 dmId)
    : Object(),
    d_ptr(new (::std::nothrow) TagMsgIdPrivate)
{
	Q_D(TagMsgId);
	d->m_testEnv = testEnv;
	d->m_dmId = dmId;
}

/*!
 * @brief Ensures TagMsgIdPrivate presence.
 *
 * @note Returns if TagMsgIdPrivate could not be allocated.
 */
#define ensureTagMsgIdPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			TagMsgIdPrivate *p = new (::std::nothrow) TagMsgIdPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::TagMsgId &Json::TagMsgId::operator=(const TagMsgId &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureTagMsgIdPrivate(*this);
	Q_D(TagMsgId);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagMsgId &Json::TagMsgId::operator=(TagMsgId &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::TagMsgId::operator==(const TagMsgId &other) const
{
	Q_D(const TagMsgId);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::TagMsgId::operator!=(const TagMsgId &other) const
{
	return !operator==(other);
}

bool Json::TagMsgId::isNull(void) const
{
	Q_D(const TagMsgId);
	return d == Q_NULLPTR;
}

bool Json::TagMsgId::isValid(void) const
{
	return !isNull() && (testEnv() != Isds::Type::BOOL_NULL) && (dmId() >= 0);
}

enum Isds::Type::NilBool Json::TagMsgId::testEnv(void) const
{
	Q_D(const TagMsgId);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return DLFT_TEST_ENV;
	}

	return d->m_testEnv;
}

void Json::TagMsgId::setTestEnv(enum Isds::Type::NilBool t)
{
	ensureTagMsgIdPrivate();
	Q_D(TagMsgId);
	d->m_testEnv = t;
}

qint64 Json::TagMsgId::dmId(void) const
{
	Q_D(const TagMsgId);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullInt;
	}

	return d->m_dmId;
}

void Json::TagMsgId::setDmId(qint64 di)
{
	ensureTagMsgIdPrivate();
	Q_D(TagMsgId);
	d->m_dmId = di;
}

Json::TagMsgId Json::TagMsgId::fromJson(const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagMsgId();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString keyTestEnv("testEnv");
static const QString keyMessageId("messageId");

Json::TagMsgId Json::TagMsgId::fromJsonVal(const QJsonValue &jsonVal, bool *ok)
{
	TagMsgId smi;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			enum Isds::Type::NilBool valNilBool = Isds::Type::BOOL_NULL;
			if (Q_UNLIKELY(!Helper::readNilBool(jsonObj,
			        keyTestEnv, valNilBool, Helper::ACCEPT_NULL))) {
				goto fail;
			}
			smi.setTestEnv(valNilBool);
		}
		{
			qint64 valInt = 0;
			if (Q_UNLIKELY(!Helper::readQint64String(jsonObj,
			        keyMessageId, valInt, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			smi.setDmId(valInt);
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return smi;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagMsgId();
}

bool Json::TagMsgId::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyTestEnv, (testEnv() != Isds::Type::BOOL_NULL) ?
	    QJsonValue(testEnv() == Isds::Type::BOOL_TRUE) : QJsonValue());
	jsonObj.insert(keyMessageId, (dmId() >= 0) ? QString::number(dmId()) : QJsonValue());

	jsonVal = jsonObj;
	return true;
}

void Json::swap(TagMsgId &first, TagMsgId &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

Json::TagMsgIdList::TagMsgIdList(void)
    : Object(),
    QList<TagMsgId>()
{
}

Json::TagMsgIdList::TagMsgIdList(const QList<TagMsgId> &other)
    : Object(),
    QList<TagMsgId>(other)
{
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::TagMsgIdList::TagMsgIdList(QList<TagMsgId> &&other)
    : Object(),
    QList<TagMsgId>(::std::move(other))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::TagMsgIdList Json::TagMsgIdList::fromJson(const QByteArray &json,
    bool *ok)
{
	QJsonArray jsonArr;
	if (!Helper::readRootArray(json, jsonArr)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return TagMsgIdList();
	}

	return fromJsonVal(jsonArr, ok);
}

Json::TagMsgIdList Json::TagMsgIdList::fromJsonVal(const QJsonValue &jsonVal,
    bool *ok)
{
	TagMsgIdList mil;

	if (Q_UNLIKELY(!jsonVal.isArray())) {
		goto fail;
	}

	{
		const QJsonArray jsonArr = jsonVal.toArray();
		for (const QJsonValue &v : jsonArr) {
			bool iOk = false;
			mil.append(TagMsgId::fromJsonVal(v, &iOk));
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return mil;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return TagMsgIdList();
}

bool Json::TagMsgIdList::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonArray arr;

	for (const TagMsgId &mi : *this) {
		QJsonValue v;
		if (Q_UNLIKELY(!mi.toJsonVal(v))) {
			goto fail;
		}
		arr.append(v);
	}

	jsonVal = arr;
	return true;

fail:
	jsonVal = QJsonArray();
	return false;
}

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
size_t Json::qHash(const TagMsgId &key, size_t seed)
#else /* < Qt-6.0 */
uint Json::qHash(const TagMsgId &key, uint seed)
#endif /* >= Qt-6.0 */
{
	return ::qHash(((int)key.testEnv()) ^ key.dmId(), seed);
}
