/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

namespace Json {

	class ActionEntryPrivate;
	/*!
	 * @brief Describes action entries.
	 */
	class ActionEntry : public Object {
		Q_DECLARE_PRIVATE(ActionEntry)
	public:
		enum Type {
			TYPE_UNKNOWN = 0, /*!< Invalid entry. */
			TYPE_ACTION, /*!< Action. */
			TYPE_SEPARATOR, /*!< Separator. */
			TYPE_WIDGET /*!< Widget. */
		};

		ActionEntry(void);
		ActionEntry(const ActionEntry &other);
#ifdef Q_COMPILER_RVALUE_REFS
		ActionEntry(ActionEntry &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~ActionEntry(void);

		ActionEntry(enum Type type, bool checked,
		    const QString &objectName);
#ifdef Q_COMPILER_RVALUE_REFS
		ActionEntry(enum Type type, bool checked,
		    QString &&objectName);
#endif /* Q_COMPILER_RVALUE_REFS */

		ActionEntry &operator=(const ActionEntry &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		ActionEntry &operator=(ActionEntry &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const ActionEntry &other) const;
		bool operator!=(const ActionEntry &other) const;

		friend void swap(ActionEntry &first, ActionEntry &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check for validity.
		 */
		bool isValid(void) const;

		enum Type type(void) const;
		void setType(Type t);
		bool checked(void) const;
		void setChecked(bool ch);
		const QString &objectName(void) const;
		void setObjectName(const QString &on);
#ifdef Q_COMPILER_RVALUE_REFS
		void setObjectName(QString &&on);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates an instance from supplied JSON document.
		 *
		 * @param[in]  json JSON data.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		ActionEntry fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Creates an instance from supplied JSON data.
		 *
		 * @param[in]  jsonObj JSON data.
		 * @param[out] ok Set to true on success.
		 * @return Invalid value on error a valid structure else.
		 */
		static
		ActionEntry fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Writes content of JSON object.
		 *
		 * @param[out] jsonVal JSON value to write to.
		 * @return True on success.
		 */
		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<ActionEntryPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<ActionEntryPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(ActionEntry &first, ActionEntry &second) Q_DECL_NOTHROW;

	/*!
	 * @brief List of action entries.
	 */
	class ActionEntryList : public Object, public QList<ActionEntry> {
	public:
		/* Expose list constructors. */
		using QList<ActionEntry>::QList;

		/* Some older compilers complain about missing constructor. */
		ActionEntryList(void);

		ActionEntryList(const QList<ActionEntry> &other);
#ifdef Q_COMPILER_RVALUE_REFS
		ActionEntryList(QList<ActionEntry> &&other);
#endif /* Q_COMPILER_RVALUE_REFS */

		static
		ActionEntryList fromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		static
		ActionEntryList fromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;
	};

}
