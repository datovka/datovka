/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QJsonObject>

#include "src/datovka_shared/compat/compiler.h" /* macroStdMove */
#include "src/datovka_shared/json/helper.h"
#include "src/json/action_entry.h"

#define INVALID_ACTION_TYPE Json::ActionEntry::TYPE_UNKNOWN
#define DFLT_CHECKED false
static const QString nullString;

/*!
 * @brief PIMPL ActionEntry class.
 */
class Json::ActionEntryPrivate {
	//Q_DISABLE_COPY(ActionEntryPrivate)
public:
	ActionEntryPrivate(void)
	    : m_type(INVALID_ACTION_TYPE), m_checked(DFLT_CHECKED),
	    m_objectName()
	{ }

	ActionEntryPrivate &operator=(const ActionEntryPrivate &other) Q_DECL_NOTHROW
	{
		m_type = other.m_type;
		m_checked = other.m_checked;
		m_objectName = other.m_objectName;

		return *this;
	}

	bool operator==(const ActionEntryPrivate &other) const
	{
		return (m_type == other.m_type) &&
		    (m_checked == other.m_checked) &&
		    (m_objectName == other.m_objectName);
	}

	enum Json::ActionEntry::Type m_type;
	bool m_checked;
	QString m_objectName;
};

Json::ActionEntry::ActionEntry(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::ActionEntry::ActionEntry(const ActionEntry &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) ActionEntryPrivate) : Q_NULLPTR)
{
	Q_D(ActionEntry);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::ActionEntry::ActionEntry(ActionEntry &&other) Q_DECL_NOEXCEPT
    : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::ActionEntry::~ActionEntry(void)
{
}

Json::ActionEntry::ActionEntry(enum Type type, bool checked,
    const QString &objectName)
    : Object(),
    d_ptr(new (::std::nothrow) ActionEntryPrivate)
{
	Q_D(ActionEntry);
	d->m_type = type;
	d->m_checked = checked;
	d->m_objectName = objectName;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::ActionEntry::ActionEntry(enum Type type, bool checked,
    QString &&objectName)
    : Object(),
    d_ptr(new (::std::nothrow) ActionEntryPrivate)
{
	Q_D(ActionEntry);
	d->m_type = type;
	d->m_checked = checked;
	d->m_objectName = ::std::move(objectName);
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures ActionEntryPrivate presence.
 *
 * @note Returns if ActionEntryPrivate could not be allocated.
 */
#define ensureActionEntryPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			ActionEntryPrivate *p = new (::std::nothrow) ActionEntryPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::ActionEntry &Json::ActionEntry::operator=(const ActionEntry &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureActionEntryPrivate(*this);
	Q_D(ActionEntry);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::ActionEntry &Json::ActionEntry::operator=(ActionEntry &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::ActionEntry::operator==(const ActionEntry &other) const
{
	Q_D(const ActionEntry);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::ActionEntry::operator!=(const ActionEntry &other) const
{
	return !operator==(other);
}

bool Json::ActionEntry::isNull(void) const
{
	Q_D(const ActionEntry);
	return d == Q_NULLPTR;
}

bool Json::ActionEntry::isValid(void) const
{
	return !isNull() && (type() != TYPE_UNKNOWN)
	    && ((!objectName().isEmpty()) || (type() == TYPE_SEPARATOR));
}

enum Json::ActionEntry::Type Json::ActionEntry::type(void) const
{
	Q_D(const ActionEntry);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return INVALID_ACTION_TYPE;
	}

	return d->m_type;
}

void Json::ActionEntry::setType(Type t)
{
	ensureActionEntryPrivate();
	Q_D(ActionEntry);
	d->m_type = t;
}

bool Json::ActionEntry::checked(void) const
{
	Q_D(const ActionEntry);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return DFLT_CHECKED;
	}

	return d->m_checked;
}

void Json::ActionEntry::setChecked(bool ch)
{
	ensureActionEntryPrivate();
	Q_D(ActionEntry);
	d->m_checked = ch;
}

const QString &Json::ActionEntry::objectName(void) const
{
	Q_D(const ActionEntry);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_objectName;
}

void Json::ActionEntry::setObjectName(const QString &on)
{
	ensureActionEntryPrivate();
	Q_D(ActionEntry);
	d->m_objectName = on;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Json::ActionEntry::setObjectName(QString &&on)
{
	ensureActionEntryPrivate();
	Q_D(ActionEntry);
	d->m_objectName = ::std::move(on);
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::ActionEntry Json::ActionEntry::fromJson(const QByteArray &json,
    bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return ActionEntry();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString strTypeUnknown("TYPE_UNKNOWN");
static const QString strTypeAction("TYPE_ACTION");
static const QString strTypeSeparator("TYPE_SEPARATOR");
static const QString strTypeWidget("TYPE_WIDGET");

static
const QString &type2str(enum Json::ActionEntry::Type type)
{
	switch (type) {
	case Json::ActionEntry::TYPE_UNKNOWN:
		return strTypeUnknown;
		break;
	case Json::ActionEntry::TYPE_ACTION:
		return strTypeAction;
		break;
	case Json::ActionEntry::TYPE_SEPARATOR:
		return strTypeSeparator;
		break;
	case Json::ActionEntry::TYPE_WIDGET:
		return strTypeWidget;
		break;
	default:
		/* Fallback. */
		return strTypeUnknown;
		break;
	}
}

static
enum Json::ActionEntry::Type str2type(const QString &str)
{
	if (str == strTypeUnknown) {
		return Json::ActionEntry::TYPE_UNKNOWN;
	} else if (str == strTypeAction) {
		return Json::ActionEntry::TYPE_ACTION;
	} else if (str == strTypeSeparator) {
		return Json::ActionEntry::TYPE_SEPARATOR;
	} else if (str == strTypeWidget) {
		return Json::ActionEntry::TYPE_WIDGET;
	} else {
		/* Fallback. */
		return Json::ActionEntry::TYPE_UNKNOWN;
	}
}

static const QString keyType("type");
static const QString keyChecked("checked");
static const QString keyObjectName("objectName");

Json::ActionEntry Json::ActionEntry::fromJsonVal(const QJsonValue &jsonVal,
    bool *ok)
{
	ActionEntry ae;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();

		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyType,
			        valStr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			ae.setType(str2type(valStr));
		}
		{
			bool valBool;
			if (Q_UNLIKELY(!Helper::readBool(jsonObj, keyChecked,
			        valBool, Helper::ACCEPT_VALID))) {
				goto fail;
			} else {
				ae.setChecked(valBool);
			}
		}
		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyObjectName,
			        valStr, Helper::ACCEPT_MISSING))) {
				goto fail;
			}
			ae.setObjectName(macroStdMove(valStr));
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return ae;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return ActionEntry();
}

bool Json::ActionEntry::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyType, type2str(type()));
	jsonObj.insert(keyChecked, checked());
	if (!objectName().isEmpty()) {
		jsonObj.insert(keyObjectName, objectName());
	}

	jsonVal = jsonObj;
	return true;
}

void Json::swap(ActionEntry &first, ActionEntry &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

Json::ActionEntryList::ActionEntryList(void)
    : Object(),
    QList<ActionEntry>()
{
}

Json::ActionEntryList::ActionEntryList(const QList<ActionEntry> &other)
    : Object(),
    QList<ActionEntry>(other)
{
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::ActionEntryList::ActionEntryList(QList<ActionEntry> &&other)
    : Object(),
    QList<ActionEntry>(::std::move(other))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::ActionEntryList Json::ActionEntryList::fromJson(const QByteArray &json,
    bool *ok)
{
	QJsonArray jsonArr;
	if (!Helper::readRootArray(json, jsonArr)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return ActionEntryList();
	}

	return fromJsonVal(jsonArr, ok);
}

Json::ActionEntryList Json::ActionEntryList::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	ActionEntryList ael;

	if (Q_UNLIKELY(!jsonVal.isArray())) {
		goto fail;
	}

	{
		const QJsonArray jsonArr = jsonVal.toArray();
		for (const QJsonValue &v : jsonArr) {
			bool iOk = false;
			ael.append(ActionEntry::fromJsonVal(v, &iOk));
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return ael;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return ActionEntryList();
}

bool Json::ActionEntryList::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonArray arr;

	for (const ActionEntry &te : *this) {
		QJsonValue v;
		if (Q_UNLIKELY(!te.toJsonVal(v))) {
			goto fail;
		}
		arr.append(v);
	}

	jsonVal = arr;
	return true;

fail:
	jsonVal = QJsonArray();
	return false;
}
