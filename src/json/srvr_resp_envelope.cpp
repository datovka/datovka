/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QJsonValue>
#include <QJsonObject>
#include <QString>
#include <utility> /* ::std::swap */

#include "src/datovka_shared/json/helper.h"
#include "src/json/srvr_resp_envelope.h"

static const QString nullString;
static const QJsonValue nullJsonValue;

class Json::SrvrResponseEnvelopePrivate {
public:
	SrvrResponseEnvelopePrivate(void)
	    : m_jsonValueName(), m_jsonValue(),
	    m_statusCode(), m_statusMessage()
	{ }

	SrvrResponseEnvelopePrivate &operator=(const SrvrResponseEnvelopePrivate &other) Q_DECL_NOTHROW
	{
		m_jsonValueName = other.m_jsonValueName;
		m_jsonValue = other.m_jsonValue;
		m_statusCode = other.m_statusCode;
		m_statusMessage = other.m_statusMessage;

		return *this;
	}

	bool operator==(const SrvrResponseEnvelopePrivate &other) const
	{
		return (m_jsonValueName == other.m_jsonValueName) &&
		    (m_jsonValue == other.m_jsonValue) &&
		    (m_statusCode == other.m_statusCode) &&
		    (m_statusMessage == other.m_statusMessage);
	}

	QString m_jsonValueName;
	QJsonValue m_jsonValue;
	QString m_statusCode;
	QString m_statusMessage;
};

Json::SrvrResponseEnvelope::SrvrResponseEnvelope(void)
    : Object(),
    d_ptr(Q_NULLPTR)
{
}

Json::SrvrResponseEnvelope::SrvrResponseEnvelope(const SrvrResponseEnvelope &other)
    : Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) SrvrResponseEnvelopePrivate) : Q_NULLPTR)
{
	Q_D(SrvrResponseEnvelope);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::SrvrResponseEnvelope::SrvrResponseEnvelope(SrvrResponseEnvelope &&other) Q_DECL_NOEXCEPT
    : Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Json::SrvrResponseEnvelope::~SrvrResponseEnvelope(void)
{
}

/*!
 * @brief Ensures private server response envelope presence.
 *
 * @note Returns if private server response envelope could not be allocated.
 */
#define ensureSrvrResponseEnvelopePrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			SrvrResponseEnvelopePrivate *p = new (::std::nothrow) SrvrResponseEnvelopePrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Json::SrvrResponseEnvelope &Json::SrvrResponseEnvelope::operator=(const SrvrResponseEnvelope &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureSrvrResponseEnvelopePrivate(*this);
	Q_D(SrvrResponseEnvelope);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Json::SrvrResponseEnvelope &Json::SrvrResponseEnvelope::operator=(SrvrResponseEnvelope &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Json::SrvrResponseEnvelope::operator==(const SrvrResponseEnvelope &other) const
{
	Q_D(const SrvrResponseEnvelope);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Json::SrvrResponseEnvelope::operator!=(const SrvrResponseEnvelope &other) const
{
	return !operator==(other);
}

bool Json::SrvrResponseEnvelope::isNull(void) const
{
	Q_D(const SrvrResponseEnvelope);
	return d == Q_NULLPTR;
}

const QString &Json::SrvrResponseEnvelope::jsonValueName(void) const
{
	Q_D(const SrvrResponseEnvelope);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_jsonValueName;
}

void Json::SrvrResponseEnvelope::setJsonValueName(const QString &n)
{
	ensureSrvrResponseEnvelopePrivate();
	Q_D(SrvrResponseEnvelope);
	d->m_jsonValueName = n;
}

#ifdef COMPILER_RVALUE_REFS
void Json::SrvrResponseEnvelope::setJsonValueName(QString &&n)
{
	ensureSrvrResponseEnvelopePrivate();
	Q_D(SrvrResponseEnvelope);
	d->m_jsonValueName = ::std::move(n);
}
#endif /* COMPILER_RVALUE_REFS */

const QJsonValue &Json::SrvrResponseEnvelope::jsonValue(void) const
{
	Q_D(const SrvrResponseEnvelope);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullJsonValue;
	}

	return d->m_jsonValue;
}

void Json::SrvrResponseEnvelope::setJsonValue(const QJsonValue &v)
{
	ensureSrvrResponseEnvelopePrivate();
	Q_D(SrvrResponseEnvelope);
	d->m_jsonValue = v;
}

#ifdef COMPILER_RVALUE_REFS
void Json::SrvrResponseEnvelope::setJsonValue(QJsonValue &&v)
{
	ensureSrvrResponseEnvelopePrivate();
	Q_D(SrvrResponseEnvelope);
	d->m_jsonValue = ::std::move(v);
}
#endif /* COMPILER_RVALUE_REFS */

const QString &Json::SrvrResponseEnvelope::statusCode(void) const
{
	Q_D(const SrvrResponseEnvelope);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_statusCode;
}

void Json::SrvrResponseEnvelope::setStatusCode(const QString &c)
{
	ensureSrvrResponseEnvelopePrivate();
	Q_D(SrvrResponseEnvelope);
	d->m_statusCode = c;
}

#ifdef COMPILER_RVALUE_REFS
void Json::SrvrResponseEnvelope::setStatusCode(QString &&c)
{
	ensureSrvrResponseEnvelopePrivate();
	Q_D(SrvrResponseEnvelope);
	d->m_statusCode = ::std::move(c);
}
#endif /* COMPILER_RVALUE_REFS */

const QString &Json::SrvrResponseEnvelope::statusMessage(void) const
{
	Q_D(const SrvrResponseEnvelope);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_statusMessage;
}

void Json::SrvrResponseEnvelope::setStatusMessage(const QString &m)
{
	ensureSrvrResponseEnvelopePrivate();
	Q_D(SrvrResponseEnvelope);
	d->m_statusMessage = m;
}

#ifdef COMPILER_RVALUE_REFS
void Json::SrvrResponseEnvelope::setStatusMessage(QString &&m)
{
	ensureSrvrResponseEnvelopePrivate();
	Q_D(SrvrResponseEnvelope);
	d->m_statusMessage = ::std::move(m);
}
#endif /* COMPILER_RVALUE_REFS */

bool Json::SrvrResponseEnvelope::statusOk(void) const
{
	const QString &valStr = statusCode();
	if (valStr.isEmpty()) {
		return false;
	}

	bool ok = false;
	qint64 readVal = valStr.toLongLong(&ok);
	if (Q_UNLIKELY(!ok)) {
		return false;
	}

	return readVal == 0;
}

Json::SrvrResponseEnvelope Json::SrvrResponseEnvelope::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (!Helper::readRootObject(json, jsonObj)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return SrvrResponseEnvelope();
	}

	return fromJsonVal(jsonObj, ok);
}

static const QString keyContentType("contentType");
static const QString keyContentValue("contentValue");
static const QString keyStatusCode("statusCode");
static const QString keyStatusMessage("statusMessage");

Json::SrvrResponseEnvelope Json::SrvrResponseEnvelope::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	SrvrResponseEnvelope sre;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyContentType,
			        valStr, Helper::ACCEPT_NULL))) {
				goto fail;
			}
			sre.setJsonValueName(::std::move(valStr));
		}
		{
			QJsonValue jsonVal;
			if (!Helper::readValue(jsonObj, keyContentValue, jsonVal,
			        Helper::ACCEPT_NULL)) {
				goto fail;
			}
			sre.setJsonValue(::std::move(jsonVal));
		}
		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyStatusCode,
			        valStr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			sre.setStatusCode(::std::move(valStr));
		}
		{
			QString valStr;
			if (Q_UNLIKELY(!Helper::readString(jsonObj, keyStatusMessage,
			        valStr, Helper::ACCEPT_VALID))) {
				goto fail;
			}
			sre.setStatusMessage(::std::move(valStr));
		}

		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return sre;
	}

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return SrvrResponseEnvelope();
}

bool Json::SrvrResponseEnvelope::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyContentType, !jsonValueName().isNull() ? jsonValueName() : QJsonValue());
	jsonObj.insert(keyContentValue, jsonValue());
	jsonObj.insert(keyStatusCode, !statusCode().isNull() ? statusCode() : QJsonValue());
	jsonObj.insert(keyStatusMessage, !statusMessage().isNull() ? statusMessage() : QJsonValue());

	jsonVal = jsonObj;
	return true;
}

void Json::swap(SrvrResponseEnvelope &first, SrvrResponseEnvelope &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
