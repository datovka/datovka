/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#define ID_ISDS_SYS_DATABOX "aaaaaaa"
#define ISDS_PING_TIMEOUT_MS 10000
#define ISDS_CONNECT_TIMEOUT_MS 10000 /* libdatovka connection time-out. */
#define ISDS_DOWNLOAD_TIMEOUT_MS 900000
#define TIMESTAMP_EXPIR_BEFORE_DAYS 15 /* Show timestamp expiration before days*/
#define RUN_FIRST_ACTION_MS 3000 // 3 sec run action after datovka start
#define TIMER_DEFAULT_TIMEOUT_MS 600000 // 10 min timer period
#define TIMER_STATUS_TIMEOUT_MS 5000 // 5s will message in status bar shown
#define TIMER_MARK_MSG_READ_MS 5000 /* Mark message as read after 5 seconds. */

#define RECMGMT_COMMUNICATION_TIMEOUT_MS ISDS_DOWNLOAD_TIMEOUT_MS

#define SUPPORT_MAIL "datovka@labs.nic.cz"
#define CZ_NIC_URL "https://www.nic.cz"
#define DATOVKA_BEST_PRACTICE_URL "https://datovka.nic.cz/redirect/best-practice.html"
#define DATOVKA_CHECK_NEW_VERSION_URL "https://datovka.nic.cz/Version"
#define DATOVKA_DONATE_DESKTOP_URL "https://datovka.nic.cz/redirect/donation-desktop.html"
#define DATOVKA_DOWNLOAD_URL "https://datovka.nic.cz/redirect/app-download.html"
#define DATOVKA_FAQ_URL "https://datovka.nic.cz/redirect/faq.html"
#define DATOVKA_HOMEPAGE_URL "https://www.datovka.cz/"
#define DATOVKA_ONLINE_HELP_URL "https://datovka.nic.cz/redirect/prirucka.html"
#define DATOVKA_PACKAGE_LIST_FILE_URL "https://datovka.nic.cz/Packages"
#define PWD_EXPIRATION_NOTIFICATION_DAYS 7 // show expiration date dialog before xx days

/* return values of Datovka functions */
typedef enum {
	Q_SUCCESS = 0,   // all operations success
	Q_GLOBAL_ERROR,  // any qdatovka error
	Q_ISDS_ERROR,
	Q_SQL_ERROR
} qdatovka_error;

/* Message direction. */
enum MessageDirection {

	/* Use only for advanced search. */
	MSG_ALL = 0,

	/* Received message type in table supplementary_message_data is set
	 * to 1; message_type = 1.
	 * Must always be set to 1 because of old Datovka compatibility.
	 */
	MSG_RECEIVED = 1,

	/* Sent message type in table supplementary_message_data is set
	 * to 2; message_type = 2.
	 * Must always be set to 2 because of old Datovka compatibility.
	 */
	MSG_SENT = 2
};

/*!
 * @brief Adds attachment into email.
 *
 * @param[in,out] message Message body.
 * @param[in] attachName  Attachment file name.
 * @param[in] base64      Attagment content in Base64.
 * @param[in] boundary    Boundary string.
 */
void addAttachmentToEmailMessage(QString &message, const QString &attachName,
    const QByteArray &base64, const QString &boundary);

/*!
 * @brief Computes the size of real (decoded from base64) data.
 *
 * @param[in] b64 Data in Base64.
 * @return Size of real data.
 */
int base64RealSize(const QByteArray &b64);

/*!
 * @brief Creates email header and message body.
 *
 * @param[in,out] message Message body.
 * @param[in] subj        Subject string.
 * @param[in] boundary    Boundary string.
 */
void createEmailMessage(QString &message, const QString &subj,
    const QString &boundary);

/*!
 * @brief Adds last line into email.
 *
 * @param[in,out] message Message body.
 * @param[in] boundary    Boundary string.
 */
void finishEmailMessage(QString &message, const QString &boundary);

/*!
 * @brief Converts base64 encoded string into plain text.
 *
 * @param[in] base64 String in Base64.
 * @return Decoded string.
 */
QString fromBase64(const QString &base64);

/*!
 * @brief Converts string to base64.
 *
 * @param[in] plain Plain input string.
 * @return text string in Base64.
 */
QString toBase64(const QString &plain);
