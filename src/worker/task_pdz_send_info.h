/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/types.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing download PDZ send information.
 */
class TaskPDZSendInfo : public Task {
public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Sender account identifier.
	 * @param[in] dbId Recipient data box identifier.
	 */
	explicit TaskPDZSendInfo(const AcntId &acntId, const QString &dbId);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	bool m_success; /*!< True on success. */
	QString m_isdsError; /*!< Error description. */
	QString m_isdsLongError; /*!< Long error description. */

	bool m_canSendNormal; /*!< True if a normal commercial messages can be sent. */
	bool m_canSendInitiatory; /*!< True if an initiatory commercial messages can be sent. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskPDZSendInfo(const TaskPDZSendInfo &);
	TaskPDZSendInfo &operator=(const TaskPDZSendInfo &);

	/*!
	 * @brief Download PDZ information from ISDS.
	 *
	 * @param[in]  acntId Account identifier.
	 * @param[in]  dbId Data box identifier.
	 * @param[in]  type Whether to send normal or initiatory
	 *                  commercial messages.
	 * @param[out] canSend True of such commercial message can be sent.
	 * @param[out] error Error description.
	 * @param[out] longError Long error description.
	 * @return True on success.
	 */
	static
	bool pdzSendInfo(const AcntId &acntId, const QString &dbId,
	    enum Isds::Type::PdzMessageType type, bool &canSend, QString &error,
	    QString &longError);

	const AcntId m_acntId; /*!< Sender account identifier. */
	const QString m_dbId; /*!< Recipient data box identifier. */
};
