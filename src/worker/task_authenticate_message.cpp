/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFile>
#include <QThread>

#include "src/datovka_shared/isds/error.h"
#include "src/datovka_shared/isds/type_description.h"
#include "src/datovka_shared/isds/types.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/settings/prefs_specific.h"
#include "src/io/isds_sessions.h"
#include "src/isds/services.h"
#include "src/worker/message_emitter.h"
#include "src/worker/task_authenticate_message.h"

TaskAuthenticateMessage::TaskAuthenticateMessage(const AcntId &acntId,
    const QString &fileName, enum MsgSizeType sizeType)
    : m_result(AUTH_ERR),
    m_isdsError(),
    m_isdsLongError(),
    m_acntId(acntId),
    m_data(),
    m_sizeType(sizeType)
{
	Q_ASSERT(m_acntId.isValid());

	QFile file(fileName);

	if (file.exists()) {
		if (file.open(QIODevice::ReadOnly)) {
			m_data = file.readAll();
			file.close();
		} else {
			logErrorNL("Couldn't open file '%s'.",
			    fileName.toUtf8().constData());
		}
	}
}

TaskAuthenticateMessage::TaskAuthenticateMessage(const AcntId &acntId,
    const QByteArray &data, enum MsgSizeType sizeType)
    : m_result(AUTH_ERR),
    m_isdsError(),
    m_isdsLongError(),
    m_acntId(acntId),
    m_data(data),
    m_sizeType(sizeType)
{
	Q_ASSERT(m_acntId.isValid());
}

void TaskAuthenticateMessage::run(void)
{
	if (Q_UNLIKELY(!m_acntId.isValid())) {
		Q_ASSERT(0);
		return;
	}

	logDebugLv0NL("Starting authenticate message task in thread '%p'",
	    (void *) QThread::currentThreadId());

	/* ### Worker task begin. ### */

	m_result = authenticateMessage(m_acntId, m_data, m_sizeType,
	    m_isdsError, m_isdsLongError);

	emit GlobInstcs::msgProcEmitterPtr->progressChange(PL_IDLE, 0);

	/* ### Worker task end. ### */

	logDebugLv0NL("Authenticate message task finished in thread '%p'",
	    (void *) QThread::currentThreadId());
}

/*
 * Typically, the raw size of 26.7 MB indicates a VoDZ.
 * Both, AuthenticateMessage and AuthenticateBigMessage can be used for
 * messages around this threshold.
 *
 * 26.7 MB / 20 MB = 1.335
 */
#define MB_RATIO 1.335

enum TaskAuthenticateMessage::Result TaskAuthenticateMessage::authenticateMessage(
    const AcntId &acntId, const QByteArray &data, enum MsgSizeType sizeType,
    QString &error, QString &longError)
{
	Q_ASSERT(acntId.isValid());

	if (data.isEmpty()) {
		return AUTH_DATA_ERROR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId.username());
	if (Q_UNLIKELY(Q_NULLPTR == session)) {
		logErrorNL("Missing active session for username '%s'.",
		    acntId.username().toUtf8().constData());
		return AUTH_ERR;
	}

	if (MSG_VODZ_SIZE_DEPENDENT == sizeType) {
		const qint64 threshold = MB_RATIO *
		    (double) PrefsSpecific::maxAttachmentMBytes(*GlobInstcs::prefsPtr);
		if (threshold <= data.size()) {
			sizeType = MSG_VODZ;
		}
	}

	logInfoNL("%s\n", (MSG_BASIC == sizeType) ?
	    "Authenticating message." : "Authenticating big message.");

	Isds::Error err = (MSG_BASIC == sizeType) ?
	    Isds::Service::authenticateMessage(session, data) :
	    Isds::Service::authenticateBigMessage(session, data);
	if (err.code() == Isds::Type::ERR_NOTEQUAL) {
		return AUTH_NOT_EQUAL;
	} else if (err.code() != Isds::Type::ERR_SUCCESS) {
		error = Isds::Description::descrError(err.code());
		longError = err.longDescr();
		logErrorNL("%s", "Error authenticating message.");
		return AUTH_ISDS_ERROR;
	}

	return AUTH_SUCCESS;
}
