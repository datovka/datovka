/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDateTime>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/worker/task.h"

class MessageDbSet; /* Forward declaration. */
class RecordsManagementDb; /* Forward declaration. */

/*!
 * @brief Task describing uploading account status information.
 */
class TaskRecordsManagementUploadAccountStatus : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		UAS_SUCCESS, /*!< Operation was successful. */
		UAS_COM_ERROR, /*!< Error communicating with records management service. */
		UAS_ERROR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] urlStr Records management URL.
	 * @param[in] tokenStr Records management access token.
	 * @param[in] boxId Data box identifier.
	 * @param[in] acntId Identifier.
	 * @param[in] dateTime Time value.
	 * @param[in] dbSet Database container.
	 * @param[in] recMgmtDb Records management database.
	 * @param[in] userAcntName User-defined account identifier.
	 */
	TaskRecordsManagementUploadAccountStatus(const QString &urlStr,
	    const QString &tokenStr, const QString &boxId,
	    const AcntId &acntId, const QDateTime &dateTime,
	    MessageDbSet *dbSet, RecordsManagementDb *recMgmtDb,
	    const QString &userAcntName);

	/*!
	 * @brief Performs actual message download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Return state. */

private:
	const QString m_url; /*!< String containing records management URL. */
	const QString m_token; /*!< Records management access token. */

	const QString m_boxId; /*!< Data box identifier. */
	const AcntId m_acntId; /*!< Account identifier. */
	QDateTime m_dateTime; /*!< Time value. */
	MessageDbSet *m_dbSet; /*!< Message database container. */
	RecordsManagementDb *m_recMgmtDb; /*!< Records management database. */
	const QString m_userAcntName; /*!< User-defined account identifier. */
};
