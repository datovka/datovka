/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDateTime>
#include <QString>

#include "src/identifiers/account_id_db.h"
#include "src/identifiers/message_id.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing signed message download.
 */
class TaskDownloadMessage : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DM_SUCCESS, /*!< Operation was successful. */
		DM_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DM_DB_INS_ERR, /*!< Error inserting into database. */
		DM_ABORTED, /*!< Operation has been aborted. */
		DM_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntIdDb Account descriptor.
	 * @param[in] msgDirect Received or sent list.
	 * @param[in] msgId Message identifier.
	 * @param[in] storeRawInDb Whether to store raw data in database file.
	 * @param[in] processFlags Message processing flags.
	 * @param[in] recMgmtHierarchyId Predefined target record management hierarchy id.
	 */
	explicit TaskDownloadMessage(const AcntIdDb &acntIdDb,
	    enum MessageDirection msgDirect, const MsgId &msgId,
	    bool storeRawInDb, int processFlags = Task::PROC_NOTHING,
	    const QString &recMgmtHierarchyId = QString());

	/*!
	 * @brief Performs actual message download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Download delivery info for message.
	 *
	 * TODO -- This method must be private.
	 *
	 * @param[in]  acntIdDb Account descriptor.
	 * @param[in]  dmId Message identifier.
	 * @param[in]  storeRawInDb Whether to store raw data in database file.
	 * @param[out] error Error description.
	 * @param[out] longError Long error description.
	 * @return Error state.
	 */
	static
	enum Result downloadAndStoreSignedDeliveryInfo(const AcntIdDb &acntIdDb,
	    qint64 dmId, bool storeRawInDb, QString &error, QString &longError);

	/*!
	 * @brief Download whole signed message (envelope, attachments, raw +
	 *     delivery info).
	 *
	 * TODO -- This method ought to be protected.
	 *
	 * @param[in]     acntIdDb Account descriptor.
	 * @param[in,out] mId Message identifier.
	 * @param[in]     msgDirect Received or sent message.
	 * @param[in]     storeRawInDb Whether to store raw data in database file.
	 * @param[out]    error     Error description.
	 * @param[out]    longError Long error description.
	 * @param[in]     progressLabel Progress-bar label.
	 * @return Error state.
	 */
	static
	enum Result downloadAndStoreSignedMessage(const AcntIdDb &acntIdDb,
	    MsgId &mId, enum MessageDirection msgDirect, bool storeRawInDb,
	    QString &error, QString &longError, const QString &progressLabel);

	enum Result m_result; /*!< Return state. */
	QString m_isdsError; /*!< Error description. */
	QString m_isdsLongError; /*!< Long error description. */

	/* Delivery time may change. */
	MsgId m_mId; /*!< Message identifier. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskDownloadMessage(const TaskDownloadMessage &);
	TaskDownloadMessage &operator=(const TaskDownloadMessage &);

	const AcntIdDb m_acntIdDb; /*!< Account descriptor. */
	enum MessageDirection m_msgDirect; /*!< Sent or received message. */
	const bool m_storeRawInDb; /*!< True whether to store attachment into database, false when to store as files. */
	const int m_processFlags; /*!< Message processing flags. */
	const QString m_recMgmtHierarchyId; /*!< Predefined target record management hierarchy id. */
};
