/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing message authentication.
 */
class TaskAuthenticateMessage : public Task {
public:
	enum MsgSizeType {
		MSG_BASIC, /*!< Always treat as normal message. */
		MSG_VODZ_SIZE_DEPENDENT, /*!< VoDZ if message exceeds the limit for basic message. */
		MSG_VODZ /*!< Always treat as VoDZ. */
	};

	/*!
	 * @Brief Return state describing what happened.
	 */
	enum Result {
		AUTH_SUCCESS, /*!< Authentication was successful. */
		AUTH_DATA_ERROR, /*!< Data to be authenticated are empty. */
		AUTH_NOT_EQUAL, /*!< Data could not be authenticated. */
		AUTH_ISDS_ERROR, /*!< Error communicating with ISDS. */
		AUTH_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor, from data.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] fileName Name of file containing message data.
	 */
	explicit TaskAuthenticateMessage(const AcntId &acntId,
	    const QString &fileName, enum MsgSizeType sizeType);

	/*!
	 * @brief Constructor, from file.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] data Message data.
	 */
	explicit TaskAuthenticateMessage(const AcntId &acntId,
	    const QByteArray &data, enum MsgSizeType sizeType);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Authenticates a message.
	 *
	 * TODO -- This method ought to be protected.
	 *
	 * @param[in]  acntId Account identifier.
	 * @param[in]  data Message data.
	 * @param[out] error Error description.
	 * @param[out] longError Long error description.
	 * @return Authentication result.
	 */
	static
	enum Result authenticateMessage(const AcntId &acntId,
	    const QByteArray &data, enum MsgSizeType sizeType,
	    QString &error, QString &longError);

	enum Result m_result; /*!< Authentication outcome. */
	QString m_isdsError; /*!< Error description. */
	QString m_isdsLongError; /*!< Long error description. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskAuthenticateMessage(const TaskAuthenticateMessage &);
	TaskAuthenticateMessage &operator=(const TaskAuthenticateMessage &);

	const AcntId m_acntId; /*!< Account identifier. */
	QByteArray m_data; /*!< Message data. */
	enum MsgSizeType m_sizeType; /*!< Message size type. */
};
