/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/message_interface_vodz.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing attachment upload.
 */
class TaskUploadAttachment : public QObject, public Task {
	Q_OBJECT
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		UPLOAD_SUCCESS, /*!< Upload was successful. */
		UPLOAD_DATA_ERROR, /*!< Data to be uploaded are empty. */
		UPLOAD_ISDS_ERROR, /*!< Error communicating with ISDS. */
		UPLOAD_ERROR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dmFile File info.
	 * @param[in] transactId Optional unique transaction identifier.
	 */
	explicit TaskUploadAttachment(const AcntId &acntId,
	    const Isds::DmFile &dmFile, const QString &transactId);

#ifdef Q_COMPILER_RVALUE_REFS
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dmFile File info.
	 * @param[in] transactId Optional unique transaction identifier.
	 */
	explicit TaskUploadAttachment(const AcntId &acntId,
	    Isds::DmFile &&dmFile, const QString &transactId);
#endif /* Q_COMPILER_RVALUE_REFS */

	/*!
	 * @brief Performs action.
	 *
	 * @note Emits uploadProgressFinished().
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Authentication outcome. */
	QString m_isdsError; /*!< Error description. */
	QString m_isdsLongError; /*!< Long error description. */
	Isds::DmAtt m_dmAtt; /*!< Uploaded attachment info. */

private slots:
	/*!
	 * @brief Watch ISDS session communication.
	 *
	 * @note Emits uploadProgress() when data uploaded.
	 *
	 * @param[in] uploadTotal Expected total upload,.
	 * @param[in] uploadCurrent Cumulative current upload progress.
	 * @param[in] downloadTotal Expected total download.
	 * @param[in] downloadCurrent Cumulative current download progress.
	 */
	void watchProgress(qint64 uploadTotal, qint64 uploadCurrent,
	    qint64 downloadTotal, qint64 downloadCurrent);

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskUploadAttachment(const TaskUploadAttachment &);
	TaskUploadAttachment &operator=(const TaskUploadAttachment &);

	/*!
	 * @brief Upload attachment.
	 *
	 * @param[in]  acntId Account identifier.
	 * @param[in]  dmFile File info.
	 * @param[out] dmAtt Uploaded attachment info.
	 * @param[out] error Error description.
	 * @param[out] longError Long error description.
	 * @return Upload result.
	 */
	static
	enum Result uploadAttachment(const AcntId &acntId,
	    const Isds::DmFile &dmFile, Isds::DmAtt &dmAtt, QString &error,
	    QString &longError);

	const AcntId m_acntId; /*!< Account identifier. */
	const Isds::DmFile m_dmFile; /*!< File info. */
	const QString m_transactId; /*!< Transaction identifier. */
};
