/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/worker/task.h"

/*!
 * @brie Task describing the obtaining of data-box users.
 */
class TaskGetDataBoxUsers2 : public Task {
public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account identifier of the account creator.
	 */
	explicit TaskGetDataBoxUsers2(const AcntId &acntId);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	bool m_success; /*!< True on success. */
	QString m_isdsError; /*!< Error description. */
	QString m_isdsLongError; /*!< Long error description. */

	QList<Isds::DbUserInfoExt2> m_users; /*!< Box user descriptions. */

private:
	/*!
	 * @brief Download data-box user information.
	 *
	 * @param[in]  acntId Account identifier of the account creator.
	 * @param[out] users List of user descriptions.
	 * @param[out] error Error description.
	 * @param[out] longError Long error description.
	 * @return True on success.
	 */
	static
	bool getDataBoxUsers2(const AcntId &acntId,
	    QList<Isds::DbUserInfoExt2> &users, QString &error,
	    QString &longError);

	const AcntId m_acntId; /*!< Account identifier of the creator. */
};
