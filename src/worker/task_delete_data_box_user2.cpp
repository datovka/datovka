/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/isds/error.h"
#include "src/datovka_shared/isds/type_description.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/io/account_db.h"
#include "src/io/isds_sessions.h"
#include "src/isds/services.h"
#include "src/worker/task_delete_data_box_user2.h"

TaskDeleteDataBoxUser2::TaskDeleteDataBoxUser2(const AcntId &acntId,
    const QString &isdsID)
    : m_success(false),
    m_status(),
    m_isdsError(),
    m_isdsLongError(),
    m_acntId(acntId),
    m_isdsID(isdsID)
{
}

void TaskDeleteDataBoxUser2::run(void)
{
	if (Q_UNLIKELY(!m_acntId.isValid())) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(m_isdsID.isEmpty())) {
		Q_ASSERT(0);
		return;
	}

	logDebugLv0NL("Starting delete data-box user 2 task in thread '%p'",
	    (void *) QThread::currentThreadId());

	m_success = deleteDataBoxUser2(m_acntId, m_isdsID, m_status,
	    m_isdsError, m_isdsLongError);

	logDebugLv0NL(
	    "Delete data-box user 2 task finished in thread '%p'",
	    (void *) QThread::currentThreadId());
}

bool TaskDeleteDataBoxUser2::deleteDataBoxUser2(const AcntId &acntId,
    const QString &isdsID, Isds::Status &status, QString &error,
    QString &longError)
{
	Isds::Session *session =
	    GlobInstcs::isdsSessionsPtr->session(acntId.username());
	if (Q_UNLIKELY(Q_NULLPTR == session)) {
		logErrorNL("Missing active session for username '%s'.",
		    acntId.username().toUtf8().constData());
		return false;
	}

	const QString acntDbKey = AccountDb::keyFromLogin(acntId.username());
	const QString dbId = GlobInstcs::accntDbPtr->dbId(acntDbKey);
	if (Q_UNLIKELY(dbId.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	Isds::Error err = Isds::Service::deleteDataBoxUser2(session, dbId,
	    isdsID, status);
	if (err.code() != Isds::Type::ERR_SUCCESS) {
		error = Isds::Description::descrError(err.code());
		longError = err.longDescr();
		logErrorNL(
		    "Deleting data-box user for account '%s' returned '%d': '%s'.",
		    acntId.username().toUtf8().constData(),
		    err.code(), error.toUtf8().constData());
		return false;
	}

	return true;
}
