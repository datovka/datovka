/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/isds/message_interface.h"
#include "src/datovka_shared/isds/types.h"
#include "src/identifiers/account_id_db.h"
#include "src/records_management/content/automatic_upload_target.h"
#include "src/worker/task.h"

class MessageDbSet; /* Forward declaration. */
namespace Isds {
	class Session; /* Forward declaration. */
}

/*!
 * @brief Task describing download message list.
 */
class TaskDownloadMessageList : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ABORTED, /*!< Operation has been aborted. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] rAcntIdDb Regular account descriptor.
	 * @param[in] sAcntId Shadow account identifier, may be invalid.
	 * @param[in] msgDirect Received or sent list.
	 * @param[in] downloadWhole True to plan downloading whole messages.
	 * @param[in] checkDownloadedList True to check message listing in database.
	 * @param[in] dmLimit Message list length limit.
	 * @param[in] dmStatusFltr Type of messages to be downloaded.
	 * @param[in] recMgmtTargets Records management targets.
	 */
	explicit TaskDownloadMessageList(const AcntIdDb &rAcntIdDb,
	    const AcntId sAcntId, enum MessageDirection msgDirect,
	    bool downloadWhole, bool checkDownloadedList,
	    unsigned long dmLimit = MESSAGE_LIST_LIMIT,
	    Isds::Type::DmFiltStates dmStatusFltr = Isds::Type::MFS_ANY,
	    const RecMgmt::AutomaticUploadTarget &recMgmtTargets = RecMgmt::AutomaticUploadTarget());

	/*!
	 * @brief Performs actual message download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Download message list from ISDS for given account.
	 *
	 * TODO -- This method ought to be protected.
	 *
	 * @param[in]  acntIdDb Account descriptor.
	 * @param[in]  msgDirect Received or sent message.
	 * @param[in]  downloadWhole True to plan downloading whole messages.
	 * @param[in]  checkDownloadedList True to check message listing in database.
	 * @param[out] error Error description.
	 * @param[out] longError Long error description.
	 * @param[in]  progressLabel Progress-bar label.
	 * @param[out] total Total number of messages.
	 * @param[out] news Number of new messages.
	 * @param[out] newMsgIdList Identifiers of new messages.
	 * @param[in]  dmLimit Maximum number of message list;
	 *                     NULL if you don't care.
	 * @param[in]  dmStatusFilter Status filter.
	 * @param[in]  recMgmtTargets Records management targets.
	 * @return Error state.
	 */
	static
	enum Result downloadMessageList(const AcntIdDb &acntIdDb,
	    enum MessageDirection msgDirect, bool downloadWhole,
	    bool checkDownloadedList, QString &error, QString &longError,
	    const QString &progressLabel, int &total, int &news,
	    QList<qint64> &newMsgIdList, ulong *dmLimit,
	    Isds::Type::DmFiltStates dmStatusFilter,
	    const RecMgmt::AutomaticUploadTarget &recMgmtTargets);

	enum Result m_result; /*!< Return state. */
	QString m_isdsError; /*!< Error description. */
	QString m_isdsLongError; /*!< Long error description. */
	QList<qint64> m_newMsgIdList; /*!<
	                               * List of ids that have not been
	                               * in database.
	                               */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskDownloadMessageList(const TaskDownloadMessageList &);
	TaskDownloadMessageList &operator=(const TaskDownloadMessageList &);

	/*!
	 * @brief Download sent message delivery info and get list of events.
	 *
	 * @param[in]     msgDirect Received or sent message.
	 * @param[in,out] session Communication session.
	 * @param[in,out] dbSet Database container.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    error Error description.
	 * @param[out]    longError Long error description.
	 * @return Error state.
	 */
	static
	enum Result downloadMessageState(enum MessageDirection msgDirect,
	    Isds::Session *session, MessageDbSet &dbSet, qint64 dmId,
	    QString &error, QString &longError);

	/*!
	 * @brief Update message information according to supplied envelope.
	 *
	 * @param[in]     msgDirect Received or sent message.
	 * @param[in,out] dbSet Database container.
	 * @param[in]     envel Message envelope.
	 * @return Error state.
	 */
	static
	enum Result updateMessageState(enum MessageDirection msgDirect,
	    MessageDbSet &dbSet, const Isds::Envelope &envel);

	const AcntIdDb m_rAcntIdDb; /*!< Regular account descriptor. */
	const AcntId m_sAcntId; /*!< Shadow account identifier. */
	enum MessageDirection m_msgDirect; /*!< Sent or received list. */
	const bool m_downloadWhole; /*!< Plan downloading whole messages. */
	const bool m_checkDownloadedList; /*!< Plan checking of message listing. */
	unsigned long m_dmLimit; /*!< List length limit. */
	Isds::Type::DmFiltStates m_dmStatusFilter; /*!< Defines type of messages to be downloaded. */
	const RecMgmt::AutomaticUploadTarget m_recMgmtTargets; /*!< Automatic records management upload targets. */
};
