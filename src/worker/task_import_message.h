/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */

#include "src/worker/task.h"

class MessageDbSet; /*Forward declaration. */
class MessageDbSingle; /*Forward declaration. */
class MsgId; /*Forward declaration. */

/*!
 * @brief Task describing messages import from external database file.
 */
class TaskImportMessage : public Task {
	Q_DECLARE_TR_FUNCTIONS(TaskImportMessage)

public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		IMP_SUCCESS, /*!< Import was successful. */
		IMP_DB_ERROR, /*!< Open database fail. */
		IMP_MSG_ID_ERR, /*!< Message ID is wrong. */
		IMP_DB_INS_ERR, /*!< Error inserting into database. */
		IMP_DB_EXISTS, /*!< Message already exists. */
		IMP_ABORTED, /*!< Operation has been aborted. */
		IMP_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] userName   Account user name.
	 * @param[in] dbSet      Non-null pointer to target database container.
	 * @param[in] dbFileList List of source databases.
	 * @param[in] dbId       Databox ID of target account.
	 */
	explicit TaskImportMessage(const QString &userName, MessageDbSet *dbSet,
	    const QStringList &dbFileList, const QString &dbId);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Import outcome. */
	QStringList m_resultDescList; /*!< List of unsuccessful messages import. */
	int m_msgCntTotal; /*!< Holds total number of messages in source db. */
	int m_importedMsg; /*!< Holds number of success imported messages */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskImportMessage(const TaskImportMessage &);
	TaskImportMessage &operator=(const TaskImportMessage &);

	/*!
	 * @brief Imports messages into database.
	 *
	 * @param[in] userName        Account user name.
	 * @param[in] dbSet           Non-null pointer to target database container.
	 * @param[in] dbFileList      List of source databases.
	 * @param[in] dbId            Databox ID of target account.
	 * @param[out] resultDescList List of import results.
	 * @param[out] msgCntTotal    Total number of messages in source db.
	 * @param[out] importedMsg    Number of success imported messages.
	 * @returns Error identifier.
	 */
	static
	enum Result importMessages(const QString &userName,
	    MessageDbSet *dbSet, const QStringList &dbFileList,
	    const QString &dbId, QStringList &resultDescList,
	    int &msgCntTotal, int &importedMsg);

	const QString m_userName; /*!< Account identifier (user login name). */
	MessageDbSet *m_dbSet; /*!< Pointer to target database container. */
	const QStringList m_dbFileList;  /*!< List of source databases. */
	const QString m_dbId; /*!< Databox ID of target account. */
	QString m_resultDesc; /*!< Result description of imported message. */
};
