/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QString>

#include "src/datovka_shared/isds/message_interface.h"
#include "src/identifiers/account_id_db.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing sending message.
 */
class TaskSendMessage : public QObject, public Task {
	Q_OBJECT
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		SM_SUCCESS, /*!< Operation was successful. */
		SM_ISDS_ERROR, /*!< Error communicating with ISDS. */
		SM_DB_INS_ERR, /*!< Error inserting into database. */
		SM_ABORTED, /*!< Operation has been aborted. */
		SM_ERR /*!< Other error. */
	};

	/*!
	 * @brief Gives more detailed information about sending outcome.
	 */
	class ResultData {
	public:
		/*!
		 * @brief Constructors.
		 */
		ResultData(void);
		ResultData(enum Result res, const QString &eInfo,
		    const QString &recId, const QString &recName, bool pdz,
		    qint64 mId);

		enum Result result; /*!< Return state. */
		QString errInfo; /*!< Error description. */
		QString dbIDRecipient; /*!< Recipient identifier. */
		QString recipientName; /*!< Recipient name. */
		bool isPDZ; /*!< True if message was sent as PDZ. */
		qint64 dmId; /*!< Sent message identifier. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntIdDb Account descriptor.
	 * @param[in] transactId Unique transaction identifier.
	 * @param[in] message Message to be sent.
	 * @param[in] recipientName Message recipient name.
	 * @param[in] recipientAddress Message recipient address.
	 * @param[in] isPDZ True if message is a PDZ.
	 * @param[in] isVodz True if message is a VoDZ.
	 * @param[in] storeRawInDb Whether to store raw data in database file.
	 * @param[in] processFlags Message processing flags.
	 * @param[in] recMgmtHierarchyId Predefined target record management hierarchy id.
	 */
	explicit TaskSendMessage(const AcntIdDb &acntIdDb,
	    const QString &transactId, const Isds::Message &message,
	    const QString &recipientName, const QString &recipientAddress,
	    bool isPDZ, bool isVodz, bool storeRawInDb,
	    int processFlags = Task::PROC_NOTHING,
	    const QString &recMgmtHierarchyId = QString());

	/*!
	 * @brief Performs actual message sending.
	 *
	 * @note Emits uploadProgressFinished().
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	ResultData m_resultData; /*!< Return state data. */

private slots:
	/*!
	 * @brief Watch ISDS session communication.
	 *
	 * @note Emits uploadProgress() when data uploaded.
	 *
	 * @param[in] uploadTotal Expected total upload,.
	 * @param[in] uploadCurrent Cumulative current upload progress.
	 * @param[in] downloadTotal Expected total download.
	 * @param[in] downloadCurrent Cumulative current download progress.
	 */
	void watchProgress(qint64 uploadTotal, qint64 uploadCurrent,
	    qint64 downloadTotal, qint64 downloadCurrent);

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskSendMessage(const TaskSendMessage &);
	TaskSendMessage &operator=(const TaskSendMessage &);

	/*!
	 * @brief Sends a single message to ISDS fro given account.
	 *
	 * @param[in]  acntIdDb Account descriptor.
	 * @param[in]  message Message being sent.
	 * @param[in]  recipientName Message recipient name.
	 * @param[in]  recipientAddress Message recipient address.
	 * @param[in]  isPDZ True if message is a PDZ.
	 * @param[in]  isVodz True if message is a VoDZ.
	 * @param[in]  storeRawInDb Whether to store raw data in database file.
	 * @param[in]  progressLabel Progress-bar label.
	 * @param[out] result Results, pass NULL if not desired.
	 * @return Error state.
	 */
	static
	enum Result sendMessage(const AcntIdDb &acntIdDb,
	    const Isds::Message &message, const QString &recipientName,
	    const QString &recipientAddress, bool isPDZ, bool isVodz,
	    bool storeRawInDb, const QString &progressLabel, ResultData *result);

	const AcntIdDb m_acntIdDb; /*!< Account descriptor. */
	const QString m_transactId; /*!< Unique transaction identifier. */
	const Isds::Message m_message; /*!< Message to be sent. */
	const QString m_recipientName; /*!< Message recipient name. */
	const QString m_recipientAddress; /*!< Message recipient address. */
	const bool m_isPDZ; /*!< True if message is a PDZ. */
	const bool m_isVodz; /*!< True if message is a VoDZ. */
	const bool m_storeRawInDb; /*!< True whether to store attachment into database, false when to store as files. */
	const int m_processFlags; /*!< Message processing flags. */
	const QString m_recMgmtHierarchyId; /*!< Predefined target record management hierarchy id. */
};
