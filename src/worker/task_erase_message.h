/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/identifiers/account_id_db.h"
#include "src/identifiers/message_id.h"
#include "src/worker/task.h"

class TaskEraseMessage : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		NOT_DELETED, /*!< Nothing succeeded. */
		DELETED_ISDS, /*!< Only deletion in ISDS succeeded. */
		DELETED_LOCAL, /*!< Only deletion in local db succeeded. */
		DELETED_ISDS_LOCAL /*!< Both deletions succeeded. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntIdDb Account descriptor.
	 * @param[in] msgId Message identifier.
	 * @param[in] msgDirect Received or sent.
	 * @param[in] delFromIsds True if also delete from ISDS.
	 */
	explicit TaskEraseMessage(const AcntIdDb &acntIdDb, const MsgId &msgId,
	    enum MessageDirection msgDirect, bool delFromIsds);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Deletion outcome. */
	QString m_isdsError; /*!< Error description. */
	QString m_isdsLongError; /*!< Long error description. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskEraseMessage(const TaskEraseMessage &);
	TaskEraseMessage &operator=(const TaskEraseMessage &);

	/*!
	 * @brief Erases messages form ISDS and/or from local database.
	 *
	 * @param[in]  acntIdDb Account descriptor.
	 * @param[in]  msgId Message identifier.
	 * @param[in]  msgDirect Received or sent.
	 * @param[in]  delFromIsds True if also delete from ISDS.
	 * @param[out] error Error description.
	 * @param[out] longError Long error description.
	 * @return Deletion result.
	 */
	static
	enum Result eraseMessage(const AcntIdDb &acntIdDb, const MsgId &msgId,
	    enum MessageDirection msgDirect, bool delFromIsds, QString &error,
	    QString &longError);

	const AcntIdDb m_acntIdDb; /*!< Account descriptor. */
	const MsgId m_msgId; /*!< Message identifier. */
	enum MessageDirection m_msgDirect; /*!< Received or sent. */
	const bool m_delFromIsds; /*!< True is also delete from ISDS. */
};
