/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/isds/error.h"
#include "src/datovka_shared/isds/type_description.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/io/isds_sessions.h"
#include "src/isds/services.h"
#include "src/worker/task_pdz_send_info.h"

TaskPDZSendInfo::TaskPDZSendInfo(const AcntId &acntId, const QString &dbId)
    : m_success(false),
    m_isdsError(),
    m_isdsLongError(),
    m_canSendNormal(false),
    m_canSendInitiatory(false),
    m_acntId(acntId),
    m_dbId(dbId)
{
}

void TaskPDZSendInfo::run(void)
{
	if (Q_UNLIKELY(!m_acntId.isValid())) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(m_dbId.isEmpty())) {
		Q_ASSERT(0);
		return;
	}

	logDebugLv0NL("Starting download PDZ send info task in thread '%p'",
	    (void *) QThread::currentThreadId());

	/* ### Worker task begin. ### */

	m_success = pdzSendInfo(m_acntId, m_dbId, Isds::Type::PMT_NORMAL,
	    m_canSendNormal, m_isdsError, m_isdsLongError);
	/* Don't ask for initiatory if cannot send normal. */
	if (m_success) {
		pdzSendInfo(m_acntId, m_dbId, Isds::Type::PMT_INIT,
		    m_canSendInitiatory, m_isdsError, m_isdsLongError);
	}

	/* ### Worker task end. ### */

	logDebugLv0NL("Download PDZ info send task finished in thread '%p'",
	    (void *) QThread::currentThreadId());
}

bool TaskPDZSendInfo::pdzSendInfo(const AcntId &acntId, const QString &dbId,
    enum Isds::Type::PdzMessageType type, bool &canSend, QString &error,
    QString &longError)
{
	Q_ASSERT(acntId.isValid());
	Q_ASSERT(!dbId.isEmpty());

	Isds::Session *session =
	    GlobInstcs::isdsSessionsPtr->session(acntId.username());
	if (Q_UNLIKELY(Q_NULLPTR == session)) {
		logErrorNL("Missing active session for username '%s'.",
		    acntId.username().toUtf8().constData());
		return false;
	}

	Isds::Error err = Isds::Service::pdzSendInfo(session, dbId, type,
	    canSend);
	if (Q_UNLIKELY(err.code() != Isds::Type::ERR_SUCCESS)) {
		error = Isds::Description::descrError(err.code());
		longError = err.longDescr();
		logErrorNL(
		    "Downloading PDZ send information returned '%d': '%s'.",
		    err.code(), longError.toUtf8().constData());
		return false;
	}

	return true;
}
