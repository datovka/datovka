/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/error.h"
#include "src/datovka_shared/isds/type_description.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/io/account_db.h"
#include "src/io/isds_sessions.h"
#include "src/isds/services.h"
#include "src/worker/message_emitter.h"
#include "src/worker/task_download_dt_info.h"

TaskDownloadDtInfo::TaskDownloadDtInfo(const AcntId &acntId,
    const QString &dbID)
    : m_success(false),
    m_isdsError(),
    m_isdsLongError(),
    m_acntId(acntId),
    m_dbID(dbID)
{
	Q_ASSERT(m_acntId.isValid());
	Q_ASSERT(!m_dbID.isEmpty());
}

void TaskDownloadDtInfo::run(void)
{
	if (Q_UNLIKELY(!m_acntId.isValid() || m_dbID.isEmpty())) {
		Q_ASSERT(0);
		return;
	}

	logDebugLv0NL("Starting download long term storage info task in thread '%p'",
	    (void *) QThread::currentThreadId());

	/* ### Worker task begin. ### */

	m_success = downloadDtInfo(m_acntId, m_dbID, m_isdsError,
	    m_isdsLongError);

	emit GlobInstcs::msgProcEmitterPtr->progressChange(PL_IDLE, 0);

	/* ### Worker task end. ### */

	logDebugLv0NL("Download long term storage info task finished in thread '%p'",
	    (void *) QThread::currentThreadId());
}

bool TaskDownloadDtInfo::downloadDtInfo(const AcntId &acntId,
    const QString &dbID, QString &error, QString &longError)
{
	Isds::Session *session =
	    GlobInstcs::isdsSessionsPtr->session(acntId.username());
	if (Q_UNLIKELY(Q_NULLPTR == session)) {
		logErrorNL("Missing active session for username '%s'.",
		    acntId.username().toUtf8().constData());
		return false;
	}

	Isds::DTInfoOutput dtInfo;
	Isds::Error err = Isds::Service::dtInfo(session, dbID, dtInfo);
	if (err.code() != Isds::Type::ERR_SUCCESS) {
		error = Isds::Description::descrError(err.code());
		longError = err.longDescr();
		logErrorNL(
		    "Downloading long term storage information for account '%s' returned '%d': '%s'.",
		    acntId.username().toUtf8().constData(),
		    err.code(), error.toUtf8().constData());
		return false;
	}

	Q_ASSERT(!dtInfo.isNull());

	return GlobInstcs::accntDbPtr->insertDTInfoIntoDb(acntId.testing(),
	    dbID, dtInfo);
}
