/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>

#include "src/identifiers/account_id_db.h"
#include "src/identifiers/message_id.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing downloaded message list check.
 */
class TaskCheckDownloadedList : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		CDL_SUCCESS, /*!< Operation was successful. */
		CDL_FOUND_UNLISTED, /*!< Found some messages that are not listed. */
		CDL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] rAcntIdDb Regular account descriptor.
	 * @param[in] msgDirect Received or sent list.
	 * @param[in] msgIds List of downloaded messages.
	 */
	explicit TaskCheckDownloadedList(const AcntIdDb &rAcntIdDb,
	    enum MessageDirection msgDirect, const QList<MsgId> &msgIds);

	/*!
	 * @brief Performs actual message download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Return state. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskCheckDownloadedList(const TaskCheckDownloadedList &);
	TaskCheckDownloadedList &operator=(const TaskCheckDownloadedList &);

	/*!
	 * @brief Compare list of downloaded messages with a list of available
	 *     messages as it is shown in the application.
	 *
	 * @param[in]  rAcntIdDb Regular account descriptor.
	 * @param[in]  msgDirect Received or sent list.
	 * @param[in]  msgIds List of downloaded messages.
	 * @param[out] unlistedMsgIds List of downloaded messages that aren't
	 *                            shown in the application.
	 * @return CDL_SUCCESS if all messages are listed,
	 *      CDL_FOUND_UNLISTED if unlisted messages found,
	 *      CDL_ERR on any error.
	 */
	static
	enum Result checkDownloadedList(const AcntIdDb &rAcntIdDb,
	    enum MessageDirection msgDirect, const QList<MsgId> &msgIds,
	    QList<MsgId> &unlistedMsgIds);

	const AcntIdDb m_rAcntIdDb; /*!< Regular account descriptor. */
	enum MessageDirection m_msgDirect; /*!< Sent or received list. */
	const QList<MsgId> m_msgIds; /*!< List of downloaded messages. */
};
