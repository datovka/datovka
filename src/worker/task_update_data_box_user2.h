/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/isds/generic_interface.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing updating an user account.
 */
class TaskUpdateDataBoxUser2 : public Task {
public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account identifier of the account creator.
	 * @param[in] isdsID User ISDS ID.
	 * @param[in] userInfo New user description.
	 */
	explicit TaskUpdateDataBoxUser2(const AcntId &acntId,
	    const QString &isdsID, const Isds::DbUserInfoExt2 &userInfo);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	bool m_success; /*!< True on success. */
	Isds::Status m_status; /*!< Operation status. */
	QString m_isdsError; /*!< Error description. */
	QString m_isdsLongError; /*!< Long error description. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskUpdateDataBoxUser2(const TaskUpdateDataBoxUser2 &);
	TaskUpdateDataBoxUser2 &operator=(const TaskUpdateDataBoxUser2 &);

	/*!
	 * @brief Modify user account.
	 *
	 * @param[in] acntId Account identifier of the account creator.
	 * @param[in] isdsID User ISDS ID.
	 * @param[in] userInfo New user description.
	 * @param[out] status Operation status.
	 * @param[out] error Error description.
	 * @param[out] longError Long error description.
	 * @return True on success.
	 */
	static
	bool updateDataBoxUser2(const AcntId &acntId, const QString &isdsID,
	    const Isds::DbUserInfoExt2 &userInfo, Isds::Status &status,
	    QString &error, QString &longError);

	const AcntId m_acntId; /*!< Account identifier of the creator. */
	const QString m_isdsID;
	const Isds::DbUserInfoExt2 m_userInfo; /*!< User description. */
};
