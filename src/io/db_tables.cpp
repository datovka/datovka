/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/io/db_tables.h"
#include "src/datovka_shared/io/records_management_db_tables.h"
#include "src/datovka_shared/io/sqlite/table.h"
#include "src/io/account_db_tables.h"
#include "src/io/message_db_tables.h"
#if defined(ENABLE_TAGS)
#  include "src/io/tag_db_tables.h"
#endif /* defined(ENABLE_TAGS) */

void SQLiteTbls::localiseTableDescriptions(void)
{
	/* account_db */
	accntinfTbl.reloadLocalisedDescription();
	userinfTbl.reloadLocalisedDescription();
	pwdexpdtTbl.reloadLocalisedDescription();
	dtinfTbl.reloadLocalisedDescription();
	/* message_db */
	msgsTbl.reloadLocalisedDescription();
	flsTbl.reloadLocalisedDescription();
	hshsTbl.reloadLocalisedDescription();
	evntsTbl.reloadLocalisedDescription();
	prcstTbl.reloadLocalisedDescription();
	rwmsgdtTbl.reloadLocalisedDescription();
	rwdlvrinfdtTbl.reloadLocalisedDescription();
	smsgdtTbl.reloadLocalisedDescription();
	crtdtTbl.reloadLocalisedDescription();
	msgcrtdtTbl.reloadLocalisedDescription();
#if defined(ENABLE_TAGS)
	/* tag_db */
	tagsTbl.reloadLocalisedDescription();
	msgtagsTbl.reloadLocalisedDescription();
#endif /* defined(ENABLE_TAGS) */
	/* records_management_db */
	srvcInfTbl.reloadLocalisedDescription();
	strdFlsMsgsTbl.reloadLocalisedDescription();
}
