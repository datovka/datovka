/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

class QByteArray; /* Forward declaration. */

#define TMP_ATTACHMENT_PREFIX "qdatovka_XXXXXX_"
#define TMP_DIR_NAME "qdatovka_dir_XXXXXX"

/*!
 * @brief Replace illegal characters when constructing a file name.
 *
 * @param[in,out] str String to be modified.
 * @param[in]     replaceSeparators Whether to replace directory separators.
 */
void replaceIllegalChars(QString &str, bool replaceSeparators);

/*!
 * @brief Replace backslashes with slash characters.
 *
 * @param[in,out] str String to be modified.
 */
void replaceBackslashes(QString &str);

/*!
 * @brief Creates complete directory path.
 *
 * @param[in] dirPath Directory path to be created.
 * @return True if directory structure already exists or was successfully
 *     created, false on error.
 */
bool createDirRecursive(const QString &dirPath);

/*!
 * @brief Creates directory structure to store file into.
 *
 * @param[in] filePath File path. File need not be existent.
 * @return True if directory structure already exists or was successfully
 *     created, false on error.
 */
bool createDirStructureRecursive(const QString &filePath);

/*
 * Example: copyDirRecursively("/foo/baz", "/bar") copies the directory
 *     '/foo/baz' and its content into '/bar'.
 *     The result is a directory '/bar/baz'.
 *
 * @param[in] fromDirPath Source directory.
 * @param[in] toPath Target path.
 * @param[in] deleteOnError Delete created files and directories when cannot
 *                          be entirely written.
 */
bool copyDirRecursively(const QString &fromDirPath, const QString &toPath,
    bool deleteOnError = false);

/*!
 * @brief Check if file with given path exists.
 *
 * @param[in] filePath Full file path.
 * @return Full path with non-conflicting file if file already exists,
 *     null string on error.
 */
QString nonconflictingFileName(QString filePath);

/*!
 * @brief Identifies the cause why a file could not be written.
 */
enum WriteFileState {
	WF_SUCCESS = 0, /*!< File was successfully created and written. */
	WF_CANNOT_CREATE, /*!< File could not be created. */
	WF_CANNOT_READ, /*!< File could not be read. */
	WF_CANNOT_WRITE_WHOLE, /*!< File could not be entirely written. */
	WF_ERROR /*!< Different error. */
};

/*!
 * @brief Create file and write data to it.
 *
 * @param[in] filePath      File path.
 * @param[in] data          Data to be written into file.
 * @param[in] deleteOnError Delete created file when cannot be entirely
 *                          written.
 * @return Status identifier.
 */
enum WriteFileState writeFile(const QString &filePath, const QByteArray &data,
    bool deleteOnError = false);

/*!
 * @brief Return preferred temporary file location.
 *
 * @param[in] create If true then the directory is created when missing.
 * @param[in] share If true then a generic cache location shared between applications is returned,
 *                  if false then a temporary directory accessible to this application is likely to be returned.
 * @return Non-empty directory path without trailing directory separator.
 */
QString tempDirPath(bool create, bool share);

/*!
 * @brief Create temporary subdirectory.
 *
 * @param[in] nameTemplate Name template.
 * @return Full path to written directory on success, empty string on failure.
 */
QString createTemporarySubdir(const QString &nameTemplate);

/*!
 * @brief Create and write data to temporary file.
 *
 * @param[in] fileName      File name.
 * @param[in] data          Data to be written into file.
 * @param[in] deleteOnError Delete created file when it cannot be entirely
 *                          written.
 * @return Full path to written file on success, empty string on failure.
 */
QString writeTemporaryFile(const QString &fileName, const QByteArray &data,
    bool deleteOnError = false);

/*!
 * @brief Returns path to configuration directory.
 *
 * @param[in] confSubdir Configuration directory name (e.g. ".dsgui").
 * @return Full path to configuration directory.
 */
QString confDirPath(const QString &confSubdir);

/*!
 * @brief Generates an auxiliary file name.
 *
 * @note repeated calls with same \a filePath may result in different returned
 *     pats - depending on the time change.
 *
 * @param[in] filePath Original file name.
 * @rerturn New file name consisting of concatenated original file name and a
 *     time-based suffix.
 */
QString auxConfFilePath(const QString &filePath);

/*!
 * @brief Changes all occurrences of '\' to '/' in given configuration file.
 *
 * @param[in] filePath Path to file.
 * @param[in] useAux If true then changes are first written to auxiliary file.
 *                   Auxiliary file is then moved to target location.
 * @return Status identifier.
 */
enum WriteFileState confFileFixBackSlashes(const QString &filePath,
    bool useAux);

/*!
 * @brief Fix account password format = compatibility with Datovka 3.
 *
 * @note Removes double quotes ('"') from account password string.
 *
 * @param[in] filePath Path to file.
 * @param[in] useAux If true then changes are first written to auxiliary file.
 *                   Auxiliary file is then moved to target location.
 * @return Status identifier.
 */
enum WriteFileState confFileRemovePwdQuotes(const QString &filePath,
    bool useAux);

/*!
 * @brief Returns the path to directory where application localisation resides.
 *
 * @return Full directory path.
 */
QString appLocalisationDir(void);

/*!
 * @brief Returns the path to directory where Qt localisation resides.
 *
 * @return Full directory path.
 */
QString qtLocalisationDir(void);

/*!
 * @brief Text files supplied with the application.
 */
enum TextFile {
	TEXT_FILE_LICENCE = 1 /*!< License file. */
};

/*!
 * @brief Return default installation location of text file.
 *
 * @param[in] textFile Text file identifier.
 */
QString expectedTextFilePath(enum TextFile textFile);

/*!
 * @brief Returns the content of the supplied text file.
 *
 * @param[in] textFile Text file identifier.
 */
QString suppliedTextFileContent(enum TextFile textFile);
