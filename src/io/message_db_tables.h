/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/io/sqlite/table.h"

/*
 * Message database table descriptions.
 */
extern SQLiteTbl msgsTbl; /*!< Table 'messages'. */
extern SQLiteTbl flsTbl; /*!< Table 'files'. */
extern SQLiteTbl hshsTbl; /*!< Table 'hashes'. */
extern SQLiteTbl evntsTbl; /*!< Table 'events'. */
extern SQLiteTbl prcstTbl; /*!< Table 'process_state'. */
extern SQLiteTbl rwmsgdtTbl; /*!< Table 'raw_message_data'. */
extern SQLiteTbl rwdlvrinfdtTbl; /*!< Table 'raw_delivery_info_data'. */
extern SQLiteTbl smsgdtTbl; /*!< Table 'supplementary_message_data'. */
extern SQLiteTbl crtdtTbl; /*!< Table 'certificate_data'. */
extern SQLiteTbl msgcrtdtTbl; /*!< Table 'message_certificate_data'. */
