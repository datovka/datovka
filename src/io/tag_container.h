/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

namespace Json {
	class Int64StringList; /* Forward declaration. */
	class TagAssignmentCommand; /* Forward declaration. */
	class TagAssignmentHash; /* Forward declaration. */
	class TagAssignmentList; /* Forward declaration. */
	class TagEntryList; /* Forward declaration. */
	class TagIdToAssignmentCountHash; /* Forward declaration. */
	class TagMsgIdList; /* Forward declaration. */
	class TagTextSearchRequestList; /* Forward declaration. */
	class TagTextSearchRequestToIdenfifierHash; /* Forward declaration. */
}
class TagClient; /*!< Forward declaration. */
class TagDb; /*!< Forward declaration. */

/*!
 * @brief Tag container.
 *     Encapsulates the functionality of the tag database and the tag client.
 */
class TagContainer : public QObject {
	Q_OBJECT
#if (QT_VERSION >= QT_VERSION_CHECK(5, 13, 0))
	Q_DISABLE_COPY_MOVE(TagContainer)
#else /* < Qt-5.13 */
	Q_DISABLE_COPY(TagContainer)
#endif /* >= Qt-5.13 */

public:
	/*!
	 * @brief Tag client back-end enumeration type.
	 */
	enum Backend {
		BACK_DB = 0, /*!< Local tag database. */
		BACK_CLIENT /*!< Server client. */
	};

	/*!
	 * @brief Constructor.
	 */
	TagContainer(enum Backend backend);

	/*!
	 * @brief Destructor.
	 */
	~TagContainer(void);

	/*
	 * TODO -- conversion.
	 */

	/*!
	 * @brief Get back end.
	 *
	 * @return Used backed.
	 */
	enum Backend backend(void) const;

	/*!
	 * @brief Set back end.
	 *
	 * @param[in] backend Back end to be used.
	 */
	void selectBackend(enum Backend backend);

	/*!
	 * @brief Access tag database back end.
	 *
	 * @return Pointer to tag database.
	 */
	TagDb *backendDb(void);

	/*!
	 * @brief Access tag client back end.
	 *
	 * @return Tag client back end.
	 */
	TagClient *backendClient(void);

	/*!
	 * @brief Check whether active back end can respond to requests.
	 *
	 * @return True if tag database is active or
	 *     if tag client is active and connected.
	 */
	bool isResponding(void);

	/*!
	 * @brief Insert new tags into database file.
	 *
	 * @note Emits tagsInserted() when new tags inserted.
	 *
	 * @param[in] entries Tag entries. The entry identifier values are ignored.
	 * @return True on success, false on any error.
	 */
	bool insertTags(const Json::TagEntryList &entries);

	/*!
	 * @brief Update tags in database file.
	 *
	 * @note Emits tagsUpdated() when tag updated.
	 *
	 * @param[in] entries List of entries. Id values must be set to proper
	 *                    values.
	 * @return True on success, false on any error.
	 */
	bool updateTags(const Json::TagEntryList &entries);

	/*!
	 * @brief Delete tags from database file.
	 *
	 * @note Emits tagsDeleted() when tag deleted.
	 *
	 * @param[in] ids List of tag identifiers.
	 * @return True on success, false on any error.
	 */
	bool deleteTags(const Json::Int64StringList &ids);

	/*!
	 * @brief Get tag data from database file.
	 *
	 * @param[in] ids List of tag identifiers.
	 * @param[out] entries List of entries.
	 * @return True on success, false on any error.
	 */
	bool getTagData(const Json::Int64StringList &ids,
	    Json::TagEntryList &entries);

	/*!
	 * @brief Get all tags from database file.
	 *
	 * @param[out] entries List of entries.
	 * @return True on success, false on any error.
	 */
	bool getAllTags(Json::TagEntryList &entries);

	/*!
	 * @brief Get numbers of assignments of given tags.
	 *
	 * @param[in] ids List of tag identifiers.
	 * @param[out] assignmentCounts Numbers of messages the tags are
	 *                              assigned to. Empty map on any error.
	 * @return Error code.
	 */
	bool getTagAssignmentCounts(const Json::Int64StringList &ids,
	    Json::TagIdToAssignmentCountHash &assignmentCounts);

	/*!
	 * @brief Get all tags related to given message.
	 *
	 * @param[in] msgIds List of message identifiers.
	 * @param[out] assignments Found entries are set. If no entries are
	 *                         found for given message then no key and value
	 *                         pair is stored.
	 * @return True on success, false on any error.
	 */
	bool getMessageTags(const Json::TagMsgIdList &msgIds,
	    Json::TagAssignmentHash &assignments);

	/*!
	 * @brief Delete all tags for message identifiers
	 *        in message_tags table.
	 *
	 * @note Emits tagAssignmentChanged() when tag assignment changes.
	 *
	 * @param[in] msgIds List of message identifiers.
	 * @return True on success, false on any error.
	 */
	bool removeAllTagsFromMsgs(const Json::TagMsgIdList &msgIds);

	/*!
	 * @brief Assign existing tags to messages.
	 *
	 * @note Emits tagAssignmentChanged() when tag assignment changes.
	 *
	 * @param[in] assignments Contains tag identifiers to be assigned to
	 *                        respective messages.
	 * @return True on success, false on any error.
	 */
	bool assignTagsToMsgs(const Json::TagAssignmentCommand &assignments);

	/*!
	 * @brief Remove tags from messages.
	 *
	 * @note Emits tagAssignmentChanged() when tag assignment changes.
	 *
	 * @param[in] assignments Contains tag identifiers to be removed from
	 *                        respective messages.
	 * @return True on success, false on any error.
	 */
	bool removeTagsFromMsgs(const Json::TagAssignmentCommand &assignments);

	/*!
	 * @brief Get message IDs related to tags containing sought text.
	 *
	 * @param[in] requests List of text and search type entries.
	 * @param[out] responses List of message identifiers associated to requests.
	 * @return True on success, false on any error.
	 */
	bool getMsgIdsContainSearchTagText(
	    const Json::TagTextSearchRequestList &requests,
	    Json::TagTextSearchRequestToIdenfifierHash &responses);

signals:
	/*!
	 * @brief Emitted when client connected to server.
	 */
	void connected(void);

	/*!
	 * @brief Emitted when client disconnected from the server.
	 */
	void disconnected(void);

	/*!
	 * @brief Emitted on significant client change.
	 */
	void reset(void);

	/*!
	 * @brief Emitted when new tags have been inserted into the database.
	 *
	 * @param[in] entries Newly created tag entries.
	 */
	void tagsInserted(const Json::TagEntryList &entries);

	/*!
	 * @brief Emitted when tags have been modified in the database.
	 *
	 * @param[in] entries Modified created tag entries.
	 */
	void tagsUpdated(const Json::TagEntryList &entries);

	/*!
	 * @brief Emitted when tags have been deleted from the database.
	 *
	 * @param[in] ids Deleted tag identifiers.
	 */
	void tagsDeleted(const Json::Int64StringList &ids);

	/*!
	 *  @brief Emitted when tag to message assignment has changed.
	 *
	 *  @param[in] assignments List of modified tag assignments.
	 */
	void tagAssignmentChanged(const Json::TagAssignmentList &assignments);

private slots:
	/*!
	 * @brief Action when tag client is connected.
	 *
	 * @note Emits connected().
	 */
	void watchConnected(void);

	/*!
	 * @brief Action when tag client is disconnected.
	 *
	 * @note Emits disconnected().
	 */
	void watchDisconnected(void);

	/*!
	 * @brief Action when tag client is reset.
	 *
	 * @note Emits reset().
	 */
	void watchReset(void);

	/*!
	 * @brief Action when tags are inserted.
	 *
	 * @param[in] entries Inserted tag entries.
	 */
	void watchTagsInserted(const Json::TagEntryList &entries);

	/*!
	 * @brief Action when tags are updated.
	 *
	 * @param[in] entries Updated tag entries.
	 */
	void watchTagsUpdated(const Json::TagEntryList &entries);

	/*!
	 * @brief Action when tags are deleted.
	 *
	 * @param[in] ids Deleted tag identifiers.
	 */
	void watchTagsDeleted(const Json::Int64StringList &ids);

	/*!
	 * @brief Action when tag assignment changes.
	 *
	 * @param[in] assignments Updated tag assignments.
	 */
	void watchTagAssignmentChanged(const Json::TagAssignmentList &assignments);

private:
	enum Backend m_backend; /*!< Back-end in use. */
	TagDb *m_tagDb; /*!< Pointer to tag database. */
	TagClient *m_tagClient; /*!< Pointer to tag client. */
};
