/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

namespace Isds {
	/* Forward class declaration. */
	class Envelope;
}

/* Default file name formats of saved/exported files. */
#define DEFAULT_TMP_FORMAT "%<AY>-%<AM>-%<AD>_%<EMID>_tmp"
#define DEFAULT_MESSAGE_FILENAME_FORMAT "%<MO>-%<EMID>"
#define DEFAULT_ACCEPTANCE_INFO_FILENAME_FORMAT "DD-%<EMID>"
#define DEFAULT_ATTACHMENT_FILENAME_FORMAT "%<FN>"
#define DEFAULT_ATTACHMENT_ACCEPTANCE_FORMAT "DD-%<EMID>-%<FN>"

/*!
 * @brief Create partial file path according to the format string.
 *
 * @note The format string knows these attributes:
 *     "%<AN>", "%<AB>", "%<AU>", "%<AY>", "%<AM>", "%<AD>", "%<Ah>", "%<Am>",
 *     "%<DY>", "%<DM>", "%<DD>", "%<Dh>", "%<Dm>", "%<EMID>", "%<EMS>", "%<MO>"
 *     "%<ESB>", "%<ESN>", "%<ESA>", "%<ERB>", "%<ERN>", "%<ERA>"
 *     "%<ESR>", "%<ESF>", "%<ERR>", "%<ERF>", "%<ETH>", "%<FN>"
 * @note The format also string knows these obsolete attributes:
 *     "%Y", "%M", "%D", "%h", "%m", "%n", "%i", "%s", "%S", "%R", "%d", "%u", "%f"
 *
 * @note Directory structure must be created explicitly.
 *
 * @param[in] format Format string, DEFAULT_TMP_FORMAT is used when empty.
 *                   Directory separators may be used here.
 * @param[in] prohibitDirSep True if directory separators should be prohibited.
 * @param[in] dmId Message id.
 * @param[in] dbId Data box identifier.
 * @param[in] userName User identifier (login).
 * @param[in] accountName Account name.
 * @param[in] attachName Attachment file name.
 * @param[in] envelope Message envelope.
 * @param[in] dmTypePrefix Message type prefix (ODZ/DDZ).
 * @param[in] suffix Predefined file suffix.
 * @return File name.
 */
QString fileSubPathFromFormat(QString format, bool prohibitDirSep, qint64 dmId,
    const QString &dbId, const QString &userName, QString accountName,
    const QString &attachName, const Isds::Envelope &envelope,
    const QString &dmTypePrefix, const QString &suffix);
