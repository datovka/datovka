/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QFileInfo>
#include <QProcess>

#include "src/datovka_shared/log/log.h"
#include "src/io/update.h"

bool Update::executeInstaller(const QString &pkgPath)
{
	debugFuncCall();

	if (Q_UNLIKELY(pkgPath.isEmpty())) {
		Q_ASSERT(0);
		return false;
	}

	/* Installation package. */
	const QFileInfo fi(pkgPath);
	if (Q_UNLIKELY(!fi.isFile())) {
		return false;
	}

#if defined(Q_OS_WIN)
	if (fi.suffix().toLower() == "msi") {
		const QString cmd = "msiexec.exe";
		const QStringList cmdArgs =
		    {"/i", QDir::toNativeSeparators(fi.absoluteFilePath())};
		logInfoNL("Executing command '%s %s'.", cmd.toUtf8().constData(),
		    cmdArgs.join(" ").toUtf8().constData());
		return QProcess::startDetached(cmd, cmdArgs);
	} else if (fi.suffix().toLower() == "exe") {
		const QString cmd = QDir::toNativeSeparators(
		    fi.absoluteFilePath());
		const QStringList cmdArgs; /* Empty. */
		logInfoNL("Executing command '%s %s'.", cmd.toUtf8().constData(),
		    cmdArgs.join(" ").toUtf8().constData());
		return QProcess::startDetached(cmd, cmdArgs);
	}
#endif /* Q_OS_WIN */

#if defined(Q_OS_MACOS)
	if (fi.suffix().toLower() == "dmg") {
		const QString cmd = "open";
		const QStringList cmdArgs =
		    {QDir::toNativeSeparators(fi.absoluteFilePath())};
		logInfoNL("Executing command '%s %s'.", cmd.toUtf8().constData(),
		    cmdArgs.join(" ").toUtf8().constData());
		return QProcess::startDetached(cmd, cmdArgs);
	}
#endif /* Q_OS_MACOS */

	logErrorNL("Unsupported installation package '%s'.",
	    pkgPath.toUtf8().constData());
	return false;
}
