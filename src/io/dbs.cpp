/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/io/dbs.h"

/*!
 * @brief Date/time format stored in db.
 *
 * @note Old implementation of Datovka is likely to contain a bug.
 * Milliseconds are probably stored as microseconds.
 */
static
const QString dbFaultyDateTimeFormat("yyyy-MM-dd HH:mm:ss.000zzz");
static
const QString dbDateTimeFormat("yyyy-MM-dd HH:mm:ss.zzz");
static
const QString dbShortDateTimeFormat("yyyy-MM-dd HH:mm:ss");
static
const QString dbDateFormat("yyyy-MM-dd");
static
const QString dbDateIsoFormat("yyyy-MM-ddThh:mm:ss.zzzZ");

QDateTime dateTimeFromDbFormat(const QString &dateTimeDbStr)
{
	QDateTime dateTime = QDateTime::fromString(dateTimeDbStr,
	    dbDateTimeFormat);
	if (dateTime.isNull() || !dateTime.isValid()) {
		/* Try the faulty format. */
		dateTime = QDateTime::fromString(dateTimeDbStr,
		    dbFaultyDateTimeFormat);
	}
	if (dateTime.isNull() || !dateTime.isValid()) {
		/* Try to ignore 3 rightmost characters. */
		dateTime = QDateTime::fromString(
		    dateTimeDbStr.left(dateTimeDbStr.size() - 3),
		    dbDateTimeFormat);
	}

	return dateTime;
}

QString qDateTimeToDbFormat(const QDateTime &dateTime)
{
	return (!dateTime.isNull()) ?
	    dateTime.toString(dbDateTimeFormat) + QStringLiteral("000") : QString();
}

QString dateTimeStrFromDbFormat(const QString &dateTimeDbStr,
    const QString &tgtFmt)
{
	QDateTime dateTime = dateTimeFromDbFormat(dateTimeDbStr);

	if (dateTime.isValid()) {
		return dateTime.toString(tgtFmt);
	} else {
		return QString();
	}
}

QDate dateFromDbFormat(const QString &dateDbStr)
{
	return QDate::fromString(dateDbStr, dbDateFormat);
}

QString qDateToDbFormat(const QDate &date)
{
	return (!date.isNull()) ? date.toString(dbDateFormat) : QString();
}

QString dateStrFromDbFormat(const QString &dateDbStr, const QString &tgtFmt)
{
	QDate date = dateFromDbFormat(dateDbStr);

	if (date.isValid()) {
		return date.toString(tgtFmt);
	} else {
		return QString();
	}
}
