/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */
#include <QList>
#include <QStringList> /* Cannot just declare QStringList because of Qt-6. */

class AcntId; /* Forward declaration. */
class QString; /* Forward declaration. */

class LoadMobileZipPackage {
	Q_DECLARE_TR_FUNCTIONS(DbConvertMobileToDesktop)

public:
	/*!
	 * @brief Load JSON file content from ZIP package and read archive info.
	 *
	 * @param[in] zipPath Full path to ZIP archive.
	 * @param[out] msgDbPathList Message database path list.
	 * @param[out] zfoDbPath ZFO database path.
	 * @param[out] infoText Error info text.
	 * @return True if ZIP archive is valid for conversion.
	 */
	static
	bool loadBackupJsonFromZip(const QString &zipPath,
	    QStringList &msgDbPathList, QString &zfoDbPath, QString &infoText);

	/*!
	 * @brief Unzip selected account files from ZIP package.
	 *
	 * @param[in] zipPath Full path to ZIP archive.
	 * @param[in] unZipPath Full path to unzip location.
	 * @param[in] selectedAcnts List of selected accounts.
	 * @param[out] msgDbPaths List of unzip message database paths.
	 * @param[out] zfoDbPath ZFO database unzip path.
	 * @return True if success.
	 */
	static
	bool unZipSelectedFilesFromZiP(const QString &zipPath,
	    const QString &unZipPath, const QList<AcntId> &selectedAcnts,
	    QStringList &msgDbPaths, QString &zfoDbPath);
};
