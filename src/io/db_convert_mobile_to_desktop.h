/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */
#include <QList>
#include <QStringList> /* Cannot just declare QStringList because of Qt-6. */

#include "src/datovka_shared/identifiers/account_id.h"

class QString; /* Forward declaration. */

/*!
 * @brief Provides message database conversions from mobile application.
 */
class DbConvertMobileToDesktop {
	Q_DECLARE_TR_FUNCTIONS(DbConvertMobileToDesktop)

public:
	/*!
	 * @brief Convert message database.
	 *
	 * @param[in] msgDbPath Message database path.
	 * @param[in] zfoDbPath ZFO database path.
	 * @param[out] errList Error list.
	 * @return True if success.
	 */
	static
	bool convertMsgDb(const QString &msgDbPath, const QString &zfoDbPath,
	    QStringList &errList);

	/*!
	 * @brief Get account id list from message databases.
	 *
	 * @param[in] msgDbs Message database list.
	 * @return Account id list.
	 */
	static
	QList<AcntId> getAcntIdsFromDbName(const QStringList &msgDbs);

	/*!
	 * @brief Split source message database by years.
	 *
	 * @param[in] msgDbPath Source message database path.
	 * @param[in] destPath Path where split databases are located.
	 * @return True if success.
	 */
	static
	bool splitDbByYears(const QString &msgDbPath, const QString &destPath);
};
