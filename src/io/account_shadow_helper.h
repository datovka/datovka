/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>

class AcntId; /* Forward declaration. */
class AccountsMap; /* Forward declaration. */

namespace ShadowHelper {

	/*!
	 * @brief Chekc whether account has list download privileges.
	 *
	 * @param[in] acntId Account identifier.
	 * @return True if requirements met.
	 */
	bool hasListDownloadPrivileges(const AcntId &acntId);

	/*!
	 * @brief Check whether account is suitable to being used for background
	 *     synchronisation.
	 *
	 * @note Currently it must:
	 *     have only privileges to download message lists - it may be able
	 *     to send messages, use password authentication and the password
	 *     must be stored in the settings within the container.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] shadowAccounts Shadow account credentials container.
	 * @return True is account satisfies all requirements.
	 */
	bool isUsableBackgroundAccount(const AcntId &acntId,
	    const AccountsMap &shadowAccounts);

	/*!
	 * @brief Find shadow accounts that are suitable to download message
	 *     lists without causing messages to be accepted for the supplied
	 *     regular account identifier.
	 *
	 * @param[in] rAcntId Regular account identifier.
	 * @param[in] regularAccounts Regular account container.
	 * @param[in] shadowAccount Shadow account container.
	 * @return List of valid shadow account identifiers if such accounts found,
	 *     empty list else.
	 */
	QList<AcntId> usableBackgroundAccounts(const AcntId &rAcntId,
	    const AccountsMap &regularAccounts,
	    const AccountsMap &shadowAccounts);

	/*!
	 * @brief Find regular accounts that can be served by the specified
	 *     shadow account for downloading message lists.
	 *
	 * @param[in] sAcntId Shadow account identifier.
	 * @param[in] regularAccounts Regular account container.
	 * @param[in] shadowAccount Shadow account container.
	 * @return List if valid regular account identifiers if such accounts found,
	 *     empty list else.
	 */
	QList<AcntId> availableForegroundAccounts(const AcntId &sAcntId,
	    const AccountsMap &regularAccounts,
	    const AccountsMap &shadowAccounts);

}
