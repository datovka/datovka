/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMap>
#include <QObject>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/io/message_db.h"
#include "src/io/message_db_set.h"

/*!
 * @brief Database container.
 *
 * @note The primary key is the username and whether it is a testing environment
 *     account.
 *
 * TODO -- Should there be a single globally accessible instance?
 *     (Actually no singleton.)
 */
class DbContainer : public QObject, private QMap<AcntId, MessageDbSet *> {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] onDisk Use false to hold databases in memory.
	 * @param[in] connectionPrefix Database connection prefix.
	 */
	explicit DbContainer(bool onDisk,
	    const QString &connectionPrefix = QString());

	/*!
	 * @brief Destructor.
	 */
	~DbContainer(void);

	/*!
	 * @brief Access dbSet that has already been opened for a given account.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Pointer to database, Q_NULLPTR if set does not exist.
	 */
	MessageDbSet *accessOpenedDbSet(const AcntId &acntId) const;

	/*!
	 * @brief Access/create+open message database set related to item.
	 *
	 * @param[in] locDir Directory where to search for the file.
	 * @param[in] acntId Account identifier.
	 * @param[in] organisation Way how the database is organised.
	 * @param[in] manner How to treat files when opening database.
	 * @return Pointer to database, Q_NULLPTR on error.
	 */
	MessageDbSet *accessDbSet(const QString &locDir,
	    const AcntId &acntId, MessageDbSet::Organisation organisation,
	    enum MessageDbSet::CreationManner manner);

	/*!
	 * @brief Delete all files related to dbset.
	 *
	 * @param dbSet Deleted database set.
	 * @return True on success.
	 */
	bool deleteDbSet(MessageDbSet *dbSet);

signals:
	/*!
	 * @brief Emitted when a database file has been added into a container.
	 *
	 * @note Added database may or may not be already opened.
	 *
	 * @param[in] primaryKey Primary key, usually the username.
	 * @param[in] testing Whether the database is related to a testing account.
	 */
	void madeAvailable(const AcntId &acntId);

	/*!
	 * @brief Emitted when a database file is opened.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void opened(const AcntId &acntId);

	/*!
	 * @brief Emitted when the locally read flag changes.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgIds Message identifiers.
	 * @param[in] read New locally read status.
	 */
	void changedLocallyRead(const AcntId &acntId,
	    const QList<MsgId> &msgIds, bool read);

	/*!
	 * @brief Emitted when the process state changes.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgIds Message identifiers.
	 * @param[in] state New message state.
	 */
	void changedProcessState(const AcntId &acntId,
	    const QList<MsgId> &msgIds, enum MessageDb::MessageProcessState state);

	/*!
	 * @brief Emitted when complete message written into the database.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgId Message identifier.
	 * @param[in] direction Sent or received messages
	 *                      (enum MessageDirection).
	 */
	void messageInserted(const AcntId &acntId, const MsgId &msgId,
	    int direction);

	/*!
	 * @brief Emitted when all message data deleted from database.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgId Message identifier.
	 */
	void messageDataDeleted(const AcntId &acntId, const MsgId &msgId);

private slots:
	/*!
	 * @brief This slot must be connected to every database set created
	 *     inside this container.
	 *
	 * @param[in] primaryKey Primary key, usually the username.
	 * @param[in] testing Whether the database is related to a testing account.
	 */
	void watchMadeAvailable(const QString &primaryKey, bool testing);

	/*!
	 * @brief This slot must be connected to every database set created
	 *     inside this container.
	 *
	 * @param[in] primaryKey Primary key, usually the username.
	 * @param[in] testing Whether the database is related to a testing account.
	 */
	void watchOpened(const QString &primaryKey, bool testing);

	/*!
	 * @brief This slot must be connected to every database set created
	 *     inside this container.
	 *
	 * @param[in] primaryKey Primary key, usually the username.
	 * @param[in] testing Whether the database is related to a testing account.
	 * @param[in] msgIds Message identifiers.
	 * @param[in] read New locally read status.
	 */
	void watchChangedLocallyRead(const QString &primaryKey, bool testing,
	    const QList<MsgId> &msgIds, bool read);

	/*!
	 * @brief This slot must be connected to every database set created
	 *     inside this container.
	 *
	 * @param[in] primaryKey Primary key, usually the username.
	 * @param[in] testing Whether the database is related to a testing account.
	 * @param[in] msgIds Message identifiers.
	 * @param[in] state New message state.
	 */
	void watchChangedProcessState(const QString &primaryKey, bool testing,
	    const QList<MsgId> &msgIds, enum MessageDb::MessageProcessState state);

	/*!
	 * @brief This slot must be connected to every database created inside
	 *     this container.
	 *
	 * @param[in] primaryKey Primary key, usually the username.
	 * @param[in] testing Whether the database is related to a testing account.
	 * @param[in] msgId Message identifier.
	 * @param[in] direction Sent or received messages
	 *                      (enum MessageDirection).
	 */
	void watchMessageInserted(const QString &primaryKey, bool testing,
	    const MsgId &msgId, int direction);

	/*!
	 * @brief This slot must be connected to every database created inside
	 *     this container.
	 *
	 * @param[in] primaryKey Primary key, usually the username.
	 * @param[in] testing Whether the database is related to a testing account.
	 * @param[in] msgId Message identifier.
	 */
	void watchMessageDataDeleted(const QString &primaryKey, bool testing,
	    const MsgId &msgId);

private:
	/*!
	 * @brief Database driver name.
	 */
	static
	const QString dbDriverType;

	const bool m_onDisk; /*!<
	                      * True if databases are generated within a file system,
	                      * false if they are held within memory.
	                      */

	const QString m_connectionPrefix; /*!< Database connection prefix. */
};
