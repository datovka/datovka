/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/isds/message_interface.h"

class MessageDb; /* Forward declaration. */
class MsgId; /* Forward declaration. */
class QString; /* Forward declaration. */

/*!
 * Encapsulates only a single database file.
 */
class MessageDbSingle {

private:
	/*!
	 * @brief Constructor.
	 *
	 * @note This container ignores file naming conventions.
	 */
	MessageDbSingle(void);

public:
	/*!
	 * @brief Destructor.
	 */
	~MessageDbSingle(void);

	/*!
	 * @brief Creates new object.
	 *
	 * @param[in] filePath         Full path to the database file.
	 * @param[in] connectionPrefix Prefix of the connection name.
	 * @return Pointer to new container or Q_NULLPTR on error.
	 */
	static
	MessageDbSingle *createNew(const QString &filePath,
	    const QString &connectionPrefix);

	/*!
	 * @brief Return all message ID from database.
	 *
	 * @return Message id set.
	 */
	QSet<MsgId> getAllMsgIds(void) const;

	/*!
	 * @brief Test if imported message is relevant to account.
	 *
	 * @param[in] dmId Message id.
	 * @param[in] dbId Databox id where message should be imported.
	 * @return True if message is relevant for import.
	 */
	bool isRelevantMsgForImport(qint64 msgId, const QString &dbId) const;

	/*!
	 * @brief Return message type (sent or received).
	 *
	 * @param[in] dmId Message id.
	 * @return Message type value, negative value -1 on error.
	 */
	int getMessageType(qint64 dmId);

	/*!
	 * @brief Insert or replace complete message data including raw data,
	 *     attachments, certificate info, etc.
	 *
	 * @note Emits messageInserted() when successful.
	 * @note This operation uses transactions and performs a roll-back on
	 *     failure.
	 *
	 * @param[in] storeRawInDb Whether to store raw data in database file.
	 * @param[in] msg Complete message.
	 * @param[in] msgDirect Message orientation.
	 * @param[in] verified Whether the message has been verified.
	 * @param[in] transaction True to create a transaction.
	 * @return True on success.
	 */
	bool insertOrReplaceCompleteMessage(bool storeRawInDb,
	    const Isds::Message &msg,
	    enum MessageDirection msgDirect, bool verified,
	    bool transaction);

	/*!
	 * @brief Insert/update raw (DER) delivery info into
	 *     raw_delivery_info_data.
	 *
	 * @param[in] storeRawInDb Whether to store raw data in database file.
	 * @param[in] dmId Message identifier.
	 * @param[in] raw Raw (in DER format) delivery information.
	 * @return True on success.
	 */
	bool insertOrReplaceDeliveryInfoRaw(bool storeRawInDb, qint64 dmId,
	    const QByteArray &raw);

private:
	MessageDb *m_db; /*!< The encapsulated database. */
};
