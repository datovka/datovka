/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/io/matomo_reporter.h"
#include "src/datovka_shared/json/report.h"

class StatsReporter {
public:
	StatsReporter(void);

	/*!
	 * @brief Escapes all double quotes.
	 *
	 * @param[in] str Input string (e.g. '{"a":"b"}')
	 * @returns String with escaped double quotes (e.g. '{\"a\":\"b\"}').
	 */
	static
	QString escapeQuotes(QString str);

	/*!
	 * @brief Return unique identifier.
	 *
	 * @note The identifier may not be invariable in time.
	 */
	static
	QString constructUniqueId(void);

	/*!
	 * @brief Construct a visit path that can be displayed as a page visit.
	 *
	 * @param[in] at Application type.
	 * @return Visit path that can be viewed at the collectors end point.
	 */
	static
	QString constructVisitPath(enum Json::Report::AppType appType);

	/*!
	 * @brief Report application stats.
	 *
	 * @param[in] at Application type.
	 * @param[in] mui Unique persistent identifier.
	 * @param[in] path Visit path constructed by constructVisitPath().
	 * @param[in] usage Usage report, none will be sent when null object passed.
	 */
	void reportVisit(enum Json::Report::AppType at, const QString &mui,
	    const QString &path, Json::UsageReport usage);

private:
	MatomoReporter m_reporter; /*!< Communicates with the collector end point. */
};
