/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QMap>
#include <QPair>
#include <QString>
#include <QVector>

#include "src/io/tag_db_tables.h"

namespace DbInfoTbl {
	const QString tabName("_db_info");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"id", DB_INTEGER}, /* NOT NULL */
	{"entry_name", DB_TEXT}, /* NOT_NULL */
	{"entry_json", DB_TEXT}
	/*
	 * PRIMARY KEY (id),
	 * UNIQUE (entry_name)
	 */
	};

	const QMap<QString, QString> colConstraints = {
	    {"id", "NOT NULL"},
	    {"entry_name", "NOT NULL"}
	};

	const QString &tblConstraint(
	    ",\n"
	    "        CONSTRAINT pk_id PRIMARY KEY (id),\n"
	    "        CONSTRAINT uq_entry_name UNIQUE (entry_name)"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	{"id",       {DB_INTEGER, ""}},
	{"entry_name", {DB_TEXT, ""}},
	{"entry_json", {DB_TEXT, ""}}
	};
} /* namespace DbInfoTbl */
SQLiteTbl _dbInfoTbl(DbInfoTbl::tabName, DbInfoTbl::knownAttrs,
    DbInfoTbl::attrProps, DbInfoTbl::colConstraints,
    DbInfoTbl::tblConstraint);

namespace TagTbl {
	const QString tabName("tags");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"id", DB_INTEGER}, /* NOT NULL */
	{"tag_name", DB_TEXT}, /* NOT NULL */
	{"tag_color", DB_TEXT},
	{"tag_description", DB_TEXT}
	/*
	 * PRIMARY KEY (id),
	 * UNIQUE (tag_name)
	 */
	};

	const QMap<QString, QString> colConstraints = {
	    {"id", "NOT NULL"},
	    {"tag_name", "NOT NULL"}
	};

	const QString &tblConstraint(
	    ",\n"
	    "        CONSTRAINT pk_id PRIMARY KEY (id),\n"
	    "        CONSTRAINT uq_tag_name UNIQUE (tag_name)"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	{"id",       {DB_INTEGER, ""}},
	{"tag_name", {DB_TEXT, ""}},
	{"tag_color", {DB_TEXT, ""}},
	{"tag_description", {DB_TEXT, ""}}
	};
} /* namespace TagTbl */
SQLiteTbl tagsTbl(TagTbl::tabName, TagTbl::knownAttrs,
    TagTbl::attrProps, TagTbl::colConstraints,
    TagTbl::tblConstraint);

namespace MsgTagsTbl {
	const QString tabName("message_tags");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"id", DB_INTEGER}, /* NOT NULL */
	{"test_env", DB_INTEGER}, /* NOT NULL */
	{"message_id", DB_INTEGER}, /* NOT NULL */
	{"tag_id", DB_INTEGER} /* NOT NULL */
	/*
	 * PRIMARY KEY (id),
	 * FOREIGN KEY(tag_id) REFERENCES tags ("id")
	 */
	};

	const QMap<QString, QString> colConstraints = {
	    {"id", "NOT NULL"},
	    {"test_env", "NOT NULL"},
	    {"message_id", "NOT NULL"},
	    {"tag_id", "NOT NULL"}
	};

	const QString &tblConstraint(
	    ",\n"
	    "        CONSTRAINT pk_id PRIMARY KEY (id),\n"
	    "        CONSTRAINT fk_tag_id FOREIGN KEY(tag_id) REFERENCES tags (id) ON DELETE CASCADE,\n"
	    "        CONSTRAINT uq_test_env_message_id_tag_id UNIQUE (test_env, message_id, tag_id),\n"
	    "        CONSTRAINT ck_test_env CHECK (test_env IN (0, 1))"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	{"id",           {DB_INTEGER, ""}},
	{"test_env",     {DB_INTEGER, ""}},
	{"message_id",   {DB_INTEGER, ""}},
	{"tag_id",       {DB_INTEGER, ""}}
	};
} /* namespace MsgTagsTbl */
SQLiteTbl msgtagsTbl(MsgTagsTbl::tabName, MsgTagsTbl::knownAttrs,
    MsgTagsTbl::attrProps, MsgTagsTbl::colConstraints,
    MsgTagsTbl::tblConstraint);
