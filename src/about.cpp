/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <cstdlib> /* ::std::free */
#include <libdatovka/isds.h>
#include <openssl/crypto.h> /* SSLeay_version(3), OpenSSL_version(3) */
#include <openssl/opensslv.h>

#include "src/about.h"

QStringList libraryDependencies(void)
{
	QStringList libs;

	libs.append(QStringLiteral("Qt ") + qVersion());

	char *isdsVer = isds_version();
	libs.append(QStringLiteral("libdatovka ") + isdsVer);
	::std::free(isdsVer); isdsVer = NULL;

	libs.append(
#if OPENSSL_VERSION_NUMBER < 0x10100000L
	    SSLeay_version(SSLEAY_VERSION)
#else /* OPENSSL_VERSION_NUMBER >= 0x10100000L */
	    OpenSSL_version(OPENSSL_VERSION)
#endif /* OPENSSL_VERSION_NUMBER < 0x10100000L */
	);

	return libs;
}
