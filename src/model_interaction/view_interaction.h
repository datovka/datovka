/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractItemView>
#include <QModelIndex>

class QAbstractProxyModel; /* Forward declaration. */

/*!
 * @brief Provides a namespace for convenience functions dealing with the
 *     attachment model.
 */
namespace ViewInteraction {

	/*!
	 * @brief Returns list of indexes into selected rows.
	 *
	 * @param[in] view View to ask for selected indexes.
	 * @param[in] column Number of column to receive indexes with.
	 * @return List of indexes.
	 */
	static inline
	QModelIndexList selectedRows(const QAbstractItemView &view,
	    int column)
	{
		return view.selectionModel()->selectedRows(column);
	}

	/*!
	 * @brief Obtain list of selected rows.
	 *
	 * @param[in] view View from which to use the selection model.
	 * @param[in] proxyModel Proxy model.
	 * @param[in] column Number of column to load indexes.
	 * @return List of row numbers.
	 */
	QList<int> selectedSrcRowNums(const QAbstractItemView &view,
	    QAbstractProxyModel &proxyModel, int column);

}
