/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QAbstractProxyModel>

#include "src/model_interaction/view_interaction.h"

QList<int> ViewInteraction::selectedSrcRowNums(const QAbstractItemView &view,
    QAbstractProxyModel &proxyModel, int column)
{
	QList<int> rowList;

	QModelIndexList selIdxs = view.selectionModel()->selectedRows(column);

	/* Translate to underlying model. */
	for (const QModelIndex &idx : selIdxs) {
		QModelIndex srcIdx = proxyModel.mapToSource(idx);
		if (Q_UNLIKELY(!srcIdx.isValid())) {
			Q_ASSERT(0);
			continue;
		}
		rowList.append(srcIdx.row());
	}

	return rowList;
}
