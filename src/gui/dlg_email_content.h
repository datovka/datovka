/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>
#include <QList>

class MsgOrigin; /* Forward declaration. */
class Prefs; /* Forward declaration. */

namespace Ui {
	class DlgEmailContent;
}

/*!
 * @brief Provides interface for email creation with attachments.
 */
class DlgEmailContent : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent widget.
	 */
	explicit DlgEmailContent(QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	virtual
	~DlgEmailContent(void);

	/*!
	 * @brief Create email.
	 *
	 * @param[in] originList List of messages to be added into email.
	 * @param[in] parent Parent object.
	 */
	static
	void createEmail(const QList<MsgOrigin> &originList,
	    QWidget *parent = Q_NULLPTR);

private slots:
	/*!
	 * @brief Activates confirmation button.
	 */
	void enableOkButton(void);

	/*!
	 * @brief Watch changes in preferences. Handle configuration signals.
	 *
	 * @param[in] valueType Utilises enum PrefsDb::ValueType values.
	 * @param[in] name Entry name.
	 * @param[in] value Entry value holding the respective type.
	 */
	void watchPrefsModification(int valueType, const QString &name,
	    const QVariant &value);

private:
	/*!
	 * @brief Load control elements from settings.
	 *
	 * @param[in] prefs Preferences.
	 */
	void loadWindowControls(const Prefs &prefs);

	/*!
	 * @brief Store control element settings.
	 *
	 * @param[out] prefs Preferences.
	 */
	void saveWindowControls(Prefs &prefs) const;

	Ui::DlgEmailContent *m_ui; /*!< UI generated from UI file. */
};
