/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMessageBox>

/*!
 * @brief Non-modal dialogues.
 */
namespace DlgMsgBoxNonModal {

	/*!
	 * @brief Display a non-modal critical message window.
	 *
	 * @param[in] parent Parent widget.
	 * @param[in] title Window title.
	 * @param[in] text Message text.
	 * @param[in] buttons Window buttons.
	 * @param[in] defaultButton Default button.
	 */
	void critical(QWidget *parent, const QString &title,
	    const QString &text,
	    QMessageBox::StandardButtons buttons = QMessageBox::Ok,
	    enum QMessageBox::StandardButton defaultButton = QMessageBox::NoButton);

	/*!
	 * @brief Display a non-modal information window.
	 *
	 * @param[in] parent Parent widget.
	 * @param[in] title Window title.
	 * @param[in] text Message text.
	 * @param[in] buttons Window buttons.
	 * @param[in] defaultButton Default button.
	 */
	void information(QWidget *parent, const QString &title,
	    const QString &text,
	    QMessageBox::StandardButtons buttons = QMessageBox::Ok,
	    enum QMessageBox::StandardButton defaultButton = QMessageBox::NoButton);

	/*!
	 * @brief Display a non-modal warning window.
	 *
	 * @param[in] parent Parent widget.
	 * @param[in] title Window title.
	 * @param[in] text Message text.
	 * @param[in] buttons Window buttons.
	 * @param[in] defaultButton Default button.
	 */
	void warning(QWidget *parent, const QString &title, const QString &text,
	    QMessageBox::StandardButtons buttons = QMessageBox::Ok,
	    enum QMessageBox::StandardButton defaultButton = QMessageBox::NoButton);

}
