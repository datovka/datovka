/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QWidget>

/* Forward class declaration. */
class AcntData;
class IsdsSessions;
class QMessageBox;

namespace Gui {

	/*!
	 * @brief Encapsulates the GUI-based log-in procedure.
	 */
	class LogInCycle : public QObject {
		Q_OBJECT
	public:
		/*!
		 * @brief Constructor.
		 *
		 * @param[in] syncAllActionName Name of the synchronise all accounts action.
		 * @param[in] parent Parent widget.
		 */
		explicit LogInCycle(const QString &syncAllActionName,
		    QWidget *parent = Q_NULLPTR);

		/*!
		 * @brief Destructor.
		 */
		~LogInCycle(void);

		/*!
		 * @brief Performs a ISDS log-in operation.
		 *
		 * @param[in,out] isdsSessions Sessions container reference.
		 * @param[in,out] acntSettings Account settings reference.
		 * @return True when logged in successfully.
		 */
		bool logIn(IsdsSessions &isdsSessions, AcntData &acntSettings);

	signals:
		/*!
		 * @brief Emitted during log-in procedure.
		 *
		 * @param[in] message Brief status description.
		 */
		void logInStatusMessage(const QString &message);

	private slots:
		/*!
		 * @brief Sets the cancel flag.
		 */
		void cancelLoop(void);

	private:
		const QString m_syncAllActionName; /*!< Name of the synchronise all accounts action. */
		bool m_cancel; /*!< Cancel flag. */
		QMessageBox *m_waitMepLogin; /*!< Waiting for MEP acknowledgement notification. */
	};

}
