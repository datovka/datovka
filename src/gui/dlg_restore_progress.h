/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>

/* Forward declaration. */
class RestoreWorker;
class QThread;

namespace Ui {
	class DlgBackupProgress;
}

/*!
 * @brief Dialogue for the restoration progress visualisation.
 */
class DlgRestoreProgress : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit DlgRestoreProgress(QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	virtual
	~DlgRestoreProgress(void);

	/*!
	 * @brief Show progress dialogue and preform restoration operation.
	 *
	 * @param[in] parent Parent object.
	 */
	static
	void restore(QWidget *parent = Q_NULLPTR);

public slots:
	/*!
	 * @brief Execute the back-up operation and show modal dialogue.
	 */
	virtual
	int exec(void) Q_DECL_OVERRIDE;

private slots:
	/*!
	 * @brief Set progress bar boundaries.
	 *
	 * @param[in] min Progress bar minimum.
	 * @param[in] max Progress bar maximum.
	 */
	void setupProgress(int min, int max);

	/*!
	 * @brief Set a label containing a brief task description.
	 *
	 * @param[in] name Task name.
	 */
	void displayWork(const QString &name);

	/*!
	 * @brief Signalises to the worker thread that it should finish.
	 */
	void cancelRestoration(void);

	/*!
	 * @brief Display a warning modal dialogue window.
	 *
	 * @param[in] title Window title.
	 * @param[in] text Warning text.
	 */
	void showWarning(const QString &title, const QString &text);

	/*!
	 * @brief Show success dialogue and exit.
	 */
	void successNotification(void);

protected:
	/*!
	 * @brief Abort restoration and close.
	 *
	 * @param[in,out] event Caught close event.
	 */
	virtual
	void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

private:
	/*!
	 * @brief Initialise the dialogue.
	 */
	void initDialogue(void);

	Ui::DlgBackupProgress *m_ui; /*!< UI generated from UI file. */

	QThread *m_restoreThread; /*!< Thread to run the restoration operation in. */
	RestoreWorker *m_restoreWorker; /*!< Restoration operation object. */
};
