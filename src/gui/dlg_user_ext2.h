/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>

#include "src/datovka_shared/isds/types.h"
#include "src/models/combo_box_model.h"

namespace Isds {
	class DbOwnerInfoExt2;
	class DbUserInfoExt2;
}

namespace Ui {
	class DlgUserExt2;
}

/*!
 * @brief UserExt2 dialogue.
 */
class DlgUserExt2 : public QDialog {
	Q_OBJECT

public:
	/*!
	 * @brief Specifies which form fields should be enabled.
	 */
	enum EditableField {
		EC_NONE = 0x00, /*!< Convenience value. */
		EC_NAME_BIRTH = 0x01, /*!< Person name and birth date. */
		EC_ADDR = 0x02, /*!< Residence address. */
		EC_CA_ADDR = 0x04, /*!< Contact address. */
		EC_USER_TYPE = 0x08, /*!< User type. */
		EC_PERMISSIONS = 0x10, /*!< Permissions. */
		EC_ALL = 0x1f /*!< Convenience value. */
	};
	Q_DECLARE_FLAGS(EditableFields, EditableField)
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_FLAG(EditableFields)
#else /* < Qt-5.5 */
	Q_FLAGS(EditableFields)
#endif /* >= Qt-5.5 */

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] user User info.
	 * @param[in] owner Owner info.
	 * @param[in] fields Specifies editable fields.
	 * @param[in] parent Parent object.
	 */
	explicit DlgUserExt2(const Isds::DbUserInfoExt2 &user,
	    const Isds::DbOwnerInfoExt2 &owner, EditableFields fields = EC_ALL,
	    QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgUserExt2(void);

	/*!
	 * @brief Edit user info.
	 *
	 * @param[in,out] user User info.
	 * @param[in] owner Owner info.
	 * @param[in] fields Specifies editable fields.
	 * @param[in] parent Parent object.
	 */
	static
	bool edit(Isds::DbUserInfoExt2 &user,
	    const Isds::DbOwnerInfoExt2 &owner, EditableFields fields = EC_ALL,
	    QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Check whether umber could be an evidence number.
	 *
	 * @note Evidence numbers are sometimes written with starting
	 *      letter 'e' or 'E'.
	 *
	 * @param[in] no Number in municipality.
	 * @return True if it could be an evidence number.
	 */
	static
	bool isEvidenceNumber(const QString &no);

	/*!
	 * @brief Strip the auxiliary evidence number identifier.
	 *
	 * @note Strips leading letter 'e' or 'E'.
	 *
	 * @param[in] no Number in municipality.
	 * @return Number without leading letter 'e' or leave number unchanged
	 *     if no such letter found.
	 */
	static
	QString stripEvidenceNumber(const QString &no);

private slots:
	/*!
	 * @brief Mutually disable the conscription and evidence number fields
	 *     if other is non-empty.
	 */
	void excludeConscriptionVsEvidenceFileds(void);

	/*!
	 * @brief Set contact address visibility.
	 *
	 * @param[in] checked False if the form should be visible, true if visible.
	 */
	void toggleCaAddressNone(bool checked);

	/*!
	 * @brief Disable contact address activity.
	 *
	 * @param[in] checked When true then the form is disabled.
	 */
	void toggleCaAddressFirm(bool checked);

	/*!
	 *  @brief Enable contact address activity.
	 *
	 * @param[in] checked When true then the form is enabled.
	 */
	void toggleCaAddressCustom(bool checked);

private:
	/*!
	 * @brief Disables parts of the window functionality.
	 *
	 * @param[in] fields Specifies which fields should be left enabled.
	 */
	void enableContent(EditableFields fields);

	/*!
	 * @brief Load window content according to supplied data.
	 *
	 * @param[in] user User info.
	 */
	void loadContent(const Isds::DbUserInfoExt2 &user);

	/*!
	 * @brief Collect window content.
	 *
	 * @param[in,out] user User info to be set according to window content.
	 *                     Some values re left untouched.
	 */
	void collectContent(Isds::DbUserInfoExt2 &user) const;

	Ui::DlgUserExt2 *m_ui; /*!< UI generated from UI file. */

	const Isds::DbOwnerInfoExt2 &m_owner; /*!< Box owner info. */

	CBoxModel m_statesCBoxModel; /*!< List of states combo box model. */
	CBoxModel m_userTypeCBoxModel; /*!< List of user type combo box model. */
	bool m_preferStateCBox; /*!< True if state combo box should be preferred over line edit. */
	bool m_preferCaStateCBox; /*!< True if caState combo box should be preferred over line edit. */
};

Q_DECLARE_OPERATORS_FOR_FLAGS(DlgUserExt2::EditableFields)
