/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>

#include "src/identifiers/account_id_db.h"
#include "src/models/restore_selection_model.h"

namespace Ui {
	class DlgRestore;
}

/*!
 * @brief Dialogue fro the restore operation.
 *
 * @note The restoration should be performed after all workers have been
 *     finished and when the main window is not active. Therefore use the
 *     \a callRestore variable to start the process after the main window
 *     has finished.
 */
class DlgRestore : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntIdDbList List of available accounts.
	 * @param[in] parent Parent object.
	 */
	explicit DlgRestore(const QList<AcntIdDb> &acntIdDbList,
	     QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Global variable used to detect planned restoration process.
	 *
	 * @note It is a nested class so it cannot be declared.
	 */
	static
	void *restoreDescr;

	/*!
	 * @brief Destructor.
	 */
	~DlgRestore(void);

	/*!
	 * @brief View restore operation dialogue.
	 *
	 * @note If true is returned then the main window should be existed and
	 *     the restoration process should be performed.
	 *
	 * @param[in] acntIdDbList List of available accounts.
	 * @param[in] parent Parent object.
	 * @return True if a restoration action has been planned.
	 */
	static
	bool restore(const QList<AcntIdDb> &acntIdDbList,
	    QWidget *parent = Q_NULLPTR);

protected:
	/*!
	 * @brief Check window geometry and set table columns width.
	 *
	 * @param[in] event Widget show event.
	 */
	virtual
	void showEvent(QShowEvent *event) Q_DECL_OVERRIDE;

private slots:
	/*!
	 * @brief Choose back-up data location.
	 */
	void chooseJsonFile(void);

	/*!
	 * @brief Update message selection.
	 *
	 * @param[in] topLeft Modified data location.
	 * @param[in] bottomRight Modified data location.
	 */
	void modifiedMessageSelection(const QModelIndex &topLeft,
	    const QModelIndex &bottomRight);

	/*!
	 * @brief Update tag selection.
	 *
	 * @param[in] state New state value.
	 */
	void modifiedTagSelection(int state);

	/*!
	 * @brief Activates confirmation button.
	 */
	void enableOkButton(void);

	/*!
	 * @brief Ask whether the proceed with the restoration process.
	 */
	void askConfirmation(void);

private:
	/*!
	 * @brief Initialise the dialogue.
	 */
	void initDialogue(void);

	/*!
	 * @brief Checks amount of available space and displays a warning.
	 *
	 * @note This method required Qt-5.4 and later to work.
	 *
	 * @return False if not enough space was detected.
	 */
	bool checkAvailableSpace(void);

	/*!
	 * @brief Collect restoration description.
	 *
	 * @return Pointer to newly allocated object.
	 */
	void *collectRestorationDescr(void) const;

	Ui::DlgRestore *m_ui; /*!< UI generated from UI file. */

	const QList<AcntIdDb> m_acntIdDbList; /*!< List of available accounts. */

	RestoreSelectionModel m_restoreSelectionModel; /*!< Choose account data to restore. */
};
