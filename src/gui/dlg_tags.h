/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>
#include <QHash>
#include <QList>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/json/basic.h" /* Json::Int64StringList is used in slots. Q_MOC_INCLUDE */
#include "src/delegates/tags_delegate.h"
#include "src/json/tag_assignment.h" /* Json::TagAssignmentList is used in slots. Q_MOC_INCLUDE */
#include "src/json/tag_entry.h" /* Json::TagEntryList is used in slots. Q_MOC_INCLUDE */
#include "src/models/tags_model.h"

namespace Json {
	class TagMsgId; /* Forward declaration. */
}
class TagContainer; /* Forward declaration. */
class TagItemList; /* Forward declaration. */

namespace Ui {
	class DlgTags;
}

/*!
 * @brief Tags management dialogue.
 */
class DlgTags : public QDialog {
	Q_OBJECT

public:
	/*!
	 * Describes the action the dialogue has performed.
	 */
	enum ReturnCode {
		NO_ACTION, /*!< Nothing happened. */
		ASSIGMENT_CHANGED, /*!< Assignment of tags to messages have changed. */
		TAGS_CHANGED /*!< Actual tags have been deleted or changed. */
	};

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] tagCont Pointer to tag container.
	 * @param[in] msgIdList List of message IDs, empty list if no assignment
	 *                      should be edited.
	 * @param[in] parent Parent widget.
	 */
	explicit DlgTags(const AcntId &acntId, TagContainer *tagCont,
	    const QList<qint64> &msgIdList, QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgTags(void);

	/*!
	 * @brief Edit all tags.
	 *
	 * @param[in] tagCont Pointer to tag container.
	 * @param[in] parent Parent widget.
	 * @return Return code.
	 */
	static
	enum ReturnCode editAvailable(TagContainer *tagCont,
	    QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Edit assigned tags.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] tagCont Pointer to tag container.
	 * @param[in] msgIdList List of message IDs.
	 * @param[in] parent Parent widget.
	 * @return Return code.
	 */
	static
	enum ReturnCode editAssignment(const AcntId &acntId, TagContainer *tagCont,
	    const QList<qint64> &msgIdList, QWidget *parent = Q_NULLPTR);

private slots:
	/*!
	 * @brief Add tag (insert into database).
	 */
	void addTag(void);

	/*!
	 * @brief Update tag (update data in database).
	 */
	void updateTag(void);

	/*!
	 * @brief Delete tag (delete tag data from database).
	 */
	void deleteTag(void);

	/*!
	 * @brief Assign selected tag(s) to messages (insert into database).
	 */
	void assignSelectedTagsToMsgs(void);

	/*!
	 * @brief Remove selected tag(s) from messages
	 *        (delete message tag records from database).
	 */
	void removeSelectedTagsFromMsgs(void);

	/*!
	 * @brief Remove all tags from messages
	 *        (delete message tag records from database).
	 */
	void removeAllTagsFromMsgs(void);

	/*!
	 * @brief Activate/deactivate buttons on available selection change.
	 */
	void handleAvailableSelectionChange(void);

	/*!
	 * @brief Activate/deactivate buttons on assigned selection change.
	 */
	void handleAssignedSelectionChange(void);

/* Handle database signals. */
	/*!
	 * @brief Update model data when new tags are created.
	 *
	 * @param[in] entries Newly added tag entries.
	 */
	void watchTagsInserted(const Json::TagEntryList &entries);

	/*!
	 * @brief Update model data when tags updated.
	 *
	 * @param[in] entries Updated tag entries.
	 */
	void watchTagsUpdated(const Json::TagEntryList &entries);

	/*!
	 * @brief Update model data when tags deleted.
	 *
	 * @param[in] ids Deleted tag identifiers.
	 */
	void watchTagsDeleted(const Json::Int64StringList &ids);

	/*!
	 *  @brief Update model data when tag assignment changes.
	 *
	 *  @param[in] assignments Updated tag assignments.
	 */
	void watchTagAssignmentChanged(const Json::TagAssignmentList &assignments);

	/*!
	 * @brief Action when tag client is connected.
	 */
	void watchTagContConnected(void);

	/*!
	 * @brief Action when tag client is disconnected.
	 */
	void watchTagContDisconnected(void);

	/*!
	 * @brief Action when tag container is reset.
	 */
	void watchTagContReset(void);

private:
	/*!
	 * @brief Fill all assignment-related data from tag container.
	 */
	void loadAssignments(void);

	/*!
	 * @brief Initialises the dialogue.
	 *
	 * @paran[in] enableTagAssignment Whether to enable tag to message
	 *                                assignment functionality.
	 */
	void initDlg(bool enableTagAssignment);

	/*!
	 * @brief Load model content.
	 */
	void loadModels(void);

	/*!
	 * @brief Clear model content.
	 */
	void clearModels(void);

	/*!
	 * @brief Connect database container signals to appropriate slots.
	 */
	void databaseConnectActions(void);

	/*!
	 * @brief Choose (select) all tags in the list view
	 *        which are assigned in selected messages.
	 */
	void selectAllAssingedTagsFromMsgs(void);

	/*!
	 * @brief Set icon set to buttons.
	 */
	void setIcons(void);

	Ui::DlgTags *m_ui; /*!< UI generated from UI file. */

	const AcntId m_acntId; /*!< Account identifier. */
	const QList<qint64> m_msgIdList; /*!< Message identifiers. */
	TagContainer *m_tagContPtr; /*!< Tag database/client pointer. */
	QHash<Json::TagMsgId, TagItemList> m_msgTagItemsMap; /*!< Maps message identifiers to tag entry lists. */

	TagsDelegate m_availableTagsDelegate; /*!< Responsible for painting. */
	TagsModel m_availableTagsModel; /*!< Available tags model. */

	TagsDelegate m_assignedTagsDelegate; /*!< Responsible for painting. */
	TagsModel m_assignedTagsModel; /*!< Assigned tags model. */

	enum ReturnCode m_retCode; /*!< Dialogue return code. */
};
