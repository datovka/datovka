/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>

namespace Ui {
	class DlgViewChangeLog;
}

/*!
 * @brief Dialogue for displaying what's new in the app.
 */
class DlgViewChangeLog : public QDialog {
	Q_OBJECT

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent widget.
	 */
	explicit DlgViewChangeLog(QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgViewChangeLog(void);

	/*!
	 * @brief View modal dialogue.
	 *
	 * @param[in] parent Parent widget.
	 */
	static
	void view(QWidget *parent = Q_NULLPTR);

private:
	Ui::DlgViewChangeLog *m_ui; /*!< UI generated from UI file. */
};
