/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMessageBox>

class DlgYesNoCheckbox : public QMessageBox {
	Q_OBJECT

public:
	/*!
	 * @brief Value returned by exec().
	 */
	enum RetVal {
		NoUnchecked = 0,
		NoChecked = 1,
		YesUnchecked = 2,
		YesChecked = 3
	};

	/*!
	 * @brief Constructs question dialogue containing a checkbox.
	 *
	 * @param[in] title Window title.
	 * @param[in] questionText Question text.
	 * @param[in] checkBoxText Text displayed next to the checkbox.
	 * @param[in] detailText Detailed description text.
	 * @param[in] parent Window parent.
	 */
	DlgYesNoCheckbox(const QString &title, const QString &questionText,
	    const QString &checkBoxText, const QString &detailText,
	    QWidget *parent = Q_NULLPTR);

public slots:
	/*!
	 * @brief Execute the back-up operation and show modal dialogue.
	 *
	 * @return Value of enum RetVal.
	 */
	virtual
	int exec(void) Q_DECL_OVERRIDE;
};
