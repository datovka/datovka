/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/app_version_info.h"
#include "src/dimensions/dimensions.h"
#include "src/global.h"
#include "src/gui/dlg_view_changelog.h"
#include "src/settings/prefs_specific.h"
#include "ui_dlg_view_changelog.h"

DlgViewChangeLog::DlgViewChangeLog(QWidget *parent) :
    QDialog(parent),
    m_ui(new (::std::nothrow) Ui::DlgViewChangeLog)
{
	m_ui->setupUi(this);

	m_ui->headerLabel->setText(
	    QString("<b>%1: " VERSION "</b>").arg(tr("Version")));
	m_ui->changeLogTextEdit->setText(AppVersionInfo::releaseNewsText());

	connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
}

DlgViewChangeLog::~DlgViewChangeLog(void)
{
	delete m_ui;
}

void DlgViewChangeLog::view(QWidget *parent)
{
	DlgViewChangeLog dlg(parent);

	const QString dlgName("view_changelog");
	const QSize dfltSize = dlg.size();
	{
		const QSize newSize = Dimensions::dialogueSize(&dlg,
		    PrefsSpecific::dlgSize(*GlobInstcs::prefsPtr, dlgName),
		    dfltSize);
		if (newSize.isValid()) {
		    dlg.resize(newSize);
		}
	}

	dlg.exec();

	PrefsSpecific::setDlgSize(*GlobInstcs::prefsPtr, dlgName,
	    dlg.size(), dfltSize);
}
