/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/dimensions/dimensions.h"
#include "src/global.h"
#include "src/gui/dlg_licence.h"
#include "src/io/filesystem.h"
#include "src/settings/prefs_specific.h"
#include "ui_dlg_licence.h"

DlgLicence::DlgLicence(QWidget *parent)
    : QDialog(parent),
    m_ui(new (::std::nothrow) Ui::DlgLicence)
{
	m_ui->setupUi(this);

	m_ui->textEdit->setPlainText(
	    suppliedTextFileContent(TEXT_FILE_LICENCE));
	if (m_ui->textEdit->toPlainText().isEmpty()) {
		m_ui->textEdit->setPlainText(
		    tr("File '%1' either doesn't exist or is empty.")
		        .arg(expectedTextFilePath(TEXT_FILE_LICENCE)));
	}

	connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(close()));
}

DlgLicence::~DlgLicence(void)
{
	delete m_ui;
}

void DlgLicence::showLicence(QWidget *parent)
{
	DlgLicence dlg(parent);

	const QString dlgName("licence");
	const QSize dfltSize = dlg.size();
	{
		const QSize newSize = Dimensions::dialogueSize(&dlg,
		    PrefsSpecific::dlgSize(*GlobInstcs::prefsPtr, dlgName),
		    dfltSize);
		if (newSize.isValid()) {
			dlg.resize(newSize);
		}
	}

	dlg.exec();

	PrefsSpecific::setDlgSize(*GlobInstcs::prefsPtr, dlgName,
	    dlg.size(), dfltSize);
}
