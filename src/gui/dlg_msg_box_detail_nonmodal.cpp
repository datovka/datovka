/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/gui/dlg_msg_box_detail.h"
#include "src/gui/dlg_msg_box_detail_nonmodal.h"

/*!
 * @brief Display a non-modal dialogue window.
 *
 * @param[in] parent Parent widget.
 * @param[in] icon Dialogue icon.
 * @param[in] title Window title.
 * @param[in] text Message text.
 * @param[in] infoText Informative text.
 * @param[in] detailText Detailed text.
 * @param[in] buttons Window buttons.
 * @param[in] defaultButton Default button.
 */
static
void showDlgDetailNonModal(QWidget *parent, enum QMessageBox::Icon icon,
    const QString &title, const QString &text, const QString &infoText,
    const QString &detailText, QMessageBox::StandardButtons buttons,
    enum QMessageBox::StandardButton defaultButton)
{
	DlgMsgBoxDetail *dlg = new (::std::nothrow) DlgMsgBoxDetail(icon,
	   title, text, infoText, detailText, buttons, defaultButton, parent);
	if (Q_UNLIKELY(dlg == Q_NULLPTR)) {
		return;
	}

	dlg->setAttribute(Qt::WA_DeleteOnClose, true);
	dlg->setModal(false);

	dlg->show();
}

void DlgMsgBoxDetailNonModal::critical(QWidget *parent, const QString &title,
    const QString &text, const QString &infoText, const QString &detailText,
    QMessageBox::StandardButtons buttons,
    enum QMessageBox::StandardButton defaultButton)
{
	showDlgDetailNonModal(parent, QMessageBox::Critical, title, text,
	    infoText, detailText, buttons, defaultButton);
}

void DlgMsgBoxDetailNonModal::information(QWidget *parent, const QString &title,
    const QString &text, const QString &infoText, const QString &detailText,
    QMessageBox::StandardButtons buttons,
    enum QMessageBox::StandardButton defaultButton)
{
	showDlgDetailNonModal(parent, QMessageBox::Information, title, text,
	    infoText, detailText, buttons, defaultButton);
}

void DlgMsgBoxDetailNonModal::warning(QWidget *parent, const QString &title,
    const QString &text, const QString &infoText, const QString &detailText,
    QMessageBox::StandardButtons buttons,
    enum QMessageBox::StandardButton defaultButton)
{
	showDlgDetailNonModal(parent, QMessageBox::Warning, title, text,
	    infoText, detailText, buttons, defaultButton);
}
