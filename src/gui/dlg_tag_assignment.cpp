/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QLabel>
#include <QVBoxLayout>

#include "src/dimensions/dimensions.h"
#include "src/global.h"
#include "src/gui/dlg_tag_assignment.h"
#include "src/settings/prefs_specific.h"
#include "src/widgets/tags_popup_control_widget.h"

static const QString dlgName("tag_assignment");

DlgTagAssignment::DlgTagAssignment(TagContainer *tagCont,
    QWidget *parent)
    : QDialog(parent),
    m_tagAssignmentControl(Q_NULLPTR),
    m_dfltSize()
{
	setWindowTitle(tr("Edit Tag Assignment"));

	QVBoxLayout *verticalLayout = new (::std::nothrow) QVBoxLayout(this);
	if (Q_NULLPTR != verticalLayout) {
		verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
		verticalLayout->setContentsMargins(0, 0, 0, 0);

		m_tagAssignmentControl =
		    new (::std::nothrow) TagsPopupControlWidget(tagCont, this);
		if (Q_NULLPTR != m_tagAssignmentControl) {
			/* Force plain widget flags. */
			m_tagAssignmentControl->setWindowFlags(Qt::Widget);

			verticalLayout->addWidget(m_tagAssignmentControl);

			connect(m_tagAssignmentControl, SIGNAL(manageTags()),
			    this, SLOT(manageTagsClicked()));
		}
	}

	const QSize m_dfltSize = this->sizeHint();
	{
		const QSize newSize = Dimensions::dialogueSize(this,
		    PrefsSpecific::dlgSize(*GlobInstcs::prefsPtr, dlgName),
		    m_dfltSize);
		if (newSize.isValid()) {
			this->resize(newSize);
		}
	}
}

DlgTagAssignment::~DlgTagAssignment(void)
{
	PrefsSpecific::setDlgSize(*GlobInstcs::prefsPtr,
	    dlgName, this->size(), m_dfltSize);
}

void DlgTagAssignment::setMsgIds(const AcntId &acntId, const QSet<MsgId> &msgIds)
{
	if (Q_NULLPTR != m_tagAssignmentControl) {
		m_tagAssignmentControl->setMsgIds(acntId, msgIds);
	}
}

void DlgTagAssignment::manageTagsClicked(void)
{
	Q_EMIT manageTags();

	close();
}
