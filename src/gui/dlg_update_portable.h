/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>
#include <QProcess>
#include <QThread>

namespace Ui {
	class DlgUpdatePortable;
}

class UpdatePortableData; /* Forward declaration. */

/*!
 * @brief Copy directories in separate thread.
 *
 * @note The class declaration if including Q_OBJECT must be within a header
 *     file ... sigh.
 */
class CopyDirThread : public QThread {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] srcDir Source directory.
	 * @param[in] tgtDir Target directory.
	 * @param[in] parent Parent object.
	 */
	CopyDirThread(const QString &srcDir, const QString &tgtDir,
	    QObject *parent = Q_NULLPTR);

protected:
	/*!
	 * @brief Copy data from source do target.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

public:
	bool m_success; /*!< Whether copying was successful. */
	const QString m_srcDir; /*!< Source directory. */
	const QString m_tgtDir; /*!< Target directory. */
};

/*!
 * @brief Update portable application dialogue.
 */
class DlgUpdatePortable : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit DlgUpdatePortable(QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgUpdatePortable(void);

	/*!
	 * @brief Update the portable application.
	 *
	 * @param[in] parent Parent object.
	 */
	static
	bool updatePortable(QWidget *parent = Q_NULLPTR);

	static
	UpdatePortableData *updatePortableData; /*!< Set when portable update should be run. */

	/*!
	 * @brief Construct a portable update context.
	 *
	 * @param[in] pkgPath Package path.
	 * @param[in] pkgVer Package version.
	 * @param[in] workConfPath Current working configuration directory path.
	 * @return Pointer to newly allocated object.
	 */
	static
	UpdatePortableData *createUpdatePortableData(const QString &pkgPath,
	    const QString &pkgVer, const QString &workConfPath);

private slots:
	/*!
	 * @brief Change directory.
	 */
	void changeDirectory(void);

	/*!
	 * @brief Handle user input.
	 */
	void processUserInput(void);

	/*!
	 * @brief Extract package.
	 */
	void extractPackage(void);

	/*!
	 * @brief Wait for extraction process to finish.
	 *
	 * @param[in] exitCode Process exit code. May not be valid on failed
	 *                     exit status.
	 * @param[in] exitStatus Process exit status.
	 */
	void extractionProcessFinished(int exitCode,
	    QProcess::ExitStatus exitStatus);

	/*!
	 * @brief Wait for data copy thread to finish.
	 */
	void copyThreadFinished(void);

private:
	/*!
	 * @brief Initialise the dialogue.
	 */
	void initDialogue(void);

	Ui::DlgUpdatePortable *m_ui; /*!< UI generated from UI file. */
};
