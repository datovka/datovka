/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>
#include <QMenu>
#include <QString>
#include <QTimer>

#include "src/models/accounts_settings_model.h"
#include "src/models/sort_filter_proxy_model.h"
#include "src/models/users_model.h"
#include "src/settings/account.h"

namespace Ui {
	class DlgCreateAccount;
}

class AccountsMap; /* Forward declaration. */
class MessageDbSet; /* Forward declaration. */

/*!
 * @brief Account properties dialogue.
 */
class DlgCreateAccount : public QDialog {
	Q_OBJECT

public:
	/*!
	 * @brief Specifies the action to be performed.
	 */
	enum Action {
		ACT_ADDNEW, /*!< Create new account. */
		ACT_EDIT, /*!< Modify existing account. */
		ACT_PWD, /*!< Change/enter locally stored password. */
		ACT_CERT, /*!< Change/enter certificate data.  */
		ACT_CERTPWD, /*!< Change/enter certificate or password data. */
		ACT_MEP /*!< Change/enter communication key data. */
	};

private:
	/*!
	 * @brief Display form content flags.
	 */
	enum Property {
		FC_NONE = 0x00, /*!< Convenience value. */
		FC_SHADOW_ACNT = 0x01, /*!< Displays shadow account content. */
		FC_EDITABLE = 0x02, /*!< Whether values can be edited. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] accountInfo Account settings.
	 * @param[in] action Specifies which parts of dialogue to be enabled.
	 * @param[in] syncAllActName Synchronise all accounts action name.
	 * @param[in] parent Parent widget.
	 */
	DlgCreateAccount(const AcntData &accountInfo, enum Action action,
	    const QString &syncAllActName, QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgCreateAccount(void);

private:
	/*!
	 * @brief Perform modification for supplied account data.
	 */
	/*!
	 * @brief Performs modification of the supplied account data.
	 *
	 * @param[in,out] accountInfo Account data structure to be modified.
	 * @param[in]     action Modification action to be performed.
	 * @param[in]     syncAllActName Synchronise all accounts action name.
	 * @param[in]     properties Describes which control elements should be displayed.
	 * @param[in]     parent Parent widget.
	 * @return True when dialogue has been accepted and new account data
	 *     have been updated.
	 */
	static
	bool modify(AcntData &accountInfo, enum Action action,
	    const QString &syncAllActName, int properties,
	    QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Performs modification of the supplied account data.
	 *
	 * @param[in,out] accountInfo Account data structure to be modified.
	 * @param[in]     action Modification action to be performed.
	 * @param[in]     syncAllActName Synchronise all accounts action name.
	 * @param[in]     parent Parent widget.
	 * @return True when dialogue has been accepted and new account data
	 *     have been updated.
	 */
	static
	bool modify(AcntData &accountInfo, enum Action action,
	    const QString &syncAllActName, QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Manage accounts.
	 */
	static
	bool manage(AccountsMap &regularAccounts, AccountsMap &shadowAccounts,
	    const QString &syncAllActName, QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Check whether account already exists.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] regularAccounts Regular accounts.
	 * @param[in] shadowAccount Shadow accounts.
	 * @return True if account found.
	 */
	static
	bool accountExists(const AcntId &acntId,
	    const AccountsMap &regularAccounts,
	    const AccountsMap &shadowAccounts);

	/*!
	 * @brief Generate account info HTML message.
	 */
	static
	QString htmlAccountInfo(const AcntData &acntData, MessageDbSet *dbSet);

private slots:
	/*!
	 * @brief Activates parts of the dialogue depending on the login method.
	 *
	 * @param[in] loginMethodIdx Index of the login method to use.
	 */
	void activateContent(int loginMethodIdx);

	/*!
	 * @brief Checks input sanity, also activates the save/store button.
	 */
	void checkInputFields(void);

	/*!
	 * @brief Toggle password visibility.
	 */
	void togglePwdVisibility(void);

	/*!
	 * @brief Update password visibility progress.
	 */
	void updatePwdVisibilityProgress(void);

	/*!
	 * @brief Opens a dialogue in order to select a certificate file.
	 */
	void addCertificateFile(void);

	/*!
	 * @brief Gathers current account information and stores the settings.
	 */
	void collectAccountData(void);

	/*!
	 * @brief Changes dialogue content according to selected account.
	 */
	void accountCurrentChanged(const QModelIndex &current,
	    const QModelIndex &previous);

	/*!
	 * @brief Add shadow account into the shadow account container.
	 */
	void addShadowAccount(void);

	/*!
	 * @brief Remove shadow account.
	 */
	void removeSelectedShadowAccount(void);

	/*!
	 * @brief Refresh account information.
	 */
	void refreshAccountInformation(void);

	/*!
	 * @brief Changes user detail content according to selected user.
	 */
	void userCurrentChanged(const QModelIndex &current,
	    const QModelIndex &previous);

	/*!
	 * @brief Refresh user list information.
	 */
	void refreshUsersInformation(void);

	/*!
	 *  @brief Add new data-box user.
	 */
	void addUser(void);

	/*!
	 *  @brief Modify user contact address.
	 */
	void modifyUserContactAddress(void);

	/*!
	 * @brief Modify user privileges.
	 */
	void modifyUserPrivileges(void);

	/*!
	 * @brief Delete existing data-box user.
	 */
	void deleteUser(void);

private:
	/*!
	 * @brief Sets dialogue content from supplied account data.
	 *
	 * @param[in] acntData Account data to use when setting content.
	 * @param[in] properties Describes which control elements should be displayed.
	 */
	void setContent(const AcntData &acntData, int properties);

	/*!
	 * @brief Load account privilege information.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] nodeType Only regular and shadow account info are accepted.
	 */
	void loadPrivilegesContent(const AcntId &acntId,
	    enum AccountSettingsModel::NodeType nodeType);

	/*!
	 * @brief Set password line edit echo mode.
	 *
	 * @note Also sets view password button name.
	 *
	 * @param[in] echoMode Password line edit echo mode value.
	 */
	void setPwdLineEchoMode(int echoMode);

	/*!
	 * @brief Constructs account data from dialogue content.
	 *
	 * @return Account settings according to the dialogue state.
	 */
	AcntData getContent(void) const;

	Ui::DlgCreateAccount *m_ui; /*!< UI generated from UI file. */

	AccountSettingsModel m_accountSettingsModel; /*!< Available account listing. */
	AccountsMap *m_regularAccounts; /*!< Pointer to regular account container. */
	AccountsMap *m_shadowAccounts; /*!< Pointer to shadow account container. */

	AcntData m_accountInfo; /*!< Account data with submitted changes. */
	const enum Action m_action; /*!< Actual action the dialogue should be configured to. */
	const bool m_showViewPwd; /*!< Whether to show view password button. */
	int m_loginMethod; /*!< Specifies the method the user uses for logging in. */
	QString m_certPath; /*!< Path to certificate. */

	QTimer m_hidePwdTimer; /*!< View password countdown timer. */
	const int m_viewPwdUpdate; /*!< Update interval in milliseconds. */
	const int m_viewPwdDuration; /*!< Duration in milliseconds. */
	int m_viewPwdRemainingCycles; /*!< Number of remaining timer cycles. */

	/* User management. */
	UsersModel m_usersModel; /*!< Data-box user listing. */
	SortFilterProxyModel m_userListProxyModel; /*!< Proxy model used for sorting. */
	QMenu m_userListMenu; /*!< User list view tool button menu. */
	QAction *m_actionUserAdd;
	QAction *m_actionUserChangeContactAddress;
	QAction *m_actionUserChangePrivils;
	QAction *m_actionUserDelete;

	QMenu m_treeViewMenu; /*!< Tree view tool button menu. */
	QAction *m_actionAddShadow;
	QAction *m_actionRemoveShadow;
};
