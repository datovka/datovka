/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>

#include "src/gui/dlg_backup_internal.h"

/* Forward declaration. */
class BackupWorker;
class QThread;

namespace Ui {
	class DlgBackupProgress;
}

/*!
 * @brief Dialogue for the back-up progress visualisation.
 */
class DlgBackupProgress : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] dirPath Backup directory path.
	 * @param[in] msgDbSets Message database sets to be backed up.
	 * @param[in] accountDbFile Non-empty file name if account database
	 *                          should be backed up.
	 * @param[in] tagDbFile Non-empty file name if tag database should be
	 *                      backed up.
	 * @param[in] parent Parent object.
	 */
	DlgBackupProgress(const QString &dirPath,
	    const QList<BackupWorker::MessageEntry> &msgDbSets,
	    const QString &accountDbFile, const QString &tagDbFile,
	    QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	virtual
	~DlgBackupProgress(void);

	/*!
	 * @brief Show progress dialogue and preform back-up operation.
	 *
	 * @param[in] dirPath Backup directory path.
	 * @param[in] msgDbSets Message database sets to be backed up.
	 * @param[in] accountDbFile Non-empty file name if account database
	 *                          should be backed up.
	 * @param[in] tagDbFile Non-empty file name if tag database should be
	 *                      backed up.
	 * @param[in] parent Parent object.
	 * @return True is back-up succeeds.
	 *
	 */
	static
	bool backup(const QString &dirPath,
	    const QList<BackupWorker::MessageEntry> &msgDbSets,
	    const QString &accountDbPath, const QString &tagDbPath,
	    QWidget *parent = Q_NULLPTR);

public slots:
	/*!
	 * @brief Execute the back-up operation and show modal dialogue.
	 */
	virtual
	int exec(void) Q_DECL_OVERRIDE;

private slots:
	/*!
	 * @brief Set progress bar boundaries.
	 *
	 * @param[in] min Progress bar minimum.
	 * @param[in] max Progress bar maximum.
	 */
	void setupProgress(int min, int max);

	/*!
	 * @brief Set a label containing a brief task description.
	 *
	 * @param[in] name Task name.
	 */
	void displayWork(const QString &name);

	/*!
	 * @brief Signalises to the worker thread that it should finish.
	 */
	void cancelBackup(void);

	/*!
	 * @brief Display a warning modal dialogue window.
	 *
	 * @param[in] title Window title.
	 * @param[in] text Warning text.
	 */
	void showWarning(const QString &title, const QString &text);

	/*!
	 * @brief Show success dialogue and exit.
	 */
	void successNotification(void);

protected:
	/*!
	 * @brief Abort back-up and close.
	 *
	 * @param[in,out] event Caught close event.
	 */
	virtual
	void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

private:
	/*!
	 * @brief Initialise the dialogue.
	 */
	void initDialogue(void);

	Ui::DlgBackupProgress *m_ui; /*!< UI generated from UI file. */

	QThread *m_backupThread; /*!< Thread to run the back-up operation in. */
	BackupWorker *m_backupWorker; /*!< Back-up operation object. */
};
