/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QIcon>
#include <QVector>

#define ICON_128x128_PATH ":/icons/png_app_logo/128x128/" /* Used in PIN dialogue. */
#define ICON_256x256_PATH ":/icons/png_app_logo/256x256/"

/*!
 * @brief Container holding icons used in the application. Its purpose is to
 *     provide icons without the need to build them every time they are needed.
 */
class IconContainer {

public:
	enum Icon {
		ICON_APP_ERROR = 0,
		ICON_APP_WARNING,
		ICON_APP_OK,
		ICON_APP_ADD,
		ICON_APP_ARROW_DOWN,
		ICON_APP_ARROW_LEFT,
		ICON_APP_ARROW_RIGHT,
		ICON_APP_ARROW_UP,
		ICON_APP_DELETE,
		ICON_APP_FOLDER,
		ICON_APP_HELP,
		ICON_APP_HOME,
		ICON_APP_IMPORT_ZFO,
		ICON_APP_INFO,
		ICON_APP_PENCIL,
		ICON_APP_PREFS,
		ICON_APP_PROXY,
		ICON_APP_SEARCH,
		ICON_APP_SETTINGS,
		ICON_APP_SHIELD,
		ICON_APP_STATISTIC,
		ICON_APP_TAG_EDIT,
		ICON_APP_USER_KEY,
		ICON_APP_VIEW_LOG,
		ICON_APP_VIEW_ZFO,
		ICON_ATTACHMENT,
		ICON_ATTACHMENT_ADD,
		ICON_ATTACHMENT_DELETE,
		ICON_ATTACHMENT_OPEN,
		ICON_ATTACHMENT_SAVE,
		ICON_ATTACHMENT_SAVE_ALL,
		ICON_BRIEFCASE,
		ICON_BRIEFCASE_SETTINGS,
		ICON_BRIEFCASE_SYNC,
		ICON_BRIEFCASE_VERIFY,
		ICON_BRIEFCASE_UPLOAD,
		ICON_DATABASE_BACKUP,
		ICON_DATABASE_CONVERT,
		ICON_DATABASE_IMPORT,
		ICON_DATABASE_REPAIR,
		ICON_DATABASE_RESTORE,
		ICON_DATABOX,
		ICON_DATABOX_ADD,
		ICON_DATABOX_DELETE,
		ICON_DATABOX_NEW_MESSAGE,
		ICON_DATABOX_SETTINGS,
		ICON_DATABOX_SYNC,
		ICON_DATABOX_SYNC_ALL,
		ICON_DATABOX_SYNC_RECEIVED,
		ICON_DATABOX_SYNC_SENT,
		ICON_DATOVKA,
		ICON_DELIVERY_INFO_EXPORT_PDF,
		ICON_DELIVERY_INFO_EXPORT_ZFO,
		ICON_DELIVERY_INFO_OPEN,
		ICON_MESSAGE,
		ICON_MESSAGE_ADD,
		ICON_MESSAGE_ADD_DOCUMENT,
		ICON_MESSAGE_DELETE,
		ICON_MESSAGE_DOWNLOAD,
		ICON_MESSAGE_EMAIL,
		ICON_MESSAGE_EXPORT_ZFO,
		ICON_MESSAGE_EXPORT_PDF,
		ICON_MESSAGE_EXPORT_ALL,
		ICON_MESSAGE_FILTER,
		ICON_MESSAGE_FIND,
		ICON_MESSAGE_FORWARD,
		ICON_MESSAGE_OPEN,
		ICON_MESSAGE_RECEIVED,
		ICON_MESSAGE_REPLY,
		ICON_MESSAGE_SENT,
		ICON_MESSAGE_SIGNATURE,
		ICON_MESSAGE_UPLOAD,
		ICON_MESSAGE_VERIFY,
		ICON_RECIPIENT_ADD,
		ICON_RECIPIENT_DELETE,
		ICON_RECIPIENT_FIND,
		ICON_TOOLBAR_ATTACHMENT,
		ICON_TOOLBAR_DATABOX,
		ICON_TOOLBAR_MAIN,
		ICON_TOOLBAR_MESSAGE,
		ICON_TOOLBAR_MESSAGE_DETAIL,
		ICON_BALL_GREY,
		ICON_BALL_YELLOW,
		ICON_BALL_RED,
		ICON_BALL_GREEN,
		ICON_FLAG,
		ICON_HAND,
		ICON_MACOS_WINDOW,
		ICON_READCOL,

		MAX_ICONNUM /* Maximal number of icons (convenience value). */
	};

	/*!
	 * @brief Constructor.
	 */
	IconContainer(void);

	/*!
	 * @brief Returns icon object held within the container. The icon is
	 *     created the first time it is needed. Subsequent calls will return
	 *     the already constructed icon.
	 *
	 * @param[in] i Icon enumeration identifier.
	 * @return Non-null icon object held within the container, null icon on error.
	 */
	const QIcon &icon(enum Icon i);

	/*!
	 * @brief Purge stored icons.
	 */
	void rebuild(void);

	/*!
	 * @brief Builds the icon from image resources.
	 *
	 * @param[in] i Icon enumeration identifier.
	 * @return Non-null icon object, null icon on error.
	 */
	static
	QIcon construcIcon(enum Icon i);

private:
	QVector<QIcon> m_icons; /*!< Vector holding all constructed icons. */
};
