/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QCheckBox>
#include <QPushButton>

#include "src/gui/dlg_yes_no_checkbox.h"

DlgYesNoCheckbox::DlgYesNoCheckbox(const QString &title,
    const QString &questionText, const QString &checkBoxText,
    const QString &detailText, QWidget *parent)
    : QMessageBox(parent)
{
	this->setWindowTitle(title);

	this->setIcon(QMessageBox::Icon::Question);

	this->addButton(QMessageBox::Yes);
	this->addButton(QMessageBox::No);
	this->setDefaultButton(QMessageBox::No);

	this->setText(questionText);

	this->setCheckBox(new (::std::nothrow) QCheckBox(checkBoxText, this));

	this->setInformativeText(detailText);
}

int DlgYesNoCheckbox::exec(void)
{
	enum RetVal ret = NoUnchecked;

	/*
	 * We cannot directly intercept the already connected signal from the
	 * button box. Calling done() from a custom slot causes troubles in
	 * various versions of Qt.
	 * Therefore the original exec() is encapsulated and the received
	 * return value is translated.
	 */
	switch (this->QMessageBox::exec()) {
	case QMessageBox::Yes:
		ret = this->checkBox()->isChecked() ? YesChecked : YesUnchecked;
		break;
	case QMessageBox::No:
		ret = this->checkBox()->isChecked() ? NoChecked : NoUnchecked;
		break;
	default:
		break;
	}

	return ret;
}
