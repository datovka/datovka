/*
 * Copyright (C) 2014-2022 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/dimensions/dimensions.h"
#include "src/global.h"
#include "src/gui/dlg_view_filename_param_list.h"
#include "src/settings/prefs_specific.h"
#include "ui_dlg_view_filename_param_list.h"

static const QString dlgName("view_filename_param_list");

DlgViewFilenameParamList::DlgViewFilenameParamList(QWidget *parent,
    Qt::WindowFlags flags)
    : QDialog(parent, flags),
    m_dfltSize(),
    m_ui(new (::std::nothrow) Ui::DlgViewFilenameParamList)
{
	m_ui->setupUi(this);

	m_dfltSize = this->size();
	{
		const QSize newSize = Dimensions::dialogueSize(this,
		    PrefsSpecific::dlgSize(*GlobInstcs::prefsPtr, dlgName),
		    m_dfltSize);
		if (newSize.isValid()) {
			this->resize(newSize);
		}
	}
}

DlgViewFilenameParamList::~DlgViewFilenameParamList(void)
{
	PrefsSpecific::setDlgSize(*GlobInstcs::prefsPtr,
	    dlgName, this->size(), m_dfltSize);

	delete m_ui;
}
