/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */
#include <QString>

#include "src/common.h" /* MessageDirection */
#include "src/identifiers/account_id_db.h"
#include "src/identifiers/message_id.h"
#include "src/io/exports.h"

class QWidget; /* FOrward declaration. */

/*!
 * @brief Describes full message origin.
 */
class MsgOrigin {
public:
	MsgOrigin(void)
	    : acntIdDb(), msgId(), isVodz(false), direction(MSG_RECEIVED),
	    downloaded(false)
	{ }

	MsgOrigin(const AcntIdDb &ai, const MsgId &mi, bool v,
	    enum MessageDirection md, bool d)
	    : acntIdDb(ai), msgId(mi), isVodz(v), direction(md), downloaded(d)
	{ }

	AcntIdDb acntIdDb; /*!< Account and database set. */
	MsgId msgId; /*!< Message identifier. */
	bool isVodz; /*!< Whether high-volume message. */
	enum MessageDirection direction; /*!< Whether received or sent. */
	bool downloaded; /*!< Whether already downloaded or not. */
};

class GuiMsgOps {
	Q_DECLARE_TR_FUNCTIONS(GuiMsgOps)

public:
	/*!
	 * @brief Email content chosen by the user.
	 */
	enum EmailContent {
		ADD_NOTHING = 0, /*!< Nothing action. */
		ADD_ATTACHMENTS = 1, /*!< Add attachments to email. */
		ADD_ZFO_MESSAGE = 2, /*!< Add zfo message to email. */
		ADD_ZFO_DELIVERY_INFO = 4 /*!< Add zfo delivery info to email. */
	};
	Q_DECLARE_FLAGS(EmailContents, EmailContent)

	/*!
	 * @brief Export selected messages to disk.
	 *
	 * @param[in] originList List of messages to be worked with.
	 * @param[in] expFileType Export file type.
	 * @param[in] parent Parent widget.
	 */
	static
	void exportSelectedData(const QList<MsgOrigin> &originList,
	    enum Exports::ExportFileType expFileType, QWidget *parent);

	/*!
	 * @brief Export selected message envelopes as PDF and attachment files.
	 *
	 * @param[in] originList List of messages to be worked with.
	 * @param[in] parent Parent widget.
	 */
	static
	void exportSelectedEnvelopeAndAttachments(
	    const QList<MsgOrigin> &originList, QWidget *parent);

	/*!
	 * @brief Construct a random boundary line.
	 *
	 * @return Boundary line string.
	 */
	static
	QString emailBoundary(void);

	/*!
	 * @brief Sends selected messages into default e-mail client.
	 *
	 * @param[in] originList List of messages to be worked with.
	 * @param[in] attchFlags Email attachment flags.
	 */
	static
	void createEmail(const QList<MsgOrigin> &originList,
	    EmailContents attchFlags);
};
