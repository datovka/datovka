/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>

#include "src/identifiers/account_id_db.h"
#include "src/models/backup_selection_model.h"

namespace Ui {
	class DlgBackup;
}

/*!
 * @brief Dialogue for the backup operation.
 */
class DlgBackup : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntIdDbList List of available accounts.
	 * @param[in] parent Parent object.
	 */
	explicit DlgBackup(const QList<AcntIdDb> &acntIdDbList,
	    QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgBackup(void);

	/*!
	 * @brief View back-up operation dialogue.
	 *
	 * @param[in] acntIdDbList List of available accounts.
	 * @param[in] parent Window parent widget.
	 */
	static
	void backup(const QList<AcntIdDb> &acntIdDbList,
	    QWidget *parent = Q_NULLPTR);

protected:
	/*!
	 * @brief Check window geometry and set table columns width.
	 *
	 * @param[in] event Widget show event.
	 */
	virtual
	void showEvent(QShowEvent *event) Q_DECL_OVERRIDE;

private slots:
	/*!
	 * @brief Compute free space on the device specified by the path
	 *     in the directory field.
	 *
	 * @param[in] dirPath Directory path.
	 */
	void computeAvailableSpace(const QString &dirPath);

	/*!
	 * @brief Choose data directory.
	 */
	void chooseDirectory(void);

	/*!
	 * @brief Change selection according to all checkbox state.
	 */
	void applyAllSelection(int state);

	/*!
	 * @brief Update all selection button.
	 */
	void modifiedSelection(void);

	/*!
	 * @brief Activates confirmation button.
	 */
	void enableOkButton(void);

	/*!
	 * @brief Performs backup operation.
	 *
	 * @note Closes the dialogue on success.
	 */
	void backupSelected(void);

private:
	/*!
	 * @brief Compute required space needed to store selected data.
	 */
	void computeRequiredSpace(void);

	/*!
	 * @brief Show size notification if needed.
	 */
	void showSizeNotificationIfNeeded(void);

	/*!
	 * @brief Initialise the dialogue.
	 */
	void initDialogue(void);

	Ui::DlgBackup *m_ui; /*!< UI generated from UI file. */

	const QList<AcntIdDb> m_acntIdDbList; /*!< Available accounts.*/

	QString m_backupVolume; /*!< Name of back-up volume (if it can be acquired). */
	qint64 m_availableSpace; /*!<
	                          * Amount of available backup storage pace,
	                          * -1 if value could not be determined.
	                          */
	qint64 m_requiredSpace; /*!<
	                         * Amount of storage space needed to store
	                         * selected data,
	                         * -1 if value could not be determined.
	                         */
	BackupSelectionModel m_backupSelectionModel; /*!< Choose account data to back up. */
};
