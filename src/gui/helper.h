/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>

#include "src/identifiers/account_id_db.h"

/* Forward declaration. */
class AcntId;
class MainWindow;
class MessageDbSet;

/*!
 * @brief GUI interaction helper functions.
 */
namespace GuiHelper {

	/*!
	 * @brief Checks whether the related account has an active connection
	 *     to ISDS.
	 *
	 * @param[in] mw Pointer to main window.
	 * @param[in] acntId Account identifier.
	 * @return True if active connection is present.
	 */
	bool isLoggedIn(MainWindow *const mw, const AcntId &acntId);

	/*!
	 * @brief Find database set related to account.
	 *
	 * @param[in] acntIdDbList Account descriptors.
	 * @param[in] acntId Account identifier.
	 * @return Pointer related to specified account, Q_NULLPTR on error.
	 */
	MessageDbSet *getDbSet(const QList<AcntIdDb> &acntIdDbList,
	    const AcntId &acntId);

}
