/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>
#include <QString>

#include "src/datovka_shared/json/basic.h" /* Json::Int64StringList is used in slots. Q_MOC_INCLUDE */
#include "src/delegates/tag_item.h"
#include "src/json/tag_entry.h" /* Json::TagEntryList is used in slots. Q_MOC_INCLUDE */

class TagDb; /* Forward declaration. */
class TagClient; /* Forward declaration. */
class TagContainer; /* Forward declaration. */

namespace Ui {
	class DlgTag;
}

/*!
 * @brief Create new tag dialogue.
 */
class DlgTag : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] tag Tag to be modified.
	 * @param[in] tagStorage Tag storage.
	 * @param[in] parent Parent widget.
	 */
	explicit DlgTag(const TagItem &tag, QObject *tagStorage,
	    QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgTag(void);

	/*!
	 * @brief Create new tag.
	 *
	 * @param[in] tagCont Pointer to tag container.
	 * @param[in] parent Parent widget.
	 */
	static
	bool createTag(TagContainer *tagCont, QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Edit existing tag.
	 *
	 * @param[in] tagCont Pointer to tag container.
	 * @param[in] tag Tag to be modified.
	 * @param[in] parent Parent widget.
	 * @return True when tag has been changed.
	 */
	static
	bool editTag(TagContainer *tagCont, const TagItem &tag,
	    QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Edit existing tag in local database.
	 *
	 * @param[in] tagDb Pointer to tag database.
	 * @param[in] tag Tag to be modified.
	 * @param[in] parent Parent widget.
	 * @return True when tag has been changed.
	 */
	static
	bool editTag(TagDb *tagDb, const TagItem &tag,
	    QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Edit existing tag in server database.
	 *
	 * @param[in] tagClient Pointer to tag client.
	 * @param[in] tag Tag to be modified.
	 * @param[in] parent Parent widget.
	 * @return True when tag has been changed.
	 */
	static
	bool editTag(TagClient *tagClient, const TagItem &tag,
	    QWidget *parent = Q_NULLPTR);

private slots:
	/*!
	 * @brief Sets window elements according to tag name.
	 *
	 * @param[in] tagName Tag name.
	 */
	void tagNameChanged(const QString &tagName);

	/*!
	 * @brief Choose or change tag colour.
	 */
	void chooseNewColor(void);

/* Handle database signals. */
	/*!
	 * @brief Update content when tags updated.
	 *
	 * @param[in] entries Updated tag entries.
	 */
	void watchTagsUpdated(const Json::TagEntryList &entries);

	/*!
	 * @brief Close dialogue when tags deleted.
	 *
	 * @param[in] ids Deleted tag identifiers.
	 */
	void watchTagsDeleted(const Json::Int64StringList &ids);

	/*!
	 * @brief Action when tag client is connected.
	 */
	void watchTagContConnected(void);

	/*!
	 * @brief Action when tag client is disconnected.
	 */
	void watchTagContDisconnected(void);

	/*!
	 * @brief Action when tag container is reset.
	 */
	void watchTagContReset(void);

private:
	/*!
	 * @brief Set actual tag colour on the preview button.
	 *
	 * @param[in] tagItem Tag item to read the colour from.
	 */
	void setPreviewButtonColor(const TagItem &tagItem);

	/*!
	 * @brief Enable the acceptance button according to window content.
	 */
	void enableOkButton(void);

	/*!
	 * @brief Update existing dialogue content from storage.
	 *
	 * @return True if content updated, false if data not available.
	 */
	bool updateContent(void);

	/*!
	 * @brief Insert or update tag data into database.
	 *
	 * @param[in] tagContPtr Pointer to tag container, client or database..
	 * @param[in] tagItem Tag content to be stored to database.
	 * @param[in] parent Parent widget.
	 * @return True when tag has been saved.
	 */
	static
	bool saveTag(QObject *tagContPtr, const TagItem &tagItem,
	    QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Connect database container signals to appropriate slots.
	 *
	 * @param[in] @param[in] tagContPtr Pointer to tag container, client or database.
	 */
	void databaseConnectActions(QObject *tagContPtr);

	Ui::DlgTag *m_ui; /*!< UI generated from UI file. */

	QObject *m_tagStorage; /*!< Pointer to tag storage. */

	TagItem m_originalTagItem; /*!< Original tag content in database. */
	TagItem m_modifiedTagItem; /*!< Modified tag content. */

	bool m_canSaveData; /*!< Controls the accept button. */

	QSize m_dfltSize; /*!< Remembered dialogue default size. */
};
