/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

class QCalendarWidget; /* Forward declaration. */

/*!
 * @brief GUI style-related functions.
 */
namespace GuiStyles {

	extern
	bool isDarkTheme; /*!< Whether dark theme is used. */

	/*!
	 * @brief Get style sheet from index.
	 *
	 * @param[in] index Value as defined by enum PrefsSpecific::AppUiTheme.
	 * @return Style sheet file path.
	 */
	QString styleSheetPath(int index);

	/*!
	 * @brief Replace colour variables for hexa colours in style sheets.
	 *
	 * @param[in] styleSheet Style sheet text to be modified.
	 * @param[in] fontSizePt Font size in points.
	 * @return Modified style sheet text.
	 */
	QString replaceStyleSheetVariables(QString styleSheet, qint64 fontSizePt);

	/*!
	 * @brief Set (or set default) calendar dates style sheet.
	 */
	void setCalendarDatesStyle(QCalendarWidget *calendar);

	/*!
	 * @brief Set calendar style sheet.
	 *
	 * @param[in,out] calendarWidget Calendar to be modified.
	 */
	void setCalendarStyle(QCalendarWidget *calendarWidget);

	/*!
	 * @brief Detect dark system theme.
	 *
	 * @return True if dark theme active.
	 */
	bool detectDarkSystemTheme(void);
}
