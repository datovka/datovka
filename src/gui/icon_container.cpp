/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QString>

#include "src/gui/icon_container.h"
#include "src/gui/styles.h"

#define ICON_LIGHT_16x16_PATH ":/icons/light/16x16/"
#define ICON_LIGHT_24x24_PATH ":/icons/light/24x24/"
#define ICON_LIGHT_32x32_PATH ":/icons/light/32x32/"
#define ICON_LIGHT_48x48_PATH ":/icons/light/48x48/"
#define ICON_LIGHT_64x64_PATH ":/icons/light/64x64/"

#define ICON_DARK_16x16_PATH ":/icons/dark/16x16/"
#define ICON_DARK_24x24_PATH ":/icons/dark/24x24/"
#define ICON_DARK_32x32_PATH ":/icons/dark/32x32/"
#define ICON_DARK_48x48_PATH ":/icons/dark/48x48/"
#define ICON_DARK_64x64_PATH ":/icons/dark/64x64/"

#define ICON_DISABLED_16x16_PATH ":/icons/disabled/16x16/"
#define ICON_DISABLED_24x24_PATH ":/icons/disabled/24x24/"
#define ICON_DISABLED_32x32_PATH ":/icons/disabled/32x32/"
#define ICON_DISABLED_48x48_PATH ":/icons/disabled/48x48/"
#define ICON_DISABLED_64x64_PATH ":/icons/disabled/64x64/"

/*
 * https://forum.qt.io/topic/64963/setting-qicon-with-svg-file-as-a-qaction-icon-problem/7
 * https://bugreports.qt.io/browse/QTBUG-55932
 *
 * In order to use SVG icons, you may use the construction:
 * ico.addPixmap(QPixmap(ICON_SVG_PATH "icon.svg"));
 * SVG icons may not work properly with older Qt versions.
 */

/* Null objects - for convenience. */
static const QIcon nullIcon;

IconContainer::IconContainer(void)
    : m_icons(MAX_ICONNUM)
{
}

const QIcon &IconContainer::icon(enum Icon i)
{
	if (Q_UNLIKELY(i == MAX_ICONNUM)) {
		Q_ASSERT(0);
		return nullIcon;
	}

	QIcon &icon(m_icons[i]);
	if (Q_UNLIKELY(icon.isNull())) {
		icon = construcIcon(i);
	}

	return icon;
}

void IconContainer::rebuild(void)
{
	m_icons.clear();
	m_icons.resize(MAX_ICONNUM);
}

static
void addIconSet(QIcon &ico, const QString &file)
{
	if (GuiStyles::isDarkTheme) {
		ico.addFile(ICON_DARK_16x16_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(ICON_DARK_24x24_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(ICON_DARK_32x32_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(ICON_DARK_48x48_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(ICON_DARK_64x64_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(ICON_DISABLED_16x16_PATH + file, QSize(), QIcon::Disabled, QIcon::Off);
		ico.addFile(ICON_DISABLED_24x24_PATH + file, QSize(), QIcon::Disabled, QIcon::Off);
		ico.addFile(ICON_DISABLED_32x32_PATH + file, QSize(), QIcon::Disabled, QIcon::Off);
		ico.addFile(ICON_DISABLED_48x48_PATH + file, QSize(), QIcon::Disabled, QIcon::Off);
		ico.addFile(ICON_DISABLED_64x64_PATH + file, QSize(), QIcon::Disabled, QIcon::Off);
	} else {
		ico.addFile(ICON_LIGHT_16x16_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(ICON_LIGHT_24x24_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(ICON_LIGHT_32x32_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(ICON_LIGHT_48x48_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(ICON_LIGHT_64x64_PATH + file, QSize(), QIcon::Normal, QIcon::Off);
	}
}

QIcon IconContainer::construcIcon(enum Icon i)
{
	QIcon ico;

	switch (i) {
	case ICON_APP_ERROR:
		addIconSet(ico, "app_error.png");
		break;
	case ICON_APP_WARNING:
		addIconSet(ico, "app_warning.png");
		break;
	case ICON_APP_OK:
		addIconSet(ico, "app_ok.png");
		break;
	case ICON_APP_ADD:
		addIconSet(ico, "app_plus.png");
		break;
	case ICON_APP_ARROW_DOWN:
		addIconSet(ico, "app_arrow_down.png");
		break;
	case ICON_APP_ARROW_LEFT:
		addIconSet(ico, "app_arrow_left.png");
		break;
	case ICON_APP_ARROW_RIGHT:
		addIconSet(ico, "app_arrow_right.png");
		break;
	case ICON_APP_ARROW_UP:
		addIconSet(ico, "app_arrow_up.png");
		break;
	case ICON_APP_DELETE:
		addIconSet(ico, "app_delete.png");
		break;
	case ICON_APP_FOLDER:
		addIconSet(ico, "app_folder.png");
		break;
	case ICON_APP_HELP:
		addIconSet(ico, "app_help.png");
		break;
	case ICON_APP_HOME:
		addIconSet(ico, "app_home.png");
		break;
	case ICON_APP_IMPORT_ZFO:
		addIconSet(ico, "app_import_zfo.png");
		break;
	case ICON_APP_INFO:
		addIconSet(ico, "app_info.png");
		break;
	case ICON_APP_PENCIL:
		addIconSet(ico, "app_pencil.png");
		break;
	case ICON_APP_PREFS:
		addIconSet(ico, "app_preferences.png");
		break;
	case ICON_APP_PROXY:
		addIconSet(ico, "app_proxy.png");
		break;
	case ICON_APP_SEARCH:
		addIconSet(ico, "app_search.png");
		break;
	case ICON_APP_SETTINGS:
		addIconSet(ico, "app_settings.png");
		break;
	case ICON_APP_SHIELD:
		addIconSet(ico, "app_shield.png");
		break;
	case ICON_APP_STATISTIC:
		addIconSet(ico, "app_statistic.png");
		break;
	case ICON_APP_TAG_EDIT:
		addIconSet(ico, "app_tag_edit.png");
		break;
	case ICON_APP_USER_KEY:
		addIconSet(ico, "app_key.png");
		break;
	case ICON_APP_VIEW_LOG:
		addIconSet(ico, "app_view_log.png");
		break;
	case ICON_APP_VIEW_ZFO:
		addIconSet(ico, "app_view_zfo.png");
		break;
	case ICON_ATTACHMENT:
		addIconSet(ico, "attachment.png");
		break;
	case ICON_ATTACHMENT_ADD:
		addIconSet(ico, "attachment_add.png");
		break;
	case ICON_ATTACHMENT_DELETE:
		addIconSet(ico, "attachment_delete.png");
		break;
	case ICON_ATTACHMENT_OPEN:
		addIconSet(ico, "attachment_open.png");
		break;
	case ICON_ATTACHMENT_SAVE:
		addIconSet(ico, "attachment_save.png");
		break;
	case ICON_ATTACHMENT_SAVE_ALL:
		addIconSet(ico, "attachment_save_all.png");
		break;
	case ICON_BRIEFCASE:
		addIconSet(ico, "briefcase.png");
		break;
	case ICON_BRIEFCASE_SETTINGS:
		addIconSet(ico, "briefcase_settings.png");
		break;
	case ICON_BRIEFCASE_SYNC:
		addIconSet(ico, "briefcase_sync.png");
		break;
	case ICON_BRIEFCASE_VERIFY:
		addIconSet(ico, "briefcase_verify.png");
		break;
	case ICON_BRIEFCASE_UPLOAD:
		addIconSet(ico, "briefcase_upload.png");
		break;
	case ICON_DATABASE_BACKUP:
		addIconSet(ico, "database_backup.png");
		break;
	case ICON_DATABASE_CONVERT:
		addIconSet(ico, "database_convert.png");
		break;
	case ICON_DATABASE_IMPORT:
		addIconSet(ico, "database_import.png");
		break;
	case ICON_DATABASE_REPAIR:
		addIconSet(ico, "database_repair.png");
		break;
	case ICON_DATABASE_RESTORE:
		addIconSet(ico, "database_restore.png");
		break;
	case ICON_DATABOX:
		addIconSet(ico, "databox.png");
		break;
	case ICON_DATABOX_ADD:
		addIconSet(ico, "databox_add.png");
		break;
	case ICON_DATABOX_DELETE:
		addIconSet(ico, "databox_delete.png");
		break;
	case ICON_DATABOX_NEW_MESSAGE:
		addIconSet(ico, "databox_new_message.png");
		break;
	case ICON_DATABOX_SETTINGS:
		addIconSet(ico, "databox_settings.png");
		break;
	case ICON_DATABOX_SYNC:
		addIconSet(ico, "databox_sync.png");
		break;
	case ICON_DATABOX_SYNC_ALL:
		addIconSet(ico, "databox_sync_all.png");
		break;
	case ICON_DATABOX_SYNC_RECEIVED:
		addIconSet(ico, "databox_sync_received.png");
		break;
	case ICON_DATABOX_SYNC_SENT:
		addIconSet(ico, "databox_sync_sent.png");
		break;
	case ICON_DATOVKA:
		addIconSet(ico, "datovka.png");
		ico.addFile(QStringLiteral(ICON_128x128_PATH "datovka.png"), QSize(), QIcon::Normal, QIcon::Off);
		ico.addFile(QStringLiteral(ICON_256x256_PATH "datovka.png"), QSize(), QIcon::Normal, QIcon::Off);
		break;
	case ICON_DELIVERY_INFO_EXPORT_PDF:
		addIconSet(ico, "delivery_info_export_pdf.png");
		break;
	case ICON_DELIVERY_INFO_EXPORT_ZFO:
		addIconSet(ico, "delivery_info_export_zfo.png");
		break;
	case ICON_DELIVERY_INFO_OPEN:
		addIconSet(ico, "delivery_info_open.png");
		break;
	case ICON_MESSAGE:
		addIconSet(ico, "message.png");
		break;
	case ICON_MESSAGE_ADD:
		addIconSet(ico, "message_add.png");
		break;
	case ICON_MESSAGE_ADD_DOCUMENT:
		addIconSet(ico, "message_add_document.png");
		break;
	case ICON_MESSAGE_DELETE:
		addIconSet(ico, "message_delete.png");
		break;
	case ICON_MESSAGE_DOWNLOAD:
		addIconSet(ico, "message_download.png");
		break;
	case ICON_MESSAGE_EMAIL:
		addIconSet(ico, "message_email.png");
		break;
	case ICON_MESSAGE_EXPORT_ZFO:
		addIconSet(ico, "message_export_zfo.png");
		break;
	case ICON_MESSAGE_EXPORT_PDF:
		addIconSet(ico, "message_export_pdf.png");
		break;
	case ICON_MESSAGE_EXPORT_ALL:
		addIconSet(ico, "message_export_all.png");
		break;
	case ICON_MESSAGE_FILTER:
		addIconSet(ico, "app_filter.png");
		break;
	case ICON_MESSAGE_FIND:
		addIconSet(ico, "message_find.png");
		break;
	case ICON_MESSAGE_FORWARD:
		addIconSet(ico, "message_forward.png");
		break;
	case ICON_MESSAGE_OPEN:
		addIconSet(ico, "message_open.png");
		break;
	case ICON_MESSAGE_RECEIVED:
		addIconSet(ico, "message_received.png");;
		break;
	case ICON_MESSAGE_REPLY:
		addIconSet(ico, "message_reply.png");
		break;
	case ICON_MESSAGE_SIGNATURE:
		addIconSet(ico, "message_signature.png");
		break;
	case ICON_MESSAGE_SENT:
		addIconSet(ico, "message_sent.png");
		break;
	case ICON_MESSAGE_UPLOAD:
		addIconSet(ico, "message_upload.png");
		break;
	case ICON_MESSAGE_VERIFY:
		addIconSet(ico, "message_verify.png");
		break;
	case ICON_RECIPIENT_ADD:
		addIconSet(ico, "app_recipient_add.png");
		break;
	case ICON_RECIPIENT_DELETE:
		addIconSet(ico, "app_recipient_delete.png");
		break;
	case ICON_RECIPIENT_FIND:
		addIconSet(ico, "app_recipient_find.png");
		break;
	case ICON_TOOLBAR_ATTACHMENT:
		addIconSet(ico, "toolbar_attachment.png");
		break;
	case ICON_TOOLBAR_DATABOX:
		addIconSet(ico, "toolbar_databox.png");
		break;
	case ICON_TOOLBAR_MAIN:
		addIconSet(ico, "toolbar_main.png");
		break;
	case ICON_TOOLBAR_MESSAGE:
		addIconSet(ico, "toolbar_message.png");
		break;
	case ICON_TOOLBAR_MESSAGE_DETAIL:
		addIconSet(ico, "toolbar_message_detail.png");
		break;
	case ICON_BALL_GREY:
		addIconSet(ico, "grey.png");
		break;
	case ICON_BALL_YELLOW:
		addIconSet(ico, "yellow.png");
		break;
	case ICON_BALL_RED:
		addIconSet(ico, "red.png");
		break;
	case ICON_BALL_GREEN:
		addIconSet(ico, "green.png");
		break;
	case ICON_FLAG:
		addIconSet(ico, "app_flag.png");
		break;
	case ICON_HAND:
		addIconSet(ico, "app_hand.png");
		break;
	case ICON_MACOS_WINDOW:
		addIconSet(ico, "macos_window.png");
		break;
	case ICON_READCOL:
		addIconSet(ico, "app_readcol.png");
		break;
	case MAX_ICONNUM:
	default:
		Q_ASSERT(0);
		return nullIcon;
		break;
	}

	return ico;
}
