/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QDialog>
#include <QString>
#include <QWidget>

namespace Ui {
	class DlgAbout;
}

class UpdateChecker; /* Forward declaration. */

/*!
 * @brief About dialogue.
 */
class DlgAbout : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] updateChecker Update checker.
	 * @param[in] tabIdx Default tab index.
	 * @param[in] parent Parent object.
	 */
	explicit DlgAbout(UpdateChecker *updateChecker, int tabIdx,
	    QWidget *parent = Q_NULLPTR);

public:

	/*!
	 * @brief Tab indexes.
	 */
	enum TabIndex {
		TAB_0_ABOUT = 0,
		TAB_1_DEVELOP = 1,
		TAB_2_LICENCE = 2,
		TAB_3_DONATE = 3
	};

	/*!
	 * @brief Destructor.
	 */
	~DlgAbout(void);

	/*!
	 * @brief View about dialogue.
	 *
	 * @param[in] updateChecker Update checker.
	 * @param[in] tabIdx Default tab index.
	 * @param[in] parent Window parent widget.
	 * @return True if window has been displayed.
	 */
	static
	bool about(UpdateChecker *updateChecker, int tabIdx,
	    QWidget *parent = Q_NULLPTR);

private slots:
	/*!
	 * @brief Show the licence file in new dialogue window.
	 */
	void showLicence(void);

	/*!
	 * @brief Load latest version information from the version checker.
	 */
	void reloadLatestVersionData(void);

	/*!
	 * @brief Run installation package download.
	 */
	void downloadPkg(void);

	/*!
	 * @brief Process the package download progress.
	 *
	 * @param[in] bytesReceived Downloaded.
	 * @param[in] bytesTotal Total.
	 */
	void processPackageDownloadProgress(qint64 bytesReceived,
	    qint64 bytesTotal);

	/*!
	 * @brief Run semi update procedure.
	 */
	void updateApp(void);

	/*!
	 * @brief Open changelog dialog.
	 */
	void viewChangeLog(void);

	/*!
	 * @brief Open donate web page.
	 */
	void onDonate(void);

private:
	Ui::DlgAbout *m_ui; /*!< UI generated from UI file. */

	UpdateChecker *m_updateChecker; /*!< Pointer to update checker. */
};
