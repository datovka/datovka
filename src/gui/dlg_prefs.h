/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>

#include "src/models/prefs_model.h"
#include "src/models/sort_filter_proxy_model.h"

class Prefs; /* Forward declaration. */

namespace Ui {
	class DlgPrefs;
}

/*!
 * @brief Views raw preferences.
 */
class DlgPrefs : public QDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in,out] prefs Preferences to be modified.
	 * @param[in] parent Parent widget.
	 */
	explicit DlgPrefs(Prefs &prefs, QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgPrefs(void);

	/*!
	 * @brief Modifies global preferences.
	 *
	 * @param[in,out] prefs Preferences to be modified.
	 * @param[in]     parent Parent widget.
	 */
	static
	void modify(Prefs &prefs, QWidget *parent = Q_NULLPTR);

private slots:
	/*!
	 * @brief View preferences table.
	 */
	void viewPrefsList(void);

	/*!
	 * @brief Apply filter text on the table.
	 */
	void filterPrefs(const QString &text);

	/*!
	 * @brief Opens edit window or alters value if used on value field.
	 */
	void prefItemDoubleClicked(const QModelIndex &index);

	/*!
	 * @brief Generates a context menu depending on the item and selection
	 *     it was invoked on.
	 *
	 * @param[in] point Invocation position.
	 */
	void viewPrefListContextMenu(const QPoint &point);

/* Preference context menu. */
	/*!
	 * @brief Reset selected preferences.
	 */
	void prefReset(void);

private:
	Ui::DlgPrefs *m_ui; /*!< UI generated from UI file. */

	SortFilterProxyModel m_prefListProxyModel; /*!<
	                                            * Used for message
	                                            * sorting and filtering.
	                                            */
	QString m_dfltFilerLineStyleSheet; /*!< Used to remember default line edit style. */
	PrefsModel m_prefTableModel; /*!< Model of preferences. */
};
