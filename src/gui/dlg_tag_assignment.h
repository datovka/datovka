/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>

class AcntId; /* Forward declaration. */
class MsgId; /* Forward declaration. */
class TagContainer; /* Forward declaration. */
class TagsPopupControlWidget; /* FOrward declaration. */

/*!
 * @brief Encapsulated tags control popup into a dilaogue.
 */
class DlgTagAssignment : public QDialog {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] tagCont Tag container.
	 * @param[in] parent Parent widget.
	 */
	explicit DlgTagAssignment(TagContainer *tagCont,
	    QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Destructor.
	 */
	virtual
	~DlgTagAssignment(void);

	/*!
	 * @brief Set watched message identifiers.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgIds Set of message IDs, empty set if no assignment
	 *                   should be edited.
	 */
	void setMsgIds(const AcntId &acntId, const QSet<MsgId> &msgIds);

Q_SIGNALS:
	/*!
	 * @brief Edit all tags.
	 */
	void manageTags(void);

private Q_SLOTS:
	/*!
	 * @brief Edit all tags.
	 *
	 * @note Emits manageTags. Closes the dialogue.
	 */
	void manageTagsClicked(void);

private:
	TagsPopupControlWidget *m_tagAssignmentControl;

	QSize m_dfltSize; /*!< Remembered dialogue default size. */
};
