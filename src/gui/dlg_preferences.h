/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>

#include "src/datovka_shared/settings/pin.h"

class Prefs; /* Forward declaration. */

namespace Ui {
	class DlgPreferences;
}

/*!
 * @brief Preferences dialogue.
 */
class DlgPreferences : public QDialog {
	Q_OBJECT

public:
	/*!
	 * @brief Tabs which this dialogue has.
	 */
	enum TabIndexes {
		TAB_DOWNLOADING = 0,
		TAB_SECURITY,
		TAB_NAVIGATION,
		TAB_INTERFACE,
		TAB_DIRECTORIES,
		TAB_SAVING,
		TAB_LANGUAGE,
		TAB_DATA_COLLECTION,

		MAX_TABNUM /* Maximal number of columns (convenience value). */
	};

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] prefs Preferences according to which to set the dialogue content.
	 * @param[in] pinSett PIN settings to use to set dialogue.
	 * @param[in] tabNum Index of tab to be selected after window is shown.
	 * @param[in] parent Parent widget.
	 */
	explicit DlgPreferences(const Prefs &prefs, const PinSettings &pinSett,
	    int tabNum = 0, QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgPreferences(void);

	/*!
	 * @brief Modifies global preferences.
	 *
	 * @param[in,out] prefs Preferences to be modified.
	 * @param[in,out] pinSett PIN settings to be modified.
	 * @param[in]     tabNum Index of tab to be selected after window is shown.
	 * @param[in]     parent Parent widget.
	 * @return True when dialogue has been accepted and preference data
	 *     have been updated.
	 */
	static
	bool modify(Prefs &prefs, PinSettings &pinSett,
	    int tabNum = 0, QWidget *parent = Q_NULLPTR);

signals:
	/*!
	 * @brief Emitted then theme index changes.
	 *
	 * @note -1 is emitted to indicate that values from preferences should
	 *     be taken.
	 */
	void themeChanged(int index);

private slots:
	/*!
	 * @brief Enables background download timer settings.
	 *
	 * @param[in] checkState Background timer checkbox state.
	 */
	void activateBackgroundTimer(int checkState);

	/*!
	 * @brief Notify user about value change.
	 *
	 * @param[in] state New state.
	 */
	void storeMessagesOnDiskChanged(int state);

	/*!
	 * @brief Notify user about value change.
	 *
	 * @param[in] state New state.
	 */
	void storeAdditionalDataOnDiskChanged(int state);

	/*!
	 * @brief Sets path for attachment saving.
	 */
	void setSavePath(void);

	/*!
	 * @brief Sets path for adding attachments.
	 */
	void setAddFilePath(void);

	/*!
	 * @brief Set PIN value.
	 */
	void setPin(void);

	/*!
	 * @brief Change PIN value.
	 */
	void changePin(void);

	/*!
	 * @brief Clear PIN value.
	 */
	void clearPin(void);

	/*!
	 * @brief Change message deletion notification colour.
	 */
	void setMsgDelColour(void);

	/*!
	 * @brief Change application theme.
	 *
	 * @param[in] index Theme index.
	 */
	void setApplicationThemeIndex(int index);

	/*!
	 * @brief Change application theme - scale fonts.
	 *
	 * @param[in] percent Font scaling factor in percent.
	 */
	void setApplicationThemeFontScale(int percent);

	/*!
	 * @brief Activate attachments file name text line.
	 */
	void activateAllAttachDelivInfoFileNameFmt(void);

	/*!
	 * @brief Show parameter list dialogue.
	 */
	void showParamListDlg(void);

	/*!
	 * @brief Close parameter list dialogue.
	 */
	void closeParamListDlg(void);

	/*!
	 * @brief Close parameter list dialogue when active tab changes.
	 *
	 * @param[in] index Tab index.
	 */
	void onTabChangedCloseParamListDlg(int index);

	/*!
	 * @brief Shows report details.
	 */
	void showBuildReportDetail(void);
	void showConfReportDetail(void);
	void showUsageReportDetail(void);

private:
	/*!
	 * @brief Save dialogue content to settings.
	 *
	 * @param[out] prefs Preferences to be modified.
	 * @param[out] pinSett PIN settings to be modified.
	 */
	void saveSettings(Prefs &prefs, PinSettings &pinSett) const;

	/*!
	 * @brief Initialises the dialogue according to settings.
	 *
	 * @param[in] prefs Preferences according to which to set the dialogue.
	 */
	void initDialogue(const Prefs &prefs);

	/*!
	 * @brief Set PIN button activity.
	 *
	 * @param[in] pinSett PIN settings.
	 */
	void activatePinButtons(const PinSettings &pinSett);

	Ui::DlgPreferences *m_ui; /*!< UI generated from UI file. */

	PinSettings m_pinSett; /*!< PIN settings copy. */

	const bool m_storeMessagesOnDiskOrig; /*!< Whether to store messages on disk, original value. */
	const bool m_storeAdditionalDataOnDiskOrig; /*!< Whether to store other data on disk, original value. */

	QDialog *m_paramListDlg; /*!< Non-null if param list dialogue is active. */
};
