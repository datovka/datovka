/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>
#include <QList>
#include <QString>
#include <QStringList>

#include "src/models/backup_selection_model.h"
#include "src/models/sort_filter_proxy_model.h"

class AcntId; /* Forward declaration. */

namespace Ui {
	class DlgConvertImportFromMobileApp;
}

/*!
 * @brief Dialogue for message database conversions and import.
 */
class DlgConvertImportFromMobileApp : public QDialog {
	Q_OBJECT

public:
	/*!
	 * @brief Destructor.
	 */
	~DlgConvertImportFromMobileApp(void);

	/*!
	 * @brief View the dialogue.
	 *
	 * @param[in] parent Parent widget.
	 */
	static
	void view(QWidget *parent = Q_NULLPTR);

protected:
	/*!
	 * @brief Check window geometry and set table columns width.
	 *
	 * @param[in] event Widget show event.
	 */
	virtual
	void showEvent(QShowEvent *event) Q_DECL_OVERRIDE;

private slots:
	/*!
	 * @brief Change selection according to all checkbox state.
	 *
	 * @param[in] state Selection state.
	 */
	void applyAllSelection(int state);

	/*!
	 * @brief Filter account list.
	 *
	 * @param[in] text Sought text.
	 */
	void filterAccount(const QString &text);

	/*!
	 * @brief Update dialogue when account selection changes.
	 */
	void modifiedAccountSelection(void);

	/*!
	 * @brief Open zip archive.
	 */
	void openZipPackage(void);

	/*!
	 * @brief Convert and split selected message databases.
	 */
	void convertAndSplitMsgDbs(void);

	/*!
	 * @brief Import message databases into application.
	 */
	void importDbs(void);

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent widget.
	 */
	DlgConvertImportFromMobileApp(QWidget *parent = Q_NULLPTR);

	/*!
	 * @brief Show convert and split results.
	 *
	 * @param[in] errorList Error list.
	 */
	void showResults(const QStringList &errorList);

	/*!
	 * @brief Convert message databases from mobile format into desktop.
	 *
	 * @param[in,out] msgDbPaths List of message database paths.
	 * @param[in] destPath Path for converted databases.
	 * @return True if success.
	 */
	bool convertSelectedMsgDbs(QStringList &msgDbPaths,
	    const QString &destPath);

	/*!
	 * @brief Split message databases by years.
	 *
	 * @param[in] msgDbPaths List of message database paths.
	 * @param[in] destPath Path for split databases.
	 * @return True if success.
	 */
	bool splitSelectedMsgDbs(const QStringList &msgDbPaths,
	    const QString &destPath);

	/*!
	 * @brief Unzip selected account files from archive.
	 *
	 * @param[in] zipPath Path to zip archive.
	 * @param[in] unZipPath Path to unzipped files.
	 * @param[in] selectedAcnts Account list.
	 * @return List of paths to unzip message databases.
	 */
	QStringList unZipSelectedAcnts(const QString &zipPath,
	    const QString &unZipPath, const QList<AcntId> &selectedAcnts);

	/*!
	 * @brief Import messages or create accounts from converted databases.
	 *
	 * @param[in] dbPathList List of message database paths.
	 * @return True if success.
	 */
	bool importAccountsFromDbs(const QStringList &dbPathList);

	/*!
	 * @brief Create account from converted databases.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] dbPathList List of message database paths.
	 * @param[out] errTxt Error text.
	 * @return True if success.
	 */
	bool createAccountFromDb(const AcntId &acntId,
	    const QStringList &dbPathList, QString &errTxt);

	Ui::DlgConvertImportFromMobileApp *m_ui; /*!< UI generated from UI file. */

	QString m_zipPath; /*!< ZIP file path. */
	QString m_zfoDbPath; /*!< ZFO database path. */
	QStringList m_msgDbPaths; /*!< List of converted message database paths. */

	SortFilterProxyModel m_accountListProxyModel; /*!<
	                                               * Used for account
	                                               * sorting and filtering.
	                                               */
	QString m_dfltFilerLineStyleSheet; /*!< Used to remember default line edit style. */
	BackupSelectionModel m_accountSelectionModel; /*!< Choose account data to scan. */
};
