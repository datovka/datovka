/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDialog>
#include <QItemSelection>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/identifiers/account_id_db.h"
#include "src/io/message_db.h"
#include "src/models/message_found_model.h"
#include "src/models/sort_filter_proxy_model.h"

class MainWindow;
class QAction;

namespace Ui {
	class DlgMsgSearch;
}

/*!
 * @brief Message search dialogue.
 */
class DlgMsgSearch : public QDialog {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntIdDbList List of available accounts.
	 * @param[in] acntId Account identifier.
	 * @param[in] mw Pointer to main window.
	 * @param[in] parent Parent widget.
	 * @param[in] flags Window flags.
	 */
	DlgMsgSearch(const QList<AcntIdDb> &acntIdDbList, const AcntId &acntId,
	    MainWindow *mw, QWidget *parent = Q_NULLPTR,
	    Qt::WindowFlags flags = Qt::WindowFlags());

	/*!
	 * @brief Destructor.
	 */
	virtual
	~DlgMsgSearch(void);

protected:
	/*!
	 * @brief Check window geometry and set table columns width.
	 *
	 * @param[in] event Widget show event.
	 */
	virtual
	void showEvent(QShowEvent *event) Q_DECL_OVERRIDE;

private slots:
	/*!
	 * @brief Check dialogue elements and enable/disable the search button.
	 */
	void checkInputFields(void);

	/*!
	 * @brief Enable/disable from date selection.
	 */
	void fromDateEnable(bool enable);

	/*!
	 * @brief Enable/disable to date selection.
	 */
	void toDateEnable(bool enable);

	/*!
	 * @brief Reflect date change.
	 */
	void reflectDateChange(void);

	/*!
	 * @brief Collects data from dialogue and searches for messages.
	 */
	void searchMessages(void);

	/*!
	 * @brief Apply filter text on the table.
	 */
	void filterFound(const QString &text);

/* Window state change. */
	/*!
	 * @brief Updates message selection.
	 *
	 * @param[in] selected Newly selected indexes.
	 * @param[in] deselect Deselected indexes.
	 */
	void updateMessageSelection(const QItemSelection &selected,
	    const QItemSelection &deselected = QItemSelection());

/* Mouse on view clicks. */
	/*!
	 * @brief Emit ID of message entry.
	 *
	 * @param[in] index Model index.
	 */
	void messageItemDoubleClicked(const QModelIndex &index);

	/*!
	 * @brief Generates a context menu depending on the item and selection
	 *     it was invoked on.
	 *
	 * @param[in] point Invocation position.
	 */
	void viewMessageListContextMenu(const QPoint &point);

/* Handle database signals. */
	/*!
	 * @brief Update window data when complete message has been written
	 *     to database.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgId Message identifier.
	 * @param[in] direction Sent or received messages
	 *                      (enum MessageDirection).
	 */
	void watchMessageInserted(const AcntId &acntId, const MsgId &msgId,
	    int direction);

	/*!
	 * @brief Update window data when message deleted.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgId Message identifier.
	 */
	void watchMessageDataDeleted(const AcntId &acntId, const MsgId &msgId);

/* Message menu. */
	/*!
	 * @brief Downloads attachments of selected messages.
	 */
	void downloadSelectedMessageAttachments(void);

	/*!
	 * @brief Export selected messages into ZFO files.
	 */
	void exportSelectedMessageZfos(void);

	/*!
	 * @brief Export selected acceptance information as ZFO files.
	 */
	void exportSelectedAcceptanceInfoZfos(void);

	/*!
	 * @brief Export selected acceptance information as PDF files.
	 */
	void exportSelectedAcceptanceInfoPdfs(void);

	/*!
	 * @brief Export selected message envelopes as PDF files.
	 */
	void exportSelectedEnvelopePdfs(void);

	/*!
	 * @brief Export selected message envelopes as PDF and attachment files.
	 */
	void exportSelectedEnvelopeAttachments(void);

	/*!
	 * @brief Sends selected messages as ZFO into default e-mail client.
	 */
	void createEmailWithZfos(void);

	/*!
	 * @brief Sends all attachments of selected messages into default
	 *     e-mail client.
	 */
	void createEmailWithAllAttachments(void);

	/*!
	 * @brief Sends selected message content into default e-mail client.
	 */
	void createEmailContentSelection(void);

signals:
	/*!
	 * @brief Signals that a message was selected should be focused
	 *     elsewhere.
	 *
	 * @param[in] username Username identifying an account.
	 * @param[in] testing True if this is an testing account.
	 * @param[in] dmId Message identifier.
	 * @param[in] year Message delivery year.
	 * @param[in] msgType Message type.
	 */
	void focusSelectedMsg(AcntId acntId, qint64 dmId, QString year,
	    int msgType);

private:
	/*!
	 * @brief Set up menu actions.
	 */
	void setupActions(void);

	/*!
	 * @brief Initialise message search dialogue.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void initSearchWindow(const AcntId &acntId);

	/*!
	 * @brief Connect database container signals to appropriate slots.
	 */
	void databaseConnectActions(void);

	/*!
	 * @brief Computes filled-in fields except the tag field.
	 *
	 * @return Number of filled-in fields except the tag field.
	 */
	int filledInExceptTags(void) const;

	/*!
	 * @brief Enable actions according to status.
	 */
	void updateActionActivation(void);

	Ui::DlgMsgSearch *m_ui; /*!< UI generated from UI file. */

	QAction *m_actionDownloadMessage; /*!< Download message. */
	QAction *m_actionExportMessageZfo; /*!< Export message as ZFO. */
	QAction *m_actionExportAcceptanceInfoZfo; /*!< Export acceptance info as ZFO. */
	QAction *m_actionExportAcceptanceInfoPdf; /*!< Export acceptance info as PDF. */
	QAction *m_actionExportEnvelopePdf; /*!< Export message enveloper as PDF. */
	QAction *m_actionExportEnvelopePdfAndAttachments; /*!< Export envelope PDF with attachments. */
	QAction *m_actionEmailZfos; /*!< E-mail with ZFOs. */
	QAction *m_actionEmailAllAttachments; /*!< E-mail with all attachments. */
	QAction *m_actionEmailContentSelection; /*!< E-mail */

	MainWindow *m_mw;  /*!< Pointer to main window. */

	QList<int> m_modelRowSelection; /*!< Holds the message selection. */

	const QList<AcntIdDb> m_acntIdDbList; /*!< Available accounts.*/

	SortFilterProxyModel m_foundListProxyModel; /*!<
	                                             * Used for message
	                                             * sorting and filtering.
	                                             */
	QString m_dfltFilerLineStyleSheet; /*!< Used to remember default line edit style. */
	MsgFoundModel m_foundTableModel; /*!< Model of found message table. */

	QSize m_dfltSize; /*!< Remembered dialogue default size. */
};
