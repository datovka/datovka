/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QPushButton>

#include "src/datovka_shared/settings/prefs.h"
#include "src/dimensions/dimensions.h"
#include "src/global.h"
#include "src/gui/dlg_email_content.h"
#include "src/gui/message_operations.h"
#include "src/settings/prefs_specific.h"
#include "ui_dlg_email_content.h"

DlgEmailContent::DlgEmailContent(QWidget *parent)
    : QDialog(parent),
    m_ui(new (::std::nothrow) Ui::DlgEmailContent)
{
	m_ui->setupUi(this);

	connect(m_ui->messageZfoCheck, SIGNAL(stateChanged(int)),
	    this, SLOT(enableOkButton()));
	connect(m_ui->deliveryInfoZfoCheck, SIGNAL(stateChanged(int)),
	    this, SLOT(enableOkButton()));
	connect(m_ui->allAttachmentsCheck, SIGNAL(stateChanged(int)),
	    this, SLOT(enableOkButton()));

	connect(GlobInstcs::prefsPtr, SIGNAL(entrySet(int, QString, QVariant)),
	    this, SLOT(watchPrefsModification(int, QString, QVariant)));

	loadWindowControls(*GlobInstcs::prefsPtr);
	enableOkButton();
}

DlgEmailContent::~DlgEmailContent(void)
{
	delete m_ui;
}

void DlgEmailContent::createEmail(const QList<MsgOrigin> &originList,
    QWidget *parent)
{
	DlgEmailContent dlg(parent);

	const QString dlgName("email_content");
	const QSize dfltSize = dlg.size();
	{
		const QSize newSize = Dimensions::dialogueSize(&dlg,
		    PrefsSpecific::dlgSize(*GlobInstcs::prefsPtr, dlgName),
		    dfltSize);
		if (newSize.isValid()) {
			dlg.resize(newSize);
		}
	}

	if (QDialog::Accepted == dlg.exec()) {
		dlg.saveWindowControls(*GlobInstcs::prefsPtr);

		GuiMsgOps::EmailContents attchFlags = GuiMsgOps::ADD_NOTHING;
		if (dlg.m_ui->allAttachmentsCheck->isChecked()) {
			attchFlags |= GuiMsgOps::ADD_ATTACHMENTS;
		}
		if (dlg.m_ui->messageZfoCheck->isChecked()) {
			attchFlags |= GuiMsgOps::ADD_ZFO_MESSAGE;
		}
		if (dlg.m_ui->deliveryInfoZfoCheck->isChecked()) {
			attchFlags |= GuiMsgOps::ADD_ZFO_DELIVERY_INFO;
		}
		GuiMsgOps::createEmail(originList, attchFlags);
	}

	PrefsSpecific::setDlgSize(*GlobInstcs::prefsPtr, dlgName,
	    dlg.size(), dfltSize);
}

void DlgEmailContent::enableOkButton(void)
{
	bool enabled = false;

	enabled = enabled || m_ui->messageZfoCheck->isChecked();
	enabled = enabled || m_ui->deliveryInfoZfoCheck->isChecked();
	enabled = enabled || m_ui->allAttachmentsCheck->isChecked();

	m_ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(enabled);
}

static const QString controlStrMessageZfo("window.email_content.message_zfo.checked");
static const QString controlStrDelInfoZfo("window.email_content.delivery_info_zfo.checked");
static const QString controlStrAllAttachs("window.email_content.all_attachments.checked");

void DlgEmailContent::watchPrefsModification(int valueType, const QString &name,
    const QVariant &value)
{
	switch (valueType) {
	case PrefsDb::VAL_BOOLEAN:
		if (name == controlStrMessageZfo) {
			m_ui->messageZfoCheck->setChecked(value.toBool());
		} else if (name == controlStrDelInfoZfo) {
			m_ui->deliveryInfoZfoCheck->setChecked(value.toBool());
		} else if (name == controlStrAllAttachs) {
			m_ui->allAttachmentsCheck->setChecked(value.toBool());
		}
		break;
	default:
		/* Do nothing. */
		break;
	}
}

void DlgEmailContent::loadWindowControls(const Prefs &prefs)
{
	bool val = false;

	if (prefs.boolVal(controlStrMessageZfo, val)) {
		m_ui->messageZfoCheck->setChecked(val);
	}
	if (prefs.boolVal(controlStrDelInfoZfo, val)) {
		m_ui->deliveryInfoZfoCheck->setChecked(val);
	}
	if (prefs.boolVal(controlStrAllAttachs, val)) {
		m_ui->allAttachmentsCheck->setChecked(val);
	}
}

void DlgEmailContent::saveWindowControls(Prefs &prefs) const
{
	prefs.setBoolVal(controlStrMessageZfo, m_ui->messageZfoCheck->isChecked());
	prefs.setBoolVal(controlStrDelInfoZfo, m_ui->deliveryInfoZfoCheck->isChecked());
	prefs.setBoolVal(controlStrAllAttachs, m_ui->allAttachmentsCheck->isChecked());
}
