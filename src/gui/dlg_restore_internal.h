/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QObject>
#include <QString>

/*!
 * @brief Encapsulates the restore function to be run in a separate thread.
 */
class RestoreWorker : public QObject {
	Q_OBJECT

public:
	/*!
	 * @brief Describes the restoration process.
	 */
	class RestorationDescr {
	public:
		/*!
		 * @brief Describes message database restoration.
		 */
		class MsgDb {
		public:
			MsgDb(void)
			    : createNew(false), testEnv(false),
			    srcAcntName(), srcUname(), srcDir(), srcFiles(), srcAssocDirs(),
			    tgtAcntName(), tgtUname(), tgtDir(), tgtFiles()
			{ }

			bool createNew; /*!< True if account should be created. */
			bool testEnv; /*!< True for testing environment. */
			QString srcAcntName; /*!< Source account name. */
			QString srcUname; /*!< Source database user name. */
			QString srcDir; /*!< Full path to directory where source files reside. */
			QList<QString> srcFiles; /*!< Source file names. */
			QList<QString> srcAssocDirs; /*!< Source associated directories. */
			QString tgtAcntName; /*!< Target account name. */
			QString tgtUname; /*!< Target database user name. */
			QString tgtDir; /*!< Full path to directory where target files reside. */
			QList<QString> tgtFiles; /*!< Target file names. */
		};

		RestorationDescr(void)
		    : backupDir(), configDir(), saveConfPath(), msgDbs(),
		      tagDbFile(), runningTagDbFilePath(),
		      accountDbFile(), runningAccountDbFilePath()
		{ }

		QString backupDir; /*!< Full path to back-up location where to take data from. */
		QString configDir; /*!< Full path to configuration location to be restored. */
		QString saveConfPath; /*!< Full path to configuration file which should be saved. */

		QList<MsgDb> msgDbs; /*!< Message databases to be restored. */

		QString tagDbFile; /*!< File name (relative to \a backupDir) to restore the tag database from. */
		QString runningTagDbFilePath; /*!< Full path to running tag database. */

		QString accountDbFile; /*!< File name (relative to \a backupDir) to restore the account database from. */
		QString runningAccountDbFilePath; /*!< Full path to running account database. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Object.
	 */
	explicit RestoreWorker(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Set restoration descriptor.
	 *
	 * @note The object does not take ownership of the passed object.
	 *     The passed object must be available through the entire duration
	 *     of the life cycle of this object.
	 *
	 * @param[in] descr Pointer to descriptor structure.
	 */
	void setDescr(const RestorationDescr *descr);

public slots:
	/*!
	 * @brief Main thread function.
	 */
	void process(void);

	/*!
	 * @brief Call this method to signalise that the loop should be aborted.
	 */
	void setBreakRestoreLoop(void);

signals:
	/*!
	 * @brief Emitted when restoration is started.
	 *
	 * @param[in] min Minimal progress value.
	 * @param[in] max Maximal progress value.
	 */
	void started(int min, int max);

	/*!
	 * @brief Emitted during restoration.
	 *
	 * @param[in] val Progress value.
	 */
	void progress(int val);

	/*!
	 * @brief Emitted during restoration.
	 *
	 * @param[in] name Back-up task name.
	 */
	void working(const QString &name);

	/*!
	 * @brief Emitted on main thread exit.
	 */
	void finished(void);

	/*!
	 * @brief Emitted before exit when everything finished successfully.
	 */
	void succeeded(void);

	/*!
	 * @brief Emitted before exit when something failed.
	 */
	void failed(void);

	/*!
	 * @brief Emitted before exit when something failed.
	 *
	 * @param[in] title Error title.
	 * @param[in] text Error description.
	 */
	void warning(const QString &title, const QString &text);

private:
	const RestorationDescr *m_descr; /*!< Brief restoration descriptor. */

	volatile bool m_breakRestoreLoop; /*!< Setting to true interrupts restoration loop. */
};
