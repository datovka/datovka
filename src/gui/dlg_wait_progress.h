/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QProgressDialog>
#include <QString>
#include <QTimer>
#include <QWidget>

/*!
 * @brief Dialogue for the wait/busy visualisation.
 */
class DlgWaitProgress: public QProgressDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] ms Waiting time in milliseconds.
	 * @param[in] text Label text.
	 * @param[in] parent Parent object.
	 */
	DlgWaitProgress(int ms, const QString &text,
	    QWidget *parent = Q_NULLPTR);

public:
	/*!
	 * @brief Show waiting progress dialogue.
	 *
	 * @note For short intervals the dialogue may not be shown at all.
	 *
	 * @param[in] ms Waiting time in milliseconds.
	 * @param[in] text Label text.
	 * @param[in] parent Parent object.
	 * @return False if the user aborted the progress by closing the dialogue.
	 */
	static
	bool wait(int ms, const QString &text, QWidget *parent = Q_NULLPTR);

private slots:
	/*!
	 * @brief Increment progress. Accepts the dialogue when limit reached.
	 */
	void incrementProgress(void);

	/*!
	 * @brief Stop the internal timer.
	 */
	void stopTimer(void);

private:
	QTimer m_timer; /*!< Progress timer. */
};
