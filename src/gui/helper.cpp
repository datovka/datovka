/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/worker/pool.h"
#include "src/global.h"
#include "src/gui/datovka.h"
#include "src/gui/helper.h"
#include "src/io/isds_sessions.h"
#include "src/worker/task_keep_alive.h"

bool GuiHelper::isLoggedIn(MainWindow *const mw, const AcntId &acntId)
{
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return false;
	}

	bool loggedIn = false;
	{
		TaskKeepAlive *task =
		    new (::std::nothrow) TaskKeepAlive(acntId);
		if (Q_UNLIKELY(task == Q_NULLPTR)) {
			return false;
		}
		task->setAutoDelete(false);
		if (Q_UNLIKELY(!GlobInstcs::workPoolPtr->runSingle(task))) {
			delete task; task = Q_NULLPTR;
			logWarningNL("%s", "Cannot execute task.");
			return false;
		}

		loggedIn = task->m_isAlive;

		delete task; task = Q_NULLPTR;
	}
	if (!loggedIn) {
		if (Q_NULLPTR != mw) {
			loggedIn = mw->connectToIsds(acntId);
		}
	}

	/* Check the presence of session. */
	if (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId.username())) {
		logErrorNL("%s", "Missing ISDS session.");
		loggedIn = false;
	}

	return loggedIn;
}

MessageDbSet *GuiHelper::getDbSet(const QList<AcntIdDb> &acntIdDbList,
    const AcntId &acntId)
{
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	MessageDbSet *dbSet = Q_NULLPTR;
	for (const AcntIdDb &acntIdDb : acntIdDbList) {
		if ((acntIdDb.username() == acntId.username()) &&
		    (acntIdDb.testing() == acntId.testing())) {
			dbSet = acntIdDb.messageDbSet();
			break;
		}
	}

	return dbSet;
}
