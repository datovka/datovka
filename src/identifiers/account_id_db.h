/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMetaType>

#include "src/datovka_shared/identifiers/account_id.h"

class AcntIdDbPrivate;
/*!
 * @brief Holds internal account identifier and pointer to message database set.
 */
class AcntIdDb : public AcntId {
	Q_DECLARE_PRIVATE(AcntIdDb)

public:
	/*
	 * Don't forget to declare types in order to make them work with
	 * the meta-object system.
	 */
	static
	void declareTypes(void);

	/*!
	 * @brief Constructor.
	 */
	AcntIdDb(void);
	AcntIdDb(const AcntIdDb &other);
#ifdef Q_COMPILER_RVALUE_REFS
	AcntIdDb(AcntIdDb &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	virtual /* To keep the compiler quiet, the destructor needs not to be virtual. */
	~AcntIdDb(void);

	explicit AcntIdDb(const QString &username, bool testing,
	    class MessageDbSet *dbSet);
#ifdef Q_COMPILER_RVALUE_REFS
	explicit AcntIdDb(QString &&username, bool testing,
	    class MessageDbSet *dbSet);
#endif /* Q_COMPILER_RVALUE_REFS */
	explicit AcntIdDb(const AcntId &acntId, class MessageDbSet *dbSet);

	AcntIdDb &operator=(const AcntIdDb &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	AcntIdDb &operator=(AcntIdDb &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const AcntIdDb &other) const;
	bool operator!=(const AcntIdDb &other) const;

	friend void swap(AcntIdDb &first, AcntIdDb &second) Q_DECL_NOTHROW;

	bool isValid(void) const;

	class MessageDbSet *messageDbSet(void) const;
	void setMessageDbSet(class MessageDbSet *s);

protected:
	/* This class uses the d_ptr from its parent class. */
	/* Allow subclasses to initialise with their own specific Private. */
	AcntIdDb(AcntIdDbPrivate *d);

private:
	/* Allow parent code to instantiate *Private subclass. */
	virtual
	bool ensurePrivate(void) Q_DECL_OVERRIDE;
};

void swap(AcntIdDb &first, AcntIdDb &second) Q_DECL_NOTHROW;

Q_DECLARE_METATYPE(AcntIdDb)
