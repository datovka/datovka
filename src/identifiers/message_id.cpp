/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDateTime>
#include <QHash> /* ::qHash */
#include <QString>
#include <QStringBuilder>
#include <utility> /* ::std::swap */

#include "src/identifiers/message_id.h"

#define nullDmId -1
static const QDateTime nullDateTime;

/*!
 * @brief PIMPL MsgId class.
 */
class MsgIdPrivate {
	//Q_DISABLE_COPY(MsgIdPrivate)
public:
	MsgIdPrivate(void)
	    : m_dmId(nullDmId), m_deliveryTime()
	{ }

	MsgIdPrivate &operator=(const MsgIdPrivate &other) Q_DECL_NOTHROW
	{
		m_dmId = other.m_dmId;
		m_deliveryTime = other.m_deliveryTime;

		return *this;
	}

	bool operator==(const MsgIdPrivate &other) const
	{
		return (m_dmId == other.m_dmId) &&
		    (m_deliveryTime == other.m_deliveryTime);
	}

	qint64 m_dmId; /*!< Message identifier. */
	QDateTime m_deliveryTime; /*!< Message delivery time. */
};

void MsgId::declareTypes(void)
{
	qRegisterMetaType<MsgId>("MsgId");
	qRegisterMetaType< QList<MsgId> >("QList<MsgId>");
}

MsgId::MsgId(void)
    : d_ptr(Q_NULLPTR)
{
}

MsgId::MsgId(const MsgId &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) MsgIdPrivate) : Q_NULLPTR)
{
	Q_D(MsgId);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
MsgId::MsgId(MsgId &&other) Q_DECL_NOEXCEPT
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    : d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    : d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

MsgId::~MsgId(void)
{
}

MsgId::MsgId(qint64 dmId, const QDateTime &deliveryTime)
    : d_ptr(new (::std::nothrow) MsgIdPrivate)
{
	Q_D(MsgId);
	d->m_dmId = dmId;
	d->m_deliveryTime = deliveryTime;
}

#ifdef Q_COMPILER_RVALUE_REFS
MsgId::MsgId(qint64 dmId, QDateTime &&deliveryTime)
    : d_ptr(new (::std::nothrow) MsgIdPrivate)
{
	Q_D(MsgId);
	d->m_dmId = dmId;
	d->m_deliveryTime = ::std::move(deliveryTime);
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures private identifier presence.
 *
 * @note Returns if private identifier could not be allocated.
 */
#define ensureMsgIdPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			MsgIdPrivate *p = new (::std::nothrow) MsgIdPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

MsgId &MsgId::operator=(const MsgId &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureMsgIdPrivate(*this);
	Q_D(MsgId);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
MsgId &MsgId::operator=(MsgId &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool MsgId::operator==(const MsgId &other) const
{
	Q_D(const MsgId);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool MsgId::operator!=(const MsgId &other) const
{
	return !operator==(other);
}

bool MsgId::isNull(void) const
{
	Q_D(const MsgId);
	return d == Q_NULLPTR;
}

bool MsgId::isValid(void) const
{
	return !isNull() && (dmId() >= 0) && deliveryTime().isValid();
}

qint64 MsgId::dmId(void) const
{
	Q_D(const MsgId);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullDmId;
	}

	return d->m_dmId;
}

void MsgId::setDmId(qint64 di)
{
	ensureMsgIdPrivate();
	Q_D(MsgId);
	d->m_dmId = di;
}

const QDateTime &MsgId::deliveryTime(void) const
{
	Q_D(const MsgId);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullDateTime;
	}

	return d->m_deliveryTime;
}

void MsgId::setDeliveryTime(const QDateTime &dt)
{
	ensureMsgIdPrivate();
	Q_D(MsgId);
	d->m_deliveryTime = dt;
}

#ifdef Q_COMPILER_RVALUE_REFS
void MsgId::setDeliveryTime(QDateTime &&dt)
{
	ensureMsgIdPrivate();
	Q_D(MsgId);
	d->m_deliveryTime = ::std::move(dt);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void swap(MsgId &first, MsgId &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

/*!
 * @brief Serialises identified data into a string.
 *
 * @param[in] dmId Message identifier number.
 * @param[in] deliveryTime Message delivery time.
 * @return Serialised data.
 */
static
QString msgIdStr(qint64 dmId, const QDateTime &deliveryTime)
{
	return QString::number(dmId) % QStringLiteral("_") %
	    (deliveryTime.isValid() ?
	        QString::number(deliveryTime.toMSecsSinceEpoch()) :
	        QStringLiteral("invalid"));
}

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
size_t qHash(const MsgId &key, size_t seed)
#else /* < Qt-6.0 */
uint qHash(const MsgId &key, uint seed)
#endif /* >= Qt-6.0 */
{
	return ::qHash(msgIdStr(key.dmId(), key.deliveryTime()), seed);
}
