/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMetaType>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

class QDateTime; /* Forward declaration. */

class MsgIdPrivate;
/*!
 * @brief Holds internal message identifier.
 */
class MsgId {
	Q_DECLARE_PRIVATE(MsgId)
public:
	/*
	 * Don't forget to declare types in order to make them work with
	 * the meta-object system.
	 */
	static
	void declareTypes(void);

	MsgId(void);
	MsgId(const MsgId &other);
#ifdef Q_COMPILER_RVALUE_REFS
	MsgId(MsgId &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	~MsgId(void);

	explicit MsgId(qint64 dmId, const QDateTime &deliveryTime);
#ifdef Q_COMPILER_RVALUE_REFS
	explicit MsgId(qint64 dmId, QDateTime &&deliveryTime);
#endif /* Q_COMPILER_RVALUE_REFS */

	MsgId &operator=(const MsgId &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	MsgId &operator=(MsgId &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const MsgId &other) const;
	bool operator!=(const MsgId &other) const;

	friend void swap(MsgId &first, MsgId &second) Q_DECL_NOTHROW;

	bool isNull(void) const;

	bool isValid(void) const;

	qint64 dmId(void) const;
	void setDmId(qint64 di);

	const QDateTime &deliveryTime(void) const;
	void setDeliveryTime(const QDateTime &dt);
#ifdef Q_COMPILER_RVALUE_REFS
	void setDeliveryTime(QDateTime &&dt);
#endif /* Q_COMPILER_RVALUE_REFS */

private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
	::std::unique_ptr<MsgIdPrivate> d_ptr;
#else /* < Qt-5.12 */
	QScopedPointer<MsgIdPrivate> d_ptr;
#endif /* >= Qt-5.12 */
};

void swap(MsgId &first, MsgId &second) Q_DECL_NOTHROW;

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
size_t qHash(const MsgId &key, size_t seed = 0);
#else /* < Qt-6.0 */
uint qHash(const MsgId &key, uint seed = 0);
#endif /* >= Qt-6.0 */

Q_DECLARE_METATYPE(MsgId)
