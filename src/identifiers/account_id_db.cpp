/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/identifiers/account_id_p.h"
#include "src/identifiers/account_id_db.h"

/*!
 * @brief PIMPL AcntIdDb class.
 */
class AcntIdDbPrivate : public AcntIdPrivate {
	//Q_DISABLE_COPY(AcntIdDbPrivate)
public:
	AcntIdDbPrivate(void)
	    : AcntIdPrivate(), m_messageDbSet(Q_NULLPTR)
	{ }

	virtual
	~AcntIdDbPrivate(void)
	{ }

	AcntIdDbPrivate &operator=(const AcntIdDbPrivate &other) Q_DECL_NOTHROW
	{
		AcntIdPrivate::operator=(other);
		m_messageDbSet = other.m_messageDbSet;

		return *this;
	}

	bool operator==(const AcntIdDbPrivate &other) const
	{
		return AcntIdPrivate::operator==(other) &&
		    m_messageDbSet == other.m_messageDbSet;
	}

	class MessageDbSet *m_messageDbSet;
};

void AcntIdDb::declareTypes(void)
{
	qRegisterMetaType<AcntIdDb>("AcntIdDb");
}

AcntIdDb::AcntIdDb(void)
    : AcntId()
{
}

AcntIdDb::AcntIdDb(const AcntIdDb &other)
    : AcntId((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) AcntIdDbPrivate) : Q_NULLPTR)
{
	Q_D(AcntIdDb);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
AcntIdDb::AcntIdDb(AcntIdDb &&other) Q_DECL_NOEXCEPT
    : AcntId(::std::move(other))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

AcntIdDb::~AcntIdDb(void)
{
}

AcntIdDb::AcntIdDb(const QString &username, bool testing,
    class MessageDbSet *dbSet)
    : AcntId(new (::std::nothrow) AcntIdDbPrivate)
{
	Q_D(AcntIdDb);
	d->m_username = username;
	d->m_testing = testing;
	d->updateStrId();
	d->m_messageDbSet = dbSet;
}

#ifdef Q_COMPILER_RVALUE_REFS
AcntIdDb::AcntIdDb(QString &&username, bool testing,
    class MessageDbSet *dbSet)
    : AcntId(new (::std::nothrow) AcntIdDbPrivate)
{
	Q_D(AcntIdDb);
	d->m_username = ::std::move(username);
	d->m_testing = testing;
	d->updateStrId();
	d->m_messageDbSet = dbSet;
}
#endif /* Q_COMPILER_RVALUE_REFS */

AcntIdDb::AcntIdDb(const AcntId &acntId, class MessageDbSet *dbSet)
    : AcntId(new (::std::nothrow) AcntIdDbPrivate)
{
	Q_D(AcntIdDb);
	d->m_username = acntId.username();
	d->m_testing = acntId.testing();
	d->updateStrId();
	d->m_messageDbSet = dbSet;
}

/*!
 * @brief Ensures private account identifier presence.
 *
 * @note Returns if private account identifier could not be allocated.
 */
#define ensureAcntIdDbPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(!ensurePrivate())) { \
			return _x_; \
		} \
	} while (0)

AcntIdDb &AcntIdDb::operator=(const AcntIdDb &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureAcntIdDbPrivate(*this);
	Q_D(AcntIdDb);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
AcntIdDb &AcntIdDb::operator=(AcntIdDb &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool AcntIdDb::operator==(const AcntIdDb &other) const
{
	Q_D(const AcntIdDb);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool AcntIdDb::operator!=(const AcntIdDb &other) const
{
	return !operator==(other);
}

bool AcntIdDb::isValid(void) const
{
	return AcntId::isValid() && (messageDbSet() != Q_NULLPTR);
}

class MessageDbSet *AcntIdDb::messageDbSet(void) const
{
	Q_D(const AcntIdDb);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Q_NULLPTR;
	}

	return d->m_messageDbSet;
}

void AcntIdDb::setMessageDbSet(class MessageDbSet *s)
{
	ensureAcntIdDbPrivate();
	Q_D(AcntIdDb);
	d->m_messageDbSet = s;
}

AcntIdDb::AcntIdDb(AcntIdDbPrivate *d)
    : AcntId(d)
{
}

bool AcntIdDb::ensurePrivate(void)
{
	if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) {
		AcntIdDbPrivate *p = new (::std::nothrow) AcntIdDbPrivate;
		if (Q_UNLIKELY(p == Q_NULLPTR)) {
			Q_ASSERT(0);
			return false;
		}
		d_ptr.reset(p);
	}
	return true;
}

void swap(AcntIdDb &first, AcntIdDb &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
