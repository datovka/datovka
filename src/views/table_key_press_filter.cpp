/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QEvent>
#include <QKeyEvent>

#include "src/views/table_key_press_filter.h"

TableKeyPressFilter::TableKeyPressFilter(QObject *parent)
    : QObject(parent),
    m_keyActions()
{ }

bool TableKeyPressFilter::eventFilter(QObject *object, QEvent *event)
{
	Q_UNUSED(object);

	const QKeyEvent *ke = Q_NULLPTR;
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
	QHash<QKeyCombination, ActionToDo>::const_iterator cIt = m_keyActions.constEnd();
#else /* < Qt-6.0 */
	QMap<int, ActionToDo>::const_iterator cIt = m_keyActions.constEnd();
#endif /* >= Qt-6.0 */

	if (event->type() == QEvent::KeyPress) {
		ke = (QKeyEvent *)event;
	}

	if (Q_NULLPTR != ke) {
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
		cIt = m_keyActions.constFind(ke->keyCombination());
#else /* < Qt-6.0 */
		int modifiers = ke->modifiers();
		int key = ke->key();

		cIt = m_keyActions.constFind(modifiers | key);
#endif /* >= Qt-6.0 */
	}

	if (m_keyActions.constEnd() != cIt) {
		void (*actionFuncPtr)(QObject *) = cIt->actionFuncPtr;
		QObject *actionObj = cIt->actionObj;

		Q_ASSERT(Q_NULLPTR != actionFuncPtr);

		actionFuncPtr(actionObj);
		if (cIt->blockEvent) {
			return true;
		}
	}

	return QObject::eventFilter(object, event);
}

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
void TableKeyPressFilter::registerAction(QKeyCombination keyComb,
    void (*func)(QObject *), QObject *obj, bool stop)
{
	if (Q_NULLPTR != func) {
		m_keyActions.insert(keyComb, ActionToDo(func, obj, stop));
	} else {
		/* Delete entry for given key combination. */
		m_keyActions.remove(keyComb);
	}
}
#else /* < Qt-6.0 */
void TableKeyPressFilter::registerAction(int keyComb, void (*func)(QObject *),
    QObject *obj, bool stop)
{
	if (Q_NULLPTR != func) {
		m_keyActions.insert(keyComb, ActionToDo(func, obj, stop));
	} else {
		/* Delete entry for given key combination. */
		m_keyActions.remove(keyComb);
	}
}
#endif /* >= Qt-6.0 */
