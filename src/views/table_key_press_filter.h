/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QtGlobal> /* QT_VERSION_CHECK */
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
#  include <QHash>
#  include <QKeyCombination>
#else /* < Qt-6.0 */
#  include <QMap>
#endif /* >= Qt-6.0 */
#include <QObject>

/*!
 * @brief This object is used as to tweak the behaviour of a QTableView and
 *    QTableWidget when selected key is pressed.
 */
class TableKeyPressFilter : public QObject {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit TableKeyPressFilter(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Event filter function.
	 *
	 * @note The function catches the given keys and calls given action
	 *     functions.
	 *
	 * @param[in,out] object View object.
	 * @param[in]     event Caught event.
	 * @return True if further processing of event should be blocked.
	 */
	virtual
	bool eventFilter(QObject *object, QEvent *event) Q_DECL_OVERRIDE;

	/*!
	 * @brief Register an action.
	 *
	 * @note Should \p func be 0 then the action is deleted.
	 *
	 * @param[in] keyComb Code of the modifier (Qt::Modifiers) and key (Qt::Key)
	 *                    to register the action for.
	 * @param[in] func Action function.
	 * @param[in] obj Action function parameter.
	 * @param[in] stop Set true if related event should be prevented
	 *                 from further processing.
	 */
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
	void registerAction(QKeyCombination keyComb, void (*func)(QObject *),
	    QObject *obj, bool stop);
#else /* < Qt-6.0 */
	void registerAction(int keyComb, void (*func)(QObject *), QObject *obj,
	    bool stop);
#endif /* >= Qt-6.0 */

private:
	/*!
	 * @brief Holds function and object for specified action.
	 *
	 * @note The class does not take possession of the pointers, it only
	 *     uses the copies.
	 */
	class ActionToDo {
	public:
		/*!
		 * @brief Constructor.
		 *
		 * @param[in] func Action function.
		 * @param[in] obj Action function parameter.
		 * @param[in] stop Set true if related event should be prevented
		 *                 from further processing.
		 */
		ActionToDo(void (*func)(QObject *), QObject *obj, bool stop)
		    : actionFuncPtr(func), actionObj(obj), blockEvent(stop)
		{ }

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] other Action description.
		 */
		ActionToDo(const ActionToDo &other)
		    : actionFuncPtr(other.actionFuncPtr),
		    actionObj(other.actionObj),
		    blockEvent(other.blockEvent)
		{ }

		/*!
		 * @brief Assignment operator.
		 *
		 * @param[in] other Action description.
		 */
		ActionToDo &operator=(const ActionToDo &other) Q_DECL_NOTHROW
		{
			actionFuncPtr = other.actionFuncPtr;
			actionObj = other.actionObj;
			blockEvent = other.blockEvent;

			return *this;
		}

		void (*actionFuncPtr)(QObject *); /*!< Pointer to a function. */
		QObject *actionObj; /*!< Parameter of the action function. */
		bool blockEvent; /*!< Set true to stop the key event being handled further. */
	};

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
	QHash<QKeyCombination, ActionToDo> m_keyActions; /*!< Registered actions. */
#else /* < Qt-6.0 */
	QMap<int, ActionToDo> m_keyActions; /*!< Registered actions. */
#endif /* >= Qt-6.0 */
};
