/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <cstdlib> /* ::std::malloc */
#include <cstring> /* ::std::memset */
#include <libdatovka/isds.h>

#include "src/datovka_shared/isds/internal_conversion.h"
#include "src/isds/generic_conversion.h"

/*!
 * @brief Converts enum isds_status_type into
 *     enum Isds::Type::ResponseStatusType.
 */
static
enum Isds::Type::ResponseStatusType libisdsStatusType2ResponseStatusType(
    enum isds_status_type ist, bool *ok = Q_NULLPTR)
{
	bool iOk = true;
	enum Isds::Type::ResponseStatusType type = Isds::Type::RST_UNKNOWN;

	switch (ist) {
	case STAT_DB: type = Isds::Type::RST_DB; break;
	case STAT_DM: type = Isds::Type::RST_DM; break;
	default:
		Q_ASSERT(0);
		iOk = false;
		type = Isds::Type::RST_UNKNOWN;
		break;
	}

	if (ok != Q_NULLPTR) {
		*ok = iOk;
	}
	return type;
}

Isds::Status Isds::libisds2status(const struct ::isds_status *is, bool *ok)
{
	if (Q_UNLIKELY(is == NULL)) {
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return Status();
	}

	bool iOk = false;
	Status status;

	status.setType(libisdsStatusType2ResponseStatusType(is->type, &iOk));
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	status.setCode(Isds::fromCStr((is->code)));
	status.setMessage(Isds::fromCStr((is->message)));
	status.setRefNum(Isds::fromCStr((is->ref_number)));

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return status;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Status();
}

/*!
 * @brief Converts enum Isds::Type::ResponseStatusType into
 *     enum isds_status_type.
 */
static
enum isds_status_type responseStatusType2libisdsStatusType(
    enum Isds::Type::ResponseStatusType st, bool *ok = Q_NULLPTR)
{
	bool iOk = true;
	enum isds_status_type ist = STAT_DB;

	switch (st) {
	/*
	 * Isds::Type::RST_UNKNOWN is handled with the dafault target.
	 */
	case Isds::Type::RST_DB: ist = STAT_DB; break;
	case Isds::Type::RST_DM: ist = STAT_DM; break;
	default:
		Q_ASSERT(0);
		iOk = false;
		break;
	}

	if (ok != Q_NULLPTR) {
		*ok = iOk;
	}
	return ist;
}

/*!
 * @brief Set isds_status structure according to the Status.
 */
static
bool setLibisdsStatusContent(struct isds_status *tgt, const Isds::Status &src)
{
	if (Q_UNLIKELY(tgt == NULL)) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;

	tgt->type = responseStatusType2libisdsStatusType(src.type(), &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}
	if (Q_UNLIKELY(!Isds::toCStrCopy(&tgt->code, src.code()))) {
		return false;
	}
	if (Q_UNLIKELY(!Isds::toCStrCopy(&tgt->message, src.message()))) {
		return false;
	}
	if (Q_UNLIKELY(!Isds::toCStrCopy(&tgt->ref_number, src.refNum()))) {
		return false;
	}

	return true;
}

struct ::isds_status *Isds::status2libisds(const Status &s, bool *ok)
{
	if (Q_UNLIKELY(s.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return NULL;
	}

	struct isds_status *is = (struct isds_status *)::std::malloc(sizeof(*is));
	if (Q_UNLIKELY(is == NULL)) {
		Q_ASSERT(0);
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return NULL;
	}
	::std::memset(is, 0, sizeof(*is));

	if (Q_UNLIKELY(!setLibisdsStatusContent(is, s))) {
		isds_status_free(&is);
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return NULL;
	}
	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return is;
}
