/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <libdatovka/isds.h>

#include "src/datovka_shared/isds/internal_conversion.h"
#include "src/isds/box_conversion2.h"
#include "src/isds/internal_type_conversion.h"
#include "src/isds/message_conversion2.h"

Isds::DmMessageAuthor Isds::libisdsMessageAuthor2MessageAuthor(
    const struct isds_dmMessageAuthor *ima, bool *ok)
{
	if (Q_UNLIKELY(ima == NULL)) {
		Q_ASSERT(0);
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return DmMessageAuthor();
	}

	DmMessageAuthor ma;

	ma.setUserType(IsdsInternal::libisdsSenderType2SenderType(ima->userType));
	ma.setPersonName(libisds2personName2(ima->personName));
	ma.setBiDate(dateFromStructTM(ima->biDate));
	ma.setBiCity(fromCStr(ima->biCity));
	ma.setBiCounty(fromCStr(ima->biCounty));
	ma.setAdCode(fromCStr(ima->adCode));
	ma.setFullAddress(fromCStr(ima->fullAddress));
	ma.setRobIdent(fromBoolPtr(ima->robIdent));

	if (ok != Q_NULLPTR) {
		*ok = true;
	}

	return ma;
}
