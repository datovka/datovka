/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

/*
 * This header file must not be included in other header files.
 *
 * Functions in this compilation unit serve for converting types
 * defined in libdatovka.
 */

#pragma once

#include <QDateTime>

struct isds_timeval; /* Forward declaration. */

namespace Isds {

	/*!
	 * @brief Converts date and time from struct isds_timeval.
	 *
	 * @note Similar function for struct timeval is defined in
	 *     src/datovka_shared/isds/internal_conversion.
	 *
	 * @param[in] cDateTime Struct isds_timeval containing date and time.
	 * @return Datetime object, null datetime if NULL pointer was supplied.
	 */
	QDateTime dateTimeFromStructTimeval(struct isds_timeval *cDateTime);

	/*!
	 * @brief Creates a struct isds_timeval copy of the supplied QDateTime.
	 *
	 * @note Similar function for struct timeval is defined in
	 *     src/datovka_shared/isds/internal_conversion.
	 *
	 * @param[in,out] cDateTimePtr Address of pointer to struct tm.
	 * @param[in] dateTime Date object.
	 * @return True on success, false in failure.
	 */
	bool toCDateTimeCopy(struct isds_timeval **cDateTimePtr,
	    const QDateTime &dateTime);

}
