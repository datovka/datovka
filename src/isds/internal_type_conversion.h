/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

/*
 * This header file must not be included in other header files.
 *
 * Functions in this compilation unit serve for converting types
 * defined in libdatovka.
 */

#pragma once

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <libdatovka/isds.h>

#include "src/datovka_shared/isds/types.h"

namespace IsdsInternal {

	/*!
	 * @brief Converts data box types.
	 */
	enum Isds::Type::DbType libisdsDbType2DbType(const isds_DbType ibt,
	    bool *ok = Q_NULLPTR);
	isds_DbType dbType2libisdsDbType(enum Isds::Type::DbType bt,
	    bool *ok = Q_NULLPTR);

	/*!
	 * @brief Converts pointer to data box type.
	 */
	enum Isds::Type::DbType libisdsDbTypePtr2DbType(const isds_DbType *ibtp,
	    bool *ok = Q_NULLPTR);
	bool dbType2libisdsDbTypePtr(isds_DbType **tgt,
	    enum Isds::Type::DbType src);

	/*!
	 * @brief Converts data box accessibility state.
	 */
	enum Isds::Type::DbState longPtr2DbState(const long int *bs,
	    bool *ok = Q_NULLPTR);
	bool dbState2longPtr(long int **tgt, enum Isds::Type::DbState src);

	/*!
	 * @brief Converts user types.
	 */
	enum Isds::Type::UserType libisdsUserType2UserType(
	    const isds_UserType *ut, bool *ok = Q_NULLPTR);
	bool userType2libisdsUserType(isds_UserType **tgt,
	    enum Isds::Type::UserType src);

	/*!
	 * @brief Converts sender types.
	 */
	enum Isds::Type::SenderType libisdsSenderType2SenderType(
	    const isds_sender_type *st, bool *ok = Q_NULLPTR);

	/*!
	 * @brief Converts privileges.
	 */
	Isds::Type::Privileges longPtr2Privileges(long int *ip);
	bool privileges2longPtr(long int **tgt, Isds::Type::Privileges src);

	/*!
	 * @brief Converts OTP method.
	 */
	enum Isds::Type::OtpMethod libisdsOtpMethod2OtpMethod(
	    isds_otp_method iom, bool *ok = Q_NULLPTR);
	isds_otp_method otpMethod2libisdsOtpMethod(
	    enum Isds::Type::OtpMethod om, bool *ok = Q_NULLPTR);

	/*!
	 * @brief Converts OTP resolution.
	 */
	enum Isds::Type::OtpResolution libisdsOtpResolution2OtpResolution(
	    isds_otp_resolution ior, bool *ok = Q_NULLPTR);
	isds_otp_resolution otpResolution2libisdsOtpResolution(
	    enum Isds::Type::OtpResolution ores, bool *ok = Q_NULLPTR);

	/*!
	 * @brief Converts MEP resolution.
	 */
	enum Isds::Type::MepResolution libisdsMepResolution2MepResolution(
	    isds_mep_resolution imp, bool *ok = Q_NULLPTR);
	isds_mep_resolution mepResolution2libisdsMepResolution(
	    enum Isds::Type::MepResolution mres, bool *ok = Q_NULLPTR);

	/*!
	 * @brief Converts vault types.
	 */
	enum Isds::Type::DTType libisdsDTType2DTType(
	    const enum isds_vault_type *vt, bool *ok = Q_NULLPTR);

	/*!
	 * @brief Converts vault payment statuses.
	 */
	enum Isds::Type::FutDTPaidState libisdsDTPaid2DTPaid(
	    const enum isds_vault_payment_status *vpt, bool *ok = Q_NULLPTR);
}
