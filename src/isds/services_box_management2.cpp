/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <cstdlib> /* ::std::free() */
#include <libdatovka/isds.h>
#include <QMutexLocker>

#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/isds/error.h"
#include "src/datovka_shared/isds/generic_interface.h"
#include "src/isds/box_conversion.h"
#include "src/isds/box_conversion2.h"
#include "src/isds/error_conversion.h"
#include "src/isds/generic_conversion.h"
#include "src/isds/services.h"
#include "src/isds/services_internal.h"
#include "src/isds/session.h"

Isds::Error Isds::Service::updateDataBoxDescr2(Session *ctx,
    const QString &dbId, const DbOwnerInfoExt2 &ownerInfo, QString &refNumber)
{
	Error err;

	if (Q_UNLIKELY((ctx == Q_NULLPTR) || dbId.isEmpty())) {
		Q_ASSERT(0);
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Insufficient input."));
		return err;
	}

	bool ok = true;
	struct isds_DbOwnerInfoExt2 *oInfo = NULL;
	char *irNumber = NULL;

	oInfo = dbOwnerInfoExt22libisds(ownerInfo, &ok);
	if (Q_UNLIKELY(!ok)) {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
		goto fail;
	}

	{
		QMutexLocker locker(ctx->mutex());

		isds_error ret = isds_UpdateDataBoxDescr2(ctx->ctx(),
		    dbId.toUtf8().constData(), oInfo, NULL, &irNumber);
		if (Q_UNLIKELY(ret != IE_SUCCESS)) {
			err.setCode(libisds2Error(ret));
			err.setLongDescr(
			    IsdsInternal::isdsLongMessage(ctx->ctx()));
			goto fail;
		}
	}

	refNumber = (irNumber != NULL) ? QString(irNumber) : QString();

	err.setCode(Type::ERR_SUCCESS);

fail:
	if (oInfo != NULL) {
		isds_DbOwnerInfoExt2_free(&oInfo);
	}
	if (irNumber != NULL) {
		::std::free(irNumber);
	}

	return err;
}

Isds::Error Isds::Service::addDataBoxUser2(Session *ctx, const QString &dbId,
    const DbUserInfoExt2 &userInfo, CredentialsDelivery &cd, Status &status)
{
	Error err;

	if (Q_UNLIKELY((ctx == Q_NULLPTR) || dbId.isEmpty())) {
		Q_ASSERT(0);
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Insufficient input."));
		return err;
	}

	bool ok = true;
	struct isds_DbUserInfoExt2 *uInfo = NULL;
	struct isds_credentials_delivery *cDelivery = NULL;
	char *irNumber = NULL;

	uInfo = dbUserInfoExt22libisds(userInfo, &ok);
	if (Q_UNLIKELY(!ok)) {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
		goto fail;
	}

	cDelivery = credentialsDelivery2libisds(cd, &ok);
	if (Q_UNLIKELY(!ok)) {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
		goto fail;
	}

	{
		QMutexLocker locker(ctx->mutex());

		isds_error ret = isds_AddDataBoxUser2(ctx->ctx(),
		    dbId.toUtf8().constData(), uInfo, cDelivery, NULL,
		    &irNumber);
		const isds_status *iStatus = isds_operation_status(ctx->ctx());
		status = libisds2status(iStatus);
		if (Q_UNLIKELY(ret != IE_SUCCESS)) {
			err.setCode(libisds2Error(ret));
			err.setLongDescr(
			    IsdsInternal::isdsLongMessage(ctx->ctx()));
			goto fail;
		}
	}

	cd = libisds2credentialsDelivery(cDelivery, &ok);
	if (Q_UNLIKELY(!ok)) {
		/*
		 * FIXME -- Not sure whether the code should fail here.
		 * AddDataBoxUser2 did finish successfully after all.
		 */
		goto fail;
	}

	err.setCode(Type::ERR_SUCCESS);

fail:
	if (uInfo != NULL) {
		isds_DbUserInfoExt2_free(&uInfo);
	}
	if (cDelivery != NULL) {
		isds_credentials_delivery_free(&cDelivery);
	}
	if (irNumber != NULL) {
		::std::free(irNumber);
	}

	return err;
}

Isds::Error Isds::Service::deleteDataBoxUser2(Session *ctx,const QString &dbId,
    const QString &isdsID, Status &status)
{
	Error err;

	if (Q_UNLIKELY((ctx == Q_NULLPTR) || dbId.isEmpty() ||
	        isdsID.isEmpty())) {
		Q_ASSERT(0);
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Insufficient input."));
		return err;
	}

	char *irNumber = NULL;

	{
		QMutexLocker locker(ctx->mutex());

		isds_error ret = isds_DeleteDataBoxUser2(ctx->ctx(),
		    dbId.toUtf8().constData(), isdsID.toUtf8().constData(),
		    NULL, &irNumber);
		const isds_status *iStatus = isds_operation_status(ctx->ctx());
		status = libisds2status(iStatus);
		if (Q_UNLIKELY(ret != IE_SUCCESS)) {
			err.setCode(libisds2Error(ret));
			err.setLongDescr(
			    IsdsInternal::isdsLongMessage(ctx->ctx()));
			goto fail;
		}
	}

	err.setCode(Type::ERR_SUCCESS);

fail:
	if (irNumber != NULL) {
		::std::free(irNumber);
	}

	return err;
}

Isds::Error Isds::Service::updateDataBoxUser2(Session *ctx, const QString &dbId,
    const QString &isdsID, const DbUserInfoExt2 &userInfo, Status &status)
{
	Error err;

	if (Q_UNLIKELY((ctx == Q_NULLPTR) || dbId.isEmpty() ||
	        isdsID.isEmpty())) {
		Q_ASSERT(0);
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Insufficient input."));
		return err;
	}

	bool ok = true;
	struct isds_DbUserInfoExt2 *uInfo = NULL;
	char *irNumber = NULL;

	uInfo = dbUserInfoExt22libisds(userInfo, &ok);
	if (Q_UNLIKELY(!ok)) {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
		goto fail;
	}

	{
		QMutexLocker locker(ctx->mutex());

		isds_error ret = isds_UpdateDataBoxUser2(ctx->ctx(),
		    dbId.toUtf8().constData(), isdsID.toUtf8().constData(),
		    uInfo, &irNumber);
		const isds_status *iStatus = isds_operation_status(ctx->ctx());
		status = libisds2status(iStatus);
		if (Q_UNLIKELY(ret != IE_SUCCESS)) {
			err.setCode(libisds2Error(ret));
			err.setLongDescr(
			    IsdsInternal::isdsLongMessage(ctx->ctx()));
			goto fail;
		}
	}

	err.setCode(Type::ERR_SUCCESS);

fail:
	if (uInfo != NULL) {
		isds_DbUserInfoExt2_free(&uInfo);
	}
	if (irNumber != NULL) {
		::std::free(irNumber);
	}

	return err;
}

Isds::Error Isds::Service::getDataBoxUsers2(Session *ctx, const QString &dbId,
    QList<DbUserInfoExt2> &users)
{
	Error err;

	if (Q_UNLIKELY((ctx == Q_NULLPTR) || dbId.isEmpty())) {
		Q_ASSERT(0);
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Insufficient input."));
		return err;
	}

	bool ok = true;
	struct isds_list *iUserList = NULL;

	{
		QMutexLocker locker(ctx->mutex());

		isds_error ret = isds_GetDataBoxUsers2(ctx->ctx(),
		    dbId.toUtf8().constData(), &iUserList);
		if (Q_UNLIKELY(ret != IE_SUCCESS)) {
			err.setCode(libisds2Error(ret));
			err.setLongDescr(
			    IsdsInternal::isdsLongMessage(ctx->ctx()));
			goto fail;
		}
	}

	users = (iUserList != NULL) ?
	    libisds2dbUserInfoExt2List(iUserList, &ok) : QList<DbUserInfoExt2>();

	if (ok) {
		err.setCode(Type::ERR_SUCCESS);
	} else {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
	}

fail:
	if (iUserList != NULL) {
		isds_list_free(&iUserList);
	}

	return err;
}
