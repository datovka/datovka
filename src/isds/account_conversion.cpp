/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <cstdlib> /* ::std::malloc */
#include <cstring> /* ::std::memset */
#include <libdatovka/isds.h>

#include "src/datovka_shared/isds/internal_conversion.h"
#include "src/isds/account_conversion.h"
#include "src/isds/internal_type_conversion.h"

Isds::Otp Isds::libisds2otp(const struct isds_otp *io, bool *ok)
{
	if (Q_UNLIKELY(io == NULL)) {
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return Otp();
	}

	bool iOk = false;
	Otp otp;

	otp.setMethod(IsdsInternal::libisdsOtpMethod2OtpMethod(io->method, &iOk));
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	otp.setOtpCode(Isds::fromCStr(io->otp_code));
	otp.setResolution(
	    IsdsInternal::libisdsOtpResolution2OtpResolution(io->resolution, &iOk));
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return otp;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Otp();
}

/*!
 * @brief Set isds_otp structure according to the Otp.
 */
static
bool setLibisdsOtpContent(struct isds_otp *tgt, const Isds::Otp &src)
{
	if (Q_UNLIKELY(tgt == NULL)) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;

	tgt->method = IsdsInternal::otpMethod2libisdsOtpMethod(src.method(), &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}
	if (Q_UNLIKELY(!Isds::toCStrCopy(&tgt->otp_code, src.otpCode()))) {
		return false;
	}
	tgt->resolution = IsdsInternal::otpResolution2libisdsOtpResolution(
	    src.resolution(), &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}

	return true;
}

struct isds_otp *Isds::otp2libisds(const Otp &o, bool *ok)
{
	if (Q_UNLIKELY(o.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return NULL;
	}

	struct isds_otp *io = (struct isds_otp *)::std::malloc(sizeof(*io));
	if (Q_UNLIKELY(io == NULL)) {
		Q_ASSERT(0);
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return NULL;
	}
	::std::memset(io, 0, sizeof(*io));

	if (Q_UNLIKELY(!setLibisdsOtpContent(io, o))) {
		otp_free(&io);
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return NULL;
	}
	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return io;
}

void Isds::otp_free(struct isds_otp **io)
{
	if ((io == NULL) || (*io == NULL)) {
		return;
	}

	::std::free((*io)->otp_code);
	::std::free(*io); *io = NULL;
}

Isds::Mep Isds::libisds2mep(const struct ::isds_mep *im, bool *ok)
{
	if (Q_UNLIKELY(im == NULL)) {
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return Mep();
	}

	bool iOk = false;
	Mep mep;

	mep.setAppName(Isds::fromCStr(im->app_name));
	mep.setIntermUri(Isds::fromCStr(im->intermediate_uri));
	mep.setResolution(
	    IsdsInternal::libisdsMepResolution2MepResolution(im->resolution, &iOk));
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return mep;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Mep();
}

/*!
 * @brief Set isds_mep structure according to the Mep.
 */
static
bool setLibisdsMepContent(struct isds_mep *tgt, const Isds::Mep &src)
{
	if (Q_UNLIKELY(tgt == NULL)) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;

	if (Q_UNLIKELY(!Isds::toCStrCopy(&tgt->app_name, src.appName()))) {
		return false;
	}
	if (Q_UNLIKELY(!Isds::toCStrCopy(&tgt->intermediate_uri, src.intermUri()))) {
		return false;
	}
	tgt->resolution = IsdsInternal::mepResolution2libisdsMepResolution(
	    src.resolution(), &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}

	return true;
}

struct ::isds_mep *Isds::mep2libisds(const Mep &m, bool *ok)
{
	if (Q_UNLIKELY(m.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return NULL;
	}

	struct isds_mep *im = (struct isds_mep *)::std::malloc(sizeof(*im));
	if (Q_UNLIKELY(im == NULL)) {
		Q_ASSERT(0);
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return NULL;
	}
	::std::memset(im, 0, sizeof(*im));

	if (Q_UNLIKELY(!setLibisdsMepContent(im, m))) {
		mep_free(&im);
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return NULL;
	}
	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return im;
}

void Isds::mep_free(struct ::isds_mep **im)
{
	if ((im == NULL) || (*im == NULL)) {
		return;
	}

	::std::free((*im)->app_name);
	::std::free((*im)->intermediate_uri);
	::std::free(*im); *im = NULL;
}
