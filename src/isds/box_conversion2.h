/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>

#include "src/datovka_shared/isds/box_interface2.h"

extern "C" {
	struct isds_AddressExt2;
	struct isds_PersonName2;
	struct isds_DbOwnerInfoExt2;
	struct isds_DbUserInfoExt2;
	struct isds_list;
}

namespace Isds {

	AddressExt2 libisds2addressExt2(const struct ::isds_AddressExt2 *ia,
	    bool *ok = Q_NULLPTR);
	struct ::isds_AddressExt2 *addressExt22libisds(const AddressExt2 &a,
	    bool *ok = Q_NULLPTR);

	PersonName2 libisds2personName2(const struct ::isds_PersonName2 *ipn,
	    bool *ok = Q_NULLPTR);
	struct ::isds_PersonName2 *personName22libisds(const PersonName2 &pn,
	    bool *ok = Q_NULLPTR);

	DbOwnerInfoExt2 libisds2dbOwnerInfoExt2(
	    const struct ::isds_DbOwnerInfoExt2 *idoi, bool *ok = Q_NULLPTR);
	struct ::isds_DbOwnerInfoExt2 *dbOwnerInfoExt22libisds(
	    const DbOwnerInfoExt2 &doi, bool *ok = Q_NULLPTR);

	QList<DbOwnerInfoExt2> libisds2dbOwnerInfoExt2List(
	    const struct ::isds_list *ioil, bool *ok = Q_NULLPTR);
	struct ::isds_list *dbOwnerInfoExt2List2libisds(
	    const QList<DbOwnerInfoExt2> &oil, bool *ok = Q_NULLPTR);

	DbUserInfoExt2 libisds2dbUserInfoExt2(
	    const struct ::isds_DbUserInfoExt2 *idui, bool *ok = Q_NULLPTR);
	struct ::isds_DbUserInfoExt2 *dbUserInfoExt22libisds(
	    const DbUserInfoExt2 &dui, bool *ok = Q_NULLPTR);

	QList<DbUserInfoExt2> libisds2dbUserInfoExt2List(
	    const struct ::isds_list *iuil, bool *ok = Q_NULLPTR);
	struct ::isds_list *dbUserInfoExt2List2libisds(
	    const QList<DbUserInfoExt2> &uil, bool *ok = Q_NULLPTR);

}
