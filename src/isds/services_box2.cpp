/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <libdatovka/isds.h>
#include <QMutexLocker>

#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/isds/error.h"
#include "src/isds/box_conversion2.h"
#include "src/isds/error_conversion.h"
#include "src/isds/services.h"
#include "src/isds/services_internal.h"
#include "src/isds/session.h"

Isds::Error Isds::Service::findDataBox2(Session *ctx,
    const DbOwnerInfoExt2 &criteria, QList<DbOwnerInfoExt2> &boxes)
{
	Error err;

	if (Q_UNLIKELY((ctx == Q_NULLPTR) || criteria.isNull())) {
		Q_ASSERT(0);
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Insufficient input."));
		return err;
	}

	bool ok = false;
	isds_error ret = IE_SUCCESS;
	struct isds_DbOwnerInfoExt2 *iCrit = NULL;
	struct isds_list *iBoxes = NULL;

	iCrit = dbOwnerInfoExt22libisds(criteria, &ok);
	if (Q_UNLIKELY(!ok)) {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
		goto fail;
	}

	{
		QMutexLocker locker(ctx->mutex());

		ret = isds_FindDataBox2(ctx->ctx(), iCrit, &iBoxes);
		if (Q_UNLIKELY((ret != IE_SUCCESS) && (ret != IE_TOO_BIG))) {
			err.setCode(libisds2Error(ret));
			err.setLongDescr(
			    IsdsInternal::isdsLongMessage(ctx->ctx()));
			goto fail;
		}
	}

	ok = true;
	boxes = (iBoxes != NULL) ?
	    libisds2dbOwnerInfoExt2List(iBoxes, &ok) : QList<DbOwnerInfoExt2>();

	if (ok) {
		/* May also be ERR_TOO_BIG. */
		err.setCode(libisds2Error(ret));
	} else {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
	}

fail:
	if (iCrit != NULL) {
		isds_DbOwnerInfoExt2_free(&iCrit);
	}
	if (iBoxes != NULL) {
		isds_list_free(&iBoxes);
	}

	return err;
}

Isds::Error Isds::Service::getOwnerInfoFromLogin2(Session *ctx,
    DbOwnerInfoExt2 &ownerInfo)
{
	Error err;

	if (Q_UNLIKELY(ctx == Q_NULLPTR)) {
		Q_ASSERT(0);
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Insufficient input."));
		return err;
	}

	bool ok = true;
	struct isds_DbOwnerInfoExt2 *oInfo = NULL;

	{
		QMutexLocker locker(ctx->mutex());

		isds_error ret = isds_GetOwnerInfoFromLogin2(ctx->ctx(), &oInfo);
		if (Q_UNLIKELY(ret != IE_SUCCESS)) {
			err.setCode(libisds2Error(ret));
			err.setLongDescr(
			    IsdsInternal::isdsLongMessage(ctx->ctx()));
			goto fail;
		}
	}

	ownerInfo = (oInfo != NULL) ?
	    libisds2dbOwnerInfoExt2(oInfo, &ok) : DbOwnerInfoExt2();

	if (ok) {
		err.setCode(Type::ERR_SUCCESS);
	} else {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
	}

fail:
	if (oInfo != NULL) {
		isds_DbOwnerInfoExt2_free(&oInfo);
	}

	return err;
}

Isds::Error Isds::Service::getUserInfoFromLogin2(Session *ctx,
    DbUserInfoExt2 &userInfo)
{
	Error err;

	if (Q_UNLIKELY(ctx == Q_NULLPTR)) {
		Q_ASSERT(0);
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Insufficient input."));
		return err;
	}

	bool ok = true;
	struct isds_DbUserInfoExt2 *uInfo = NULL;

	{
		QMutexLocker locker(ctx->mutex());

		isds_error ret = isds_GetUserInfoFromLogin2(ctx->ctx(), &uInfo);
		if (Q_UNLIKELY(ret != IE_SUCCESS)) {
			err.setCode(libisds2Error(ret));
			err.setLongDescr(IsdsInternal::isdsLongMessage(ctx->ctx()));
			goto fail;
		}
	}

	userInfo = (uInfo != NULL) ?
	    libisds2dbUserInfoExt2(uInfo, &ok) : DbUserInfoExt2();

	if (ok) {
		err.setCode(Type::ERR_SUCCESS);
	} else {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
	}

fail:
	if (uInfo != NULL) {
		isds_DbUserInfoExt2_free(&uInfo);
	}

	return err;
}
