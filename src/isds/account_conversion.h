/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/isds/account_interface.h"

extern "C" {
	struct isds_otp;
	struct isds_mep;
}

namespace Isds {

	Otp libisds2otp(const struct ::isds_otp *io, bool *ok = Q_NULLPTR);
	struct ::isds_otp *otp2libisds(const Otp &o, bool *ok = Q_NULLPTR);
	/* Libdatovka does not provide isds_otp_free(). */
	void otp_free(struct ::isds_otp **io);

	Mep libisds2mep(const struct ::isds_mep *im, bool *ok = Q_NULLPTR);
	struct ::isds_mep *mep2libisds(const Mep &m, bool *ok = Q_NULLPTR);
	/* Libdatovka does not provide isds_mep_free(). */
	void mep_free(struct ::isds_mep **im);

}
