/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */
#include <cstdlib> /* ::std::free */

#include <libdatovka/isds.h>
#include <QMutexLocker>

#include "src/datovka_shared/isds/error.h"
#include "src/datovka_shared/isds/message_interface2.h"
#include "src/isds/error_conversion.h"
#include "src/isds/internal_type_conversion.h"
#include "src/isds/message_conversion2.h"
#include "src/isds/services.h"
#include "src/isds/services_internal.h"
#include "src/isds/session.h"

Isds::Error Isds::Service::getMessageAuthor2(Session *ctx, qint64 dmId,
    DmMessageAuthor &dmMessageAuthor)
{
	Error err;

	if (Q_UNLIKELY((ctx == Q_NULLPTR) || (dmId < 0))) {
		Q_ASSERT(0);
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Insufficient input."));
		return err;
	}

	struct isds_dmMessageAuthor *author = NULL;
	bool ok = true;

	{
		QMutexLocker locker(ctx->mutex());

		isds_error ret = isds_GetMessageAuthor2(ctx->ctx(),
		    QString::number(dmId).toUtf8().constData(), &author);
		if (Q_UNLIKELY(ret != IE_SUCCESS)) {
			err.setCode(libisds2Error(ret));
			err.setLongDescr(IsdsInternal::isdsLongMessage(ctx->ctx()));
			goto fail;
		}
	}

	dmMessageAuthor = (author != NULL) ?
	    libisdsMessageAuthor2MessageAuthor(author, &ok) : DmMessageAuthor();

	if (ok) {
		err.setCode(Type::ERR_SUCCESS);
	} else {
		err.setCode(Type::ERR_ERROR);
		err.setLongDescr(tr("Error converting types."));
	}

fail:
	if (author != NULL) {
		::std::free(author); author = NULL;
	}

	return err;
}
