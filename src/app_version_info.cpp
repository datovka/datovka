/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include<QStringBuilder>

#include "src/datovka_shared/app_version_info.h"

#define textLineNL(text) \
	(QString("‣ ") % (text) % QLatin1String("<br>"))

QString AppVersionInfo::releaseNewsText(void)
{
	QString content;

	content.append(textLineNL(tr("Simplified the tag assignment dialogue. Tag assignment can be edited by checking or unchecking items in a provided list.")));
	content.append(textLineNL(tr("Tag assignment and editing of available tags are separate actions. Both can be added to available tool bars in the main window.")));
	content.append(textLineNL(tr("Added a simple tool that should help with data recovery from corrupted database files. It can be found in the '%1' menu.").arg("Tools")));
	content.append(textLineNL(tr("Added the possibility to increase the text size in the application. The new setting can be found in '%1' on the '%2' tab.").arg("Preferences").arg("Interface")));
	content.append(textLineNL(tr("Displaying release news in the application.")));

	return content;
}
