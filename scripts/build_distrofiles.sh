#!/usr/bin/env sh

# Obtain location of source root.
src_root () {
	local SCRIPT_LOCATION=""
	local SYSTEM=$(uname -s)
	if [ ! "x${SYSTEM}" = "xDarwin" ]; then
		local SCRIPT=$(readlink -f "$0")
		SCRIPT_LOCATION=$(dirname $(readlink -f "$0"))
	else
		SCRIPT_LOCATION=$(cd "$(dirname "$0")"; pwd)
	fi

	echo $(cd "$(dirname "${SCRIPT_LOCATION}")"; pwd)
}

SRC_ROOT=$(src_root)
cd "${SRC_ROOT}"

. "${SRC_ROOT}"/scripts/helper_dependency_sources.sh
. "${SRC_ROOT}"/scripts/helper_packaging.sh

PACKAGE=""
VERSION=""

H_SHORT="-h"
H_LONG="--help"

P_SHORT="-p"
P_LONG="--package"
P_DATOVKA="datovka"
P_LIBISDS="libisds"
V_SHORT="-v"
V_LONG="--version"

USAGE="Usage:\n\t$0 [options]\n\n"
USAGE="${USAGE}Supported options:\n"
USAGE="${USAGE}\t${H_SHORT}, ${H_LONG}\n\t\tPrints help message.\n"
USAGE="${USAGE}\t${P_SHORT}, ${P_LONG} <package>\n\t\tSupported arguments are '${P_DATOVKA}' and '${P_LIBISDS}'.\n"
USAGE="${USAGE}\t${V_SHORT}, ${V_LONG} <version>\n\t\tExplicitly specify package version (applies only to datovka).\n"

# Parse rest of command line
while [ $# -gt 0 ]; do
	KEY="$1"
	VAL="$2"
	case "${KEY}" in
	${H_SHORT}|${H_LONG})
		echo -e ${USAGE}
		exit 0
		;;
	${P_SHORT}|${P_LONG})
		if [ "x${VAL}" = "x" ]; then
			echo "Argument '${KEY}' requires an argument." >&2
			exit 1
		fi
		if [ "x${PACKAGE}" = "x" ]; then
			PACKAGE="${VAL}"
			shift
		else
			echo "Package name already specified or in conflict." >&2
			exit 1
		fi
		;;
	${V_SHORT}|${V_LONG})
		if [ "x${VAL}" = "x" ]; then
			echo "Argument '${KEY}' requires an argument." >&2
			exit 1
		fi
		if [ "x${VERSION}" = "x" ]; then
			VERSION="${VAL}"
			shift
		else
			echo "Version already specified or in conflict." >&2
			exit 1
		fi
		;;
	--)
		shift
		break
		;;
	-*|*)
		echo "Unknown option '${KEY}'." >&2
		echo -e ${USAGE} >&2
		exit 1
		;;
	esac
	shift
done
if [ $# -gt 0 ]; then
	echo -e "Unknown options: $@" >&2
	echo -en ${USAGE} >&2
	exit 1
fi

#PACKAGE=""
#VERSION=""
RELEASE=""

PACKAGE_SRC=""

# Set package to be uploaded.
case "x${PACKAGE}" in
x${P_DATOVKA})
	if [ "x${VERSION}" != "x" ]; then
		if [ ! -f "${PACKAGE}-${VERSION}.tar.xz" ]; then
			echo "Missing file '${PACKAGE}-${VERSION}.tar.xz'." >&2
			exit 1
		fi
	else
		# Use latest release as default.
		VERSION="4.25.0"

		ensure_source_presence "${SRC_ROOT}" "${PACKAGE}-${VERSION}.tar.xz" \
		    "https://datovka.nic.cz/${VERSION}/" "c9229de5ecdaf52f11d56c67bcfd300eddf581f202fceb40099a7cc1dcd8dea9" "" "" || exit 1
	fi

	RELEASE="1"
	PACKAGE_SRC="${SRC_ROOT}/${PACKAGE}-${VERSION}.tar.xz"
	;;
x${P_LIBISDS})
	VERSION="0.11"

	ensure_source_presence "${SRC_ROOT}/libs/srcs" "${_LIBISDS_ARCHIVE}" \
	    "${_LIBISDS_URL_PREFIX}" "${_LIBISDS_SHA256}" "${_LIBISDS_SIG_SUFF}" "${_LIBISDS_KEY_FP}" || exit 1

	RELEASE="1"
	PACKAGE_SRC="${SRC_ROOT}/libs/srcs/${PACKAGE}-${VERSION}.tar.xz"
	;;
x)
	echo "Unspecified package." >&2
	exit 1
	;;
*)
	echo "Unsupported package '${PACKAGE}'." >&2
	exit 1
	;;
esac

file_present "${PACKAGE_SRC}" || exit 1

DISTRO_WORK_DIR="_distrofiles/${PACKAGE}"
rm_and_create_dir "${DISTRO_WORK_DIR}" || exit 1

cp -ra "${SRC_ROOT}/distro/${PACKAGE}/"* "${DISTRO_WORK_DIR}"
rm_swp_files "${DISTRO_WORK_DIR}"

fill_in_files "${DISTRO_WORK_DIR}" || exit 1

# Rename archive to Debian format.
cp -p "${PACKAGE_SRC}" "${DISTRO_WORK_DIR}/${PACKAGE}_${VERSION}.orig.tar.xz" || exit 1

# List all alternative control files.
CONTROL_FILES=$(echo "${SRC_ROOT}/${DISTRO_WORK_DIR}/deb/debian/control-"*)
if [ "x${CONTROL_FILES}" != "x" ]; then
	mv ${CONTROL_FILES} "${SRC_ROOT}/${DISTRO_WORK_DIR}/"
fi
CONTROL_FILES=$(echo "${SRC_ROOT}/${DISTRO_WORK_DIR}/control-"*)
CONTROL_FILE_VERSIONS=""
if [ "x${CONTROL_FILES}" != "x" ]; then
	for CONTROL_FILE in ${CONTROL_FILES}; do
		CONTROL_FILE_VERSIONS="${CONTROL_FILE_VERSIONS} $(echo ${CONTROL_FILE} | sed -e 's/^.*control-//g')"
	done
fi
unset CONTROL_FILES

TIME="2018-04-06 12:00Z"

# Create Debian archive.
pushd "${SRC_ROOT}/${DISTRO_WORK_DIR}"
cd deb
ARCHIVE="${PACKAGE}_${VERSION}-${RELEASE}.debian.tar.xz"
tar --mtime="${TIME}" -chJf "${ARCHIVE}" debian
cd ..
popd

# Create alternative Debian archives.
#for CONTROL_FILE_VERSION in ${CONTROL_FILE_VERSIONS}; do
#	pushd "${SRC_ROOT}/${DISTRO_WORK_DIR}"
#	cp "control-${CONTROL_FILE_VERSION}" deb/debian/control
#	cd deb
#		ARCHIVE="${PACKAGE}_${VERSION}-${RELEASE}-${CONTROL_FILE_VERSION}.debian.tar.xz"
#		tar --mtime="${TIME}" -chJf "${ARCHIVE}" debian
#	cd ..
#	popd
#done

# Create dsc file.
pushd "${SRC_ROOT}/${DISTRO_WORK_DIR}"
cd deb
DSC_FILES=$(echo "${PACKAGE}"*".dsc")
cd ..
for DSC_FILE in ${DSC_FILES}; do
	DSC_FILE_VERSION=$(echo "${DSC_FILE}" | sed -e "s/^${PACKAGE}//g" -e 's/^-//g' -e 's/\.dsc$//g')

	cd deb
	# Find patching Debain archive.
	ORDINARY_ARCHIVE="${PACKAGE}_${VERSION}-${RELEASE}.debian.tar.xz"
	ARCHIVE=""
	if [ "x${DSC_FILE_VERSION}" != "x" ]; then
		ARCHIVE="${PACKAGE}_${VERSION}-${RELEASE}-${DSC_FILE_VERSION}.debian.tar.xz"
	else
		ARCHIVE="${ORDINARY_ARCHIVE}"
	fi
	# Use default if not found.
	if [ ! -f "${ARCHIVE}" ]; then
		ARCHIVE="${ORDINARY_ARCHIVE}"
	fi
	echo "Populating '${DSC_FILE}' using '${ARCHIVE}'."
	echo " $(compute_md5_checksum ${ARCHIVE}) $(wc -c ${ARCHIVE})" >> "${DSC_FILE}"
	cd ..
	ARCHIVE="${PACKAGE}_${VERSION}.orig.tar.xz"
	echo " $(compute_md5_checksum ${ARCHIVE}) $(wc -c ${ARCHIVE})" >> "deb/${DSC_FILE}"
done
popd
