<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AccountModel</name>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="267"/>
        <source>Recent Received</source>
        <translation>Nedávno přijaté</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="280"/>
        <source>Recent Sent</source>
        <translation>Nedávno odeslané</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="283"/>
        <source>All</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="286"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="289"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="446"/>
        <source>data box</source>
        <translation>schránka</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="451"/>
        <source>recently received messages</source>
        <translation>nedávno přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="458"/>
        <location filename="../src/models/accounts_model.cpp" line="498"/>
        <source>contains %1 unread</source>
        <translation>obsahuje %1 nepřečtených</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="465"/>
        <source>recently sent messages</source>
        <translation>nedávno odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="468"/>
        <source>all messages</source>
        <translation>všechny zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="471"/>
        <source>all received messages</source>
        <translation>všechny přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="474"/>
        <source>all sent messages</source>
        <translation>všechny odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="487"/>
        <source>invalid received messages</source>
        <translation>nevalidní přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="490"/>
        <source>messages received in year %1</source>
        <translation>zprávy přijaté v roce %1</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="514"/>
        <source>invalid sent messages</source>
        <translation>nevalidní odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="516"/>
        <source>messages sent in year %1</source>
        <translation>zprávy odeslané v roce %1</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="571"/>
        <source>Data Boxes</source>
        <translation>Schránky</translation>
    </message>
</context>
<context>
    <name>AccountSettingsModel</name>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="292"/>
        <source>Regular Data Boxes</source>
        <translation>Běžné schránky</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="295"/>
        <source>Shadow Data Boxes</source>
        <translation>Stínové schránky</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="304"/>
        <source>Data-Box Users</source>
        <translation>Uživatelé datové schránky</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="308"/>
        <source>Data-Box Information</source>
        <translation>Informace o schránce</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="350"/>
        <source>Available regular data boxes</source>
        <translation>Dostupné běžné schránky</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="353"/>
        <source>Available shadow data boxes</source>
        <translation>Dostupné stínové schránky</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="356"/>
        <source>Data box</source>
        <translation>Schránka</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="360"/>
        <source>Shadow data box</source>
        <translation>Stínová schránka</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="364"/>
        <location filename="../src/models/accounts_settings_model.cpp" line="371"/>
        <source>Information about data box</source>
        <translation>Informace o schránce</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="390"/>
        <source>Data Boxes</source>
        <translation>Schránky</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_settings_model.cpp" line="368"/>
        <source>Users with access to data box</source>
        <translation>Uživatelé s přístupem do datové schránky</translation>
    </message>
</context>
<context>
    <name>AccountStatusControl</name>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="43"/>
        <source>Mode: online</source>
        <translation>Režim: připojeno (online)</translation>
    </message>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="44"/>
        <source>Mode: offline</source>
        <translation>Režim: nepřipojeno (offline)</translation>
    </message>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="66"/>
        <source>The status control shows whether there are any
sessions (regular or shadow) for data boxes active.
Click on it to display more information.</source>
        <translation>Tento ovládací prvek zobrazuje, zda jsou nějaké
relace (běžné nebo stínové) pro datové schránky aktivní.
Klikněte na něj, aby se zobrazilo více informací.</translation>
    </message>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="76"/>
        <source>Sessions</source>
        <translation>Relace</translation>
    </message>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="116"/>
        <source>Disconnect All Sessions</source>
        <translation>Odpojit všechny relace</translation>
    </message>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="117"/>
        <source>Click here to deactivate all
active (regular or shadow) sessions.</source>
        <translation>Klikněte sem, aby se deaktivovaly všechny
aktivní (běžné nebo stínové) relace.</translation>
    </message>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="152"/>
        <source>Regular</source>
        <translation>Běžné</translation>
    </message>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="154"/>
        <source>Regular data boxes are listed in the main window.</source>
        <translation>Běžné schránky se zobrazují v hlavním okně.</translation>
    </message>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="171"/>
        <source>Shadow</source>
        <translation>Stínové</translation>
    </message>
    <message>
        <location filename="../src/widgets/account_status_widget.cpp" line="173"/>
        <source>Shadow data boxes exist in the background of the application.</source>
        <translation>Stínové schránky existují v pozadí této aplikace.</translation>
    </message>
</context>
<context>
    <name>AccountStatusDelegate</name>
    <message>
        <location filename="../src/delegates/account_status_delegate.cpp" line="69"/>
        <source>Disconnect</source>
        <translation>Odpojit</translation>
    </message>
</context>
<context>
    <name>AccountStatusModel</name>
    <message>
        <location filename="../src/models/account_status_model.cpp" line="125"/>
        <source>Created at %1</source>
        <translation>Vytvořeno %1</translation>
    </message>
</context>
<context>
    <name>AccountStatusValue</name>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="130"/>
        <source>password</source>
        <translation>jméno + heslo</translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="133"/>
        <source>certificate</source>
        <translation>certifikát</translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="136"/>
        <source>certificate + password</source>
        <translation>certifikát + heslo</translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="139"/>
        <source>password + security code</source>
        <translation>heslo + bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="142"/>
        <source>password + security SMS</source>
        <translation>heslo + SMS kód</translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="145"/>
        <source>Mobile Key</source>
        <translation>Mobilní klíč</translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="148"/>
        <source>unknown</source>
        <translation>neznámý</translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="193"/>
        <location filename="../src/delegates/account_status_value.cpp" line="281"/>
        <source>Login Method: </source>
        <translation>Způsob přihlašování: </translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="205"/>
        <location filename="../src/delegates/account_status_value.cpp" line="290"/>
        <source>Connection to ISDS: </source>
        <translation>Spojení s ISDS: </translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="206"/>
        <location filename="../src/delegates/account_status_value.cpp" line="291"/>
        <source>active</source>
        <translation>aktivní</translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="206"/>
        <location filename="../src/delegates/account_status_value.cpp" line="291"/>
        <source>inactive</source>
        <translation>neaktivní</translation>
    </message>
    <message>
        <location filename="../src/delegates/account_status_value.cpp" line="218"/>
        <source>Disconnect</source>
        <translation>Odpojit</translation>
    </message>
</context>
<context>
    <name>ActionListModel</name>
    <message>
        <location filename="../src/models/action_list_model.cpp" line="120"/>
        <source>separator</source>
        <translation>oddělovač</translation>
    </message>
    <message>
        <location filename="../src/models/action_list_model.cpp" line="307"/>
        <source>Operations</source>
        <translation>Operace</translation>
    </message>
</context>
<context>
    <name>AppVersionInfo</name>
    <message>
        <location filename="../src/app_version_info.cpp" line="35"/>
        <source>Simplified the tag assignment dialogue. Tag assignment can be edited by checking or unchecking items in a provided list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/app_version_info.cpp" line="36"/>
        <source>Tag assignment and editing of available tags are separate actions. Both can be added to available tool bars in the main window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/app_version_info.cpp" line="37"/>
        <source>Added a simple tool that should help with data recovery from corrupted database files. It can be found in the &apos;%1&apos; menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/app_version_info.cpp" line="38"/>
        <source>Added the possibility to increase the text size in the application. The new setting can be found in &apos;%1&apos; on the &apos;%2&apos; tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/app_version_info.cpp" line="39"/>
        <source>Displaying release news in the application.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AttachmentInteraction</name>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="228"/>
        <source>Error storing attachment.</source>
        <translation>Chyba ukládání přílohy.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="229"/>
        <source>Cannot write temporary file for attachment &apos;%1&apos;.</source>
        <translation>Nelze zapsat dočasný soubor s přílohou &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="267"/>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="412"/>
        <source>Save attachment</source>
        <translation>Uložit přílohu</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="285"/>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="405"/>
        <source>Error saving attachment.</source>
        <translation>Chyba při ukládání přílohy.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="286"/>
        <source>Cannot write file &apos;%1&apos;.</source>
        <translation>Nemohu zapsat soubor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="386"/>
        <source>Save attachments</source>
        <translation>Uložit všechny přílohy</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="406"/>
        <source>Some files already exist.</source>
        <translation>Některé soubory již existují.</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="432"/>
        <source>In total %n attachment file(s) could not be written.</source>
        <translation>
            <numerusform>Celkem %n soubor s přílohami nemohl být uložen.</numerusform>
            <numerusform>Celkem %n soubory s přílohami nemohly být uloženy.</numerusform>
            <numerusform>Celkem %n souborů s přílohami nemohlo být uloženo.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="434"/>
        <source>These are:</source>
        <translation>Jsou to:</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="440"/>
        <source>Error saving attachments.</source>
        <translation>Chyba při ukládání příloh.</translation>
    </message>
</context>
<context>
    <name>AttachmentTblModel</name>
    <message>
        <location filename="../src/models/attachments_model.cpp" line="161"/>
        <source>The file name &apos;%1&apos; contains no suffix.</source>
        <translation>Název souboru &apos;%1&apos; neobsahuje příponu.</translation>
    </message>
    <message>
        <location filename="../src/models/attachments_model.cpp" line="163"/>
        <source>The suffix of file &apos;%1&apos; does not match the list of suffixes listed in the Operating Rules of ISDS.</source>
        <translation>Přípona souboru &apos;%1&apos; neodpovídá seznamu přípon uvedeném v Provozním řádu ISDS.</translation>
    </message>
    <message>
        <location filename="../src/models/attachments_model.cpp" line="166"/>
        <source>It may happen that the server will reject sending this message.</source>
        <translation>Může se stát, že server odmítne odeslat tuto zprávu.</translation>
    </message>
    <message>
        <location filename="../src/models/attachments_model.cpp" line="243"/>
        <source>local database</source>
        <translation>místní databáze</translation>
    </message>
    <message>
        <location filename="../src/models/attachments_model.cpp" line="297"/>
        <source>bytes</source>
        <translation>bajtů</translation>
    </message>
    <message>
        <location filename="../src/models/attachments_model.cpp" line="485"/>
        <source>File size</source>
        <translation>Velikost souboru</translation>
    </message>
    <message>
        <location filename="../src/models/attachments_model.cpp" line="487"/>
        <source>File path</source>
        <translation>Cesta k souboru</translation>
    </message>
    <message>
        <location filename="../src/models/attachments_model.cpp" line="603"/>
        <location filename="../src/models/attachments_model.cpp" line="629"/>
        <source>unknown</source>
        <translation>neznámý</translation>
    </message>
</context>
<context>
    <name>BackupSelectionModel</name>
    <message>
        <location filename="../src/models/backup_selection_model.cpp" line="187"/>
        <source>Data box</source>
        <translation>Schránka</translation>
    </message>
    <message>
        <location filename="../src/models/backup_selection_model.cpp" line="190"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../src/models/backup_selection_model.cpp" line="193"/>
        <source>Testing</source>
        <translation>Testovací</translation>
    </message>
    <message>
        <location filename="../src/models/backup_selection_model.cpp" line="196"/>
        <source>Box identifier</source>
        <translation>Identifikátor schránky</translation>
    </message>
    <message>
        <location filename="../src/models/backup_selection_model.cpp" line="219"/>
        <source>Messages</source>
        <translation>Zprávy</translation>
    </message>
</context>
<context>
    <name>BackupWorker</name>
    <message>
        <location filename="../src/gui/dlg_backup_internal.cpp" line="137"/>
        <source>tag database</source>
        <translation>databáze tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_internal.cpp" line="99"/>
        <source>databases for data box &apos;%1&apos;</source>
        <translation>databáze schránky &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_internal.cpp" line="160"/>
        <source>data-box database</source>
        <translation>databáze schránek</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_internal.cpp" line="191"/>
        <source>These data failed to be backed up:</source>
        <translation>Zálohování těchto dat selhalo:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_internal.cpp" line="199"/>
        <source>These back-ups were skipped:</source>
        <translation>Tyto zálohy byly vynechány:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_internal.cpp" line="203"/>
        <source>Back-up incomplete</source>
        <translation>Záloha nedokončena</translation>
    </message>
</context>
<context>
    <name>BoxContactsModel</name>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="60"/>
        <location filename="../src/models/data_box_contacts_model.cpp" line="97"/>
        <source>yes</source>
        <translation>ano</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="57"/>
        <location filename="../src/models/data_box_contacts_model.cpp" line="94"/>
        <source>no</source>
        <translation>ne</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="54"/>
        <source>unknown</source>
        <translation>neznámý</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="149"/>
        <source>It&apos;s unknown whether public or commercial messages can be sent to this recipient.</source>
        <translation>Není známo, zda tomuto adresátu lze poslat veřejnou či poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="152"/>
        <source>It&apos;s unknown whether a public messages can be sent to this recipient.</source>
        <translation>Není známo, zda tomuto adresátu lze poslat veřejnou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="155"/>
        <source>It&apos;s unknown whether a commercial messages can be sent to this recipient.</source>
        <translation>Není známo, zda tomuto adresátu lze poslat poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="158"/>
        <source>Receiving of PDZs has been disabled in the recipient data box or there are no available payment methods for PDZs.</source>
        <translation>Příjem PDZ byl v datové schránce adresáta zakázán nebo nejsou dostupné platební metody pro PDZ.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="169"/>
        <source>Sender has the effective OVM flag set.</source>
        <translation>Odesílatel má nastaven příznak OVM.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="191"/>
        <source>Cannot send a public data message.</source>
        <translation>Nelze odeslat veřejnou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="194"/>
        <source>Can send a public data message.</source>
        <translation>Lze odeslat veřejnou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="207"/>
        <source>Cannot send a commercial data message.</source>
        <translation>Nelze odeslat poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="210"/>
        <source>Can send a commercial data message.</source>
        <translation>Lze odeslat poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="269"/>
        <source>selected</source>
        <translation>vybráno</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="269"/>
        <source>not selected</source>
        <translation>nevybráno</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="272"/>
        <source>box identifier</source>
        <translation>identifikátor schránky</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="393"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="394"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="396"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="398"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="400"/>
        <source>Postal Code</source>
        <translation>PSČ</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="402"/>
        <source>Public Message</source>
        <translation>Veřejná zpráva</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="404"/>
        <source>PDZ</source>
        <translation>PDZ</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="405"/>
        <source>PDZ Payment</source>
        <translation>Platba za PDZ</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="408"/>
        <source>Determines the type of the commercial message and the means of payment.</source>
        <translation>Určuje typ poštovní datové zprávy a způsob platby.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="700"/>
        <source>Public</source>
        <translation>Veřejná</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="703"/>
        <source>Response to initiatory</source>
        <translation>Odpověď na iniciační</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="706"/>
        <source>Subsidised</source>
        <translation>Dotovaná</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="709"/>
        <source>Contractual</source>
        <translation>Smluvní</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="712"/>
        <source>Initiatory</source>
        <translation>Iniciační</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="715"/>
        <source>Prepaid credit</source>
        <translation>Předplacený kredit</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="727"/>
        <source>Type of the message is not determined.</source>
        <translation>Typ zprávy není určen.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="730"/>
        <source>A free-of-charge public data message will be sent to the recipient.</source>
        <translation>Bezplatná veřejná datová zpráva bude odeslána adresátu.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="734"/>
        <source>The recipient offers a pre-paid commercial message response to be sent to him.
This setting allows you to use the offered payment.
The transfer charges are going to be paid by the recipient.</source>
        <translation>Adresát nabízí předplacenou odpověď na poštovní datovou zprávu, která mu může být zaslána.
Tato volba umožňuje využít nabízený způsob platby.
Poplatek za odeslání bude hrazen adresátem.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="740"/>
        <source>Send a subsidised commercial data message.
Another data box is paying for the transfer charges.</source>
        <translation>Odešle dotovanou poštovní datovou zprávu.
Poplatek za odeslání datové zprávy je hrazen jinou datovou schránkou.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="744"/>
        <source>Send a contractual commercial data message.
The charges for sending this message are going to be billed by the Czech Post.
An active contract with the Czech Post is needed.</source>
        <translation>Odešle spluvní poštovní datovou zprávu.
Poplatek za odeslání datové zprávy bude fakturován Českou poštou.
Je vyžadována smlouva s Českou poštou.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="749"/>
        <source>Send an initiatory commercial data message.
This type of message enables the recipient to send a response on behalf of your data box.
A sender reference number must be filled in in order to send this type of message.
An active contract with the Czech Post or a subsidising data box is needed.</source>
        <translation>Odešle iniciační poštovní datovou zprávu.
Tento typ zprávy umožňuje adresátu odeslat odpověď na tuto zprávu na účet Vaší schránky.
Před odesláním je potřeba vyplnit referenční číslo odesílatele, aby bylo možné tento typ zprávy odeslat.
Je vyžadována smlouva s Českou poštou nebo schránka odesílatele musí být dotována jinou schránkou.</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="755"/>
        <source>Send a commercial data message.
Pay charges from a prepaid credit.</source>
        <translation>Odešle poštovní datovou zprávu.
Poplatek je hrazen z předplaceného kreditu.</translation>
    </message>
</context>
<context>
    <name>CLIParser</name>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="34"/>
        <source>Data box application</source>
        <translation>Aplikace pro datové schránky</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="39"/>
        <source>Use &lt;%1&gt; subdirectory for configuration.</source>
        <translation>Použít &lt;%1&gt; podadresář pro konfiguraci.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="39"/>
        <location filename="../src/cli/cli_parser.cpp" line="40"/>
        <source>conf-subdir</source>
        <translation>conf-subdir</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="44"/>
        <source>On start load &lt;%1&gt; file.</source>
        <translation>Po spuštění načíst soubor &lt;%1&gt;.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="44"/>
        <location filename="../src/cli/cli_parser.cpp" line="48"/>
        <source>conf</source>
        <translation>conf</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="48"/>
        <source>On stop save &lt;%1&gt; file.</source>
        <translation>Po ukončení uložit soubor &lt;%1&gt;.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="52"/>
        <source>Log messages to &lt;%1&gt;.</source>
        <translation>Zaznamenávat výpisy do &lt;%1&gt;.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="52"/>
        <source>file</source>
        <translation>soubor</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="56"/>
        <source>Set verbosity of logged messages to &lt;%1&gt;. Default is %2.</source>
        <translation>Nastavit podrobnost ladicích zpráv na &lt;%1&gt;. Výchozí je %2.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="57"/>
        <location filename="../src/cli/cli_parser.cpp" line="59"/>
        <location filename="../src/cli/cli_parser.cpp" line="72"/>
        <location filename="../src/cli/cli_parser.cpp" line="74"/>
        <source>level</source>
        <translation>úroveň</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="66"/>
        <source>Enable debugging information.</source>
        <translation>Zapnout debugovací výpisy.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="71"/>
        <source>Set debugging verbosity to &lt;%1&gt;. Default is %2.</source>
        <translation>Nastavit podrobnost ladicích informací na &lt;%1&gt;. Výchozí je %2.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="82"/>
        <source>Service: connect to ISDS and login into data box.</source>
        <translation>Služba: připojit se k datové schránce.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="83"/>
        <location filename="../src/cli/cli_parser.cpp" line="88"/>
        <location filename="../src/cli/cli_parser.cpp" line="93"/>
        <location filename="../src/cli/cli_parser.cpp" line="98"/>
        <location filename="../src/cli/cli_parser.cpp" line="103"/>
        <location filename="../src/cli/cli_parser.cpp" line="123"/>
        <location filename="../src/cli/cli_parser.cpp" line="128"/>
        <location filename="../src/cli/cli_parser.cpp" line="133"/>
        <location filename="../src/cli/cli_parser.cpp" line="138"/>
        <location filename="../src/cli/cli_parser.cpp" line="143"/>
        <location filename="../src/cli/cli_parser.cpp" line="148"/>
        <source>string-of-parameters</source>
        <translation>řetězec-parametrů</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="87"/>
        <source>Service: download list of received/sent messages from ISDS (synchronization).</source>
        <translation>Služba: stáhnout seznam přijatých a odeslaných zpráv z ISDS (sychronizace).</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="92"/>
        <source>Service: create and send a new message to ISDS.</source>
        <translation>Služba: vytvořit a odeslat novou zprávu do ISDS.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="97"/>
        <source>Service: download complete message with signature and time stamp of MV.</source>
        <translation>Služba: stáhnout kompletní zprávu s podpisem a časovým razítkem MV.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="102"/>
        <source>Service: download acceptance info of message with signature and time stamp of MV.</source>
        <translation>Služba: stáhnout informace o doručení zprávy s podpisem a časovým razítkem MV.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="107"/>
        <source>Service: get information about user (role, privileges, ...).</source>
        <translation>Služba: stáhnout informace o uživateli (role, oprávnění, ...).</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="112"/>
        <source>Service: get information about owner and its data box.</source>
        <translation>Služba: stáhnout informace o vlastníkovi a jeho datové schránce.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="117"/>
        <source>Service: get list of messages where attachment missing (local database only).</source>
        <translation>Služba: získat seznam zpráv bez přílohy (pouze lokální databaze).</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="122"/>
        <source>Service: find a data box via several parameters.</source>
        <translation>Služba: vyhledat datovou schránku na ISDS.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="127"/>
        <source>Service: get list of message IDs (received/sent) from local database.</source>
        <translation>Služba: získat seznam identifikátorů zpráv (doručených/odeslaných) z místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="132"/>
        <source>Service: export message from local database.</source>
        <translation>Služba: exportovat zprávu z místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="137"/>
        <source>Service: export messages from local database.</source>
        <translation>Služba: exportovat zprávy z místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="142"/>
        <source>Service: import message into local database.</source>
        <translation>Služba: importovat zprávu do místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="147"/>
        <source>Service: import messages into local database.</source>
        <translation>Služba: importovat zprávy do místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="152"/>
        <source>[zfo-file]</source>
        <translation>[zfo-soubor]</translation>
    </message>
    <message>
        <location filename="../src/cli/cli_parser.cpp" line="153"/>
        <source>ZFO file to be viewed.</source>
        <translation>Zobrazovaný ZFO soubor.</translation>
    </message>
</context>
<context>
    <name>CLIPin</name>
    <message>
        <location filename="../src/cli/cli_pin.cpp" line="156"/>
        <source>Enter PIN code</source>
        <translation>Zadejte PIN kód</translation>
    </message>
</context>
<context>
    <name>CmdCompose</name>
    <message>
        <location filename="../src/cli/cmd_compose.cpp" line="178"/>
        <source>Brings up the send message dialogue window and fills in the supplied data.</source>
        <translation>Vyvolá dialogové okno pro vytvoření zprávy a vyplní dodané informace.</translation>
    </message>
    <message>
        <location filename="../src/cli/cmd_compose.cpp" line="179"/>
        <source>message-options</source>
        <translation>možnosti-zprávy</translation>
    </message>
</context>
<context>
    <name>DbConvertMobileToDesktop</name>
    <message>
        <location filename="../src/io/db_convert_mobile_to_desktop.cpp" line="237"/>
        <source>Cannot open message database file &apos;%1&apos;.</source>
        <translation>Nelze otevřít soubor databáze zpráv &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/io/db_convert_mobile_to_desktop.cpp" line="295"/>
        <source>%1: cannot find/read message ZFO file &apos;%2&apos;.</source>
        <translation>%1: nelze nalézt/číst ZFO soubor zprávy &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/io/db_convert_mobile_to_desktop.cpp" line="306"/>
        <source>%1: cannot parse ZFO file &apos;%2&apos;.</source>
        <translation>%1: nelze načíst ZFO soubor &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/io/db_convert_mobile_to_desktop.cpp" line="314"/>
        <source>%1: cannot insert complete message &apos;%2&apos; into database.</source>
        <translation>%1: nelze vložit kompletní zprávu &quot;%2&quot; do databáze.</translation>
    </message>
    <message>
        <location filename="../src/io/db_convert_mobile_to_desktop.cpp" line="325"/>
        <source>%1: cannot find/read delivery info ZFO file &apos;%2&apos;.</source>
        <translation>%1: nelze nalézt/číst ZFO soubor doručenky &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/io/db_convert_mobile_to_desktop.cpp" line="332"/>
        <source>%1: cannot insert delivery info &apos;%2&apos; into database.</source>
        <translation>%1: nelze vložit doručenku &quot;%2&quot; do databáze.</translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="238"/>
        <source>Unknown backup type.</source>
        <translation>Neznámý typ zálohy.</translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="245"/>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="252"/>
        <source>The selected file isn&apos;t a valid ZIP archive containing a backup or transfer data.</source>
        <translation>Vybraný soubor není platný ZIP archiv obsahující data zálohy nebo transferu.</translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="270"/>
        <source>The selected file isn&apos;t a valid ZIP archive containing a backup.</source>
        <translation>Vybraný soubor není platný ZIP archiv obsahující zálohu.</translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="307"/>
        <source>The selected ZIP archive doesn&apos;t contain a valid backup info file.</source>
        <translation>Vybraný ZIP archiv neobsahuje platný soubor s popisem zálohy.</translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="315"/>
        <source>The backup file &apos;%1&apos; doesn&apos;t contain a valid backup information.</source>
        <translation>Soubor zálohy &quot;%1&quot; neobsahuje platné informace o záloze.</translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="322"/>
        <source>Backup</source>
        <translation>Záloha</translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="325"/>
        <source>Transfer</source>
        <translation>Transfer</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/io/load_mobile_zip_package.cpp" line="333"/>
        <source>%1 ZIP archive was taken at %2 and contains %n file(s). Found message databases for conversion: %3.</source>
        <translation>
            <numerusform>%1 ZIP archiv byl vytvořen %2 a obsahuje %n soubor. Počet nalezených databází pro konverzi: %3.</numerusform>
            <numerusform>%1 ZIP archiv byl vytvořen %2 a obsahuje %n soubory. Počet nalezených databází pro konverzi: %3.</numerusform>
            <numerusform>%1 ZIP archiv byl vytvořen %2 a obsahuje %n souborů. Počet nalezených databází pro konverzi: %3.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="350"/>
        <source>The amount of free space on the target volume couldn&apos;t be determined.</source>
        <translation>Množství volného místa na cílovém svazku nemohlo být zjištěno.</translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="355"/>
        <source>Not enough free space to unzip data on the target volume.</source>
        <translation>Na cílovém svazku není dostatek volného místa pro rozbalení dat.</translation>
    </message>
    <message>
        <location filename="../src/io/load_mobile_zip_package.cpp" line="360"/>
        <source>It is likely that the target volume has not enough free space left for databases conversion.</source>
        <translation>Je pravděpodobné, že cílový svazek nemá dostatek volného místa pro konverzi databáze.</translation>
    </message>
</context>
<context>
    <name>DbMsgsTblModel</name>
    <message numerus="yes">
        <location filename="../src/models/messages_model.cpp" line="168"/>
        <source>The message will be deleted from the ISDS in %n day(s).</source>
        <translation>
            <numerusform>Zpráva bude z ISDS smazána za %n den.</numerusform>
            <numerusform>Zpráva bude z ISDS smazána za %n dny.</numerusform>
            <numerusform>Zpráva bude z ISDS smazána za %n dnů.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="172"/>
        <source>The message has already been deleted from the ISDS.</source>
        <translation>Zpráva již byla v ISDS smazána.</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="220"/>
        <source>message identifier</source>
        <translation>identifikátor zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="238"/>
        <source>personal delivery</source>
        <translation>zpráva určena do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="240"/>
        <source>not a personal delivery</source>
        <translation>zpráva není určena do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="253"/>
        <source>marked as read</source>
        <translation>označeno jako přečtené</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="255"/>
        <source>marked as unread</source>
        <translation>označeno jako nepřečtené</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="260"/>
        <source>attachments downloaded</source>
        <translation>přílohy staženy</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="262"/>
        <source>attachments not downloaded</source>
        <translation>přílohy nestaženy</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="273"/>
        <source>unsettled</source>
        <translation>nevyřízeno</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="276"/>
        <source>in progress</source>
        <translation>vyřizuje se</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="279"/>
        <source>settled</source>
        <translation>vyřízeno</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="619"/>
        <source>Attachments downloaded</source>
        <translation>Přílohy staženy</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="623"/>
        <source>Processing state</source>
        <translation>Stav vyřizování</translation>
    </message>
</context>
<context>
    <name>Description</name>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="34"/>
        <source>system box</source>
        <translation>systémová schránka</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="35"/>
        <source>public authority</source>
        <translation>orgán veřejné moci</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="36"/>
        <source>public authority - notary</source>
        <translation>orgán veřejné moci - notář</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="37"/>
        <source>public authority - bailiff</source>
        <translation>orgán veřejné moci - exekutor</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="38"/>
        <source>public authority - at request</source>
        <translation>orgán veřejné moci - na žádost</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="39"/>
        <source>public authority - natural person</source>
        <translation>orgán veřejné moci - fyzická osoba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="40"/>
        <source>public authority - self-employed person</source>
        <translation>orgán veřejné moci - podnikající fyzická osoba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="41"/>
        <source>public authority - legal person</source>
        <translation>orgán veřejné moci - právnická osoba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="42"/>
        <source>legal person</source>
        <translation>právnická osoba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="43"/>
        <source>legal person - founded by an act</source>
        <translation>právnická osoba - ze zákona</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="44"/>
        <source>legal person - at request</source>
        <translation>právnická osoba - na žádost</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="45"/>
        <source>self-employed person</source>
        <translation>podnikající fyzická osoba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="46"/>
        <source>self-employed person - advocate</source>
        <translation>podnikající fyzická osoba - advokát</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="47"/>
        <source>self-employed person - tax advisor</source>
        <translation>podnikající fyzická osoba - daňový poradce</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="48"/>
        <source>self-employed person - insolvency administrator</source>
        <translation>podnikající fyzická osoba - insolvenční správce</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="49"/>
        <source>self-employed person - statutory auditor</source>
        <translation>podnikající fyzická osoba - auditor</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="50"/>
        <source>self-employed person - expert witness</source>
        <translation>podnikající fyzická osoba - soudní znalec</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="51"/>
        <source>self-employed person - sworn translator</source>
        <translation>podnikající fyzická osoba - soudní překladatel nebo tlumočník</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="52"/>
        <source>self-employed person - architect</source>
        <translation>podnikající fyzická osoba - architekt</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="53"/>
        <source>self-employed person - authorised engineer / technician</source>
        <translation>podnikající fyzická osoba - autorizovaný inženýr / technik</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="54"/>
        <source>self-employed person - authorised geodetics engineer</source>
        <translation>podnikající fyzická osoba - autorizovaný zeměměřický inženýr</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="55"/>
        <source>self-employed person - at request</source>
        <translation>podnikající fyzická osoba - na žádost</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="56"/>
        <source>natural person</source>
        <translation>fyzická osoba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="60"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="341"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="387"/>
        <source>An error occurred while checking the type.</source>
        <translation>Nastala chyba při ověřování typu.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="73"/>
        <source>The data box is accessible. It is possible to send messages into it. It can be looked up on the Portal.</source>
        <translation>Datová schránka je přístupná, lze do ní dodávat zprávy, na Portále lze vyhledat.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="78"/>
        <source>The data box is temporarily inaccessible (at own request). It may be made accessible again at some point in the future.</source>
        <translation>Datová schránka je dočasně znepřístupněna (na vlastní žádost), může být později opět zpřístupněna.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="83"/>
        <source>The data box is so far inactive. The owner of the box has to log into the web interface at first in order to activate the box.</source>
        <translation>Datová schránka je dosud neaktivní. Vlastník schránky se musí poprvé přihlásit do webového rozhraní, aby došlo k aktivaci schránky.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="88"/>
        <source>The data box is permanently inaccessible. It is waiting to be deleted (but it may be made accessible again).</source>
        <translation>Datová schránka je trvale znepřístupněna, čeká na smazání (může být opět zpřístupněna).</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="93"/>
        <source>The data box has been deleted (none the less it exists in ISDS).</source>
        <translation>Datová schránka byla smazána (přesto existuje v ISDS).</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="98"/>
        <source>The data box is temporarily inaccessible (because of reasons listed in law). It may be made accessible again at some point in the future.</source>
        <translation>Datová schránka je dočasně znepřístupněna (z důvodů vyjmenovaných v zákoně), může být později opět zpřístupněna.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="104"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="235"/>
        <source>An error occurred while checking the status.</source>
        <translation>Nastala chyba při ověřování stavu.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="115"/>
        <source>Full control</source>
        <translation>Plný přístup</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="117"/>
        <source>Restricted control</source>
        <translation>Omezený přístup</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="122"/>
        <source>download and read incoming DM</source>
        <translation>stahovat a číst došlé DZ</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="128"/>
        <source>download and read DM sent into own hands</source>
        <translation>stahovat a číst DZ určené do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="133"/>
        <source>create and send DM, download sent DM</source>
        <translation>vytvářet a odesílat DZ, stahovat odeslané DZ</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="138"/>
        <source>retrieve DM lists, delivery and acceptance reports</source>
        <translation>načítat seznamy DZ, dodejky a doručenky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="143"/>
        <source>search for data boxes</source>
        <translation>vyhledávat DS</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="147"/>
        <source>manage the data box</source>
        <translation>spravovat DS</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="151"/>
        <source>read message in data vault</source>
        <translation>číst zprávy v datovém trezoru</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="155"/>
        <source>erase messages from data vault</source>
        <translation>mazat zprávy v datovém trezoru</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="169"/>
        <source>Message has been submitted (has been created in ISDS)</source>
        <translation>Zpráva byla podána (vznikla v ISDS)</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="175"/>
        <source>Data message including its attachments signed with time-stamp.</source>
        <translation>Datová zpráva včetně písemností podepsána časovým razítkem.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="182"/>
        <source>Message did not pass through AV check; infected paper deleted; final status before deletion.</source>
        <translation>Zpráva neprošla AV kontrolou; nakažená písemnost je smazána; konečný stav zprávy před smazáním.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="187"/>
        <source>Message handed into ISDS (delivery time recorded).</source>
        <translation>Zpráva dodána do ISDS (zapsán čas dodání).</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="195"/>
        <source>10 days have passed since the delivery of the public message which has not been accepted by logging-in (assumption of acceptance through fiction in non-OVM DS); this state cannot occur for commercial messages.</source>
        <translation>Uplynulo 10 dnů od dodání veřejné zprávy, která dosud nebyla doručena přihlášením (předpoklad doručení fikcí u ne-OVM DS); tento stav nemůže nastat v případě poštovních datových zpráv.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="205"/>
        <source>A person authorised to read this message has logged in -- delivered message has been accepted.</source>
        <translation>Osoba oprávněná číst tuto zprávu se přihlásila -- dodaná zpráva byla doručena.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="210"/>
        <source>Message has been read (on the portal or by ESS action).</source>
        <translation>Zpráva byla přečtena (na portále nebo akcí ESS).</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="217"/>
        <source>Message marked as undeliverable because the target DS has been made inaccessible.</source>
        <translation>Zpráva byla označena jako nedoručitelná, protože DS adresáta byla zpětně znepřístupněna.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="225"/>
        <source>Message content deleted, envelope including hashes has been moved into archive.</source>
        <translation>Obsah zprávy byl smazán, obálka zprávy včetně hashů přesunuta do archivu.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="230"/>
        <source>Message resides in data vault.</source>
        <translation>Zpráva je v Datovém trezoru.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="256"/>
        <source>Subsidised postal data message, initiating reply postal data message</source>
        <translation>Poštovní datová zpráva dotovaná, iniciuje použití odpovědní poštovní datové zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="260"/>
        <source>Subsidised postal data message, initiating reply postal data message - used for sending reply</source>
        <translation>Poštovní datová zpráva dotovaná, iniciuje použití odpovědní poštovní datové zprávy - využitá pro odeslání odpovědi</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="264"/>
        <source>Subsidised postal data message, initiating reply postal data message - unused for sending reply, expired</source>
        <translation>Poštovní datová zpráva dotovaná, iniciuje použití odpovědní poštovní datové zprávy - nevyužitá a expirovaná</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="268"/>
        <source>Postal data message sent using a subscription (prepaid credit)</source>
        <translation>Poštovní datová zpráva odeslaná pomocí předplacení (kreditu)</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="272"/>
        <source>Postal data message sent in endowment mode by another data box to the benefactor account</source>
        <translation>Poštovní datová zpráva zaslaná v režimu dotování jinou schránkou na účet schránky donátora</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="276"/>
        <source>Postal data message</source>
        <translation>Poštovní datová zpráva</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="279"/>
        <source>Initiating postal data message</source>
        <translation>Iniciační poštovní datová zpráva</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="282"/>
        <source>Reply postal data message; sent at the expense of the sender of the initiating postal data message</source>
        <translation>Odpovědní poštovní datová zpráva; zasílaná zdarma na účet odesílatele iniciační poštovní datové zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="286"/>
        <source>Public message (recipient or sender is a public authority)</source>
        <translation>Veřejná zpráva (adresát nebo odesílatel je OVM)</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="289"/>
        <source>Initiating postal data message - unused for sending reply, expired</source>
        <translation>Iniciační poštovní datová zpráva - nevyužitá a expirovaná</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="292"/>
        <source>Initiating postal data message - used for sending reply</source>
        <translation>Iniciační poštovní datová zpráva - využitá pro odeslání odpovědi</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="297"/>
        <source>Unrecognised message type</source>
        <translation>Nerozpoznaný typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="310"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="354"/>
        <source>Primary user</source>
        <translation>Oprávněná osoba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="314"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="358"/>
        <source>Entrusted user</source>
        <translation>Pověřená osoba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="318"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="362"/>
        <source>Administrator</source>
        <translation>Administrátor</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="321"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="366"/>
        <source>Official</source>
        <translation>Systém ISDS</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="370"/>
        <source>Virtual</source>
        <translation>Virtuální přístup</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="410"/>
        <source>Invalid enumeration value</source>
        <translation>Neplatná výčtová hodnota</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="324"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="373"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="328"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="376"/>
        <source>Liquidator</source>
        <translation>Likvidátor</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="332"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="379"/>
        <source>Receiver</source>
        <translation>Nucený správce</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="336"/>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="382"/>
        <source>Guardian</source>
        <translation>Opatrovník</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="395"/>
        <source>Success</source>
        <translation>Úspěch</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="396"/>
        <source>Unspecified error</source>
        <translation>Nespecifikovaná chyba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="397"/>
        <source>Not supported</source>
        <translation>Není podporováno</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="398"/>
        <source>Invalid value</source>
        <translation>Neplatná hodnota</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="399"/>
        <source>Invalid context</source>
        <translation>Neplatný kontext</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="400"/>
        <source>Not logged in</source>
        <translation>Nepřihlášen</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="401"/>
        <source>Connection closed</source>
        <translation>Spojení ukončeno</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="404"/>
        <source>Out of memory</source>
        <translation>Nedostatek paměti</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="405"/>
        <source>Network problem</source>
        <translation>Chyba sítě</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="406"/>
        <source>HTTP problem</source>
        <translation>Chyba HTTP</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="407"/>
        <source>SOAP problem</source>
        <translation>Chyba SOAP</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="408"/>
        <source>XML problem</source>
        <translation>Chyba XML</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="409"/>
        <source>ISDS server problem</source>
        <translation>Chyba serveru ISDS</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="411"/>
        <source>Invalid date value</source>
        <translation>Neplatné datum</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="412"/>
        <source>Too big</source>
        <translation>Příliš velké</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="413"/>
        <source>Too small</source>
        <translation>Příliš malé</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="414"/>
        <source>Value not unique</source>
        <translation>Hodnota není unikátní</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="415"/>
        <source>Values not equal</source>
        <translation>Hodnota se nerovná</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="416"/>
        <source>Some suboperations failed</source>
        <translation>Selhaly některé podoperace</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="417"/>
        <source>Operation aborted</source>
        <translation>Operace přerušena</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="418"/>
        <source>Security problem</source>
        <translation>Bezpečnostní problém</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/type_description.cpp" line="421"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>DlgAbout</name>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="67"/>
        <source>(64-bit)</source>
        <translation>(64 bitů)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="67"/>
        <source>(%1; 64-bit)</source>
        <translation>(%1; 64 bitů)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="69"/>
        <source>(32-bit)</source>
        <translation>(32 bitů)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="69"/>
        <source>(%1; 32-bit)</source>
        <translation>(%1; 32 bitů)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="74"/>
        <source>Portable version</source>
        <translation>Přenosná verze</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="102"/>
        <source>It&apos;s distributed free of charge. It doesn&apos;t contain advertisements or in-app purchases.</source>
        <translation>Je distribuována zdarma. Neobsahuje reklamy ani nákupy v aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="106"/>
        <source>Do you want to support us in this effort?</source>
        <translation>Chcete nás v tomto záměru podpořit?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="107"/>
        <source>Make a donation.</source>
        <translation>Darujte příspěvek.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="125"/>
        <source>Depends on libraries:</source>
        <translation>Závisí na knihovnách:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="130"/>
        <source>If you have any problems with the application or you have found any errors or you just have an idea how to improve this application please contact us using the following e-mail address:</source>
        <translation>Máte-li s touto aplikací nějaký problém, našli jste nějakou chybu nebo máte nápad, jak tuto aplikaci zlepšit, prosím, napište nám na uvedenou e-mailovou adresu:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="100"/>
        <location filename="../src/gui/dlg_about.cpp" line="128"/>
        <source>This application is being developed by %1 as open source.</source>
        <translation>Tuto aplikaci vyvíjí %1 jako open source.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="104"/>
        <source>We want to provide an easy-to-use data-box client.</source>
        <translation>Chceme poskytovat snadno použitelného klienta pro datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="109"/>
        <source>Did you know that there is a mobile version of Datovka?</source>
        <translation>Věděli jste, že existuje i verze Datovky pro mobilní zařízení?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="136"/>
        <source>This application is available as it is. Usage of this application is at own risk. The association CZ.NIC is in no circumstances liable for any damage directly or indirectly caused by using or not using this application.</source>
        <translation>Tato aplikace je k dispozici tak, jak je. Použití této aplikace je na vlastní nebezpečí. Sdružení CZ.NIC není v žádném případě odpovědno za škody přímo či nepřímo způsobené použitím nebo nepoužitím této aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="138"/>
        <source>CZ.NIC isn&apos;t the operator nor the administrator of the Data Box Information System (ISDS). The operation of the ISDS is regulated by Czech law and regulations.</source>
        <translation>CZ.NIC není provozovatelem ani správcem Informačního systému datových schránek (ISDS). Provoz ISDS je řízen zákony a dalšími předpisy České republiky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="140"/>
        <source>This software is distributed under the GNU GPL version 3 licence.</source>
        <translation>Tento software je distribuován pod licencí GNU GPL verze 3.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="145"/>
        <source>File &apos;%1&apos; either doesn&apos;t exist or is empty.</source>
        <translation>Soubor &quot;%1&quot; buď neexistuje nebo je prázdný.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="151"/>
        <source>We&apos;re glad that you are using the Datovka application.</source>
        <translation>Těší nás, že používáte aplikaci Datovka.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="155"/>
        <source>The Datovka project aims to provide client software available for everyone for accessing the Data Box Information System.</source>
        <translation>Projekt Datovka si klade za cíl poskytovat všem dostupný klientský software pro přístup do Informačního systému datových schránek.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="157"/>
        <source>CZ.NIC, z.s.p.o. provides this application free of charge and without advertisements.</source>
        <translation>CZ.NIC, z.s.p.o. poskytuje tuto aplikaci zdarma a bez reklam.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="159"/>
        <source>The state doesn&apos;t contribute to the development of this application.</source>
        <translation>Na vývoj této aplikace nepřispívá stát.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="161"/>
        <source>We&apos;ll be very happy if you make a donation for the development and support of the application.</source>
        <translation>Budeme moc rádi, když na vývoj a podporu aplikace přispějete.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="164"/>
        <source>Donate now</source>
        <translation>Darovat teď</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="232"/>
        <source>Checking for new version...</source>
        <translation>Kontroluji nové verze...</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="246"/>
        <source>New version %1 is available.</source>
        <translation>Je dostupná nová verze %1.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="255"/>
        <source>Download version %1</source>
        <translation>Stáhnout verzi %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="258"/>
        <location filename="../src/gui/dlg_about.cpp" line="273"/>
        <location filename="../src/gui/dlg_about.cpp" line="333"/>
        <source>Update to version %1</source>
        <translation>Aktualizovat na verzi %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="298"/>
        <source>Downloaded %1 of %2.</source>
        <translation>Stáhnuto %1 z %2.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="319"/>
        <source>The installation of the new application version &apos;%1&apos; will be attempted. The installer will automatically be executed. The running application will be closed before the installation. Stored user data won&apos;t be changed.</source>
        <translation>Bude proveden pokus o instalaci nové verze aplikace &quot;%1&quot;. Před spuštěním instalátoru bude běžící aplikace ukončena. Uložená uživatelská data nebudou dotčena.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="324"/>
        <source>The installation of the new application version &apos;%1&apos; will be attempted. The extraction of the new application package will be attempted. The running application will be closed before the extraction.</source>
        <translation>Bude proveden pokus o instalaci nové verze aplikace &quot;%1&quot;. Před rozbalením balíku s aplikací bude běžící aplikace ukončena.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="335"/>
        <source>Don&apos;t show this notification again.</source>
        <translation>Znovu nezobrazovat toto upozornění.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="336"/>
        <source>Do you want to proceed?</source>
        <translation>Chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="383"/>
        <source>64-Bit Version %1</source>
        <translation>64bitová verze %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="384"/>
        <source>A 64-bit portable package in the version %1 is available. You can migrate to the 64-bit version.</source>
        <translation>Je dostupný balík s 64bitovou přenosnou verzí %1. Můžete přejít na 64bitovou verzi.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="385"/>
        <source>Do you want to download the 64-bit package and update to the new version %1?</source>
        <translation>Chcete stáhnout 64bitový balík a aktualizovat na novou verzi %1?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="386"/>
        <source>Warning: A 64-bit application may not operate on other computers with 32-bit Windows. If you are using this application from a removable media and you want to use it on a 32-bit system then continue using the 32-bit version.</source>
        <translation>Upozornění: 64bitová aplikace nemusí pracovat na jiných počítačích s 32bitovými Windows. Pokud tuto aplikaci provozujete z přenosného média a chcete ji používat i na 32bitových systémech, tak nadále používejte 32bitovou verzi.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="425"/>
        <source>Update failed</source>
        <translation>Aktualizace selhala</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="426"/>
        <source>Update to version &apos;%1&apos; failed. Cannot run downloaded installer.</source>
        <translation>Aktualizace na verzi &quot;%1&quot; selhala. Nelze spustit stažený instalátor.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="427"/>
        <source>Do you want to download and install the new version manually?</source>
        <translation>Přejete si ručně stáhnout a nainstalovat novou verzi?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="103"/>
        <location filename="../src/gui/dlg_about.cpp" line="76"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="394"/>
        <location filename="../src/gui/dlg_about.cpp" line="113"/>
        <source>Home Page</source>
        <translation>Domovská stránka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="413"/>
        <location filename="../src/gui/dlg_about.cpp" line="116"/>
        <source>User Guide</source>
        <translation>Uživatelská příručka</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="55"/>
        <source>Free client for Czech eGov data boxes.</source>
        <translation>Svobodný klient pro Datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="432"/>
        <location filename="../src/gui/dlg_about.cpp" line="117"/>
        <source>FAQ</source>
        <translation>FAQ</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="121"/>
        <source>Support</source>
        <translation>Podpora</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="20"/>
        <source>About Datovka</source>
        <translation>O Datovce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="141"/>
        <source>Update</source>
        <translation>Aktualizovat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="76"/>
        <source>Datovka</source>
        <translation>Datovka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="30"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="86"/>
        <source>Description</source>
        <translation>Popis</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="115"/>
        <source>Checking...</source>
        <translation>Ověřuji...</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="122"/>
        <source>Download</source>
        <translation>Stáhnout</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="129"/>
        <source>Downloading...</source>
        <translation>Stahuji...</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="170"/>
        <source>Donate</source>
        <translation>Přispět</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="203"/>
        <source>Development</source>
        <translation>Vývoj</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="209"/>
        <location filename="../src/gui/ui/dlg_about.ui" line="264"/>
        <location filename="../src/gui/ui/dlg_about.ui" line="300"/>
        <location filename="../src/gui/ui/dlg_about.ui" line="327"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="239"/>
        <source>Show News</source>
        <translation>Ukázat novinky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="321"/>
        <source>Support Us</source>
        <translation>Podpořte nás</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="356"/>
        <source>Donate Now</source>
        <translation>Datovat teď</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="451"/>
        <location filename="../src/gui/dlg_about.cpp" line="118"/>
        <source>Best Practice</source>
        <translation>Doporučené zásady</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="484"/>
        <source>Copyright ©</source>
        <translation>Copyright ©</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="294"/>
        <source>Licence</source>
        <translation>Licence</translation>
    </message>
</context>
<context>
    <name>DlgBackup</name>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="14"/>
        <source>Back Up Data</source>
        <translation>Zálohovat data</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="20"/>
        <location filename="../src/gui/ui/dlg_backup.ui" line="67"/>
        <location filename="../src/gui/ui/dlg_backup.ui" line="136"/>
        <location filename="../src/gui/ui/dlg_backup.ui" line="146"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="40"/>
        <source>Where do you want to back up your data?</source>
        <translation>Kam chcete zálohovat svá data?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="48"/>
        <source>Directory</source>
        <translation>Adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="58"/>
        <source>Choose</source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="77"/>
        <source>What do you want to back up?</source>
        <translation>Co chcete zálohovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="83"/>
        <source>Keep a copy of you login credentials and preserve it in a safe place
where only allowed persons have access.</source>
        <translation>Udržujte si kopii Vašich přihlašovacích údajů a uchovávejte ji na
bezpečném místě, kam mají přístup pouze oprávněné osoby.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="87"/>
        <source>Because of security reasons the application does not allow you to make back-ups of login credentials.</source>
        <translation>Z bezpečnostních důvodů Vám aplikace nedovoluje vytváření záloh přihlašovacích údajů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="100"/>
        <source>Select all</source>
        <translation>Vybrat vše</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_backup.ui" line="129"/>
        <source>Tag database</source>
        <translation>Databáze tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="181"/>
        <source>Available space on volume %1: %2</source>
        <translation>Dostupné místo na svazku %1: %2</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="191"/>
        <source>Open Directory</source>
        <translation>Otevřít adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="302"/>
        <source>Not a directory</source>
        <translation>Není adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="303"/>
        <source>The path &apos;%1&apos; is not a directory.</source>
        <translation>Cesta &quot;%1&quot; není adresář.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="312"/>
        <source>Non-existent directory</source>
        <translation>Neexistující adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="313"/>
        <source>The specified directory does not exist. Do you want to create the directory &apos;%1&apos;?</source>
        <translation>Uvedený adresář neexistuje. Chcete vytvořit adresář &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="322"/>
        <source>Cannot create directory</source>
        <translation>Nelze vytvořit adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="323"/>
        <source>Could not create the directory &apos;%1&apos;.</source>
        <translation>Nezdařilo se vytvořit adresář &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="333"/>
        <source>Cannot write into directory</source>
        <translation>Nelze zapisovat do adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="334"/>
        <source>You don&apos;t have permissions to write into directory &apos;%1&apos;.</source>
        <translation>Nemáte oprávnění k zápisu do adresáře &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="533"/>
        <source>Preserve the back-up in a safe place as it contains private data.</source>
        <translation>Uchovávejte zálohu na bezpečném místě, protože obsahuje osobní data.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="374"/>
        <source>Directory not empty</source>
        <translation>Adresář není prázdný</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="375"/>
        <source>The directory &apos;%1&apos; is not empty. Some of the existing data may be overwritten. Do you want to proceed?</source>
        <translation>Adresář &quot;%1&quot; není prázdný. Část existujících dat může být přepsána. Chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="404"/>
        <source>Cannot access directory</source>
        <translation>Nelze vstoupit do adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="405"/>
        <source>Cannot access directory &apos;%1&apos;.</source>
        <translation>Nelze vstoupit do adresáře &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="498"/>
        <source>Required space to back up data: %1</source>
        <translation>Prostor vyžadovaný pro zálohu: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="517"/>
        <source>It is likely that the target volume has not enough free space left.</source>
        <translation>Je možné, že na cílovém svazku není dostatek volného místa.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="529"/>
        <source>Specify the location where you want to back up your data and select what data you want to back up. Selected data will be written into the specified location.</source>
        <translation>Určete umístění, kam chcete zálohovat Vaše data, a vyberte data, která chcete zálohovat. Vybraná data budou zapsána do uvedeného umístění.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup.cpp" line="531"/>
        <source>Don&apos;t modify any of the written files unless you really know what you are doing.</source>
        <translation>Nemodifikujte žádný ze zapsaných souborů pokud si nejste opravdu jisti, co děláte.</translation>
    </message>
</context>
<context>
    <name>DlgBackupProgress</name>
    <message>
        <location filename="../src/gui/ui/dlg_backup_progress.ui" line="17"/>
        <location filename="../src/gui/ui/dlg_backup_progress.ui" line="31"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_progress.cpp" line="122"/>
        <source>Backing up %1</source>
        <translation>Zálohuji %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_progress.cpp" line="134"/>
        <source>Cancelling current and skipping all further tasks.</source>
        <translation>Ruším současné a přeskakuji následující úlohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_progress.cpp" line="145"/>
        <source>Back-up Finished</source>
        <translation>Záloha dokončena</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_progress.cpp" line="146"/>
        <source>Back-up task finished without any errors.</source>
        <translation>Všechny zálohy dokončeny bez chyb.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_backup_progress.cpp" line="159"/>
        <source>Back-Up Progress</source>
        <translation>Průběh zálohování</translation>
    </message>
</context>
<context>
    <name>DlgChangeDirectory</name>
    <message>
        <location filename="../src/gui/dlg_change_directory.cpp" line="106"/>
        <source>Open Directory</source>
        <translation>Otevřít adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_directory.cpp" line="184"/>
        <source>Database files for &apos;%1&apos; have been successfully moved to

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; byly úspěšně přesunuty do

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_directory.cpp" line="193"/>
        <source>Database files for &apos;%1&apos; could not be moved to

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; nemohly být přesunuty do

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_directory.cpp" line="211"/>
        <source>Database files for &apos;%1&apos; have been successfully copied to

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; byly úspěšně překopírovány do

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_directory.cpp" line="220"/>
        <source>Database files for &apos;%1&apos; could not be copied to

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; nemohly být překopírovány do

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_directory.cpp" line="238"/>
        <source>New database files for &apos;%1&apos; have been successfully created in

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; byly úspěšně vytvořeny v

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_directory.cpp" line="247"/>
        <source>New database files for &apos;%1&apos; could not be created in

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; nemohly být vytvořeny v

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="14"/>
        <location filename="../src/gui/dlg_change_directory.cpp" line="183"/>
        <location filename="../src/gui/dlg_change_directory.cpp" line="192"/>
        <location filename="../src/gui/dlg_change_directory.cpp" line="210"/>
        <location filename="../src/gui/dlg_change_directory.cpp" line="219"/>
        <location filename="../src/gui/dlg_change_directory.cpp" line="237"/>
        <location filename="../src/gui/dlg_change_directory.cpp" line="246"/>
        <source>Change data directory for current data box</source>
        <translation>Změnit adresář pro ukládání dat pro aktuální schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="20"/>
        <source>Data for this data box is currenly stored in:</source>
        <translation>Data pro tuto schránku jsou aktuálně uložena v:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="32"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="39"/>
        <source>Select a new directory where data should be stored:</source>
        <translation>Vyberte nový adresář, kam budou data uložena:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="52"/>
        <source>New data directory</source>
        <translation>Nový adresář pro data</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="75"/>
        <source>Choose</source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="103"/>
        <source>You cannot use the original directory as destination!</source>
        <translation>Nemůžete použít původní adresář jako cílový!</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="119"/>
        <source>Action</source>
        <translation>Akce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="131"/>
        <source>Move data to the new directory</source>
        <translation>Přesunout stávající data do nového adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="147"/>
        <source>Copy data to the new directory</source>
        <translation>Kopírovat stávající data do nového adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="160"/>
        <source>Start afresh in the new directory</source>
        <translation>Začít novou databázi v novém adresáři</translation>
    </message>
</context>
<context>
    <name>DlgChangePwd</name>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="72"/>
        <source>Enter security code:</source>
        <translation>Zadejte bezpečnostní kód:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="20"/>
        <source>Change password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="26"/>
        <source>This changes the password on the ISDS server. Please enter your current and the new password below:</source>
        <translation>Zde se mění heslo na serveru ISDS. Níže prosím zadejte stávající a nové heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="47"/>
        <source>Data-box ID:</source>
        <translation>ID datové schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="64"/>
        <source>Current password:</source>
        <translation>Stávající heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="78"/>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="89"/>
        <source>The password must be at least 8 characters long and
must contain at least one digit and one capital letter.</source>
        <translation>Heslo musí mít nejméně 8 znaků a musí obsahovat
alespoň jednu číslici a jedno velké písmeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="82"/>
        <source>New password:</source>
        <translation>Nové heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="100"/>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="112"/>
        <source>Repeat the password. The password must be
at least 8 characters long and must contain
at least one digit and one capital letter.</source>
        <translation>Zopakujte heslo. Heslo musí mít nejméně 8
znaků a musí obsahovat alespoň
jednu číslici a jedno velké písmeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="105"/>
        <source>Repeat new password:</source>
        <translation>Nové heslo znovu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="153"/>
        <source>Generate</source>
        <translation>Generovat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="162"/>
        <source>Username:</source>
        <translation>Uživatelské jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="186"/>
        <source>Note: Remember your new password.</source>
        <translation>Poznámka: Zapamatujte si Vaše nové heslo.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="202"/>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="79"/>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="314"/>
        <source>Enter SMS code:</source>
        <translation>Zadejte SMS kód:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="209"/>
        <source>Enter SMS or security code</source>
        <translation>Zadejte SMS nebo bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="216"/>
        <source>Send SMS security code.</source>
        <translation>Zaslat bezpečnostní SMS kód.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="219"/>
        <source>Send SMS code</source>
        <translation>Zaslat SMS kód</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="222"/>
        <source>Hide</source>
        <translation>Skrýt</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="146"/>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="222"/>
        <source>Show</source>
        <translation>Ukázat</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="150"/>
        <source>Password has been successfully changed on the ISDS server.</source>
        <translation>Heslo bylo na ISDS serveru úspěšně změněno.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="170"/>
        <source>An error occurred during an attempt to change the password.</source>
        <translation>Nastala chyba v průběhu pokusu o změnu hesla.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="172"/>
        <source>Fix the problem and try it again.</source>
        <translation>Napravte chybu a zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="306"/>
        <source>Enter SMS security code</source>
        <translation>Zadejte SMS bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="317"/>
        <source>Login error</source>
        <translation>Chyba přihlášení</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="318"/>
        <source>An error occurred while preparing request for SMS with OTP security code.</source>
        <translation>Došlo k chybě v průběhu přípravy požadavku pro přihlášení pomocí SMS OTP.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="320"/>
        <source>Please try again later or you have to use the official web interface of Datové schránky to access to your data box.</source>
        <translation>Zkuste to později nebo použijte oficiální webové rozhraní pro Datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="149"/>
        <source>Password has been changed</source>
        <translation>Heslo bylo změněno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="152"/>
        <source>Restart the application. Also don&apos;t forget to remember the new password so you will still be able to log into your data box via the web interface.</source>
        <translation>Restartujte aplikaci. Nezapomeňte si také zapamatovat nové heslo, abyste je byli schopni použít pro přihlašování přes webové rozhraní.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="163"/>
        <source>Error: </source>
        <translation>Chyba: </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="165"/>
        <source>ISDS returns</source>
        <translation>ISDS vrací</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="169"/>
        <source>Password error</source>
        <translation>Chybné heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="273"/>
        <source>SMS code for data box &apos;%1&apos;</source>
        <translation>SMS kód pro schránku &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="274"/>
        <source>Data box &apos;%1&apos; requires authentication via security code to connect to data box.</source>
        <translation>Schránka &quot;%1&quot; vyžaduje pro přihlášení k datové schránce autorizaci bezpečnostním kódem OTP.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="307"/>
        <source>SMS security code for data box &apos;%1&apos;&lt;br/&gt;has been sent to your mobile phone...</source>
        <translation>SMS bezpečnostní kód pro schránku &quot;%1&quot;&lt;br/&gt;byl zaslán do Vašeho mobilu...</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="310"/>
        <source>Enter SMS security code for data box</source>
        <translation>Zadejte SMS bezpečnostní kód pro schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="277"/>
        <source>The security code will be sent to you via a Premium SMS.</source>
        <translation>Bezpečnostní kód Vám bude zaslán prémiovou SMS zprávou.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="279"/>
        <source>Do you request a Premium SMS containing the security code?</source>
        <translation>Chcete poslat Prémiovou SMS s bezpečnostním kódem do Vašeho mobilu?</translation>
    </message>
</context>
<context>
    <name>DlgContacts</name>
    <message>
        <location filename="../src/gui/dlg_contacts.cpp" line="94"/>
        <source>Enter sought expression</source>
        <translation>Zadete hledaný výraz</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_contacts.ui" line="14"/>
        <source>Add recipient</source>
        <translation>Přidat adresáta</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_contacts.ui" line="20"/>
        <source>Select recipients from the list of current contacts (collected from existing messages):</source>
        <translation>Vyberte adresáty ze seznamu kontaktů (vytvořeno z existujících zpráv):</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_contacts.ui" line="29"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
</context>
<context>
    <name>DlgConvertImportFromMobileApp</name>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="14"/>
        <source>Database conversion tool</source>
        <translation>Nástroj pro konverzi databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="20"/>
        <source>ZIP archives containing transfer or backup data from the mobile application can be read and their content converted into a format used by the desktop application. Resulting data can then be imported into the desktop application.
The original ZIP archive is not modified.</source>
        <translation>ZIP archivy obsahující data záloh nebo transferů z mobilní aplikace mohou být načteny a jejich obsah převeden do formátu používaného desktopovou aplikací. Výsledná data mohou být importována do desktopové aplikace.
Původní ZIP archiv není modifikován.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="34"/>
        <source>Load ZIP archive from mobile application</source>
        <translation>Načíst ZIP archiv mobilní aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="42"/>
        <source>Choose ZIP</source>
        <translation>Vyberte ZIP</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="71"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="84"/>
        <source>Select data boxes to be converted and imported</source>
        <translation>Vybrat schránky pro převod a importování</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="149"/>
        <source>Convert data of selected data boxes</source>
        <translation>Převést data vybraných schránek</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="92"/>
        <source>Select all</source>
        <translation>Vybrat vše</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="112"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="157"/>
        <source>Convert</source>
        <translation>Převést</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="203"/>
        <source>Import converted data</source>
        <translation>Importovat převedená data</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_convert_import_from_mobile_app.ui" line="211"/>
        <source>Import</source>
        <translation>Importovat</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="493"/>
        <source>Open ZIP File</source>
        <translation>Otevřít ZIP soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="494"/>
        <source>ZIP file (*.zip)</source>
        <translation>ZIP soubor (*.zip)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="500"/>
        <source>File: %1</source>
        <translation>Soubor: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="529"/>
        <source>All message databases have been successfully converted.</source>
        <translation>Všechny databáze zpráv byly úspěšně převedeny.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="546"/>
        <source>Cannot create temporary directory for extracted files.</source>
        <translation>Nelze vytvořit dočasný adresář pro extrahované soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="553"/>
        <source>No file has been extracted.</source>
        <translation>Nebyl extrahován žádný soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="562"/>
        <source>Cannot create temporary directory for conversion.</source>
        <translation>Nelze vytvořit dočasný adresář pro konverzi.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="569"/>
        <source>Some message databases haven&apos;t been converted successfully. See details below.</source>
        <translation>Některé databáze zpráv se nepodařilo úspěšně převést, viz popis níže.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="581"/>
        <source>Cannot create directory in order to split database content.</source>
        <translation>Nelze vytvořit adresář pro rozdělení obsahu databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="589"/>
        <source>Some message databases haven&apos;t been converted/split successfully. See details below.</source>
        <translation>Některé databáze zpráv se nepodařilo převést/rozdělit, viz popis níže.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="537"/>
        <source>No data boxes selected.</source>
        <translation>Není vybrána žádná schránka.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="620"/>
        <source>All data boxes have been imported.</source>
        <translation>Všechny schránky byly importovány.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="622"/>
        <source>Some data boxes haven&apos;t been imported.</source>
        <translation>Některé schránky nebyly importovány.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="656"/>
        <source>Converting database &apos;%1&apos;...</source>
        <translation>Převádím databázi &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="668"/>
        <source>Cannot move converted message database file &apos;%1&apos; to &apos;%2&apos;.</source>
        <translation>Nelze přemístit soubor převedené databáze z &quot;%1&quot; do &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="697"/>
        <source>Splitting database &apos;%1&apos; according to years...</source>
        <translation>Rozděluji databázi &quot;%1&quot; podle let...</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="705"/>
        <source>Cannot split converted message database file &apos;%1&apos; according to years.</source>
        <translation>Nelze rozdělit převáděný soubor &quot;%1&quot; s databází zpráv podle let.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="714"/>
        <source>Converted message databases are located in the following location: &apos;%1&apos;.</source>
        <translation>Převedené databáze zpráv naleznete v uvedeném místě: &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="733"/>
        <source>Unpacking archive: %1</source>
        <translation>Rozbaluji archiv: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="739"/>
        <source>All files have been unpacked successfully.</source>
        <translation>Všechny soubory byly úspěšně rozbaleny.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="742"/>
        <source>No file has been unpacked.</source>
        <translation>Nebyl rozbalen žádny soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="773"/>
        <source>Import data box: %1</source>
        <translation>Importovat schránku: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="774"/>
        <source>Data box with username &apos;%1&apos; already exists.</source>
        <translation>Schránka s uživatelským jménem &quot;%1&quot; již existuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="775"/>
        <source>Do you want to import messages from converted database into the data box?</source>
        <translation>Přejete si importovat zprávy z převedené databáze do této schránky?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="787"/>
        <source>Data box with name &apos;%1&apos; has been created (username &apos;%1&apos;). The database files have been set as the actual message databases for this data box.</source>
        <translation>Schránka se jménem &quot;%1&quot; byla vytvořena (uživatelské jméno &quot;%1&quot;). Databázové soubory byly použity pro tuto schránku.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="790"/>
        <source>Data box with username &apos;%1&apos; hasn&apos;t been created.</source>
        <translation>Schránka pro uživatelské jméno &quot;%1&quot; nebyla vytvořena.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="795"/>
        <source>Create data box: %1</source>
        <translation>Nová schránka: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="831"/>
        <source>Cannot add data box into application.</source>
        <translation>Nelze přidat schránku do aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="835"/>
        <source>You&apos;ll have to modify the data box properties in order to log in to the ISDS server correctly.</source>
        <translation>Budete muset upravit nastavení schránky, abyste se mohli správně přihlašovat do ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="812"/>
        <source>Some message databases with username &apos;%1&apos; already exist in the target location &apos;%2&apos;. Move or delete them and try importing the data again.</source>
        <translation>Pro uživatelské jméno &quot;%1&quot; v cílovém umístění &quot;%2&quot; již nějaké databáze zpráv existují. Přesuňte nebo smažte je a pak zkuste data importovat znovu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="818"/>
        <source>No free space to move databases into the target location &apos;%1&apos;.</source>
        <translation>Není dostatek volného místa k přesunutí databáze do cílového umístění &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_convert_import_from_mobile_app.cpp" line="824"/>
        <source>Cannot move databases into the target location &apos;%1&apos;.</source>
        <translation>Nelze přemístit databáze do cílového umístění &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>DlgCorrespondenceOverview</name>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="189"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="192"/>
        <source>messages: </source>
        <translation>zpráv: </translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="164"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="485"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="198"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="495"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="14"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="456"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="467"/>
        <source>Correspondence overview</source>
        <translation>Přehled korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="28"/>
        <source>Data box:</source>
        <translation>Schránka:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="66"/>
        <source>Output format:</source>
        <translation>Výstupní formát:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="91"/>
        <source>Select the dates and types of messages to export.</source>
        <translation>Vyberte časové období a typy zpráv pro export.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="102"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="470"/>
        <source>From date:</source>
        <translation>Od data:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="129"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="474"/>
        <source>To date:</source>
        <translation>Do data:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="156"/>
        <source>Message type:</source>
        <translation>Typ zprávy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="233"/>
        <source>HTML overview:</source>
        <translation>HTML přehled:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="241"/>
        <source>Add tags</source>
        <translation>Přidat tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="248"/>
        <source>Colour tags</source>
        <translation>Barvit tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="260"/>
        <source>Export selected messages also as:</source>
        <translation>Exportovat vybrané zprávy také jako:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="268"/>
        <source>ZFO message files</source>
        <translation>Zprávy ve formátu ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="275"/>
        <source>PDF envelope files</source>
        <translation>Obálku zprávy do PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="286"/>
        <source>ZFO acceptance info files</source>
        <translation>Doručenky ve formátu ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="293"/>
        <source>PDF acceptance info files</source>
        <translation>Doručenku do PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="478"/>
        <source>Generated:</source>
        <translation>Vygenerováno:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="385"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="386"/>
        <source>Message type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="387"/>
        <source>Delivery time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="388"/>
        <source>Acceptance time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="346"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="389"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="350"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="390"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="391"/>
        <source>Sender Address</source>
        <translation>Adresa odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="354"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="392"/>
        <source>Recipient</source>
        <translation>Adresát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="303"/>
        <source>Tags</source>
        <translation>Tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="336"/>
        <source>Delivery</source>
        <translation>Dodání</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="341"/>
        <source>Acceptance</source>
        <translation>Doručení</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="393"/>
        <source>Recipient Address</source>
        <translation>Adresa adresáta</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="398"/>
        <source>Sender data-box ID</source>
        <translation>ID schránky odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="399"/>
        <source>Recipient data-box ID</source>
        <translation>ID schránky adresáta</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="526"/>
        <source>Select file to save correspondence overview</source>
        <translation>Vybrat soubor pro uložení přehledu korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="542"/>
        <source>Correspondence Overview Export Error</source>
        <translation>Chyba při exportu přehledu korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="550"/>
        <source>correspondence overview file was exported to HTML.</source>
        <translation>soubor s přehledem korespondence byl exportován do HTML.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="551"/>
        <source>correspondence overview file was exported to CSV.</source>
        <translation>soubor s přehledem korespondence byl exportován do CSV.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="555"/>
        <source>correspondence overview file was exported.</source>
        <translation>soubor s přehledem korespondence byl exportován.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="578"/>
        <source>Message &apos;%1&apos; does not contain data necessary for ZFO export.</source>
        <translation>Zpráva &apos;%1&apos; neobsahuje potřebná data pro export do ZFO souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="585"/>
        <source>Message &apos;%1&apos; does not contain acceptance info data necessary for ZFO export.</source>
        <translation>Zpráva &apos;%1&apos; neobsahuje data potřebná pro export doručenky do ZFO souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="592"/>
        <source>Message &apos;%1&apos; does not contain message envelope data necessary for PDF export.</source>
        <translation>Zpráva &apos;%1&apos; neobsahuje data potřebná pro export obálky do PDF souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="599"/>
        <source>Message &apos;%1&apos; does not contain acceptance info data necessary for PDF export.</source>
        <translation>Ke zprávě &apos;%1&apos; chybí data doručenky, která jsou potřeba pro export do PDF.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="682"/>
        <source>Select directory for export of ZFO/PDF file(s)</source>
        <translation>Vyberte adresář pro export ZFO/PDF souborů</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="689"/>
        <source>messages were successfully exported to ZFO/PDF.</source>
        <translation>zpráv bylo úspěšně exportováno do ZFO/PDF.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="717"/>
        <source>messages were successfully exported to ZFO.</source>
        <translation>zpráv bylo úspěšně exportováno do ZFO.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="738"/>
        <source>acceptance infos were successfully exported to ZFO.</source>
        <translation>doručenek bylo exportováno do ZFO.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="759"/>
        <source>message envelopes were successfully exported to PDF.</source>
        <translation>obálek zpráv bylo úspěšně exportováno do PDF.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="780"/>
        <source>acceptance infos were successfully exported to PDF.</source>
        <translation>doručenek bylo exportováno do PDF.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="799"/>
        <source>Export results</source>
        <translation>Výsledky exportu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="800"/>
        <source>Export of correspondence overview finished with these results:</source>
        <translation>Export přehledu korespondence skončil s tímto výsledkem:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="787"/>
        <source>Some errors occurred during export.</source>
        <translation>Během exportu se vyskytly nějaké chyby.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="789"/>
        <source>See detail for more info...</source>
        <translation>Více informací v podrobnostech...</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="516"/>
        <source>Overview</source>
        <translation>Prehled</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="394"/>
        <source>Sender file mark</source>
        <translation>Spisová značka odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="395"/>
        <source>Sender reference number</source>
        <translation>Jednací číslo odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="396"/>
        <source>Recipient file mark</source>
        <translation>Spisová značka adresáta</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="397"/>
        <source>Recipient reference number</source>
        <translation>Jednací číslo adresáta</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="527"/>
        <source>Files</source>
        <translation>Soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="543"/>
        <source>Correspondence overview file &apos;%1&apos; could not be written.</source>
        <translation>Soubor s přehledem korespondence &apos;%1&apos; nemohl být zapsán.</translation>
    </message>
</context>
<context>
    <name>DlgCreateAccount</name>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="134"/>
        <source>Password</source>
        <translation>Jméno + Heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="135"/>
        <source>Certificate</source>
        <translation>Certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="136"/>
        <source>Certificate + Password</source>
        <translation>Certifikát + Heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="139"/>
        <source>Mobile key</source>
        <translation>Mobilní klíč</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="296"/>
        <source>Add User</source>
        <translation>Přidat uživatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="298"/>
        <source>Change Contact Address</source>
        <translation>Změnit kontaktní adresu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="300"/>
        <source>Change Privileges</source>
        <translation>Změnit oprávnění</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="302"/>
        <source>Delete User</source>
        <translation>Smazat uživatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="529"/>
        <source>Certificate Files (*.pfx *.p12 *.pem)</source>
        <translation>Soubory certifikátů (*.pfx *.p12 *.pem)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="592"/>
        <source>Shadow data boxes are data boxes which exist in the background of the application.

Shadow data boxes have low access privileges and don&apos;t have any databases associated to them. They become useful when you have access to a data box to which multiple users have access to. In this case a data box which can only download message lists (and send messages) can be used as a shadow data box.

Data boxes which can only download message lists (and send messages) can&apos;t cause the acceptance of delivered messages. These data boxes can then be used to automatically check whether there are newly delivered messages without accepting them.</source>
        <translation>Stínové schránky jsou schránky, které existuji v pozadí této aplikace.

Stínové schránky mají nízká přístupová oprávnění a nejsou jim přiřazeny žádné vlastní databáze zpráv. Mohou být užitečné, pokud máte přístup k datové schránce, ke které dokáže přistupovat několik uživatelů. V tom případě schránku, která smí stahovat pouze seznamy zpráv (a odesílat datové zprávy), lze použít jako stínovou schránku.

Schránky, které smí pouze stahovat seznamy zpráv (a odesílat zprávy), nemohou způsobit doručení dodaných zpráv. Takové schránky mohou pak být použity pro automatické ověření, zda ve schránce existují doručené zprávy, aniž by došlo k jejich doručení.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="643"/>
        <source>Data-box users</source>
        <translation>Uživatelé datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="152"/>
        <source>Synchronise this data box when &apos;%1&apos; is activated</source>
        <translation>Synchronizovat tuto schránku při spuštění akce &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="187"/>
        <source>Sign in and Add Data Box</source>
        <translation>Přihlásit a přidat schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="262"/>
        <source>Add Shadow Data Box</source>
        <translation>Přidat stínovou schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="264"/>
        <source>Remove Shadow Data Box</source>
        <translation>Odebrat stínovou schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="270"/>
        <source>Refresh data-box information from ISDS.</source>
        <translation>Aktualizovat informace o schránce z ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="584"/>
        <source>Regular data boxes</source>
        <translation>Běžné schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="585"/>
        <source>Regular data boxes are data boxes which you can see listed in the main window of the application.

Database files containing message data are associated with these data boxes.</source>
        <translation>Bežné schránky jsou schránky, které se zobrazují v přehledu schránek v hlavním okně aplikace.

Těmto schránkám jsou přiřazeny soubory s databázemi obsahující datové zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="591"/>
        <source>Shadow data boxes</source>
        <translation>Stínové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="626"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="651"/>
        <source>Data-box info %1</source>
        <translation>Informace o schránce %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="711"/>
        <source>Adding new shadow data box failed</source>
        <translation>Přidávání nové stínového schránky selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="712"/>
        <source>A data box using the supplied username already exists. Data box has not been added.</source>
        <translation>Schránka se stejným uživatelským jménem již existuje. Schránka nebyla přidána.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="732"/>
        <source>Unusable Shadow Data Box</source>
        <translation>Nepoužitelná stínová schránka</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="733"/>
        <source>This data box won&apos;t be used to download message lists on background. View the data-box information for more detail.</source>
        <translation>Tato schránka nebude používána ke stahování seznamu zpráv na pozadí. Pro více informací zobrazte informace o schránce.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="741"/>
        <source>Remove Shadow Data Box?</source>
        <translation>Odstranit stínovou schránku?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="742"/>
        <source>Do you want to remove the shadow data box &apos;%1&apos;?</source>
        <translation>Přejete si odstranit stínovou schránku &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="867"/>
        <source>Failed User List Download</source>
        <translation>Stažení seznamu uživatelů selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="868"/>
        <source>User list download failed. ISDS returns: %1</source>
        <translation>Stažení seznamu uživatelů selhalo. ISDS vrací: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1001"/>
        <source>New user has been created successfully.</source>
        <translation>Nový uživatel byl úspěšně vytvořen.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1003"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="1102"/>
        <source>ISDS server returns: %1</source>
        <translation>Server ISDS vrací: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1006"/>
        <source>Couldn&apos;t create a new user.</source>
        <translation>Nezdařilo se vytvořit nového uživatele.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1008"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="1105"/>
        <source>ISDS server returns: %1 %2</source>
        <translation>Server ISDS vrací: %1 %2</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1220"/>
        <source>The list of users having access to this data box could not be obtained from the server.</source>
        <translation>Seznam uživatelů s přístupem do této datové schránky se nepodařilo stáhnout ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1272"/>
        <source>ev. no.</source>
        <translation>ev.č.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1302"/>
        <source>Born on</source>
        <translation>Narozen(a)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1363"/>
        <source>Because of your request a new %1 is going to be created for the data box.</source>
        <translation>Na Vaši žádost bude zřízena nový/nová %1 k datové schránce.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1366"/>
        <source>If the person can be identified in the Population register and if it owns an accessible natural person data box then the login credentials will be sent directly into this data box.</source>
        <translation>Pokud se podaří tuto osobu ztotožnit s Registrem obyvatel a má zpřístupněnou datovou schránku fyzické osoby, budou jí přístupové údaje dodány přímo do této datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1367"/>
        <source>Otherwise the login credentials will be sent in a letter into own hands to the address:</source>
        <translation>Jinak budou přístupové údaje zaslány dopisem do vlastních rukou na následující adresu:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1371"/>
        <source>Please ensure yourself that the address is correct and the person can be found on this address.</source>
        <translation>Prosím ujistěte se, že adresa je správná a daná osoba je na této adrese dohledatelná.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1372"/>
        <source>In opposite case the letter containing the login credentials won&apos;t be able to be handed in and the given person won&apos;t gain access to the data box.</source>
        <translation>V opačném případě nebude moci být dopis s přístupovými údaji doručen a daná osoba nebude mít do schránky přístup.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1374"/>
        <source>Note: If it is found that the given person&apos;s delivery address is held in the Basic population register then the login credentials will be sent to the found address regardless to the above mentioned address.</source>
        <translation>Poznámka: Pokud bude zjištěno, že tato osoba má v Základním registru obyvatel zadanou doručovací adresu, pak budou přístupové údaje zaslány na ni, bez ohledu na výše zadanou adresu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1377"/>
        <source>Add a New User?</source>
        <translation>Přidat nového uživatele?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1442"/>
        <source>A new data-box user has successfully been created and the requested privileges have been set.</source>
        <translation>Nový uživatel datové schránky byl úspěšně založen a byla mu nastavena požadovaná práva.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1444"/>
        <source>The login credentials are going delivered to the new user in a letter to the supplied address or if he was identified in the Population register and owns an accessible natural person data box then the login credentials will be sent directly into his data box.</source>
        <translation>Přihlašovací údaje budou novému uživateli zaslány poštou na adresu uvedenou v této žádosti, nebo, pokud byl ztotožněn v Registru obyvatel a má zpřístupněnou datovou schránku fyzické osoby, budou dodány do jeho schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1445"/>
        <source>After receiving his login credentials the user will be able to log into the data box and perform actions according to the supplied user privileges.</source>
        <translation>Teprve poté se bude moci do této datové schránky přihlásit a provádět úkony podle přidělených práv.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1446"/>
        <source>Note: The user must log into the ISDS web interface to activate the created data box and change his assigned password.</source>
        <translation>Poznámka: První přihlášení uživatele musí proběhnout přes webový portál ISDS, kde si změní přidělené heslo.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1447"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="1453"/>
        <source>After this operation he will be able to use third party applications to access the data box.</source>
        <translation>Po této operaci pak bude moci používat aplikace třetích stran k přístupu do této schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1452"/>
        <source>Note: The user must log into the testing instance of the ISDS web interface to activate the created data box and change his assigned password.</source>
        <translation>Poznámka: První přihlášení uživatele musí proběhnout přes testovací instanci webového portálu ISDS, kde si změní přidělené heslo.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1865"/>
        <source>Enter password/certificate for data box %1</source>
        <translation>Zadejte heslo/certifikát pro schránku %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1951"/>
        <source>The following shadow data boxes can be used by this data box:</source>
        <translation>Tato schránka může využívat následující stínové schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1959"/>
        <source>There is no shadow data box available for this data box.</source>
        <translation>Pro tuto schránku není k dispozici žádná stínová schránka.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1981"/>
        <source>The following regular data boxes may utilise this data box:</source>
        <translation>Tato schránka může být využita následujícími běžnými schránkami:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1990"/>
        <source>This data box won&apos;t be used to download message lists in background because it doesn&apos;t meet the following requirements:
- The data box must have low access privileges. It must be able to download message lists but it must be prohibited from downloading received messages.
- It must use a login procedure which does not require user interaction (username and password) and complete login credentials must be remembered by the application.</source>
        <translation>Tato schránka nebude používána ke stahování seznamů zpráv na pozadí, protože nesplňuje některý z následujících požadavků:
- Schránka musí mít nízká oprávnění. Musí být schopna stahovat seznamy zpráv, ale nesmí z níj být možné stahovat přijaté zprávy.
- Musí využívat takový způsob přihlašování, který nevyžaduje uživatelskou interakci (přihlášení jménem a heslem) a všechny přihlašovací údaje si aplikace musí pamatovat.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1972"/>
        <source>This data box can be used to download message lists in background because:
- It cannot download any received message because of low access privileges and thus won&apos;t cause any message to be accepted.
- It uses a login procedure which does not require user interaction (username and password) and all login credentials are remembered by the application.</source>
        <translation>Tato schránka může být použita ke stahování seznamů zpráv na pozadí, protože:
- Díky nízkým oprávněním z ní nelze stahovat dodané zprávy a proto nebude způsobovat doručení datových zpráv.
- Používá způsob přihlašování, který nevyžaduje uživatelskou interakci (přihlášení jménem a heslem) a všechny přihlašovací údaje si aplikace pamatuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1456"/>
        <source>Adding new user succeeded</source>
        <translation>Úspěšné přidání nového uživatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1459"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="1738"/>
        <source>Adding new user failed</source>
        <translation>Přidání nového uživatele selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1550"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="1637"/>
        <source>User modification succeeded</source>
        <translation>Úspěšné upravení dat uživatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1553"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="1640"/>
        <source>User modification failed</source>
        <translation>Upravení dat uživatele selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1684"/>
        <source>Delete User?</source>
        <translation>Smazat uživatele?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1685"/>
        <source>Do you want to delete the user &apos;%1&apos;?</source>
        <translation>Chcete smazat uživatele &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1739"/>
        <source>User &apos;%1&apos; could not be deleted.</source>
        <translation>Uživatel &quot;%1&quot; nemohl být smazán.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="2023"/>
        <source>Hide</source>
        <translation>Skrýt</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="528"/>
        <source>Open Certificate</source>
        <translation>Otevřít certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="137"/>
        <source>Password + Security code</source>
        <translation>Heslo + Bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="138"/>
        <source>Password + Security SMS</source>
        <translation>Heslo + SMS kód</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="392"/>
        <source>Test Environment Data Box</source>
        <translation>Schránka testovacího prostředí</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="393"/>
        <source>Production Environment Data Box</source>
        <translation>Schránka produkčního prostředí</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1217"/>
        <source>The list contains users who have access to this data box.</source>
        <translation>Seznam obsahuje uživatele, kteří mají přístup do této datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1443"/>
        <source>You can change or withdraw his privileges at any time in future.</source>
        <translation>Kdykoliv v budoucnu můžete jeho oprávnění změnit nebo zrušit.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1450"/>
        <source>You are using a testing environment data box.</source>
        <translation>Používáte schránku testovacícho prostředí.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1451"/>
        <source>The server response displayed above directly contains the login credentials.</source>
        <translation>Odezva serveru zobrazená výše přímo obsahuje přihlašovací údaje.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1842"/>
        <source>Update data box %1</source>
        <translation>Aktualizovat schránku %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1848"/>
        <source>Enter password for data box %1</source>
        <translation>Zadejte heslo pro schránku %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1856"/>
        <source>Set certificate for data box %1</source>
        <translation>Nastavit certifikát pro schránku %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1872"/>
        <source>Enter communication code for data box %1</source>
        <translation>Zadejte komunikační kód pro schránku %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="1911"/>
        <source>File does not exists or cannot be read.</source>
        <translation>Soubor neexistuje, nebo jej nelze přečíst.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="107"/>
        <source>Please enter credentials for your data box.</source>
        <translation>Prosím, zadejte přihlašovací údaje pro přístup k Vaší datové schránce.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="166"/>
        <source>Username:</source>
        <translation>Uživatelské jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="78"/>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="402"/>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="443"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="20"/>
        <source>Create a new data box</source>
        <translation>Přidat novou schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="61"/>
        <source>Data-Box Actions</source>
        <translation>Akce se schránkami</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="138"/>
        <source>Data-box title:</source>
        <translation>Pojmenování schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="173"/>
        <source>Enter the login name for this data box.</source>
        <translation>Zadejte přihlašovací jméno k této schránce.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="198"/>
        <source>This is a test data box</source>
        <translation>Tato schránka je testovací</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="211"/>
        <source>Login method:</source>
        <translation>Způsob přihlašování:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="302"/>
        <source>The certificate file is needed for authentication purposes. The supplied file
must contain a certificate and its corresponding private key. Only PEM and PFX file
formats are accepted.</source>
        <translation>Soubor s certifikátem je vyžadován za účelem autentizace. Dodaný soubor
musí obsahovat jak certifikát, tak i odpovídající soukromý klíč. Jen formáty
PEM a PFX jsou podporovány.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="248"/>
        <source>Enter your password.</source>
        <translation>Zadejte Vaše heslo.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="258"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="2018"/>
        <source>View</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="283"/>
        <source>If no PIN is configured then the password will be saved
in a readable format in the configuration file which is
usually located in your home directory. Do not use this
possibility if you are concerned about the security
of your account.

If you configure a PIN (master password) then the password
is going to be stored in an encrypted form into the configuration
file. If you forget the PIN then there is no easy way how to obtain
the stored password in decrypted form.</source>
        <translation>Pokud není nastaven PIN, pak bude heslo uloženo v čitelném
formátu v konfiguračním souboru, který se obyčejně nachází
ve Vašem domovském adresáři. Nepoužívejte tuto možnost,
pokud si nejste jisti zabezpečením svého uživatelského účtu.

Pokud nastavíte PIN (hlavní heslo), pak bude heslo uloženo
do konfiguračního souboru v zašifrované podobě. Pokud PIN
zapomenete, pak neexistuje jednoduchý způsob, jak obdržet
uložené heslo v dešifrované podobě.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="316"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="1918"/>
        <source>Select a certificate.</source>
        <translation>Vyberte certifikát pro přihlášení.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="319"/>
        <location filename="../src/gui/dlg_create_account.cpp" line="1917"/>
        <source>Select File</source>
        <translation>Vybrat soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="345"/>
        <source>The communication code is a string which can be generated in the ISDS web portal.</source>
        <translation>Komunikační kód je textový řetězec, který se vygeneruje na portálu ISDS při párování
zařízení s aplikací eGov mobilní klíč.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="348"/>
        <source>Code:</source>
        <translation>Komunikační kód:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="355"/>
        <source>Enter the communication code.</source>
        <translation>Zadejte komunikační kód.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="371"/>
        <source>If selected, the data box will be included into
the synchronisation process of all data boxes.</source>
        <translation>Schránka bude zahrnuta do hromadné synchronizace
všech schránek, pokud je tato volba nastavena.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="375"/>
        <source>Synchronise this data box together with all data boxes</source>
        <translation>Synchronizovat tuto schránku společně se všemi schránkami</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="427"/>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="468"/>
        <source>Update Information</source>
        <translation>Aktualizovat informace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="520"/>
        <source>User Actions</source>
        <translation>Akce s uživateli</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="239"/>
        <source>Password:</source>
        <translation>Heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="132"/>
        <source>The databox title is a user-specified name used for the identification
of the databox in the application (e.g. &apos;My Personal Data Box&apos;, &apos;Firm Box&apos;,
etc.). The chosen name serves only for your convenience.
The entry must be filled in.</source>
        <translation>Pojmenování schránky je uživatelem zvolené jméno, které slouží k identifikaci
schránky v této aplikaci (např. &quot;Moje osobní datová schránka&quot;, &quot;Firemní schránka&quot;,
atd.). Zvolené jméno slouží jen pro Vaši lepší orientaci.
Tato položka musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="145"/>
        <source>Enter custom data-box name.</source>
        <translation>Zadejte název schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="159"/>
        <source>The username must consist of at least 6 characters without spaces (only
combinations of lower-case letters and digits are allowed).
The entry must be filled in.

Notification: The username is not a data-box ID.</source>
        <translation>Uživatelské jméno se musí skládat alespoň z 6 znaků bez mezer (jsou povoleny
jen kombinace malých písmen a číslic).
Položka musí být vyplněna.

Upozornění: Uživatelské jméno není ID datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="193"/>
        <source>If the credentials you entered are for a test data box,
select this option. For normal data box
(created at Czech Point) leave this unchecked.</source>
        <translation>Pokud zadáváte přihlašovací údaje k testovací schránce,
vyberte tuto možnost.V případě normální schránky
(založené na Czech Pointu) ponechte nastavení nezaškrtnuté.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="205"/>
        <source>Select the login method which you use to access the data box in the ISDS.
Note: NIA login methods such as bank ID, mojeID or eCitizen aren&apos;t
supported because the ISDS system doesn&apos;t provide such
functionality for third-party applications.</source>
        <translation>Zvolte způsob přihlašování, který používíte pro přístup do datové schránky
v prostředí ISDS.
Poznámka: Přihlašovací metody NIA jako bankovní ID, mojeID nebo eObčanka
nejsou podporovány, protože systém ISDS takovou funkcionalitu pro aplikace
třetích stran neposkytuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="218"/>
        <source>Select authorisation method for logging in to the data box.</source>
        <translation>Vyberte metodu pro přihlášení do datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="232"/>
        <source>The password must be valid and non-expired. To check whether you&apos;ve entered
the password correctly you may use the button &apos;View&apos; in the field on the right.

Notification: If it is your very first attempt to log into the data box then you must
log into the ISDS web portal first where you will need to change your password.</source>
        <translation>Heslo musí být platné. Pro kontrolu a náhled správnosti zadaného hesla
použijte tlačítko &quot;Zobrazit&quot; vpravo.

Poznámka: Musíte si nejprve změnit heslo na porálu ISDS, pokud se přihlašujete
do schránky úplně poprvé s přiděleným heslem od provozovatele systému ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="295"/>
        <source>Remember password</source>
        <translation>Pamatovat si heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="307"/>
        <source>Certificate file:</source>
        <translation>Certifikát:</translation>
    </message>
</context>
<context>
    <name>DlgCreateAccountFromDb</name>
    <message>
        <location filename="../src/gui/ui/dlg_account_from_db.ui" line="14"/>
        <source>Create data box from database file</source>
        <translation>Vytvořit schránku z existující databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_account_from_db.ui" line="27"/>
        <source>What do you want to create the data box(es) from?</source>
        <translation>Jakým způsobem chcete schránku(schránky) vytvořit?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_account_from_db.ui" line="33"/>
        <source>Database files from directory</source>
        <translation>Databázové soubory z adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_account_from_db.ui" line="43"/>
        <source>Selected database files</source>
        <translation>Vybrané databázové soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="46"/>
        <source>A new data box will be created according to the name and the content of the database file. This data box will operate over the selected database. Should such a data box or database file already exist in Datovka then the association will fail. During the association no database file copy is created nor is the content of the database file modified. Nevertheless, we strongly advice you to back-up all important files before associating a database file. In order for the association to succeed you will need an active connection to the ISDS server.</source>
        <translation>Z názvu a obsahu vybraného databázového souboru se v aplikaci vytvoří nová schránka, která bude pracovat s vybranou databází. Pokud již tato schránka či databáze v Datovce existují, nebude se účet znova vytvářet. Při vytváření schránky a jejího přiřazení k databázi se nevytváří žádná kopie databázového souboru ani se nemodifikuje jeho obsah, přesto silně doporučujeme databázový soubor pro jistotu před importem zálohovat. Pro úspěšné přiřazení databáze k nové schránce je také zapotřebí aktivní připojení k serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="86"/>
        <source>Select directory</source>
        <translation>Vybrat adresář pro import</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="100"/>
        <source>No database file found</source>
        <translation>Nenalezen žádný databázový soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="101"/>
        <source>No database file found in selected directory &apos;%1&apos;.</source>
        <translation>Nenalezeny žádné databázové soubory ve vybraném adresáři &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="112"/>
        <source>Select database files</source>
        <translation>Vybrat databázové soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="113"/>
        <source>Database file (*.db)</source>
        <translation>Databázový soubor (*.db)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="156"/>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="180"/>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="207"/>
        <source>Create data box: %1</source>
        <translation>Nová schránka: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="175"/>
        <source>Data box with username &apos;%1&apos; and its message database already exist. New data box was not created and selected database file was not associated with this data box.</source>
        <translation>Schránka s uživatelským jménem &quot;%1&quot; a jeho databáze již existují. Nová schránka nebyla vytvořena a vybraná databáze nebyla ke schránce přiřazena.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="199"/>
        <source>Data box with name &apos;%1&apos; has been created (username &apos;%1&apos;).</source>
        <translation>Schránka s názvem &quot;%1&quot; byla vytvořena (uživatelské jméno &quot;%1&quot;).</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="202"/>
        <source>This database file has been set as the actual message database for this data box. You&apos;ll probably have to modify the data-box properties in order to log in to the ISDS server correctly.</source>
        <translation>Tento databázový soubor byl nastaven jako aktuální databáze pro ukládání zpráv pro tuto schránku. Možná bude zapotřebí upravit vlastnosti této schránky, aby bylo možné se připojit k serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="158"/>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="182"/>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="209"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
</context>
<context>
    <name>DlgDownloadMessages</name>
    <message>
        <location filename="../src/gui/ui/dlg_download_messages.ui" line="14"/>
        <source>Download Messages</source>
        <translation>Stažení zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_download_messages.ui" line="52"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="381"/>
        <source>You don&apos;t have to download the content of those messages again.</source>
        <translation>Obsah těchto zpráv nemusíte znovu stahovat.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="383"/>
        <source>Do you also want to download the content of the already downloaded messages?</source>
        <translation>Chcete také stáhnout obsah již stažených zpráv?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="366"/>
        <source>The content of the following messages has already been downloaded:</source>
        <translation>Obsah následujících zpráv již byl stažen:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="379"/>
        <source>Message Content Available</source>
        <translation>Obsah zprávy k dispozici</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="380"/>
        <source>The content of some messages is already available.</source>
        <translation>Obsah některých zpráv je již k dipozici.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="433"/>
        <source>The content of the following messages has not been completely downloaded:</source>
        <translation>Obsah následujících zpráv ještě nebyl kompletně stažen:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="446"/>
        <source>Message Content Not Available</source>
        <translation>Obsah zprávy není k dispozici</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="447"/>
        <source>The content of some messages is not available.</source>
        <translation>Obsah některých zpráv není k dipozici.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="448"/>
        <source>Messages with missing content will be skipped.</source>
        <translation>Zprávy, kterým chybí jejich obsah, budou přeskočeny.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="450"/>
        <source>Do you want to download the missing message content now?</source>
        <translation>Přejete si nyní stáhnout chybějící obsah zpráv?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="530"/>
        <source>Interrupting download.</source>
        <translation>Přerušuji stahování.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="559"/>
        <source>Some messages could not be downloaded.</source>
        <translation>Některé zprávy nemohly být staženy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="562"/>
        <source>Do you want to proceed using all available messages?</source>
        <translation>Přejete si pokračovat se všemi dostupnými zprávami?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="574"/>
        <source>Download has been interrupted.</source>
        <translation>Stahování bylo přerušeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="577"/>
        <source>Do you want to proceed using the available messages?</source>
        <translation>Přejete si pokračovat s dostupnými zprávami?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="593"/>
        <source>No messages have been downloaded.</source>
        <translation>Nebyly staženy žádné zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="596"/>
        <source>There are other already downloaded messages. Do you want to proceed?</source>
        <translation>Jsou zde jiné již stažené zprávy. Přejete si pokračovat?</translation>
    </message>
</context>
<context>
    <name>DlgDsSearch</name>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="115"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="132"/>
        <source>All</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="115"/>
        <source>All types</source>
        <translation>Všechny</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="118"/>
        <source>OVM</source>
        <translation>OVM</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="121"/>
        <source>PO</source>
        <translation>PO</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="124"/>
        <source>PFO</source>
        <translation>PFO</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="127"/>
        <source>FO</source>
        <translation>FO</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="132"/>
        <source>Search in all fields</source>
        <translation>Hledat ve všech položkách</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="135"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="135"/>
        <source>Search in address data</source>
        <translation>Hledat podle adresy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="138"/>
        <source>Identification number</source>
        <translation>Identifikační číslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="138"/>
        <source>IČO</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="141"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="141"/>
        <source>Box identifier</source>
        <translation>ID datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="202"/>
        <source>Enter sought expression</source>
        <translation>Zadete hledaný výraz</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="325"/>
        <source>This is a special ID for a ISDS system data box. You can&apos;t use this ID for message delivery. Try again.</source>
        <translation>Toto je speciální ID &quot;systémové schránky&quot; ISDS. Momentálně není možné zasílat zprávy do této schránky. Hledejte dál.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="395"/>
        <source>Full-text data box search. Enter phrase for finding and set optional restrictions:</source>
        <translation>Fulltextové vyhledávání datových schránek. Zadejte vyhledávaný výraz a zvolte případná omezení:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="494"/>
        <source>commercial messages are enabled</source>
        <translation>poštovní datové zprávy jsou povoleny</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="502"/>
        <source>commercial messages are disabled</source>
        <translation>poštovní datové zprávy jsou vypnuty</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="552"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="553"/>
        <source>Enter last name of the PFO or company name.</source>
        <translation>Zadejte příjmení PFO nebo název firmy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="559"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="560"/>
        <source>Enter last name or last name at birth of the FO.</source>
        <translation>Zadejte příjmení nebo rodné příjmení FO.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="675"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="806"/>
        <source>Cannot execute task.</source>
        <translation>Nelze spustit úlohu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="896"/>
        <source>Total found</source>
        <translation>Celkem nalezeno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="772"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="879"/>
        <source>It was not possible find any data box because</source>
        <translation>Nebylo možné najít žádnou datovou schránku protože</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="118"/>
        <source>Public authority</source>
        <translation>Orgán veřejné moci</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="121"/>
        <source>Legal person</source>
        <translation>Právnická osoba</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="124"/>
        <source>Self-employed person</source>
        <translation>Podnikající fyzická osoba</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="127"/>
        <source>Natural person</source>
        <translation>Fyzická osoba</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="417"/>
        <source>Enter the box ID, identification number (IČO) or at least three letters from the name of the data box you look for:</source>
        <translation>Zadejte ID schránky, IČO nebo alespoň tři písmena z názvu hledané datové schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="484"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="496"/>
        <source>Your data box is of type</source>
        <translation>Vaše schránka je typu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="486"/>
        <source>You have also Post Data Messages activated.
This means you can only search for data boxes of type OVM and data boxes that have Post Data Messages delivery activated.
Because of this limitation the results of your current search might not contain all otherwise matching data boxes.</source>
        <translation>Máte také aktivovány poštovní datové zprávy.
To znamená, že můžete vyhledávat pouze schránky typu OVM a schránky,
které mají aktivováno doručování poštovních datových zpráv.
Vzhledem k tomuto omezení nemusí výsledky tohoto hledání obsahovat
všechny datové schránky, které by jinak vyhovovaly parametrům.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="498"/>
        <source>This means you can only search for data boxes of type OVM.
The current search settings will thus probably yield no result.</source>
        <translation>To znamená, že můžete vyhledávat pouze schránky typu OVM.
Vyhledávání s aktuálními parametry tak pravděpodobně nevrátí žádné výsledky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="781"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="887"/>
        <source>It was not possible find any data box because an error occurred during the search process!</source>
        <translation>Nebylo možné najít žádnou datovou schránku, protože během procesu vyhledávání došlo k chybě!</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="829"/>
        <source>Displayed</source>
        <translation>Zobrazeno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="537"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="544"/>
        <source>Subject Name:</source>
        <translation>Název subjektu:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="538"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="539"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="545"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="546"/>
        <source>Enter name of subject</source>
        <translation>Zadejte název instituce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="14"/>
        <source>Search recipient</source>
        <translation>Vyhledat adresáta</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="29"/>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="67"/>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="211"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="60"/>
        <source>Use full-text search similar to the ISDS client portal.</source>
        <translation>Použít fulltextové vyhledávání podobné vyhledávání na portálu ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="22"/>
        <source>Current data box:</source>
        <translation>Aktuální schránka:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="82"/>
        <source>Your data box is not of type OVM (i.e. non-OVM). Sending of post data messages
from your data box is activated. This means that you can only search for data boxes
of the type OVM and data boxes that have the the receiving of post data messages
activated. Because of this limitation the results of your current search may not
contain all otherwise matching databoxes.</source>
        <translation>Vaše schránka není typu OVM (tedy ne-OVM). Zasílání poštovních datových zpráv z Vaší
schránky je aktivní. To znamená, že můžete vyhledávat pouze datové schránky typu OVM
nebo schránky, které mají aktivován příjem poštovních datových zpráv. Díky tomuto
omezení nemusí výsledek tohoto vyhledávání obsahovat všechny jinak odpovídající
datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="89"/>
        <source>Note: Your search results will be limited. See tooltip for more information.</source>
        <translation>Poznámka: Výsledky vyhledávání budou omezeny. Více najdete v tooltipu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="109"/>
        <source>Databox type:</source>
        <translation>Typ datové schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="119"/>
        <source>Search in fileds:</source>
        <translation>Vyhledávat v položkách:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="136"/>
        <source>Text:</source>
        <translation>Text:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="146"/>
        <source>Box ID:</source>
        <translation>ID schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="156"/>
        <source>Identification number (IČO):</source>
        <translation>IČO:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="166"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="551"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="176"/>
        <source>Postal code:</source>
        <translation>Směrovací číslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="191"/>
        <source>Search</source>
        <translation>Vyhledat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="231"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="558"/>
        <source>Last Name:</source>
        <translation>Příjmení:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="324"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="765"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="771"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="873"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="878"/>
        <source>Search result</source>
        <translation>Výsledek hledání</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="780"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="886"/>
        <source>Search error</source>
        <translation>Chyba při vyhledávání</translation>
    </message>
</context>
<context>
    <name>DlgEmailContent</name>
    <message>
        <location filename="../src/gui/ui/dlg_email_content.ui" line="14"/>
        <source>Select E-mail Content</source>
        <translation>Výběr obsahu e-mailu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_email_content.ui" line="20"/>
        <source>What content do you want to append into the created e-mail?</source>
        <translation>Jaký obsah si přejete vložit do vytvářeného e-mailu?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_email_content.ui" line="26"/>
        <source>whole data message in ZFO format</source>
        <translation>celou datovou zprávu v ZFO formátu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_email_content.ui" line="33"/>
        <source>delivery info in ZFO format</source>
        <translation>doručenku v ZFO formátu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_email_content.ui" line="40"/>
        <source>all attachments of the messages</source>
        <translation>všechny přílohy zpráv</translation>
    </message>
</context>
<context>
    <name>DlgGovService</name>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="14"/>
        <source>E-Government Request</source>
        <translation>Elektronické podání</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="28"/>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="41"/>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="68"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="48"/>
        <source>Data box:</source>
        <translation>Schránka:</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="55"/>
        <source>Recipient:</source>
        <translation>Adresát:</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="75"/>
        <source>Request:</source>
        <translation>Požadavek:</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="91"/>
        <source>The service requires these fields to be filled in:</source>
        <translation>Služba vyžaduje vyplnění těchto položek:</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="151"/>
        <source>Send</source>
        <translation>Odeslat</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_service.ui" line="158"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="212"/>
        <source>Message sent</source>
        <translation>Zpráva odeslána</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="213"/>
        <source>E-gov request was successfully sent.</source>
        <translation>Elektronické podání bylo úspěšně odesláno.</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="214"/>
        <source>Message was sent to &lt;i&gt;%1 (%2)&lt;/i&gt; as message number &lt;i&gt;%3&lt;/i&gt;.</source>
        <translation>Zpráva byla odeslána adresátu &lt;i&gt;%1 (%2)&lt;/i&gt; jako zpráva číslo &lt;i&gt;%3&lt;/i&gt;.</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="220"/>
        <source>Message not sent</source>
        <translation>Zpráva neodeslána</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="221"/>
        <source>E-gov request could NOT be sent.</source>
        <translation>Elektronické podání NEBYLO možné odeslat.</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="222"/>
        <source>ISDS returns:</source>
        <translation>ISDS vrací:</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="244"/>
        <source>No user data needed.</source>
        <translation>Nejsou vyžadována uživatelská data.</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="361"/>
        <source>Request: %1</source>
        <translation>Požadavek: %1</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="363"/>
        <source>Recipient: %1</source>
        <translation>Adresát: %1</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="367"/>
        <source>Send e-gov request</source>
        <translation>Odeslat elektronické podání</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_service.cpp" line="368"/>
        <source>Do you want to send the e-gov request to data box &apos;%1&apos;?</source>
        <translation>Chcete odeslat elektronické podání do datové schránky &apos;%1&apos;?</translation>
    </message>
</context>
<context>
    <name>DlgGovServices</name>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_services.ui" line="14"/>
        <source>E-government Requests</source>
        <translation>Elektronická podání</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_services.ui" line="20"/>
        <source>Select the e-gov request form you want to fill in and send:</source>
        <translation>Zvolte formulář elektronického podání, jenž chcete vyplnit a odeslat:</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_services.ui" line="45"/>
        <source>Data box:</source>
        <translation>Schránka:</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_services.ui" line="63"/>
        <source>You cannot send any e-gov requests from this data box.</source>
        <translation>Z této schránky nelze odeslat žádné elektronické podání.</translation>
    </message>
    <message>
        <location filename="../src/gov_services/ui/dlg_gov_services.ui" line="82"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
    <message>
        <location filename="../src/gov_services/gui/dlg_gov_services.cpp" line="66"/>
        <source>Enter sought expression</source>
        <translation>Zadete hledaný výraz</translation>
    </message>
</context>
<context>
    <name>DlgImportZFO</name>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="20"/>
        <source>ZFO import</source>
        <translation>Import ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="59"/>
        <source>What do you want to import?</source>
        <translation>Co chcete importovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="95"/>
        <source>How do you want to import?</source>
        <translation>Jakým způsobem chcete importovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="111"/>
        <source>Import all ZFO files from selected directory</source>
        <translation>Importovat všechny ZFO soubory ze zvoleného adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="65"/>
        <source>Message and acceptance info ZFO files</source>
        <translation>Zprávy a doručenky současně</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="26"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="75"/>
        <source>Only message ZFO files</source>
        <translation>Pouze soubory zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="85"/>
        <source>Only acceptance info ZFO files</source>
        <translation>Pouze doručenky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="101"/>
        <source>Import selected ZFO files</source>
        <translation>Importovat vybrané ZFO soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="126"/>
        <source>Include subdirectories to the import</source>
        <translation>Zahrnout podadresáře do importu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="154"/>
        <source>Import options</source>
        <translation>Možnosti importu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="162"/>
        <source>Sending imported data to ISDS server to check them
is significantly slower but safer.
By disabling this option you may introduce invalid data
into your local database.</source>
        <translation>Odesílání importovaných dat serveru ISDS za účelem
jejich ověření je značně pomalejší, ale bezpečnější.
Vypnutím této volby můžete do své lokální databáze
zanést neplatná data.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="168"/>
        <source>Check imported ZFO files on server</source>
        <translation>Ověřit importované ZFO soubory na serveru</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_import_zfo.cpp" line="37"/>
        <source>Here you can import whole messages and message acceptance information from ZFO files into the local database. The message or acceptance information import will succeed only for those files whose validity can be approved by the ISDS server (working connection to server is required). Acceptance information ZFOs will be inserted into the local database only if a corresponding complete message already exists in the local database.</source>
        <translation>Zde můžete importovat celé zprávy a doručenky ze ZFO souborů do místní databáze. Import zprávy nebo doručenky proběhne úspěšně jen pro ty soubory, jejichž správnost se podaří ověřit na serveru Datové schránky (je vyžadováno funkční spojení se serverem). ZFO doručenky jde vložit do databáze pouze tehdy, pokud již v databázi existuje odpovídající zpráva.</translation>
    </message>
</context>
<context>
    <name>DlgImportZFOResult</name>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="14"/>
        <source>Import ZFO result</source>
        <translation>Výsledek importu ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="22"/>
        <source>Number of ZFO files for import to databases:</source>
        <translation>Počet ZFO souborů k importování do databází:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="53"/>
        <source>Number of newly imported files:</source>
        <translation>Počet nově importovaných souborů:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="75"/>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="123"/>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="164"/>
        <source>Details:</source>
        <translation>Detaily:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="101"/>
        <source>Number of existing files:</source>
        <translation>Počet již existujícíh v databázi:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="142"/>
        <source>Number of unsuccessfully imported files:</source>
        <translation>Počet neúspěšně importovaných souborů:</translation>
    </message>
</context>
<context>
    <name>DlgLicence</name>
    <message>
        <location filename="../src/gui/ui/dlg_licence.ui" line="20"/>
        <source>Licence</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_licence.cpp" line="41"/>
        <source>File &apos;%1&apos; either doesn&apos;t exist or is empty.</source>
        <translation>Soubor &apos;%1&apos; buď neexistuje nebo je prázdný.</translation>
    </message>
</context>
<context>
    <name>DlgMsgSearch</name>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="65"/>
        <source>Search also in other data boxes</source>
        <translation>Vyhledávat také v ostatních schránkách</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="81"/>
        <source>Message type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="87"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="97"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="110"/>
        <source>Fill in data according to which you want to search:</source>
        <translation>Vyplňte údaje, dle kterých chcete vyhledávat:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="120"/>
        <source>Message ID:</source>
        <translation>ID zprávy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="138"/>
        <source>Subject:</source>
        <translation>Předmět:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="162"/>
        <source>Sender data-box ID:</source>
        <translation>ID schránky odesílatete:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="179"/>
        <source>Sender name:</source>
        <translation>Jméno odesílatele:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="189"/>
        <source>Sender ref. num.:</source>
        <translation>Jednací číslo odesílatele:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="199"/>
        <source>Sender file mark:</source>
        <translation>Spisová značka odesílatele:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="243"/>
        <source>Recipient ref. num.:</source>
        <translation>Jednací číslo adresáta:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="253"/>
        <source>Recipient file mark:</source>
        <translation>Spisová značka adresáta:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="310"/>
        <source>Delivery from:</source>
        <translation>Dodání od:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="319"/>
        <source>Check this field if you want to restrict the delivery date
of the oldest sought message.</source>
        <translation>Zaškrtněte toto pole, chcete-li omezit datum dodání
nejstarší hledané zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="376"/>
        <source>Delivery to:</source>
        <translation>Dodání do:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="385"/>
        <source>Check this field if you want to restrict the delivery date
of the youngest sought message.</source>
        <translation>Zaškrtněte toto pole, chcete-li omezit datum dodání
nejmladší hledané zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="529"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="219"/>
        <source>Recipient name:</source>
        <translation>Jméno adresáta:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="20"/>
        <source>Advanced Message Search</source>
        <translation>Rozšířené vyhledávání zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="50"/>
        <source>Data box:</source>
        <translation>Schránka:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="62"/>
        <source>It may be slow.</source>
        <translation>Může být pomalé.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="233"/>
        <source>Recipient data-box ID:</source>
        <translation>ID schránky adresáta:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="271"/>
        <source>Address:</source>
        <translation>Adresa:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="285"/>
        <source>To hands:</source>
        <translation>K rukám:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="443"/>
        <source>Attachment name:</source>
        <translation>Název přílohy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="457"/>
        <source>Tag text:</source>
        <translation>Text tagu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="481"/>
        <source>Too many parameters to search for!</source>
        <translation>Příliš mnoho parametrů vyhledávání!</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="493"/>
        <source>Search for messages matching all supplied criteria</source>
        <translation>Hledat zprávy odpovídající všem zadaným kriteriím</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="511"/>
        <source>Search</source>
        <translation>Vyhledat</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="83"/>
        <source>Enter sought expression</source>
        <translation>Zadete hledaný výraz</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="591"/>
        <source>E-mail with</source>
        <translation>E-mail s</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="785"/>
        <source>Download Signed Message</source>
        <translation>Stáhnout podepsanou zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="786"/>
        <source>Download the complete message, i.e. including attachments, and verify its signature.</source>
        <translation>Stáhnout kompletní zprávu, tj. včetně příloh, a ověřit její podpis.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="794"/>
        <source>Export Message as ZFO</source>
        <translation>Exportovat zprávu jako soubor ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="795"/>
        <source>Export the selected message as a ZFO file.</source>
        <translation>Exportovat vybranou zprávu jako ZFO soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="803"/>
        <source>Export Acceptance Info as ZFO</source>
        <translation>Exportovat doručenku do ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="804"/>
        <source>Export the acceptance information of the selected message as a ZFO file.</source>
        <translation>Exportovat doručenku vybrané zprávy jako ZFO soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="812"/>
        <source>Export Acceptance Info as PDF</source>
        <translation>Exportovat doručenku do PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="813"/>
        <source>Export the content of the acceptance information
of the selected message into a PDF file.</source>
        <translation>Exportovat obsah doručenky
vybrané zprávy do PDF souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="822"/>
        <source>Export Message Envelope as PDF</source>
        <translation>Exportovat obálku zprávy do PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="823"/>
        <source>Export content of the envelope
of the selected message as a PDF file.</source>
        <translation>Exportovat obsah obálky
vybrané zprávy do PDF souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="832"/>
        <source>Export Envelope PDF with Attachments</source>
        <translation>Exportovat obálku jako PDF s přílohami</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="833"/>
        <source>Export the content of the envelope
of the selected message to a PDF file.
Also export message attachments.</source>
        <translation>Exportovat obsah obálky
vybrané zprávy do PDF souboru.
Také exportovat přílohy zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="843"/>
        <source>Message ZFOs</source>
        <translation>ZFO zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="852"/>
        <source>All Attachments</source>
        <translation>Všemi přílohami</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="844"/>
        <source>Creates an e-mail containing ZFOs of selected messages.</source>
        <translation>Vytvořit e-mail obsahující ZFO vybraných zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="853"/>
        <source>Creates an e-mail containing all attachments of selected messages.</source>
        <translation>Vytvořit e-mail obsahující všechny přílohy vybraných zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="861"/>
        <source>Selected Content</source>
        <translation>Vybraným obsahem</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="862"/>
        <source>Creates an e-mail containing selected content.</source>
        <translation>Vytvořit e-mail s vybraným obsahem.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="889"/>
        <source>Double clicking on a found message will change focus of the selected message in the main application window. Note: You can view additional information when hovering the mouse cursor over the message ID.</source>
        <translation>Dvojklik na nalezenou zprávu změní fokus zvolené zprávy v hlavním okně aplikace. Poznámka: Dodatečné informace je možné zobrazit podržením kurzoru myši nad identifikátorem zprávy.</translation>
    </message>
</context>
<context>
    <name>DlgPinInput</name>
    <message>
        <location filename="../src/gui/ui/dlg_pin_input.ui" line="14"/>
        <source>Enter PIN</source>
        <translation>Zadejte PIN</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_pin_input.ui" line="39"/>
        <location filename="../src/gui/ui/dlg_pin_input.ui" line="63"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_pin_input.ui" line="115"/>
        <source>Enter the PIN value.</source>
        <translation>Zadejte PIN.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_pin_input.cpp" line="69"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_pin_input.ui" line="118"/>
        <source>Enter PIN code</source>
        <translation>Zadejte PIN kód</translation>
    </message>
</context>
<context>
    <name>DlgPinSetup</name>
    <message>
        <location filename="../src/gui/ui/dlg_pin_setup.ui" line="14"/>
        <source>PIN Setup</source>
        <translation>Nastavení PINu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_pin_setup.ui" line="25"/>
        <source>The PIN (master password) may consist of any character. It isn&apos;t restricted to digits only. Easy passwords provide less protection.</source>
        <translation>PIN (hlavní heslo) může sestávat z jakýchkoli znaků. Není omezen pouze na číslice. Jednoduchá hesla poskytují slabší ochranu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_pin_setup.ui" line="43"/>
        <source>Current PIN</source>
        <translation>Současný PIN</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_pin_setup.ui" line="53"/>
        <source>New PIN</source>
        <translation>Nový PIN</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_pin_setup.ui" line="63"/>
        <source>Repeat new PIN</source>
        <translation>Potvrdit nový PIN</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_pin_setup.cpp" line="80"/>
        <location filename="../src/gui/dlg_pin_setup.cpp" line="90"/>
        <source>Wrong PIN value</source>
        <translation>Špatný PIN</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_pin_setup.cpp" line="81"/>
        <source>Entered wrong current PIN.</source>
        <translation>Byl zadán špatný současný PIN.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_pin_setup.cpp" line="91"/>
        <source>Entered new PIN values are different.</source>
        <translation>Zadané hodnoty nového PINu se liší.</translation>
    </message>
</context>
<context>
    <name>DlgPreferences</name>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="135"/>
        <source>Restart Needed</source>
        <translation>Vyžadován restart</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="136"/>
        <source>In order to put this change into action you must restart the application after the new settings have been confirmed.</source>
        <translation>Aby se tato změna promítla do nastavení aplikace, musíte po potvrzení nových nastavení restartovat aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="158"/>
        <location filename="../src/gui/dlg_preferences.cpp" line="169"/>
        <source>Select Directory</source>
        <translation>Vybrat adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="243"/>
        <source>Choose a Colour</source>
        <translation>Zvolte barvu</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/gui/dlg_preferences.cpp" line="508"/>
        <source>Note: If you have a slow network connection or you cannot download complete messages, here you can increase the connection timeout. Default value is %n minute(s). Use 0 to disable the timeout limit (not recommended).</source>
        <translation>
            <numerusform>Poznámka: Pokud máte pomalé síťové připojení, nebo se Vám nedaří stáhnout komplentí zprávy, můžete zvýšit časový limit spojení. Výchozí hodnota je %n minuta. Použijte 0 pro vypnutí limitu (není doporučeno).</numerusform>
            <numerusform>Poznámka: Pokud máte pomalé síťové připojení, nebo se Vám nedaří stáhnout komplentí zprávy, můžete zvýšit časový limit spojení. Výchozí hodnota je %n minuty. Použijte 0 pro vypnutí limitu (není doporučeno).</numerusform>
            <numerusform>Poznámka: Pokud máte pomalé síťové připojení, nebo se Vám nedaří stáhnout komplentí zprávy, můžete zvýšit časový limit spojení. Výchozí hodnota je %n minut. Použijte 0 pro vypnutí limitu (není doporučeno).</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/gui/dlg_preferences.cpp" line="520"/>
        <source>Note: A marked unread message will be marked as read after the set interval. Default value is %n second(s). Use -1 to disable the function.</source>
        <translation>
            <numerusform>Poznámka: Nepřečtená zpráva bude označena jako lokálně přečtená za nastavený čas. Výchozí hodnota je %n sekunda. Použijte -1 pro vytnutí této funkce.</numerusform>
            <numerusform>Poznámka: Nepřečtená zpráva bude označena jako lokálně přečtená za nastavený čas. Výchozí hodnota je %n sekundy. Použijte -1 pro vytnutí této funkce.</numerusform>
            <numerusform>Poznámka: Nepřečtená zpráva bude označena jako lokálně přečtená za nastavený čas. Výchozí hodnota je %n sekund. Použijte -1 pro vytnutí této funkce.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="646"/>
        <source>Note: Messages to be deleted from the ISDS within the specified period are going to be highlighted using the set colour in the message listing. Use 0 to disable the function.</source>
        <translation>Poznámka: Zprávy, které mají být z ISDS smazány v průběhu určeného intervalu, budou v seznamu zpráv zvýrazněny nastavenou barvou. Použijte 0 pro vytnutí této funkce.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="655"/>
        <location filename="../src/gui/dlg_preferences.cpp" line="658"/>
        <source>Default</source>
        <translation>Výchozí</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="656"/>
        <source>Custom</source>
        <translation>Vlastní</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="659"/>
        <source>Light</source>
        <translation>Světlý</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="660"/>
        <source>Dark</source>
        <translation>Tmavý</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="770"/>
        <source>Use system language</source>
        <translation>Použít jazyk systému</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="771"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="772"/>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="20"/>
        <source>Datovka - Preferences</source>
        <translation>Datovka - nastavení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="42"/>
        <source>Downloading</source>
        <translation>Stahování</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="65"/>
        <source>Message downloading</source>
        <translation>Stahování zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="82"/>
        <source>When allowed, the application will automatically download
messages on background without blocking the user interface.
Download period can be selected below.</source>
        <translation>Povolíte-li tuto možnost, aplikace bude automaticky stahovat
nové zprávy na pozadí bez blokování uživatelského rozhraní.
Periodu stahování můžete nastavit níže.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="87"/>
        <source>Automatically synchronise all in background</source>
        <translation>Automaticky provádět hromadnou synchronizaci na pozadí</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="101"/>
        <source>Check every</source>
        <translation>Kontrolovat každých</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="113"/>
        <source>Sets a time interval for automatic synchronisation
of all accounts on background.</source>
        <translation>Nastavuje interval pro periodickou automatickou
synchronizaci všech účtů na pozadí aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="135"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="246"/>
        <source>minutes</source>
        <translation>minut</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="162"/>
        <source>When accessing the data boxes, only envelopes of messages are downloaded at first.
You can then download the whole message, including any attachments, manually.
When this option is turned on, the application will perform automatic downloading
of complete messages. The only downside to this approach is a longer waiting time
on slower internet connections.</source>
        <translation>Při přístupu k datovým schránkám bude aplikace nejprve stahovat pouze obálky zpráv.
Poté je možné ručně stáhnout celou zprávu včetně příloh. Pokud zapnete tuto volbu,
aplikace automaticky stáhne kompletní zprávy. Jedinou nevýhodou tohoto přístupu
je delší doba stahování při pomalejším internetovém připojení.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="169"/>
        <source>Automatically download whole messages (may be slow)</source>
        <translation>Automaticky stahovat kompletní zprávy (může být pomalé)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="200"/>
        <source>Connection settings</source>
        <translation>Nastavení připojení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="213"/>
        <source>Timeout for message downloading is set on</source>
        <translation>Časový limit pro stahování zpráv je nastaven na</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="225"/>
        <source>Allows to set a timeout interval for network connection.
If you have a slow network connection or you cannot
download complete messages, here you can increase
the connection timeout.</source>
        <translation>Umožňuje nastavit časový limit pro držení sítového připojení.
Jestliže máte pomalé připojení k internetu, nebo se nedaří
stáhnou kompletní zprávy, zde můžete zvýšit časový limit pro
udržení spojení.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="292"/>
        <source>Mark message as read settings</source>
        <translation>Nastavení označování přečtených zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="308"/>
        <source>Automatically mark message as read after</source>
        <translation>Automaticky označit zprávu jako lokálně přečtenou za</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="320"/>
        <source>Allows to set a timeout interval for
automatic marking of messages as read.</source>
        <translation>Umožňuje nastavit časový interval pro
automatické označování zpráv jako přečtené.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="342"/>
        <source>seconds</source>
        <translation>sekund</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="388"/>
        <source>New versions</source>
        <translation>Nové verze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="399"/>
        <source>When this option is active, the application will automatically
check for new application versions on startup.</source>
        <translation>Pokud je tato volba aktivní, aplikace bude automaticky
zjišťovat nové verze programu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="403"/>
        <source>Check for new application versions on startup</source>
        <translation>Po spuštění kontrolovat nové verze aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="430"/>
        <source>Security</source>
        <translation>Bezpečnost</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="441"/>
        <source>Storage options</source>
        <translation>Volba uložení dat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="453"/>
        <source>It migth be possible for a person with access to the configuration directory to modify data in the application databases with a malicious intent or to steal you data-box login credentials. Using the checkboxes below, you can influence which data the application stores on the disk.
Note: Password storing can be adjusted on a per-account basis in the credentials dialogue.</source>
        <translation>Osoby, které mají přístup do konfiguračního adresáře, mohou zlomyslně modifikovat data v databázi, nebo odcizit přihlašovací údaje k datovým schránkám. Pomocí následujících voleb můžete zvolit, jaká data se budou ukládat na disk.
Poznámka: Ukládání hesla se dá nastavit pro jednotlivé účty v nastavení účtu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="469"/>
        <source>When allowed, messages are stored in database files. Such storage
might be compromised by an attacker and the content of messages
might be modified.
When turned off, messages are freshly downloaded each time you
start the application. In this case, messages older than 90 days
may not be available.</source>
        <translation>Povolíte-li tuto možnost, budou stažené zprávy ukládány do databázových
souborů. Takové úložiště může být napadeno útočníkem a obsah zpráv
upraven.
Pokud tuto možnost vypnete, zprávy se budou při každém spuštění aplikace
nově stahovat. V tomto případě nemusí být k dispozici zprávy starší 90 dní.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="477"/>
        <source>Allow storing of messages</source>
        <translation>Povolit ukládání zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="492"/>
        <source>The application stores some additional data outside the message
database. These contain information about the data boxes allowing
for notification about expiring passwords etc. These data might also
be changed by an attacker, but with a relative low impact.</source>
        <translation>Aplikace ukládá některá doplňková data mimo databázi zpráv. Jsou to data
o datových schránkách, která umožňují varovat před vypršením hesla, atd.
Tato data by mohla být rovněž změněna případným útočníkem, ale s relativně
malým dopadem.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="498"/>
        <source>Allow storing of additional data</source>
        <translation>Povolit ukládání ostatních dat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="514"/>
        <source>Note: Storage settings will not be applied until you restart the application.</source>
        <translation>Poznámka: Nastavení ukládání nebude změněno dokud nerestartujete aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="529"/>
        <source>Signing certificate validity</source>
        <translation>Platnost podepisujícího certifikátu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="545"/>
        <source>Check against current date (safer)</source>
        <translation>Kontrolovat proti aktuálnímu datu (bezpečnější)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="567"/>
        <source>Check against the date of download</source>
        <translation>Kontrolovat proti datu stažení zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="586"/>
        <source>Check certificate revocation list (CRL).</source>
        <translation>Kontrolovat seznam zneplatněných certifikátů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="589"/>
        <source>Check certificate revocation list (CRL)</source>
        <translation>Kontrolovat seznam zneplatněných certifikátů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="607"/>
        <source>Message time stamps expiring within the selected numer
of days will be included into the expiration notification summary.</source>
        <translation>Časová razítka zpráv expirujících v průběhu zvoleného počtu
dnů budou zahrnuta do upozornění o expiraci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="611"/>
        <source>Message time stamp expiration</source>
        <translation>Expirace časových razítek zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="700"/>
        <source>When PIN (master password) is set then the application cannot be started without entering a valid PIN value. The PIN is also used to protect the stored data-box passwords. If an attacker gains the access to the application configuration he cannot easily recover the stored passwords as they are encrypted using the PIN value. The PIN does not protect any stored messages.
Note: If you forget the PIN then you&apos;ll loose all stored passwords. Other stored data won&apos;t be affected.</source>
        <translation>Pokud je nastaven PIN (hlavní heslo), tak aplikace nemůže být spuštěna bez zadání platného PINu. PIN je také použit k ochraně uložených hesel jednotlivých schránek. Pokud útočník získá přístup ke konfiguraci aplikace, tak nemůže jednoduše zjistit uložená hesla, protože ta jsou zašifrována pomocí PINu. PIN není použit k ochraně uložených zpráv.
Poznámka: Jestliže zapomenete PIN, pak přijdete o uložená hesla. Ostatní uložená data nebudou ztrátou postižena.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="792"/>
        <source>When data box is opened, select</source>
        <translation>Když je vybrána schránka, označit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="803"/>
        <source>After a data box is selected,
select the newest message.</source>
        <translation>Po vybrání schránky bude zvolena
nejnovější zpráva.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="838"/>
        <source>After a data box is selected,
no particular message will be selected.</source>
        <translation>Po vybrání schránky nebude zvolena
žádná konkrétní zpráva.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="880"/>
        <source>Application Theme</source>
        <translation>Vzhled aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="905"/>
        <source>Application theme:</source>
        <translation>Vzhled aplikace:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="944"/>
        <source>Font Size</source>
        <translation>Velikost písma</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="957"/>
        <source>Font scaling:</source>
        <translation>Škálování písma:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1021"/>
        <source>Note: In order to apply all size changes properly you will need to restart the application.</source>
        <translation>Poznámka: Aby se změna velikosti všude správně promítla, budete muset restartovat aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1036"/>
        <source>Highlighting</source>
        <translation>Zvýrazňování</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1049"/>
        <source>Highlight messages to be deleted from ISDS within</source>
        <translation>Zvýrazňovat zprávy, které budou v ISDS smazány v průběhu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1073"/>
        <source>days using the colour</source>
        <translation>dnů barvou</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1414"/>
        <source>Specifies the format of file names for attachment files
and other exported files. When saving attachment
files, the default value is %&lt;FN&gt; (i.e. saved file name will
match the original file name including its suffix).</source>
        <translation>Určuje formát názvu souboru pro přílohy a jiné exportované
soubory. Výchozí hodnota pro ukládání souborů příloh je %&lt;FN&gt; (tzn.
jméno ukládáného souboru bude odpovídat původnímu jmému
přílohy včetně přípony).</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1439"/>
        <source>Message filename:</source>
        <translation>Jméno souboru zprávy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1476"/>
        <source>Acceptance info filename:</source>
        <translation>Jméno souboru doručenky:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1488"/>
        <source>Attachment filename:</source>
        <translation>Jméno souboru přílohy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1540"/>
        <source>Saves acceptance info for every attachment file separately.
The acceptance info file name has to contain the original
attachment file name (parameter %&lt;FN&gt; is required).</source>
        <translation>Uloží zvlášť doručenku pro každý soubor přílohy.
Název souboru doručenky musí obsahovat původní
název přílohy (je vyžadován parametr %&lt;FN&gt;).</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1545"/>
        <source>Save acceptance info for every attachment file.</source>
        <translation>Uložit doručenku pro každý soubor přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1559"/>
        <source>Format:</source>
        <translation>Formát:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1586"/>
        <source>Note: Use predefined parameters in order to add additional information from message envelope into the exported filenames. The symbol &quot;/&quot; in the format string stands for a subdirectory. Illegal characters in the resulting filenames will be replaced.</source>
        <translation>Poznámka: Definované parametry lze použít ke vkládání dodatečných informací z obálky datové zprávy do názvů exportovaných souborů. Symbol „/“ ve formátovacím řetězci znamená oddělovač adresářů. Neplatné znaky ve výsledných názvech souborů budou nahrazeny.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1606"/>
        <source>Show Parameter List</source>
        <translation>Zobrazit seznam parametrů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1618"/>
        <source>Close Parameter List</source>
        <translation>Skrýt seznam parametrů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1700"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1711"/>
        <source>Data Collection</source>
        <translation>Sběr dat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1722"/>
        <source>We value the privacy of your data. But we would like to gain some basic information about how this application is being deployed and configured so we can further improve it.

Data are sent via an encrypted channel to a server where they are available for the developers. These data serve for the purpose of improving the application. No personal data such as data-box identifiers no login information nor information from data messages are being or will be sent away.

List with descriptions of the collected data:</source>
        <translation>Ceníme si důvěrnosti Vašich dat. Rádi bychom však získali alespoň základní údaje o nasazení a konfiguraci, abychom jsme aplikaci mohli dále zlepšovat.

Data jsou odesílána šifrovaným kanálem na server, kde jsou dostupná vývojářům. Tato data slouží pro potřeby zlepšování kvality aplikace. V žádném případě se neodesílají osobní údaje, identifikátory schránek, přihlašovací údaje či informace z datových zpráv.

Seznam sbíraných základních údajů:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1743"/>
        <source>Information like the name of the operating system, processor architecture or version of Qt libraries are reported.

We need this information because it will helps us to identify dominant environments which we can focus on.

(e.g. OS Windows 10 64-bit, CPU x86_64, Qt 6.6.2 or OS Linux Mint 64-bit, CPU x86_64, Qt 5.15.2)</source>
        <translation>Jsou hlášeny informace jako je název operačního systému, architektura procesoru nebo verze knihoven Qt.

Tyto informace potřebujeme, protože nám pomůžou identifikovat dominantní prostředí, na které se můžeme zaměřit.

(např. OS Windows 10 64-bit, CPU x86_64, Qt 6.6.2 nebo OS Linux Mint 64-bit, CPU x86_64, Qt 5.15.2)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1750"/>
        <source>- information about the application build</source>
        <translation>- informace o sestavení aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1760"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1791"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1835"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1776"/>
        <source>Application version and available functionality are reported.

We need this information because it shows us how package maintainers configure the application.</source>
        <translation>Jsou hlášeny informace jako je verze aplikace nebo v aplikaci dostupná funkcionalita.

Tyto informace potřebujeme, protože nám ukazují, jak správci balíků konfigurují tuto aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1781"/>
        <source>- information how the application was configured when it was built</source>
        <translation>- informace o konfiguraci aplikace, když byla sestavována</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1805"/>
        <source>It is further possible to let us collect additional information about the configuration and the usage of this application.
We ask you politely to let us receive this additional information.</source>
        <translation>Dále je možné povolit nám sbírat dodatečné informace o nastavení a používání této aplikace.
Zdvořile Vás proto žádáme, abyste nám tyto informace poskytli.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1820"/>
        <source>We ask you kindly to provide the information about:
- the number of data boxes
- an estimate of how much data the application stores
- whether tags are being used
- whether PIN is being used
- whether records management functionality is being used</source>
        <translation>Prosíme Vás, abyste poskytli tyto informace:
- počet schránek v aplikaci
- odhad, kolik dat aplikace ukládá
- zda jsou používány tagy
- zda je používán PIN
- zda jsou používány funkce pro externí spisovou službu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1828"/>
        <source>Collect information about the usage of the application.</source>
        <translation>Sbírat informace o používání této aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="540"/>
        <source>Validity of the signing certificate will be checked against
the current date. This provides the highest protection against
compromised certificates.</source>
        <translation>Platnost podepisujícího certifikátu je kontrolována proti aktuálnímu
datu. Toto nastavení poskytuje maximální ochranu proti
kompromitovaným certifikátům.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="560"/>
        <source>The validity of the signing certificate is checked against
the message download date. This allows a long-term storing
of messages without generating false alarms about invalid
signatures. It is less safe, because an attacker could modify
download dates in the application databases.</source>
        <translation>Platnost podepisujícího certifikátu je kontrolována proti času
stažení zprávy. Toto nastavení umožňuje dlouhodobé ukládání
zpráv bez falešných hlášení o neplatném podpisu. Tato možnost
je méně bezpečná, protože útočník by mohl upravit časy stažení
zpráv v databázi aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="622"/>
        <source>Check for time stamps expiring within</source>
        <translation>Zjišťovat časová razítka expirující do</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="634"/>
        <source>Allows to set an interval specifying how many days before
a timestamp expiration a message will be included in
signature check results.</source>
        <translation>Umožňuje nastavit časový interval udávající, s jakým předstihem
před vypršením platnosti časového razítka bude zpráva zahrnuta
do výsledku kontroly.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="660"/>
        <source>days</source>
        <translation>dnů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="781"/>
        <source>Navigation</source>
        <translation>Navigace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="807"/>
        <source>Newest message</source>
        <translation>Nejnovější zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="826"/>
        <source>Last displayed message</source>
        <translation>Poslední zobrazenou zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="842"/>
        <source>Nothing</source>
        <translation>Nic</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="869"/>
        <source>Interface</source>
        <translation>Rozhraní</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1152"/>
        <source>Directories</source>
        <translation>Adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1166"/>
        <source>When allowed, the application will use the following
directories as paths for loading and saving files for
all accounts. Per-account path remembering will
be disabled.</source>
        <translation>V případě povolení bude aplikace používat zvolené
adresáře k ukládání a otevírání zpráv pro všechny účty.
Pamatování cest pro jednotlivé účty bude vypnuto.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1172"/>
        <source>Use global path settings</source>
        <translation>Používat nastavené globální cesty</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1189"/>
        <source>Path for attachment saving is currently set to:</source>
        <translation>Cesta pro ukládání příloh je aktuálně nastavena na:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1097"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1219"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1276"/>
        <source>Change</source>
        <translation>Změnit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1246"/>
        <source>Path for adding files as attachments is currently set to:</source>
        <translation>Cesta pro přidávání příloh je aktuálně nastavena na:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1216"/>
        <source>Select a new path for attachment saving.</source>
        <translation>Vyberte novou cestu pro ukládání příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="181"/>
        <source>When allowed, the application will automatically synchronise data boxes
and will also download new messages on background at startup.</source>
        <translation>Pokud povolíte tuto možnost, aplikace bude automaticky synchronizovat
schránky a stahovat nové zprávy na pozadí po spuštění aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="185"/>
        <source>Automatically synchronize all data boxes on startup</source>
        <translation>Automaticky synchronizovat všechny schránky po spuštění aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="688"/>
        <source>PIN settings</source>
        <translation>Nastavení PINu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="718"/>
        <source>Set PIN</source>
        <translation>Nastavit PIN</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="730"/>
        <source>Change PIN</source>
        <translation>Změnit PIN</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="742"/>
        <source>Clear PIN</source>
        <translation>Zrušit PIN</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="822"/>
        <source>After a data box is selected,
select the last displayed message for this data box.</source>
        <translation>Po vybrání schránky bude zvolena
poslední zbrazená zpráva dané schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1273"/>
        <source>Select a new path for adding attachment files.</source>
        <translation>Vyberte novou cestu pro vkládání příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1315"/>
        <source>Saving</source>
        <translation>Ukládání</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1326"/>
        <source>These actions will be performed when saving all message attachments.</source>
        <translation>Tyto akce budou prováděny současně s ukládáním všech příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1329"/>
        <source>When saving all attachments also</source>
        <translation>Při ukládání všech příloh také</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1345"/>
        <source>When saving all attachments also save
signed message as ZFO to the same path.</source>
        <translation>Uložit také podepsanou zprávu jako ZFO
soubor do cesty, kam se ukládají přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1349"/>
        <source>Save signed message to ZFO</source>
        <translation>Ulož podepsanou zprávu jako ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1361"/>
        <source>When saving all attachments also save
message envelope as PDF to the same path.</source>
        <translation>Uložit také obálku zprávy jako PDF
soubor do cesty, kam se ukládají přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1365"/>
        <source>Save message envelope to PDF</source>
        <translation>Ulož obálku zprávy jako PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1377"/>
        <source>When saving all attachments also save
signed acceptance info as ZFO to the same path.</source>
        <translation>Uložit také podepsanou doručenku jako ZFO
soubor do cesty, kam se ukládají přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1381"/>
        <source>Save signed acceptance info to ZFO</source>
        <translation>Ulož podepsanou doručenku jako ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1393"/>
        <source>When saving all attachments also save
acceptance info as PDF to the same path.</source>
        <translation>Uložit také informace o doručení jako PDF
soubor do cesty, kam se ukládají přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1397"/>
        <source>Save acceptance info to PDF</source>
        <translation>Ulož doručenku jako PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1420"/>
        <source>File name format of saved/exported files</source>
        <translation>Formát jména ukládaných/exportovaných souborů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1644"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1655"/>
        <source>Application language</source>
        <translation>Jazyk aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1676"/>
        <source>Note: Language settings will not be applied until you restart the application.</source>
        <translation>Poznámka: Nastavení jazyka nebude změněno dokud nerestartujete aplikaci.</translation>
    </message>
</context>
<context>
    <name>DlgPrefs</name>
    <message>
        <location filename="../src/gui/ui/dlg_prefs.ui" line="14"/>
        <source>All preferences</source>
        <translation>Všechny předvolby</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_prefs.ui" line="37"/>
        <source>Changing these settings directly can be harmful to the stability, security and performance of this application. You should only continue if you are sure of what you are doing.</source>
        <translation>Přímé nastavení těchto hodnot může mít špatný vliv na stabilitu, bezpečnost a výkonnost této aplikace. Pokračujte pouze pokud víte, co děláte.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_prefs.ui" line="65"/>
        <source>I accept the risk.</source>
        <translation>Beru na vědomí.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_prefs.ui" line="106"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_prefs.cpp" line="80"/>
        <source>Enter sought expression</source>
        <translation>Zadete hledaný výraz</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_prefs.cpp" line="188"/>
        <location filename="../src/gui/dlg_prefs.cpp" line="200"/>
        <location filename="../src/gui/dlg_prefs.cpp" line="212"/>
        <location filename="../src/gui/dlg_prefs.cpp" line="224"/>
        <location filename="../src/gui/dlg_prefs.cpp" line="242"/>
        <location filename="../src/gui/dlg_prefs.cpp" line="261"/>
        <source>Enter a Value</source>
        <translation>Zadejte hodnotu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_prefs.cpp" line="231"/>
        <location filename="../src/gui/dlg_prefs.cpp" line="250"/>
        <location filename="../src/gui/dlg_prefs.cpp" line="269"/>
        <source>Value Error</source>
        <translation>Chybná hodnota</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_prefs.cpp" line="232"/>
        <location filename="../src/gui/dlg_prefs.cpp" line="251"/>
        <location filename="../src/gui/dlg_prefs.cpp" line="270"/>
        <source>Entered invalid value.</source>
        <translation>Zadaná neplatná hodnota.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_prefs.cpp" line="295"/>
        <source>Reset</source>
        <translation>Resetovat</translation>
    </message>
</context>
<context>
    <name>DlgProxysets</name>
    <message>
        <location filename="../src/gui/dlg_proxysets.cpp" line="53"/>
        <location filename="../src/gui/dlg_proxysets.cpp" line="74"/>
        <source>Proxy has been detected</source>
        <translation>Proxy byla detekována</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_proxysets.cpp" line="48"/>
        <location filename="../src/gui/dlg_proxysets.cpp" line="69"/>
        <source>No proxy detected, direct connection will be used.</source>
        <translation>Nebyla detekována žádná proxy, použije se přímé spojení.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="32"/>
        <source>Proxy Settings</source>
        <translation>Nastavení proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="41"/>
        <source>The HTTPS protocol is used when accessing the ISDS server while HTTP is used to download the certificate revocation list and information about new application versions.</source>
        <translation>Protokol HTTPS je využíván k přístupu na server ISDS. Protokol HTTP je použit ke stažení seznamu zneplatněných certifikátů a informací o nových verzích aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="60"/>
        <source>HTTPS proxy</source>
        <translation>HTTPS proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="72"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="75"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="286"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="289"/>
        <source>No proxy</source>
        <translation>Bez proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="88"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="91"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="302"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="305"/>
        <source>Automatic proxy detection</source>
        <translation>Automatická detekce proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="121"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="124"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="335"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="338"/>
        <source>Manual proxy settings</source>
        <translation>Ruční nastavení proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="139"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="353"/>
        <source>Proxy hostname:</source>
        <translation>Jméno počítače proxy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="152"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="366"/>
        <source>Enter your proxy hostname</source>
        <translation>Zadejte název vašeho proxy hostitele</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="165"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="379"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="184"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="410"/>
        <source>Enter port</source>
        <translation>Zadejte číslo portu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="206"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="209"/>
        <source>Show HTTPS proxy authentication</source>
        <translation>Zobrazit přihlašovací údaje HTTPS proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="432"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="435"/>
        <source>Show HTTP proxy authentication</source>
        <translation>Zobrazit přihlašovací údaje HTTP proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="225"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="451"/>
        <source>Username:</source>
        <translation>Uživatelské jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="232"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="458"/>
        <source>Enter your username</source>
        <translation>Zadejte své uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="245"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="471"/>
        <source>Password:</source>
        <translation>Heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="252"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="478"/>
        <source>Enter your password</source>
        <translation>Zadejte heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="274"/>
        <source>HTTP proxy</source>
        <translation>HTTP proxy</translation>
    </message>
</context>
<context>
    <name>DlgRecordsManagement</name>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="14"/>
        <source>Records Management Settings</source>
        <translation>Nastavení spisové služby</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="28"/>
        <source>Service location URL.</source>
        <translation>URL určující umístění služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="31"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="41"/>
        <source>Access token into the records management service.</source>
        <translation>Přístupový token do spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="44"/>
        <source>Token:</source>
        <translation>Token:</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="71"/>
        <source>Service Info</source>
        <translation>Info o službě</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="78"/>
        <source>Erase</source>
        <translation>Vymazat</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="100"/>
        <source>Service name:</source>
        <translation>Jméno služby:</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="110"/>
        <source>Token name:</source>
        <translation>Jméno tokenu:</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="131"/>
        <source>When downloading complete messages and the sender or recipient
reference number or file mark of a newly downloaded message
unambiguously matches a place in the records management
hierarchy then this message will be automatically uploaded into
the matching place of the records management service.</source>
        <translation>Pokud při stahování kompletních zpráv jednací číslo nebo spisová
značka odesílatele nebo adresáta u nově stažené zprávy jednoznačně
odpovídá místu v hierarchii spisové služby, pak bude tato zpráva
automaticky nahrána do odpovídajícího místa ve spisové službě.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management.ui" line="138"/>
        <source>Upload new messages to locations with matching identifiers.</source>
        <translation>Nahrávat nové zprávy do umístění s odpovídajícími identifikátory.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management.cpp" line="209"/>
        <location filename="../src/records_management/gui/dlg_records_management.cpp" line="217"/>
        <location filename="../src/records_management/gui/dlg_records_management.cpp" line="244"/>
        <source>Communication Error</source>
        <translation>Chyba komunikace</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management.cpp" line="210"/>
        <source>Received invalid response.</source>
        <translation>Obdržena neplatná odezva.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management.cpp" line="218"/>
        <source>Received empty response.</source>
        <translation>Obdržena prázdná odezva.</translation>
    </message>
</context>
<context>
    <name>DlgRecordsManagementCheckMsgs</name>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="14"/>
        <source>Check Messages Against Records Management</source>
        <translation>Ověřit zprávy proti spisové službě</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="20"/>
        <source>Update local information about already uploaded messages
from records management service before checking
whether to upload the messages.</source>
        <translation>Aktualizovat lokální informace o již nahraných zprávách
ze spisové služby před ověřováním,
zda se mají zprávy do služby nahrát.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="25"/>
        <source>Update list of uploaded messages from the service.</source>
        <translation>Aktualizovat seznam nahraných zpráv ze služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="40"/>
        <source>Select all</source>
        <translation>Vybrat vše</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="60"/>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="239"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="107"/>
        <source>From date:</source>
        <translation>Od data:</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="114"/>
        <source>Check only messages with the specified delivery date and later.</source>
        <translation>Kontrolovat pouze zprávy s určeným datem dodání a pozdějším.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="128"/>
        <source>To date:</source>
        <translation>Do data:</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="135"/>
        <source>Check only messages with the specified delivery date and earlier.</source>
        <translation>Kontrolovat pouze zprávy s určeným datem dodání a dřívějším.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="159"/>
        <source>Without time costraints.</source>
        <translation>Bez časových omezení.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="32"/>
        <source>Select data boxes to be checked.</source>
        <translation>Vyberte schránky pro ověření.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="155"/>
        <source>If checked then all messages in selected data boxes are going to be checked.
No delivery time constraints are going to be imposed.</source>
        <translation>Pokud je zaškrtnuto, tak budou ve vybraných schránkách ověřovány všechny zprávy.
Nebudou uplatněna žádná omezení na čas dodání.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="184"/>
        <source>Generates a list of all messages from the selected data boxes
which have not been uploaded into the records management service.</source>
        <translation>Z vybraných schránek vygeneruje seznam všech zpráv,
které nebyly nahrány do spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="188"/>
        <source>Check Missing Messages</source>
        <translation>Ověřit chybějící zprávy</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="207"/>
        <source>List of messages not present in the records management service</source>
        <translation>Seznam zpráv, které se nenacházejí ve spisové službě</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="215"/>
        <source>Try automatically downloading missing attachments
before uploading into records management service.</source>
        <translation>Pokusí se stáhnout chybějící přílohy
před nahráváním do spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="219"/>
        <source>Download missing attachments.</source>
        <translation>Stahovat chybějící přílohy.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="275"/>
        <source>Export displayed message list into a CSV file.</source>
        <translation>Exportuje data ze zobrazeného seznamu zpráv do CSV souboru.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="278"/>
        <source>Export CSV</source>
        <translation>Exportovat CSV</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="298"/>
        <source>Tries finding a match between available message identifiers
(e.g. file marks, reference numbers) and data found
in the records management hierarchy.

If there is a unique match then an automatic message upload
into the records management service is performed.</source>
        <translation>Pokusí se najít shodu mezi dostupnými identifikátory zprávy
(např. spisovými značkami, jednacími čísly) a daty nalezenými
v hierarchii spisové služby.

Pokud je nalezena jednoznačná shoda, tak je zpráva automaticky
nahrána do spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_check_msgs.ui" line="306"/>
        <source>Try Automatic Upload</source>
        <translation>Zkusit automatické nahrání</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="214"/>
        <source>Upload info</source>
        <translation>Info o nahrání</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="261"/>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="310"/>
        <source>Enter sought expression</source>
        <translation>Zadete hledaný výraz</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="801"/>
        <source>Cancel Entire Operation?</source>
        <translation>Zrušit celou operaci?</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="802"/>
        <source>Previous message upload has been cancelled. Should the remaining uploads be also cancelled?</source>
        <translation>Nahrávání předchozí zprávy bylo zrušeno. Má být zbývající nahrávání zpráv také zrušeno?</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="823"/>
        <source>Missing Message Content</source>
        <translation>Chybí obsah zprávy</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="824"/>
        <source>Complete message &apos;%1&apos; is missing.</source>
        <translation>Chybí úplná zpráva &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="825"/>
        <source>First you must download the complete message to continue with the action.</source>
        <translation>Před pokračováním akce musíte nejdříve stáhnout úplnou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="827"/>
        <source>Do you want to download the complete message now?</source>
        <translation>Chcete nyní stáhnout úplnou zprávu?</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="891"/>
        <source>Collecting messages from records management.</source>
        <translation>Sbírají se data ze spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="972"/>
        <source>No hierarchy match.</source>
        <translation>Bez shody v hierarchii.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="980"/>
        <source>Multiple possible targets have been detected.</source>
        <translation>Nalezeno více možných cílů.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1031"/>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1070"/>
        <source>No message data.</source>
        <translation>Chybí obsah zprávy.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1145"/>
        <source>Messages</source>
        <translation>Zprávy</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1156"/>
        <source>Select CSV File</source>
        <translation>Vybrat CSV soubor</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1157"/>
        <source>Files</source>
        <translation>Soubory</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1171"/>
        <source>Cannot Open File</source>
        <translation>Nelze otevřít soubor</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1172"/>
        <source>Cannot open file &apos;%1&apos;.</source>
        <translation>Nemohu otevřít soubor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1179"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1181"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1182"/>
        <source>Message Type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1183"/>
        <source>Delivery Time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1184"/>
        <source>Acceptance Time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1185"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1186"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1187"/>
        <source>Sender Address</source>
        <translation>Adresa odesílatele</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1188"/>
        <source>Recipient</source>
        <translation>Adresát</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1189"/>
        <source>Recipient Address</source>
        <translation>Adresa adresáta</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1190"/>
        <source>Sender File Mark</source>
        <translation>Spisová značka odesílatele</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1191"/>
        <source>Sender Reference Number</source>
        <translation>Jednací číslo odesílatele</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1192"/>
        <source>Recipient File Mark</source>
        <translation>Spisová značka adresáta</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1193"/>
        <source>Recipient Reference Number</source>
        <translation>Jednací číslo adresáta</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1194"/>
        <source>Upload Info</source>
        <translation>Info o nahrání</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1245"/>
        <source>Download Signed Message</source>
        <translation>Stáhnout podepsanou zprávu</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1246"/>
        <source>Download the complete message, i.e. including attachments, and verify its signature.</source>
        <translation>Stáhnout kompletní zprávu, tj. včetně příloh, a ověřit její podpis.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1254"/>
        <source>Send to Records Management</source>
        <translation>Odeslat do spisové služby</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1255"/>
        <source>Send message to records management service.</source>
        <translation>Odeslat zprávu do spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1317"/>
        <source>Collecting messages for data box &apos;%1&apos;.</source>
        <translation>Sbírají se data pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1352"/>
        <source>There are no messages to be uploaded.</source>
        <translation>Nebyly nalezeny zprávy k nahrání.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_check_msgs.cpp" line="1358"/>
        <source>Number of missing messages in the records management: %1</source>
        <translation>Počet zpráv chybějících ve spisové službě: %1</translation>
    </message>
</context>
<context>
    <name>DlgRecordsManagementStored</name>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_stored.cpp" line="60"/>
        <source>Records Management Stored Messages</source>
        <translation>Zprávy uložené ve spisové službě</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_stored.cpp" line="132"/>
        <source>Updating stored information about messages.</source>
        <translation>Aktualizuji uložené informace o zprávách.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_stored.cpp" line="172"/>
        <source>Downloading information about messages from data box:
%1 (%2).</source>
        <translation>Stahuji informace o zprávách ze schránky:
%1 (%2).</translation>
    </message>
</context>
<context>
    <name>DlgRecordsManagementUpload</name>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_upload.ui" line="14"/>
        <source>Upload Message into Records Management Service</source>
        <translation>Nahrát zprávu do spisové služby</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_upload.ui" line="33"/>
        <source>Appeal</source>
        <translation>Výzva</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_upload.ui" line="64"/>
        <source>Reload</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location filename="../src/records_management/ui/dlg_records_management_upload.ui" line="71"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="74"/>
        <source>Select the location where you want
to upload the message &apos;%1&apos; into.</source>
        <translation>Vyberte umístění, kam chcete nahrát
zprávu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="195"/>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="203"/>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="313"/>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="501"/>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="510"/>
        <source>Communication Error</source>
        <translation>Chyba komunikace</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="196"/>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="502"/>
        <source>Received invalid response.</source>
        <translation>Obdržena neplatná odezva.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="204"/>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="511"/>
        <source>Received empty response.</source>
        <translation>Obdržena prázdná odezva.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="397"/>
        <source>Unknown Hierarchy Identifier</source>
        <translation>Neznámý identifikátor v hierarchii</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="398"/>
        <source>Some of the identifiers &apos;%1&apos; do not exist in the hierarchy.</source>
        <translation>Některé z identifikátorů &quot;%1&quot; neexistují v hierarchii.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="428"/>
        <source>Message &apos;%1&apos; could not be uploaded.</source>
        <translation>Zpráva &apos;%1&apos; nemohla být nahrána.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="431"/>
        <source>Received error</source>
        <translation>Obdržená chyba</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="437"/>
        <source>File Upload Error</source>
        <translation>Chyba při nahrávání souboru</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="445"/>
        <source>Successful File Upload</source>
        <translation>Úspěšné nahrání souboru</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="446"/>
        <source>Message &apos;%1&apos; was successfully uploaded into the records management service.</source>
        <translation>Zpráva &apos;%1&apos; byla úspěšně nahrána do spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload.cpp" line="448"/>
        <source>It can be now found in the records management service in these locations:</source>
        <translation>Ve spisové službě ji můžete nyní nalézt v těchto místech:</translation>
    </message>
</context>
<context>
    <name>DlgRecordsManagementUploadProgress</name>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload_progress.cpp" line="51"/>
        <source>Records Management Upload Progress</source>
        <translation>Průběh nahrávání do spisové služby</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload_progress.cpp" line="54"/>
        <source>Sending message &apos;%1&apos; into records management service.</source>
        <translation>Odesílání zprávy &apos;%1&apos; do spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload_progress.cpp" line="111"/>
        <source>Communication timeout</source>
        <translation>Vypršel čas na komunikaci</translation>
    </message>
    <message>
        <location filename="../src/records_management/gui/dlg_records_management_upload_progress.cpp" line="112"/>
        <source>No data have been transferred during the most recent time period. There may be a problem with the data transfer. Do you wan to abort the transfer?</source>
        <translation>Během posledního časového intervalu nebyla přenesena žádná data. Je možné, že nastal problém s přenosem. Chcete přenos ukončit?</translation>
    </message>
</context>
<context>
    <name>DlgRestore</name>
    <message>
        <location filename="../src/gui/ui/dlg_restore.ui" line="14"/>
        <source>Restore Data</source>
        <translation>Obnovit data</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_restore.ui" line="20"/>
        <location filename="../src/gui/ui/dlg_restore.ui" line="61"/>
        <location filename="../src/gui/ui/dlg_restore.ui" line="111"/>
        <location filename="../src/gui/ui/dlg_restore.ui" line="123"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_restore.ui" line="34"/>
        <source>Where do you want to load the back-up from?</source>
        <translation>Odkud chcete načíst zálohovaná data?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_restore.ui" line="42"/>
        <source>JSON file</source>
        <translation>JSON soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_restore.ui" line="52"/>
        <source>Choose</source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_restore.ui" line="71"/>
        <source>What data do you want to restore?</source>
        <translation>Jaká data si přejete obnovit?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_restore.ui" line="104"/>
        <source>Tag database</source>
        <translation>Databáze tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="167"/>
        <source>Cannot Open File</source>
        <translation>Nelze otevřít soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="168"/>
        <source>File &apos;%1&apos; cannot be opened.</source>
        <translation>Soubor &quot;%1&quot; nelze otevřít.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="179"/>
        <source>Cannot Read Content</source>
        <translation>Nelze načíst obsah</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="180"/>
        <source>File &apos;%1&apos; seems not to contain a valid back-up description.</source>
        <translation>Zdá se, že soubor &quot;%1&quot; neobsahuje platný popis zálohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="191"/>
        <source>Open JSON File</source>
        <translation>Otevřít JSON soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="191"/>
        <source>JSON File (*.json)</source>
        <translation>Soubor JSON (*.json)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="199"/>
        <source>Not a Readable File</source>
        <translation>Nečitelný soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="200"/>
        <source>File &apos;%1&apos; cannot be read.</source>
        <translation>Soubor &quot;%1&quot; nelze číst.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="214"/>
        <source>Backup was taken at %1.</source>
        <translation>Záloha byla provedena %1.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="289"/>
        <source>Restore Message Database?</source>
        <translation>Obnovit databázi zpráv?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="290"/>
        <source>The data box &apos;%1&apos; with the corresponding username already exists. The action will delete all existing message data for this data box and replace them with data from the back-up. Do you wish to restore the message database using the data from the back-up?</source>
        <translation>Schránka &quot;%1&quot; s odpovídajícím uživatelským jménem již existuje. Operace smaže data všech uložených zpráv pro tuto schránku a nahradí je daty ze zálohy. Přejete si obnovit databázi zpráv z dat ze zálohy?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="305"/>
        <source>Create New Data Box?</source>
        <translation>Přidat novou schránku?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="306"/>
        <source>There is no data box corresponding to the username. The action will create an additional data box and fill it with data from the back-up. Do you wish to restore the message database using the data from the back-up?</source>
        <translation>Neexistuje schránka odpovídající danému uživatelskému jménu. Operace vytvoří další schránku a naplní ji daty ze zálohy. Přejete si obnovit databázi zpráv z dat ze zálohy?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="643"/>
        <source>Restored data box</source>
        <translation>Obnovená schránka</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="341"/>
        <source>Restore Tag Database?</source>
        <translation>Obnovit databázi tagů?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="342"/>
        <source>The action will delete all existing tag data and replace them with data from the backup. Do you wish to restore the tag database using the data from the back-up?</source>
        <translation>Operace smaže data všech tagů a nahradí je daty ze zálohy. Přejete si obnovit databázi tagů z dat ze zálohy?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="372"/>
        <source>Proceed?</source>
        <translation>Pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="373"/>
        <source>The action will close the application and will continue with the restoration of selected data. According to the selection some currently available data may be deleted. Do you wish to restore the selected data from the back-up?</source>
        <translation>Operace ukončí aplikaci a bude pokračovat obnovením zvolených dat. V závislosti na volbě mohou některá momentálně dosupná data být smazána. Přejete si obnovit zvolená data ze zálohy?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="386"/>
        <source>Selected data will be restored from the specified location.</source>
        <translation>Zvolená data budou obnovena z určeného umístění.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="388"/>
        <source>Data in the application will be replaced by the data from the back-up.</source>
        <translation>Data v aplikaci budou nahrazena daty ze zálohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore.cpp" line="547"/>
        <source>Not enough space on volume %1.</source>
        <translation>Na svazku %1 není dostatek místa.</translation>
    </message>
</context>
<context>
    <name>DlgRestoreProgress</name>
    <message>
        <location filename="../src/gui/dlg_restore_progress.cpp" line="113"/>
        <source>Restoring %1</source>
        <translation>Obnovuji %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_progress.cpp" line="124"/>
        <source>Cancel Restoration?</source>
        <translation>Zrušit obnovování?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_progress.cpp" line="125"/>
        <source>Cancelling the restoration task may lead to data loss in the application. Do you wish to cancel the restoration task?</source>
        <translation>Zrušení obnovovací úlohy může vést ke zrátě dat v aplikaci. Přejete si zrušit obnovovací úlohu?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_progress.cpp" line="131"/>
        <source>Cancelling current and skipping all further tasks.</source>
        <translation>Ruším současné a přeskakuji následující úlohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_progress.cpp" line="143"/>
        <source>Restoration Finished</source>
        <translation>Obnovování dokončeno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_progress.cpp" line="144"/>
        <source>Restoration task finished without any errors.</source>
        <translation>Obnovování dokončeno bez chyb.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_progress.cpp" line="157"/>
        <source>Restoration Progress</source>
        <translation>Průběh obnovování</translation>
    </message>
</context>
<context>
    <name>DlgSendMessage</name>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="22"/>
        <source>Create and send message</source>
        <translation>Vytvořit a poslat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="36"/>
        <source>Please fill in at least the &lt;b&gt;subject&lt;/b&gt;, &lt;b&gt;one recipient&lt;/b&gt; and &lt;b&gt;one attachment&lt;/b&gt;:</source>
        <translation>Prosím, vyplňte ve formuláři &lt;b&gt;předmět&lt;/b&gt;, &lt;b&gt;alespoň jednoho adresáta&lt;/b&gt; a &lt;b&gt;alespoň jednu přílohu&lt;/b&gt;:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="93"/>
        <source>Sender:</source>
        <translation>Odesílatel:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="146"/>
        <source>Subject:</source>
        <translation>Předmět zprávy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="161"/>
        <source>Enter subject of the message</source>
        <translation>Zadejte smysluplný předmět Vaší zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="622"/>
        <source>Law:</source>
        <translation>Zákon:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="634"/>
        <source>Number of law</source>
        <translation>Číslo zákona</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="644"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="651"/>
        <source>Year of law</source>
        <translation>Rok vydání zákona</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="661"/>
        <source>§</source>
        <translation>§</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="668"/>
        <source>Section of law</source>
        <translation>Paragraf zákona</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="709"/>
        <source>Paragraph of the corresponding section of the law</source>
        <translation>Odstavec odpovídajícího paragrafu zákona</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="366"/>
        <source>Our reference number:</source>
        <translation>Naše číslo jednací:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="242"/>
        <source>Find and add a recipient from ISDS server</source>
        <translation>Najít a přidat adresáta ze serveru ISDS</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="263"/>
        <source>Allows to enter a data box identifier manually</source>
        <translation>Umožňuje ruční vložení identifikátoru datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="554"/>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="582"/>
        <source>Enter reference number</source>
        <translation>Zadejte jednací číslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="568"/>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="596"/>
        <source>Enter file mark</source>
        <translation>Zadejte spisovou značku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="547"/>
        <source>Sender (our) ref. num.:</source>
        <translation>Jednací číslo odesílatele (naše):</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="527"/>
        <source>Optional</source>
        <translation>Nepovinné</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="608"/>
        <source>Mandate</source>
        <translation>Zmocnění</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="533"/>
        <source>Reference Numbers and File Marks</source>
        <translation>Jednací čísla a spisové značky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="561"/>
        <source>Sender (our) file mark:</source>
        <translation>Spisová značka odesílatele (naše):</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="575"/>
        <source>Recipient (your) ref. num.:</source>
        <translation>Jednací číslo adresáta (vaše):</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="589"/>
        <source>Recipient (your) file mark:</source>
        <translation>Spisová značka adresáta (vaše):</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="700"/>
        <source>Point:</source>
        <translation>Písmeno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="736"/>
        <source>Point of the paragraph</source>
        <translation>Písmeno odstavce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="766"/>
        <source>Delivery</source>
        <translation>Dodání</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="780"/>
        <source>To hands:</source>
        <translation>K rukám:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="789"/>
        <source>Enter name of person</source>
        <translation>Zadejte jméno osoby</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="796"/>
        <source>Personal delivery:</source>
        <translation>Do vlastních rukou:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="803"/>
        <source>Enable personal delivery</source>
        <translation>Povolit doručení do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="194"/>
        <source>Recipients:</source>
        <translation>Adresáti:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="74"/>
        <source>Basic</source>
        <translation>Základní</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="124"/>
        <source>Data Box:</source>
        <translation>Datová schránka:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="201"/>
        <source>Add recipient from contacts selected from existing messages</source>
        <translation>Přidat adresáta z kontaktů existujících zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="204"/>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="366"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="218"/>
        <source>Remove selected recipients from the list</source>
        <translation>Odstranit vybrané adresáty ze seznamu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="221"/>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="383"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="245"/>
        <source>Find</source>
        <translation>Najít</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="266"/>
        <source>Enter Box ID</source>
        <translation>Vložit ID schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="328"/>
        <source>Even if the recipient did not read this message,
the message is considered to be accepted after
(currently) 10 days. This is acceptance through fiction.</source>
        <translation>I když si adresát nepřečetl zprávu,
bude zpráva považována za doručenou
(aktuálně) po 10 dnech. Toto je doručení fikcí.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="333"/>
        <source>Allow acceptance through fiction</source>
        <translation>Povolit doručení fikcí</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="321"/>
        <source>Include sender identification</source>
        <translation>Přidat identifikaci odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="356"/>
        <source>Attachments:</source>
        <translation>Přílohy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="363"/>
        <source>Add a new file to the attachments</source>
        <translation>Přidat nový soubor do příloh</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="380"/>
        <source>Remove the selected file from attachments</source>
        <translation>Odstranit vybraný soubor z příloh</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="404"/>
        <source>Open selected file in associated application</source>
        <translation>Otevřít vybraný soubor v externí aplikaci</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="407"/>
        <source>Open</source>
        <translation>Otevřít</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="428"/>
        <source>Add Text</source>
        <translation>Přidat text</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="494"/>
        <source>The complete sent message is going to be downloaded together with
all necessary digital signatures immediately after it has been successfully
accepted by the ISDS system.

Downloading the content of the sent message won&apos;t cause the acceptance
of any delivered message.</source>
        <translation>Úplná odeslaná zpráva bude stažena společně se všemi nezbytnými
digitálními podpisy ihned potom, co bude úspěšně přijata systémem
ISDS.

Stažení obsahu odeslané zprávy nezpůsobí doručení žádné dodané
zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="502"/>
        <source>Immediately download sent message content</source>
        <translation>Ihned stáhnout obsah odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="509"/>
        <source>Before uploading the newly sent message into the records management service
the application needs to download the newly sent message from
the ISDS server. This is because the application needs to acquire all
necessary digital signatures.</source>
        <translation>Před nahráním nově odeslané zprávy do spisové služby potřebuje aplikace
stáhnout nově odeslanou zprávu ze serveru ISDS, protože aplikace potřebuje
získat všechny nezbytné digitální podpisy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="515"/>
        <source>Immediately upload sent message into records management service</source>
        <translation>Ihned nahrát odeslanou zprávu do spisové služby</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="481"/>
        <source>Attachment size is larger than 50 MB. Message cannot be sent.</source>
        <translation>Velikost příloh je větší než 50 MB. Zprávu nebude možné odeslat.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="51"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="318"/>
        <source>Provide additional information about the sender of the message.</source>
        <translation>Poskytnout dodatečné informace o odesílateli zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="425"/>
        <source>Add a file containing a specified text to the attachments.</source>
        <translation>Přidat soubor obsahující zadaný text do příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="693"/>
        <source>Paragraph:</source>
        <translation>Odstavec:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="834"/>
        <source>Author Info</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="840"/>
        <source>You may select which additional data you want to provide to the recipient.</source>
        <translation>Můžete určit, které dodatečné informace chcete poskytnout adresátu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="843"/>
        <source>Select author identification data to be provided to the recipient.</source>
        <translation>Vyberte identifikační údaje o autorovi, které se poskytnou adresátu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="849"/>
        <source>Provide the information whether the sender&apos;s personal data
are identified within the ROB and therefore being updated.</source>
        <translation>Poskytnout informaci, zda osobní údaje o odesílateli
jsou ztotožněny s ROB a jsou tudíž aktualizované.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="853"/>
        <source>ROB identification flag</source>
        <translation>příznak ztotožnění s ROB</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="860"/>
        <source>Full (personal) name of the sender.</source>
        <translation>Celé (osobní) jméno odesílatele.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="863"/>
        <source>full name</source>
        <translation>plné jméno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="873"/>
        <source>Date of birth of the sender.</source>
        <translation>Datum narození odesílatele.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="876"/>
        <source>date of birth</source>
        <translation>datum narození</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="883"/>
        <source>City of birth of the sender.</source>
        <translation>Jméno obce, kde se odesílatel narodil.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="886"/>
        <source>city of birth</source>
        <translation>místo narození</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="893"/>
        <source>County where the city of birth lies in or
name of state if birth place lies outside of the Czech republic.</source>
        <translation>Okres, kde se odesílatel narodil,
nebo stát, pokud se narodil mimo Českou republiku.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="897"/>
        <source>county of birth</source>
        <translation>okres narození</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="904"/>
        <source>Full residential address.</source>
        <translation>Plná adresa trvalého bydliště.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="907"/>
        <source>full address</source>
        <translation>adresa pobytu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="914"/>
        <source>RUIAN address code</source>
        <translation>adresní kód RUIAN</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="938"/>
        <source>Text Message</source>
        <translation>Textová zpráva</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="944"/>
        <source>Here you can create a short text message and add it to attachments as a PDF file.</source>
        <translation>Zde můžete napsat krátkou textovou zprávu a vložit ji do příloh jako PDF soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="957"/>
        <source>Add the message to attachments after you have finished editing the text.</source>
        <translation>Přidejte zprávu do příloh poté, co jste ukončili editaci textu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="969"/>
        <source>Text format:</source>
        <translation>Formát textu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="999"/>
        <source>Shows a preview of the resulting document.</source>
        <translation>Zobrazí náhled výsledného dokumentu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="1002"/>
        <source>Preview</source>
        <translation>Náhled</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="1009"/>
        <source>Generates a PDF and views it in a default application.</source>
        <translation>Vygeneruje PDF a zobrazí jej ve výchozí aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="1012"/>
        <source>View as PDF</source>
        <translation>Zobrazit PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="1019"/>
        <source>Generates a PDF file and adds it to message attachments.</source>
        <translation>Vygeneruje PDF a přidá jej k přílohám zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="1022"/>
        <source>Append PDF</source>
        <translation>Přiložit PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="1053"/>
        <source>Send message</source>
        <translation>Odeslat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="1056"/>
        <source>Send</source>
        <translation>Odeslat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="1063"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="371"/>
        <source>Enter reference number:</source>
        <translation>Zadejte Vaše referenční číslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2117"/>
        <source>Message contains non-OVM recipients.</source>
        <translation>Zpráva obsahuje více ne-OVM adresátů.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="281"/>
        <source>Data box ID</source>
        <translation>ID datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="282"/>
        <source>Enter data box ID (7 characters):</source>
        <translation>Vložte ID datové schránky (7 znaků):</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1634"/>
        <source>Wrong data box ID</source>
        <translation>Špatné ID datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1635"/>
        <source>Wrong data box ID &apos;%1&apos;!</source>
        <translation>Špatné ID datové schránky &apos;%1&apos;!</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1474"/>
        <source>Empty file</source>
        <translation>Prázdný soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1475"/>
        <source>Cannot add empty file &apos;%1&apos; to attachments.</source>
        <translation>Nelze vložit prázdný soubor &quot;%1&quot; do příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="622"/>
        <source>Message was successfully sent to &lt;i&gt;%1 (%2)&lt;/i&gt; as PDZ with number &lt;i&gt;%3&lt;/i&gt;.</source>
        <translation>Zpráva byla úspěšně odeslána do &lt;i&gt;%1 (%2)&lt;/i&gt; jako PDZ s číslem &lt;i&gt;%3&lt;/i&gt;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="542"/>
        <source>Retain Recipients?</source>
        <translation>Ponechat adresáty?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="543"/>
        <source>The sender data box has been changed to &apos;%1&apos;. It&apos;s likely that it won&apos;t be possible to send messages to some already filled-in recipients from the newly selected data box.</source>
        <translation>Datová schránka odesílatele byla změněna na &quot;%1&quot;. Je pravděpodobné, že některým vyplněným adresátům nebude možné odeslat zprávy z nově vybrané datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="544"/>
        <source>Do you want to keep all previously filled-in recipients in the recipient list?</source>
        <translation>Chcete ponechat všechny dříve vyplněné adresáty v seznamu adresátů?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="630"/>
        <source>Message was successfully sent to &lt;i&gt;%1 (%2)&lt;/i&gt; as message number &lt;i&gt;%3&lt;/i&gt;.</source>
        <translation>Zpráva byla úspěšně odeslána do &lt;i&gt;%1 (%2)&lt;/i&gt; jako zpráva s číslem &lt;i&gt;%3&lt;/i&gt;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="639"/>
        <source>Message was NOT sent to &lt;i&gt;%1 (%2)&lt;/i&gt;. Server says: %3</source>
        <translation>Zpráva nebyla odeslána do &lt;i&gt;%1 (%2)&lt;/i&gt;. Server vrací: %3</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="651"/>
        <source>Message was successfully sent to all recipients.</source>
        <translation>Zpráva byla úspěšně odeslána všem adresátům.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="659"/>
        <source>Message was NOT sent to all recipients.</source>
        <translation>Zpráva nebyla odeslána všem adresátům.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="696"/>
        <source>The source text of the text message has been modified since it has been appended to the attachments as a PDF file.</source>
        <translation>Zdrojový text textové zprávy byl změněn poté, co byl přidán do příloh jako PDF soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="698"/>
        <source>Append the text message using the &apos;%1&apos; button on the &apos;%2&apos; tab in order to apply all modifications to the attachment.</source>
        <translation>Přiložte textovou zprávu pomocí tlačítka &quot;%1&quot; na záložce &quot;%2&quot;, aby se změny promítly do přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="703"/>
        <source>The source text of the text message has been edited but it isn&apos;t attached as a PDF file.</source>
        <translation>Zdrojový text textové zprávy byl editován, ale není přiložen jako PDF soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="705"/>
        <source>Append the text message using the &apos;%1&apos; button on the &apos;%2&apos; tab after you&apos;ve finished editing.</source>
        <translation>Přiložte textovou zprávu pomocí tlačítka &quot;%1&quot; na záložce &quot;%2&quot; po ukončení editace.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="875"/>
        <source>Attachment &lt;i&gt;%1&lt;/i&gt; was NOT uploaded to server. Server says: %2</source>
        <translation>Příloha &lt;i&gt;%1&lt;/i&gt; nebyla odeslána na server. Server vrací: %2</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="886"/>
        <source>Attachment uploading error</source>
        <translation>Chyba odesílání přílohy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="887"/>
        <source>Attachments couldn&apos;t be uploaded.</source>
        <translation>Přílohy nemohly být odeslány.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1102"/>
        <source>Plain text</source>
        <translation>Prostý text</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1105"/>
        <source>Text will be written to the output as it is.</source>
        <translation>Text bude zapsán na výstup tak, jak je.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1108"/>
        <source>Markdown</source>
        <translation>Markdown</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1111"/>
        <source>You can use Markdown syntax to to specify how the resulting formatted text should look like.
Only a subset of the Markdown language is supported.
Use the preview functionality to check the content of the resulting document.</source>
        <translation>Můžete použít Markdown syntaxi, která dovoluje formátování textu.
Je podporována pouze část jazyka Markdown.
Použijte funkci náhledu, pro ověření obsahu výsledného dokumentu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1126"/>
        <source>Enter a short text message.</source>
        <translation>Zadejte krátkou textovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1363"/>
        <source>Non-Existent Sender</source>
        <translation>Neexistující odesílatel</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1364"/>
        <source>No data box matches the sender identifier &apos;%1&apos;.

Cannot select data box with supplied data-box ID or username.</source>
        <translation>Žádná datová schránka neodpovídá identifikátoru odesílatele &quot;%1&quot;.

Nelze vybrat datovou schránku k zadanému identifikátoru datové schránky nebo přihlašovacímu jménu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1459"/>
        <source>Non-existent file</source>
        <translation>Soubor neexistuje</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1460"/>
        <source>Cannot add non-existent file &apos;%1&apos; to attachments.</source>
        <translation>Nelze přidat neexistující soubor &apos;%1&apos; do příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1464"/>
        <source>File not readable</source>
        <translation>Soubor nelze číst</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1465"/>
        <source>Cannot add file &apos;%1&apos; without readable permissions to attachments.</source>
        <translation>Nelze přidat soubor &apos;%1&apos;, pro který nemáte oprávnění ke čtení, do příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1469"/>
        <source>File already present</source>
        <translation>Soubor již existuje</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1508"/>
        <source>Total size of attachments is %1 B.</source>
        <translation>Celková velikost příloh je %1 B.</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/gui/dlg_send_message.cpp" line="1516"/>
        <source>%n file(s) is/are being attached.</source>
        <translation>
            <numerusform>Je přiložen %n soubor.</numerusform>
            <numerusform>Jsou přiloženy %n soubory.</numerusform>
            <numerusform>Je přiloženo %n souborů.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1557"/>
        <source>Total size of attachments is ~%1 KB.</source>
        <translation>Celková velikost příloh je ~%1 KB.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1560"/>
        <source>Total size of attachments is ~%1 B.</source>
        <translation>Celková velikost příloh je ~%1 B.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1671"/>
        <source>Cannot execute task.</source>
        <translation>Nelze spustit úlohu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1715"/>
        <source>Information about recipient data box &apos;%1&apos; could not be obtained.</source>
        <translation>Nebylo možné obdržet informace o datové schránce adresáta &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1764"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="1774"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="1785"/>
        <source>Unknown message type</source>
        <translation>Neznámý typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1765"/>
        <source>No information about the recipient data box &apos;%1&apos; could be obtained.</source>
        <translation>Nebylo možné obdržet žádné informace o datové schránce adresáta &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1766"/>
        <source>It&apos;s unknown whether public or commercial messages can be sent to this recipient.</source>
        <translation>Není známo, zda tomuto adresátu lze poslat veřejnou či poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1775"/>
        <source>No commercial message to the recipient data box &apos;%1&apos; can be sent.</source>
        <translation>Do datové schránky adresáta &apos;%1&apos; není možné zaslat poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1776"/>
        <source>It&apos;s unknown whether a public messages can be sent to this recipient.</source>
        <translation>Není známo, zda tomuto adresátu lze poslat veřejnou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1786"/>
        <source>No public message to the recipient data box &apos;%1&apos; can be sent.</source>
        <translation>Do datové schránky adresáta &apos;%1&apos; není možné zaslat veřejnou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1787"/>
        <source>It&apos;s unknown whether a commercial messages can be sent to this recipient.</source>
        <translation>Není známo, zda tomuto adresátu lze poslat poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1793"/>
        <source>Cannot send message to data box</source>
        <translation>Zprávu nelze odeslat do datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2074"/>
        <source>Couldn&apos;t create a task to upload the attachment.</source>
        <translation>Nemohla být vytvořena úloha pro odeslání přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2191"/>
        <source>An error occurred while loading external attachments into message.</source>
        <translation>Nastala chyba při nahrávání příloh do zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2245"/>
        <source>The initiatory commercial message for the recipient &apos;%1&apos; does not have a sender reference number filled in.
It is a mandatory information for this type of commercial message. Please provide one.</source>
        <translation>Iniciační poštovní datová zpráva adresátu &quot;%1&quot; nemá vyplněno referenční číslo odesílatele.
Je to vyžadovaná informace pro tento typ poštovních datových zpráv. Prosím vyplňte jej.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2279"/>
        <source>Couldn&apos;t create a task to send the message.</source>
        <translation>Nemohla být vytvořena úloha pro odeslání zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2328"/>
        <source>Cannot send commercial messages from this data box. The sender data box hasn&apos;t got any active payment methods for commercial messages.</source>
        <translation>Z této schránky nelze odesílat poštovní datové zprávy. Datová schránka odesílatele nemá žádné aktivní způsoby platby za poštovní datové zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2330"/>
        <source>PDZ sending: unavailable</source>
        <translation>Odesílání PDZ: nedostupné</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2334"/>
        <source>PDZ sending: active</source>
        <translation>Odesílání PDZ: aktivní</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2336"/>
        <source>The sender data box can send commercial messages.</source>
        <translation>Datová schránka odesílatele může zasílat poštovní datové zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2337"/>
        <source>Available payment methods are:</source>
        <translation>Jsou dostupné tyto způsoby platby:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2345"/>
        <source>The sender data box is subsidised by the data box with ID &apos;%1&apos;.</source>
        <translation>Datová schránka odesílatele je dotována schránkou ID &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2348"/>
        <source>Up to %1 subsidised commercial messages can be sent.</source>
        <translation>Lze odeslat až %1 dotovaných poštovních datových zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2351"/>
        <source>The number of subsidised commercial messages is unlimited.</source>
        <translation>Počet dotovaných datových zpráv není omezen.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2355"/>
        <source>Sending of subsidised commercial messages is not limited in time.</source>
        <translation>Odesílání dotovaných datových zpráv není časově omezeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2358"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="2362"/>
        <source>Initiatory commercial messages can also be sent.</source>
        <translation>Lze také odesílat iniciační poštovní datové zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2360"/>
        <source>Contract with Czech post.</source>
        <translation>Smlouva s Českou poštou.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2365"/>
        <source>Remaining credit: %1 Kč</source>
        <translation>Zbývající kredit: %1 Kč</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2366"/>
        <source>Credit.</source>
        <translation>Kredit.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2369"/>
        <source>The remaining credit is %1 Kč.</source>
        <translation>Zbývající kretit činí %1 Kč.</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/gui/dlg_send_message.cpp" line="2371"/>
        <source>The credit suffices for %n commercial message(s).</source>
        <translation>
            <numerusform>Kredit postačuje na %n poštovní datovou zprávu.</numerusform>
            <numerusform>Kredit postačuje na %n poštovní datové zprávy.</numerusform>
            <numerusform>Kredit postačuje na %n poštovních datových zpráv.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2374"/>
        <source>The credit is too low or it has expired.</source>
        <translation>Kredit je příliš nízký nebo již expiroval.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1707"/>
        <source>Wrong Recipient</source>
        <translation>Chybný adresát</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/gui/dlg_send_message.cpp" line="1520"/>
        <source>The permitted number of %n attachment(s) has been exceeded.</source>
        <translation>
            <numerusform>Povolený počet %n přílohy byl překročen.</numerusform>
            <numerusform>Povolený počet %n příloh byl překročen.</numerusform>
            <numerusform>Povolený počet %n příloh byl překročen.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1714"/>
        <source>Recipient Search Failed</source>
        <translation>Chyba vyhledávání adresáta</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1716"/>
        <source>Do you still want to add the box &apos;%1&apos; into the recipient list?</source>
        <translation>Chcete stále přidat schránku &apos;%1&apos; do seznamu adresátů?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1717"/>
        <source>Obtained ISDS error</source>
        <translation>Obdržená chyba ISDS</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2113"/>
        <source>Your remaining credit is %1 Kč.</source>
        <translation>Váš zbývající kredit je %1 Kč.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2118"/>
        <source>Don&apos;t show this notification again.</source>
        <translation>Znovu nezobrazovat toto upozornění.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2290"/>
        <source>It has not been possible to send a message to the ISDS server.</source>
        <translation>Nebylo možné odeslat zprávu na server ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="650"/>
        <source>Message sent</source>
        <translation>Zpráva odeslána</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="658"/>
        <source>Message sending error</source>
        <translation>Chyba odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="656"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="884"/>
        <source>Do you want to close the Send message form?</source>
        <translation>Chcete zavřít formulář pro odesílání zpráv a zahodit vyplněné údaje?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2289"/>
        <source>Send message error</source>
        <translation>Chyba při odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1470"/>
        <source>Cannot add file &apos;%1&apos; because the file is already present in the attachments.</source>
        <translation>Nelze přidat soubor &apos;%1&apos;, protože tento je již v přílohách obsažen.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1531"/>
        <source>Total size of attachments exceeds %1 MB.</source>
        <translation>Celková velikost příloh přesahuje %1 MB.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1541"/>
        <source>Total size of attachments exceeds %1 MB. Message will be sent as a high-volume message (VoDZ).</source>
        <translation>Celková velikost příloh přesahuje %1 MB. Zpráva bude odeslána jako velkoobjemová zpráva (VoDZ).</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1548"/>
        <source>Total size of attachments exceeds %1 MB. Most of the data boxes cannot receive messages larger than %1 MB. However, some OVM data boxes can receive message up to %2 MB.</source>
        <translation>Celková velikost příloh přesahuje %1 MB. Většina datových schránek nemůže přijímat zprávy větší než %1 MB. Nicméně některé datové schránky typu OVM mohou přijímat zprávy až do %2 MB.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1677"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="1678"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1688"/>
        <source>Data box is not active</source>
        <translation>Datová schránka není aktivní</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1689"/>
        <source>Recipient with data box ID &apos;%1&apos; does not have active data box.</source>
        <translation>Adresát s datovou schránkou &apos;%1&apos; nemá tuto schránku aktivní.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1691"/>
        <source>The message cannot be delivered.</source>
        <translation>Zprávu nelze dodat.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1708"/>
        <source>Recipient with data box ID &apos;%1&apos; does not exist.</source>
        <translation>Adresát s datovou schránkou &apos;%1&apos; neexistuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1794"/>
        <source>No public data message nor a commercial data message (PDZ) can be sent to the recipient data box &apos;%1&apos;.</source>
        <translation>Do datové schránky adresáta &quot;%1&quot; nelze zaslat veřejnou ani poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1795"/>
        <source>Receiving of PDZs has been disabled in the recipient data box or there are no available payment methods for PDZs.</source>
        <translation>Příjem PDZ byl v datové schránce adresáta zakázán nebo nejsou dostupné platební metody pro PDZ.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2101"/>
        <source>Your message contains one or more non-OVM recipients. To these recipients the message will be sent as a commercial data message (PDZ). Number of PDZ recipients is: %1</source>
        <translation>Vaše zpráva obsahuje jednoho nebo více ne-OVM adresátů. Těmto adresátům bude tato zpráva odeslaná v režimu placené poštovní datové zprávy (PDZ). Počet adresátů PDZ je: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2160"/>
        <source>Recipient ID: %1, Payment method: %2</source>
        <translation>ID adresáta: %1, Způsob platby: %2</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2199"/>
        <source>An error occurred while loading attachments into message.</source>
        <translation>Nastala chyba v průběhu vkládání příloh do zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2207"/>
        <source>An error occurred during message envelope creation.</source>
        <translation>Nastala chyba v průběhu vytváření obálky zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2244"/>
        <source>Commercial message send error</source>
        <translation>Chyba odesílání poštovní datové zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2287"/>
        <source>The message will be discarded.</source>
        <translation>Zpráva bude zahozena.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="2119"/>
        <source>Do you want to send all messages?</source>
        <translation>Chcete opravdu odeslat tuto zprávu všem adresátům?</translation>
    </message>
</context>
<context>
    <name>DlgSignatureDetail</name>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="14"/>
        <source>Signature detail for current message</source>
        <translation>Detaily o podpisu zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="26"/>
        <source>Message signature</source>
        <translation>Podpis zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="40"/>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="99"/>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="179"/>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="247"/>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="299"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="85"/>
        <source>Signing certificate</source>
        <translation>Podepisující certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="142"/>
        <source>Show verification detail</source>
        <translation>Zobrazit detail ověření</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="210"/>
        <source>Show certificate detail</source>
        <translation>Zobrazit detail certifikátu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="285"/>
        <source>Timestamp</source>
        <translation>Časové razítko</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="218"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="221"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="224"/>
        <source>Information not available</source>
        <translation>Informace není dostupná</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="233"/>
        <source>Message signature is not present.</source>
        <translation>Podpis zprávy není přítomný.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="248"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="253"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="360"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="600"/>
        <source>Valid</source>
        <translation>Platný</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="375"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="417"/>
        <source>Certificate revocation check is turned off!</source>
        <translation>Kontrola zneplatnění certifikátu je vypnutá!</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="392"/>
        <source>Trusted certificates were found</source>
        <translation>Důvěryhodné certifikáty nalezeny</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="396"/>
        <source>Signing algorithm supported</source>
        <translation>Podepisující algoritmus je podporován</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="401"/>
        <source>Trusted parent certificate found</source>
        <translation>Důvěryhodný rodičovský certifikát nalezen</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="405"/>
        <source>Certificate time validity is ok</source>
        <translation>Časová platnost certifikátu je v pořádku</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="413"/>
        <source>Certificate was not revoked</source>
        <translation>Certifikát nebyl revokován</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="422"/>
        <source>Certificate signature verified</source>
        <translation>Podpis certifikátu ověřen</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="434"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="436"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="499"/>
        <source>Serial number</source>
        <translation>Sériové číslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="441"/>
        <source>Signature algorithm</source>
        <translation>Algoritmus podpisu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="444"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="618"/>
        <source>Issuer</source>
        <translation>Vydal</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="449"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="487"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="620"/>
        <source>Organisation</source>
        <translation>Organizace</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="456"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="493"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="628"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="462"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="505"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="632"/>
        <source>Country</source>
        <translation>Země</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="466"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="638"/>
        <source>Validity</source>
        <translation>Platnost</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="475"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="639"/>
        <source>Valid from</source>
        <translation>Platnost od</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="478"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="642"/>
        <source>Valid to</source>
        <translation>Platnost do</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="483"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="587"/>
        <source>Time stamp not present.</source>
        <translation>Časové razítko není přítomno.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="611"/>
        <source>Time</source>
        <translation>Čas</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="625"/>
        <source>Organisational unit</source>
        <translation>Organizační jednotka</translation>
    </message>
</context>
<context>
    <name>DlgTag</name>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="154"/>
        <source>Choose tag colour</source>
        <translation>Vyberte barvu tagu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="306"/>
        <location filename="../src/gui/dlg_tag.cpp" line="341"/>
        <source>Tag error</source>
        <translation>Chyba tagu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="306"/>
        <source>Tag name is empty.</source>
        <translation>Jméno tagu je prázdné.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="307"/>
        <location filename="../src/gui/dlg_tag.cpp" line="344"/>
        <source>Tag wasn&apos;t created.</source>
        <translation>Tag nebyl vytvořen.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="342"/>
        <source>Cannot write tag with name &apos;%1&apos; to tag storage.</source>
        <translation>Nelze zapsat tag se jmémen &quot;%1&quot; do úložiště tagů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag.ui" line="14"/>
        <source>Tag properties</source>
        <translation>Vlastnosti tagu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag.ui" line="22"/>
        <source>Tag name:</source>
        <translation>Jméno tagu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag.ui" line="32"/>
        <source>Tag color:</source>
        <translation>Barva tagu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag.ui" line="54"/>
        <source>Change</source>
        <translation>Změnit</translation>
    </message>
</context>
<context>
    <name>DlgTagAssignment</name>
    <message>
        <location filename="../src/gui/dlg_tag_assignment.cpp" line="41"/>
        <source>Edit Tag Assignment</source>
        <translation>Editovat přiřazení tagů</translation>
    </message>
</context>
<context>
    <name>DlgTagSettings</name>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="24"/>
        <source>Connection</source>
        <translation>Spojení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="35"/>
        <source>Tag storage</source>
        <translation>Úložiště tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="46"/>
        <source>Use local tag database stored on this computer.</source>
        <translation>Použít místní databázi tagů uloženou na tomto počítači.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="61"/>
        <source>Access tag data on a server.
These data can then be shared between multiple users.</source>
        <translation>Přistupovat k datům tagů na serveru.
Tato data mohou být sdílena mezi více uživateli.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="49"/>
        <source>Use local tag database.</source>
        <translation>Použít místní databázi tagů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="65"/>
        <source>Connect to tag server.</source>
        <translation>Připojit se k serveru pro sdílení tagů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="80"/>
        <source>Tag server</source>
        <translation>Server pro sdílení tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="93"/>
        <source>Address or hostname:</source>
        <translation>Adresa nebo název hostitele:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="117"/>
        <source>Port number:</source>
        <translation>Číslo portu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="129"/>
        <source>Enter the port number.</source>
        <translation>Zadejte číslo portu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="141"/>
        <source>Certificate file:</source>
        <translation>Soubor certifikátu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="105"/>
        <source>Enter the name of the tag server, e.g. &apos;servername.domain&apos;.</source>
        <translation>Zadejte jméno serveru pro sdílení tagů, např. &quot;server.domena&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="14"/>
        <source>Tag Storage Settings</source>
        <translation>Nastavení úložiště tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="155"/>
        <source>Path to the CA (certification authority) certificate file
if the CA certificate isn&apos;t already stored in the CA certificate storage.</source>
        <translation>Cesta k souboru CA (certifikační autorita) certifikátu,
pokud tento CA certifikát již není uložen v úložišti CA certifikátů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="168"/>
        <source>Select certificate file.</source>
        <translation>Vyberte soubor certifikátu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="171"/>
        <source>Select</source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="196"/>
        <source>Username:</source>
        <translation>Uživatelské jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="208"/>
        <source>Enter the username to connect to the tag server.</source>
        <translation>Zadejte uživatelské jméno pro připojení k serveru pro sdílení tagů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="220"/>
        <source>Password:</source>
        <translation>Heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="234"/>
        <source>Provide a password to connect to the tag server.</source>
        <translation>Poskytněte heslo za účelem připojení k serveru pro sdílení tagů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="255"/>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="354"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="280"/>
        <source>Connect to tag server using the provided settings.</source>
        <translation>Připojit se k serveru pro sdílení tagů za použití poskytnutých údajů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="283"/>
        <source>Connect</source>
        <translation>Připojit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="306"/>
        <source>Profile:</source>
        <translation>Profil:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="335"/>
        <source>Refresh the profile list.</source>
        <translation>Obnovit seznam profilů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="338"/>
        <source>Refresh</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="365"/>
        <source>Tags</source>
        <translation>Tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="373"/>
        <source>Local Database</source>
        <translation>Místní databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="384"/>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="465"/>
        <source>Update</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="428"/>
        <source>To Server</source>
        <translation>Na server</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag_settings.ui" line="454"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="286"/>
        <source>Open Certificate</source>
        <translation>Otevřít certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="287"/>
        <source>Certificate Files (*.pem *.der)</source>
        <translation>Soubory certifikátů (*.pem *.der)</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="386"/>
        <source>The profile &apos;%1&apos; cannot be set.</source>
        <translation>Profil &quot;%1&quot; nelze nastavit.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="650"/>
        <source>Tag &apos;%1&apos; is similar to &apos;%2&apos;.</source>
        <translation>Tag &quot;%1&quot; je podobný &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="654"/>
        <source>Similar Tag Names</source>
        <translation>Podobná jména tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="655"/>
        <source>The following tag entries have similar but not exactly same names with names on the server. You may rename those tags to achieve a name match.</source>
        <translation>Následující záznamy tagů mají podobná, ale ne shodná, jména se jmény na serveru. Můžete tyto tagy přejmenovat, abyste dosáhli shody jmen.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="657"/>
        <source>Some locally stored tag entries won&apos;t be transferred to the server because of the listed similarities. See the details to view a listing.</source>
        <translation>Některé místně uložené záznamy tagů nebudou přeneseny na server kvůli uvedeným podobnostem. V podrobnostech je ueden seznam.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="665"/>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="714"/>
        <source>Message Assignment Failed</source>
        <translation>Přiřazení zprávám selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="666"/>
        <source>Could not copy tag assignment to server.</source>
        <translation>Nepodařilo se zkopírovat přiřazení tagů na server.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="681"/>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="690"/>
        <source>Tag Creation Failed</source>
        <translation>Vytvoření tagu selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="682"/>
        <source>Could not create new tags on server.</source>
        <translation>Nepodařilo se vytvořit nové tagy na serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="691"/>
        <source>Could list tags from server.</source>
        <translation>Podařilo se vyčíst tagy ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="715"/>
        <source>Could not copy new tag assignment to server.</source>
        <translation>Nepodařilo se zkopírovat přiřazení nových tagů na server.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="749"/>
        <source>Connected to server.</source>
        <translation>Připojen k serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="759"/>
        <location filename="../src/gui/dlg_tag_settings.cpp" line="891"/>
        <source>Not connected to server.</source>
        <translation>Nepřipojen k serveru.</translation>
    </message>
</context>
<context>
    <name>DlgTags</name>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="14"/>
        <source>Tag manager</source>
        <translation>Správce tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="22"/>
        <source>Available Tags</source>
        <translation>Dostupné tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="33"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="44"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="55"/>
        <source>Update</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="99"/>
        <source>Assign selected available tags.</source>
        <translation>Přiřadit vybrané dostupné tagy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="102"/>
        <source>Assign</source>
        <translation>Přiřadit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="113"/>
        <source>Cancel selected assignment.</source>
        <translation>Zrušit vybrané přiřazení.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="116"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="142"/>
        <source>Assigned Tags</source>
        <translation>Přiřazené tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="153"/>
        <source>Remove all</source>
        <translation>Odstranit vše</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tags.cpp" line="239"/>
        <source>Delete assigned tags</source>
        <translation>Smazat přiřazené tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tags.cpp" line="240"/>
        <source>Some selected tags are assigned to some messages.</source>
        <translation>Některé vybrané tagy jsou přiřazené některým zprávám.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tags.cpp" line="242"/>
        <source>Do you want to delete the selected tags?</source>
        <translation>Přejete si smazat vybrané tagy?</translation>
    </message>
</context>
<context>
    <name>DlgTimestampExpir</name>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="20"/>
        <source>Time stamp expiration checking</source>
        <translation>Kontrola expirace časového razítka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="26"/>
        <source>What do you want to do?</source>
        <translation>Co chcete udělat?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="32"/>
        <source>Check for expiring time stamps in current data box</source>
        <translation>Zkontrolovat expiraci časových razítek v aktuální schránce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="42"/>
        <source>Check for expiring time stamps in all data boxes</source>
        <translation>Zkontrolovat expiraci časových razítek u všech schránek</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="54"/>
        <source>Note: Checking in all data boxes can be slow. The action cannot be aborted.</source>
        <translation>Poznámka: Kontrola přes všechny schránky může být pomalá. Akci nebude možné přerušit.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="71"/>
        <source>Check for expiring time stamps in ZFO files in selected directory</source>
        <translation>Zkontrolovat expiraci časových razítek u ZFO souborů ve vybraném adresáři</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="86"/>
        <source>Include subdirectories</source>
        <translation>Zahrnout podadresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="116"/>
        <source>Note: Checking many files can be slow. The action cannot be aborted.</source>
        <translation>Poznámka: Kontrola velkého počtu ZFO souborů může být pomalá. Akci nebude možné přerušit.</translation>
    </message>
</context>
<context>
    <name>DlgToolBar</name>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="14"/>
        <source>Tool Bar</source>
        <translation>Panel nástrojů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="17"/>
        <source>Assign selected available actions to tool bar.</source>
        <translation>Přiřadit vybrané dostupné akce do panelu nástrojů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="27"/>
        <source>Filter</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="37"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="47"/>
        <source>Available Operations</source>
        <translation>Dostupné operace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="59"/>
        <source>Description</source>
        <translation>Popis</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="97"/>
        <source>Cancel selected assignment.</source>
        <translation>Zrušit vybrané přiřazení.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="128"/>
        <source>Style of Buttons</source>
        <translation>Vzhled tlačítek</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="134"/>
        <source>Only icons will be displayed in the toolbar.</source>
        <translation>Pouze ikony budou zobrazeny v nástrojovém panelu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="137"/>
        <source>Only display the icon</source>
        <translation>Zobrazovat pouze ikony</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="144"/>
        <source>Icons and text beside them will be displayed in the toolbar.</source>
        <translation>Ikony a popisky vedle nich budou zobrazeny v nástrojovém panelu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="147"/>
        <source>The text appears beside the icon</source>
        <translation>Zobrazovat text za ikonou</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="154"/>
        <source>Icons and text under them will be displayed in the toolbar.</source>
        <translation>Ikony a popisky pod nimi budou zobrazeny v nástrojovém panelu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="157"/>
        <source>The text appears under the icon</source>
        <translation>Zobrazovat text pod ikonou</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="167"/>
        <source>Assigned Operations</source>
        <translation>Přiřazené operace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="176"/>
        <source>Customise</source>
        <translation>Přizpůsobení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="185"/>
        <source>Insert Separator</source>
        <translation>Vložit oddělovač</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tool_bar.ui" line="205"/>
        <source>Defaults</source>
        <translation>Výchozí</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tool_bar.cpp" line="448"/>
        <source>Commands Unlisted in Top Menu</source>
        <translation>Příkazy neuvedené v horním menu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tool_bar.cpp" line="468"/>
        <source>All Commands</source>
        <translation>Všechny příkazy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tool_bar.cpp" line="840"/>
        <source>Label: %1
Command: %2
Tooltip: %3</source>
        <translation>Název: %1
Příkaz: %2
Nápověda: %3</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tool_bar.cpp" line="1380"/>
        <source>Type to search.</source>
        <translation>Pište hledané.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tool_bar.cpp" line="1381"/>
        <source>Enter sought expression.</source>
        <translation>Zadete hledaný výraz.</translation>
    </message>
</context>
<context>
    <name>DlgUpdatePortable</name>
    <message>
        <location filename="../src/gui/ui/dlg_update_portable.ui" line="17"/>
        <location filename="../src/gui/ui/dlg_update_portable.ui" line="62"/>
        <location filename="../src/gui/ui/dlg_update_portable.ui" line="69"/>
        <location filename="../src/gui/ui/dlg_update_portable.ui" line="83"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_update_portable.ui" line="29"/>
        <source>Change</source>
        <translation>Změnit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_update_portable.ui" line="38"/>
        <source>Leaves the configuration and all stored application data in the currently running
application version. No data will be copied into the newly extracted directory.
You&apos;ll need to copy or move the data manually in order to access it from
the newly extracted application package.</source>
        <translation>Ponechá konfigurační a všechna uložená aplikační data v současně bežící
verzi aplikace. Do nově rozbaleného adresáře nebudou kopírována žádná data.
Budete muset zkopírovat nebo přesunout data ručně, aby s nimi bylo možné pracovat
z nově rozbaleného balíku.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_update_portable.ui" line="44"/>
        <source>Leave current storage in place. I&apos;ll move it manually later.</source>
        <translation>Ponechat současné úložiště na místě. Přesunu jej ručně později.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_update_portable.ui" line="51"/>
        <source>The application will attempt to copy all the configuration and stored
application data into the newly extracted directory.</source>
        <translation>Aplikace se pokusí zkopírovat všechna konfigurační a uložená
aplikační data do nově rozbaleného adresáře.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_update_portable.ui" line="55"/>
        <source>Copy current storage to new place.</source>
        <translation>Zkopírovat současné úložiště do nového umístění.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="173"/>
        <source>Select Directory</source>
        <translation>Vybrat adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="242"/>
        <source>The selected directory is on a %1 file system. This file system may not be able to store very large files. Consider using NTFS or exFAT to avoid these types of problems.</source>
        <translation>Vybraný adresář je na souborovém systému typu %1. Tento souborový systém nemusí být schopen ukládat velmi veliké soubory. Zvažte použití NTFS nebo exFAT, abyste předešli tomuto typu problémů.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="261"/>
        <source>There is not enough space on device &apos;%1&apos;. At least %2 is required.</source>
        <translation>Na zařízení &quot;%1&quot; není dostatek volného místa. Je vyžadováno nejméně %2.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="297"/>
        <source>Extracting package...</source>
        <translation>Rozbaluji balík...</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="324"/>
        <location filename="../src/gui/dlg_update_portable.cpp" line="364"/>
        <source>Error extracting package</source>
        <translation>Chyba při rozbalování balíku</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="325"/>
        <location filename="../src/gui/dlg_update_portable.cpp" line="365"/>
        <source>Could not extract the package &apos;%1&apos; into destination directory &apos;%2&apos;.</source>
        <translation>Nezdařilo se rozbalit balík &quot;%1&quot; do cílového adresáře &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="373"/>
        <source>Package extracted</source>
        <translation>Balík rozbalen</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="374"/>
        <source>Package has been extracted into &apos;%1&apos;.</source>
        <translation>Balík byl rozbalen do &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="380"/>
        <source>Error copying data</source>
        <translation>Chyba při kopírování dat</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="381"/>
        <source>Could not find target directory &apos;%1&apos;.</source>
        <translation>Nezdařilo se najít cílový adresář &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="393"/>
        <source>Copying data...</source>
        <translation>Kopíruji data...</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="436"/>
        <source>Package extracted and data copied</source>
        <translation>Balík rozbalen a data zkopírována</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="437"/>
        <source>Package has been extracted into &apos;%1&apos;. Stored data have been copied into &apos;%2&apos;.</source>
        <translation>Balík byl rozbalen do &quot;%1&quot;. Uložená data byla zkopírována do &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="441"/>
        <source>Cannot copy data</source>
        <translation>Nelze zkopírovat data</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="442"/>
        <source>Could not copy data from &apos;%1&apos; to &apos;%2&apos;.</source>
        <translation>Nezdařilo se zkopírovat data z &quot;%1&quot; do &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_update_portable.cpp" line="454"/>
        <source>Select the location where to extract the new package and specify what to do with the data stored in this version.</source>
        <translation>Vyberte umístění, kam se má rozbalit nový balík, a určete, co udělat s uloženými daty v této verzi aplikace.</translation>
    </message>
</context>
<context>
    <name>DlgUserExt2</name>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="14"/>
        <source>User Information</source>
        <translation>Informace o uživateli</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="20"/>
        <source>User</source>
        <translation>Uživatel</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="32"/>
        <source>Name or names of the person.</source>
        <translation>Jméno nebo jména osoby.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="35"/>
        <source>Names:</source>
        <translation>Jména:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="45"/>
        <source>Surname of the person.</source>
        <translation>Příjmení osoby.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="48"/>
        <source>Surname:</source>
        <translation>Příjmení:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="58"/>
        <source>Person birth date. (dd.mm.yyyy)</source>
        <translation>Datum narození osoby. (dd.mm.rrrr)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="61"/>
        <source>Birth date:</source>
        <translation>Datum narození:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="81"/>
        <source>Address of residence</source>
        <translation>Adresa bydliště</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="93"/>
        <source>Street:</source>
        <translation>Ulice:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="105"/>
        <source>No. in street:</source>
        <translation>Č. orientační:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="121"/>
        <source>District:</source>
        <translation>Část obce:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="133"/>
        <source>Conscription no.:</source>
        <translation>Č. popisné:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="149"/>
        <source>Name of the municipality.</source>
        <translation>Jméno obce.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="152"/>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="277"/>
        <source>City:</source>
        <translation>Obec:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="164"/>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="289"/>
        <source>Postal code:</source>
        <translation>Směrovací číslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="180"/>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="305"/>
        <source>State:</source>
        <translation>Stát:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="212"/>
        <source>Evidence no.:</source>
        <translation>Č. evidenční:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="227"/>
        <source>Address where the login credetials should be delivered</source>
        <translation>Adresa doručení přístupových údajů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="235"/>
        <source>Use the above mentioned residence address.</source>
        <translation>Doručit na výše uvedenou adresu bydliště.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="242"/>
        <source>Use the company&apos;s headquarter address.</source>
        <translation>Doručit na adresu společnosti.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="249"/>
        <source>Use different address.</source>
        <translation>Doručit na zadanou doručovací adresu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="267"/>
        <source>Street and number:</source>
        <translation>Ulice a číslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="328"/>
        <source>Access rights</source>
        <translation>Přístupová oprávnění</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="342"/>
        <source>Access type</source>
        <translation>Typ oprávnění</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="356"/>
        <source>Read received messages</source>
        <translation>Číst přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="363"/>
        <source>Read received messages into own hands</source>
        <translation>Číst přijaté zprávy do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="370"/>
        <source>Send messages and read sent messages</source>
        <translation>Posílat zprávy a číst odeslané</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="377"/>
        <source>View lists, history and delivery info</source>
        <translation>Zobrazovat seznamy, historii a doručenky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="384"/>
        <source>Search for data boxes</source>
        <translation>Vyhledávat schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_user_ext2.ui" line="391"/>
        <source>Erase messages in vault</source>
        <translation>Mazat zprávy v trezoru</translation>
    </message>
</context>
<context>
    <name>DlgViewChangeLog</name>
    <message>
        <location filename="../src/gui/ui/dlg_view_changelog.ui" line="14"/>
        <source>What&apos;s new?</source>
        <translation>Co je nového?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_changelog.ui" line="20"/>
        <source>Version:</source>
        <translation>Verze:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_changelog.cpp" line="38"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
</context>
<context>
    <name>DlgViewFilenameParamList</name>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="14"/>
        <source>File Name Parameter List</source>
        <translation>Seznam parametrů v názvech souborů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="20"/>
        <source>List of available parameters and their descriptions. These parameters can be used in names of exported files. Use these parameters to pass additional information from message envelopes or related data boxes into the generated file names.</source>
        <translation>Seznam dostupných parametrů a jejich popisy. Tyto parametry lze použít v názvech exportovaných souborů. Použijte tyto parametry ke vkládání dodatečných informací z obálek datových zpráv nebo schránek do generovaných názvů souborů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="42"/>
        <source>Data box</source>
        <translation>Schránka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="73"/>
        <source>data-box name (separated by &apos;-&apos;); formerly %n</source>
        <translation>pojmenování schránky (odděleno &quot;-&quot;); dříve %n</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="105"/>
        <source>data-box ID; formerly %d</source>
        <translation>ID schránky; dříve %d</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="137"/>
        <source>user ID; formerly %u</source>
        <translation>ID uživatele; dříve %u</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="152"/>
        <source>Acceptance Time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="278"/>
        <source>acceptance year (YYYY); formerly %Y</source>
        <translation>rok doručení (YYYY); dříve %Y</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="291"/>
        <source>acceptance year short (YY)</source>
        <translation>rok doručení krátký (YY)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="304"/>
        <source>acceptance month (MM); formerly %M</source>
        <translation>měsíc doručení (MM); dříve %M</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="317"/>
        <source>acceptance day (DD); formerly %D</source>
        <translation>den doručení (DD); dříve %D</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="330"/>
        <source>acceptance hour (hh); formerly %h</source>
        <translation>hodina doručení (hh); dříve %h</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="343"/>
        <source>acceptance minute (mm); formerly %m</source>
        <translation>minuta doručení (mm); dříve %m</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="358"/>
        <source>Delivery Time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="484"/>
        <source>delivery year (YYYY)</source>
        <translation>rok dodání (YYYY)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="497"/>
        <source>delivery year short (YY)</source>
        <translation>rok dodání krátký (YY)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="510"/>
        <source>delivery month (MM)</source>
        <translation>měsíc dodání (MM)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="523"/>
        <source>delivery day (DD)</source>
        <translation>den dodání (DD)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="536"/>
        <source>delivery hour (hh)</source>
        <translation>hodina doručení (HH)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="549"/>
        <source>delivery minute (mm)</source>
        <translation>minuta doručení (mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="564"/>
        <source>Attachment File</source>
        <translation>Soubor přílohy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="595"/>
        <source>attachment filename (including suffix); formerly %f</source>
        <translation>jméno souboru přílohy (včetně přípony); dříve %f</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="614"/>
        <source>Message Envelope</source>
        <translation>Obálka zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="645"/>
        <source>message ID; formerly %i</source>
        <translation>ID zprávy; dříve %i</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="677"/>
        <source>message subject (separated by &apos;-&apos;); formerly %s</source>
        <translation>předmět zprávy (oddělen &quot;-&quot;); dříve %s</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="709"/>
        <source>sender data-box ID</source>
        <translation>ID schránky odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="741"/>
        <source>sender name (separated by &apos;-&apos;); formerly %S</source>
        <translation>jméno odesílatele (odděleno &quot;-&quot;); dříve %S</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="773"/>
        <source>sender address (separated by &apos;-&apos;)</source>
        <translation>adresa odesílatele (oddělena &quot;-&quot;)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="805"/>
        <source>recipient data-box ID</source>
        <translation>ID schránky adresáta</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="837"/>
        <source>recipient name (separated by &apos;-&apos;); formerly %R</source>
        <translation>jméno adresáta (odděleno &quot;-&quot;); dříve %R</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="869"/>
        <source>recipient address (separated by &apos;-&apos;)</source>
        <translation>adresa adresáta (oddělena &quot;-&quot;)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="901"/>
        <source>sender ref. number (separated by &apos;-&apos;)</source>
        <translation>jedn. číslo odesílatele (odděleno &quot;-&quot;)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="933"/>
        <source>sender file mark (separated by &apos;-&apos;)</source>
        <translation>spisová značka odesílatele (oddělena &quot;-&quot;)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="965"/>
        <source>recipient ref. number (separated by &apos;-&apos;)</source>
        <translation>jedn. číslo adresáta (odděleno &quot;-&quot;)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="997"/>
        <source>recipient file mark (separated by &apos;-&apos;)</source>
        <translation>spisová značka adresáta (oddělena &quot;-&quot;)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="1029"/>
        <source>to hands (separated by &apos;-&apos;)</source>
        <translation>k rukám (odděleno &quot;-&quot;)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="1044"/>
        <source>Message Orientation</source>
        <translation>Orientace zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="1075"/>
        <source>message orientation</source>
        <translation>orientace zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="1088"/>
        <source>ODZ - sent data message</source>
        <translation>ODZ - odeslaná datová zpráva</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="1101"/>
        <source>DDZ - received data message</source>
        <translation>DDZ - přijatá datová zpráva</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="1116"/>
        <source>Directories and File Paths</source>
        <translation>Adresáře a cesty k souborům</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="1144"/>
        <source>means directory separator</source>
        <translation>znamená oddělovač adresářů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="1168"/>
        <source>For example:</source>
        <translation>Příklad:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_filename_param_list.ui" line="1181"/>
        <source>can expand to</source>
        <translation>může expandovat na</translation>
    </message>
</context>
<context>
    <name>DlgViewLog</name>
    <message>
        <location filename="../src/gui/ui/dlg_view_log.ui" line="14"/>
        <source>Log content</source>
        <translation>Obsah logu</translation>
    </message>
</context>
<context>
    <name>DlgViewZfo</name>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="74"/>
        <source>Error parsing content</source>
        <translation>Chyba načítání obsahu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="116"/>
        <source>Cannot parse the content of file &apos;%1&apos;.</source>
        <translation>Nelze načíst obsah souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="160"/>
        <source>Cannot parse the content of message.</source>
        <translation>Nelze načíst obsah zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="195"/>
        <source>Open attachment</source>
        <translation>Otevřít přílohu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="200"/>
        <source>Save attachment</source>
        <translation>Uložit přílohu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="203"/>
        <source>Save attachments</source>
        <translation>Uložit všechny přílohy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="356"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="369"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="389"/>
        <source>Valid</source>
        <translation>Platný</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="358"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="371"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="389"/>
        <source>Invalid</source>
        <translation>Neplatný</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="359"/>
        <source>Message signature and content do not correspond!</source>
        <translation>Podpis zprávy a její obsah si neodpovídají!</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="375"/>
        <source>Certificate revocation check is turned off!</source>
        <translation>Kontrola zneplatnění certifikátu je vypnutá!</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_zfo.ui" line="14"/>
        <source>View message from ZFO</source>
        <translation>Zobrazit zprávu ze ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_zfo.ui" line="56"/>
        <source>Signature details</source>
        <translation>Detail podpisu</translation>
    </message>
</context>
<context>
    <name>ErrorEntry</name>
    <message>
        <location filename="../src/datovka_shared/records_management/json/entry_error.cpp" line="312"/>
        <source>No error occurred</source>
        <translation>Nevyskytla se žádná chyba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/records_management/json/entry_error.cpp" line="315"/>
        <source>Request was malformed</source>
        <translation>Požadavek byl vadný</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/records_management/json/entry_error.cpp" line="318"/>
        <source>Identifier is missing</source>
        <translation>Chybí identifikátor</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/records_management/json/entry_error.cpp" line="321"/>
        <source>Supplied identifier is wrong</source>
        <translation>Dodaný identifikátor je špatný</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/records_management/json/entry_error.cpp" line="324"/>
        <source>File format is not supported</source>
        <translation>Formát souboru není podporován</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/records_management/json/entry_error.cpp" line="327"/>
        <source>Data are already present</source>
        <translation>Data jsou již k dispozici</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/records_management/json/entry_error.cpp" line="330"/>
        <source>Service limit was exceeded</source>
        <translation>Limit služby byl dosažen</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/records_management/json/entry_error.cpp" line="333"/>
        <source>Unspecified error</source>
        <translation>Nespecifikovaná chyba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/records_management/json/entry_error.cpp" line="337"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>Export</name>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="97"/>
        <source>at</source>
        <translation>v</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="164"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="924"/>
        <source>User Type</source>
        <translation>Typ uživatele</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="171"/>
        <source>Full Name</source>
        <translation>Plné jméno</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="279"/>
        <source>paragraph</source>
        <translation>odstavec</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="283"/>
        <source>letter</source>
        <translation>písmeno</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="305"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="491"/>
        <source>Our Reference Number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="309"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="494"/>
        <source>Our File Mark</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="313"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="497"/>
        <source>Your Reference Number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="317"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="500"/>
        <source>Your File Mark</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="351"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="489"/>
        <source>Mandate</source>
        <translation>Zmocnění</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="356"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="503"/>
        <source>To Hands</source>
        <translation>K rukám</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="360"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="505"/>
        <source>Personal Delivery</source>
        <translation>Do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="361"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="507"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="510"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="630"/>
        <source>yes</source>
        <translation>ano</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="382"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="456"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="720"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="383"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="472"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="720"/>
        <source>Recipient</source>
        <translation>Adresát</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="384"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="385"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="386"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="460"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="476"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="743"/>
        <source>Data-Box ID</source>
        <translation>ID schránky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="388"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="462"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="478"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="744"/>
        <source>Data-Box Type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="391"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="465"/>
        <source>Message Author</source>
        <translation>Odesílající osoba</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="407"/>
        <source>Delivery Info</source>
        <translation>Dodejka</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="410"/>
        <source>Acceptance Info</source>
        <translation>Doručenka</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="414"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="427"/>
        <source>Message Envelope</source>
        <translation>Obálka datové zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="422"/>
        <source>Message Delivery Info</source>
        <translation>Dodejka datové zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="424"/>
        <source>Message Acceptance Info</source>
        <translation>Doručenka datové zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="431"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="561"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="621"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="719"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="432"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="560"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="620"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="718"/>
        <source>Message ID</source>
        <translation>ID zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="434"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="563"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="625"/>
        <source>Message Type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="438"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="530"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="566"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="669"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="684"/>
        <source>Message State</source>
        <translation>Stav zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="442"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="528"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="570"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="672"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="681"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="723"/>
        <source>Acceptance Time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="445"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="526"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="573"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="675"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="679"/>
        <source>Delivery Time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="449"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="622"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="725"/>
        <source>Attachment Size</source>
        <translation>Velikost příloh</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="487"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="490"/>
        <source>not specified</source>
        <translation>nezadáno</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="507"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="510"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="630"/>
        <source>no</source>
        <translation>ne</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="508"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="628"/>
        <source>Prohibited Acceptance through Fiction</source>
        <translation>Zakázáno doručení fikcí</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="515"/>
        <source>Message Events</source>
        <translation>Události zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="538"/>
        <source>Message Attachments</source>
        <translation>Přílohy zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="559"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="619"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="717"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="595"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="664"/>
        <source>Additional</source>
        <translation>Doplňující</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="600"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="690"/>
        <source>Events</source>
        <translation>Události</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="700"/>
        <source>Records Management Location</source>
        <translation>Umístění ve spisové službě</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="702"/>
        <source>Location</source>
        <translation>Umístění</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="742"/>
        <source>Data-Box Info</source>
        <translation>Informace o schránce</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="746"/>
        <source>Open Addressing</source>
        <translation>Otevřené adresování</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="748"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="748"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="749"/>
        <source>Data-Box State</source>
        <translation>Stav schránky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="752"/>
        <source>Upper Data-Box ID</source>
        <translation>ID nadřízené schránky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="756"/>
        <source>OVM ID</source>
        <translation>OVM ID</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="760"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="933"/>
        <source>ROB Identification</source>
        <translation>Ztotožnění s ROB</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="762"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="935"/>
        <source>identified</source>
        <translation>ztotožněn</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="762"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="935"/>
        <source>not identified</source>
        <translation>není ztotožněn</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="766"/>
        <source>Data-Box Owner Info</source>
        <translation>Informace o majiteli datové schránky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="768"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="852"/>
        <source>Company Name</source>
        <translation>Název firmy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="772"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="856"/>
        <source>IČ</source>
        <translation>IČ</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="775"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="859"/>
        <source>Given Names</source>
        <translation>Jména</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="779"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="863"/>
        <source>Last Name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="785"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="867"/>
        <source>Date of Birth</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="789"/>
        <source>City of Birth</source>
        <translation>Místo narození</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="793"/>
        <source>County of Birth</source>
        <translation>Okres narození</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="797"/>
        <source>State of Birth</source>
        <translation>Stát narození</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="803"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="907"/>
        <source>Street of Residence</source>
        <translation>Sídlo - ulice</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="807"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="877"/>
        <source>Number in Street</source>
        <translation>Číslo orientační</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="811"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="881"/>
        <source>Number in Municipality</source>
        <translation>Číslo popisné</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="815"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="893"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="915"/>
        <source>Zip Code</source>
        <translation>PSČ</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="819"/>
        <source>District of Residence</source>
        <translation>Sídlo - část obce</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="823"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="911"/>
        <source>City of Residence</source>
        <translation>Sídlo - město</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="827"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="919"/>
        <source>State of Residence</source>
        <translation>Sídlo - stát</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="831"/>
        <source>Nationality</source>
        <translation>Státní občanství</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="835"/>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="901"/>
        <source>RUIAN Address Code</source>
        <translation>Adresní kód RUIAN</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="850"/>
        <source>User Info</source>
        <translation>Informace o uživateli</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="873"/>
        <source>Street</source>
        <translation>Ulice</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="885"/>
        <source>District</source>
        <translation>Část obce</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="889"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="897"/>
        <source>State</source>
        <translation>Stát</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="926"/>
        <source>Permissions</source>
        <translation>Oprávnění</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="929"/>
        <source>Unique User ISDS ID</source>
        <translation>Unikátní ID uživatele ISDS</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="948"/>
        <source>Password Expiration</source>
        <translation>Expirace hesla</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="954"/>
        <source>Local Database Location</source>
        <translation>Umístění místní databáze</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="957"/>
        <source>Data box has no own message database associated.</source>
        <translation>Schránka nemá přiřazenou žádnou vlastní databázi zpráv.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="960"/>
        <source>local database file</source>
        <translation>místní databázový soubor</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="987"/>
        <source>All Messages</source>
        <translation>Všechny zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="988"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="989"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="998"/>
        <source>Signature</source>
        <translation>Podpis</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="1000"/>
        <source>Message Signature</source>
        <translation>Podpis zprávy</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="1003"/>
        <source>Signing Certificate</source>
        <translation>Podepisující certifikát</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/html/html_export.cpp" line="1006"/>
        <source>Time Stamp</source>
        <translation>Časové razítko</translation>
    </message>
</context>
<context>
    <name>Exports</name>
    <message>
        <location filename="../src/io/exports.cpp" line="146"/>
        <location filename="../src/io/exports.cpp" line="311"/>
        <source>message</source>
        <translation>zpráva</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="134"/>
        <location filename="../src/io/exports.cpp" line="299"/>
        <location filename="../src/io/exports.cpp" line="573"/>
        <location filename="../src/io/exports.cpp" line="675"/>
        <source>Cannot access message database for username &apos;%1&apos;.</source>
        <translation>Nelze zpřístupnit databázový soubor zpráv pro uživatele &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="152"/>
        <location filename="../src/io/exports.cpp" line="158"/>
        <location filename="../src/io/exports.cpp" line="164"/>
        <location filename="../src/io/exports.cpp" line="170"/>
        <location filename="../src/io/exports.cpp" line="318"/>
        <location filename="../src/io/exports.cpp" line="325"/>
        <location filename="../src/io/exports.cpp" line="333"/>
        <location filename="../src/io/exports.cpp" line="340"/>
        <source>acceptance info</source>
        <translation>doručenka</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="176"/>
        <location filename="../src/io/exports.cpp" line="348"/>
        <source>message envelope</source>
        <translation>obálka zpráv</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="393"/>
        <source>Save %1 as file (*%2)</source>
        <translation>Uložit %1 jako soubor (*%2)</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="394"/>
        <source>File (*%1)</source>
        <translation>Soubor (*%1)</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="183"/>
        <location filename="../src/io/exports.cpp" line="356"/>
        <source>Export file type of message &apos;%1&apos; was not specified!</source>
        <translation>Nebyl specifikován výstupní typ exportované zprávy &quot;%1&quot;!</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="195"/>
        <location filename="../src/io/exports.cpp" line="364"/>
        <source>Complete message &apos;%1&apos; missing!</source>
        <translation>Chybí kompletní zpráva &quot;%1&quot;!</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="199"/>
        <location filename="../src/io/exports.cpp" line="368"/>
        <source>Export of %1 &apos;%2&apos; to %3 was not successful!</source>
        <translation>Export %1 &quot;%2&quot; do %3 nebyl úspěšný!</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="262"/>
        <location filename="../src/io/exports.cpp" line="444"/>
        <source>Export of %1 &apos;%2&apos; to %3 was successful.</source>
        <translation>Export %1 &quot;%2&quot; do %3 byl úspěšný.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="266"/>
        <location filename="../src/io/exports.cpp" line="448"/>
        <source>Export of %1 &apos;%2&apos; to %3 wasn&apos;t successful. Cannot write the file. File name may be too long.</source>
        <translation>Export %1 &quot;%2&quot; do %3 nebyl úspěšný. Nelze zapsat soubor. Jméno souborů může být příliš dlouhé.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="582"/>
        <location filename="../src/io/exports.cpp" line="684"/>
        <source>Complete message &apos;%1&apos; is missing.</source>
        <translation>Chybí úplná zpráva &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="598"/>
        <location filename="../src/io/exports.cpp" line="721"/>
        <source>Some attachments of message &apos;%1&apos; couldn&apos;t be stored.</source>
        <translation>Některé přílohy zprávy &quot;%1&quot; nebyly uloženy.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="615"/>
        <location filename="../src/io/exports.cpp" line="738"/>
        <source>Some files of message &apos;%1&apos; have not been written to storage.</source>
        <translation>Některé soubory zprávy &quot;%1&quot; nebyly zapsány do úložiště.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="637"/>
        <source>Export of message envelope &apos;%1&apos; to PDF was not successful.</source>
        <translation>Export obálky zprávy &quot;%1&quot; do souboru PDF nebyl úspěšný.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="652"/>
        <source>All attachments of message &apos;%1&apos; and the envelope PDF have been successfully saved to target directory.</source>
        <translation>Všechny přílohy zprávy &quot;%1&quot; a PDF obálky byly úspěšně uloženy do cílového adresáře.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="655"/>
        <source>The envelope PDF of message &apos;%1&apos; hasn&apos;t been saved.</source>
        <translation>PDF obálky zprávy &quot;%1&quot; nebyl uložen.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="786"/>
        <source>All attachments of message &apos;%1&apos; have been successfully saved to target directory.</source>
        <translation>Všechny přílohy zprávy &quot;%1&quot; byly úspěšně uloženy do cílového adresáře.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="790"/>
        <source>Some attachments of message &apos;%1&apos; haven&apos;t been saved.</source>
        <translation>Některé přílohy zprávy &quot;%1&quot; nebyly uloženy.</translation>
    </message>
</context>
<context>
    <name>GovServiceListModel</name>
    <message>
        <location filename="../src/gov_services/models/gov_service_list_model.cpp" line="102"/>
        <source>data box: %1 - %2</source>
        <translation>datová schránka: %1 - %2</translation>
    </message>
    <message>
        <location filename="../src/gov_services/models/gov_service_list_model.cpp" line="104"/>
        <source>Send request %1 into the data box %2 %3.</source>
        <translation>Odeslat podání %1 do datové schránky %2 %3.</translation>
    </message>
</context>
<context>
    <name>Gui::LogInCycle</name>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="86"/>
        <source>The login procedure for data box &apos;%1&apos; has been cancelled.</source>
        <translation>Přihlašovací procedura pro schránku &quot;%1&quot; byla přerušena.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="90"/>
        <source>Login cancelled for data box &apos;%1&apos;.</source>
        <translation>Přihlašování do schránky &quot;%1&quot; zrušeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="105"/>
        <source>Invalid Certificate Data</source>
        <translation>Neplatná data certifikátu</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="106"/>
        <source>The certificate or the supplied pass-phrase are invalid.</source>
        <translation>Certifikát nebo zadané heslo jsou neplatné.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="108"/>
        <source>Please enter a path to a valid certificate and/or provide a correct key to unlock the certificate.</source>
        <translation>Prosím zadejte cestu k platnému certifikátu a/nebo zadejte správný klíč pro odemknutí certifikátu.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="111"/>
        <source>Bad certificate data for data box &apos;%1&apos;.</source>
        <translation>Špatná data certifikátu pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="124"/>
        <location filename="../src/gui/isds_login.cpp" line="186"/>
        <location filename="../src/gui/isds_login.cpp" line="200"/>
        <location filename="../src/gui/isds_login.cpp" line="213"/>
        <location filename="../src/gui/isds_login.cpp" line="272"/>
        <location filename="../src/gui/isds_login.cpp" line="287"/>
        <location filename="../src/gui/isds_login.cpp" line="320"/>
        <location filename="../src/gui/isds_login.cpp" line="334"/>
        <source>It was not possible to connect to the data box &apos;%1&apos;.</source>
        <translation>Nebylo možné se připojit k datové schránce &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="129"/>
        <source>The log-in method used in data box &apos;%1&apos; is not implemented.</source>
        <translation>Přihlašovací metoda použitá ve schránce &quot;%1&quot; není implementována.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="141"/>
        <source>Waiting for acknowledgement from the Mobile key application for the data box &apos;%1&apos;.</source>
        <translation>Čeká se na potvrzení z aplikace Mobilní klíč pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="224"/>
        <source>Data box: %1
Username: %2
Certificate file: %3
Enter password to unlock certificate file:</source>
        <translation>Schránka: %1
Uživatelské jméno: %2
Soubor certifikátu: %3
Zadejte heslo pro odemčení souboru certifikátu:</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="256"/>
        <source>Data box &apos;%1&apos; requires an OTP security code authentication&lt;br/&gt;in order to connect to the data box.</source>
        <translation>Pro připojení ke schránce &quot;%1&quot; je&lt;br/&gt;vyžadována OTP autentizace bezpečnostním kódem.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="259"/>
        <source>Enter OTP security code for data box</source>
        <translation>Zadejte OTP bezpečnostní kód pro schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="308"/>
        <source>SMS Code for Data Box &apos;%1&apos;</source>
        <translation>SMS kód pro schránku &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="309"/>
        <source>Data box &apos;%1&apos; requires a security code authentication in order to connect.</source>
        <translation>Schránka &quot;%1&quot; vyžaduje pro přihlášení autorizaci bezpečnostním kódem.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="140"/>
        <source>Mobile Key Login</source>
        <translation>Přihlášení mobilním klíčem</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="223"/>
        <source>Password Required</source>
        <translation>Vyžadováno heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="254"/>
        <source>Enter OTP Security Code</source>
        <translation>Zadejte OTP bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="312"/>
        <source>The security code will be sent to you via a premium SMS.</source>
        <translation>Bezpečnostní kód Vám bude zaslán prémiovou SMS zprávou.</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="314"/>
        <source>Do you want to send a premium SMS containing a security code into your mobile phone?</source>
        <translation>Chcete zaslat prémiovou SMS obsahující bezpečnostní kód do Vašeho mobilu?</translation>
    </message>
    <message>
        <location filename="../src/gui/isds_login.cpp" line="85"/>
        <source>Login Cancelled</source>
        <translation>Přihlašování zrušeno</translation>
    </message>
</context>
<context>
    <name>GuiMsgOps</name>
    <message>
        <location filename="../src/gui/message_operations.cpp" line="91"/>
        <source>Select Directory</source>
        <translation>Vybrat adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/message_operations.cpp" line="139"/>
        <source>Data message</source>
        <translation>Datová zpráva</translation>
    </message>
    <message>
        <location filename="../src/gui/message_operations.cpp" line="139"/>
        <source>Data messages</source>
        <translation>Datové zprávy</translation>
    </message>
</context>
<context>
    <name>Isds</name>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="64"/>
        <source>User type</source>
        <translation>Typ uživatele</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="70"/>
        <source>Full name</source>
        <translation>Plné jméno</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="77"/>
        <source>Date of birth</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="83"/>
        <source>City of birth</source>
        <translation>Místo narození</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="87"/>
        <source>County of birth</source>
        <translation>Okres narození</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="91"/>
        <source>RUIAN address code</source>
        <translation>Adresní kód RUIAN</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="95"/>
        <source>Full address</source>
        <translation>Adresa pobytu</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="100"/>
        <source>ROB identification</source>
        <translation>Ztotožnění s ROB</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="101"/>
        <source>identified</source>
        <translation>ztotožněn</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/isds/to_text_conversion.cpp" line="101"/>
        <source>not identified</source>
        <translation>není ztotožněn</translation>
    </message>
</context>
<context>
    <name>IsdsLogin</name>
    <message>
        <location filename="../src/io/isds_login.cpp" line="110"/>
        <source>Error when connecting to ISDS server!</source>
        <translation>Chyba během připojování k serveru ISDS!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="116"/>
        <source>Error during authentication!</source>
        <translation>Chyba během autentizace!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="120"/>
        <source>Authentication failed!</source>
        <translation>Chyba autentizace!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="122"/>
        <location filename="../src/io/isds_login.cpp" line="138"/>
        <location filename="../src/io/isds_login.cpp" line="152"/>
        <location filename="../src/io/isds_login.cpp" line="169"/>
        <location filename="../src/io/isds_login.cpp" line="183"/>
        <location filename="../src/io/isds_login.cpp" line="197"/>
        <location filename="../src/io/isds_login.cpp" line="213"/>
        <location filename="../src/io/isds_login.cpp" line="226"/>
        <location filename="../src/io/isds_login.cpp" line="240"/>
        <location filename="../src/io/isds_login.cpp" line="256"/>
        <source>Error: </source>
        <translation>Chyba: </translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="124"/>
        <source>Please check your credentials and login method together with your password.</source>
        <translation>Prosím, zkontrolujte své přihlašovací údaje, metodu přihlášení či heslo.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="126"/>
        <location filename="../src/io/isds_login.cpp" line="142"/>
        <source>It is also possible that your password has expired - in this case, you need to use the official ISDS web interface to change it.</source>
        <translation>Také je možné, že vypršela platnost Vašeho hesla - v tomto případě se musíte přihlásit na stránky ISDS a heslo si změnit.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="132"/>
        <source>Error during OTP authentication!</source>
        <translation>Chyba během OTP autentizace!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="136"/>
        <source>OTP authentication failed!</source>
        <translation>OTP autentizace selhala!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="140"/>
        <source>Please check your credentials together with entered security/SMS code and try again.</source>
        <translation>Prosím, zkontrolujte své přihlašovací údaje, metodu přihlášení a zadaný bezpečnostní/SMS kód a zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="148"/>
        <source>It was not possible to establish a connection within a set time.</source>
        <translation>Nebylo možné vytvořit spojení ve stanoveném čase.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="150"/>
        <source>Time-out for connection to server expired!</source>
        <translation>Vypršel čas vymezený pro připojení k serveru!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="154"/>
        <source>This is either caused by an extremely slow and/or unstable connection or by an improper set-up.</source>
        <translation>Tento problém je obvykle způsoben velmi pomalým a/nebo nestabilním spojením či nesprávným nastavením sítě.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="156"/>
        <location filename="../src/io/isds_login.cpp" line="201"/>
        <source>Please check your internet connection and try again.</source>
        <translation>Prosím, zkontrolujte Vaše připojení k internetu a zkuste akci opakovat.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="158"/>
        <source>It might be necessary to use a proxy to connect to the server. It is also possible that the ISDS server is inoperative or busy. Try again later.</source>
        <translation>Je možné, že pro spojení se serverem je třeba použít proxy. Také je možné, že ISDS server je mimo provoz, nebo je zaneprázdněný. Zkuste to později.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="165"/>
        <location filename="../src/io/isds_login.cpp" line="179"/>
        <location filename="../src/io/isds_login.cpp" line="193"/>
        <location filename="../src/io/isds_login.cpp" line="209"/>
        <location filename="../src/io/isds_login.cpp" line="222"/>
        <location filename="../src/io/isds_login.cpp" line="236"/>
        <source>It was not possible to establish a connection between your computer and the ISDS server.</source>
        <translation>Nebylo možné navázat spojení mezi Vaším počítačem a serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="167"/>
        <source>HTTPS problem occurred or redirection to server failed!</source>
        <translation>Vyskytl se problém s HTTPS nebo selhalo přesměrování na server!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="171"/>
        <location filename="../src/io/isds_login.cpp" line="185"/>
        <source>This is usually caused by either lack of internet connectivity or by some problem with the ISDS server.</source>
        <translation>Je to obvykle způsobeno výpadkem internetového připojení nebo nějakým problémem na serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="173"/>
        <location filename="../src/io/isds_login.cpp" line="187"/>
        <source>It is possible that the ISDS server is inoperative or busy. Try again later.</source>
        <translation>Pravděpodobně je server ISDS mimo provoz nebo momentálně zaneprázdněný. Zkuste to později.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="181"/>
        <source>An ISDS server problem occurred or service was not found!</source>
        <translation>Vyskytl se problém serveru ISDS nebo požadována služba nebyla nalezena!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="195"/>
        <source>The connection to server failed or a problem with the network occurred!</source>
        <translation>Připojení k serveru selhalo nebo se vyskytl problém se síťovým spojením!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="199"/>
        <source>This is usually caused by either lack of internet connectivity or by a firewall on the way.</source>
        <translation>Tento problém je obvykle způsoben neexistencí aktivního internetového připojení nebo přítomností firewallu.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="203"/>
        <source>It might be necessary to use a proxy to connect to the server. If yes, please set it up in the proxy settings menu.</source>
        <translation>Je možné, že pro spojení se serverem je třeba použít proxy. Pokud ano, nastavte ji prosím v menu nastavení proxy.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="211"/>
        <source>Problem with HTTPS connection!</source>
        <translation>Problém s HTTPS spojením!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="215"/>
        <source>This may be caused by a missing certificate for the SSL communication or the application cannot open an SSL socket.</source>
        <translation>Pravděpodobně chybí potřebný certifikát pro HTTPS komunikaci se serverem nebo se nepodařilo otevřít SSL soket.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="228"/>
        <source>This may be caused by a missing SSL certificate needed for communication with the server or it was not possible to establish a secure connection with the ISDS server.</source>
        <translation>Toto může být způsobeno chybějícím SSL certifikátem, který je potřeba pro komunikaci se serverem, nebo nebylo možné ustavit bezpečný komunikační kanál se serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="242"/>
        <source>This may be caused by an error in SOAP or the XML content for this web service is invalid.</source>
        <translation>Toto může být způsobeno chybou požadavku SOAP nebo obsah XML pro tuto službu není validní.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="217"/>
        <source>It is also possible that some libraries (e.g. CURL, SSL) may be missing or may be incorrectly configured.</source>
        <translation>Je také možné, že nebyly nalezeny některé potřebné knihovny (např. CURL, SSL) anebo jsou nesprávně nakonfigurovány.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="118"/>
        <location filename="../src/io/isds_login.cpp" line="134"/>
        <source>It was not possible to connect to the data box &apos;%1&apos;.</source>
        <translation>Nebylo možné se připojit k datové schránce &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="224"/>
        <source>HTTPS problem or security problem!</source>
        <translation>Problém s HTTPS spojením či bezpečnostní problém!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="230"/>
        <source>It is also possible that the certificate has expired.</source>
        <translation>Dále je možné, že vypršela platnost certifikátu.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="238"/>
        <source>SOAP problem or XML problem!</source>
        <translation>Problém SOAP či XML!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="244"/>
        <source>It is also possible that the ISDS server is inoperative or busy. Try again later.</source>
        <translation>Je také možné, že server ISDS je momentálně mimo provoz nebo přetížen. Zkuste to později.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="250"/>
        <location filename="../src/io/isds_login.cpp" line="254"/>
        <source>Datovka internal error!</source>
        <translation>Interní chyba Datovky!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="252"/>
        <source>It was not possible to establish a connection to the ISDS server.</source>
        <translation>Nebylo možné navázat spojení se serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="258"/>
        <source>An unexpected error occurred. Please restart the application and try again. It this doesn&apos;t help then you should contact the support for this application.</source>
        <translation>Nastala neočekávaná chyba v aplikaci. Prosím, restartujte aplikaci a zkuste to znova. Kontaktujte technickou podporu aplikace, pokud se problém opakuje.</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../src/isds/services_login.cpp" line="53"/>
        <location filename="../src/isds/services_login.cpp" line="84"/>
        <location filename="../src/isds/services_login.cpp" line="151"/>
        <location filename="../src/isds/services_login.cpp" line="220"/>
        <location filename="../src/isds/services_login.cpp" line="289"/>
        <location filename="../src/isds/services_login.cpp" line="357"/>
        <source>Insufficient input.</source>
        <translation>Nedostatečný vstup.</translation>
    </message>
    <message>
        <location filename="../src/isds/services_login.cpp" line="94"/>
        <location filename="../src/isds/services_login.cpp" line="161"/>
        <location filename="../src/isds/services_login.cpp" line="230"/>
        <location filename="../src/isds/services_login.cpp" line="300"/>
        <location filename="../src/isds/services_login.cpp" line="320"/>
        <location filename="../src/isds/services_login.cpp" line="367"/>
        <source>Insufficient memory.</source>
        <translation>Nedostatek paměti.</translation>
    </message>
    <message>
        <location filename="../src/isds/services_login.cpp" line="308"/>
        <source>Error converting types.</source>
        <translation>Chyba při konverzi typů.</translation>
    </message>
    <message>
        <location filename="../src/isds/services_login.cpp" line="388"/>
        <source>Invalid conversion.</source>
        <translation>Neplatná konverze.</translation>
    </message>
</context>
<context>
    <name>MWDbRepair</name>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="14"/>
        <source>Database Repair</source>
        <translation>Oprava databáze</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="29"/>
        <source>Source (corrupted) database file:</source>
        <translation>Zdrojový (poškozený) soubor databáze:</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="36"/>
        <source>Select source message database file.</source>
        <translation>Vyberte zdrojový soubor s databází zpráv.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="45"/>
        <source>Extracted data are stored in the work directory.</source>
        <translation>Vytažená data jsou ukládána do pracovního adresáře.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="48"/>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="55"/>
        <source>Select work directory.</source>
        <translation>Vyberte pracovní adresář.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="58"/>
        <source>Choose Directory</source>
        <translation>Vybrat adresář</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="67"/>
        <source>Work directory:</source>
        <translation>Pracovní adresář:</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="85"/>
        <source>Try reassembling new database from recovered data.</source>
        <translation>Pokusit se o sestavení nové databáze ze získaných dat.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="105"/>
        <source>Start the data recovery.
Scans the source database for readable data.
Extracts message and delivery info data and stores them as separate files.</source>
        <translation>Spustí obnovu dat.
Prohledá zdrojovou databázi na čitelná data.
Vytáhne data datových zpráv a dodejek a uloží je jako samostatné soubory.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="110"/>
        <source>Run Recovery</source>
        <translation>Spustit obnovení</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="119"/>
        <source>Recovered Database</source>
        <translation>Obnovená databáze</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="127"/>
        <source>Recovered database file:</source>
        <translation>Obnovený soubor databáze:</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="141"/>
        <source>Create a copy of the source (corrupted) database file before replacing it.</source>
        <translation>Vytvořit kopii zdrojového (poškozeného) souboru databáze, než bude nahrazen.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="161"/>
        <source>Overwrites the source (corrupted) database file with the restored database.</source>
        <translation>Přepíše zdrojový (poškozený) soubor databáze obnovenou databází.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="164"/>
        <source>Replace Corrupted</source>
        <translation>Nahradit poškozenou</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="189"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="200"/>
        <source>Quit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="203"/>
        <source>Quit the application.</source>
        <translation>Ukončit aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="206"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../src/db_repair/ui/mw_db_repair.ui" line="214"/>
        <source>Load message database file.</source>
        <translation>Nahrát soubor s databází zpráv.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="779"/>
        <source>Pending Tasks</source>
        <translation>Nedokončené úlohy</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="780"/>
        <source>The application is currently processing some tasks.</source>
        <translation>Aplikace právě zpracovává úlohy.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="781"/>
        <source>Do you want to abort all pending actions and close the application?</source>
        <translation>Přejete si zrušit všechny nedokončené akce a zavřít aplikaci?</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="818"/>
        <source>Open File</source>
        <translation>Otevřít soubor</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="819"/>
        <source>Database file (*.db)</source>
        <translation>Databázový soubor (*.db)</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="836"/>
        <source>Select Directory</source>
        <translation>Vybrat adresář</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="849"/>
        <source>Not a Directory</source>
        <translation>Není adresář</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="850"/>
        <source>The path &apos;%1&apos; isn&apos;t a directory.</source>
        <translation>Cesta &quot;%1&quot; není adresář.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="857"/>
        <source>Not Writeable</source>
        <translation>Není zapisovatelný</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="858"/>
        <source>Directory &apos;%1&apos; cannot be written to.</source>
        <translation>Do adresáře &quot;%1&quot; nelze zapisovat.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="885"/>
        <source>Error:</source>
        <translation>Chyba:</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="923"/>
        <source>Finished.</source>
        <translation>Dokončeno.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="975"/>
        <source>Overwrite File?</source>
        <translation>Přepsat soubor?</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="976"/>
        <source>Do you want to overwrite the file &apos;%1&apos;?</source>
        <translation>Chcete přepsat soubor &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="988"/>
        <source>There isn&apos;t enough space in directory &apos;%1&apos;.</source>
        <translation>V adresáři &quot;%1&quot; není dostatek místa.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="991"/>
        <source>Not Enough Space</source>
        <translation>Nedostatek místa</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1000"/>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1023"/>
        <source>Copied file &apos;%1&apos; to &apos;%2&apos;.</source>
        <translation>Soubor &quot;%1&quot; zkopírován do &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1004"/>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1027"/>
        <source>Cannot copy file &apos;%1&apos; to &apos;%2&apos;.</source>
        <translation>Nemohu zkopírovat soubor &quot;%1&quot; do &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1008"/>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1031"/>
        <source>Copy Error</source>
        <translation>Chyba kopírování</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1015"/>
        <source>Cannot delete file &apos;%1&apos;.</source>
        <translation>Nemohu smazat soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1018"/>
        <source>Deletion Error</source>
        <translation>Chyba mazání</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1048"/>
        <source>Not a File</source>
        <translation>Není souborem</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1049"/>
        <source>&apos;%1&apos; isn&apos;t a file.</source>
        <translation>&quot;%1&quot; není souborem.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1056"/>
        <source>Not Readable</source>
        <translation>Není čitelný</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1057"/>
        <source>File &apos;%1&apos; cannot be read.</source>
        <translation>Soubor &quot;%1&quot; nelze číst.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1067"/>
        <source>Cannot Open Database</source>
        <translation>Nelze otevřít databázi</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1068"/>
        <source>File &apos;%1&apos; cannot be opened as a database.</source>
        <translation>Soubor &quot;%1&quot; nelze otevřít jako databázi.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1075"/>
        <source>Invalid Database Content</source>
        <translation>Neplatný obsah databáze</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="1076"/>
        <source>File &apos;%1&apos; is either not a message database or doesn&apos;t contain a database at all.</source>
        <translation>Soubor &quot;%1&quot; buď neobsahuje databázi datových zpráv nebo neobsahuje databázi vůbec.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1099"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1101"/>
        <source>Powered by</source>
        <translation>Vytvořil</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2371"/>
        <source>If you want to use a new blank file then delete, rename or move the existing file so that the application can create a new empty file.</source>
        <translation>Pokud chcete použít nový prázdný soubor, pak odstraňte, přejmenujte nebo přesuňte stávající soubor, aby aplikace mohla vytvořit nový prázdný soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2381"/>
        <source>I&apos;ll try to create an empty one.</source>
        <translation>Pokusím se vytvořit prázdný.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2369"/>
        <source>The existing database files %1 in &apos;%2&apos; are going to be used.</source>
        <translation>Existující databázové soubory %1 v &apos;%2&apos; budou použity.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2379"/>
        <source>Database files are missing in &apos;%1&apos;.</source>
        <translation>Databázové soubory chybí v &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2389"/>
        <source>Some databases of %1 in &apos;%2&apos; are not a file.</source>
        <translation>Některé databáze %1 v &apos;%2&apos; asi nejsou soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2397"/>
        <source>Some databases of &apos;%1&apos; in &apos;%2&apos; cannot be accessed.</source>
        <translation>Některé databáze %1 v &apos;%2&apos; nelze zpřístupnit.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2399"/>
        <source>You don&apos;t have enough access rights to use the file.</source>
        <translation>Nemáte dostatečná oprávnění k použití souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9771"/>
        <source>Connection to ISDS or user authentication failed!</source>
        <translation>Připojení k serveru datové schránky se nezdařilo nebo selhala autorizace uživatele!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9773"/>
        <source>Please check your internet connection and try again or it is possible that your password (certificate) has expired - in this case, you need to use the official web interface of Datové schránky to change it.</source>
        <translation>Zkontrolujte prosím připojení k internetu a zkuste to znovu. Také je možné, že vaše heslo (certifikát) již vypršelo - v tomto případě musíte použít oficiální webové rozhraní Datové schránky a heslo si změnit.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4556"/>
        <location filename="../src/gui/datovka.cpp" line="4613"/>
        <source>Add ZFO file</source>
        <translation>Vybrat ZFO soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3216"/>
        <location filename="../src/gui/datovka.cpp" line="4556"/>
        <location filename="../src/gui/datovka.cpp" line="4615"/>
        <source>ZFO file (*.zfo)</source>
        <translation>Soubor ZFO (*.zfo)</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7288"/>
        <location filename="../src/gui/datovka.cpp" line="7289"/>
        <source>disk</source>
        <translation>disk</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1095"/>
        <location filename="../src/gui/datovka.cpp" line="1136"/>
        <source>Datovka - Free client for data boxes</source>
        <translation>Datovka - Svobodný klient pro datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="156"/>
        <source>Enter sought expression.</source>
        <translation>Zadete hledaný výraz.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2366"/>
        <source>Database file present</source>
        <translation>Databázový soubor přítomen</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2376"/>
        <location filename="../src/gui/datovka.cpp" line="2386"/>
        <location filename="../src/gui/datovka.cpp" line="2394"/>
        <location filename="../src/gui/datovka.cpp" line="2404"/>
        <location filename="../src/gui/datovka.cpp" line="2414"/>
        <source>Problem loading database</source>
        <translation>Problém otevírání databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2409"/>
        <source>The file either does not contain an SQLite database or the file is corrupted.</source>
        <translation>Soubor buď neobsahuje SQLite databázi nebo je poškozen.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2438"/>
        <source>Database opening error</source>
        <translation>Chyba otevírání databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3044"/>
        <location filename="../src/gui/datovka.cpp" line="3363"/>
        <source>Keep in mind that this operation may take a while. The actual duration depends on the number of messages in the database.</source>
        <translation>Mějte na paměti, že tato akce může chvíli trvat. Délka trvání závisí na počtu zpráv v databázi.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3046"/>
        <source>The progress will be displayed in the status bar.</source>
        <translation>Průběh bude zobrazován ve stavovém řádku.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3062"/>
        <source>No database files selected.</source>
        <translation>Databázové soubory nebyly vybrány.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3172"/>
        <location filename="../src/gui/datovka.cpp" line="4301"/>
        <location filename="../src/gui/datovka.cpp" line="9973"/>
        <location filename="../src/gui/datovka.cpp" line="10190"/>
        <source>Select Directory</source>
        <translation>Vybrat adresář</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3205"/>
        <location filename="../src/gui/datovka.cpp" line="3208"/>
        <source>No ZFO files found in selected directory.</source>
        <translation>V adresáři nebyly nalezeny ZFO soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3207"/>
        <source>No ZFO Files</source>
        <translation>Žádné ZFO soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3220"/>
        <source>No ZFO files selected.</source>
        <translation>Nebyly vybrány ZFO soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3273"/>
        <source>Database Operation Error</source>
        <translation>Chyba funkce databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3304"/>
        <source>Clean Message Database</source>
        <translation>Pročisti databázi zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3335"/>
        <source>Database Clean-Up Successful</source>
        <translation>Pročištění úspěšně dokončeno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3340"/>
        <source>Database Clean-Up Failed</source>
        <translation>Pročištění selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3355"/>
        <source>Split Database</source>
        <translation>Rozdělení databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3359"/>
        <source>The original database file will be copied to a selected directory and new database files will be created in the original location. If the operation finishes successfully then the newly created databases will be used instead of the original one. Application restart will be required.</source>
        <translation>Původní databáze bude zkopírována do vybraného adresáře. Nové databázové soubory budou vytvořeny v původním umístění. Jestliže bude operace rozdělení úspěšná, bude původní databáze nahrazena novými databázemi. Poté bude potřeba restartovat aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3362"/>
        <source>Note:</source>
        <translation>Poznámka:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3391"/>
        <source>Select Directory for New Databases</source>
        <translation>Vyberte adresář pro nové databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3406"/>
        <source>Splitting message database finished with an error.</source>
        <translation>Rozdělení databáze skončilo chybou.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3409"/>
        <source>Database File Error</source>
        <translation>Chyba databazového souboru</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3439"/>
        <source>Splitting message database finished.</source>
        <translation>Rozdělení databáze zpráv skončilo.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3442"/>
        <location filename="../src/gui/datovka.cpp" line="3450"/>
        <source>Database Split Result</source>
        <translation>Výsledek rozdělení databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3962"/>
        <source>Verification Error</source>
        <translation>Chyba verifikace</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3963"/>
        <source>The message hash is not in local database.
Download the complete message from ISDS and try again.</source>
        <translation>Otisk zprávy nebyl nalezen v lokální databázi.
Stáhněte kompletní zprávu ze serveru ISDS a zkuste to znova.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3977"/>
        <source>The ISDS server confirms that the message hash is valid.</source>
        <translation>Server ISDS potvrdil, že otisk zprávy je validní.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3978"/>
        <source>Message Hash Valid</source>
        <translation>Otisk zprávy je validní</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3979"/>
        <source>The message hash was &lt;b&gt;successfully verified&lt;/b&gt; against data on the ISDS server.</source>
        <translation>Otisk zprávy byl &lt;b&gt;úspěšně ověřen&lt;/b&gt; proti datům na serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3986"/>
        <source>The ISDS server informs that the message hash is not valid.</source>
        <translation>Server ISDS informuje, že otisk zprávy není validní.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3987"/>
        <source>Message Hash Invalid</source>
        <translation>Otisk zprávy není validní</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3988"/>
        <source>The message hash was &lt;b&gt;not&lt;/b&gt; verified by the ISDS.</source>
        <translation>Otisk zprávy &lt;b&gt;nebyl&lt;/b&gt; verifikován systémem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3994"/>
        <location filename="../src/gui/datovka.cpp" line="4003"/>
        <source>Message hash verification failed.</source>
        <translation>Ověření otisku zprávy selhalo.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3995"/>
        <location filename="../src/gui/datovka.cpp" line="4004"/>
        <source>Verification Failed</source>
        <translation>Verifikace selhala</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3996"/>
        <source>Verification of the message hash has been interrupted because the connection to ISDS failed.</source>
        <translation>Ověřování otisku zprávy bylo přerušeno, protože selhalo spojení se systémem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4045"/>
        <location filename="../src/gui/datovka.cpp" line="4111"/>
        <source>Export Error</source>
        <translation>Chyba exportu</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4046"/>
        <source>Cannot export message &apos;%1&apos;.</source>
        <translation>Nelze exportovat zprávu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4047"/>
        <location filename="../src/gui/datovka.cpp" line="4113"/>
        <source>First you must download the complete message.</source>
        <translation>Nejdříve musíte stáhnout kompletní zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4063"/>
        <source>Message &apos;%1&apos; stored into temporary file &apos;%2&apos;.</source>
        <translation>Zpráva &quot;%1&quot; uložena do dočasného souboru &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4069"/>
        <source>Message &apos;%1&apos; couldn&apos;t be stored into temporary file.</source>
        <translation>Zpráva &quot;%1&quot; nemohla být uložena do dočasného souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4112"/>
        <source>Cannot export acceptance information &apos;%1&apos;.</source>
        <translation>Nelze exportovat doručenku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4129"/>
        <source>Acceptance information &apos;%1&apos; stored into temporary file &apos;%2&apos;.</source>
        <translation>Doručenka &quot;%1&quot; uložena do dočasného souboru &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4135"/>
        <source>Acceptance information &apos;%1&apos; couldn&apos;t be stored into temporary file.</source>
        <translation>Doručenka &quot;%1&quot; nemohla být uložena do dočasného souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4332"/>
        <source>Some attachments of the message &apos;%1&apos; could not be saved to target directory &apos;%2&apos;.</source>
        <translation>Některé přílohy zprávy &quot;%1&quot; nebyly uloženy do cílového adresáře &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4339"/>
        <source>There may be a problem with the storage which the files are written to.</source>
        <translation>Na úložišti, do kterého se mají zapsat soubory, může vyskytovat problém.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4341"/>
        <source>Do you want to cancel the remaining operations?</source>
        <translation>Přejete si zrušit zbývající operace?</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/gui/datovka.cpp" line="3801"/>
        <source>The message will be deleted from ISDS in approximately %n day(s).</source>
        <translation>
            <numerusform>Zpráva bude z ISDS smazána přibližně za %n den.</numerusform>
            <numerusform>Zpráva bude z ISDS smazána přibližně za %n dny.</numerusform>
            <numerusform>Zpráva bude z ISDS smazána přibližně za %n dnů.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/gui/datovka.cpp" line="3806"/>
        <source>The message will be deleted from ISDS in approximately %n day(s) because the long term storage is full.</source>
        <translation>
            <numerusform>Zpráva bude z ISDS smazána přibližně za %n den, protože dlouhodobé úložiště je zaplněno.</numerusform>
            <numerusform>Zpráva bude z ISDS smazána přibližně za %n dny, protože dlouhodobé úložiště je zaplněno.</numerusform>
            <numerusform>Zpráva bude z ISDS smazána přibližně za %n dnů, protože dlouhodobé úložiště je zaplněno.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/gui/datovka.cpp" line="3810"/>
        <source>The message will be moved to the long term storage in approximately %n day(s) if the storage is not full.</source>
        <translation>
            <numerusform>Zpráva bude přesunuta do dlouhodobého úložiště přibližně za %n den, pokud není úložiště zaplněno.</numerusform>
            <numerusform>Zpráva bude přesunuta do dlouhodobého úložiště přibližně za %n dny, pokud není úložiště zaplněno.</numerusform>
            <numerusform>Zpráva bude přesunuta do dlouhodobého úložiště přibližně za %n dnů, pokud není úložiště zaplněno.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3813"/>
        <source>The message will be deleted from the ISDS server if the storage is full.</source>
        <translation>Zpráva bude smazána ze serveru ISDS, pokud je úložiště zaplněno.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3817"/>
        <source>The message has already been deleted from the ISDS.</source>
        <translation>Zpráva již byla v ISDS smazána.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6327"/>
        <location filename="../src/gui/datovka.cpp" line="9357"/>
        <source>Missing Message Content</source>
        <translation>Chybí obsah zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4330"/>
        <source>Error Saving Attachments</source>
        <translation>Chyba ukládání příloh</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2137"/>
        <source>Synchronise all data boxes with ISDS server.</source>
        <translation>Synchronizují se všechny schránky ze serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2367"/>
        <source>Database file for data box &apos;%1&apos; already exists.</source>
        <translation>Databázový soubor pro schránku &quot;%1&quot; již existuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2377"/>
        <location filename="../src/gui/datovka.cpp" line="2387"/>
        <location filename="../src/gui/datovka.cpp" line="2395"/>
        <location filename="../src/gui/datovka.cpp" line="2405"/>
        <location filename="../src/gui/datovka.cpp" line="2415"/>
        <location filename="../src/gui/datovka.cpp" line="2439"/>
        <source>Could not load data from the database for data box &apos;%1&apos;.</source>
        <translation>Nemohu načíst data z databázového souboru pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2823"/>
        <location filename="../src/gui/datovka.cpp" line="9707"/>
        <source>Change password of data box &apos;%1&apos;.</source>
        <translation>Změnit heslo schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2839"/>
        <source>Change properties of data box &apos;%1&apos;.</source>
        <translation>Změnit vlastnosti schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2860"/>
        <source>A data box &apos;%1&apos; for the newly entered username &apos;%2&apos; is already present. Cannot change the username to &apos;%2&apos;.</source>
        <translation>Schránka &quot;%1&quot; pro nově zadané uživatelské jméno &quot;%2&quot; již existuje. Nelze změnit uživatelské jméno na &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2869"/>
        <source>Do you want to change the username from &apos;%1&apos; to &apos;%2&apos; for the data box &apos;%3&apos;?</source>
        <translation>Zcete změnit uživatelské jméno z &quot;%1&quot; na &quot;%2&quot; pro schránku &quot;%3&quot;?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2873"/>
        <source>Note: This will also change all related local database names and stored data-box information.</source>
        <translation>Poznámka: Změní se také jména všech odpovídajících databází a uložené informace o schránce.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2908"/>
        <source>Cannot change the username &apos;%1&apos; to &apos;%2&apos; for the data box &apos;%3&apos;.</source>
        <translation>Nelze změnit uživatelské jméno &quot;%1&quot; na &quot;%2&quot; pro schránku &quot;%3&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2971"/>
        <source>Data box &apos;%1&apos; was updated.</source>
        <translation>Schránka &quot;%1&quot; byla aktualizována.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2991"/>
        <source>Data box was moved up.</source>
        <translation>Schránka byla posunuta nahoru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3003"/>
        <source>Data box was moved down.</source>
        <translation>Schránka byla posunuta dolů.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3021"/>
        <source>Change data directory of data box &apos;%1&apos;.</source>
        <translation>Změnit adresář pro data schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3043"/>
        <source>Here you can import messages from selected database files into the current data box.</source>
        <translation>Zde můžete do aktuální schránky importovat zprávy z vybraných databázových souborů.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3240"/>
        <source>There is no data box to import ZFO files into.</source>
        <translation>Není schránka, do které lze importovat ZFO soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3305"/>
        <source>Performs a message database clean-up for the selected data box. This action will block the entire application. The action may take several minutes to be completed. Furthermore, it requires more than %1 of free disk space to successfully proceed.</source>
        <translation>Provede pročištění databáze zpráv ve zvolené schránce. Tato akce zablokuje celou aplikaci. Dokončení akce může trvat několik minut. Akce navíc k úspěšnému dokončení vyžaduje více jak %1 volného místa na disku.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3356"/>
        <source>The following operation splits the message database of the selected data box into several separate databases. Each of which will contain messages related only to a single year. Dividing large databases according to years is recommended because it may improve the performance.</source>
        <translation>Následující operace rozdělí databázi zpráv ve vybrané schránce do několika samostatných databází. Každá bude obsahovat zprávy jen k jedinému roku. Rozdělení velké databáze podle let je doporučeno, protože může zvýšit celkový výkon.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3443"/>
        <source>Message database for data box &apos;%1&apos; was split successfully. Restart the application in order to load the new databases.</source>
        <translation>Databáze zpráv schránky &quot;%1&quot; byla úspěšně rozdělena. Restartujte aplikaci, aby se načetly nové databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3451"/>
        <source>Message database splitting for data box &apos;%1&apos; was not successful. Restart the application in order to reload the original database.</source>
        <translation>Rozdělení databáze zpráv schránky &quot;%1&quot; nebylo úspěšné. Restartujte aplikaci pro načtení původní databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3593"/>
        <source>The message &apos;%1&apos; is in an unaccepted state. It is likely that you won&apos;t be able to download the message without synchronising the data box first.</source>
        <translation>Zpráva &quot;%1&quot; se jeví ve stavu, kdy není doručena. Je možné, že nebudete moct tuto zprávu stáhnout bez předchozí synchronizace schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4420"/>
        <source>Delete Message &apos;%1&apos;</source>
        <translation>Vymazat zprávu &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4421"/>
        <source>Do you want to delete the message &apos;%1&apos;?</source>
        <translation>Chcete smazat zprávu &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4422"/>
        <source>Delete this message also from the ISDS server.</source>
        <translation>Smazat tuto zprávu také ze serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4423"/>
        <location filename="../src/gui/datovka.cpp" line="4429"/>
        <location filename="../src/gui/datovka.cpp" line="9054"/>
        <source>Warning:</source>
        <translation>Upozornění:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4424"/>
        <source>If you delete the message from ISDS then this message will be lost forever.</source>
        <translation>Jestliže smažete zprávu ze serveru ISDS, bude tato zpráva navždy ztracena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4426"/>
        <source>Delete Messages</source>
        <translation>Vymazat zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4427"/>
        <source>Do you want to delete the selected messages?</source>
        <translation>Chcete smazat vybrané zprávy?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4428"/>
        <source>Delete these messages also from the ISDS server.</source>
        <translation>Smazat tyto zprávy také ze serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4430"/>
        <source>If you delete the messages from ISDS then these messages will be lost forever.</source>
        <translation>Jestliže smažete vybrané zprávy ze serveru ISDS, budou tyto zprávy navždy ztraceny.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4561"/>
        <source>Authenticating the ZFO file &apos;%1&apos;.</source>
        <translation>Ověřování ZFO souboru &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4567"/>
        <source>The ISDS server confirms that the message is authentic.</source>
        <translation>Server ISDS potvrdil, že zpráva je autentická.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4576"/>
        <source>The ISDS server informs that the message is not authentic.</source>
        <translation>Server ISDS informuje, že zpráva není autentická.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4578"/>
        <source>The message was &lt;b&gt;not&lt;/b&gt; authenticated by the ISDS.</source>
        <translation>Zpráva &lt;b&gt;nebyla&lt;/b&gt; autentifikována systémem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4732"/>
        <source>Do you want to quit the application and launch the database file repair tool?</source>
        <translation>Přejete si ukončit aplikaci a spustit nástroj pro opravu souborů databází?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4733"/>
        <source>Do you want to quit the application and launch the database file repair tool on the file &apos;%1&apos;?</source>
        <translation>Přejete si ukončit aplikaci a spustit nástroj pro opravu souborů databází na souboru &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5369"/>
        <source>All Received Messages</source>
        <translation>Všechny přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5382"/>
        <source>All Sent Messages</source>
        <translation>Všechny odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6001"/>
        <location filename="../src/gui/datovka.cpp" line="6004"/>
        <source>Open File Location</source>
        <translation>Otevřít umístění souboru</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6014"/>
        <location filename="../src/gui/datovka.cpp" line="6017"/>
        <source>Repair Message Database File</source>
        <translation>Opravit soubor databáze zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6369"/>
        <source>Cannot Write Configuration File</source>
        <translation>Nemohu zapsat konfigurační soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6370"/>
        <source>An error occurred while attempting to save the configuration.</source>
        <translation>Nastala chyba v průběhu pokusu o uložení konfigurace.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9615"/>
        <source>According to the last available information, your password for data box &apos;%1&apos; (login &apos;%2&apos;) expired %3 day(s) ago (%4).</source>
        <translation>Podle nejnovějších dostupných informací Vaše heslo pro schránku &quot;%1&quot; (přihlašovací jméno &quot;%2&quot;) vypršelo před %3 dnem/dny (%4).</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9625"/>
        <source>According to the last available information, your password for data box &apos;%1&apos; (login &apos;%2&apos;) will expire in %3 day(s) (%4).</source>
        <translation>Podle nejnovějších dostupných informací Vaše heslo pro schránku &quot;%1&quot; (přihlašovací jméno &quot;%2&quot;) vyprší za %3 dny/dnů (%4).</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10257"/>
        <source>Import of messages to data box &apos;%1&apos; finished</source>
        <translation>Import zpráv do schránky &quot;%1&quot; skončil</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10268"/>
        <source>Import of messages into data box &apos;%1&apos; finished with result:</source>
        <translation>Import zpráv do schránky &quot;%1&quot; skončil s tímto výsledkem:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3990"/>
        <location filename="../src/gui/datovka.cpp" line="4580"/>
        <source>It is either not a valid ZFO file or it was modified since it was obtained from ISDS.</source>
        <translation>Soubor buď neobsahuje platný ZFO obsah, nebo byl od obdržení ze systému ISDS modifikován.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4586"/>
        <source>Authentication of the message has been interrupted because the connection to ISDS failed.</source>
        <translation>Ověřování zprávy bylo přerušeno, protože selhalo spojení se systémem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3998"/>
        <location filename="../src/gui/datovka.cpp" line="4588"/>
        <source>Check your internet connection.</source>
        <translation>Zkontrolujte Vaše připojení k internetu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4594"/>
        <source>Authentication of the message has been stopped because the message file has a wrong format.</source>
        <translation>Ověřování zprávy bylo zastaveno, protože zpráva má chybný formát.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4005"/>
        <location filename="../src/gui/datovka.cpp" line="4601"/>
        <source>An undefined error occurred.</source>
        <translation>Došlo k neznámé chybě.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4006"/>
        <location filename="../src/gui/datovka.cpp" line="4602"/>
        <source>Try it again later.</source>
        <translation>Zkuste to později.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4138"/>
        <source>Error Opening Acceptance Information &apos;%1&apos;</source>
        <translation>Chyba otevírání doručenky &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4568"/>
        <source>Message Authentic</source>
        <translation>Zpráva je autentická</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4569"/>
        <source>The message was &lt;b&gt;successfully authenticated&lt;/b&gt; against data on the ISDS server.</source>
        <translation>Zpráva byla &lt;b&gt;úspěšně ověřena&lt;/b&gt; proti datům na serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4577"/>
        <source>Message Not Authentic</source>
        <translation>Zpráva není autentická</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4584"/>
        <location filename="../src/gui/datovka.cpp" line="4592"/>
        <location filename="../src/gui/datovka.cpp" line="4599"/>
        <source>Message authentication failed.</source>
        <translation>Ověření zprávy selhalo.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4585"/>
        <location filename="../src/gui/datovka.cpp" line="4593"/>
        <location filename="../src/gui/datovka.cpp" line="4600"/>
        <source>Authentication Failed</source>
        <translation>Autentizace selhala</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7002"/>
        <source>Uploaded to records management service</source>
        <translation>Nahráno do spisové služby</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7292"/>
        <location filename="../src/gui/datovka.cpp" line="7296"/>
        <source>memory</source>
        <translation>paměť</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7299"/>
        <source>Storage:</source>
        <translation>Úložiště:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5311"/>
        <source>Database Access Error</source>
        <translation>Chyba přístupu k databázi</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5891"/>
        <location filename="../src/gui/datovka.cpp" line="5915"/>
        <location filename="../src/gui/datovka.cpp" line="5939"/>
        <location filename="../src/gui/datovka.cpp" line="6136"/>
        <source>As Unsettled</source>
        <translation>Nevyřízeno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5895"/>
        <location filename="../src/gui/datovka.cpp" line="5919"/>
        <location filename="../src/gui/datovka.cpp" line="5943"/>
        <location filename="../src/gui/datovka.cpp" line="6140"/>
        <source>As in Progress</source>
        <translation>Vyřizuje se</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5899"/>
        <location filename="../src/gui/datovka.cpp" line="5923"/>
        <location filename="../src/gui/datovka.cpp" line="5947"/>
        <location filename="../src/gui/datovka.cpp" line="6144"/>
        <source>As Settled</source>
        <translation>Vyřízeno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7954"/>
        <source>Cannot create message</source>
        <translation>Nelze vytvořit zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8070"/>
        <source>ISDS</source>
        <translation>ISDS</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8143"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3592"/>
        <source>Downloading Message &apos;%1&apos;.</source>
        <translation>Stažení zprávy &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2087"/>
        <location filename="../src/gui/datovka.cpp" line="2105"/>
        <source>Pending Tasks</source>
        <translation>Nedokončené úlohy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2088"/>
        <location filename="../src/gui/datovka.cpp" line="2106"/>
        <source>The application is currently processing some tasks.</source>
        <translation>Aplikace právě zpracovává úlohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2090"/>
        <location filename="../src/gui/datovka.cpp" line="2107"/>
        <source>Do you want to abort all pending actions and close the application?</source>
        <translation>Přejete si zrušit všechny nedokončené akce a zavřít aplikaci?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2091"/>
        <source>Do you want to abort all pending actions and continue with this operation?</source>
        <translation>Přejete si zrušit všechny nedokončené akce a pokračovat touto operací?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2859"/>
        <source>Username Already Present</source>
        <translation>Uživatelské jméno již existuje</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2868"/>
        <source>Username Change</source>
        <translation>Změna uživatelského jména</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2907"/>
        <source>Data Box Mismatch</source>
        <translation>Neshoda datových schránek</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2912"/>
        <source>The old username was used to access the data box &apos;%1&apos; whereas the new username accesses the data box &apos;%2&apos;. You cannot change the username if it accesses a different data box.</source>
        <translation>Staré uživatelské jméno bylo používáno k přístupu do datové schránky &quot;%1&quot;, zatímco nové uživatelské jméno přistupuje ke schránce &quot;%2&quot;. Nelze změnit uživatelské jméno, pokud přistupuje k jiné datové schránce.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2930"/>
        <source>Operation Cancelled</source>
        <translation>Operace zrušena</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2931"/>
        <source>The operation has been cancelled.</source>
        <translation>Operace byla zrušena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2963"/>
        <source>Restart Needed</source>
        <translation>Vyžadován restart</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2964"/>
        <source>The username has been successfully changed. The application is going to be closed. Then you may start it again.</source>
        <translation>Uživatelské jméno bylo úspěšně změněno. Aplikace bude ukončena. Můžete ji pak znovu spustit.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2508"/>
        <location filename="../src/gui/datovka.cpp" line="2511"/>
        <source>Data restoration cannot be performed on databases in memory.</source>
        <translation>Obnovování dat nemůže být provedeno na databázích v paměti.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9517"/>
        <source>First you must download the complete message to continue with the action.</source>
        <translation>Před pokračováním akce musíte nejdříve stáhnout úplnou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9629"/>
        <source>You can change your password now or later using the &apos;%1&apos; command. Your new password will be valid for 90 days.

Change password now?</source>
        <translation>Heslo můžete změnit nyní nebo později pomocí příkazu &apos;%1&apos;. Nové heslo bude platné po dobu 90 dnů.

Změnit heslo nyní?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1349"/>
        <location filename="../src/gui/datovka.cpp" line="1240"/>
        <source>In Progress</source>
        <translation>Vyřizuje se</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5316"/>
        <source>The file cannot be accessed or is corrupted. Please fix the access privileges or remove or rename the file so that the application can create a new empty file.</source>
        <translation>Nelze přistoupik k souboru nebo je soubor poškozen. Opravte prosím přístupová oprávnění nebo odstraňte nebo přejmenujte soubor, aby aplikace mohla vytvořit nový prázdný soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5319"/>
        <source>Create a backup copy of the affected file. This will help when trying to perform data recovery.</source>
        <translation>Vytvořte si kopii postiženého souboru. Může to pomoct při případném obnovování dat.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5322"/>
        <source>In general, it is recommended to create backup copies of the database files to prevent data loss.</source>
        <translation>Všeobecně je doporučováno vyrábět záložní kopie databázových souborů aby se zamezilo ztrátě dat.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5874"/>
        <location filename="../src/gui/datovka.cpp" line="6121"/>
        <source>Mark</source>
        <translation>Označit</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5881"/>
        <location filename="../src/gui/datovka.cpp" line="5905"/>
        <location filename="../src/gui/datovka.cpp" line="5929"/>
        <location filename="../src/gui/datovka.cpp" line="6126"/>
        <source>As Read</source>
        <translation>Přečtené</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5885"/>
        <location filename="../src/gui/datovka.cpp" line="5909"/>
        <location filename="../src/gui/datovka.cpp" line="5933"/>
        <location filename="../src/gui/datovka.cpp" line="6130"/>
        <source>As Unread</source>
        <translation>Nepřečtené</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4073"/>
        <location filename="../src/gui/datovka.cpp" line="4139"/>
        <location filename="../src/gui/datovka.cpp" line="6372"/>
        <source>Cannot write file &apos;%1&apos;.</source>
        <translation>Nemohu uložit soubor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8146"/>
        <source>Download message list error</source>
        <translation>Chyba stahování seznamu zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3981"/>
        <location filename="../src/gui/datovka.cpp" line="4571"/>
        <source>This message has passed through the ISDS system and has not been tampered with since.</source>
        <translation>Tato zpráva prošla systémem ISDS a nebyla od té doby upravena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3233"/>
        <source>No ZFO files to import.</source>
        <translation>Žádné ZFO soubory k importování.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9516"/>
        <source>Complete message &apos;%1&apos; is missing.</source>
        <translation>Chybí úplná zpráva &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9519"/>
        <source>Do you want to download the complete message now?</source>
        <translation>Chcete nyní stáhnout úplnou zprávu?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9524"/>
        <source>Complete message &apos;%1&apos; has been downloaded.</source>
        <translation>Úplná zpráva &apos;%1&apos; byla stažena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9529"/>
        <source>Complete message &apos;%1&apos; has not been downloaded.</source>
        <translation>Úplná zpráva &apos;%1&apos; nebyla stažena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9552"/>
        <source>Attachment of message %1</source>
        <translation>Příloha zprávy %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9552"/>
        <source>Attachments of message %1</source>
        <translation>Přílohy zprávy %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3047"/>
        <location filename="../src/gui/datovka.cpp" line="3310"/>
        <location filename="../src/gui/datovka.cpp" line="3365"/>
        <source>Do you want to continue?</source>
        <translation>Chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3058"/>
        <source>DB file (*.db)</source>
        <translation>DB soubor (*.db)</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10267"/>
        <source>Messages import result</source>
        <translation>Výsledek importu zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10271"/>
        <source>Imported messages: %1</source>
        <translation>Importováno zpráv: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10272"/>
        <source>Non-imported messages: %1</source>
        <translation>Ignorováno zpráv: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3410"/>
        <source>Database file cannot be split into original directory.</source>
        <translation>Databázový soubor nelze rozdělit do původní složky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3411"/>
        <source>Please choose another directory.</source>
        <translation>Prosím, vyberte jiný adresář.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3446"/>
        <source>Note: Original database file was backed up to:</source>
        <translation>Původní databáze byla přesunuta do:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3270"/>
        <source>Vacuum cannot be performed on databases in memory.</source>
        <translation>Vakuum nelze aplikovat na databáze v paměti.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2510"/>
        <source>Database operation error</source>
        <translation>Chyba funkce databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3274"/>
        <source>Database clean-up cannot be performed on database in memory.</source>
        <translation>Pročištění databáze nelze provést s databází v paměti.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3275"/>
        <source>Cannot call VACUUM on database in memory.</source>
        <translation>Nelze volat VACUUM na databázi v paměti.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3317"/>
        <source>Performing database clean-up.</source>
        <translation>Provádím pročišťování databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3329"/>
        <source>Database clean-up finished.</source>
        <translation>Počištění databáze dokončeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3336"/>
        <source>The database clean-up has finished successfully.</source>
        <translation>Pročištění databáze bylo úspěšně dokončeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3341"/>
        <source>The database clean-up failed with error message: %1</source>
        <translation>Pročišťování databáze selhalo s chybovým hlášením: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5313"/>
        <source>Database files for data box &apos;%1&apos; cannot be accessed in location &apos;%2&apos;.</source>
        <translation>Databázové soubory pro schránku &quot;%1&quot; nelze zpřístupnit v umístění &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6471"/>
        <source>Message &apos;%1&apos; has been downloaded from server.</source>
        <translation>Zpráva &quot;%1&quot; byla stažena ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7890"/>
        <location filename="../src/gui/datovka.cpp" line="7931"/>
        <source>Saving attachment of message &apos;%1&apos; into a file was not successful.</source>
        <translation>Nepodařilo se uložit přílohu zprávy &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7923"/>
        <source>Saving attachment of message &apos;%1&apos; into a file was successful.</source>
        <translation>Příloha zprávy &quot;%1&quot; úspěšně uložena do souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8073"/>
        <source>Download message error</source>
        <translation>Chyba stahování zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8091"/>
        <source>Message &apos;%1&apos; could not be uploaded</source>
        <translation>Zpráva &apos;%1&apos; nemohla být nahrána</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8092"/>
        <source>Message &apos;%1&apos; could not be uploaded into records management service.</source>
        <translation>Zpráva &quot;%1&quot; nemohla být nahrána do spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8094"/>
        <source>The hierarchy location could not be determined because multiple possible targets have been detected.</source>
        <translation>Umístění v hierarchii nemohlo být jednoznačně určeno, protože bylo zjištěno více možných cílů.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8137"/>
        <source>It was not possible to download the received message list from the server.</source>
        <translation>Nebylo možné stáhnout seznam přijatých zpráv ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8138"/>
        <source>It was not possible to download the sent message list from the server.</source>
        <translation>Nebylo možné stáhnout seznam odeslaných zpráv ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8163"/>
        <source>%1 delivered at %2</source>
        <translation>%1 dodaná %2</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8191"/>
        <source>The following database files seem to have problems recalling written data:</source>
        <translation>Z následujících databázových souborů zřejmě nelze správně číst zapsaná data:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8222"/>
        <source>These messages are not being recalled from the databases:</source>
        <translation>Následující zprávy se nedaří vypsat v seznamech z databází:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8226"/>
        <source>Unlisted Messages</source>
        <translation>Zprávy neuvedené v seznamech</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8318"/>
        <source>Waiting %1 seconds before downloading the sent message.</source>
        <translation>Čekám %1 sekund před stažením odeslané zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8556"/>
        <source>Message &quot;%1&quot; was deleted from local database.</source>
        <translation>Zpráva &quot;%1&quot; byla smazána z místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8566"/>
        <source>Message &quot;%1&quot; was deleted from ISDS and local database.</source>
        <translation>Zpráva &quot;%1&quot; byla smazána z ISDS i z místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8539"/>
        <source>Message &quot;%1&quot; was deleted only from ISDS.</source>
        <translation>Zpráva &quot;%1&quot; byla smazána pouze z ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8549"/>
        <source>Message &quot;%1&quot; was deleted only from local database.</source>
        <translation>Zpráva &quot;%1&quot; byla smazána pouze z místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8532"/>
        <source>Message &quot;%1&quot; was not deleted.</source>
        <translation>Zpráva &quot;%1&quot; nebyla smazána.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8597"/>
        <source>Messages on the server</source>
        <translation>Zprávy na serveru</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8598"/>
        <source>received</source>
        <translation>přijato</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8599"/>
        <location filename="../src/gui/datovka.cpp" line="8601"/>
        <source>new</source>
        <translation>nových</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8600"/>
        <source>sent</source>
        <translation>odesláno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3961"/>
        <source>Message verification failed.</source>
        <translation>Verifikace zprávy selhala.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10002"/>
        <location filename="../src/gui/datovka.cpp" line="10006"/>
        <source>ZFO file(s) not found in selected directory.</source>
        <translation>V adresáři nebyly nalezeny ZFO soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10005"/>
        <source>No ZFO file(s)</source>
        <translation>Žádné ZFO soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4072"/>
        <source>Error opening message &apos;%1&apos;.</source>
        <translation>Chyba při otevírání zprávy &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9614"/>
        <location filename="../src/gui/datovka.cpp" line="9624"/>
        <source>Password expiration</source>
        <translation>Expirace hesla</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8835"/>
        <source>Create and send a message.</source>
        <translation>Vytvoř a odešli zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7004"/>
        <source>Tags</source>
        <translation>Tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3056"/>
        <source>Select Database Files</source>
        <translation>Vybrat databázové soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3214"/>
        <source>Select ZFO Files</source>
        <translation>Vybrat ZFO soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7004"/>
        <source>User-assigned tags</source>
        <translation>Uživatelem přiřazené tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4388"/>
        <source>Attachment &apos;%1&apos; stored into temporary file &apos;%2&apos;.</source>
        <translation>Příloha &apos;%1&apos; uložena do dočasného souboru &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4392"/>
        <source>Attachment &apos;%1&apos; couldn&apos;t be stored into temporary file.</source>
        <translation>Příloha &apos;%1&apos; nemohla být uložena do dočasného souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8071"/>
        <source>A connection error occurred or the message has already been deleted from the server.</source>
        <translation>Nastala chyba spojení, nebo zpráva již byla smazána ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8081"/>
        <source>Couldn&apos;t download message &apos;%1&apos;.</source>
        <translation>Nešlo stáhnout zprávu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8144"/>
        <source>A connection error occurred.</source>
        <translation>Nastala chyba spojení.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8288"/>
        <source>Message from &apos;%1&apos; (%2) has been successfully sent to &apos;%3&apos; (%4).</source>
        <translation>Zpráva od &apos;%1&apos; (%2) byla úspěšně odeslána adresátu &apos;%3&apos; (%4).</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8296"/>
        <source>Error while sending message from &apos;%1&apos; (%2) to &apos;%3&apos; (%4).</source>
        <translation>Chyba během odesílání zprávy od &apos;%1&apos; (%2) adresátu &apos;%3&apos; (%4).</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2407"/>
        <source>Some databases of %1 in &apos;%2&apos; cannot be used.</source>
        <translation>Některé databáze %1 v &apos;%2&apos; nelze použít.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2417"/>
        <source>Conflicting databases %1 in &apos;%2&apos; cannot be used.</source>
        <translation>Konfliktní databáze %1 v &apos;%2&apos; nelze použít.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2419"/>
        <source>Please remove the conflicting files.</source>
        <translation>Prosím, odstraňte konfliktní databázové soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2441"/>
        <source>Database files in &apos;%1&apos; cannot be created or are corrupted.</source>
        <translation>Databázové soubory v &apos;%1&apos; nelze vytvořit nebo jsou poškozené.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1097"/>
        <location filename="../src/gui/datovka.cpp" line="1138"/>
        <source>Portable version</source>
        <translation>Přenosná verze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9619"/>
        <source>You have to change your password from the ISDS web interface. Your new password will be valid for 90 days.</source>
        <translation>Heslo si změňte ve webovém rozhraní ISDS. Vaše nové heslo bude platné 90 dnů.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10176"/>
        <source>Time stamp expiration check results</source>
        <translation>Výsledky kontroly expirace časových razítek</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9053"/>
        <source>Delete also message database from storage.</source>
        <translation>Odstranit také databázi zpráv z disku.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9055"/>
        <source>If you delete the message database then all locally accessible messages that are not stored on the ISDS server will be lost.</source>
        <translation>Jestliže smažete databázi zpráv, potom všechny zprávy, které nejsou uloženy na serveru ISDS, budou ztraceny.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8065"/>
        <location filename="../src/gui/datovka.cpp" line="8074"/>
        <source>It was not possible to download the complete message &apos;%1&apos; from the ISDS server.</source>
        <translation>Nebylo možné stáhnout celou zprávu &quot;%1&quot; ze serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4495"/>
        <source>Find data boxes from data box &apos;%1&apos;.</source>
        <translation>Vyhledat datové schránky ze schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4730"/>
        <source>Quit the Application?</source>
        <translation>Ukončit aplikaci?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7955"/>
        <source>Create a data box first before trying to create and send a message.</source>
        <translation>Nejprve založte schránku, než se pokusíte odeslat zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8178"/>
        <source>Received messages that are not being listed have been detected in the data box &apos;%1&apos;.</source>
        <translation>Ve schránce „%1“ byly zjištěny přijaté zprávy, které nejsou zobrazovány v seznamech.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8181"/>
        <source>Sent messages that are not being listed have been detected in the data box &apos;%1&apos;.</source>
        <translation>Ve schránce „%1“ byly zjištěny odeslané zprávy, které nejsou zobrazovány v seznamech.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8184"/>
        <source>Messages that are not being listed have been detected in the data box &apos;%1&apos;.</source>
        <translation>Ve schránce „%1“ byly zjištěny zprávy, které nejsou zobrazovány v seznamech.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8916"/>
        <source>No data box synchronised.</source>
        <translation>Nebyla synchronizována žádná schránka.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9050"/>
        <source>Remove Data Box &apos;%1&apos;</source>
        <translation>Odstranit schránku &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9051"/>
        <source>Do you want to remove the data box &apos;%1&apos; (%2)?</source>
        <translation>Chcete opravdu odstranit schránku &quot;%1&quot; (%2)?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9073"/>
        <source>Data box &apos;%1&apos; was deleted together with message database file.</source>
        <translation>Schránka &quot;%1&quot; byla smazána společně s její databází zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9077"/>
        <source>Data box &apos;%1&apos; was deleted but its message database was not deleted.</source>
        <translation>Schránka &quot;%1&quot; byla smazána, ale její databázi zpráv se smazat nepodařilo.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9083"/>
        <source>Data box &apos;%1&apos; was deleted.</source>
        <translation>Schránka &quot;%1&quot; byla smazána.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9764"/>
        <source>New data box error</source>
        <translation>Chyba nové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9767"/>
        <source>It was not possible to get user info and databox info from ISDS server for data box</source>
        <translation>Nebylo možné získat informace o uživateli a jeho datové schránce ze serveru ISDS pro schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9779"/>
        <source>Data box</source>
        <translation>Schránka</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9810"/>
        <location filename="../src/gui/datovka.cpp" line="9816"/>
        <source>Adding new data box failed</source>
        <translation>Přidávání nové schránky selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9811"/>
        <source>Data box could not be added because an error occurred.</source>
        <translation>Schránka nemohla být přidána, protože nastala chyba.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9817"/>
        <source>Data box could not be added because data box already exists.</source>
        <translation>Schránka nemohla být přidána, protože schránka již existuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9951"/>
        <location filename="../src/gui/datovka.cpp" line="9962"/>
        <source>Checking time stamps in data box &apos;%1&apos;...</source>
        <translation>Kontrola časových razítek ve schránce &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10069"/>
        <source>Time stamp expiration check of ZFO files finished with result:</source>
        <translation>Kontrola časových razítek ZFO souborů skončila s výsledkem:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10071"/>
        <source>Total of ZFO files: %1</source>
        <translation>Celkem ZFO souborů: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10073"/>
        <source>ZFO files with time stamp expiring within %1 days: %2</source>
        <translation>ZFO souborů s časovým razítkem expirujícím do %1 dnů: %2</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10076"/>
        <source>Unchecked ZFO files: %1</source>
        <translation>Nezkontrolovaných ZFO souborů: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10109"/>
        <source>Time stamp expiration check in data box &apos;%1&apos; finished with result:</source>
        <translation>Kontrola časových razítek ve schránce &quot;%1&quot; skončila s výsledkem:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10113"/>
        <location filename="../src/gui/datovka.cpp" line="10270"/>
        <source>Total of messages in database: %1</source>
        <translation>Celkem zpráv v databázi: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10115"/>
        <source>Messages with time stamp expiring within %1 days: %2</source>
        <translation>Zpráv s časovým razítkem expirujícím do %1 dnů: %2</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10118"/>
        <source>Unchecked messages: %1</source>
        <translation>Nezkontrolovaných zpráv: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10128"/>
        <source>See details for more info...</source>
        <translation>Více informací naleznete v podrobnostech...</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10131"/>
        <source>Do you want to export the expiring messages to ZFO?</source>
        <translation>Chcete exportovat expirující zprávy do ZFO?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10137"/>
        <location filename="../src/gui/datovka.cpp" line="10153"/>
        <source>Time stamp of message %1 expires within specified interval.</source>
        <translation>Časové razítko zprávy %1 expiruje do určeného limitu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="10145"/>
        <location filename="../src/gui/datovka.cpp" line="10161"/>
        <source>Time stamp of message %1 is not present.</source>
        <translation>Časové razítko zprávy %1 není k dispozici.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="21"/>
        <source>Datovka</source>
        <translation>Datovka</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9782"/>
        <source>was not created!</source>
        <translation>nebyl vytvořen!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1192"/>
        <source>Welcome...</source>
        <translation>Vítejte...</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1198"/>
        <source>Storage: disk | disk</source>
        <translation>Úložiště: disk | disk</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="240"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="244"/>
        <source>Records Management</source>
        <translation>Spisová služba</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="275"/>
        <source>Data Box</source>
        <translation>Schránka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="279"/>
        <source>Mark All Received as</source>
        <translation>Označit všechny přijaté jako</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="290"/>
        <location filename="../src/gui/datovka.cpp" line="5865"/>
        <source>Synchronisation</source>
        <translation>Synchronizace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="322"/>
        <source>Message</source>
        <translation>Zpráva</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="326"/>
        <location filename="../src/gui/datovka.cpp" line="6112"/>
        <source>E-mail with</source>
        <translation>E-mail s</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="361"/>
        <source>Tools</source>
        <translation>Nástroje</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="365"/>
        <source>Tool Bars</source>
        <translation>Panely nástrojů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="399"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="420"/>
        <source>toolBar</source>
        <translation>Panel nástrojů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="497"/>
        <source>Proxy Settings</source>
        <translation>Nastavení proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="500"/>
        <source>View proxy settings.</source>
        <translation>Zobrazit nastavení proxy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="515"/>
        <source>View application prefererences.</source>
        <translation>Zobrazit nastavení aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="611"/>
        <source>Create a Message</source>
        <translation>Vytvořit zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="614"/>
        <source>Create and send a new data message.</source>
        <translation>Vytvořit a poslat novou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="629"/>
        <source>Change Password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="690"/>
        <source>Change Data Directory</source>
        <translation>Změnit adresář pro uložení dat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="705"/>
        <source>About the Application</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="708"/>
        <source>Show information about the application.</source>
        <translation>Zobrazit informace o aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="726"/>
        <source>Open the user manual webpage.</source>
        <translation>Otevřít webovou stránku s uživatelským návodem.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="741"/>
        <source>Search for data box on the ISDS server.</source>
        <translation>Hledat datovou schránku na serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="753"/>
        <source>Authenticate Message from ZFO File</source>
        <translation>Ověřit zprávu ze souboru ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="768"/>
        <source>View Message from ZFO File</source>
        <translation>Zobrazit zprávu ze souboru ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="771"/>
        <source>View content of a ZFO file.</source>
        <translation>Zobrazit obsah ZFO souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="783"/>
        <source>Export Correspondence Overview</source>
        <translation>Exportovat přehled korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="786"/>
        <source>Create a correspondence overview.</source>
        <translation>Vytvořit přehled korespondence.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="798"/>
        <source>Download Signed Message</source>
        <translation>Stáhnout podepsanou zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="801"/>
        <source>Download the complete message, i.e. including attachments, and verify its signature.</source>
        <translation>Stáhnout kompletní zprávu, tj. včetně příloh, a ověřit její podpis.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="813"/>
        <source>Reply to Message</source>
        <translation>Odpovědět na zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="816"/>
        <source>Reply to the selected message.</source>
        <translation>Odpovědět na vybranou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="831"/>
        <source>Signature Detail</source>
        <translation>Detail podpisu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="861"/>
        <source>Delete Message</source>
        <translation>Vymazat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="864"/>
        <source>Delete message from local database and/or from ISDS server.</source>
        <translation>Vymazat zprávu z lokální databáze a/nebo ze serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="872"/>
        <source>Export Message as ZFO</source>
        <translation>Exportovat zprávu jako soubor ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="875"/>
        <source>Export the selected message as a ZFO file.</source>
        <translation>Exportovat vybranou zprávu jako ZFO soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="883"/>
        <source>Open Message Externally</source>
        <translation>Otevřít zprávu externí aplikací</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="886"/>
        <source>Pass the selected message to an external application.</source>
        <translation>Předat vybranou zprávu externí aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="894"/>
        <source>Open Acceptance Info Externally</source>
        <translation>Otevřít doručenku externí aplikací</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="897"/>
        <source>Pass the acceptance information of the selected message to an external application.</source>
        <translation>Předat doručenku vybrané zprávy externí aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="905"/>
        <source>Export Acceptance Info as ZFO</source>
        <translation>Exportovat doručenku do ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="908"/>
        <source>Export the acceptance information of the selected message as a ZFO file.</source>
        <translation>Exportovat doručenku vybrané zprávy jako ZFO soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="916"/>
        <source>Export Acceptance Info as PDF</source>
        <translation>Exportovat doručenku do PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="919"/>
        <source>Export the content of the acceptance information
of the selected message into a PDF file.</source>
        <translation>Exportovat obsah doručenky
vybrané zprávy do PDF souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="928"/>
        <source>Export Message Envelope as PDF</source>
        <translation>Exportovat obálku zprávy do PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="940"/>
        <source>Export Envelope PDF with Attachments</source>
        <translation>Exportovat obálku jako PDF s přílohami</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="957"/>
        <source>Open Attachment</source>
        <translation>Otevřít přílohu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="960"/>
        <source>Open attachment in an associated application.</source>
        <translation>Otevřít přílohu v přiřazené aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="972"/>
        <source>Save Attachment</source>
        <translation>Uložit přílohu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="975"/>
        <source>Save selected attachments to files.</source>
        <translation>Uložit vybrané přílohy do souborů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="987"/>
        <source>Save All Attachments</source>
        <translation>Uložit vše</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="990"/>
        <source>Save all attachments of the selected message.</source>
        <translation>Uložit všechny přílohy vybrané zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1002"/>
        <source>Import Messages from ZFO Files</source>
        <translation>Importovat zprávy ze souborů ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1005"/>
        <source>Import a message from a ZFO file into the database.</source>
        <translation>Importovat zprávu se souboru ZFO do databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1017"/>
        <source>Use Message as Template</source>
        <translation>Použít zprávu jako šablonu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1020"/>
        <source>Use the selected message as a template.</source>
        <translation>Použít vybranou zprávu jako šablonu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1032"/>
        <source>Search Message</source>
        <translation>Vyhledat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1035"/>
        <source>Search in message envelopes.</source>
        <translation>Hledat v obálkách zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1050"/>
        <source>Time Stamp Expiration Check</source>
        <translation>Zkontrolovat expirace časových razítek</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1053"/>
        <source>Check whether the message time stamp is not expired or expiring.</source>
        <translation>Oveřit, zda časové razítko zprávy neexpirovalo nebo neexpiruje.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1068"/>
        <source>Open the application homepage.</source>
        <translation>Otevřít domovskou stránku aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1080"/>
        <location filename="../src/gui/datovka.cpp" line="3042"/>
        <source>Import Messages from Database</source>
        <translation>Importovat zprávy z databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1083"/>
        <source>Import messages into database from an external database file.</source>
        <translation>Importovat zprávy do databáze z externího databázového souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1095"/>
        <source>Split Database According to Years</source>
        <translation>Rozdělit databázi podle roků</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1098"/>
        <source>Messages are going to be stored into separate database files according to years.</source>
        <translation>Zprávy budou ukládány do databázových souborů rozdělených podle let.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1106"/>
        <source>Selected Content</source>
        <translation>Vybraným obsahem</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1109"/>
        <source>Creates an e-mail containing selected content.</source>
        <translation>Vytvořit e-mail s vybraným obsahem.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1132"/>
        <source>Edit Available Tags</source>
        <translation>Editovat dostupné tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1135"/>
        <source>Manage all available tags.</source>
        <translation>Spravovat všechny dostupné tagy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1248"/>
        <source>Back Up Data-Box Data</source>
        <translation>Zálohovat data schránek</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1251"/>
        <source>Back up data-box data.</source>
        <translation>Zálohovat data schránek.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1263"/>
        <source>Restore Data-Box Data</source>
        <translation>Obnovit data schránek</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1266"/>
        <source>Restore data box data.</source>
        <translation>Obnovit data schránek.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1289"/>
        <source>Data-Box Settings</source>
        <translation>Nastavení schránek</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1292"/>
        <source>View all data-box settings.</source>
        <translation>Zobrazit nastavení všech schránek.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1375"/>
        <source>Message ZFOs</source>
        <translation>ZFO zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1378"/>
        <source>Creates an e-mail containing ZFOs of selected messages.</source>
        <translation>Vytvořit e-mail obsahující ZFO vybraných zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1386"/>
        <source>All Attachments</source>
        <translation>Všemi přílohami</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1389"/>
        <source>Creates an e-mail containing all attachments of selected messages.</source>
        <translation>Vytvořit e-mail obsahující všechny přílohy vybraných zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1401"/>
        <source>Check Uploaded Messages In Records Management</source>
        <translation>Ověřit nahrané zprávy ve spisové službě</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1404"/>
        <source>Check messages for presence in the records management</source>
        <translation>Oveřit zprávy na existenci ve spisové službě</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1416"/>
        <source>Convert and Import Data from Mobile App</source>
        <translation>Konvertovat a importovat data z mobilní aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1419"/>
        <source>Loads data from mobile application backup or transfer.
These data can then be converted and imported into the desktop application.</source>
        <translation>Načte data ze zálohy nebo transferu z mobilní aplikace.
Tato data mohou být převedena a importována do desktopové aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1432"/>
        <source>Tag Storage Settings</source>
        <translation>Nastavení úložiště tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1435"/>
        <source>Set where to store tag data.</source>
        <translation>Nastavit, kam se mají ukládat data tagů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1443"/>
        <source>Customise Tool Bar</source>
        <translation>Přizpůsobit panel nástrojů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1451"/>
        <source>Customise Attachment Bar</source>
        <translation>Přizpůsobit panel příloh</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1459"/>
        <source>Customise Message Detail Bar</source>
        <translation>Přizpůsobit panel detailu zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1467"/>
        <source>Customise Message Bar</source>
        <translation>Přizpůsobit panel zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1475"/>
        <source>Customise Data-Box Bar</source>
        <translation>Přizpůsobit panel schránek</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1487"/>
        <source>Filter Messages</source>
        <translation>Filtrovat zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1490"/>
        <source>Filter message list.</source>
        <translation>Filtrovat seznam zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1501"/>
        <source>Tool Bar</source>
        <translation>Panel nástrojů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1512"/>
        <source>Top Panel Bars</source>
        <translation>Panely nad seznamy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1524"/>
        <source>Edit Tag Assignment</source>
        <translation>Editovat přiřazení tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1527"/>
        <source>Manage assignment of tags to messages.</source>
        <translation>Spravovat přiřazení tagů ke zprávám.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1535"/>
        <source>Repair Message Database Files</source>
        <translation>Opravit soubory databází zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1538"/>
        <source>Launches a repair repair tool in order to attempt
to recover data from message database files.</source>
        <translation>Spustí opravný nástroj a pokusí se o obnovení
dat ze souborů databází datových zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="445"/>
        <source>Synchronise All Data Boxes</source>
        <translation>Synchronizovat schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="448"/>
        <source>Synchronise all of those data boxes which have been set
to be synchronised when this action is triggered.</source>
        <translation>Synchronizuje všechny takové schránky, které mají nastaveno,
že se mají synchronizovat, když je tato akce vyvolána.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="464"/>
        <source>Add a New Data Box</source>
        <translation>Přidat novou schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="467"/>
        <location filename="../src/gui/datovka.cpp" line="2146"/>
        <source>Add a new data box.</source>
        <translation>Přidat novou schránku.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="479"/>
        <source>Remove Data Box</source>
        <translation>Odstranit schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="482"/>
        <source>Remove selected data box.</source>
        <translation>Odstranit vybranou schránku.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="545"/>
        <source>Create Data Box from Database</source>
        <translation>Vytvořit schránku z databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="548"/>
        <source>Create a data box from supplied message database.</source>
        <translation>Vytvořit schránku ze souboru databáze zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="560"/>
        <source>Synchronise Data Box</source>
        <translation>Synchronizovat schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="563"/>
        <source>Synchronise the received and sent message lists of the selected data box.
The synchronisation causes the acceptance of delivered messages.</source>
        <translation>Synchronizuje seznam přijatých a odeslaných zpráv vybrané schránky.
Synchronizace způsobí doručení dodaných zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="579"/>
        <source>Synchronise Data Box - Received Messages Only</source>
        <translation>Synchronizovat schránku - jen přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="582"/>
        <source>Synchronise only the received message list of the selected data box.
The synchronisation causes the acceptance of delivered messages.</source>
        <translation>Synchronizuje jen seznam přijatých zpráv vybrané schránky.
Synchronizace způsobí doručení dodaných zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="595"/>
        <source>Synchronise Data Box - Sent Messages Only</source>
        <translation>Synchronizovat schránku - jen odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="598"/>
        <source>Synchronise only the sent message list of the selected data box.
The synchronisation doesn&apos;t cause the acceptance of delivered messages.</source>
        <translation>Synchronizuje jen seznam odeslaných zpráv vybrané schránky.
Synchronizace nezpůsobí doručení dodaných zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="632"/>
        <source>Sets a new password to the selected data box.
The password is going to be modified on the ISDS server.</source>
        <translation>Vybrané schránce nastaví nové heslo.
Heslo bude změněno na serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="645"/>
        <source>Data-Box Properties</source>
        <translation>Nastavení schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="648"/>
        <source>Manage data-box properties.</source>
        <translation>Spravovat nastavení schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="660"/>
        <source>Move Data Box Up</source>
        <translation>Posunout schránku nahoru</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="663"/>
        <source>Move selectet data box one position up.</source>
        <translation>Posunout vybranou schránku jednu pozici nahoru.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="675"/>
        <source>Move Data Box Down</source>
        <translation>Posunout schránku dolů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="678"/>
        <source>Move selectet data box one position down.</source>
        <translation>Posunout vybranou schránku jednu pozici dolů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="693"/>
        <source>Change the directory where data are being stored for the selected data box.</source>
        <translation>Změnit adresář, kam se ukládají data pro vybranou schránku.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="834"/>
        <source>Display details about the digital signature of the selected message.</source>
        <translation>Zobrazit podrobnosti o digitálním podpisu vybrané zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1147"/>
        <source>Vacuum the Message Database</source>
        <translation>Vakuum databáze zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1150"/>
        <source>It may reduce the database file size and optimise the access speed.</source>
        <translation>Může zmenšit velikost databáze a optimalizovat přístupovou rychlost.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1162"/>
        <source>Forward Message</source>
        <translation>Přeposlat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1165"/>
        <source>Forward the message.</source>
        <translation>Přeposlat zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1177"/>
        <source>Records Management Settings</source>
        <translation>Nastavení spisové služby</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1180"/>
        <source>View records management settings.</source>
        <translation>Zobrazit nastavení spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1192"/>
        <source>Send to Records Management</source>
        <translation>Odeslat do spisové služby</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1195"/>
        <source>Send message to records management service.</source>
        <translation>Odeslat zprávu do spisové služby.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1207"/>
        <source>Update Records Management Information</source>
        <translation>Aktualizovat informace ze spisové služby</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1210"/>
        <source>Update records management information about the data messages.</source>
        <translation>Aktualizovat informace spisové služby o datových zprávách.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1218"/>
        <source>View Log</source>
        <translation>Zobrazit log</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1221"/>
        <source>View application log content.</source>
        <translation>Zobrazit obsah logu aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1233"/>
        <source>Send E-Gov Request</source>
        <translation>Odeslat elektronické podání</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1236"/>
        <source>Create and send an e-gov request.</source>
        <translation>Vytvořit a odeslat elektronické podání.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1274"/>
        <source>View All Settings</source>
        <translation>Zobrazit všechny předvolby</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1277"/>
        <source>View internal application settings.</source>
        <translation>Zobrazit interní předvolby aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1367"/>
        <source>Mark all received messages as settled.</source>
        <translation>Označit všechny přijaté jako vyřízené.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="756"/>
        <source>Authenticate the message file on the ISDS server.</source>
        <translation>Ověřit soubor se zprávou serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="846"/>
        <source>Verify Message</source>
        <translation>Ověřit zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="849"/>
        <source>Verify the selected message hash on the ISDS server.</source>
        <translation>Ověřit vybranou zprávu serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="931"/>
        <source>Export content of the envelope
of the selected message as a PDF file.</source>
        <translation>Exportovat obsah obálky
vybrané zprávy do PDF souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="943"/>
        <source>Export the content of the envelope
of the selected message to a PDF file.
Also export message attachments.</source>
        <translation>Exportovat obsah obálky
vybrané zprávy do PDF souboru.
Také exportovat přílohy zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1304"/>
        <source>Read</source>
        <translation>Přečtené</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1307"/>
        <source>Mark all received messages as read.</source>
        <translation>Označit všechny přijaté jako přečtené.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1319"/>
        <source>Unread</source>
        <translation>Nepřečtené</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1322"/>
        <source>Mark all received messages as unread.</source>
        <translation>Označit všechny přijaté jako nepřečtené.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1334"/>
        <location filename="../src/gui/datovka.cpp" line="1237"/>
        <source>Unsettled</source>
        <translation>Nevyřízeno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1337"/>
        <source>Mark all received messages as unsettled.</source>
        <translation>Označit všechny přijaté jako nevyřízené.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1352"/>
        <source>Mark all received messages as in progress.</source>
        <translation>Označit všechny přijaté jako vyřizuje se.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1364"/>
        <location filename="../src/gui/datovka.cpp" line="1243"/>
        <source>Settled</source>
        <translation>Vyřízeno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="452"/>
        <source>Shift+F5</source>
        <translation>Shift+F5</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="159"/>
        <source>Type to filter.</source>
        <translation>Pište hledané.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="169"/>
        <source>Hide filter field.</source>
        <translation>Skrýt pole filtru.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="512"/>
        <source>Preferences</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="527"/>
        <source>Quit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="530"/>
        <source>Quit the application.</source>
        <translation>Ukončit aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="533"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="567"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="617"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="738"/>
        <source>Find Data Box</source>
        <translation>Vyhledat datovou schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1065"/>
        <source>Homepage</source>
        <translation>Domovská stránka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1117"/>
        <source>E-mail with selected attachments</source>
        <translation>E-mail s vybranými přílohami</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1120"/>
        <source>Creates an e-mail containing selected attachments</source>
        <translation>Vytvoří e-mail obsahující vybrané přílohy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1038"/>
        <source>Ctrl+Shift+F</source>
        <translation>Ctrl+Shift+F</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="723"/>
        <source>User manual</source>
        <translation>Uživatelská příručka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="819"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
</context>
<context>
    <name>MessageDb</name>
    <message>
        <location filename="../src/io/message_db.cpp" line="1347"/>
        <location filename="../src/io/message_db.cpp" line="1356"/>
        <location filename="../src/io/message_db.cpp" line="1390"/>
        <source>Invalid</source>
        <translation>Neplatný</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1348"/>
        <source>Message signature and content do not correspond!</source>
        <translation>Podpis zprávy a její obsah si neodpovídají!</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1351"/>
        <location filename="../src/io/message_db.cpp" line="1356"/>
        <location filename="../src/io/message_db.cpp" line="1390"/>
        <source>Valid</source>
        <translation>Platný</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1359"/>
        <source>Certificate revocation check is turned off!</source>
        <translation>Kontrola zneplatnění certifikátu je vypnutá!</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1365"/>
        <location filename="../src/io/message_db.cpp" line="1376"/>
        <source>Not present</source>
        <translation>Není k dispozici</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1366"/>
        <source>Download the complete message in order to verify its signature.</source>
        <translation>Stáhněte kompletní zprávu pro ověření jejího podpisu.</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1402"/>
        <source>Download the complete message in order to verify its time stamp.</source>
        <translation>Stáhněte kompletní zprávu pro ověření jejího časového razítka.</translation>
    </message>
</context>
<context>
    <name>MessageDbSet</name>
    <message>
        <location filename="../src/io/message_db_set.cpp" line="1143"/>
        <location filename="../src/io/message_db_set.cpp" line="1175"/>
        <location filename="../src/io/message_db_set.cpp" line="1182"/>
        <source>File &apos;%1&apos; does not contain a valid database filename.</source>
        <translation>Databázový soubor &apos;%1&apos; neobsahuje správny název.</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_set.cpp" line="1150"/>
        <source>File &apos;%1&apos; does not contain a valid username in the database filename.</source>
        <translation>Databáze &apos;%1&apos; neobsahuje správné uživatelské jméno v názvu souboru.</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_set.cpp" line="1165"/>
        <source>File &apos;%1&apos; does not contain a valid year in the database filename.</source>
        <translation>Databáze &apos;%1&apos; neobsahuje správný rok v názvu souboru.</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_set.cpp" line="1193"/>
        <source>File &apos;%1&apos; does not contain a valid account type flag or filename has wrong format.</source>
        <translation>Databáze &apos;%1&apos; neobsahuje správný typ účtu nebo název souboru má špatný formát.</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_set.cpp" line="1200"/>
        <source>File &apos;%1&apos; does not contain a valid message database or filename has wrong format.</source>
        <translation>Soubor &apos;%1&apos; neobsahuje validní databázi zpráv nebo název souboru má špatný formát.</translation>
    </message>
</context>
<context>
    <name>MessageDownloadThread</name>
    <message>
        <location filename="../src/gui/dlg_download_messages.cpp" line="128"/>
        <source>Cannot execute task.</source>
        <translation>Nelze spustit úlohu.</translation>
    </message>
</context>
<context>
    <name>MsgDownloadModel</name>
    <message>
        <location filename="../src/models/message_download_model.cpp" line="110"/>
        <source>Not attempted</source>
        <translation>Nebylo provedeno</translation>
    </message>
    <message>
        <location filename="../src/models/message_download_model.cpp" line="113"/>
        <source>Failed</source>
        <translation>Selhalo</translation>
    </message>
    <message>
        <location filename="../src/models/message_download_model.cpp" line="116"/>
        <source>Succeeded</source>
        <translation>Úspěšně provedeno</translation>
    </message>
    <message>
        <location filename="../src/models/message_download_model.cpp" line="183"/>
        <source>Message ID</source>
        <translation>ID zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/message_download_model.cpp" line="186"/>
        <source>Downloaded</source>
        <translation>Staženo</translation>
    </message>
    <message>
        <location filename="../src/models/message_download_model.cpp" line="189"/>
        <source>Download Error</source>
        <translation>Chyba stahování</translation>
    </message>
</context>
<context>
    <name>MsgFoundModel</name>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="156"/>
        <source>attachments downloaded</source>
        <translation>přílohy staženy</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="158"/>
        <source>attachments not downloaded</source>
        <translation>přílohy nestaženy</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="142"/>
        <source>Data-box username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="239"/>
        <source>Data Box</source>
        <translation>Schránka</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="242"/>
        <source>Message ID</source>
        <translation>ID zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="245"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="248"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="251"/>
        <source>Recipient</source>
        <translation>Adresát</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="254"/>
        <source>Delivery Time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="257"/>
        <source>Acceptance Time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../src/models/message_found_model.cpp" line="284"/>
        <source>Attachments downloaded</source>
        <translation>Přílohy staženy</translation>
    </message>
</context>
<context>
    <name>MvCrrVbhData</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="91"/>
        <source>Driving licence ID</source>
        <translation>Číslo řidičského oprávnění</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="92"/>
        <source>Enter driving licence ID without spaces</source>
        <translation>Zadejte číslo řidičského průkazu bez mezer</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="102"/>
        <source>Data-box owner name</source>
        <translation>Jméno vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="112"/>
        <source>Data-box owner surname</source>
        <translation>Příjmení vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="122"/>
        <source>Birth date</source>
        <translation>Datum narození</translation>
    </message>
</context>
<context>
    <name>MvIrVpData</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvRtVtData</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="106"/>
        <source>Data-box owner name</source>
        <translation>Jméno vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="116"/>
        <source>Data-box owner surname</source>
        <translation>Příjmení vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="126"/>
        <source>Birth date</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="136"/>
        <source>Birth region</source>
        <translation>Okres (narození)</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="146"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
</context>
<context>
    <name>MvRtpoVtData</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="521"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="522"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvSkdVpData</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="517"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="518"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvVrVpData</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="550"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="551"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvZrVpData</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>PrefsModel</name>
    <message>
        <location filename="../src/models/prefs_model.cpp" line="141"/>
        <source>default</source>
        <translation>výchozí</translation>
    </message>
    <message>
        <location filename="../src/models/prefs_model.cpp" line="144"/>
        <source>modified</source>
        <translation>změněno</translation>
    </message>
    <message>
        <location filename="../src/models/prefs_model.cpp" line="320"/>
        <source>unspecified</source>
        <translation>neuvedeno</translation>
    </message>
    <message>
        <location filename="../src/models/prefs_model.cpp" line="352"/>
        <source>Preference Name</source>
        <translation>Název předvolby</translation>
    </message>
    <message>
        <location filename="../src/models/prefs_model.cpp" line="355"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/models/prefs_model.cpp" line="358"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/models/prefs_model.cpp" line="361"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="393"/>
        <source>Application is loading...</source>
        <translation>Aplikace se načítá...</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="93"/>
        <source>Created using Datovka</source>
        <translation>Vytvořeno Datovkou</translation>
    </message>
    <message>
        <location filename="../src/io/imports.cpp" line="66"/>
        <source>Wrong ZFO format. This file does not contain correct data for import.</source>
        <translation>Špatný formát ZFO. Soubor neobsahuje platná data pro import.</translation>
    </message>
    <message>
        <location filename="../src/io/imports.cpp" line="88"/>
        <source>The selection does not contain any valid ZFO file.</source>
        <translation>Vybraný soubor neobsahuje platný ZFO formát.</translation>
    </message>
</context>
<context>
    <name>QuaGzipFile</name>
    <message>
        <location filename="../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="60"/>
        <source>QIODevice::Append is not supported for GZIP</source>
        <translation>QIODevice::Append není podporován pro GZIP</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="66"/>
        <source>Opening gzip for both reading and writing is not supported</source>
        <translation>Otevření gzip pro čtení a zápis současně není podporováno</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="75"/>
        <source>You can open a gzip either for reading or for writing. Which is it?</source>
        <translation>Gzip můžete otevřít jen pro čtení nebo pro zápis. Co to bude?</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="81"/>
        <source>Could not gzopen() file</source>
        <translation>Nelze provést gzopen() se souborem</translation>
    </message>
</context>
<context>
    <name>QuaZIODevice</name>
    <message>
        <location filename="../src/datovka_shared/3rdparty/quazip-1.4/quazip/quaziodevice.cpp" line="178"/>
        <source>QIODevice::Append is not supported for QuaZIODevice</source>
        <translation>QIODevice::Append není podporován pro QuaZIODevice</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/3rdparty/quazip-1.4/quazip/quaziodevice.cpp" line="183"/>
        <source>QIODevice::ReadWrite is not supported for QuaZIODevice</source>
        <translation>QIODevice::ReadWrite není podporován pro QuaZIODevice</translation>
    </message>
</context>
<context>
    <name>QuaZipFile</name>
    <message>
        <location filename="../src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipfile.cpp" line="251"/>
        <source>ZIP/UNZIP API error %1</source>
        <translation>ZIP/UNZIP API chyba %1</translation>
    </message>
</context>
<context>
    <name>RepairThread</name>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="162"/>
        <source>Missing source message database.</source>
        <translation>Chybí zdrojová databáze datových zpráv.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="168"/>
        <source>Work directory is not specified.</source>
        <translation>Není určen pracovní adresář.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="176"/>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="381"/>
        <source>File &apos;%1&apos; cannot be opened as a database.</source>
        <translation>Soubor &quot;%1&quot; nelze otevřít jako databázi.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="184"/>
        <source>File &apos;%1&apos; is either not a message database or doesn&apos;t contain an SQLite database at all.</source>
        <translation>Soubor &quot;%1&quot; buď neobsahuje databázi datových zpráv nebo neobsahuje SQLite databázi vůbec.</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="197"/>
        <source>Found entries of %n message envelope(s).</source>
        <translation>
            <numerusform>Nalezeny záznamy %n obálky datové zprávy.</numerusform>
            <numerusform>Nalezeny záznamy %n obálek datových zpráv.</numerusform>
            <numerusform>Nalezeny záznamy %n obálek datových zpráv.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="207"/>
        <source>Found entries of %n received message(s).</source>
        <translation>
            <numerusform>Nalezeny záznamy %n přijaté datové zprávy.</numerusform>
            <numerusform>Nalezeny záznamy %n přijatých datových zpráv.</numerusform>
            <numerusform>Nalezeny záznamy %n přijatých datových zpráv.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="208"/>
        <source>Found entries of %n sent message(s).</source>
        <translation>
            <numerusform>Nalezeny záznamy %n odeslané datové zprávy.</numerusform>
            <numerusform>Nalezeny záznamy %n odeslaných datových zpráv.</numerusform>
            <numerusform>Nalezeny záznamy %n odeslaných datových zpráv.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="209"/>
        <source>Found entries of %n unidentified message(s).</source>
        <translation>
            <numerusform>Nalezeny záznamy %n neurčené datové zprávy.</numerusform>
            <numerusform>Nalezeny záznamy %n neurčených datových zpráv.</numerusform>
            <numerusform>Nalezeny záznamy %n neurčených datových zpráv.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="216"/>
        <source>Found entries of %n delivery info(s).</source>
        <translation>
            <numerusform>Nalezeny záznamy %n dodejky.</numerusform>
            <numerusform>Nalezeny záznamy %n dodejek.</numerusform>
            <numerusform>Nalezeny záznamy %n dodejek.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="230"/>
        <source>Found entries of %n received message(s) in supplementary table.</source>
        <translation>
            <numerusform>Nalezeny záznamy %n přijaté datové zprávy v doplňkové tabulce.</numerusform>
            <numerusform>Nalezeny záznamy %n přijatých datových zpráv v doplňkové tabulce.</numerusform>
            <numerusform>Nalezeny záznamy %n přijatých datových zpráv v doplňkové tabulce.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="231"/>
        <source>Found entries of %n sent message(s) in supplementary table.</source>
        <translation>
            <numerusform>Nalezeny záznamy %n odeslané datové zprávy v doplňkové tabulce.</numerusform>
            <numerusform>Nalezeny záznamy %n odeslaných datových zpráv v doplňkové tabulce.</numerusform>
            <numerusform>Nalezeny záznamy %n odeslaných datových zpráv v doplňkové tabulce.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="232"/>
        <source>Found entries of %n unidentified message(s) in supplementary table.</source>
        <translation>
            <numerusform>Nalezeny záznamy %n neurčené datové zprávy v doplňkové tabulce.</numerusform>
            <numerusform>Nalezeny záznamy %n neurčených datových zpráv v doplňkové tabulce.</numerusform>
            <numerusform>Nalezeny záznamy %n neurčených datových zpráv v doplňkové tabulce.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="241"/>
        <source>Found entries of %n high-volume message(s) in supplementary table.</source>
        <translation>
            <numerusform>Nalezeny záznamy %n velkoobjemové datové zprávy v doplňkové tabulce.</numerusform>
            <numerusform>Nalezeny záznamy %n velkoobjemových datových zpráv v doplňkové tabulce.</numerusform>
            <numerusform>Nalezeny záznamy %n velkoobjemových datových zpráv v doplňkové tabulce.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="265"/>
        <source>Found conflicting entries for %n message(s).</source>
        <translation>
            <numerusform>Nalezeny konfliktní záznamy pro %n datovou zprávu.</numerusform>
            <numerusform>Nalezeny konfliktní záznamy pro %n datové zprávy.</numerusform>
            <numerusform>Nalezeny konfliktní záznamy pro %n datových zpráv.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="331"/>
        <source>Saved delivery info file &apos;%1&apos;.</source>
        <translation>Uložen soubor s dodejkou &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="334"/>
        <source>Cannot save delivery info file &apos;%1&apos;: %2</source>
        <translation>Nemohu uložit soubor s dodejkou &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="339"/>
        <source>Missing delivery info data for delivery info &apos;%1&apos;.</source>
        <translation>Chybí data dodejky pro dodejku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="364"/>
        <source>No reliable envelope data could be read from original database file. Won&apos;t attempt any reassembling.</source>
        <translation>Nepodařilo se přečíst spolehlivé údaje obálek datových zpráv z původního souboru databáze. Pokus o opětovné sestavení neproběhne.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="377"/>
        <source>Created database file &apos;%1&apos;.</source>
        <translation>Vytvořen soubor databáze &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="428"/>
        <source>A message database file &apos;%1&apos; has been formed from recovered data.</source>
        <translation>Ze získaných dat byl vytvořen soubor s databází datových zpráv &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="444"/>
        <source>Saved message file &apos;%1&apos;.</source>
        <translation>Uložen soubor s datovou zprávou &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="447"/>
        <source>Cannot save message file &apos;%1&apos;: %2</source>
        <translation>Nemohu uložit soubor s datovou zprávou &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="454"/>
        <source>Missing message data for received message &apos;%1&apos;.</source>
        <translation>Chybí data zprávy pro přijatou datovou zprávu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="457"/>
        <source>Missing message data for sent message &apos;%1&apos;.</source>
        <translation>Chybí data zprávy pro odeslanou datovou zprávu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="460"/>
        <source>Missing message data for message &apos;%1&apos;.</source>
        <translation>Chybí data zprávy pro datovou zprávu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="480"/>
        <source>Cannot read envelope of message &apos;%1&apos;.</source>
        <translation>Nemohu načíst obálku datové zprávy &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="536"/>
        <source>Cannot insert envelope of message &apos;%1&apos;.</source>
        <translation>Nemohu vložit obálku datové zprávy &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/db_repair/gui/mw_db_repair.cpp" line="565"/>
        <source>Cannot insert delivery info of message &apos;%1&apos;.</source>
        <translation>Nemohu vložit dodejku datové zprávy &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>RestoreSelectionModel</name>
    <message>
        <location filename="../src/models/restore_selection_model.cpp" line="190"/>
        <source>Restoration action</source>
        <translation>Operace obnovení</translation>
    </message>
    <message>
        <location filename="../src/models/restore_selection_model.cpp" line="193"/>
        <source>Data box</source>
        <translation>Schránka</translation>
    </message>
    <message>
        <location filename="../src/models/restore_selection_model.cpp" line="196"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../src/models/restore_selection_model.cpp" line="199"/>
        <source>Testing</source>
        <translation>Testovací</translation>
    </message>
    <message>
        <location filename="../src/models/restore_selection_model.cpp" line="202"/>
        <source>Box identifier</source>
        <translation>Identifikátor schránky</translation>
    </message>
    <message>
        <location filename="../src/models/restore_selection_model.cpp" line="225"/>
        <source>Messages</source>
        <translation>Zprávy</translation>
    </message>
    <message>
        <location filename="../src/models/restore_selection_model.cpp" line="289"/>
        <source>Replace</source>
        <translation>Nahradit</translation>
    </message>
    <message>
        <location filename="../src/models/restore_selection_model.cpp" line="292"/>
        <source>Create</source>
        <translation>Vytvořit</translation>
    </message>
</context>
<context>
    <name>RestoreWorker</name>
    <message>
        <location filename="../src/gui/dlg_restore_internal.cpp" line="457"/>
        <source>tag database</source>
        <translation>databáze tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_internal.cpp" line="427"/>
        <source>databases for data box &apos;%1&apos;</source>
        <translation>databáze schránky &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_internal.cpp" line="478"/>
        <source>data-box database</source>
        <translation>databáze schránek</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_internal.cpp" line="498"/>
        <source>These data failed to be restored:</source>
        <translation>Obnovování těchto dat selhalo:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_internal.cpp" line="506"/>
        <source>Restoration of these data were skipped:</source>
        <translation>Obnovování těchto dat bylo vynecháno:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_restore_internal.cpp" line="510"/>
        <source>Restoration incomplete</source>
        <translation>Obnovování nedokončeno</translation>
    </message>
</context>
<context>
    <name>SQLiteTbls</name>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="77"/>
        <source>Data box ID</source>
        <translation>ID datové schránky</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="78"/>
        <source>Data box type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="79"/>
        <location filename="../src/io/account_db_tables.cpp" line="157"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="80"/>
        <location filename="../src/io/account_db_tables.cpp" line="146"/>
        <source>Given name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="81"/>
        <location filename="../src/io/account_db_tables.cpp" line="147"/>
        <source>Middle name</source>
        <translation>Prostření jméno</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="82"/>
        <location filename="../src/io/account_db_tables.cpp" line="148"/>
        <source>Surname</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="83"/>
        <location filename="../src/io/account_db_tables.cpp" line="149"/>
        <source>Surname at birth</source>
        <translation>Rodné příjmení</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="84"/>
        <location filename="../src/io/account_db_tables.cpp" line="158"/>
        <source>Company name</source>
        <translation>Název firmy</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="85"/>
        <location filename="../src/io/account_db_tables.cpp" line="156"/>
        <source>Date of birth</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="86"/>
        <source>City of birth</source>
        <translation>Místo narození</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="87"/>
        <source>County of birth</source>
        <translation>Okres narození</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="88"/>
        <source>State of birth</source>
        <translation>Stát narození</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="89"/>
        <location filename="../src/io/account_db_tables.cpp" line="160"/>
        <source>City of residence</source>
        <translation>Sídlo - město</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="90"/>
        <location filename="../src/io/account_db_tables.cpp" line="159"/>
        <source>Street of residence</source>
        <translation>Sídlo - ulice</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="91"/>
        <location filename="../src/io/account_db_tables.cpp" line="152"/>
        <source>Number in street</source>
        <translation>Číslo orientační</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="92"/>
        <location filename="../src/io/account_db_tables.cpp" line="153"/>
        <source>Number in municipality</source>
        <translation>Číslo popisné</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="93"/>
        <location filename="../src/io/account_db_tables.cpp" line="154"/>
        <location filename="../src/io/account_db_tables.cpp" line="161"/>
        <source>Zip code</source>
        <translation>PSČ</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="94"/>
        <location filename="../src/io/account_db_tables.cpp" line="162"/>
        <source>State of residence</source>
        <translation>Sídlo - stát</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="95"/>
        <source>Nationality</source>
        <translation>Státní občanství</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="98"/>
        <source>Databox state</source>
        <translation>Stav schránky</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="99"/>
        <source>Effective OVM</source>
        <translation>Efektivní OVM</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="100"/>
        <source>Open addressing</source>
        <translation>Otevřené adresování</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="144"/>
        <source>User type</source>
        <translation>Typ uživatele</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="145"/>
        <source>Permissions</source>
        <translation>Oprávnění</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="150"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="151"/>
        <source>Street</source>
        <translation>Ulice</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="155"/>
        <source>State</source>
        <translation>Stát</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="225"/>
        <source>Public test environment data box</source>
        <translation>Schránka v prostředí veřejného testu</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="226"/>
        <source>Data-box ID</source>
        <translation>ID schránky</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="227"/>
        <source>Current long-term storage type</source>
        <translation>Současný typ datového trezoru</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="228"/>
        <source>Current long-term storage capacity</source>
        <translation>Současná kapacita datového trezoru</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="229"/>
        <source>Current long-term storage active from</source>
        <translation>Současný datový trezor aktivní od</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="230"/>
        <source>Current long-term storage active to</source>
        <translation>Současný datový trezor aktivní do</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="231"/>
        <source>Current long-term storage capacity used</source>
        <translation>Současná využitá kapacita datového trezoru</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="232"/>
        <source>Future long-term storage type</source>
        <translation>Budoucí typ datového trezoru</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="233"/>
        <source>Future long-term storage capacity</source>
        <translation>Budoucí kapacita datového trezoru</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="234"/>
        <source>Future long-term storage active from</source>
        <translation>Budoucí datový trezor aktivní od</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="235"/>
        <source>Future long-term storage active to</source>
        <translation>Budoucí datový trezor aktivní do</translation>
    </message>
    <message>
        <location filename="../src/io/account_db_tables.cpp" line="236"/>
        <source>Future long-term storage payment state</source>
        <translation>Stav platby za budoucí datový trezor</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="91"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="95"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="96"/>
        <source>Sender address</source>
        <translation>Adresa odesílatele</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="98"/>
        <source>Recipient</source>
        <translation>Adresát</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="99"/>
        <source>Recipient address</source>
        <translation>Adresa adresáta</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="106"/>
        <source>To hands</source>
        <translation>K rukám</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="107"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="108"/>
        <source>Recipient reference number</source>
        <translation>Jednací číslo adresáta</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="109"/>
        <source>Sender reference number</source>
        <translation>Jednací číslo odesílatele</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="110"/>
        <source>Recipient file mark</source>
        <translation>Spisová značka adresáta</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="111"/>
        <source>Sender file mark</source>
        <translation>Spisová značka odesílatele</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="112"/>
        <source>Law</source>
        <translation>Zákon</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="113"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="114"/>
        <source>Section</source>
        <translation>Paragraf</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="115"/>
        <source>Paragraph</source>
        <translation>Odstavec</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="116"/>
        <source>Point</source>
        <translation>Písmeno</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="117"/>
        <source>Personal delivery</source>
        <translation>Doručení do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="118"/>
        <source>Acceptance through fiction enabled</source>
        <translation>Doručení fikcí povoleno</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="120"/>
        <source>Delivery time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="121"/>
        <source>Acceptance time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="122"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="123"/>
        <source>Attachment size</source>
        <translation>Velikost příloh</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="164"/>
        <source>File name</source>
        <translation>Název souboru</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="167"/>
        <source>Mime type</source>
        <translation>Typ MIME</translation>
    </message>
    <message>
        <location filename="../src/io/message_db_tables.cpp" line="375"/>
        <source>Read locally</source>
        <translation>Přečteno lokálně</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/io/prefs_db_tables.cpp" line="55"/>
        <source>Preference Name</source>
        <translation>Název předvolby</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/io/prefs_db_tables.cpp" line="56"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/io/prefs_db_tables.cpp" line="57"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
</context>
<context>
    <name>Service</name>
    <message>
        <location filename="../src/isds/services_account.cpp" line="54"/>
        <location filename="../src/isds/services_account.cpp" line="113"/>
        <location filename="../src/isds/services_box.cpp" line="55"/>
        <location filename="../src/isds/services_box.cpp" line="124"/>
        <location filename="../src/isds/services_box.cpp" line="151"/>
        <location filename="../src/isds/services_box.cpp" line="209"/>
        <location filename="../src/isds/services_box.cpp" line="253"/>
        <location filename="../src/isds/services_box.cpp" line="320"/>
        <location filename="../src/isds/services_box.cpp" line="409"/>
        <location filename="../src/isds/services_box.cpp" line="458"/>
        <location filename="../src/isds/services_box.cpp" line="507"/>
        <location filename="../src/isds/services_box2.cpp" line="50"/>
        <location filename="../src/isds/services_box2.cpp" line="109"/>
        <location filename="../src/isds/services_box2.cpp" line="154"/>
        <location filename="../src/isds/services_box_management2.cpp" line="55"/>
        <location filename="../src/isds/services_box_management2.cpp" line="106"/>
        <location filename="../src/isds/services_box_management2.cpp" line="179"/>
        <location filename="../src/isds/services_box_management2.cpp" line="220"/>
        <location filename="../src/isds/services_box_management2.cpp" line="272"/>
        <location filename="../src/isds/services_message.cpp" line="268"/>
        <location filename="../src/isds/services_message.cpp" line="296"/>
        <location filename="../src/isds/services_message.cpp" line="354"/>
        <location filename="../src/isds/services_message.cpp" line="400"/>
        <location filename="../src/isds/services_message.cpp" line="456"/>
        <location filename="../src/isds/services_message.cpp" line="497"/>
        <location filename="../src/isds/services_message2.cpp" line="52"/>
        <location filename="../src/isds/services_message_vodz.cpp" line="161"/>
        <location filename="../src/isds/services_message_vodz.cpp" line="208"/>
        <location filename="../src/isds/services_message_vodz.cpp" line="236"/>
        <source>Insufficient input.</source>
        <translation>Nedostatečný vstup.</translation>
    </message>
    <message>
        <location filename="../src/isds/services_account.cpp" line="64"/>
        <location filename="../src/isds/services_box.cpp" line="70"/>
        <location filename="../src/isds/services_box.cpp" line="97"/>
        <location filename="../src/isds/services_box.cpp" line="163"/>
        <location filename="../src/isds/services_box.cpp" line="187"/>
        <location filename="../src/isds/services_box.cpp" line="234"/>
        <location filename="../src/isds/services_box.cpp" line="278"/>
        <location filename="../src/isds/services_box.cpp" line="340"/>
        <location filename="../src/isds/services_box.cpp" line="378"/>
        <location filename="../src/isds/services_box.cpp" line="439"/>
        <location filename="../src/isds/services_box.cpp" line="537"/>
        <location filename="../src/isds/services_box2.cpp" line="62"/>
        <location filename="../src/isds/services_box2.cpp" line="87"/>
        <location filename="../src/isds/services_box2.cpp" line="135"/>
        <location filename="../src/isds/services_box2.cpp" line="179"/>
        <location filename="../src/isds/services_box_management2.cpp" line="66"/>
        <location filename="../src/isds/services_box_management2.cpp" line="118"/>
        <location filename="../src/isds/services_box_management2.cpp" line="125"/>
        <location filename="../src/isds/services_box_management2.cpp" line="231"/>
        <location filename="../src/isds/services_box_management2.cpp" line="299"/>
        <location filename="../src/isds/services_message.cpp" line="305"/>
        <location filename="../src/isds/services_message.cpp" line="428"/>
        <location filename="../src/isds/services_message.cpp" line="522"/>
        <location filename="../src/isds/services_message2.cpp" line="78"/>
        <location filename="../src/isds/services_message_vodz.cpp" line="189"/>
        <location filename="../src/isds/services_message_vodz.cpp" line="245"/>
        <source>Error converting types.</source>
        <translation>Chyba při konverzi typů.</translation>
    </message>
    <message>
        <location filename="../src/isds/services_message.cpp" line="323"/>
        <location filename="../src/isds/services_message_vodz.cpp" line="263"/>
        <source>Missing identifier of sent message.</source>
        <translation>Chybí identifikátor odeslané zprávy.</translation>
    </message>
    <message>
        <location filename="../src/isds/services_message.cpp" line="332"/>
        <location filename="../src/isds/services_message_vodz.cpp" line="272"/>
        <source>Cannot convert sent message identifier.</source>
        <translation>Nelze konvertovat identifikátor odeslané právy.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_service.cpp" line="170"/>
        <source>Cannot access date field.</source>
        <translation>Nelze získat datum.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_service.cpp" line="179"/>
        <source>The field &apos;%1&apos; contains an invalid date &apos;%2&apos;.</source>
        <translation>Pole &apos;%1&apos; obsahuje neplatné datum &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_service.cpp" line="199"/>
        <source>Cannot access IČO field.</source>
        <translation>Nelze získat IČO.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_service.cpp" line="208"/>
        <source>The field &apos;%1&apos; contains an invalid value &apos;%2&apos;.</source>
        <translation>Pole &apos;%1&apos; obsahuje neplatnou hodnotu &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_service.cpp" line="228"/>
        <location filename="../src/datovka_shared/gov_services/service/gov_service.cpp" line="256"/>
        <source>Cannot access string.</source>
        <translation>Nelze získat řetězec.</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_service.cpp" line="236"/>
        <location filename="../src/datovka_shared/gov_services/service/gov_service.cpp" line="264"/>
        <source>The field &apos;%1&apos; contains no value.</source>
        <translation>Pole &apos;%1&apos; neobsahuje žádnou hodnotu.</translation>
    </message>
    <message>
        <location filename="../src/isds/services_box.cpp" line="473"/>
        <source>Invalid input.</source>
        <translation>Neplatný vstup.</translation>
    </message>
</context>
<context>
    <name>ServicePrivate</name>
    <message>
        <location filename="../src/isds/services_message.cpp" line="178"/>
        <location filename="../src/isds/services_message.cpp" line="224"/>
        <location filename="../src/isds/services_message_vodz.cpp" line="117"/>
        <source>Insufficient input.</source>
        <translation>Nedostatečný vstup.</translation>
    </message>
    <message>
        <location filename="../src/isds/services_message.cpp" line="205"/>
        <location filename="../src/isds/services_message.cpp" line="249"/>
        <location filename="../src/isds/services_message_vodz.cpp" line="142"/>
        <source>Error converting types.</source>
        <translation>Chyba při konverzi typů.</translation>
    </message>
</context>
<context>
    <name>SrvcMvCrrVbh</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="152"/>
        <source>Printout from the driver penalty point system</source>
        <translation>Výpis bodového hodnocení z centrálního registru řidičů</translation>
    </message>
</context>
<context>
    <name>SrvcMvIrVp</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="545"/>
        <source>Printout from the insolvency register</source>
        <translation>Výpis z insolvenčního rejstříku</translation>
    </message>
</context>
<context>
    <name>SrvcMvRtVt</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="175"/>
        <source>Printout from the criminal records</source>
        <translation>Výpis z rejstříku trestů</translation>
    </message>
</context>
<context>
    <name>SrvcMvRtpoVt</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="550"/>
        <source>Printout from the criminal records of legal persons</source>
        <translation>Výpis z rejstříku trestů právnických osob</translation>
    </message>
</context>
<context>
    <name>SrvcMvSkdVp</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="546"/>
        <source>Printout from the list of qualified suppliers</source>
        <translation>Výpis ze seznamu kvalifikovaných dodavatelů</translation>
    </message>
</context>
<context>
    <name>SrvcMvVrVp</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="579"/>
        <source>Printout from the public register</source>
        <translation>Výpis z veřejného rejstříku</translation>
    </message>
</context>
<context>
    <name>SrvcMvZrVp</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="545"/>
        <source>Printout from the company register</source>
        <translation>Výpis z živnostenského rejstříku</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVu</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_rob_vu.cpp" line="94"/>
        <source>Printout from the resident register</source>
        <translation>Výpis z registru obyvatel</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVvu</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="134"/>
        <source>Printout about the usage of entries from the resident register</source>
        <translation>Výpis o využití údajů z registru obyvatel</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="234"/>
        <source>The date of start cannot be later than the date of end.</source>
        <translation>Datum začátku nemůže být později než datum konce.</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRosVv</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="131"/>
        <source>Public printout from the person register</source>
        <translation>Veřejný výpis z registru osob</translation>
    </message>
</context>
<context>
    <name>SzrRobVvuData</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="93"/>
        <source>From</source>
        <translation>Od</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="94"/>
        <source>Select start date</source>
        <translation>Zvolte počáteční datum</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="104"/>
        <source>To</source>
        <translation>Do</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="105"/>
        <source>Select end date</source>
        <translation>Zvolte koncové datum</translation>
    </message>
</context>
<context>
    <name>SzrRosVvData</name>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="102"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="103"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>TagAssignmentModel</name>
    <message>
        <location filename="../src/models/tag_assignment_model.cpp" line="258"/>
        <source>Tags</source>
        <translation>Tagy</translation>
    </message>
</context>
<context>
    <name>TagsLabelControlWidget</name>
    <message>
        <location filename="../src/widgets/tags_label_control_widget.cpp" line="41"/>
        <location filename="../src/widgets/tags_label_control_widget.cpp" line="49"/>
        <location filename="../src/widgets/tags_label_control_widget.cpp" line="52"/>
        <location filename="../src/widgets/tags_label_control_widget.cpp" line="60"/>
        <source>Tags: %1</source>
        <translation>Tagy: %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/tags_label_control_widget.cpp" line="42"/>
        <source>local storage</source>
        <translation>místní úložiště</translation>
    </message>
    <message>
        <location filename="../src/widgets/tags_label_control_widget.cpp" line="50"/>
        <location filename="../src/widgets/tags_label_control_widget.cpp" line="61"/>
        <source>error</source>
        <translation>chyba</translation>
    </message>
    <message>
        <location filename="../src/widgets/tags_label_control_widget.cpp" line="54"/>
        <source>connected</source>
        <translation>připojeno</translation>
    </message>
    <message>
        <location filename="../src/widgets/tags_label_control_widget.cpp" line="55"/>
        <source>disconnected</source>
        <translation>odpojeno</translation>
    </message>
</context>
<context>
    <name>TagsModel</name>
    <message>
        <location filename="../src/models/tags_model.cpp" line="64"/>
        <source>Tags</source>
        <translation>Tagy</translation>
    </message>
</context>
<context>
    <name>TagsPopupControlWidget</name>
    <message>
        <location filename="../src/widgets/tags_popup_control_widget.cpp" line="59"/>
        <source>Enter sought expression.</source>
        <translation>Zadete hledaný výraz.</translation>
    </message>
    <message>
        <location filename="../src/widgets/tags_popup_control_widget.cpp" line="60"/>
        <source>Filter</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location filename="../src/widgets/tags_popup_control_widget.cpp" line="93"/>
        <source>Edit Available Tags</source>
        <translation>Editovat dostupné tagy</translation>
    </message>
    <message>
        <location filename="../src/widgets/tags_popup_control_widget.cpp" line="94"/>
        <source>Manage all available tags.</source>
        <translation>Spravovat všechny dostupné tagy.</translation>
    </message>
</context>
<context>
    <name>TaskImportMessage</name>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="112"/>
        <source>Failed to open database file of target data box &apos;%1&apos;</source>
        <translation>Chyba při otevírání databáze zpráv cílové schránky &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="120"/>
        <source>Message &apos;%1&apos; already exists in database for this data box.</source>
        <translation>Zpráva &quot;%1&quot; již existuje v databázi pro tuto schránku.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="128"/>
        <source>Message &apos;%1&apos; cannot be imported into this data box. Message does not contain any valid ID of databox corresponding with this data box.</source>
        <translation>Zpráva &quot;%1&quot; nebyla importována do aktuální schránky. Zpráva neobsahuje identifikátor datové schránky korespondující s touto schránkou.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="137"/>
        <source>Message &apos;%1&apos; cannot be inserted into database of this data box. An error occurred during insertion procedure.</source>
        <translation>Zpráva &quot;%1&quot; nebyla vložena do databáze aktuální schránky. Zpráva neobsahuje validní informace pro import nebo se vyskytla neočekávaná chyba během importu.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="185"/>
        <source>Database file &apos;%1&apos; cannot be imported into the selected data box because username of the data box and the username of the database file don&apos;t correspond.</source>
        <translation>Databázový soubor &quot;%1&quot; nemůže být importován do vybrané schránky, protože uživatelské jméno schránky nekoresponduje s názvem souboru.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="196"/>
        <source>Failed to open import database file %1&apos;.</source>
        <translation>Nepodařilo se otevřít importovaný databázový soubor &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>TaskImportZfo</name>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="203"/>
        <source>This file (message) has not been inserted into the database because the corresponding database file could not be accessed or created.</source>
        <translation>Tento soubor (zpráva) nebyl vložen do databáze, protože odpovídající databázový soubor nemohl být otevřen, nebo vytvořen.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="214"/>
        <source>Message &apos;%1&apos; already exists in the local database, data box &apos;%2&apos;.</source>
        <translation>Zpráva &quot;%1&quot; již v lokální databázi existuje, schránka &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="227"/>
        <location filename="../src/worker/task_import_zfo.cpp" line="397"/>
        <source>Couldn&apos;t read data from file for authentication on the ISDS server.</source>
        <translation>Data ze souboru nemohla být přečtena za účelem ověření serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="232"/>
        <location filename="../src/worker/task_import_zfo.cpp" line="402"/>
        <source>Error contacting ISDS server.</source>
        <translation>Chyba při navazování spojení se serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="237"/>
        <source>Message &apos;%1&apos; could not be authenticated by ISDS server.</source>
        <translation>Zpráva &apos;%1&apos; nemohla být ověřena serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="248"/>
        <location filename="../src/worker/task_import_zfo.cpp" line="417"/>
        <source>File has not been imported because an error was detected during insertion process.</source>
        <translation>Soubor nebyl importován, protože byla zjištěna chyba v průběhu ukládání.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="253"/>
        <source>Imported message &apos;%1&apos;, data box &apos;%2&apos;.</source>
        <translation>Importována zpráva &quot;%1&quot;, schránka &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="383"/>
        <source>Acceptance info for message &apos;%1&apos; already exists in the local database, data box &apos;%2&apos;.</source>
        <translation>Doručenka &quot;%1&quot; již v lokální databázi existuje, schránka &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="422"/>
        <source>Imported acceptance info for message &apos;%1&apos;, data box &apos;%2&apos;.</source>
        <translation>Importována doručenka zprávy &quot;%1&quot;, schránka &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="275"/>
        <location filename="../src/worker/task_import_zfo.cpp" line="444"/>
        <source>Wrong ZFO format. This file does not contain correct data for import.</source>
        <translation>Špatný formát ZFO. Soubor neobsahuje platná data pro import.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="376"/>
        <source>This file (acceptance info) has not been inserted into database because there isn&apos;t any related message with id &apos;%1&apos; in the databases.</source>
        <translation>Tento soubor (doručenka) nebyl vložen do databáze, protože v databázi nebyla nalezena žádná odpovídající zpráva s id &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="406"/>
        <source>Acceptance info for message &apos;%1&apos; could not be authenticated by ISDS server.</source>
        <translation>Doručenka zprávy &apos;%1&apos; nemohla být ověřena serverem ISDS.</translation>
    </message>
</context>
<context>
    <name>TaskSplitDb</name>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="106"/>
        <source>Action was cancelled and original database file was restored.</source>
        <translation>Akce byla zrušena a původní databáze byla navrácena zpět.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="130"/>
        <source>Copying database file from origin to selected location</source>
        <translation>Kopírování původní databáze do vybrané složky</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="185"/>
        <source>Creating a new database file for year &apos;%1&apos;.</source>
        <translation>Vytvářím novou databázi pro rok &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="204"/>
        <source>Existing file &apos;%1&apos; could not be deleted.</source>
        <translation>Existující soubor &quot;%1&quot; nemohl být smazán.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="236"/>
        <source>Action was cancelled and the original database is now used from location:
&apos;%1&apos;</source>
        <translation>Akce byla zrušena a původní databáze byla nově načtena z:
&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="241"/>
        <source>Moving new database files to origin database location.</source>
        <translation>Přemísťování nových databázových souborů do původního umístění.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="123"/>
        <source>Database file cannot be split according to years because this data box already uses database files split according to years.</source>
        <translation>Databázový soubor nelze rozdělit podle roků. Tato schránka již má databázi rozdělenou po jednotlivých letech.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="134"/>
        <source>Cannot copy database file for data box &apos;%1&apos; to &apos;%2&apos;. Probably not enough space on storage.</source>
        <translation>Nepodařilo se zkopirovat databázový soubor schránky &quot;%1&quot; do &quot;%2&quot;. Pravděpodobně nemáte dostatek místa na úložišti.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="143"/>
        <source>Database file for data box &apos;%1&apos; does not exist.</source>
        <translation>Databázový soubor pro schránku &quot;%1&quot; neexistuje.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="165"/>
        <source>New database file set for data box&apos;%1&apos; could not be created.</source>
        <translation>Nové databázové soubory pro schránku &quot;%1&quot; nemohly být vytvořeny.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="213"/>
        <source>New database file for data box &apos;%1&apos; related to year &apos;%2&apos; could not be created. Messages were not copied.</source>
        <translation>Nový databázový soubor schránky &quot;%1&quot; pro rok &quot;%2&quot; nemohl být vytvořen. Zprávy nebyly zkopírovány.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="222"/>
        <source>Messages from year &apos;%1&apos; for data box &apos;%2&apos; were not copied.</source>
        <translation>Zprávy korespondující s rokem &quot;%1&quot; pro schránku &quot;%2&quot; nebyly zkopírovány.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="234"/>
        <source>Error setting and opening original database for data box &apos;%1&apos;.</source>
        <translation>Chyba při otevírání původní databáze u schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="246"/>
        <source>Error while moving new databases for data box &apos;%1&apos;.</source>
        <translation>Chyba při přesouvání nových databázových souborů pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="248"/>
        <source>Action was cancelled because new databases cannot moved from
&apos;%1&apos;
to origin path
&apos;%2&apos;.</source>
        <translation>Akce byla zrušena protože se nepodařilo přesunout nové databáze z
&quot;%1&quot;
do původního umístění
&quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="250"/>
        <source>Probably not enough available space on storage. The original database is still used.</source>
        <translation>Pravděpodobně není dostatek místa na úložišti. Bude použita původní databáze.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="255"/>
        <source>Deleting old database from origin location.</source>
        <translation>Mazání staré databáze z původní lokace.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="259"/>
        <source>Error while removing original database for data box &apos;%1&apos;.</source>
        <translation>Nepodařilo se odstranit původní databázi pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="274"/>
        <source>A problem occurred when opening new databases for data box &apos;%1&apos;.</source>
        <translation>Nastal problém s otevřením nových databázových souborů pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="261"/>
        <source>Action was cancelled.</source>
        <translation>Akce byla zrušena.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="263"/>
        <source>Please, remove the original database file manually from location:
&apos;%1&apos;</source>
        <translation>Prosím, odstraňte původní databázový soubor ručně z umístění:
&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="268"/>
        <source>Opening new database files.</source>
        <translation>Otevírání nových databázových souborů.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="276"/>
        <source>Action completed but cannot open new database files.</source>
        <translation>Akce byla dokončena, nelze ale otevřít nové databázové soubory.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="278"/>
        <source>Please, restart the application.</source>
        <translation>Prosím, restartujte aplikaci.</translation>
    </message>
</context>
<context>
    <name>TaskVacuumDbSet</name>
    <message>
        <location filename="../src/worker/task_vacuum_db_set.cpp" line="172"/>
        <source>Could not determine database directory</source>
        <translation>Nelze určit adresář uložení databáze</translation>
    </message>
    <message>
        <location filename="../src/worker/task_vacuum_db_set.cpp" line="178"/>
        <source>Not enough space on device where &apos;%1&apos; resides.</source>
        <translation>Nedostatek místa na zařízení, kde se nachází &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_vacuum_db_set.cpp" line="184"/>
        <source>Calling vacuum on database set failed.</source>
        <translation>Nastala chyba při aplikaci vakua na množitě databází.</translation>
    </message>
</context>
<context>
    <name>UploadHierarchyModel</name>
    <message>
        <location filename="../src/records_management/models/upload_hierarchy_model.cpp" line="167"/>
        <source>Records Management Hierarchy</source>
        <translation>Hierarchie spisové služby</translation>
    </message>
</context>
<context>
    <name>UsersModel</name>
    <message>
        <location filename="../src/models/users_model.cpp" line="154"/>
        <source>Given Names</source>
        <translation>Jména</translation>
    </message>
    <message>
        <location filename="../src/models/users_model.cpp" line="157"/>
        <source>Last Name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../src/models/users_model.cpp" line="160"/>
        <source>User Type</source>
        <translation>Typ uživatele</translation>
    </message>
</context>
<context>
    <name>Utility</name>
    <message>
        <location filename="../src/datovka_shared/utility/strings.cpp" line="59"/>
        <source>unknown</source>
        <translation>neznámý</translation>
    </message>
</context>
</TS>
