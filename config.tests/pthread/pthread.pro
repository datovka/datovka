
QT =

TEMPLATE = app
APP_NAME = pthread_test
TARGET = $${APP_NAME}

QMAKE_CXXFLAGS = -g -O0
isEqual(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++11
}
greaterThan(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++17
}

#INCLUDEPATH +=

# QMAKE_HOST.arch provides information about the host, not the target architexture.
# https://www.qtcentre.org/threads/41426-To-detect-32-bit-or-64-bit-platform-in-pro-file
# https://blog.shantanu.io/2016/06/25/printing-a-list-of-qmake-variables/
#for(var, $$list($$enumerate_vars())) {
#    message($$var -> $$eval($$var))
#}
macx-clang-32 { #macx:*-32 {
       QMAKE_CXXFLAGS += -arch i386
}
macx-clang-64 { #macx:*-64 {
       QMAKE_CXXFLAGS += -arch x86_64
}

# Test whether pthread functionality is integrated into system library.
LIBS =

SOURCES = main.cpp
