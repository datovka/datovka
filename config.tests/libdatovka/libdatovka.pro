
QT =

TEMPLATE = app
APP_NAME = libdatovka_test
TARGET = $${APP_NAME}

QMAKE_CXXFLAGS = -g -O0
isEqual(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++11
}
greaterThan(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++17
}

#INCLUDEPATH +=

LIBS = \
	-ldatovka

unix:!macx {
	# Some users of yet another obscure Linux distro were
	# complaining about the hard-wired libxml2 include location
	# because their distro has a better place for them. Because they
	# were incompetent to create a single-line patch solving their
	# purpose a 'universal' solution is used.
	#CONFIG += link_pkgconfig
	#PKGCONFIG += libxml-2.0
	# Setting PKGCONFIG causes the linker to link against libxml2
	# which we do not need. So the shell must be invoked - hooray.
	INCLUDEPATH += $$system(pkg-config --cflags-only-I libxml-2.0 | sed -e 's/-I//g')
}

SOURCES = main.cpp
