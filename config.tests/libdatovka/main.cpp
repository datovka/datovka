
#if defined(__APPLE__) || defined(__clang__)
#  define __USE_C99_MATH
#  define _Bool bool
#else /* !__APPLE__ */
#  include <cstdbool>
#endif /* __APPLE__ */

#include <cstdio>
#include <cstdlib>
#include <libdatovka/isds.h>

static
bool check_lib_version(void)
{
#if (ISDS_LIB_VER_NUM >= ISDS_LIB_VER_CHECK(0, 7, 0))
	return true;
#else /* < libdatovka-0.7.0 */
#  error "Insufficient library version." /* This should fail at compile time. */
	fprintf(stderr, "Insufficient libdatovka version '%s'.\n", isds_lib_ver_str());
	return false;
#endif /* >= libdatovka-0.7.0 */
}

/* Added in libdatovka-0.2.1. */
static
bool have_func_isds_DTInfo(void)
{
	/* This should fail at compile time. */
	return NULL != isds_DTInfo;
}

/* Added in libdatovka-0.3.0. */
static
bool have_type_DBTYPE_PFO_REQ(void)
{
	/* This should fail at compile time. */
	return 0 != DBTYPE_PFO_REQ;
}

/* Added in libdatovka-0.3.0. */
static
bool have_func_isds_GetMessageAuthor2(void)
{
	/* This should fail at compile time. */
	return NULL != isds_GetMessageAuthor2;
}

/* Added in libdatovka-0.4.0. */
static
bool have_func_isds_CreateBigMessage(void)
{
	/* This should fail at compile time. */
	return NULL != isds_CreateBigMessage;
}

/* Added in libdatovka-0.5.0. */
static
bool have_type_DBTYPE_PFO_ARCH(void)
{
	/* This should fail at compile time. */
	return 0 != DBTYPE_PFO_ARCH;
}

/* Added in libdatovka-0.6.0. */
static
bool have_func_isds_set_xferinfo_callback(void)
{
	/* This should fail at compile time. */
	return NULL != isds_set_xferinfo_callback;
}

/* Added in libdatovka-0.7.0. */
static
bool have_type_ISDS_CREDIT_DELETED_MESSAGE_RECOVERED(void)
{
	/* This should fail at compile time. */
	return 0 != ISDS_CREDIT_DELETED_MESSAGE_RECOVERED;
}

int main(void)
{
	if (!check_lib_version()) {
		return EXIT_FAILURE;
	}

	if (!have_func_isds_DTInfo()) {
		return EXIT_FAILURE;
	}

	if (!have_type_DBTYPE_PFO_REQ()) {
		return EXIT_FAILURE;
	}

	if (!have_func_isds_GetMessageAuthor2()) {
		return EXIT_FAILURE;
	}

	if (!have_func_isds_CreateBigMessage()) {
		return EXIT_FAILURE;
	}

	if (!have_type_DBTYPE_PFO_ARCH()) {
		return EXIT_FAILURE;
	}

	if (!have_func_isds_set_xferinfo_callback()) {
		return EXIT_FAILURE;
	}

	if (!have_type_ISDS_CREDIT_DELETED_MESSAGE_RECOVERED()) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
