
#include <cstdlib>
#include <signal.h>
#include <pthread.h>

static sigset_t setint; /*!< Set of signals to be handled. */

static
bool have_pthread_sigmask(void)
{
	/* This should fail on compile time. */
	return NULL != pthread_sigmask;
}

static
bool call_pthread_sigmask(void)
{
	sigemptyset(&setint);
	sigaddset(&setint, SIGINT);
	sigaddset(&setint, SIGTERM);

	if (pthread_sigmask(SIG_BLOCK, &setint, NULL) != 0) {
		return false;
	}

	return true;
}

int main(void)
{
	if (!have_pthread_sigmask()) {
		return EXIT_FAILURE;
	}

	if (!call_pthread_sigmask()) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
