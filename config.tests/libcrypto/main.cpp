
#include <cstdlib>
#include <openssl/cms.h>

static
bool have_d2i_CMS_bio(void)
{
	/* This should fail on compile time. */
	return NULL != d2i_CMS_bio;
}

int main(void)
{
	if (!have_d2i_CMS_bio()) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
