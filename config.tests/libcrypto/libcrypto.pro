
QT =

TEMPLATE = app
APP_NAME = libcrypto_test
TARGET = $${APP_NAME}

QMAKE_CXXFLAGS = -g -O0
isEqual(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++11
}
greaterThan(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++17
}

#INCLUDEPATH +=

LIBS = \
	-lcrypto

SOURCES = main.cpp
