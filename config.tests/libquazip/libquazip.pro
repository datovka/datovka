
QT = core

TEMPLATE = app
APP_NAME = libquazip_test
TARGET = $${APP_NAME}

QMAKE_CXXFLAGS = -g -O0
isEqual(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++11
}
isEqual(QT_MAJOR_VERSION, 6) {
	QMAKE_CXXFLAGS += -std=c++17

	QT += core5compat
}

QUAZIP_PKG_NAME = quazip1-qt$${QT_MAJOR_VERSION}

#INCLUDEPATH +=

#LIBS =

unix:!macx {
	QUAZIP_INCLUDEPATH += $$system(pkg-config --cflags-only-I "$${QUAZIP_PKG_NAME}" | sed -e 's/-I//g')
	QUAZIP_LIBS += $$system(pkg-config --libs "$${QUAZIP_PKG_NAME}")
	#QUAZIP_LIBS ~= s/-l[^ ]*Core//g # remove -lQt${VER}Core

	INCLUDEPATH += \
	    $${QUAZIP_INCLUDEPATH}
	LIBS += \
	    $${QUAZIP_LIBS}
}

SOURCES = main.cpp
