
#include <cstdlib>
#include <quazip/quazip.h>

static
bool have_class_QuaZip(void)
{
	QuaZip zip;
	zip.getZipName();
	return true;
}

int main(void)
{
	if (!have_class_QuaZip()) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
