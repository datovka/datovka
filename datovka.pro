#-------------------------------------------------
#
# Project created by QtCreator 2014-03-24T10:01:11
#
#-------------------------------------------------

CONFIG += recheck
load(configure) # qtCompileTest

QT = core gui
QT += network sql widgets
isEqual(QT_MAJOR_VERSION, 5) : lessThan(QT_MINOR_VERSION, 3) {
    # Only Qt-5.2 is allowed to compile without websockets.
    warning(Compiling without websockets. Tag client is being disabled.)
} else {
    # Qt-5.3 provides websoctets so install them if you want to use full functionality.
    qtHaveModule(websockets) {
        QT += websockets
        DEFINES += HAVE_WEBSOCKETS
    } else {
        error(Qt websockets are needed to compile and work properly.)
    }
}
QT += printsupport
isEqual(QT_MAJOR_VERSION, 5) {
	QT += svg
}
greaterThan(QT_MAJOR_VERSION, 5) {
	QT += svgwidgets # QGraphicsSvgItem in Qt-6
}

# The APP_NAME and APP_ORG_NAME values may affect the configuration file
# location so do not change them.
TEMPLATE = app
APP_NAME = datovka
TARGET = $${APP_NAME}

include(pri/version.pri)
include(pri/check_qt_version.pri)

# Generate localisation. Must be run manually.
#system(lrelease datovka.pro)

# Copy Qt localisation on architectures.
macx {
	warning(Copying Qt translation from $$[QT_INSTALL_DATA].)
	system(cp $$[QT_INSTALL_DATA]/translations/qtbase_cs.qm locale/qtbase_cs.qm)
}
win32 {
	warning(Copying Qt translation from $$[QT_INSTALL_DATA].)
	system(copy $$[QT_INSTALL_DATA]/translations/qtbase_cs.qm locale/qtbase_cs.qm)
}

# Qt 5.2.1 contains a bug causing the application to crash on some drop events.
# Qt prior to version 5.4.1 may not behave correctly on some drop events.
# Version 5.4.1 should be fine.
sufficientQtVersion(5, 3, 4, 1)

isEmpty(MOC_DIR) {
	MOC_DIR = gen_moc
}
isEmpty(OBJECTS_DIR) {
	OBJECTS_DIR = gen_objects
}
isEmpty(UI_DIR) {
	UI_DIR = gen_ui
}
CONFIG += object_parallel_to_source

DEFINES += \
	ENABLE_TAGS=1 \
	DEBUG=1 \
	VERSION=\\\"$${VERSION}\\\" \
	APP_NAME=\\\"$${APP_NAME}\\\" \
	APP_ORG_DOMAIN=\\\"cz.nic\\\" \
	APP_ORG_NAME=\\\"CZ.NIC\\\"

unix:!macx {
	isEmpty(PREFIX) {
		PREFIX = "/usr/local"
	}

	BINDIR="$${PREFIX}/bin"
	DATADIR="$${PREFIX}/share"

	isEmpty(TEXT_FILES_INST_DIR) {
		TEXT_FILES_INST_DIR = "$${DATADIR}/doc/$${APP_NAME}"
	}
	LOCALE_INST_DIR = "$${DATADIR}/$${APP_NAME}/localisations"

	application.target = $${APP_NAME}
	application.path = "$${BINDIR}"
	application.files = $${APP_NAME}

	desktop.path = "$${DATADIR}/applications"
	desktop.files += deployment/datovka.desktop

	metainfo.path = "$${DATADIR}/metainfo"
	metainfo.files += deployment/datovka.metainfo.xml

	icon16.path = "$${DATADIR}/icons/hicolor/16x16/apps"
	icon16.files += "res/icons/png_app_logo/16x16/datovka.png"

	icon24.path = "$${DATADIR}/icons/hicolor/24x24/apps"
	icon24.files += "res/icons/png_app_logo/24x24/datovka.png"

	icon32.path = "$${DATADIR}/icons/hicolor/32x32/apps"
	icon32.files += "res/icons/png_app_logo/32x32/datovka.png"

	icon48.path = "$${DATADIR}/icons/hicolor/48x48/apps"
	icon48.files += "res/icons/png_app_logo/48x48/datovka.png"

	icon64.path = "$${DATADIR}/icons/hicolor/64x64/apps"
	icon64.files += "res/icons/png_app_logo/64x64/datovka.png"

	icon128.path = "$${DATADIR}/icons/hicolor/128x128/apps"
	icon128.files += "res/icons/png_app_logo/128x128/datovka.png"

	icon256.path = "$${DATADIR}/icons/hicolor/256x256/apps"
	icon256.files += "res/icons/png_app_logo/256x256/datovka.png"

	localisation.path = "$${LOCALE_INST_DIR}"
	localisation.files += locale/datovka_cs.qm \
		locale/datovka_en.qm

	additional.path = "$${TEXT_FILES_INST_DIR}"
	additional.files = \
		AUTHORS \
		COPYING

	DEFINES += DATADIR=\\\"$$DATADIR\\\" \
		PKGDATADIR=\\\"$$PKGDATADIR\\\" \
		LOCALE_INST_DIR="\"\\\"$${LOCALE_INST_DIR}\\\"\"" \
		TEXT_FILES_INST_DIR="\"\\\"$${TEXT_FILES_INST_DIR}\\\"\""

	# Portable version cannot be installed.
	isEmpty(PORTABLE_APPLICATION) {
		INSTALLS += application \
			desktop \
			metainfo \
			icon16 \
			icon24 \
			icon32 \
			icon48 \
			icon64 \
			icon128 \
			icon256 \
			localisation \
			additional
	}
}

QMAKE_CXXFLAGS = -g -O0
isEqual(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++11
}
greaterThan(QT_MAJOR_VERSION, 5) {
	QMAKE_CXXFLAGS += -std=c++17
}
QMAKE_CXXFLAGS += \
	-Wall -Wextra -pedantic \
	-Wdate-time -Wformat -Werror=format-security

#INCLUDEPATH +=

LIBS = \
	-ldatovka

isEqual(WITH_BUILT_LIBS, 1) {
	warning(Linking with locally built libraries.)
} else {
	unix:!macx {
		# Some users of yet another obscure Linux distro were
		# complaining about the hard-wired libxml2 include location
		# because their distro has a better place for them. Because they
		# were incompetent to create a single-line patch solving their
		# purpose a 'universal' solution is used.
		#CONFIG += link_pkgconfig
		#PKGCONFIG += libxml-2.0
		# Setting PKGCONFIG causes the linker to link against libxml2
		# which we do not need. So the shell must be invoked - hooray.
		INCLUDEPATH += $$system(pkg-config --cflags-only-I libxml-2.0 | sed -e 's/-I//g')
	}

	!qtCompileTest(libcrypto) {
		error(The application requires libcrypto to compile and work properly.)
	}
	!qtCompileTest(libdatovka) {
		error(The application requires libdatovka-0.7.0 or newer to compile and work properly.)
	}
	CONFIG(config_libdatovka) {
		# Can do something here.
	}
	qtCompileTest(libquazip) {
		SYSTEM_LIBQUAZIP = 1
	}
}

isEqual(STATIC, 1) {
	warning(Linking statically.)
}

!isEmpty(PORTABLE_APPLICATION) {
	warning(Building portable version.)
	DEFINES += PORTABLE_APPLICATION=1
	TARGET = $${TARGET}-portable
}

!isEmpty(DISABLE_VERSION_CHECK_BY_DEFAULT) {
	warning(Disabling version check by default.)
	DEFINES += DISABLE_VERSION_CHECK_BY_DEFAULT=1
}

!isEmpty(DISABLE_VERSION_NOTIFICATION) {
	warning(Disabling version notification.)
	DEFINES += DISABLE_VERSION_NOTIFICATION=1
}

macx {
	ICON = res/datovka.icns

	OSX_MIN="10.6" # Qt 5.5.1 and earlier.
	isEqual(QT_MAJOR_VERSION, 5) {
		greaterThan(QT_MINOR_VERSION, 5) {
			OSX_MIN="10.7" # Qt 5.6.0.
		}
	} else:isEqual(QT_MAJOR_VERSION, 6) {
		contains(QT_ARCH, x86_64) {
			OSX_MIN="10.7" # Qt 6.0.
		}
		contains(QT_ARCH, arm64) {
			OSX_MIN="10.15" # Qt 6.5.
		}
	}

	contains(QT_ARCH, arm64) {
		contains(QT_ARCH, x86_64)|contains(QT_ARCH, i386) {
			error(Multiple architectures specified together with arm64.)
		}
		QMAKE_CXXFLAGS += -arch arm64
		message(Building for arm64.)
	} else:contains(QT_ARCH, x86_64) {
		contains(QT_ARCH, arm64)|contains(QT_ARCH, i386) {
			error(Multiple architectures specified together with x86_64.)
		}
		QMAKE_CXXFLAGS += -arch x86_64
		message(Building for x86_64.)
	} else:contains(QT_ARCH, i386) {
		contains(QT_ARCH, arm64)|contains(QT_ARCH, x86_64) {
			error(Multiple architectures specified together with i386.)
		}
		message(Building for i386.)
		QMAKE_CXXFLAGS += -arch i386
	} else {
		error(Unknown architecture $$QT_ARCH.)
	}

	LIB_LOCATION=""
	isEqual(WITH_BUILT_LIBS, 1) {
		isEqual(STATIC, 1):contains(QT_ARCH, arm64):!contains(QT_ARCH, x86_64):!contains(QT_ARCH, i386) {
			LIB_LOCATION=libs/static_built_arm64
		} else:isEqual(STATIC, 1):!contains(QT_ARCH, arm64):contains(QT_ARCH, x86_64):!contains(QT_ARCH, i386) {
			LIB_LOCATION=libs/static_built_x86_64
		} else:isEqual(STATIC, 1):!contains(QT_ARCH, arm64):!contains(QT_ARCH, x86_64):contains(QT_ARCH, i386) {
			LIB_LOCATION=libs/static_built_i386
		} else:!isEqual(STATIC, 1):contains(QT_ARCH, arm64):!contains(QT_ARCH, x86_64):!contains(QT_ARCH, i386) {
			LIB_LOCATION=libs/shared_built_arm64
		} else:!isEqual(STATIC, 1):!contains(QT_ARCH, arm64):contains(QT_ARCH, x86_64):!contains(QT_ARCH, i386) {
			LIB_LOCATION=libs/shared_built_x86_64
		} else:!isEqual(STATIC, 1):!contains(QT_ARCH, arm64):!contains(QT_ARCH, x86_64):contains(QT_ARCH, i386) {
			LIB_LOCATION=libs/shared_built_i386
		}
	}

	# See https://bugreports.qt.io/browse/QTBUG-28097
	# for further details.
	QMAKE_CXXFLAGS += -mmacosx-version-min=$$OSX_MIN
	QMAKE_CXXFLAGS += -stdlib=libc++
	CONFIG += c++11
	isEmpty(SDK_VER) {
		# There is no way how to pass this variable into lrelease so
		# it must be set manually.
		SDK_VER = 10.11
	}
	QMAKE_MAC_SDK = macosx$${SDK_VER}
	QMAKE_MACOSX_DEPLOYMENT_TARGET = $$OSX_MIN

	QMAKE_INFO_PLIST = deployment/datovka.plist
	SED_EXT = -e
	QMAKE_POST_LINK += sed -i "$${SED_EXT}" "s/@VERSION@/$${VERSION}/g" "./$${TARGET}.app/Contents/Info.plist";
	QMAKE_POST_LINK += rm -f "./$${TARGET}.app/Contents/Info.plist$${SED_EXT}";

	!equals(LIB_LOCATION, "") {
		INCLUDEPATH += \
			$${LIB_LOCATION}/include \
			$${LIB_LOCATION}/include/libxml2
		LIBPATH += \
			$${LIB_LOCATION}/lib

		LIBS += \
			-lexpat \
			-lxml2 \
			-lcurl \
			-liconv
	} else {
		INCLUDEPATH += /usr/local/include \
			/opt/local/include
		LIBPATH += /usr/local/lib \
			/opt/local/lib
	}

	localisation.path = "Contents/Resources/locale"
	localisation.files += locale/datovka_cs.qm \
		locale/datovka_en.qm
	localisation.files += locale/qtbase_cs.qm

	additional.path = "Contents/Resources"
	additional.files = \
		AUTHORS \
		COPYING \
		ChangeLog

	QMAKE_BUNDLE_DATA +=\
		localisation \
		additional
}

win32 {
        RC_FILE += res/datovka.rc

	#QUADVERSION_COMMAS=$$replace(VERSION, '\.', ',')
	# Development builds tent to have more than 3 version numbers.
	# Version info needs exactly 4.
	VERSION_NUMBERS=$$split(VERSION, '.')
	VERSION_NUMBERS_SIZE=$$size(VERSION_NUMBERS)
	lessThan(VERSION_NUMBERS_SIZE, 3) {
		error(Need at least 3 version numbers.)
	}
	isEqual(VERSION_NUMBERS_SIZE, 3) {
		# Add fourth entry.
		VERSION_NUMBERS+=0
	}
	greaterThan(VERSION_NUMBERS_SIZE, 3) {
		# Take only 4.
		VERSION_NUMBERS=$$member(VERSION_NUMBERS, 0, 3)
	}
	QUADVERSION_COMMAS=$$join(VERSION_NUMBERS, ',')
	unset(VERSION_NUMBERS)
	unset(VERSION_NUMBERS_SIZE)
	DEFINES += \
		WIN32=1 \
		QUADVERSION_COMMAS=$${QUADVERSION_COMMAS} \
		INRC_FILEDESCR=\\\"Datovka\\\" \
		INRC_ORIG_FNAME=\\\"$${APP_NAME}.exe\\\"

	ARCH = ""
	contains(QT_ARCH, x86_64):!contains(QT_ARCH, i386) {
		ARCH="x86_64"
	} else:!contains(QT_ARCH, x86_64):contains(QT_ARCH, i386) {
		ARCH = "i386"
	} else {
		error(Could not detect target architecture.)
	}

	LIB_LOCATION=""
	equals(ARCH, "x86_64") {
		LIB_LOCATION=libs/shared_built_x86_64
	} else:equals(ARCH, "i386") {
		LIB_LOCATION=libs/shared_built_i386
	}

	INCLUDEPATH = \
		$${LIB_LOCATION}/include/ \
		$${LIB_LOCATION}/include/libxml2

	LIBS = $${_PRO_FILE_PWD_}/$${LIB_LOCATION}/bin/libdatovka-8.dll

	atLeastQtVersion(6, 7, 0) {
		# OpenSSL-3 can be used with Qt-6.5 and newer.
		# https://www.qt.io/blog/moving-to-openssl-3-in-binary-builds-starting-from-qt-6.5-beta-2
		equals(ARCH, "x86_64") {
			LIBS += $${_PRO_FILE_PWD_}/$${LIB_LOCATION}/bin/libcrypto-3-x64.dll
		}
	} else:atLeastQtVersion(5, 12, 4) {
		# OpenSSL-1.1.1 required since Qt-5.12.4.
		# https://www.qt.io/blog/2019/06/17/qt-5-12-4-released-support-openssl-1-1-1
		# https://mta.openssl.org/pipermail/openssl-dev/2016-August/008351.html
		equals(ARCH, "i386") {
			LIBS += $${_PRO_FILE_PWD_}/$${LIB_LOCATION}/bin/libcrypto-1_1.dll
		}
		equals(ARCH, "x86_64") {
			LIBS += $${_PRO_FILE_PWD_}/$${LIB_LOCATION}/bin/libcrypto-1_1-x64.dll
		}
	} else {
		# Only 32-bit Windows targets supported.
		LIBS += $${_PRO_FILE_PWD_}/$${LIB_LOCATION}/bin/libeay32.dll
	}

	SOURCES += \
	    src/compat/compat_win.c \
	    src/compat/compat_win64.cpp \
	    src/system/native_event_filter.cpp

	HEADERS += \
	    src/compat/compat_win.h \
	    src/compat/compat_win64.h \
	    src/system/native_event_filter.h

	# Since Qt-5.14 the debug or release directories are not created automatically.
	CONFIG(debug, debug|release) {
		DESTDIR = debug
	} else {
		DESTDIR = release
	}
} else {
	LIBS += \
		-lcrypto
}

!isEmpty(UNSPECIFIED_VARIABLE) {
	# This block of configuration code is only a hack. It is not intended
	# to be actually used in code configuration. Its sole purpose is
	# to trick the lupdate tool into reading header file content and
	# to force it to handle namespaces properly.
	# If this is not done then errors like:
	# 'Qualifying with unknown namespace/class ::Isds'
	# are being issued. The translator is then unable to find the proper
	# translation strings and some translations are thus not loaded by
	# the running application.
	# See e.g. https://stackoverflow.com/questions/6504902/lupdate-error-qualifying-with-unknown-namespace-class

	warning(UNSPECIFIED_VARIABLE set to \'$${UNSPECIFIED_VARIABLE}\'.)
	INCLUDEPATH += .
}

isEqual(SYSTEM_LIBQUAZIP, 1) {
	QUAZIP_PKG_NAME = quazip1-qt$${QT_MAJOR_VERSION}
	QUAZIP_INCLUDEPATH += $$system(pkg-config --cflags-only-I "$${QUAZIP_PKG_NAME}" | sed -e 's/-I//g')
	QUAZIP_LIBS += $$system(pkg-config --libs "$${QUAZIP_PKG_NAME}")
	QUAZIP_LIBS ~= s/-l[^ ]*Core//g # remove -lQt${VER}Core

	message(Compiling against found libquazip ($${QUAZIP_LIBS}).)

	INCLUDEPATH += \
	    $${QUAZIP_INCLUDEPATH}
	LIBS += \
	    $${QUAZIP_LIBS}
} else {
	BUNDLED_QUAZIP = 1
}
isEqual(BUNDLED_QUAZIP, 1) {
	message(Compiling and statically linking bundled QuaZIP sources.)

	greaterThan(QT_MAJOR_VERSION, 5) {
		QT += core5compat # QuaZip-1.4
	}

	# 3rd party libraries and packages
	DEFINES += \
	    QUAZIP_STATIC
	SOURCES += \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/JlCompress.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/qioapi.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quaadler32.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quachecksum32.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quacrc32.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quaziodevice.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazip.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipdir.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipfile.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipfileinfo.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipnewinfo.cpp \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/unzip.c \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/zip.c
	HEADERS += \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/JlCompress.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/ioapi.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/minizip_crypt.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quaadler32.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quachecksum32.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quacrc32.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quaziodevice.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazip.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazip_global.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazip_qt_compat.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipdir.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipfile.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipfileinfo.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipnewinfo.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/unzip.h \
	    src/datovka_shared/3rdparty/quazip-1.4/quazip/zip.h
	INCLUDEPATH += \
	    src/datovka_shared/3rdparty/quazip-1.4
	LIBS += \
	    -lz
}

unix {
	SOURCES += \
	    src/system/unix_signals.cpp

	HEADERS += \
	    src/system/unix_signals.h

	!qtCompileTest(pthread) {
		qtCompileTest(libpthread) {
			LIBS += \
			    -lpthread
		} else {
			error(The application requires pthreads to compile and work properly.)
		}
	}
}

SOURCES += \
    src/about.cpp \
    src/app_version_info.cpp \
    src/cli/cli.cpp \
    src/cli/cli_login.cpp \
    src/cli/cli_parser.cpp \
    src/cli/cli_pin.cpp \
    src/cli/cmd_compose.cpp \
    src/cli/cmd_tokeniser.cpp \
    src/common.cpp \
    src/compat/compat_time.cpp \
    src/crypto/crypto.c \
    src/crypto/crypto_threads.cpp \
    src/datovka_shared/app_version_info.cpp \
    src/datovka_shared/compat_qt/random.cpp \
    src/datovka_shared/crypto/crypto_pin.c \
    src/datovka_shared/crypto/crypto_pwd.c \
    src/datovka_shared/crypto/crypto_trusted_certs.c \
    src/datovka_shared/crypto/crypto_version.cpp \
    src/datovka_shared/crypto/crypto_wrapped.cpp \
    src/datovka_shared/gov_services/helper.cpp \
    src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp \
    src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp \
    src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp \
    src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp \
    src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp \
    src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp \
    src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp \
    src/datovka_shared/gov_services/service/gov_service_form_field.cpp \
    src/datovka_shared/gov_services/service/gov_service.cpp \
    src/datovka_shared/gov_services/service/gov_services_all.cpp \
    src/datovka_shared/gov_services/service/gov_szr_rob_vu.cpp \
    src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp \
    src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp \
    src/datovka_shared/graphics/colour.cpp \
    src/datovka_shared/graphics/graphics.cpp \
    src/datovka_shared/html/html_export.cpp \
    src/datovka_shared/identifiers/account_id.cpp \
    src/datovka_shared/io/matomo_reporter.cpp \
    src/datovka_shared/io/prefs_db.cpp \
    src/datovka_shared/io/prefs_db_tables.cpp \
    src/datovka_shared/io/records_management_db.cpp \
    src/datovka_shared/io/records_management_db_tables.cpp \
    src/datovka_shared/io/sqlite/db.cpp \
    src/datovka_shared/io/sqlite/db_single.cpp \
    src/datovka_shared/io/sqlite/table.cpp \
    src/datovka_shared/isds/account_interface.cpp \
    src/datovka_shared/isds/box_interface.cpp \
    src/datovka_shared/isds/box_interface2.cpp \
    src/datovka_shared/isds/error.cpp \
    src/datovka_shared/isds/generic_interface.cpp \
    src/datovka_shared/isds/internal_conversion.cpp \
    src/datovka_shared/isds/json_conversion.cpp \
    src/datovka_shared/isds/message_attachment.cpp \
    src/datovka_shared/isds/message_interface.cpp \
    src/datovka_shared/isds/message_interface2.cpp \
    src/datovka_shared/isds/message_interface_vodz.cpp \
    src/datovka_shared/isds/to_text_conversion.cpp \
    src/datovka_shared/isds/type_conversion.cpp \
    src/datovka_shared/isds/type_description.cpp \
    src/datovka_shared/json/basic.cpp \
    src/datovka_shared/json/helper.cpp \
    src/datovka_shared/json/object.cpp \
    src/datovka_shared/json/report.cpp \
    src/datovka_shared/localisation/countries.cpp \
    src/datovka_shared/localisation/localisation.cpp \
    src/datovka_shared/log/global.cpp \
    src/datovka_shared/log/log.cpp \
    src/datovka_shared/log/log_c.cpp \
    src/datovka_shared/log/log_device.cpp \
    src/datovka_shared/log/memory_log.cpp \
    src/datovka_shared/records_management/conversion.cpp \
    src/datovka_shared/records_management/io/records_management_connection.cpp \
    src/datovka_shared/records_management/json/entry_error.cpp \
    src/datovka_shared/records_management/json/service_info.cpp \
    src/datovka_shared/records_management/json/stored_files.cpp \
    src/datovka_shared/records_management/json/upload_account_status.cpp \
    src/datovka_shared/records_management/json/upload_file.cpp \
    src/datovka_shared/records_management/json/upload_hierarchy.cpp \
    src/datovka_shared/records_management/models/upload_hierarchy_proxy_model.cpp \
    src/datovka_shared/settings/account.cpp \
    src/datovka_shared/settings/account_container.cpp \
    src/datovka_shared/settings/pin.cpp \
    src/datovka_shared/settings/prefs.cpp \
    src/datovka_shared/settings/prefs_helper.cpp \
    src/datovka_shared/settings/records_management.cpp \
    src/datovka_shared/utility/date_time.cpp \
    src/datovka_shared/utility/pdf_printer.cpp \
    src/datovka_shared/utility/strings.cpp \
    src/datovka_shared/worker/pool.cpp \
    src/db_repair/gui/mw_db_repair.cpp \
    src/db_repair/io/corrupt_message_db.cpp \
    src/delegates/account_status_delegate.cpp \
    src/delegates/account_status_value.cpp \
    src/delegates/combobox_delegate.cpp \
    src/delegates/progress_delegate.cpp \
    src/delegates/progress_value.cpp \
    src/delegates/tag_item.cpp \
    src/delegates/tags_delegate.cpp \
    src/dimensions/dimensions.cpp \
    src/global.cpp \
    src/gov_services/gui/dlg_gov_service.cpp \
    src/gov_services/gui/dlg_gov_services.cpp \
    src/gov_services/models/gov_service_list_model.cpp \
    src/gui/datovka.cpp \
    src/gui/dlg_about.cpp \
    src/gui/dlg_account_from_db.cpp \
    src/gui/dlg_backup.cpp \
    src/gui/dlg_backup_internal.cpp \
    src/gui/dlg_backup_progress.cpp \
    src/gui/dlg_change_directory.cpp \
    src/gui/dlg_change_pwd.cpp \
    src/gui/dlg_contacts.cpp \
    src/gui/dlg_convert_import_from_mobile_app.cpp \
    src/gui/dlg_correspondence_overview.cpp \
    src/gui/dlg_create_account.cpp \
    src/gui/dlg_download_messages.cpp \
    src/gui/dlg_ds_search.cpp \
    src/gui/dlg_email_content.cpp \
    src/gui/dlg_import_zfo.cpp \
    src/gui/dlg_import_zfo_result.cpp \
    src/gui/dlg_licence.cpp \
    src/gui/dlg_msg_box_detail.cpp \
    src/gui/dlg_msg_box_detail_nonmodal.cpp \
    src/gui/dlg_msg_box_nonmodal.cpp \
    src/gui/dlg_msg_search.cpp \
    src/gui/dlg_pin_input.cpp \
    src/gui/dlg_pin_setup.cpp \
    src/gui/dlg_preferences.cpp \
    src/gui/dlg_prefs.cpp \
    src/gui/dlg_proxysets.cpp \
    src/gui/dlg_restore.cpp \
    src/gui/dlg_restore_internal.cpp \
    src/gui/dlg_restore_progress.cpp \
    src/gui/dlg_send_message.cpp \
    src/gui/dlg_signature_detail.cpp \
    src/gui/dlg_tag_assignment.cpp \
    src/gui/dlg_tag.cpp \
    src/gui/dlg_tags.cpp \
    src/gui/dlg_tag_settings.cpp \
    src/gui/dlg_timestamp_expir.cpp \
    src/gui/dlg_tool_bar.cpp \
    src/gui/dlg_update_portable.cpp \
    src/gui/dlg_user_ext2.cpp \
    src/gui/dlg_view_changelog.cpp \
    src/gui/dlg_view_filename_param_list.cpp \
    src/gui/dlg_view_log.cpp \
    src/gui/dlg_view_zfo.cpp \
    src/gui/dlg_wait_progress.cpp \
    src/gui/dlg_yes_no_checkbox.cpp \
    src/gui/helper.cpp \
    src/gui/icon_container.cpp \
    src/gui/isds_login.cpp \
    src/gui/message_operations.cpp \
    src/gui/mw_status.cpp \
    src/gui/styles.cpp \
    src/identifiers/account_id_db.cpp \
    src/identifiers/message_id.cpp \
    src/initialisation.cpp \
    src/initialisation_gui.cpp \
    src/io/account_db.cpp \
    src/io/account_db_tables.cpp \
    src/io/account_shadow_helper.cpp \
    src/io/db_convert_mobile_to_desktop.cpp \
    src/io/db_tables.cpp \
    src/io/dbs.cpp \
    src/io/export_file_name.cpp \
    src/io/exports.cpp \
    src/io/file_downloader.cpp \
    src/io/filesystem.cpp \
    src/io/imports.cpp \
    src/io/isds_helper.cpp \
    src/io/isds_login.cpp \
    src/io/isds_sessions.cpp \
    src/io/load_mobile_zip_package.cpp \
    src/io/message_db.cpp \
    src/io/message_db_set.cpp \
    src/io/message_db_set_container.cpp \
    src/io/message_db_set_delegated.cpp \
    src/io/message_db_single.cpp \
    src/io/message_db_tables.cpp \
    src/io/session_keeper.cpp \
    src/io/sqlite/delayed_access_db.cpp \
    src/io/stats_reporter.cpp \
    src/io/tag_client.cpp \
    src/io/tag_container.cpp \
    src/io/tag_db.cpp \
    src/io/tag_db_tables.cpp \
    src/io/update.cpp \
    src/io/update_checker.cpp \
    src/isds/account_conversion.cpp \
    src/isds/box_conversion.cpp \
    src/isds/box_conversion2.cpp \
    src/isds/error_conversion.cpp \
    src/isds/generic_conversion.cpp \
    src/isds/initialisation.cpp \
    src/isds/internal_conversion.cpp \
    src/isds/internal_type_conversion.cpp \
    src/isds/message_conversion.cpp \
    src/isds/message_conversion2.cpp \
    src/isds/message_functions.cpp \
    src/isds/qt_signal_slot.cpp \
    src/isds/services_account.cpp \
    src/isds/services_box.cpp \
    src/isds/services_box2.cpp \
    src/isds/services_box_management2.cpp \
    src/isds/services_login.cpp \
    src/isds/services_message.cpp \
    src/isds/services_message_vodz.cpp \
    src/isds/services_message2.cpp \
    src/isds/session.cpp \
    src/isds/to_text_conversion.cpp \
    src/json/action_entry.cpp \
    src/json/backup.cpp \
    src/json/srvr_profile.cpp \
    src/json/srvr_req_envelope.cpp \
    src/json/srvr_resp_envelope.cpp \
    src/json/tag_assignment.cpp \
    src/json/tag_assignment_hash.cpp \
    src/json/tag_entry.cpp \
    src/json/tag_message_id.cpp \
    src/json/tag_text_search_request.cpp \
    src/json/tag_text_search_request_hash.cpp \
    src/main.cpp \
    src/model_interaction/account_interaction.cpp \
    src/model_interaction/attachment_interaction.cpp \
    src/model_interaction/view_interaction.cpp \
    src/models/accounts_model.cpp \
    src/models/accounts_settings_model.cpp \
    src/models/account_status_model.cpp \
    src/models/account_unique_numid.cpp \
    src/models/action_list_model.cpp \
    src/models/attachments_model.cpp \
    src/models/backup_selection_model.cpp \
    src/models/collapsible_account_proxy_model.cpp \
    src/models/combo_box_model.cpp \
    src/models/data_box_contacts_model.cpp \
    src/models/helper.cpp \
    src/models/message_download_model.cpp \
    src/models/message_found_model.cpp \
    src/models/messages_model.cpp \
    src/models/prefs_model.cpp \
    src/models/progress_proxy_model.cpp \
    src/models/restore_selection_model.cpp \
    src/models/sort_filter_proxy_model.cpp \
    src/models/table_model.cpp \
    src/models/tag_assignment_model.cpp \
    src/models/tags_model.cpp \
    src/models/users_model.cpp \
    src/records_management/content/automatic_upload_target.cpp \
    src/records_management/content/automatic_upload_target_interaction.cpp \
    src/records_management/gui/dlg_records_management.cpp \
    src/records_management/gui/dlg_records_management_check_msgs.cpp \
    src/records_management/gui/dlg_records_management_stored.cpp \
    src/records_management/gui/dlg_records_management_upload.cpp \
    src/records_management/gui/dlg_records_management_upload_progress.cpp \
    src/records_management/models/upload_hierarchy_model.cpp \
    src/records_management/widgets/svg_view.cpp \
    src/settings/account.cpp \
    src/settings/accounts.cpp \
    src/settings/convert_to_prefs.cpp \
    src/settings/preferences.cpp \
    src/settings/prefs_defaults.cpp \
    src/settings/prefs_specific.cpp \
    src/settings/proxy.cpp \
    src/settings/registry.cpp \
    src/settings/system_detection.cpp \
    src/settings/tag_container_settings.cpp \
    src/settings/tag_container_settings_apply.cpp \
    src/single/single_instance.cpp \
    src/views/attachment_table_view.cpp \
    src/views/external_size_hint_tool_button.cpp \
    src/views/lowered_table_view.cpp \
    src/views/lowered_table_widget.cpp \
    src/views/lowered_tree_view.cpp \
    src/views/table_home_end_filter.cpp \
    src/views/table_key_press_filter.cpp \
    src/views/table_space_selection_filter.cpp \
    src/views/table_tab_ignore_filter.cpp \
    src/widgets/account_status_widget.cpp \
    src/widgets/tags_label_control_widget.cpp \
    src/widgets/tags_popup_control_widget.cpp \
    src/worker/task.cpp \
    src/worker/task_add_data_box_user2.cpp \
    src/worker/task_authenticate_message.cpp \
    src/worker/task_change_pwd.cpp \
    src/worker/task_check_downloaded_list.cpp \
    src/worker/task_delete_data_box_user2.cpp \
    src/worker/task_download_credit_info.cpp \
    src/worker/task_download_dt_info.cpp \
    src/worker/task_download_message.cpp \
    src/worker/task_download_message_list.cpp \
    src/worker/task_download_owner_info.cpp \
    src/worker/task_download_password_info.cpp \
    src/worker/task_download_user_info.cpp \
    src/worker/task_erase_message.cpp \
    src/worker/task_get_data_box_users2.cpp \
    src/worker/task_get_owner_info_from_login2.cpp \
    src/worker/task_get_user_info_from_login2.cpp \
    src/worker/task_import_message.cpp \
    src/worker/task_import_zfo.cpp \
    src/worker/task_keep_alive.cpp \
    src/worker/task_pdz_info.cpp \
    src/worker/task_pdz_send_info.cpp \
    src/worker/task_records_management_stored_messages.cpp \
    src/worker/task_records_management_upload_account_status.cpp \
    src/worker/task_search_owner.cpp \
    src/worker/task_search_owner_fulltext.cpp \
    src/worker/task_send_message.cpp \
    src/worker/task_split_db.cpp \
    src/worker/task_update_data_box_user2.cpp \
    src/worker/task_upload_attachment.cpp \
    src/worker/task_vacuum_db_set.cpp \
    src/worker/task_verify_message.cpp

HEADERS += \
    src/about.h \
    src/cli/cli.h \
    src/cli/cli_login.h \
    src/cli/cli_parser.h \
    src/cli/cli_pin.h \
    src/cli/cmd_compose.h \
    src/cli/cmd_tokeniser.h \
    src/common.h \
    src/compat/compat_time.h \
    src/crypto/crypto.h \
    src/crypto/crypto_funcs.h \
    src/crypto/crypto_threads.h \
    src/datovka_shared/app_version_info.h \
    src/datovka_shared/compat/compiler.h \
    src/datovka_shared/compat_qt/misc.h \
    src/datovka_shared/compat_qt/random.h \
    src/datovka_shared/compat_qt/variant.h \
    src/datovka_shared/crypto/crypto_pin.h \
    src/datovka_shared/crypto/crypto_pwd.h \
    src/datovka_shared/crypto/crypto_trusted_certs.h \
    src/datovka_shared/crypto/crypto_version.h \
    src/datovka_shared/crypto/crypto_wrapped.h \
    src/datovka_shared/gov_services/helper.h \
    src/datovka_shared/gov_services/service/gov_mv_crr_vbh.h \
    src/datovka_shared/gov_services/service/gov_mv_ir_vp.h \
    src/datovka_shared/gov_services/service/gov_mv_rt_vt.h \
    src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.h \
    src/datovka_shared/gov_services/service/gov_mv_skd_vp.h \
    src/datovka_shared/gov_services/service/gov_mv_vr_vp.h \
    src/datovka_shared/gov_services/service/gov_mv_zr_vp.h \
    src/datovka_shared/gov_services/service/gov_service_form_field.h \
    src/datovka_shared/gov_services/service/gov_service.h \
    src/datovka_shared/gov_services/service/gov_services_all.h \
    src/datovka_shared/gov_services/service/gov_szr_rob_vu.h \
    src/datovka_shared/gov_services/service/gov_szr_rob_vvu.h \
    src/datovka_shared/gov_services/service/gov_szr_ros_vv.h \
    src/datovka_shared/graphics/colour.h \
    src/datovka_shared/graphics/graphics.h \
    src/datovka_shared/html/html_export.h \
    src/datovka_shared/identifiers/account_id.h \
    src/datovka_shared/identifiers/account_id_p.h \
    src/datovka_shared/io/db_tables.h \
    src/datovka_shared/io/matomo_reporter.h \
    src/datovka_shared/io/prefs_db.h \
    src/datovka_shared/io/prefs_db_tables.h \
    src/datovka_shared/io/records_management_db.h \
    src/datovka_shared/io/records_management_db_tables.h \
    src/datovka_shared/io/sqlite/db.h \
    src/datovka_shared/io/sqlite/db_single.h \
    src/datovka_shared/io/sqlite/table.h \
    src/datovka_shared/isds/account_interface.h \
    src/datovka_shared/isds/box_interface.h \
    src/datovka_shared/isds/box_interface2.h \
    src/datovka_shared/isds/error.h \
    src/datovka_shared/isds/generic_interface.h \
    src/datovka_shared/isds/internal_conversion.h \
    src/datovka_shared/isds/json_conversion.h \
    src/datovka_shared/isds/message_attachment.h \
    src/datovka_shared/isds/message_interface.h \
    src/datovka_shared/isds/message_interface2.h \
    src/datovka_shared/isds/message_interface_vodz.h \
    src/datovka_shared/isds/to_text_conversion.h \
    src/datovka_shared/isds/type_conversion.h \
    src/datovka_shared/isds/type_description.h \
    src/datovka_shared/isds/types.h \
    src/datovka_shared/json/basic.h \
    src/datovka_shared/json/helper.h \
    src/datovka_shared/json/object.h \
    src/datovka_shared/json/report.h \
    src/datovka_shared/localisation/countries.h \
    src/datovka_shared/localisation/localisation.h \
    src/datovka_shared/log/global.h \
    src/datovka_shared/log/log_c.h \
    src/datovka_shared/log/log_common.h \
    src/datovka_shared/log/log_device.h \
    src/datovka_shared/log/log.h \
    src/datovka_shared/log/memory_log.h \
    src/datovka_shared/records_management/conversion.h \
    src/datovka_shared/records_management/io/records_management_connection.h \
    src/datovka_shared/records_management/json/entry_error.h \
    src/datovka_shared/records_management/json/service_info.h \
    src/datovka_shared/records_management/json/stored_files.h \
    src/datovka_shared/records_management/json/upload_account_status.h \
    src/datovka_shared/records_management/json/upload_file.h \
    src/datovka_shared/records_management/json/upload_hierarchy.h \
    src/datovka_shared/records_management/models/upload_hierarchy_proxy_model.h \
    src/datovka_shared/settings/account.h \
    src/datovka_shared/settings/account_container.h \
    src/datovka_shared/settings/account_p.h \
    src/datovka_shared/settings/pin.h \
    src/datovka_shared/settings/prefs.h \
    src/datovka_shared/settings/prefs_helper.h \
    src/datovka_shared/settings/records_management.h \
    src/datovka_shared/utility/date_time.h \
    src/datovka_shared/utility/pdf_printer.h \
    src/datovka_shared/utility/strings.h \
    src/datovka_shared/worker/pool.h \
    src/db_repair/gui/mw_db_repair.h \
    src/db_repair/io/corrupt_message_db.h \
    src/delegates/account_status_delegate.h \
    src/delegates/account_status_value.h \
    src/delegates/combobox_delegate.h \
    src/delegates/progress_delegate.h \
    src/delegates/progress_value.h \
    src/delegates/tag_item.h \
    src/delegates/tags_delegate.h \
    src/dimensions/dimensions.h \
    src/global.h \
    src/gov_services/gui/dlg_gov_service.h \
    src/gov_services/gui/dlg_gov_services.h \
    src/gov_services/models/gov_service_list_model.h \
    src/gui/datovka.h \
    src/gui/dlg_about.h \
    src/gui/dlg_account_from_db.h \
    src/gui/dlg_backup.h \
    src/gui/dlg_backup_internal.h \
    src/gui/dlg_backup_progress.h \
    src/gui/dlg_change_directory.h \
    src/gui/dlg_change_pwd.h \
    src/gui/dlg_contacts.h \
    src/gui/dlg_convert_import_from_mobile_app.h \
    src/gui/dlg_correspondence_overview.h \
    src/gui/dlg_create_account.h \
    src/gui/dlg_download_messages.h \
    src/gui/dlg_ds_search.h \
    src/gui/dlg_email_content.h \
    src/gui/dlg_import_zfo.h \
    src/gui/dlg_import_zfo_result.h \
    src/gui/dlg_licence.h \
    src/gui/dlg_msg_box_detail.h \
    src/gui/dlg_msg_box_detail_nonmodal.h \
    src/gui/dlg_msg_box_nonmodal.h \
    src/gui/dlg_msg_search.h \
    src/gui/dlg_pin_input.h \
    src/gui/dlg_pin_setup.h \
    src/gui/dlg_preferences.h \
    src/gui/dlg_prefs.h \
    src/gui/dlg_proxysets.h \
    src/gui/dlg_restore.h \
    src/gui/dlg_restore_internal.h \
    src/gui/dlg_restore_progress.h \
    src/gui/dlg_send_message.h \
    src/gui/dlg_signature_detail.h \
    src/gui/dlg_tag_assignment.h \
    src/gui/dlg_tag.h \
    src/gui/dlg_tags.h \
    src/gui/dlg_tag_settings.h \
    src/gui/dlg_timestamp_expir.h \
    src/gui/dlg_tool_bar.h \
    src/gui/dlg_update_portable.h \
    src/gui/dlg_user_ext2.h \
    src/gui/dlg_view_changelog.h \
    src/gui/dlg_view_filename_param_list.h \
    src/gui/dlg_view_log.h \
    src/gui/dlg_view_zfo.h \
    src/gui/dlg_wait_progress.h \
    src/gui/dlg_yes_no_checkbox.h \
    src/gui/helper.h \
    src/gui/icon_container.h \
    src/gui/isds_login.h \
    src/gui/message_operations.h \
    src/gui/mw_status.h \
    src/gui/styles.h \
    src/identifiers/account_id_db.h \
    src/identifiers/message_id.h \
    src/initialisation.h \
    src/initialisation_gui.h \
    src/io/account_db.h \
    src/io/account_db_tables.h \
    src/io/account_shadow_helper.h \
    src/io/db_convert_mobile_to_desktop.h \
    src/io/dbs.h \
    src/io/export_file_name.h \
    src/io/exports.h \
    src/io/file_downloader.h \
    src/io/filesystem.h \
    src/io/imports.h \
    src/io/isds_helper.h \
    src/io/isds_login.h \
    src/io/isds_sessions.h \
    src/io/load_mobile_zip_package.h \
    src/io/message_db.h \
    src/io/message_db_set.h \
    src/io/message_db_set_container.h \
    src/io/message_db_single.h \
    src/io/message_db_tables.h \
    src/io/session_keeper.h \
    src/io/sqlite/delayed_access_db.h \
    src/io/stats_reporter.h \
    src/io/tag_client.h \
    src/io/tag_container.h \
    src/io/tag_db.h \
    src/io/tag_db_tables.h \
    src/io/update.h \
    src/io/update_checker.h \
    src/isds/account_conversion.h \
    src/isds/box_conversion.h \
    src/isds/box_conversion2.h \
    src/isds/error_conversion.h \
    src/isds/generic_conversion.h \
    src/isds/conversion_internal.h \
    src/isds/initialisation.h \
    src/isds/internal_conversion.h \
    src/isds/internal_type_conversion.h \
    src/isds/message_conversion.h \
    src/isds/message_conversion2.h \
    src/isds/message_functions.h \
    src/isds/qt_signal_slot.h \
    src/isds/services.h \
    src/isds/services_internal.h \
    src/isds/services_login.h \
    src/isds/session.h \
    src/isds/to_text_conversion.h \
    src/json/action_entry.h \
    src/json/backup.h \
    src/json/srvr_profile.h \
    src/json/srvr_req_envelope.h \
    src/json/srvr_resp_envelope.h \
    src/json/tag_assignment.h \
    src/json/tag_assignment_hash.h \
    src/json/tag_entry.h \
    src/json/tag_message_id.h \
    src/json/tag_text_search_request.h \
    src/json/tag_text_search_request_hash.h \
    src/model_interaction/account_interaction.h \
    src/model_interaction/attachment_interaction.h \
    src/model_interaction/view_interaction.h \
    src/models/accounts_model.h \
    src/models/accounts_settings_model.h \
    src/models/account_status_model.h \
    src/models/account_unique_numid.h \
    src/models/action_list_model.h \
    src/models/attachments_model.h \
    src/models/backup_selection_model.h \
    src/models/collapsible_account_proxy_model.h \
    src/models/combo_box_model.h \
    src/models/data_box_contacts_model.h \
    src/models/helper.h \
    src/models/message_download_model.h \
    src/models/message_found_model.h \
    src/models/messages_model.h \
    src/models/prefs_model.h \
    src/models/progress_proxy_model.h \
    src/models/restore_selection_model.h \
    src/models/sort_filter_proxy_model.h \
    src/models/table_model.h \
    src/models/tag_assignment_model.h \
    src/models/tags_model.h \
    src/models/users_model.h \
    src/records_management/content/automatic_upload_target.h \
    src/records_management/content/automatic_upload_target_interaction.h \
    src/records_management/gui/dlg_records_management.h \
    src/records_management/gui/dlg_records_management_check_msgs.h \
    src/records_management/gui/dlg_records_management_stored.h \
    src/records_management/gui/dlg_records_management_upload.h \
    src/records_management/gui/dlg_records_management_upload_progress.h \
    src/records_management/models/upload_hierarchy_model.h \
    src/records_management/widgets/svg_view.h \
    src/settings/account.h \
    src/settings/accounts.h \
    src/settings/convert_to_prefs.h \
    src/settings/preferences.h \
    src/settings/prefs_defaults.h \
    src/settings/prefs_specific.h \
    src/settings/proxy.h \
    src/settings/registry.h \
    src/settings/system_detection.h \
    src/settings/tag_container_settings.h \
    src/settings/tag_container_settings_apply.h \
    src/single/single_instance.h \
    src/views/attachment_table_view.h \
    src/views/external_size_hint_tool_button.h \
    src/views/lowered_table_view.h \
    src/views/lowered_table_widget.h \
    src/views/lowered_tree_view.h \
    src/views/table_home_end_filter.h \
    src/views/table_key_press_filter.h \
    src/views/table_space_selection_filter.h \
    src/views/table_tab_ignore_filter.h \
    src/widgets/account_status_widget.h \
    src/widgets/tags_label_control_widget.h \
    src/widgets/tags_popup_control_widget.h \
    src/worker/message_emitter.h \
    src/worker/task.h \
    src/worker/task_add_data_box_user2.h \
    src/worker/task_authenticate_message.h \
    src/worker/task_change_pwd.h \
    src/worker/task_check_downloaded_list.h \
    src/worker/task_delete_data_box_user2.h \
    src/worker/task_download_credit_info.h \
    src/worker/task_download_dt_info.h \
    src/worker/task_download_message.h \
    src/worker/task_download_message_list.h \
    src/worker/task_download_owner_info.h \
    src/worker/task_download_password_info.h \
    src/worker/task_download_user_info.h \
    src/worker/task_erase_message.h \
    src/worker/task_get_data_box_users2.h \
    src/worker/task_get_owner_info_from_login2.h \
    src/worker/task_get_user_info_from_login2.h \
    src/worker/task_import_message.h \
    src/worker/task_import_zfo.h \
    src/worker/task_keep_alive.h \
    src/worker/task_pdz_info.h \
    src/worker/task_pdz_send_info.h \
    src/worker/task_records_management_stored_messages.h \
    src/worker/task_records_management_upload_account_status.h \
    src/worker/task_search_owner.h \
    src/worker/task_search_owner_fulltext.h \
    src/worker/task_send_message.h \
    src/worker/task_split_db.h \
    src/worker/task_update_data_box_user2.h \
    src/worker/task_upload_attachment.h \
    src/worker/task_vacuum_db_set.h \
    src/worker/task_verify_message.h

FORMS += \
    src/db_repair/ui/mw_db_repair.ui \
    src/gov_services/ui/dlg_gov_service.ui \
    src/gov_services/ui/dlg_gov_services.ui \
    src/gui/ui/datovka.ui \
    src/gui/ui/dlg_about.ui \
    src/gui/ui/dlg_account_from_db.ui \
    src/gui/ui/dlg_backup.ui \
    src/gui/ui/dlg_backup_progress.ui \
    src/gui/ui/dlg_change_directory.ui \
    src/gui/ui/dlg_change_pwd.ui \
    src/gui/ui/dlg_contacts.ui \
    src/gui/ui/dlg_convert_import_from_mobile_app.ui \
    src/gui/ui/dlg_correspondence_overview.ui \
    src/gui/ui/dlg_create_account.ui \
    src/gui/ui/dlg_download_messages.ui \
    src/gui/ui/dlg_ds_search.ui \
    src/gui/ui/dlg_email_content.ui \
    src/gui/ui/dlg_import_zfo_result.ui \
    src/gui/ui/dlg_import_zfo.ui \
    src/gui/ui/dlg_licence.ui \
    src/gui/ui/dlg_msg_search.ui \
    src/gui/ui/dlg_pin_input.ui \
    src/gui/ui/dlg_pin_setup.ui \
    src/gui/ui/dlg_preferences.ui \
    src/gui/ui/dlg_prefs.ui \
    src/gui/ui/dlg_proxysets.ui \
    src/gui/ui/dlg_restore.ui \
    src/gui/ui/dlg_send_message.ui \
    src/gui/ui/dlg_signature_detail.ui \
    src/gui/ui/dlg_tag.ui \
    src/gui/ui/dlg_tag_settings.ui \
    src/gui/ui/dlg_tags.ui \
    src/gui/ui/dlg_timestamp_expir.ui \
    src/gui/ui/dlg_tool_bar.ui \
    src/gui/ui/dlg_update_portable.ui \
    src/gui/ui/dlg_user_ext2.ui \
    src/gui/ui/dlg_view_changelog.ui \
    src/gui/ui/dlg_view_filename_param_list.ui \
    src/gui/ui/dlg_view_log.ui \
    src/gui/ui/dlg_view_zfo.ui \
    src/records_management/ui/dlg_records_management.ui \
    src/records_management/ui/dlg_records_management_check_msgs.ui \
    src/records_management/ui/dlg_records_management_progress.ui \
    src/records_management/ui/dlg_records_management_upload.ui

RESOURCES += \
    res/resources.qrc

TRANSLATIONS += locale/datovka_en.ts \
    locale/datovka_cs.ts

OTHER_FILES +=
